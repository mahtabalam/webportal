<?php

require_once realpath(dirname(__FILE__)).'/fileGenerator.class.php';

/*
 * This file create pdf file and take actions of downloading it to browser, saving it to server and emailing it
 */
class Pdf extends FileGenerator {

	function __construct($authorInfo) {
		parent::__construct($authorInfo);

	}
	
/*
 * function to download created pdf file
 */

	private function download($filename) {
	
		header('Content-Type: application/pdf');
		header('Content-Disposition: attachment;filename='.$filename.'.pdf');
		header('Cache-Control: max-age=0');
		echo $this->getContents();
	 
}

/*
 * this function handles creation and various action on pdf as suggested by user
 */
	public function makePdf($content,$header='',$action='Download',$name='example') {
		$this->makeFile($content,$header);
		$this->createWriter('PDF');
		switch(strtolower($action)) {
		case 'download':
		$this->download($name);
		break;	
		case 'email':
		case 'mail':
			$this->sendAsEmail($name,'pdf');
		break;

		}

	}





}
$arr=Array
        (
                        0 => Array
                        (
                                'buyable' => 2,
                                'nodeID' => 344,
                                'nodeName' => 'ROOT',
                                'parentID' => 344
                        ),
                        1 => Array
                        (
                                'buyable' => 1,
                                'nodeID' => 3,
                                'nodeName' => 'In-Market',
                                'parentID' => 344
                        )
		);
try {
$obj=new Pdf(); 
$obj->makePdf($arr,'','email','varun');
}
catch(Exception $e) {
echo $e->getMessage();	
}
	die;      

