<?php
session_start();
include_once(dirname(__FILE__).'/include/config.php');
include_once ABSPATH."/common/config/TrackerSynonyms.inc";
include_once(dirname(__FILE__).'/adserver/servfuncs.php');
include_once(dirname(__FILE__).'/adserver/vastfuncs.php');
include_once(dirname(__FILE__).'/../include/plist.php');
include_once ABSPATH."/common/adserving/campaign/processSRIAdServing.php";
include_once(dirname(__FILE__).'/adserver/trkfuncs.php');
global $conn;

$adid = $_REQUEST['ad_id'];

header("Content-type: text/xml");
echo "<?xml version='1.0' encoding='utf-8' ?>\n";
echo "<VAST version='2.0'>\n";

$sql = $conn->Prepare("select c.id as cid, a.branded_img_right_ref_imgname as title, a.* from campaign c inner join campaign_members cm on c.id=cm.cid inner join ads a on a.id=cm.ad_id where a.id=?");
$rs = $conn->execute($sql, array($adid));

if($rs&&$rs->recordcount()>0){
	$ad_format = $rs->fields['ad_format'];
	$ads[$ad_format] = $rs->fields;
	
	if($ads[$ad_format]['ad_format']=='video'){
    	getLinearXML($ads, $ad_format, 0);
    	
	}elseif($ads[$ad_format]['ad_format']=='overlay' || $ads[$ad_format]['ad_format']=='branded'){
		getNonLinearXML($ads, $ad_format, 0);
	}
	echo "<Extensions></Extensions>\n";
	echo "</VAST>\n";
}else{
	echo "<error>Ad Not Found</error>\n";
    echo "</VAST>\n";
}

?>