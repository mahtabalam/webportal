<?php
  require_once('include/config.php');
error_reporting(E_ALL);  
# Script to allow load balancers to check the health of app servers
  if($_GET['sec'] != 'eu239fjsakfana'){
    echo "NO";
    exit(0);
  }
  $hour=date('h');
  $suffix=$hour%2;
  $sql1 = "select count(*) from ad_log$suffix";
  $rs1=$conn->execute($sql1);
  $sql2 = "select count(*) from memcamp";
  $rs2=$conn->execute($sql2);
 
  $slaveStatus=getSlaveStatus();
  $load = sys_getloadavg();
/*
  $rs2=1;
*/
  if((!$rs1) || (!$rs2) || ($load[0]>50 && $load[2]>50) || (!$slaveStatus)){
    header('HTTP/1.1 503 Service Temporarily Unavailable');
    header('Status: 503 Service Temporarily Unavailable');
    header('Retry-After: 300');//300 seconds
  } else {
    echo "OK";
  }

  function getSlaveStatus()
  {
    global $conn;
    $sql3="show slave status";

 $rs3=$conn->execute($sql3);
//  var_dump($rs3);
$data=$rs3->getRows();
if(empty($data))
{
return false;
}
$slave=false;

if( ($data[0]['Seconds_Behind_Master']==0) &&($data[0]['Slave_IO_Running']=="Yes") && ($data[0]['Slave_SQL_Running']=="Yes"))  
{
apc_store('hc_success_time',time());
return true;
}
$last_success_time=apc_fetch("hc_success_time");
if (empty($last_success_time))
  return false;

//Master is available but slave is lagging behind upto a certain limit. After that disable the machine to allow it to catch up
if(($data[0]['Seconds_Behind_Master'] <10800)  && ($data[0]['Slave_SQL_Running']=="Yes"))
  {
         return true;
  }
 //Master is Not available but Sql slave is running - slave lag is undefined
if(($data[0]['Slave_IO_Running']=="No") && ($data[0]['Slave_SQL_Running']=="Yes"))
  {
  return true;
 }

return $slave;
  }

?>
