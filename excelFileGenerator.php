<?php

require_once realpath(dirname(__FILE__)).'/fileGenerator.class.php';
/*
 * This file create pdf file and take actions of downloading it to browser, saving it to server and emailing it
 */
class ExcelFileGenerator extends FileGenerator {

	
	function __construct($authorInfo) {
		
		parent::__construct($authorInfo);

	}
	

/*
 * this function will download the created file with given name in browser of user
 * @param : $filename is the name of file without extensions
 */
	private function download($filename) {
	
		header('Content-Type: application/vnd.ms-excel');
	    	header('Content-Disposition: attachment; filename='.$filename.'.xlsx');
		echo $this->getContents();
			
}

/*
 * This function will process given input and create excel and take required action as suggested
 * @params : $content is associative array to be written in file
 * $header is optional associatiative array to be written vertically on top of excel file
 * $action is 'download' ,'email' or 'returnContent' the action you want to take over excel
 * $mail_params expect an array of format array(
 *
 * 						"recipient"=>"",
 * 						"from"=>"",  
 * 						"subject"=>"",
 * 						"message"=>""
 * 						)
 *
 */			

	public function makeExcel($content,$header,$action,$name,$mail_params='') {
		if($name=='')
			$name='example';
		$name=preg_replace('/[^aA-zZ0-9\_\-]/', '', $name);
		
		if($action=='')
			$action='Download';
		
		$this->makeFile($content,$header);
		$this->createWriter('Excel2007');
		
		switch(strtolower($action)) {
			case 'download':
				$this->download($name);
				break;	
			case 'email':
			case 'mail':
				$this->sendAsEmail($name,'excel',$mail_params);
				break;
			case 'save':
				$this->saveFile($name.".xlsx");
				break;
			case 'returnContent':
				return $this->getContents();
			default :
				throw new excelException('Invalid Action taken',0);
		}

	}
}

$arr=Array
(
		0 => Array
		(
				'buyable' => 2,
				'nodeID' => 344,
				'nodeName' => 'ROOT',
				'parentID' => 344
		),
		1 => Array
		(
				'buyable' => 1,
				'nodeID' => 3,
				'nodeName' => 'In-Market',
				'parentID' => 344
		)
	);
$headers=array(
	"CPM-Report"=>"123",
	"NAME"=>"Krishna Balram"

);
try {
$obj=new ExcelFileGenerator();
$obj->makeExcel($arr,$headers,'download','',array("recipient"=>"robin.goyal@vdopia.com"));

}
catch(Exception $e) {
	echo $e->getMessage();


}
die;


