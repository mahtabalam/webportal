<?php
if (! empty ( $argc ) && strstr ( $argv [0], basename ( __FILE__ ) )) {
  # xdebug_disable();
  ini_set("log_errors", "false");
  require_once dirname(__FILE__) . '/../include/config.php';
  ini_set("log_errors", "true");
  # xdebug_enable();
  
  require_once dirname(__FILE__) . '/../common/include/channelchoice.php';

  class ChannelChoiceBitSetTest {
    private $argc;
    private $argv;
    public function __construct($ac, &$av){
      $this->argc = $ac; 
      $this->argv = $av; 
    }

    private function printUsage(){
      $n = $this->argv[0];
      echo "usage: $n [-c <campaign_id> -C -v <campaign_id> -V ]\n";
      echo "  -c - convert channel_choice to channel_set for one campaign \n";
      echo "  -C - convert channel_choice to channel_set for all active campaigns \n";
      echo "  -V - verify that channel_choice matches channel_set for one campaign \n";
      echo "  -V - verify that channel_choice matches channel_set for all active campaigns \n";
      echo "  -h - help. Print this message \n";
    }
    
    function fullArrayDiff($left, $right) { 
      return array_diff(array_merge($left, $right), array_intersect($left, $right)); 
    } 
  
    private function verifyChannelSet($campaignId, $channel_choice, $channel_set){
      $goodResult = array(true, "Channel set and choice matched for $campaignId. choice = $channel_choice.");
      $badResult = array(false, "EXCEPTION: Channel set and choice don't match for $campaignId. channel_choice = $channel_choice.");

      if(($channel_choice == NULL || $channel_choice == "NULL")){
        if($channel_set == NULL){
          return $goodResult;
        } else {
          return $badResult;
        }
      } else if($channel_set == NULL){
          return $badResult;
      }
      $conv_channel_arr = VdoChannelChoice::bitSetToIntArr($channel_set);
      $camp_channel_arr = explode(',', $channel_choice);
      $diff = $this->fullArrayDiff($conv_channel_arr, $camp_channel_arr);
      if(count($diff) == 0){
        return $goodResult;
      } else {
          var_dump($diff);
          return $badResult;
      }
    }

    private function updateChannelSet($campaignId, $channel_choice){
      global $conn;
      $sql = $conn->Prepare("update campaign set channel_set=? where id=?");
      echo $channel_choice."\n";
      $channel_set = VdoChannelChoice::intListToBitSet($channel_choice);
      echo VdoChannelChoice::strToHex($channel_set) ."\n";
      $rs = $conn->Execute($sql, array( $channel_set, $campaignId));
      if(!$rs){ throw new Exception($conn->ErrorMsg());}
      return "channel_set set to match $channel_choice.";
    }

    private function convertCampaign($campaignId){
      global $conn;
      $sql = $conn->Prepare("select channel_choice from campaign where id=?");
      $rs = $conn->Execute($sql,array($campaignId));
      if(!$rs){ throw new Exception($conn->ErrorMsg());}
      if($rs && $rs->recordcount()>0){
        $rows = $rs->getRows();
        echo $this->updateChannelSet($campaignId, $rows[0]['channel_choice']) . "\n"; 
      } else {
        echo "No such channel -- $campaignId."."\n";
      }
    }

    private function convertActiveCampaigns(){
      global $conn;
      $sql = $conn->Prepare("select id,channel_choice from campaign where status='active'");
      $rs = $conn->Execute($sql,array());
      if(!$rs){ throw new Exception($conn->ErrorMsg());}
      if($rs->recordcount() > 2000){ throw new Exception($conn->ErrorMsg());}
      $idChanSetArray = array();
      $idArray = array();
      if($rs && $rs->recordcount()>0){
        $rows = $rs->getRows();
        $sqlStr = "update campaign set channel_set = CASE id ";
        $whereStr = "";
        foreach($rows as $row){
          array_push($idChanSetArray, $row['id'], VdoChannelChoice::intListToBitSet($row['channel_choice']));
          array_push($idArray, $row['id']);
          $sqlStr .= " WHEN ? THEN ? "; 
          if($whereStr == ""){
            $whereStr .= "?";
          } else {
            $whereStr .= ",?";
          }
        }
        $sqlStr .= " END where id in ($whereStr) ";
        // echo $sqlStr."\n";
        // echo "id array - " . count($idArray) . " map array - " . count($idChanSetArray) ."\n";
        $beginT = microtime();
        $sql = $conn->Prepare($sqlStr);
        $rs = $conn->Execute($sql, array_merge( $idChanSetArray, $idArray));
        $endT = microtime();
        if(!$rs){ throw new Exception($conn->ErrorMsg());}
        echo "Updating " . count($idArray) . " campaigns completed in " . ($endT - $beginT)/1000 . "ms. \n";
      } else {
        echo "No active channels."."\n";
      }
    }
 
    private function validateCampaign($campaignId){
      global $conn;
      $sql = $conn->Prepare("select channel_choice,channel_set from campaign where id=?");
      $rs = $conn->Execute($sql,array($campaignId));
      if(!$rs){ throw new Exception($conn->ErrorMsg());}
      if($rs && $rs->recordcount()>0){
        $rows = $rs->getRows();
        $result = $this->verifyChannelSet($campaignId, $rows[0]['channel_choice'], $rows[0]['channel_set']);
        echo $result[1]. "\n"; 
      } else {
        echo "No such channel -- $campaignId."."\n";
      }
    }

    private function validateActiveCampaigns(){
      global $conn;
      $sql = $conn->Prepare("select id,channel_choice,channel_set from campaign where status='active'");
      $rs = $conn->Execute($sql,array());
      if(!$rs){ throw new Exception($conn->ErrorMsg());}
      if($rs && $rs->recordcount()>0){
        $rows = $rs->getRows();
        $bad = 0;
        foreach($rows as $row){
          $result = $this->verifyChannelSet($row['id'], $row['channel_choice'], $row['channel_set']);
          if($result[0] != true){ 
            echo $result[1]. "\n"; 
            $bad++; 
          }
        }
        echo "Verification result: " . (count($rows) - $bad) . " out of " . count($rows) . " passed.\n";
      } else {
        echo "No active channels."."\n";
      }
    }

    public function run(){
      $options = getopt("c:Cv:Vh");
      $didSomething = false;
      if(isset($options['c'])){
        $this->convertCampaign($options['c']); 
        $didSomething = true;
      }
      if(isset($options['C'])){
        $this->convertActiveCampaigns(); 
        $didSomething = true;
      }
      if(isset($options['v'])){
        $this->validateCampaign($options['v']); 
        $didSomething = true;
      }
      if(isset($options['V'])){
        $this->validateActiveCampaigns(); 
        $didSomething = true;
      }
      if(isset($options['h'])){
        $this->printUsage(); 
        $didSomething = true;
      }
      if(!$didSomething){
        $this->printUsage(); 
      }
    }
  }
  
  $ccbst = new ChannelChoiceBitSetTest($argc, $argv);
  $ccbst->run(); 
}

?>
