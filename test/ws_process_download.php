<?php

function errorExit($err, $line1, $line2, $line3){
  echo "Error: $err\n";
  if($line1){ echo "$line1\n";}
  if($line2){ echo "$line2\n";}
  if($line3){ echo "$line3\n";}
  exit(0);
}

function usageExit(){
  @errorExit("usage: php ws_process.php -f <data_file>");
}

// Only allow this to run from the command line
if (!empty($argc) && strstr($argv[0], basename(__FILE__))) {
  require_once('/var/www/html/dev/pankaj_adserver/include/adodb/adodb.inc.php');
  $conn = &ADONewConnection('mysql');
  $conn->PConnect('localhost', 'root', 'vdoROOT123', 'atomic');
  # 174.129.165.246 - - [17/Sep/2010:10:51:09 +0000] "GET /adserver/iphonetracker.php?m=vi&ci=1675&ai=3725&chid=1785&ou=rd&di=7c6ccd5ca06e08e7a94dba9417b568f68c0df845&dt=Unknown&rand=1234 HTTP/1.1" 500 - "-" "-" 4565 217 794
  $pattern = '/^([^-]+)(-) ([^ ]+) \[([^:]+):([^ ]+) ([^\]]+)\] "(.*) (.*) (.*)" ([0-9\-]+) ([0-9\-]+) "(.*)" "(.*)"/';
  $reqPattern ='/\/adserver\/iphonetracker.php\?m=([^&]*)&ci=([^&]*)&ai=([^&]*)&chid=([^&]*)&ou=([^&]*)&di=([^&]*)/';
  //$reqPattern ='/\/adserver\/iphonetracker.php\?m=([a-zA-Z])&/';

  $options = getopt("f:");
  if(!isset($options["f"])){ usageExit(); }
  $fh = fopen($options["f"], "r") or die($php_errormsg);

  $i = 1;
  $aff = 0;
  $requests = array();
  while (! feof($fh)) {
    // read each line and trim off leading/trailing whitespace
    if ($s = trim(fgets($fh,16384))) {
       // match the line to the pattern
       if (preg_match($pattern,$s,$matches)) {
         // put each part of the match in an appropriately-named variable
         list($whole_match,$remote_host,$logname,$user,$date, $time, $tz, $method,
              $request,$protocol,$status,$bytes,$referer, $user_agent) = $matches;
         if(preg_match($reqPattern,$request,$reqMatches)){
              list($req_match, $method, $ci, $ai, $chid, $ou, $di) = $reqMatches;
         } else {
           errorExit("Invalid request",$reqPattern, $request, $s);
         }
         // keep track of the count of each request
         $user_agent=mysql_escape_string($user_agent);
         $dateArr=explode("/",$date);
         $months = array ("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04",
                    "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08",
                    "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");
         $mdate = $dateArr[2]."-".$months[$dateArr[1]]."-".$dateArr[0]." $time";
         $sql = "insert into download_log(campaign_id, device_id, download_ts) values('$ci', '$di', '$mdate') on duplicate key update download_ts='$mdate'";
         echo "$sql\n";
         $rs = $conn->execute($sql);
         if(!$rs){
           error_log("Error: Error from DB - ".$conn->ErrorMsg());
           error_log("Dummy");
         }
         ///$aff = mysql_affected_rows();
         ///$mod += $aff;
         ///if( $aff != 0){
           ///error_log("rows affected = ".$aff);
         ///}
       } else {
         // complain if the line didn't match the pattern
         error_log("Error:Can't parse line $i: $s");
         error_log("pattern to parse line: $pattern");
       }
    }
    $i++;
    if($i%100 == 0) echo "Processed $i rows - modified $mod \n";
  }
  fclose($fh) or die($php_errormsg);
}
?>


