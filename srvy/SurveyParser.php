<?php

class SurveyParser {
	private $literal;
	private $current_index;
	
	public function __construct() {
	
	}
	
	private function P($str) {
		$str_res = var_export ( $str, true );
		if (is_array ( $str )) {
			$str_res = "Values Passed:";
			foreach ( $str as $key => $value ) {
				$str_res .= ("$key=>" . var_export ( $value, true ) . "\t");
			}
		}
		echo date ( "Y-m-d H:i:s" ) . " - SurveyParser: " . $str_res . "<br>\n";
		trigger_error ( "SurveyParser: " . $str_res, E_USER_WARNING );
	}
	
	//output is array
	public function run($literal, $current_index, $check_output_source, $output_value) {
		$literal = $literal . "-";
		$len = strlen ( $literal );
		
		if ($check_output_source == true) {
			//$this->P("Current Index:$current_index:checking output");
			$current_index = $this->checkOutput ( $literal, $current_index, $output_value, $len );
		}
		
		//echo $current_index;exit;
		

		$questions = array ();
		$check_output = false;
		$num = "";
		$count = 0;
		
		while ( $current_index < $len ) {
			$a = $literal [$current_index];
			if (is_numeric ( $a ) == true) {
				$num = $num . $a;
			} else if ($a == ",") {
				if ($num != "") {
					array_push ( $questions, $num );
				}
				$num = "";
			} else if ($a == "[") {
				$check_output = true;
				array_push ( $questions, $num );
				break;
			} else if ($a == "-") {
				$current_index ++;
				if (strlen ( $num ) == 0)
					continue;
				if ($num != "") {
					array_push ( $questions, $num );
				}
				
				break;
			} else if ($a == "]" && $count != 0) {
				if ($num != "") {
					array_push ( $questions, $num );
				}
				break;
			} else if ($a == "]" && $count == 0) {
				//echo $current_index;
				$current_index = $this->iteratebrackets ( $literal, $current_index + 1, $len );
				//echo $current_index;
				

				continue;
			}
			$count ++;
			$current_index ++;
		}
		
		if (count($questions)==0)
			return null;
		return array ('questions' => $questions, 'check_output' => $check_output, 'current_index' => $current_index );
	}
	
	private function checkOutput($literal, $current_index, $output_value, $len) {
		while ( true ) {
			$substr = substr ( $literal, $current_index );
			$option_index = strpos ( $substr, "-" );
			if ($option_index === false) {
				return $len;
			}
			$options = substr ( $substr, 1, $option_index - 1 );
			
			$options_len = strlen ( $options );
			
			$options = explode ( ":", $options );
			
			$found = false;
			if(is_array($output_value)==false){
				$output_value=array($output_value);
			}
			foreach($output_value as $output_data){
			foreach ( $options as $values ) {
				if ($values == $output_data) {
					$found = true;
					break;
				}
			}
			}
			if ($found == true) {
				return $current_index + $options_len + 2;
			}
			
			$current_index = $this->getNextOptionBracket ( $literal, $current_index, $len );
			if (isset ( $current_index ) == false) {
				return $len;
				//throw error
			}
		}
	}
	
	private function getNextOptionBracket($literal, $current_index, $len) {
		//     $this->P("getNextOptionBracket called with values:$current_index,$len");
		

		$queue_value = 0;
		$count = 0;
		while ( $current_index < $len ) {
			if ($literal [$current_index] == "[") {
				$queue_value ++;
			} else if ($literal [$current_index] == "]") {
				$queue_value --;
				if ($queue_value == 0) {
					return $current_index + 1;
				}
			}
			$current_index ++;
		}
		return null;
	}
	
	private function iteratebrackets($literal, $current_index, $len) {
		//  $this->P("iteratebrackets called with values:$literal,$current_index,$len");
		$queue_value = 0;
		while ( $current_index < $len ) {
			if ($literal [$current_index] == "[") {
				$queue_value ++;
			} else if ($literal [$current_index] == "]") {
				$queue_value --;
				if ($queue_value <= 0) {
					break;
				}
			
			}
			if ($queue_value == 0) {
				break;
			}
			$current_index ++;
		}
		//exit;
		

		return $current_index + 1;
	}

}
//1[<13:13 to 17-3-4-5-6-7-8][18 to 24:25 to 34:35 to 54:55+-2[Nope-3-9-10-11][VERY true-3-14-15-16]-8-12-13]
//It should be run from command line only..
if (! empty ( $argc ) && strstr ( $argv [0], basename ( __FILE__ ) )) {
	$SurveyParser = new SurveyParser ();
	// $literal1="110[a:b-10-15][c:d-18,19-20[x:f-22][z-23]]-19";
	$literal = "110[a:b-10-15][c:d-18,19-20[x:f-22][z-23]-24,25]-19";
	// $SurveyParser->run($literal,$current_index,$check_output_source,$output_value)
	print_r ( $literal );
	$output = $SurveyParser->run ( $literal, 0, false, "x" );
	var_dump ( $output );
	$output = $SurveyParser->run ( $literal, 3, true, "b" );
	var_dump ( $output );
	$output = $SurveyParser->run ( $literal, 11, false, "b" );
	var_dump ( $output );
	$output = $SurveyParser->run ( $literal, 13, false, "b" );
	var_dump ( $output );
	$output = $SurveyParser->run ( $literal, 52, false, "b" );
	var_dump ( $output );
	$output = $SurveyParser->run ( $literal, 3, true, "c" );
	var_dump ( $output );
	$output = $SurveyParser->run ( $literal, 25, false, "c" );
	var_dump ( $output );
	$output = $SurveyParser->run ( $literal, 27, true, "f" );
	var_dump ( $output );
	$output = $SurveyParser->run ( $literal, 34, false, "f" );
	var_dump ( $output );
	$output = $SurveyParser->run ( $literal, 47, false, "f" );
	var_dump ( $output );
	$output = $SurveyParser->run ( $literal, 52, false, "f" );
	// $output=$SurveyParser->run($literal1,34,false,"x");
	// $output=$SurveyParser->run($literal1,3,true,"d");
	//   $output=$SurveyParser->run($literal1,25,false,"d");
	//$output=$SurveyParser->run($literal1,27,true,"f");
	//$output=$SurveyParser->run($literal1,34,false,"f");
	var_dump ( $output );
}

?>