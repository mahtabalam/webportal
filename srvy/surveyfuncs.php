<?php
function validateField($frmName, $fldName, $answer_format, $optionSize, $validate, $error){
	$jscript='document.getElementById("error'.$fldName.'").innerHTML="";';
	switch ($answer_format) {
		case 'input':
			$jscript.='if(document.'.$frmName.'.'.$fldName.'.value == ""){
							document.getElementById("error'.$fldName.'").innerHTML = "Your answer is required.";
							return false;
					};';	
			$validate_arr = explode(";", $validate);
			$error_arr = explode(";", $error);
			$i = 0;
			foreach ($validate_arr as $val){
				$err =  $error_arr[$i];
				$validate_type_arr = explode(":",$val);
				$validate_type = $validate_type_arr[0];
				
				if($validate_type=="email"){
					$jscript.='
							var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
								var address = document.'.$frmName.'.'.$fldName.'.value;
								if(reg.test(address) == false) {
						     	document.getElementById("error'.$fldName.'").innerHTML = "'.$err.'";
						      return false;
						   }';				
				}
				elseif ($validate_type == "date")
				{
					$jscript.='var txtDate = document.'.$frmName.'.'.$fldName.'.value;
					if (!isDate(txtDate)){  
						document.getElementById("error'.$fldName.'").innerHTML = "'.$err.'.(mm/dd/yyyy)";
						return false;
					}	
						function isDate(txtDate){  
								var objDate;  
								var mSeconds;
								if (txtDate.length != 10) return false;  
								var day   = txtDate.substring(3,5)  - 0;  
								var month = txtDate.substring(0,2)  - 1;
								var year  = txtDate.substring(6,10) - 0;  					   
								if (txtDate.substring(2,3) != "/") return false;  
								if (txtDate.substring(5,6) != "/") return false; 
								if (year < 1900 || year > 2000) return false;  
								mSeconds = (new Date(year, month, day)).getTime();  
								objDate = new Date();  
								objDate.setTime(mSeconds);  
								if (objDate.getFullYear() != year)  return false;  
								if (objDate.getMonth()    != month) return false;  
								if (objDate.getDate()     != day)   return false; 
								return true;  
					 		}'; 
				}
				elseif ($validate_type == "numberrange"){
					
					$jscript.='var x = document.'.$frmName.'.'.$fldName.'.value;
								if (isNaN(x)){
									document.getElementById("error'.$fldName.'").innerHTML = "'.$err.'";
									return false;
								};';
					
					$rangeStr = $validate_type_arr[1]; 
					$range_arr = explode(",",$rangeStr);
					$jscript.='var x = document.'.$frmName.'.'.$fldName.'.value;';
					$j =0;
					foreach ($range_arr as $value){
						$range = explode("-",$value);
						$min = $range[0]; 
						$max = $range[1]; 
						if($j ==0)
							$x = '((parseInt('.$min.')) <= (parseInt(x)) &&  (parseInt(x)) <= (parseInt('.$max.')))';
						else
							$x.= '|| ((parseInt('.$min.')) <= (parseInt(x)) &&  (parseInt(x)) <= (parseInt('.$max.')))';
						
						$j++;
					}
					$jscript.='
					if(!('.$x.')){
						document.getElementById("error'.$fldName.'").innerHTML = "'.$err.'";
						return false;
					};';
				}
				$i++;
			}
			
		break;				
		case 'radio':
			if($optionSize > 0){
				for($i=0; $i<$optionSize; $i++){
					if($i==0)
						$checkRadio.= '( document.'.$frmName.'.'.$fldName.'['.$i.'].checked == false )';
					else
						$checkRadio.= ' && ( document.'.$frmName.'.'.$fldName.'['.$i.'].checked == false )'; 			
				}
				$jscript.='if ('.$checkRadio.') { 
								document.getElementById("error'.$fldName.'").innerHTML = "Your answer is required."; 
								return false; 
						}';
			}
		break;
	case 'checkbox':
		$jscript.='var p=0;
		var a=document.'.$frmName.'.'.$fldName.';
		for(i=0;i<a.length;i++){
			if(a[i].checked == 1){
				p=1; break;
			}
		}
		if (p==0){
			document.getElementById("error'.$fldName.'").innerHTML = "Your answer is required.";
			return false;
		}';
	break;
	case 'select':
		$jscript.='if(document.'.$frmName.'.'.$fldName.'.value == ""){
			document.getElementById("error'.$fldName.'").innerHTML = "Your answer is required.";
			return false;
		}';
	break;
	case 'textarea':
		$jscript.='if(document.'.$frmName.'.'.$fldName.'.value == ""){
				document.getElementById("error'.$fldName.'").innerHTML = "Your answer is required.";
				return false;
		}';
			$validate_arr = explode(";", $validate);
			$error_arr = explode(";", $error);
			$i = 0;
			foreach ($validate_arr as $val){
				$err =  $error_arr[$i];
				$validate_type_arr = explode(":",$val);
				$validate_type = $validate_type_arr[0];
				$maxlimit = $validate_type_arr[1]; 
				if($validate_type=="maxlimit"){
					$jscript.='
							if (document.'.$frmName.'.'.$fldName.'.value.length > '.$maxlimit.') {
									document.getElementById("error'.$fldName.'").innerHTML = "'.$err.'";
								return false;
							}';
				}
				$i++;
			}
	break;	
	}
	$jscript.='';
	return $jscript;
}

function GenerateQuestion($id,$question,$numbering,$type,$options,$previousAns,$empty_check_required) {
	//echo $previousAns;
	$questStr  ="<tr><td colspan='2' class='text12'>$numbering) $question<br />";
	$questStr.="<div id='error$id' style='display:inline;color:#FF0000;'></div><br>";
	switch ($type) {
		case 'input':
			$questStr.= "<input type='text' class='textBox' name='$id' id='$id' value='$previousAns'/><br /><br /><br /></td></tr>"; 
		break;
		case 'radio':
			$option_arr = explode("|",$options);
			foreach ($option_arr as $val){
				if(strlen($val)==0)continue;
				if($val==$previousAns){
			//		echo "hmm";
					$questStr.= "<input type='radio' name='$id' value='$val' id='$id' checked='true'>&nbsp;$val<br />";
				}
				else{
					$questStr.= "<input type='radio' name='$id' value='$val' id='$id'>&nbsp;$val<br />";
				}
			}
			$questStr.="<br /><br /><br />";
		break;
		case 'checkbox':
			$option_arr = explode("|",$options);
			$pre_array=explode("|",$previousAns);
			foreach ($option_arr as $val){
				if(strlen($val)==0)continue;
				if(in_array($val,$pre_array)){
					$questStr.= '<input type="checkbox" name="'.$id.'[]" value="'.$val.'" id="'.$id.'" checked="true" >&nbsp;'.$val.'<br />';
				}else{
				$questStr.= "<input type='checkbox' name='".$id."[]' value='$val' id='$id' >&nbsp;$val<br />";
				}
			}
			$questStr.="<br /><br /><br />";
		break;
		case 'select':
			$option_arr = explode("|",$options);
			$questStr.="<select name='$id' id='$id'>";
			$flag=false;
			foreach ($option_arr as $value) {
				if(strlen($value)==0)continue;
				if($previousAns==$value){
					$questStr.= "<option value='$value'>$value</option>";
					@$flag=true;
				}
			}
			if($flag===false)$questStr.="<option value=''>Please select</option>";
			foreach ($option_arr as $val){
				if(strlen($val)==0)continue;
				if($previousAns!=$val){
				$questStr.= "<option value='$val'>$val</option>";
			}
			}
			$questStr.="</select><br /><br /><br />";
		break;
		case 'textarea':
			$questStr.= "<textarea name='$id' cols='25' rows='5' id='$id'>$previousAns</textarea><br /><br /><br /></td></tr>"; 
		break;
	} 
	
	return $questStr;
}

?>
