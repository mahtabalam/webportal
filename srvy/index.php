<?php
//ini_set('display_errors', 'on'); 
//error_reporting(E_ALL);
require_once ('../include/config.php');
require_once ('surveyfuncs.php');

require_once ('../include/function.php');
require_once ('../include/smartyBlockFunction.php');
require_once ('SurveyParser.php');
require_once (dirname(__FILE__).'/../adserver/DeviceInfo.php');
//$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);


class Survey {
	
	private $survey;
	private $uniqueId;
	private $conn;
	private $smarty;
	private $sid;
	private $sessionId;
	private $chid;
	private $placeholders;
  private $devtype;
  private $mobile_os_version;
  private $mobile_os;

  public function __construct() {
    global $conn;
    global $smarty;
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    $DeviceInfo=DeviceInfo::singleton();
    $DeviceInfo->processUserAgent($user_agent);
    $this->devtype = $DeviceInfo->getDeviceType();
    $this->mobile_os_version = $DeviceInfo->getDeviceOSVersion();
    $this->mobile_os = $DeviceInfo->getDeviceOS();

		$this->conn = &$conn;
		$this->smarty = &$smarty;
		if ($_REQUEST ['di']) {
			$uniqueId = $_REQUEST ['di'];
		} else {
			$uniqueId = $_SESSION ['uniqueID'];
		}
		$this->placeholders=&$_SESSION ['SURVEY']['placeholders'];
		$this->uniqueId = $uniqueId;
		$this->sessionId = session_id ();
	
	}
	
	//$question_id,$survey_question_id,$question,$ques_type,$options,$check_output,$current_index,$total_questions_in_this_random_set,$questions_showed_in_this_random_set
	private function fetchQuestions($sid, $current_index,$question_answer, $check_output, $total_questions_in_this_random_set, $questions_showed_in_this_random_set) {
		
		//first check if have anything to serve from random
		if (count ( $total_questions_in_this_random_set )) {
			$questions_left = array_diff ( $total_questions_in_this_random_set, $questions_showed_in_this_random_set );
			if (count ( $questions_left ) > 0) {
				$survey_question_id = $questions_left [rand ( 0, count ( $questions_left ) - 1 )];
				$data = $this->getQuestion ( $sid, $survey_question_id );
				$total_questions_in_this_random_set=implode(",",$total_questions_in_this_random_set);
				$questions_showed_in_this_random_set=implode(",",$questions_showed_in_this_random_set);
				return array ($data ["id"], $survey_question_id, $data ["question"], $data ["ans_format"], $data ["question_option"], false, $current_index, $total_questions_in_this_random_set, $questions_showed_in_this_random_set );
			}
		}
		$total_questions_in_this_random_set = null;
		$questions_showed_in_this_random_set = null;
		$SurveyParser = new SurveyParser ();
		$sql = "select map from survey_question_map where sid='$sid'";
		$rs = $this->conn->execute ( $sql );
		if ($rs && $rs->recordcount () > 0) {
			$literal = $rs->fields ['map'];
		}
		//array ('questions'=>$questions,'check_output' =>$check_output,'current_index'=>$current_index);
		$val = $SurveyParser->run ( $literal, $current_index, $check_output, $question_answer );
		//var_dump($val);exit;
		if(isset($val)==false)return null;
		
		//check for random values
		if(count($val['questions'])>1){
			$total_questions_in_this_random_set=implode(",",$val['questions']);
			 $questions_showed_in_this_random_set=null;
			 $survey_question_id = $val['questions'] [rand ( 0, count ( $val['questions'] ) - 1 )];
                $data = $this->getQuestion ( $sid, $survey_question_id );
                return array ($data ["id"], $survey_question_id, $data ["question"], $data ["ans_format"], $data ["question_option"], false, $val['current_index'], $total_questions_in_this_random_set, $questions_showed_in_this_random_set,$data ["placeholder"],$data['empty_check_required'] );
		}
		else if(count($val['questions'])==1){
			$total_questions_in_this_random_set=null;
			$questions_showed_in_this_random_set=null;
			$survey_question_id = $val['questions'][0];
             $data = $this->getQuestion ( $sid, $survey_question_id );
          return array ($data ["id"], $survey_question_id, $data ["question"], $data ["ans_format"], $data ["question_option"], $val['check_output'], $val['current_index'], $total_questions_in_this_random_set, $questions_showed_in_this_random_set, $data ["placeholder"],$data['empty_check_required']);
		}
		return null;
	}
	
	private function getQuestion($sid, $question_id) {
		$question_id = mysql_escape_string ( $question_id );
		$sid = mysql_escape_string ( $sid );
		$sql = "select id,sid,ans_format,question,question_option,placeholder,empty_check_required from survey_questions_advance where sid='$sid' and qid='$question_id'";
		$rs = $this->conn->execute ( $sql );
		return $rs->FetchRow ();
	}
	
	private function saveAnswer($question_id, $question_answer) {
    $questionId = mysql_escape_string ( $question_id );
		$answer = mysql_escape_string ( $question_answer );

	   if (is_array($question_answer)) {
               $answer = mysql_escape_string ( strip_tags(implode ( "|", $question_answer )));
                if (isset ( $answer ) == false) {
                    $answer = "";
                }
            }
		//   echo "$questionId..."."$answer";

                                          //  channel_id= '$this->chid',
		$sql = "insert into survey_data set 
                                            qid         =   '$questionId',
                                            unique_id       =   '$this->uniqueId',
                                            ip              =   '$_SESSION[ip]',
                                            session_id   = '$this->sessionId',
                                            answer      =   '$answer',
                                            channel_id= '$this->chid',
                                            devtype = '$this->devtype',
                                            ccode = '$_SESSION[country_code]',
                                            state_code = '$_SESSION[state_code]',
                                            carrier_type = '$_SESSION[carrier]',
                                            mobile_os_version = '$this->mobile_os_version',
                                            city = '$_SESSION[city]',
                                            mobile_os = '$this->mobile_os'
                                            ON DUPLICATE KEY UPDATE 
                                            answer = '$answer',
                                            devtype = '$this->devtype',
                                            ccode = '$_SESSION[country_code]',
                                            state_code = '$_SESSION[state_code]',
                                            carrier_type = '$_SESSION[carrier]',
                                            mobile_os_version = '$this->mobile_os_version',
                                            city = '$_SESSION[city]',
                                            mobile_os = '$this->mobile_os',
                                            tstamp=now()";
	//	echo $sql;exit;
		$rs = $this->conn->execute ( $sql );
	}
	
	private function getSurveyData($sid) {
		$sql = "select id,message,status  from survey where id='$sid'";
		$rs = $this->conn->execute ( $sql );
		if ($rs && $rs->recordcount () > 0) {
			$data=json_decode($rs->fields ['message'],true);
			return array ("message" => $data ['message'],"logo"=> $data ['logo'],"right_link"=> $data ['right_link'],"left_link"=> $data ['left_link'],"right_data"=> $data ['right_data'],"left_data"=> $data ['left_data'],"status" => $rs->fields ['status'] );
		}
		return null;
	
	}
	
	public function run($sid) {
		$sid=trim($sid);
		$survey = $this->getSurveyData ( $sid );
		$number=1;
		//check if survey is active
		if (isset ( $survey ) == false || $survey ['status'] == 0) {
			$this->smarty->assign ( "left_link", $survey ['left_link'] );
        $this->smarty->assign ( "right_link", $survey ['right_link'] );
        
        $this->smarty->assign ( "left_data", $survey ['left_data'] );
        $this->smarty->assign ( "right_data", $survey ['right_data'] );
        $this->smarty->assign ( "logo", $survey ['logo'] );
			//show notactivePage
			$this->smarty->assign ( "notActive", "1" );
			$this->smarty->display ( "srvy/final.tpl" );
			return;
		}
		
		$this->chid=getRequest ( 'chid', 0 ); 
		if (isset ( $_POST ['submit'] )) {
			$number=$_POST ['number'];
			$number++;
			$survey_question_id = $_POST ['survey_question_id'];
			$question_id= $_POST ['qid'];
			$question_answer = $_POST ["question_$survey_question_id"];
			$placeholder = $_POST ["placeholder"];
			if(isset($placeholder)){
			$this->placeholders[$placeholder]= strip_tags( $question_answer );
			}
			//var_dump($question_answer);exit;
			$this->saveAnswer ( $question_id, $question_answer );
			$total_questions_showed = $_POST ['total_questions_showed'];
			$total_questions_showed = explode ( ",", $total_questions_showed );
			$total_questions_showed [] = $survey_question_id;
			$questions_showed_in_this_random_set = $_POST ['questions_showed_in_this_random_set'];
			$questions_showed_in_this_random_set = explode ( ",", $questions_showed_in_this_random_set );
			$total_questions_in_this_random_set = $_POST ['total_questions_in_this_random_set'];
			$total_questions_in_this_random_set = explode ( ",", $total_questions_in_this_random_set );
			$current_index=$_POST ['current_index'];
			$check_output=$_POST ['check_output'];
			if (count ( $total_questions_in_this_random_set ) > 0) {
				$questions_showed_in_this_random_set [] = $survey_question_id;
			}
		} else {
			//show front page
			$current_index = 0;
			$check_output = false;
		  $question_answer=null;
		}
		
				//echo " $sid, $current_index, $question_answer,$check_output, $total_questions_in_this_random_set, $questions_showed_in_this_random_set";
		//exit;
		$output = $this->fetchQuestions ( $sid, $current_index, $question_answer,$check_output, $total_questions_in_this_random_set, $questions_showed_in_this_random_set );
		//var_dump($output); exit;
		if (isset ( $output ) == false) {
			unset($_SESSION ['SURVEY']);
			$this->smarty->assign ( "left_link", $survey ['left_link'] );
        $this->smarty->assign ( "right_link", $survey ['right_link'] );
        
        $this->smarty->assign ( "left_data", $survey ['left_data'] );
        $this->smarty->assign ( "right_data", $survey ['right_data'] );
        $this->smarty->assign ( "logo", $survey ['logo'] );
			$this->smarty->display ( "srvy/final.tpl" );
			return;
		}
		list ( $question_id, $survey_question_id, $question, $ques_type, $options, $check_output, $current_index, $total_questions_in_this_random_set, $questions_showed_in_this_random_set ,$placeholder,$empty_check_required) = $output;
		//array ('questions' => $questions, 'check_output' => $check_output, 'current_index' => $current_index )
		//check for placeholders and replace them.
		
		//var_dump($empty_check_required);exit;
		//var_dump($options); var_dump($this->placeholders); exit;
		foreach($this->placeholders as $key=>$value){
		$options=preg_replace("/::$key::/",$value,$options);
		$question=preg_replace("/::$key::/",$value,$question);
		}
		//remove placeholders if any in case it is normal flow
		$options=preg_replace("/::([\\d]+)::/","",$options);
		$question=preg_replace("/::([\\d]+)::/","",$question);
		$result = "";
		$count = 0;
		
		$optionSize = sizeof ( explode ( "|", $options ) );
		if (isset ( $optionSize ) == false)
			$optionSize = 0;
		$result .= GenerateQuestion ( "question_" . $survey_question_id, $question, $number, $ques_type, $options, null );
		if($empty_check_required==1){
		$jscript = '<script type="text/javascript">function validateForm(){';
		$jscript .= validateField ( 'frm1', "question_" . $survey_question_id, $ques_type, $optionSize, null, null );
		$jscript .= '}</script>';
		}
		$count ++;
		//echo $this->survey ['metaData']['message'];
		$this->smarty->assign ( "msg", $survey ['message'] );
		$this->smarty->assign ( "questionStr", $result );
		$this->smarty->assign ( "logo", $survey ['logo'] );
		$this->smarty->assign ( "left_link", $survey ['left_link'] );
		$this->smarty->assign ( "right_link", $survey ['right_link'] );
		
		$this->smarty->assign ( "left_data", $survey ['left_data'] );
        $this->smarty->assign ( "right_data", $survey ['right_data'] );
        
		if (isset ( $_POST ['submit'] )==false) {
			$this->smarty->assign ( "firstpage", 1 );
		}
		$this->smarty->assign ( "number", $number );
		$this->smarty->assign ( "survey_question_id", $survey_question_id );
		$this->smarty->assign ( "qid", $question_id );
		$this->smarty->assign ( "check_output", $check_output );
		$this->smarty->assign ( "current_index", $current_index );
		$this->smarty->assign ( "questions_showed_in_this_random_set", $questions_showed_in_this_random_set );
		$this->smarty->assign ( "total_questions_in_this_random_set", $total_questions_in_this_random_set );
		$this->smarty->assign ( "total_questions_showed", $total_questions_showed );
		if(isset($placeholder))
		$this->smarty->assign ( "placeholder", $placeholder );

		$this->smarty->assign ( 'jscript', $jscript );
		$this->smarty->assign ( "totalCount", $this->survey ['totalcount'] );
		$this->smarty->display ( "srvy/middle.tpl" );
	}
}

$var = new Survey ( );
//$_SESSION['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
//var_dump($_SERVER);
//exit;

//echo getRequest('sid',0);
$var->run (getRequest ( 'sid', 0 ) );

?>
