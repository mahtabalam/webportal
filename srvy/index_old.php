<?php
session_start();
include_once('../include/config.php');
include_once('serveyfuncs.php');
include_once('function.php');
require_once('../include/smartyBlockFunction.php');
$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);

try {
	if($_REQUEST['step']=="" && $_SESSION['step']!="")
		$_REQUEST['step']=$_SESSION['step'];
	elseif($_REQUEST['step']=="" && $_SESSION['step']=="")
		$_REQUEST['step']="first";
		
	//vdopia.com/survey/?sid=1&apikey=AX123&UUID=asdfaswerwasdf&cid=143
	
	if(isset($_SESSION['SURVEY'])) {
		$servey = $_SESSION["SURVEY"];
	}
	else 
	{
		$sid 		= getRequest('sid',1);
		$apikey 	=  getRequest('apikey',0);
		$UUID		=  getRequest('UUID',1);
		$cid		=  getRequest('cid',0);	
		
		$sql = "select id, apikey from channels where apikey='$apikey'";
		$rs = $conn->execute($sql);
		if($rs && $rs->recordcount()>0)
			$chid =  $rs->fields['id'];
		else
			$chid = "";
		
		$questions=fetchQuestions($sid);
		$survey['servey'] = $questions; 
		session_register("SURVEY");
		$_SESSION["SURVEY"]['question']=$questions;
		$_SESSION["SURVEY"]['sid']=$sid;
		$_SESSION["SURVEY"]['UUID']=$UUID;
		$_SESSION["SURVEY"]['cid']=$cid;
		$_SESSION["SURVEY"]['chid']=$chid;
	}
	switch ($_REQUEST['step']) {
		case 'first':
			if($_REQUEST["action_middle"]!="") {
				$_REQUEST['step'] = "middle";
				$_SESSION['step'] = "middle";
				header("Location: $config[baseurl]/survey/");
			}
			if($_SESSION['count']=="")
				$_SESSION['count'] = -1;
			$msg ="Welcome -- and thank you for participating in our survey today!<br><br>Click Continue to start the survey!";
		break;
		case 'middle':
			if($_REQUEST["action_middle"]!=""){
				
				$_REQUEST['step'] = "middle";
				$_SESSION['step'] = "middle";
				
				$_SESSION['count'] = $_SESSION['count']+1;
				if($_REQUEST['answer']!="")
				{
					$_SESSION["SURVEY"]['question'][$_SESSION[count]-1]['answer'] = $_REQUEST['answer']; 
					if($_SESSION['count']==count($servey['question']))
					{
						$_REQUEST['step'] = "final";
						$_SESSION['step'] = "final";
						
						$sid 		= $_SESSION["SURVEY"]['sid'];
						$unique_id 	= $_SESSION["SURVEY"]['UUID'];
						$cid 		= $_SESSION["SURVEY"]['cid'];
						$chid 		= $_SESSION["SURVEY"]['chid'];
						$sid = mysql_escape_string(strip_tags($_SESSION["SURVEY"]['sid']));
						$unique_id = mysql_escape_string(strip_tags($_SESSION["SURVEY"]['UUID']));
						$cid = mysql_escape_string(strip_tags($_SESSION["SURVEY"]['cid']));
						$chid = mysql_escape_string(strip_tags($_SESSION["SURVEY"]['chid']));
						
						foreach ($_SESSION["SURVEY"]['question'] as $value)
						{
							
							$sqid		=	mysql_escape_string(strip_tags($value['id']));
							$answer		=	mysql_escape_string(strip_tags($value['answer']));
							if($value['ans_format']=="checkbox")
								$answer = implode("|", $value['answer']);
							$sql = "insert into survey_data set 
											sid 		= 	'$sid',
											sqid 		= 	'$sqid',
											answer 		= 	'$answer',
											unique_id 	= 	'$unique_id',
											channel_id 	= 	'$chid',
											campaign_id = 	'$cid',
											tstamp		=	now()";
							
							$rs = $conn->execute($sql);
						}
					}
					header("Location: $config[baseurl]/survey/");
					exit();
				}
				else
				{
					$_SESSION['count'] = $_SESSION['count']-1;	
				}
			}
			elseif ($_REQUEST["action_back"])
			{
				$_SESSION['count'] = $_SESSION['count']-1;
				header("Location: $config[baseurl]/survey/");
					exit();
			}
			if($_SESSION['count'] < 0)
				$_SESSION['count']=0;
			
			//=====================
			if($servey['question'][$_SESSION[count]]['question_option']!="")
				$optionSize = sizeof(explode("|",$servey['question'][$_SESSION[count]]['question_option']));
			else
				$optionSize = 0; 
				
			$jscript = validateField('frm1','answer',$servey['question'][$_SESSION[count]]['ans_format'], $optionSize, $servey['question'][$_SESSION[count]]['validate'], $servey['question'][$_SESSION[count]]['error']);	
			
			//=================
			$questionStr = GenerateQuestion($servey['question'][$_SESSION[count]]['question'], $servey['question'][$_SESSION[count]]['ans_format'], $servey['question'][$_SESSION[count]]['question_option']); 
		
		break;
		case 'final':
			//	echo "<pre>";;
			//	print_r($servey);
		break;
	}
	
	$smarty->assign('jscript',$jscript);
	$progressBarr = ceil(($_SESSION['count']*100)/count($servey['question']));
	$_REQUEST['answer'] = $_SESSION["SURVEY"]['question'][$_SESSION[count]]['answer'];
	$smarty->assign("step_count", "$_SESSION[count]");
	$smarty->assign("progressBarr", "$progressBarr");
	$smarty->assign("msg", "$msg");
	$smarty->assign("questionStr", "$questionStr");
	$smarty->display("survey/$_REQUEST[step].tpl");
}
catch (Exception $e)
{
	echo "error=".$e->getMessage();	
}

?>
