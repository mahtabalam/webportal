<?php
/**
 * 
 * @author abhay
 * This class responsible to get records for various inventory reports.
 */

include_once ABSPATH."/common/config/TrackerSynonyms.inc";

class InventoryReport{
	private $conn;
	private $smarty;
	private $countryLists;
	
	/**
	 * Constructor for InventoryReport class
	 * @return unknown_type
	 */
	public function __construct(){
	  global $conn, $smarty, $countryLists, $reportConn;    
	 // $this->conn=$conn;
	 $this->conn=$reportConn;
	  $this->smarty=$smarty;
	  $this->countryLists=$countryLists;
	}
	
	
	/**
	 * These parameters help to filter data.
	 * @param $period
	 * @param $adtype
	 * @param $startdate
	 * @param $enddate
	 * @param $channel
	 * @param $region
	 * @return recordset
	 */
	public function report($period, $adtype, $startdate, $enddate, $publisher, $channel, $region){ 
		# Get data from various tables
		$appRows = $this->GetFromAggregateChannels($period, $adtype, $startdate, $enddate, $publisher, $channel, $region, TRACKERSYNONYMS::TRK_AVAILABLE_INVENTORY);
		$appUIRows = $this->GetFromAggregateChannels($period, $adtype, $startdate, $enddate, $publisher, $channel, $region, TRACKERSYNONYMS::TRK_UNFILLED_INVENTORY);
		$statRows = $this->GetFromDailyStats($period, $adtype, $startdate, $enddate, $publisher, $channel, $region);
		
		# Aggregate all data and return
		$appRows = $this->AggregateUnfilledRows($appRows, $appUIRows, $period);
		return $this->AggregateRows($appRows, $statRows, $period);
	}
	
	
	
	private function GetFromAggregateChannels($period, $adtype, $startdate, $enddate, $publisher, $channel, $region, $inventory){
		# build various condition or parameters on the basis of user requirment
		$regionSnip = $this->getRegionSnip('aggregate_channels', $region, $adtype, 'select', $inventory);
		$dateSnip = $this->GetDateFieldSnip('aggregate_channels', $period);
		$whereSnip = $this->getRegionSnip('aggregate_channels', $region, $adtype, 'where', $inventory);
		$publisherSnip = $this->GetPublisherSnip('publisher', $publisher);
		$channelSnip = $this->GetChannelSnip('aggregate_channels', $channel);
		$groupBySnip = $this->GetGroupBySnip('aggregate_channels', $period);
		
		if($regionSnip!=""){
			$regionSnip= " $regionSnip as region, aggregate_channels.adpos as adtype ";
		}
		
		if($dateSnip!=""){
			$dateSnip= ", $dateSnip ";
		}
		
	    if($region!="Overall"){
		  $countrySnip= ", aggregate_channels.ccode ";
		}else{
		  $countrySnip='';
		}
		
		if($groupBySnip!=''){
			$groupBySnip = " Group By $groupBySnip, publisher.id, aggregate_channels.channel_id, aggregate_channels.adpos $countrySnip ";
		}else{
			$groupBySnip = " Group By publisher.id, aggregate_channels.channel_id, aggregate_channels.adpos $countrySnip";
		}
		
		# Select query
		$sql="select $regionSnip $dateSnip, publisher.id as publisher_id, publisher.company_name, aggregate_channels.channel_id, channels.name as channel_name, sum(aggregate_channels.val) as total_call from aggregate_channels join channels on 
		channels.id=aggregate_channels.channel_id join publisher on publisher.id=channels.publisher_id 
		where aggregate_channels.measure='$inventory' and channels.verified='Verified' and channels.allow_sitetargeting='1' and channels.channel_type!='iphone' and aggregate_channels.date>='2011-01-15' and (aggregate_channels.date between '$startdate' and '$enddate') $whereSnip 
		$channelSnip $publisherSnip $groupBySnip;";
	    
		# Get rows and return
		return $this->run($sql, 'get');
	}
	
	private function GetFromDailyStats($period, $adtype, $startdate, $enddate, $publisher, $channel, $region){
		# build various condition or parameters on the basis of user requirment
		$regionSnip = $this->getRegionSnip('daily_stats', $region, $adtype, 'select', '');
		$dateSnip = $this->GetDateFieldSnip('daily_stats', $period);
		$whereSnip = $this->getRegionSnip('daily_stats', $region, $adtype, 'where', '');
		$adtypeSnip = $this->GetAdtypeSnip('daily_stats', $adtype);
		$publisherSnip = $this->GetPublisherSnip('publisher', $publisher);
		$channelSnip = $this->GetChannelSnip('daily_stats', $channel);
		$groupBySnip = $this->GetGroupBySnip('daily_stats', $period);
		
		if($regionSnip!=""){
			$regionSnip= " $regionSnip as region, adtype ";
		}
		
		if($dateSnip!=""){
		  $dateSnip= ", $dateSnip ";
		}
		
	    if($region!="Overall"){
		  $countrySnip= ", daily_stats.ccode ";
		}else{
		  $countrySnip='';
		}
		
		if($groupBySnip!=''){
		  ($adtype=='all' || $adtype=='')?$groupBySnip = " Group By $groupBySnip, publisher.id, daily_stats.channel_id, daily_stats.adtype $countrySnip":$groupBySnip = " Group By $groupBySnip, daily_stats.channel_id $countrySnip" ;
		}else{
		  $groupBySnip=" Group By publisher.id, daily_stats.channel_id, daily_stats.adtype $countrySnip";
		}
		
		# Select query
		$sql="select $regionSnip $dateSnip, publisher.id as publisher_id, publisher.company_name, daily_stats.channel_id, channels.name as channel_name, sum(daily_stats.impressions) as total_views, 
		sum(daily_stats.pubrevenue_advcurrency * daily_stats.ex_rate_advpub) as revenue 
		from daily_stats join channels on channels.id=daily_stats.channel_id join publisher on 
		publisher.id=channels.publisher_id where channels.verified='Verified' and channels.allow_sitetargeting='1' and channels.channel_type!='iphone' and daily_stats.date>='2011-01-15' and daily_stats.adtype<>'tracker' and (daily_stats.date between '$startdate' and 
		'$enddate') $whereSnip $channelSnip $adtypeSnip $publisherSnip $groupBySnip;";				
		
		# Get rows and return 
		return $this->run($sql, 'get');
	}
	
	/**
	 * 
	 * @param $appRows - data row of aggregate_channels table
	 * @param $appUIRows - data row of aggregate_channels table for Unfilled Inventory
	 * @param $period - daily/weekly/monthly/quarterly/yearly
	 * @return array
	 */
	private function AggregateUnfilledRows($appRows, $appUIRows, $period){
	  $set = array();
	  
	  if($period=="summary"){
	    $ctr=0;
		# merge app rows & app ui rows int set
		foreach($appRows as $appKey => $appVal){
		  $flag=0;
		  foreach($appUIRows as $appUIKey => $appUIVal){
		    if($appVal['adtype']==$appUIVal['adtype'] && $appVal['region']==$appUIVal['region'] && $appVal['publisher_id']==$appUIVal['publisher_id'] && $appVal['channel_id']==$appUIVal['channel_id']){
			  $set[$ctr]['region']=$appVal['region'];
			  $set[$ctr]['adtype']=$appVal['adtype'];
			  $set[$ctr]['publisher_id']=$appVal['publisher_id'];
			  $set[$ctr]['company_name']=$appVal['company_name'];
			  $set[$ctr]['channel_id']=$appVal['channel_id'];
			  $set[$ctr]['channel_name']=$appVal['channel_name'];
			  $set[$ctr]['total_call']=$appVal['total_call'];
			  $set[$ctr]['total_ui']=$appUIVal['total_call'];
			  $appUIRows[$appUIKey]['processed']=1;
			  $flag=1;
              $ctr++;
		    }
		  }
		  if($flag==0){
            $set[$ctr]['region']=$appVal['region'];
            $set[$ctr]['adtype']=$appVal['adtype'];
            $set[$ctr]['publisher_id']=$appVal['publisher_id'];
    		$set[$ctr]['company_name']=$appVal['company_name'];
    		$set[$ctr]['channel_id']=$appVal['channel_id'];
    		$set[$ctr]['channel_name']=$appVal['channel_name'];
            $set[$ctr]['total_call']=$appVal['total_call'];
            $set[$ctr]['total_ui']=0;
            $ctr++;
          }
		}
		
	    foreach($appUIRows as $appUIKey => $appUIVal){
		  if($appUIVal['processed']!=1 && $appUIVal['adtype']!=""){
			$set[$ctr]['region']=$appUIVal['region'];
			$set[$ctr]['adtype']=$appUIVal['adtype'];
			$set[$ctr]['publisher_id']=$appVal['publisher_id'];
    		$set[$ctr]['company_name']=$appVal['company_name'];
    		$set[$ctr]['channel_id']=$appVal['channel_id'];
    		$set[$ctr]['channel_name']=$appVal['channel_name'];
			$set[$ctr]['total_call']=0;
			$set[$ctr]['total_ui']=$appUIVal['total_call'];
			$ctr++;
		  }
		}
	  }else{
		$ctr=0;
		# merge app rows & app ui rows into set
		foreach($appRows as $appKey => $appVal){
		  $flag=0;
		  foreach($appUIRows as $appUIKey => $appUIVal){
			if($appVal['date']==$appUIVal['date'] && $appVal['adtype']==$appUIVal['adtype'] && $appVal['region']==$appUIVal['region'] && $appVal['publisher_id']==$appUIVal['publisher_id'] && $appVal['channel_id']==$appUIVal['channel_id']){
			  $set[$ctr]['region']=$appVal['region'];
			  $set[$ctr]['adtype']=$appVal['adtype'];
			  $set[$ctr]['publisher_id']=$appVal['publisher_id'];
			  $set[$ctr]['company_name']=$appVal['company_name'];
    		  $set[$ctr]['channel_id']=$appVal['channel_id'];
    		  $set[$ctr]['channel_name']=$appVal['channel_name'];
              $set[$ctr]['date']=$appVal['date'];
              $set[$ctr]['total_call']=$appVal['total_call'];
              $set[$ctr]['total_ui']=$appUIVal['total_call'];
              $appUIRows[$appUIKey]['processed']=1;
              $flag=1;
              $ctr++;
			}
		  }
          if($flag==0){
            $set[$ctr]['region']=$appVal['region'];
            $set[$ctr]['adtype']=$appVal['adtype'];
            $set[$ctr]['publisher_id']=$appVal['publisher_id'];
			$set[$ctr]['company_name']=$appVal['company_name'];
    		$set[$ctr]['channel_id']=$appVal['channel_id'];
    		$set[$ctr]['channel_name']=$appVal['channel_name'];
            $set[$ctr]['date']=$appVal['date'];
            $set[$ctr]['total_call']=$appVal['total_call'];
            $set[$ctr]['total_ui']=0;
            $ctr++;
          }
		}

		foreach($appUIRows as $appUIKey => $appUIVal){
		  if($appUIVal['processed']!=1){
			$set[$ctr]['region']=$appUIVal['region'];
			$set[$ctr]['adtype']=$appUIVal['adtype'];
			$set[$ctr]['publisher_id']=$appVal['publisher_id'];
			$set[$ctr]['company_name']=$appVal['company_name'];
    		$set[$ctr]['channel_id']=$appVal['channel_id'];
    		$set[$ctr]['channel_name']=$appVal['channel_name'];
			$set[$ctr]['date']=$appUIVal['date'];
			$set[$ctr]['total_call']=0;
			$set[$ctr]['total_ui']=$appUIVal['total_call'];
			$ctr++;
		  }
		}
	  }
	  return $set;
	}
	
	/**
	 * 
	 * @param $appRows - data row of aggregate_channels table
	 * @param $statRows - data row of daily_stats table
	 * @param $period - daily/weekly/monthly/quarterly/yearly
	 * @return array
	 */
	private function AggregateRows($appRows, $statRows, $period){
		# check if period equal summary
		if($period=="summary"){
		  $ctr=0;		  # merge stat rows with app rows into fSet
  		  foreach($appRows as $appKey => $appVal){
  		    $flag=0;
  		    foreach($statRows as $statKey => $statVal){
  		      if($appVal['adtype']==$statVal['adtype'] && $appVal['region']==$statVal['region'] && $appVal['publisher_id']==$statVal['publisher_id'] && $appVal['channel_id']==$statVal['channel_id']){
  		        if($appVal['region']!='Overall'){
  		          $fSet[$ctr]['region']=$this->countryLists[$appVal['region']];
  		        }else{
  			      $fSet[$ctr]['region']=$appVal['region'];
  		        }
  			    $fSet[$ctr]['adtype']=$appVal['adtype'];
  			    $fSet[$ctr]['publisher_id']=$appVal['publisher_id'];
  			    $fSet[$ctr]['channel_id']=$appVal['channel_id'];
  			    $fSet[$ctr]['company_name']=$appVal['company_name'];
  			    $fSet[$ctr]['channel_name']=$appVal['channel_name'];
  			    $fSet[$ctr]['total_call']=$appVal['total_call'];
  			    $fSet[$ctr]['total_ui']=$appVal['total_ui'];
  			    $fSet[$ctr]['total_views']=$statVal['total_views']?$statVal['total_views']:0;
  			    $fSet[$ctr]['total_ad_call']=$fSet[$ctr]['total_ui']+$fSet[$ctr]['total_views'];
      			$fSet[$ctr]['total_call_drops']=$fSet[$ctr]['total_call']-$fSet[$ctr]['total_ad_call'];
  			    
//  			    $fSet[$ctr]['ei']=round(((($appVal['total_call']-$appVal['total_ui'])/$fSet[$ctr]['total_views'])*$appVal['total_call']),2);
//  			    $fSet[$ctr]['ut']=round(($fSet[$ctr]['total_views']/$fSet[$ctr]['ei'])*100,2);
//  			    $fSet[$appKey]['efficiency']=round(((($appVal['total_call']-$appVal['total_ui'])/$fSet[$ctr]['total_views'])*100),2);
  			    
  			    $fSet[$ctr]['ut']=round((($appVal['total_call']-$appVal['total_ui'])/$appVal['total_call'])*100,2);
      			$fSet[$ctr]['ei']=round((($statVal['total_views']/($appVal['total_call']-$appVal['total_ui']))*$appVal['total_call']),0);
      			$fSet[$appKey]['efficiency']=round((($statVal['total_views']/($appVal['total_call']-$appVal['total_ui']))*100),2);
  			    
  			    $fSet[$ctr]['revenue']=$statVal['revenue'];
  			    $fSet[$ctr]['eCPM']=round(($statVal['revenue'])*1000/(($statVal['total_views'])),2);
  			    $flag=1;
  			    $ctr++;
  		      }
  			}

  			if($flag==0){
  			  if($appVal['region']!='Overall'){
  		        $fSet[$ctr]['region']=$this->countryLists[$appVal['region']];
  		      }else{
  			    $fSet[$ctr]['region']=$appVal['region'];
  		      }
			  $fSet[$ctr]['adtype']=$appVal['adtype'];
			  $fSet[$ctr]['publisher_id']=$appVal['publisher_id'];
  			  $fSet[$ctr]['channel_id']=$appVal['channel_id'];
  			  $fSet[$ctr]['company_name']=$appVal['company_name'];
  			  $fSet[$ctr]['channel_name']=$appVal['channel_name'];
			  $fSet[$ctr]['total_call']=$appVal['total_call'];
			  $fSet[$ctr]['total_ui']=$appVal['total_ui'];
			  $fSet[$ctr]['total_views']=0;
			  $fSet[$ctr]['total_ad_call']=$fSet[$ctr]['total_ui']+$fSet[$ctr]['total_views'];
      		  $fSet[$ctr]['total_call_drops']=$fSet[$ctr]['total_call']-$fSet[$ctr]['total_ad_call'];
			  
//			  $fSet[$ctr]['ei']=round(((($appVal['total_call']-$appVal['total_ui'])/$fSet[$ctr]['total_views'])*$appVal['total_call']),2);
//  			  $fSet[$ctr]['ut']=round(($fSet[$ctr]['total_views']/$fSet[$ctr]['ei'])*100,2);
//  			  $fSet[$appKey]['efficiency']=round(((($appVal['total_call']-$appVal['total_ui'])/$fSet[$ctr]['total_views'])*100),2);
  			    
			  $fSet[$ctr]['ei']=round((($fSet[$ctr]['total_views']/($appVal['total_call']-$appVal['total_ui']))*$appVal['total_call']),0);
			  $fSet[$ctr]['ut']=round((($appVal['total_call']-$appVal['total_ui'])/$appVal['total_call'])*100,2); 
			  $fSet[$appKey]['efficiency']=round((($fSet[$ctr]['total_views']/($appVal['total_call']-$appVal['total_ui']))*100),2);
			  
			  $fSet[$ctr]['revenue']=0;
			  $fSet[$ctr]['eCPM']=0;
			  $ctr++;
		    }
  		  }
		}else{
          $ctr=0;
          foreach($appRows as $appKey => $appVal){
          	$flag=0;
          	foreach($statRows as $statKey => $statVal){
      		  if($appVal['date']==$statVal['date'] && $appVal['adtype']==$statVal['adtype'] && $appVal['region']==$statVal['region'] && $appVal['publisher_id']==$statVal['publisher_id'] && $appVal['channel_id']==$statVal['channel_id']){
      		  if($appVal['region']!='Overall'){
  		          $fSet[$ctr]['region']=$this->countryLists[$appVal['region']];
  		        }else{
  			      $fSet[$ctr]['region']=$appVal['region'];
  		        }
      			$fSet[$ctr]['date']=$appVal['date'];
      			$fSet[$ctr]['adtype']=$appVal['adtype'];
      			$fSet[$ctr]['publisher_id']=$appVal['publisher_id'];
      	        $fSet[$ctr]['channel_id']=$appVal['channel_id'];
      	        $fSet[$ctr]['company_name']=$appVal['company_name'];
  			    $fSet[$ctr]['channel_name']=$appVal['channel_name'];
      			$fSet[$ctr]['total_call']=$appVal['total_call'];
      			$fSet[$ctr]['total_ui']=$appVal['total_ui'];
      			$fSet[$ctr]['total_views']=$statVal['total_views']?$statVal['total_views']:0;
      			$fSet[$ctr]['total_ad_call']=$fSet[$ctr]['total_ui']+$fSet[$ctr]['total_views'];
      			$fSet[$ctr]['total_call_drops']=$fSet[$ctr]['total_call']-$fSet[$ctr]['total_ad_call'];
      			
//      			$fSet[$ctr]['ei']=round(((($appVal['total_call']-$appVal['total_ui'])/$fSet[$ctr]['total_views'])*$appVal['total_call']),2);
//  			    $fSet[$ctr]['ut']=round(($fSet[$ctr]['total_views']/$fSet[$ctr]['ei'])*100,2);
//  			    $fSet[$appKey]['efficiency']=round(((($appVal['total_call']-$appVal['total_ui'])/$fSet[$ctr]['total_views'])*100),2);
  			    
      			$fSet[$ctr]['ut']=round((($appVal['total_call']-$appVal['total_ui'])/$appVal['total_call'])*100,2);
      			$fSet[$ctr]['ei']=round((($statVal['total_views']/($appVal['total_call']-$appVal['total_ui']))*$appVal['total_call']),0);
      			$fSet[$appKey]['efficiency']=round((($statVal['total_views']/($appVal['total_call']-$appVal['total_ui']))*100),2);
      			
      			$fSet[$ctr]['revenue']=$statVal['revenue'];
      			$fSet[$ctr]['eCPM']=round(($statVal['revenue'])*1000/(($statVal['total_views'])),2);
      			$flag=1;
      			$ctr++;
      		  }
          	}
          	if($flag==0){
          	  if($appVal['region']!='Overall'){
  		        $fSet[$ctr]['region']=$this->countryLists[$appVal['region']];
  		      }else{
  			    $fSet[$ctr]['region']=$appVal['region'];
  		      }
          	  $fSet[$ctr]['date']=$appVal['date'];
          	  $fSet[$ctr]['adtype']=$appVal['adtype'];
          	  $fSet[$ctr]['publisher_id']=$appVal['publisher_id'];
          	  $fSet[$ctr]['channel_id']=$appVal['channel_id'];
          	  $fSet[$ctr]['company_name']=$appVal['company_name'];
  			  $fSet[$ctr]['channel_name']=$appVal['channel_name'];
          	  $fSet[$ctr]['total_call']=$appVal['total_call'];
          	  $fSet[$ctr]['total_ui']=$appVal['total_ui'];
          	  $fSet[$ctr]['total_views']=0;
          	  $fSet[$ctr]['total_ad_call']=$fSet[$ctr]['total_ui']+$fSet[$ctr]['total_views'];
      			$fSet[$ctr]['total_call_drops']=$fSet[$ctr]['total_call']-$fSet[$ctr]['total_ad_call'];
          	  
//          	  $fSet[$ctr]['ei']=round(((($appVal['total_call']-$appVal['total_ui'])/$fSet[$ctr]['total_views'])*$appVal['total_call']),2);
//  			  $fSet[$ctr]['ut']=round(($fSet[$ctr]['total_views']/$fSet[$ctr]['ei'])*100,2);
//  			  $fSet[$appKey]['efficiency']=round(((($appVal['total_call']-$appVal['total_ui'])/$fSet[$ctr]['total_views'])*100),2);
  			  
          	  $fSet[$ctr]['ut']=round((($appVal['total_call']-$appVal['total_ui'])/$appVal['total_call'])*100,2);
          	  $fSet[$ctr]['ei']=round((($fSet[$ctr]['total_views']/($appVal['total_call']-$appVal['total_ui']))*$appVal['total_call']),0);
          	  $fSet[$appKey]['efficiency']=round((($fSet[$ctr]['total_views']/($appVal['total_call']-$appVal['total_ui']))*100),2);
          	  
          	  $fSet[$ctr]['revenue']=0;
          	  $fSet[$ctr]['eCPM']=0;
          	  $ctr++;
          	}
          }
		}
		# return final set
		return $fSet;
	}
	
	/**
	 * 
	 * @param $table - table name
	 * @param $period
	 * @return String
	 */
	private function GetDateFieldSnip($table,$period){
		if(!isset($table) && $table==""){
			throw new Exception("Invalid table.");
		}
		
		if(!isset($period) && $period==""){
			throw new Exception("Invalid period.");
		}
		
		switch($period){
			case 'summary':
				$sqlSnip="";
				break;
			case 'daily':
				$sqlSnip=" $table.date ";
				break;
			case 'weekly':
				$sqlSnip=" concat('Week','-',week($table.date)+1) as date ";
				break;
			case 'dayofweek':
				$sqlSnip=" date_format($table.date, '%a') as date ";
				break;
			case 'monthly':
				$sqlSnip=" concat_ws(' ', monthname($table.date), year($table.date)) as date ";
				break;
			case 'quarterly':
				$sqlSnip=" concat_ws(' ', concat('Q',quarter($table.date)), year($table.date)) as date ";
				break;
			case 'yearly':
				$sqlSnip=" year($table.date) as date ";
				break;						
		}
		
		return $sqlSnip;
	}
	
	/**
	 * 
	 * @param $table - table name
	 * @param $period
	 * @return String
	 */
	private function GetOrderBySnip($table,$period){
		if(!isset($table) && $table==""){
			throw new Exception("Invalid table.");
		}
		
		if(!isset($period) && $period==""){
			throw new Exception("Invalid period.");
		}
		
		switch($period){
			case 'summary':
				$sqlSnip="";
				break;
			case 'daily':
				$sqlSnip=" Order By $table.date ";
				break;
			case 'weekly':
				$sqlSnip=" Order By week($table.date) ";
				break;
			case 'dayofweek':
				$sqlSnip=" Order By dayofweek($table.date) ";
				break;
			case 'monthly':
				$sqlSnip=" Order By month($table.date) ";
				break;
			case 'quarterly':
				$sqlSnip=" Order By quarter($table.date) ";
				break;
			case 'yearly':
				$sqlSnip=" Order By year($table.date) ";
				break;						
		}
		
		return $sqlSnip;
	}
	
	/**
	 * 
	 * @param $table - table name
	 * @param $period
	 * @return String
	 */
	private function GetGroupBySnip($table,$period){
		if(!isset($table) && $table==""){
			throw new Exception("Invalid table.");
		}
		
		if(!isset($period) && $period==""){
			throw new Exception("Invalid period.");
		}
		
		switch($period){
			case 'summary':
				$sqlSnip="";
				break;
			case 'daily':
				$sqlSnip=" day($table.date),month($table.date),year($table.date) ";
				break;
			case 'weekly':
				$sqlSnip=" week($table.date), year($table.date) ";
				break;
			case 'dayofweek':
				$sqlSnip=" dayofweek($table.date), year($table.date) ";
				break;
			case 'monthly':
				$sqlSnip=" month($table.date),year($table.date) ";
				break;
			case 'quarterly':
				$sqlSnip=" quarter($table.date),year($table.date) ";
				break;
			case 'yearly':
				$sqlSnip=" year($table.date) ";
				break;						
		}
		
		return $sqlSnip;
	}
	
	/**
	 * 
	 * @param $table - table name
	 * @param $publisher
	 * @return String
	 */
	private function GetPublisherSnip($table, $publisher){
	  if($publisher=='all' || $publisher=='') return $sqlSnip=""; else return $sqlSnip=" and $table.id='$publisher'";
	}
	
	/**
	 * 
	 * @param $table - table name
	 * @param $channel
	 * @return String
	 */
	private function GetChannelSnip($table, $channel){
	  if($channel=='all' || $channel=='') return $sqlSnip=""; else return $sqlSnip=" and $table.channel_id='$channel'";
	}
	
	/**
	 * 
	 * @param $table - table name
	 * @param $adtype
	 * @return String
	 */
	private function GetAdtypeSnip($table, $adtype){
	  if($adtype=='all' || $adtype=='') return $sqlSnip=""; else return $sqlSnip=" and $table.adtype='$adtype'";
	}
	
	/**
	 * 
	 * @param $region
	 * @param $adtype
	 * @param $inventory
	 * @return String
	 */
	private function GetCountrySnip($region, $table){
	  $country = explode(',', $region);
	  foreach($country as $name){
	    $countrySnip.="$table.ccode='$name' or ";
	  }
	  
	  if($countrySnip!=""){
	    $countrySnip = "and (".trim($countrySnip, ' or ').")";
	  }else{
	    $countrySnip="";
	  }
	  return $countrySnip;
	}
	
	/**
	 * 
	 * @param $table - table name
	 * @param $region
	 * @param $action
	 * @return String
	 */
	private function GetRegionSnip($table, $region, $adtype, $action, $inventory){
	  $countrySnip = $this->GetCountrySnip($region, $table);
	  if($action=="select"){
			if($region=='Overall'){
			  $sqlSnip="'Overall'";	
			}else{
			  $sqlSnip="$table.ccode";
			}
		}elseif($action=="where"){
			if($region=='Overall' ){
				if($table=="aggregate_channels"){
				  if($adtype=='all'){
					$sqlSnip=" and ($table.adpos='preroll' or $table.adpos='midroll' or $table.adpos='postroll' or $table.adpos='overlay' or $table.adpos='branded' or $table.adpos='tracker') ";
				  }else{
				    $sqlSnip=" and $table.adpos='$adtype'";
				  }
				}else{
					$sqlSnip="";
				} 
			}else{
				if($table=='aggregate_channels'){
				  if($adtype=="all"){
				    $sqlSnip=" and ($table.adpos='preroll' or $table.adpos='midroll' or $table.adpos='postroll' or $table.adpos='overlay' or $table.adpos='branded' or $table.adpos='tracker') $countrySnip ";
				  }else{
				    $sqlSnip=" and $table.adpos='$adtype' $countrySnip";
				  }
				}else{
				  $sqlSnip=" $countrySnip ";
				}
			}
		}
		return $sqlSnip;
	}
	
	/**
	 * 
	 * @param $sql - sql query that will be executed
	 * @param $action - action explains how to execute query (select/insert/update) etc.
	 * @return record set
	 */
	private function run($sql, $action){
		switch($action){
			case 'get':
				$rs=$this->conn->execute($sql);
				if(!$rs) 
					throw new Exception("Get operation failed.");
				else
					return $rs->getrows();
				break;	
			default:
				throw new Exception("Invalid argument.");
				break;		
		}
	}
}

?>
