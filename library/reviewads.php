<?php

include_once('include/function.php');
include_once('include/config.php');
require_once('include/smartyBlockFunction.php');
include_once('include/datefuncs.php');

$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);



switch ($_REQUEST['sub']) {
	case 'preview':
		if($_REQUEST['action_review']=='Disapprove'){
			$campaign_id=requestParams('campaign_id');
			$channel_id=requestParams('channel_id');
			$reason_code=requestParams('reason_code');

			if(!empty($campaign_id) && !empty($channel_id)){
				$sql = "insert into blocked_campaigns set channel_id=$channel_id, reason_code='$reason_code', campaign_id=$campaign_id";
				$rs = $conn->execute($sql);
				$rows =  $conn->Affected_Rows();
				if($rows>0){
					$error = "Campaign disapproved";
						/*	if($config['sandbox']!='true'){*/
										$sql1 = "select email, fname, lname, campaign.name from advertiser, campaign where campaign.id = '$campaign_id' and campaign.advertiser_id = advertiser.id";
										$rs1 = $conn->execute($sql1);

										$sql2 = "select name,url from channels where id = '$channel_id'";
										$rs2 = $conn->execute($sql2);
										if($rs2 && $rs2->recordcount()>0){
											$smarty->assign("channel_name", $rs2->fields['name']);
											$smarty->assign("channel_url", $rs2->fields['url']);
										}
										if($rs1 && $rs1->recordcount()>0){
										    $email = $rs1->fields['email'];
											$subj = "Vdopia - Campaign Disapproved";
											$smarty->assign("receiver_fname", $rs1->fields['fname']);
											$smarty->assign("reason_code", $reason_code);
											$smarty->assign("campaign_name", $rs1->fields['name']);

											$smarty->assign("email", $rs1->fields['email']);
											$body = $smarty->fetch("emails/campaigndisapproved.tpl");

											mail($email,$subj,$body,$config['support_headers']);
										}
							/*}*/
				}else{
					$error = "Campaign could not be disapproved";
				}
				$smarty->assign("error",$error);
				//doForward("$config[baseurl]/$_REQUEST[page]");
			}
			//exit();
		}
		elseif ($_REQUEST['action_review']=='Approve'){
			$campaign_id=requestParams('campaign_id');
			$channel_id=requestParams('channel_id');

			if(!empty($campaign_id) && !empty($channel_id)){
				$sql = "delete from blocked_campaigns where channel_id=$channel_id and campaign_id=$campaign_id";
				error_log($sql);
				$rs = $conn->execute($sql);
				$rows = $conn->Affected_Rows();
				if($rows>0){
					$error = "Campaign Approved";
                    /*		if($config['sandbox']!='true'){*/
										$sql1 = "select email, fname, lname, campaign.name from advertiser, campaign where campaign.id = '$campaign_id' and campaign.advertiser_id = advertiser.id";
										$rs1 = $conn->execute($sql1);

									    $sql2 = "select name,url from channels where id = '$channel_id'";
										$rs2 = $conn->execute($sql2);
										if($rs2 && $rs2->recordcount()>0){
											$smarty->assign("channel_name", $rs2->fields['name']);
											$smarty->assign("channel_url", $rs2->fields['url']);
										}
										if($rs1 && $rs1->recordcount()>0){
										    $email = $rs1->fields['email'];
											$subj = "Vdopia - Campaign Approved";
											$smarty->assign("receiver_fname", $rs1->fields['fname']);

											$smarty->assign("campaign_name", $rs1->fields['name']);

											$smarty->assign("email", $rs1->fields['email']);
											$body = $smarty->fetch("emails/campaignapproved.tpl");

											mail($email,$subj,$body,$config['support_headers']);
										}
							/*}*/
				}else{
					$error = "Campaign could not be Approved";
				}
				$smarty->assign("error",$error);
				//doForward("$config[baseurl]/$_REQUEST[page]");
			}
		//exit();
		}
		else
		{
			$smarty->assign("reason_codes",$reason_codes);
			if($_REQUEST['campaign_id']!=null){
				$campaign_id = requestParams("campaign_id");
				$sql = "select campaign.name, campaign.video_choice from campaign where id = '$campaign_id'";
				$rs = $conn->execute($sql);
				if($rs && $rs->recordcount()>0){
					$campaign['name']=$rs->fields['name'];
					$campaign['video_choice']=$rs->fields['video_choice'];

				}
				$channel_id = requestParams("channel_id");
				$sql = "select reason_code from blocked_campaigns where campaign_id = $campaign_id and channel_id = $channel_id";
				$rs = $conn->execute($sql);
				if($rs && $rs->recordcount()>0){
					$reason_code = $rs->fields['reason_code'];
					$smarty->assign('reason_code',$reason_code);
				}


				$sql = "select * from  campaign_members left join ads  on campaign_members.ad_id = ads.id where campaign_members.cid = '$campaign_id'";

				$rs = $conn->execute($sql);
				$i=0;

				$textads=array();
				$bannerads=array();
				$videoads=array();
				$brandedads=array();
				while(!$rs->EOF) {
					$campaign['type']=$rs->fields['ad_format']."advertisement";
					if($rs->fields['ad_format']=='video')
					{
						$videoad['id']=$rs->fields['id'];
						$videoad['uri']=substr($rs->fields['playlist_ad_ref'],0,strlen($rs->fields['playlist_ad_ref'])-4);
						$videoad['name']=$rs->fields['branded_img_right_ref_imgname'];
						$videoad['desturl']=$rs->fields['destination_url'];
						$videoads[]=$videoad;
					} elseif ($rs->fields['ad_format']=='branded') {
						$brandedad['id']=$rs->fields['id'];
						$brandedad['name']=$rs->fields['branded_img_right_ref_imgname'];
						$brandedad['desturl']=$rs->fields['destination_url'];
						$brandedad['skinreff']=$rs->fields['branded_color'];
						$brandedad['trackurl']=$rs->fields['tracker_url'];
						$brandedad['imgurl']=$rs->fields['branded_img_top_ref_txtitle'];
						$campaign['type']="brandedplayer";
						$brandedads[]=$brandedad;
					} elseif ($rs->fields['branded_img_top_ref_txtitle']!="" ) {
						$textad['id']=$rs->fields['id'];
						$textad['title']=$rs->fields['branded_img_top_ref_txtitle'];
						$textad['body']=$rs->fields['branded_img_bot_ref_txbody'];
						$textad['desturl']=$rs->fields['destination_url'];
						$textads[]=$textad;
						$campaign['subtype']="textadvertisement";
					} else {
						$bannerad['id']=$rs->fields['id'];;
						$bannerad['imgname']=$rs->fields['branded_img_right_ref_imgname'];;
						$bannerad['imgurl']=$rs->fields['playlist_ad_ref'];;
						$bannerad['desturl']=$rs->fields['destination_url'];
						$campaign['subtype']="banneradvertisement";
						$bannerads[]=$bannerad;
					}
					$rs->movenext();
				}
				$campaign['textads']=$textads;
				$campaign['bannerads']=$bannerads;
				$campaign['videoads']=$videoads;
				$campaign['brandedads']=$brandedads;
				$smarty->assign('readOnly',"1");
				$smarty->assign('viewonly',"1");
				$smarty->assign('campaign',$campaign);
			}
		}
		break;
	case 'review':

		$reviewoptions = array(
		'all' => "Approved/Rejected Ads",
		'approvedads'=> "Approved Ads",
		'rejectedads'=> "Rejected Ads");
		
		$adtype= array(
		'all' => "All Ad Types",
		'video'=> "Video Ads",
		'ytpreroll' => 'Youtube Video Ads',
		'overlay'=> "Overlay Ads",
		'branded'=> "Branded Player Ads",'tracker'=>"Tracker Ads(Banner,Videobanner & trackers)");
		
		// Provide All Site names
		$sql = "select name, id from channels where publisher_id = '$_SESSION[PUB_ID]' and channel_type='PC'";
		$rs = $conn->execute($sql);
		if($rs && $rs->recordcount()>0){
			$siteoptions = $rs->getrows();
		}

		$smarty->assign("reviewoptions",$reviewoptions);
		$smarty->assign("siteoptions",$siteoptions);
		$smarty->assign("adtype",$adtype);
		$sitestring="";
		
		if( $_SESSION['ADMIN_PRIVILEGES']=='superadmin' || $_SESSION['ADMIN_PRIVILEGES']=='sales'){
			if($_REQUEST['action_block']!=null || $_REQUEST['action_block1']!=null){
				$cids = $_REQUEST["cids"];
				foreach($cids as $cid){
					$cid = explode('-', $cid);
					$sql = "insert into blocked_campaigns set channel_id=".$cid[1].", reason_code='Other', campaign_id=".$cid[0];
					$rs = $conn->execute($sql);
					if($rs){
						$message = "Selected campaigns blocked succefully.";
					}else{
						$message = "Sorry, due to technical glitch your operation has been failed. Please try again after some time...";
						break;
					}
				}
				doForward($config['baseurl']."/pub.php?page=reviewads&message=".$message);
			}
			
			if($_REQUEST['action_approve']!=null || $_REQUEST['action_approve1']!=null){
			$cids = $_REQUEST["cids"];
				foreach($cids as $cid){
					$cid = explode('-', $cid);
					$sql = "delete from blocked_campaigns where channel_id=".$cid[1]." and campaign_id=".$cid[0];
					$rs = $conn->execute($sql);
					if($rs){
						$message = "Selected campaigns aprroved succefully.";
					}else{
						$message = "Sorry, due to technical glitch your operation has been failed. Please try again after some time...";
						break;
					}
				}
				doForward($config['baseurl']."/pub.php?page=reviewads&message=".$message);
			}
		}

		if($_REQUEST['action_getadstoreview']!=null){
			$site = requestParams("site");
			if($site == ""){
				$site = "all";
			}
			$_REQUEST['site']=$site;
			if($site != "all"){
				$sitestring = "and channels.id = '$site'";
			}


			$reviewoptions = requestParams("reviewoptions");
			if($reviewoptions == ""){
				$reviewoptions = "all";
			}
			$_REQUEST['reviewoptions']=$reviewoptions;
			if($reviewoptions != "all"){
				if($reviewoptions == "approvedads"){
					$reviewoptionsstring = "and isnull(reason_code)";
				}else{
					$reviewoptionsstring = "and not isnull(reason_code)";
				}

			}


			$adtype = requestParams("adtype");
			if($adtype == ""){
				$adtype = "all";
			}
			$_REQUEST['adtype']=$adtype;
			if($adtype != "all"){
				if($adtype=="video")
					$adtypestring = "and campaign_type in ('preroll','midroll','postroll')";
				else
					$adtypestring = "and campaign_type = '$adtype'";
			}
		}
		
		$sql = "select channels.*,channel_campaigns.*, geography.country_codes, geography.state_codes, concat(sec_to_time(timetargetfrom*60
		),' GMT to ',sec_to_time(timetargetto*60),' GMT') as timetarget ,concat(frequency.impressions,' impressions every ',frequency.unit)
		as frequencytarget, sum(views) as newviews,blocked_campaigns.reason_code, campaign.name as campaign_name,date(campaign.validfrom)
		as startdate, date(campaign.validto) as enddate, campaign.status as status1, if(campaign.money_spent_today<(campaign.daily_limit-campaign.price_per_unit) && campaign.tot_money_spent<(campaign.budget_amt-campaign.price_per_unit),'Live','Not live') as status2 from
		channels, channel_campaigns left join blocked_campaigns on channel_campaigns.channel_id = blocked_campaigns.channel_id and 
		channel_campaigns.campaign_id = blocked_campaigns.campaign_id left join campaign on channel_campaigns.campaign_id =campaign.id 
		left join geography on geography.campaign_id = channel_campaigns.campaign_id left join frequency on frequency.campaign_id = 
		channel_campaigns.campaign_id where publisher_id='$_SESSION[PUB_ID]' and channel_campaigns.channel_id=channels.id  and 
		campaign.status='active' $sitestring  $adtypestring $reviewoptionsstring group by 
		channel_campaigns.campaign_id,channel_campaigns.channel_id";
		
		$rs = $reportConn->execute($sql);
		if($rs && $rs->recordcount()>0){
			$summary = $rs->getrows();
		}
		$smarty->assign("summary",$summary);
		break;
}
?>
