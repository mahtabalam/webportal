<?php
include_once('include/datefuncs.php');
include_once('include/reportLib.php');
include_once('include/zip.class.php');
include_once('UniquesFetcher.php');

$channellist = array();		   	    
$sql = "select channels.name, channels.id from channels left join daily_stats on channels.id=channel_id left join campaign on campaign.id=campaign_id where campaign.advertiser_id='$_SESSION[ADV_ID]' and channels.channel_type!='iphone' group by daily_stats.channel_id order by channels.name";
$rs = $conn->execute($sql);
if($rs && $rs->recordcount()>0){
    $channellist = $rs->getrows();			    	
}
$smarty->assign("channellist",$channellist);

$campaignlist = array();
		   
   $sql = "select name, id from campaign where advertiser_id='$_SESSION[ADV_ID]' and device='pc' order by name";
   $rs = $conn->execute($sql);
if($rs && $rs->recordcount()>0){
    $campaignlist = $rs->getrows();
}
$smarty->assign("campaignlist", $campaignlist);

function getChannelName($channelId){
	global $conn;
	$sql="select channels.name from channels where id=$channelId";
	$rs = $conn->execute($sql);
	if($rs && $rs->recordcount()>0){
    return $rs->fields[0];			    	
}
return null;
}

function getCampaignName($campaignId){
	global $conn;
	$sql="select name from campaign where id=$campaignId";
	$rs = $conn->execute($sql);
	if($rs && $rs->recordcount()>0){
    return $rs->fields[0];			    	
}
return null;
}
if($_POST['submit']!=""){

	$date=getRequest('date');
	$campaign_id=getRequest('campaign');
	$channel_id=getRequest('channel');
	$period=getRequest('period');
	switch($period){
		case 'week':$date=UniquesFetcher::getWeek($date);$period='w';
		break;
		case 'month':$date=UniquesFetcher::getMonth($date);$period='m';
		break;
	}
	$measure_ca="ca";
	$measure_ch="ch";
	$measure2=getChannelName($channel_id);
	$measure1=getCampaignName($campaign_id);
	if($campaign_id=="0"){
		$campaign_id=$_SESSION[ADV_ID];
		$measure_ca="adv";
		$measure1="All Campaigns";
	}
	if($channel_id=="0"){
		$measure_ch="all";
		$measure2="All Channels";
	}
	$measure="$period.$measure_ca.$measure_ch.vi";
	//UniquesFetcher::getResult($date,adv_id/campiagn_id,0/channel_id, d/w/m . adv/ca . all/ch .vi );
	$result=UniquesFetcher::getResult($date,$campaign_id,$channel_id, $measure);
	
	$data["date"]=$date; $data["uniques"]=$result;
	$data["measure1"]=$measure1;
	$data["measure2"]=$measure2;
	$smarty->assign("data", $data);
	
	//echo "<pre>"; print_r($result);
	//print_r($_POST);
}
?>