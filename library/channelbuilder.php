<?php
include_once('include/datefuncs.php');

$sql = "select * from category";
$rs=$conn->execute($sql);
if($rs && $rs->recordcount()>0){
	$category= $rs->getrows();
	$smarty->assign('category',$category);
}
$vastVersions = array('vast1.0','vast2.0','vast3.0');
switch ($_REQUEST['sub']) {
	case 'channel':
		if($_REQUEST['action_allowsitetargeting']!="" || $_REQUEST['action_allowsitetargeting1']!="") {
			$channelid = $_REQUEST['chids'];
			$channelid = implode(',', $channelid);
			if($channelid!=""){
				if($_SESSION['ADMIN_PRIVILEGES']=="superadmin"  || $_SESSION['ADMIN_PRIVILEGES']=="sales"){
					$sql="update channels set verified='Verified', allow_sitetargeting=1 where id in($channelid) and channel_type='PC' and publisher_id = ".$_SESSION["PUB_ID"];
					$rs = $conn->execute($sql);
					if($rs){
						$msg="Allow site targeting done successfully.";
					}else{
						$msg = "Sorry, Please try again....";
					}
				}else{
					$msg = "You have no permission. Please contact with admin.";
				}
			}else{
				$msg="Choose at least one channel.";
			}
			doForward("$config[baseurl]/pub.php?page=channelbuilder&sub=channel&msg=$msg");
		}
		
		if($_REQUEST['action_block']!="" || $_REQUEST['action_block1']!="") { 
			$channelid = $_REQUEST['chids'];
			$channelid = implode(',', $channelid);
			if($channelid!=""){
				echo $sql="update channels set verified='Not Verified', allow_sitetargeting=0 where id in($channelid) and channel_type='PC' and publisher_id = ".$_SESSION["PUB_ID"];
				$rs = $conn->execute($sql);
				if($rs){
					$msg="Channels blocked successfully.";
				}else{
					$msg = "Sorry, Please try again....";
				}
			}else{
				$msg="Choose at least one channel.";
			}
			doForward("$config[baseurl]/pub.php?page=channelbuilder&sub=channel&msg=$msg");
		}
		
		if($_REQUEST['action_unblock']!="" || $_REQUEST['action_unblock1']!="") {
			$channelid = $_REQUEST['chids'];
			$channelid = implode(',', $channelid);
			if($channelid!=""){
				$sql="update channels set verified='Verified' where id in($channelid) and channel_type='PC' and publisher_id = ".$_SESSION["PUB_ID"];
				$rs = $conn->execute($sql);
				if($rs){
					$msg="Channels unblocked successfully.";
				}else{
					$msg = "Sorry, Please try again....";
				}
			}else{
				$msg="Choose at least one channel.";
			}
			doForward("$config[baseurl]/pub.php?page=channelbuilder&sub=channel&msg=$msg");
		}
		
		if($_REQUEST['action_deploy']){
		  $channelid=mysql_escape_string($_REQUEST['channelid']);
		  $rollback=mysql_escape_string($_REQUEST['rollback']);
		  $channelname=mysql_escape_string($_REQUEST['name']);
		  $sql="select * from channel_settings where channel_id='$channelid'";
		  $rs=$conn->execute($sql);
		  if($rs && $rs->recordcount()>0){
		  	$json = json_decode($rs->fields['additional_settings']);
		  	$channel['channelskins']=$json->channelskins;
		  	$playerurl = playerurlFormat($channel['channelskins']->skin, $channel['channelskins']->country, $json->plversion);
			if($rollback!=$playerurl){
			  $sql="update channel_settings set rollback='$rollback', deployed='true', finalize_action='false' where channel_id='$channelid'";
			  $rs=$conn->execute($sql);
			  if($rs){
			  	$msg="Deployed Successfull";
			  }else{
			  	$msg="Try again.....";
			  }
			}else{
			  $msg="No changes made";
			}
		  }else{
		  	throw new Exception('Invalid channel id');
		  }
		  doForward("$config[baseurl]/pub.php?page=channelbuilder&sub=channel&msg=$msg");
		}
		
		if($_REQUEST['changeStatus']=="true"){
			$_SESSION['channel']="";
			unset($_SESSION['channel']);
		}
		
		if($_REQUEST['action_rollback']){
		  $channelid=mysql_escape_string($_REQUEST['channelid']);
		  $sql="update channel_settings set rollback='null', deployed='false', finalize_action='true' where channel_id='$channelid'";
		  $rs=$conn->execute($sql);
		  if($rs){
		  	$msg="Changes has been rollbacked.";
		  }else{
		  	$msg="Try again.....";
		  }
		  doForward("$config[baseurl]/pub.php?page=channelbuilder&sub=channel&msg=$msg");
		}
		
		
		if($_REQUEST['action_finalize']){
		  $channelid=mysql_escape_string($_REQUEST['channelid']);
		  $rollback=mysql_escape_string($_REQUEST['rollback']);
		  $channelname=mysql_escape_string($_REQUEST['name']);
		  $sql="select * from channel_settings where channel_id='$channelid'";
		  $rs=$conn->execute($sql);
		  if($rs && $rs->recordcount()>0){
		  	$json = json_decode($rs->fields['additional_settings']);
		  	$channel['channelskins']=$json->channelskins;
		  	if($json->vidtype == "livevideo") $plversion=$json->lvplversion; else $plversion=$json->plversion;
		  	$playerurl = playerurlFormat($channel['channelskins']->skin, $channel['channelskins']->country, $plversion);
			if($rollback!=$playerurl){
			  $sql="update channels set playerurl='$playerurl' where id='$channelid'";
			  $rs=$conn->execute($sql);
			  if($rs){
			  	$sql1="update channel_settings set rollback='null', finalize_action='true' where channel_id='$channelid'";
			    $rs1=$conn->execute($sql1);
			  	$msg="Final action has been successfully executed";
			  }else{
			  	$msg="Try again.....";
			  }
			}else{
			  $msg="No changes made";
			}
		  }else{
		  	throw new Exception('Invalid channel id');
		  }
		  doForward("$config[baseurl]/pub.php?page=channelbuilder&sub=channel&msg=$msg");
		}
		
		if($_REQUEST['status']=="") $status = "";
		else if($_REQUEST['status']=="active") $status =" and verified='Verified'";
		else if($_REQUEST['status']=="blocked") $status =" and verified='Not Verified' and allow_sitetargeting=0 ";
		else $status = "";
		
		if($_REQUEST["searchtext"]!="") {
			$searchtext = mysql_escape_string($_REQUEST['searchtext']);
			$searchTxt = " and (name regexp '$searchtext' or url regexp '$searchtext') ";
		}else $searchTxt = "";
		
		$sql = "select channels.id as id, if(instr(cs.additional_settings, 'v3'), 'V3', if(instr(cs.additional_settings, 'v2'), 'V2', if(instr(cs.additional_settings, 'vast3.0'), 'VAST3.0',if(instr(cs.additional_settings, 'vast2.0'), 'VAST2.0',if(instr(cs.additional_settings,'vast1.0'), 'VAST1.0','Others'))))) as version, cs.deployed, cs.rollback, cs.finalize_action, if(cs.channel_id,'true','false') as chstatus, channels.type, channels.playerurl, name, url, apikey, verified, approval, allow_sitetargeting, cast(video_unit_price*1000 as decimal) as videocpm, cast(branded_unit_price * 1000 as decimal)  as brandedcpm, cast(overlay_unit_price * 1000 as decimal) as overlaycpm from channels left join publishersheet on channels.id=publishersheet.channel_id and isnull(publishersheet.validto) left join channel_settings cs on cs.channel_id=publishersheet.channel_id where publisher_id = '$_SESSION[PUB_ID]' and channels.channel_type='PC' $status $searchTxt order by version, name";
		$rs = $conn->execute($sql);
		if($rs && $rs->recordcount()>0){
			$data = $rs->getrows();
			$smarty->assign('data',$data);
		}
		
		$_SESSION['countryObj']=json_encode(getCountryList());
		$sql="select skin_vkey, title, version from skin where publisher_id=0 or publisher_id={$_SESSION['PUB_ID']}";
		$rs=$conn->execute($sql);
		if($rs && $rs->recordcount()>0){
			while(!$rs->EOF){
				if($rs->fields['version']=='v2'){
					$v2[$rs->fields['skin_vkey']]=$rs->fields['title'];
				}elseif ($rs->fields['version']=='v3'){
					$v3[$rs->fields['skin_vkey']]=$rs->fields['title'];
				}
				$rs->movenext();
			}
			$skinArr=array('v2' => $v2, 'v3' => $v3);
			$skinObj=json_encode($skinArr);
			$_SESSION['skinObj']=$skinObj;
		}
		
		if($_REQUEST['action_paused']!=null){
			if(!empty($campaignids)){
				$campaignids=$_REQUEST['campaignids'];
				$ids = "(".implode( ',', $campaignids ).")";
				$sql = "update campaign set status = 'paused' where id in $ids and status = 'active'";
				$rs = $conn->execute($sql);
				$rows = $conn->Affected_Rows();
				if($rows>0){
					$error = "Campaign paused successfully";

				}else{
					$error = "Only active campaigns can be paused";
				}
			}
		} 
		break;
	case 'remnant':
		# fetch to edit 
		if(!$_REQUEST['action_remnant'] && !$_GET['status'] && $_GET['tag_id']){
			$tag_id = getRequest('tag_id');
			$sql = "select channel_id, ad_format, tag, priority from vast_tags join channels on vast_tags.channel_id=channels.id where vast_tags.id=$tag_id and channels.channel_type='PC'";
			$rs = $conn->execute($sql);
			if($rs && $rs->recordcount()>0){
				$_REQUEST['channel_id']=$rs->fields['channel_id'];
				$_REQUEST['adformat']=$rs->fields['ad_format'];
				$_REQUEST['vast_tag']=$rs->fields['tag'];
				$_REQUEST['priority']=$rs->fields['priority'];
			}
		}
		
		# enable/disable remnant ad servers
		if($_GET['status'] && $_GET['tag_id']){
			$status = getRequest('status');
			$tag_id = getRequest('tag_id');
			if($status=="enable"){
				$sql = "update vast_tags set status='enable' where id=$tag_id";
				$rs = $conn->execute($sql);
				if($rs) $msg="Tag has been enabled....."; else $msg="Please try again......";
			}elseif($status=="disable"){
				$sql = "update vast_tags set status='disable' where id=$tag_id";
				$rs = $conn->execute($sql);
				if($rs) $msg="Tag has been disabled....."; else $msg="Please try again......";
			}else{
				$msg="Invalid operation....";
			}
		}
		
		# add/edit remnant ad servers
		if($_REQUEST['action_remnant']){
			//echo "<pre>"; print_r($_REQUEST);
			$channel_id = getRequest('channel_id');
			$ad_format = getRequest('adformat');
			$tag = getRequest('vast_tag');
			$priority = getRequest('priority');
			$tag_id = getRequest('tag_id');
			if(!$tag_id){
				$sql = "select vast_tags.id from vast_tags join channels on vast_tags.channel_id=channels.id  where channel_id=$channel_id and ad_format='$ad_format' and tag='$tag' and channels.channel_type='PC'";
				$rs = $conn->execute($sql);
				if($rs && $rs->recordcount()>0){
					$msg = "\"$tag\" already exist......";
				}else{
					$insert_sql = "insert into vast_tags set channel_id=$channel_id, ad_format='$ad_format', status='disable',
							priority=$priority, tag='$tag'";
					$insert_rs = $conn->execute($insert_sql);
					if($insert_rs){
						$msg="Added successfully......";
						clearRequestVar();
					}else{
						$msg="Please try again......";
					}
				}
			}else{
				$sql = "select vast_tags.id from vast_tags join channels on vast_tags.channel_id=channels.id where channel_id=$channel_id and ad_format='$ad_format' and tag='$tag' and vast_tags.id<>$tag_id and channels.channel_type='PC'";
				$rs = $conn->execute($sql);
				if($rs && $rs->recordcount()>0){
					$msg = "\"$tag\" already exist for \"$ad_format\"......";
					clearRequestVar();
				}else{
					$insert_sql = "update vast_tags set ad_format='$ad_format',	priority=$priority, tag='$tag' where id=$tag_id";
					$insert_rs = $conn->execute($insert_sql);
					if($insert_rs){
						$msg="Updated successfully......";
						clearRequestVar();
					}else{
						$msg="Please try again......";
					}
				}
			}
		}
		
		# Fetch remnant Adservers
		$channel_id=getRequest('channel_id');
		$sql="select channel_id,vast_tags.id as id,ad_format,priority,status,tag from vast_tags join channels on vast_tags.channel_id=channels.id where channel_id=$channel_id and channels.channel_type='PC'";
		$rs=$conn->execute($sql);
		if($rs && $rs->recordcount()>0){
			$data=$rs->getrows();
			$smarty->assign("data", $data);
		}
		$smarty->assign("msg", $msg);
		break;	
	case 'controlclient':
		$streamid=mysql_escape_string($_REQUEST['streamid']);
		$backup=mysql_escape_string($_REQUEST['backup']);
		$sql = "select id, primary_connect_url, vdopia_stream_name, backup_connect_url, cdn_details from stream where publisher_id='".$_SESSION[PUB_ID]."' and id='$streamid'";
		$rs = $conn->execute($sql);
		if($rs && $rs->recordcount()>0){
			$cdnDetails=json_decode($rs->fields['cdn_details']);
			$smarty->assign('needStreamUrl',$cdnDetails->needStreamUrl);
			$smarty->assign('stream_url',$rs->fields['vdopia_stream_name']);
			if($backup=="true"){
				$smarty->assign('connect_url',$rs->fields['backup_connect_url']);
			}else{
				$smarty->assign('connect_url',$rs->fields['primary_connect_url']);
			}
		}
		break;
	case 'stream':
		$sql = "select id, cdn, name, backup_connect_url, date_format(event_start_date,'%M %d %Y') as event_start_date, date_format(event_end_date,'%M %d %Y') as event_end_date from stream where publisher_id='".$_SESSION[PUB_ID]."'";
		$rs = $conn->execute($sql);
		if($rs && $rs->recordcount()>0){
			$data = $rs->getrows();
			$smarty->assign('data',$data);
		}
		break;	
	case 'adstream':
		if($_REQUEST['action_save']!=""){
			$rules = array(); 							# stores the validation rules
		  	$rules[] = "required,stream_type,Error";
		  	$rules[] = "required,bit_rate,Error";
		  	$rules[] = "required,cl_password,Error";
		  	$rules[] = "required,secure,Error";
		  	$rules[] = "required,needStreamUrl,Error";
		  	$rules[] = "required,start_date,Error";
		  	$rules[] = "required,end_date,Error";
			
		  	$error = validateFields($_POST, $rules);
		  	
		  	if(empty($error)){
		  		$stream_name=mysql_escape_string($_REQUEST['stream_name']);
		  		$stream_type=mysql_escape_string($_REQUEST['stream_type']);
		  		$bit_rate=mysql_escape_string($_REQUEST['bit_rate']);
		  		$needStreamUrl=mysql_escape_string($_REQUEST['needStreamUrl']);
		  		$overlaymsg=mysql_escape_string($_REQUEST['overlaymsg']);
		  		$message=mysql_escape_string($_REQUEST['message']);
		  		$geoerrormsg=mysql_escape_string($_REQUEST['geoerrormsg']);
		  		$domainerrormsg=mysql_escape_string($_REQUEST['domainerrormsg']);
		  		$fatalerrormsg=mysql_escape_string($_REQUEST['fatalerrormsg']);
		  		$format=mysql_escape_string($_REQUEST['format']);
		  		$cl_username=mysql_escape_string($_REQUEST['cl_username']);
		  		$cl_password=mysql_escape_string($_REQUEST['cl_password']);
		  		$event_start_date=mysql_escape_string($_REQUEST['start_date']);
		  		$event_end_date=mysql_escape_string($_REQUEST['end_date']);
		  		$secure=mysql_escape_string($_REQUEST['secure']);
		  		$maxAdsPerAdBreak=mysql_escape_string($_REQUEST['maxAdsPerAdBreak']);
		  		$maxTimeBetweenPlayListFetches=mysql_escape_string($_REQUEST['maxTimeBetweenPlayListFetches']);
		  		
		  		if($_REQUEST['defaultAd']!='' && !file_exists("$config[AD_IMG_DIR]/".$_REQUEST['defaultAd'])){
					$srcImage="$config[tempfilepath]/".$_REQUEST['defaultAd'];
					$destImage="$config[AD_IMG_DIR]/".$_REQUEST['defaultAd'];
					$defaultAd="$config[CDN_URL]/files/images/".$_REQUEST['defaultAd'];
					error_log(rename($srcImage,$destImage));
					rename($srcImage,$destImage);
				}elseif($_REQUEST['defaultAd']!=""){
					$defaultAd="$config[CDN_URL]/files/images/".$_REQUEST['defaultAd'];
				}
		  		
		  		if($event_start_date && $event_start_date!="") $event_start_date=date("Ymd",strtotime($event_start_date));
		  		if($event_end_date && $event_end_date!="") $event_end_date=date("Ymd",strtotime($event_end_date));
		  		if($stream_type=='bitgravity'){
		  			# LIVE
		  			$bg_fms_url=mysql_escape_string($_REQUEST['bg_fms_url']);
		  			$port=substr($bg_fms_url,0,4);
		  			if(strtolower($port)=='rtmp'){
		  				;
		  			}elseif(strtolower($port)=="http"){
		  				$bg_rtmp="rtmp";
		  				$sp = explode('/',$bg_fms_url);
		  				foreach($sp as $key => $val){
		  					if($key<=2) $bg_server.="/$sp[$key]";
		  					if($key>=3) $bg_stream_name.="/$sp[$key]";
		  				}
		  				$bg_server=trim($bg_server,'/');
		  			}
		  			
		  			# VOD
		  			$vod_bg_rtmp=mysql_escape_string($_REQUEST['vod_bg_rtmp']);
		  			if(strtolower($vod_bg_rtmp)=='rtmp'){
		  				$bg_rtmp="true";
		  				$vod_bg_plconnect_url=mysql_escape_string($_REQUEST['rtmp_vod_bg_fms_url']);
		  				$bg_media_folder=mysql_escape_string($_REQUEST['vod_bg_media_folder']);
		  			}elseif(strtolower($vod_bg_rtmp)=="http"){
		  				$bg_rtmp="false";
		  				$vod_bg_fms_url=mysql_escape_string($_REQUEST['vod_bg_fms_url']);
		  				$sp = explode('/',$vod_bg_fms_url);
		  				foreach($sp as $key => $val){
		  					if($key<=2) $vod_bg_server.="/$sp[$key]";
		  					if($key>=3) $bg_media_folder.="/$sp[$key]";
		  				}
		  				$vod_bg_server=trim($vod_bg_server,'/');
		  			}
		  			$bg_secure_streaming_password=mysql_escape_string($_REQUEST['bg_secure_streaming_password']);
		  			$vod_bg_secure_streaming_password=mysql_escape_string($_REQUEST['vod_bg_secure_streaming_password']);
		  			$allowed_countries=trim(mysql_escape_string($_REQUEST['country_codes']),',');
		  			
		  			$cdnDetailsArray=array(
					'bg_server' => "$bg_server",
					'vod_bg_server' => "$vod_bg_server",
					'stream_name' => "$bg_stream_name",
					"vod_player_connect_url" =>"$vod_bg_plconnect_url",
					"needStreamUrl" =>"$needStreamUrl",
					"secure_streaming_password" =>"$vod_bg_secure_streaming_password",
					"vod_secure_streaming_profile" =>"",
					"vod_secure_streaming_password" =>"$vod_bg_secure_streaming_password",
					"bg_media_folder" =>"$bg_media_folder",
					"bg_rtmp" =>"$bg_rtmp",
					"allowed_countries" =>"$allowed_countries");
		  		}elseif($stream_type=='akamai'){
		  			$akamai_stream_name=mysql_escape_string($_REQUEST['akamai_stream_name']);
		  			$akamai_user_name=mysql_escape_string($_REQUEST['akamai_user_name']);
		  			$akamai_user_password=mysql_escape_string($_REQUEST['akamai_user_password']);
		  			$akamai_pep=mysql_escape_string($_REQUEST['akamai_pep']);
		  			$akamai_bep=mysql_escape_string($_REQUEST['akamai_bep']);
		  			$akamai_plconnect_url=mysql_escape_string($_REQUEST['akamai_plconnect_url']);
		  			$akamai_encoder_ipadd=mysql_escape_string($_REQUEST['akamai_encoder_ipadd']);
		  			$akamai_ssp=mysql_escape_string($_REQUEST['akamai_ssp']);
		  			$akamai_sspwd=mysql_escape_string($_REQUEST['akamai_sspwd']);
		  			$akamai_fingerprint=mysql_escape_string($_REQUEST['akamai_fingerprint']);
		  			$vod_player_connect_url=mysql_escape_string($_REQUEST['vod_player_connect_url']);
		  			$vod_secure_streaming_profile=mysql_escape_string($_REQUEST['vod_secure_streaming_profile']);
		  			$vod_secure_streaming_password=mysql_escape_string($_REQUEST['vod_secure_streaming_password']);
		  			$vod_player_prefix=mysql_escape_string($_REQUEST['vod_player_prefix']);
		  			
		  			$cdnDetailsArray=array(
		  			'stream_name' => "$akamai_stream_name",
		  			'user_name' => "$akamai_user_name",
		  			'user_password' => "$akamai_user_password",
		  			'primary_entry_point' => "$akamai_pep",
		  			'backup_entry_point' => "$akamai_bep",
					'player_connect_url' => "$akamai_plconnect_url",
					'encoder_ipaddress' => "$akamai_encoder_ipadd",
					'secure_streaming_profile' => "$akamai_ssp",
					"needStreamUrl" =>"$needStreamUrl",
					'fingerprint' => "$akamai_fingerprint",
					'secure_streaming_password' => "$akamai_sspwd",
					"vod_player_connect_url" =>$vod_player_connect_url,
					"vod_secure_streaming_profile" =>$vod_secure_streaming_profile,
					"vod_secure_streaming_password" =>$vod_secure_streaming_password,
					"vod_player_prefix" =>$vod_player_prefix);
		  		}
		  		
				$cdnDetailsJson = mysql_real_escape_string(json_encode($cdnDetailsArray));
				$streamDetailsJson = json_encode(array("maxTimeBetweenPlayListFetches" => $maxTimeBetweenPlayListFetches, 
														"maxAdsPerAdBreak" => $maxAdsPerAdBreak, 
														"defaultAd" => $defaultAd,
														"overlaymsg" => $overlaymsg,
														"geoerrormsg" => $geoerrormsg,
														"domainerrormsg" => $domainerrormsg,
														"fatalerrormsg" => $fatalerrormsg)
												);
				
				if($_REQUEST['stream_id']){
					$prefix='update';
					$sqlSnip="";
					$postfix="where id='".$_REQUEST['stream_id']."' and publisher_id='".$_SESSION['PUB_ID']."'";
				}else{
					$prefix='insert into';
					$sqlSnip="primary_connect_url='rtmp://i2.vdopia.com', backup_connect_url='NULL', vdopia_stream_name='ID-Something',";
					$postfix='';
				}
		  		$sql="$prefix stream set
		  			publisher_id='".$_SESSION['PUB_ID']."',
		  			cdn='$stream_type',
		  			name='$stream_name',
		  			cdn_details='$cdnDetailsJson',
		  			stream_details='$streamDetailsJson',
		  			message='$message',
		  			bit_rate='$bit_rate',
		  			format='$format',
		  			$sqlSnip
		  			client_username='$cl_username',
		  			client_password='$cl_password',
		  			event_start_date='$event_start_date',
		  			event_end_date='$event_end_date',
		  			secure='$secure' $postfix";
		  		
		  		$rs=$conn->execute($sql);
		  		if($rs){
		  			$msg="Stream details has been successfully saved.";
		  			doForward("$config[baseurl]/pub.php?page=channelbuilder&sub=stream&msg=$msg");
		  		}else{
		  			$msg="Stream details not saved.Please make sure any messages not contain single or double quote(', \"). Please try again.............";
		  			doForward("$config[baseurl]/pub.php?page=channelbuilder&sub=stream&msg=$msg");
		  		}
		  	}
		}elseif(isset($_REQUEST['id'])){
			$stream_id=mysql_escape_string($_REQUEST['id']);
			$sql="select * from stream where id='$stream_id' and publisher_id='".$_SESSION['PUB_ID']."'";
			$rs = $conn->execute($sql);
			if($rs && $rs->recordcount() > 0){
				$_REQUEST['stream_type']=$rs->fields['cdn'];
				$_REQUEST['stream_name']=$rs->fields['name'];
				$_REQUEST['bit_rate']=$rs->fields['bit_rate'];
				$_REQUEST['message']=$rs->fields['message'];
				$_REQUEST['format']=$rs->fields['format'];
				$_REQUEST['cl_username']=$rs->fields['client_username'];
				$_REQUEST['cl_password']=$rs->fields['client_password'];
				$_REQUEST['start_date']=date("M d Y",strtotime($rs->fields['event_start_date']));
				$_REQUEST['end_date']=date("M d Y",strtotime($rs->fields['event_end_date']));
				$_REQUEST['secure']=$rs->fields['secure'];
				$streamDetails=json_decode($rs->fields['stream_details']);
				$_REQUEST['maxTimeBetweenPlayListFetches']=$streamDetails->maxTimeBetweenPlayListFetches;
				$_REQUEST['maxAdsPerAdBreak']=$streamDetails->maxAdsPerAdBreak;
				$_REQUEST['overlaymsg']=$streamDetails->overlaymsg;
				$_REQUEST['geoerrormsg']=$streamDetails->geoerrormsg;
				$_REQUEST['domainerrormsg']=$streamDetails->domainerrormsg;
				$_REQUEST['fatalerrormsg']=$streamDetails->fatalerrormsg;
				$defaultAd=explode('/',$streamDetails->defaultAd);
				$_REQUEST['defaultAd']=$defaultAd[count($defaultAd)-1];
				$cdnDetails=json_decode($rs->fields['cdn_details']);
				$_REQUEST['needStreamUrl'] = $cdnDetails->needStreamUrl;
				if($rs->fields['cdn']=='bitgravity'){
		  			$_REQUEST['bg_fms_url'] = $cdnDetails->bg_server.$cdnDetails->stream_name;
		  			if($cdnDetails->bg_rtmp=="true"){
		  				$_REQUEST['vod_bg_rtmp']="rtmp";
		  				$_REQUEST['rtmp_vod_bg_fms_url'] = $cdnDetails->vod_player_connect_url;
		  				$_REQUEST['vod_bg_media_folder'] = $cdnDetails->bg_media_folder;
		  			}else{
		  				$_REQUEST['vod_bg_rtmp']="http";
		  				$_REQUEST['vod_bg_fms_url'] = $cdnDetails->vod_bg_server.$cdnDetails->bg_media_folder;
		  			}
					$_REQUEST['vod_bgsecure_streaming_profile']=$cdnDetails->vod_secure_streaming_profile;
					$_REQUEST['bg_secure_streaming_password']=$cdnDetails->secure_streaming_password;
					$_REQUEST['vod_bg_secure_streaming_password']=$cdnDetails->vod_secure_streaming_password;
					$smarty->assign('geoTarget',$cdnDetails->allowed_countries);
		  		}elseif($rs->fields['cdn']=='akamai'){
		  			$_REQUEST['akamai_stream_name'] = $cdnDetails->stream_name;
		  			$_REQUEST['akamai_user_name'] = $cdnDetails->user_name;
		  			$_REQUEST['akamai_user_password'] = $cdnDetails->user_password;
		  			$_REQUEST['akamai_pep'] = $cdnDetails->primary_entry_point;
		  			$_REQUEST['akamai_bep'] = $cdnDetails->backup_entry_point;
		  			$_REQUEST['akamai_plconnect_url'] = $cdnDetails->player_connect_url;
		  			$_REQUEST['akamai_encoder_ipadd'] = $cdnDetails->encoder_ipaddress;
		  			$_REQUEST['akamai_ssp'] = $cdnDetails->secure_streaming_profile;
		  			$_REQUEST['akamai_fingerprint'] = $cdnDetails->fingerprint;
		  			$_REQUEST['akamai_sspwd'] = $cdnDetails->secure_streaming_password;
		  			$_REQUEST['vod_player_connect_url']=$cdnDetails->vod_player_connect_url;
					$_REQUEST['vod_secure_streaming_profile']=$cdnDetails->vod_secure_streaming_profile;
					$_REQUEST['vod_secure_streaming_password']=$cdnDetails->vod_secure_streaming_password;
					$_REQUEST['vod_player_prefix']=$cdnDetails->vod_player_prefix;
		  		}
		  		$smarty->assign("stream_id",$stream_id);
			}
		}
		$countryList=getCountryList();
		$smarty->assign("countrycodes",$countryList);
		break;	
	case 'blockedip':
		$streamid=mysql_escape_string($_REQUEST['streamid']);
		if($_REQUEST['action_blockip']!=""){
			$encip=mysql_escape_string(trim(strtolower($_REQUEST['encip'])));
			$label=mysql_escape_string(trim(strtolower($_REQUEST['label'])));
			$longip=decodeIP($encip);
			if($_REQUEST['subnet']=="yes"){
				$longip = $longip&0xffffff00;
			}
			$sql="insert into blocked_ips set stream_id='$streamid', longip='$longip', label='$label'";
			$rs=$conn->execute($sql);
			$rows=$conn->Affected_Rows();
			$ip = long2ip($longip);
			if($rows>0){
				$msg="Succesfully blocked this IP($ip).";
			}else{
				$msg="This IP($ip) is already blocked. Please try with another ip.....";
			}
			doForward("$config[baseurl]/pub.php?page=channelbuilder&sub=blockedip&msg=$msg&streamid=$streamid");
		}
		
		if($_REQUEST['action_delete']!=""){
			$longip=mysql_escape_string($_REQUEST['longip']);
			$sql="delete from blocked_ips where longip='$longip' and stream_id='$streamid'";
			$rs=$conn->execute($sql);
			$rows=$conn->Affected_Rows();
			if($rows>0){
				$msg="Succesfully removed this IP(".long2ip($longip).") from blocked list.";
			}else{
				$msg="Please try some time later.....";
			}
			doForward("$config[baseurl]/pub.php?page=channelbuilder&sub=blockedip&msg=$msg&streamid=$streamid");
		}
		
		$sql = "select cdn, name, longip, label from stream, blocked_ips where stream.id=blocked_ips.stream_id and id='$streamid' order by name";
		$rs = $conn->execute($sql);
		if($rs && $rs->recordcount()>0){
			$data = $rs->getrows();
			foreach($data as $value){
				$dt['cdn']=$value['cdn'];
				$dt['name']=$value['name'];
				$dt['longip']=$value['longip'];
				$dt['label']=$value['label'];
				$dt['ip']=long2ip($value['longip']);
				$dat[]=$dt;
			}
			$smarty->assign('data',$dat);
		}
		break;
	case 'skin':
		$sql = "select * from skin where publisher_id = '$_SESSION[PUB_ID]' order by title";
		$rs = $conn->execute($sql);
		if($rs && $rs->recordcount()>0){
			$data = $rs->getrows();
			$smarty->assign('data',$data);
		}
		break;
	case 'adskin';
		if($_REQUEST['action_skinad']!=""){
		  $skintitle=mysql_escape_string(strip_tags($_REQUEST['skintitle']));
		  $frompath=$config['tempfilepath']."/".$_REQUEST['swfurl'];
		  $imgfile=md5($_REQUEST['swfurl'].rand(0,1000000000));
		  $topath=$config['SKIN_DIR']."/$imgfile.swf";			# $skinpath=$config['AD_SWF_URL']."/$imgfile1.swf";
		  $version=mysql_escape_string(strip_tags($_REQUEST['version']));
		  $description=mysql_escape_string(strip_tags($_REQUEST['description']));
		  $rs=false;
		  
		  error_log("rename($frompath,$topath)");
		
		  if(rename($frompath,$topath)){
		  	$sql="insert into skin set publisher_id='$_SESSION[PUB_ID]', skin_vkey='$imgfile', version='$version', title='$skintitle', description='$description'";
		  	$rs=$conn->execute($sql);
			if(!$rs) {
				$msg="Skin uploading failed. Please try some time later.";
				rename($topath,$frompath.".swf");
			}else{
				$msg="Skin uploaded successfully.";
			}
		  }else{
		  	$msg="Permission Denied.";
		  }
		  doForward("$config[baseurl]/pub.php?page=channelbuilder&sub=skin&msg=$msg");
		}
		break;
	case 'editadskin':
		if($_REQUEST['action_editskinad']){
		  $id=mysql_escape_string(strip_tags($_REQUEST['id']));
		  $skintitle=mysql_escape_string(strip_tags($_REQUEST['skintitle']));
		  $version=mysql_escape_string(strip_tags($_REQUEST['version']));
		  $description=mysql_escape_string(strip_tags($_REQUEST['description']));
		  $sql="update skin set title='$skintitle', version='$version', description='$description' where publisher_id='$_SESSION[PUB_ID]' and id='$id'";
		  $rs=$conn->execute($sql);
		  if($rs && $conn->Affected_Rows()>0){
		  	$msg="Skin successfully saved.";
		  }else{
		  	$msg="No changes made";
		  }
		  doForward("$config[baseurl]/pub.php?page=channelbuilder&sub=skin&msg=$msg");
		}
		$id=mysql_escape_string($_REQUEST['id']);
		$sql="select * from skin where id='$id'";
		$rs=$conn->execute($sql);
		if($rs && $rs->recordcount()>0){
		  $_REQUEST['id']=$rs->fields['id'];
		  $_REQUEST['skintitle']=$rs->fields['title'];
		  $_REQUEST['version']=$rs->fields['version'];
		  $_REQUEST['skin_vkey']=$rs->fields['skin_vkey'];
		  $_REQUEST['description']=$rs->fields['description'];
		}else {
		  throw new Exception('Invalid Skin Id.');
		}
		break;		
	case 'pubchnladoptions':
		try {
			if($_REQUEST['action_pubchnladoptions']!=null){
			  /* TODO: add rules */
			  //var_dump($_REQUEST);exit;
			  $rules = array(); // stores the validation rules
			  $rules[] = "required,name,Error";
			  $rules[] = "required,url,Error";
			  $rules[] = "required,channelType,Error";
			  if($_SESSION['ADMIN_PRIVILEGES']=="superadmin"  || $_SESSION['ADMIN_PRIVILEGES']=="sales"){
			  	$rules[] = "range=0-100,fill_rate,Numbers should be in b/w 0-100";
			  	$rules[] = "digits_only,skipoffset,Skipoffset should be a valid number";
			  }
			  $rules[] = "if:channelType=video,required,vidtype, Error";
			  $rules[] = "if:channelType=video,if:vidtype=vod,required,vidsubtype, Error";
			  $rules[] = "if:channelType=video,if:vidtype=vod,if:vidsubtype=flv,required,flvvidpltype, Error";
			  $rules[] = "if:channelType=video,if:vidtype=vod,if:vidsubtype=flv,if:flvvidpltype=vdopiaplayer,required,plversion, Error";
			  $rules[] = "if:channelType=video,if:vidtype=vod,if:vidsubtype=flv,if:flvvidpltype=publisherplayer,required,vodpubpltype, Error";
			  $rules[] = "if:channelType=video,if:vidtype=vod,if:vidsubtype=wmv,required,wmvvidpltype, Error";
			  $rules[] = "if:channelType=video,if:vidtype=livevideo,required,lvvidsubtype, Error";
			  $rules[] = "if:channelType=video,if:vidtype=livevideo,if:lvvidsubtype=flv,required,flvlvvidpltype, Error";
			  $rules[] = "if:channelType=video,if:vidtype=livevideo,if:vidsubtype=flv,if:flvlvvidpltype=vdopiaplayer,required,lvplversion, Error";
			  $rules[] = "if:channelType=video,if:vidtype=livevideo,if:vidsubtype=flv,if:flvlvvidpltype=publisherplayer,required,lvpubpltype, Error";
			  $rules[] = "if:channelType=video,if:vidtype=livevideo,if:lvvidsubtype=wmv,required,wmvlvvidpltype, Error";
			  $rules[] = "if:channelType=video,if:vidtype=games,required,gamesubtype, Error";
			  $rules[] = "if:channelType=richmedia,required,richmediaadformat, Error";

			  $error = validateFields($_GET, $rules);
			  			  
			  if(empty($error)){
			    $id = mysql_escape_string($_REQUEST['id']);
			    $name = mysql_escape_string($_REQUEST['name']);
			    $url = mysql_escape_string(canonicalizeHostUrl($_REQUEST['url']));
			    if($_SESSION['ADMIN_PRIVILEGES']=="superadmin"  || $_SESSION['ADMIN_PRIVILEGES']=="sales"){
			    	$run_untargeted = mysql_escape_string($_REQUEST['run_untargeted']);
			    	$fill_rate = mysql_escape_string($_REQUEST['fill_rate']);
			    	$groups = $_REQUEST['groups'];
			    	$skipoffset = mysql_escape_string($_REQUEST['skipoffset']);
			    }
			    $apikey = md5("".rand(0,4000000000)."".rand(0,4000000000)."".rand(0,4000000000)."".rand(0,4000000000));
			    $approval = mysql_escape_string($_REQUEST['approval']);
			    $channelType = mysql_escape_string($_REQUEST['channelType']);
			    $action_pubchnladoptions = mysql_escape_string($_REQUEST['action_pubchnladoptions']);
				
			    channelsetting($channelType, &$channel, &$json);
			    if($_SESSION['ADMIN_PRIVILEGES']=="superadmin"  || $_SESSION['ADMIN_PRIVILEGES']=="sales"){
			    	$json['run_untargeted']=$run_untargeted==1?"true":"false";
			    	$json['fill_rate']=$fill_rate;
			    	$json['skipoffset']=$skipoffset;
			    }
			    $additional_settings = implode('->',$json);
			    $jsonstring = json_encode($json);
				
			    $channel['id'] = $id;
			    $channel['name'] = $name;
			    $channel['url'] = $url;
			    $channel['apikey'] = $apikey;
			    if($_SESSION['ADMIN_PRIVILEGES']=="superadmin"  || $_SESSION['ADMIN_PRIVILEGES']=="sales") { $channel['groups'] = $groups; }
			    $channel['approval'] = $approval;
			    $channel['channelType'] = $channelType;
			    $channel['jsonstring']=$jsonstring;
			    $channel['selSkinObj'] = json_encode($channel['channelskins']['skin']);
		  		$channel['selCountryObj'] = json_encode($channel['channelskins']['country']);
		  		$channel['playerurl']=playerurlFormat($channel['channelskins']['skin'], $channel['channelskins']['country'], $channel['version']);
			    $channel['action_pubchnladoptions']=$action_pubchnladoptions;
			    $channel['brandedcpm']=0;
			    $channel['videocpm']=0;
			    $channel['overlaycpm']=0; 
			    			    				
			    session_register('channel');
			    $_SESSION['channel']=$channel;
				
			    doForward("$config[baseurl]/pub.php?page=channelbuilder&sub=viewchannel&id=$id");
			  }else{
			  	$channelType = mysql_escape_string($_REQUEST['channelType']);
			  	channelsetting($channelType, &$channel, &$json);
			  	$_REQUEST['selSkinObj'] = json_encode($channel['channelskins']['skin']);
		  		$_REQUEST['selCountryObj'] = json_encode($channel['channelskins']['country']);
		  		$_REQUEST['version'] = $channel['version'];
		  	  }
			}else{
				$_REQUEST['useFP']='true';
			}
		
			$_SESSION['countryObj']=json_encode(getCountryList());
			$sql="select skin_vkey, title, version from skin where publisher_id=0 or publisher_id={$_SESSION['PUB_ID']}";
			$rs=$conn->execute($sql);
			if($rs && $rs->recordcount()>0){
				while(!$rs->EOF){
					if($rs->fields['version']=='v2'){
						$v2[$rs->fields['skin_vkey']]=$rs->fields['title'];
					}elseif ($rs->fields['version']=='v3'){
						$v3[$rs->fields['skin_vkey']]=$rs->fields['title'];
					}
					$rs->movenext();
				}
				$skinArr=array('v2' => $v2, 'v3' => $v3);
				$skinObj=json_encode($skinArr);
				$_SESSION['skinObj']=$skinObj;
			}
		
			# get all groups
			if($_SESSION['ADMIN_PRIVILEGES']=="superadmin"  || $_SESSION['ADMIN_PRIVILEGES']=="sales"){
				$sql = $conn->Prepare("select id, name from groups where portal=?");
				$rs = $conn->Execute($sql, array('vdopia'));
				if($rs && $rs->recordcount()>0){
					$groups = $rs->getrows();
					foreach($groups as $group) $groupdata[$group['id']]=$group['name'];
					$smarty->assign("groups", $groupdata);
				}
			}
		} catch (Exception  $e) {
		  $error['globalerror']=$e->getMessage();
		  $smarty->assign('error', $error);
		}
		break;
	case 'viewchannel':
		$channel=array();
		$id = mysql_escape_string($_REQUEST['id']);
		if($_SESSION['channel']['id']!=$id && $id!=''){
		  $sql = $conn->Prepare("select channel_settings.ad_format, channel_settings.additional_settings,channel_settings.codeParam, channels.id as id, name, url, apikey, verified, type, groups, channel_category, approval, cast(video_unit_price*1000 as decimal) as videocpm, cast(branded_unit_price * 1000 as decimal)  as brandedcpm, cast(overlay_unit_price * 1000 as decimal) as overlaycpm, margin, sales_percent from channels left join publishersheet on channels.id=publishersheet.channel_id and isnull(publishersheet.validto) left join channel_settings on channel_settings.channel_id=channels.id where channels.id=? and channels.channel_type='PC' and publisher_id=? limit 1");
		  $rs = $conn->execute($sql, array($id, $_SESSION[PUB_ID]));
	 	  if($rs && $rs->recordcount()>0){
			$channel['id'] = $rs->fields['id'];
			$channel['name'] = $rs->fields['name'];
			$channel['url'] = $rs->fields['url'];
			$channel['apikey'] = $rs->fields['apikey'];
			if($_SESSION['ADMIN_PRIVILEGES']=="superadmin"  || $_SESSION['ADMIN_PRIVILEGES']=="sales") { $channel['groups'] = explode(',', $rs->fields['groups']); }
			$channel['approval'] = $rs->fields['approval'];
			$channel['ad_type'] = explode(',', $rs->fields['ad_format']);
			if(in_array('banner', $channel['adformat']) || in_array('vdobanner', $channel['adformat'])) $channel['channelType'] = "richmedia"; else $channel['channelType'] = $rs->fields['type'];
			if($channel['channelType']=='tracker') $channel['adtype'] = 'trackerswf,trackerimage,extimage,extswf,dblclik,videobanner'; else $channel['adtype'] = $rs->fields['ad_format'];
			$channel['adformat'] = explode(',', $rs->fields['ad_format']);
			$channel['vodadformat'] = explode(',', $rs->fields['ad_format']);
			$channel['richmediaadformat'] = explode(',', $rs->fields['ad_format']);
			$channel['category'] = explode(',', $rs->fields['channel_category']);
			$channel['codeParam'] = $rs->fields['codeParam'];
			$json = json_decode($rs->fields['additional_settings']);
			foreach($json as $key => $val){
			  if($key=='plversion' || $key=='lvplversion' || $key=='vodpubpltype' || $key=='lvpubpltype') $channel['version']=$val;
			  if($key=='vidsubtype' || $key=='lvvidsubtype') $channel['video_type']=$val;
			  if($key=='flvvidpltype' || $key=='flvlvvidpltype' || $key=="wmvvidpltype" || $key=="wmvlvvidpltype") $channel['player_type']=$val;
			  if($key=='run_untargeted'){ 
			  	$channel["$key"]=$val=="false"?0:1; 
			  }else $channel["$key"]=$val;
			  $additional_settings[]=$val;
			}
		    $channel['selSkinObj'] = json_encode($channel['channelskins']->skin);
		    $channel['selCountryObj'] = json_encode($channel['channelskins']->country);
		    $channel['playerurl'] = playerurlFormat($channel['channelskins']->skin, $channel['channelskins']->country, $channel['version']);
			$channel['jsonstring']=$rs->fields['additional_settings'];
     	  
			$channel['brandedcpm']=$rs->fields['brandedcpm'];
			$channel['videocpm']=$rs->fields['videocpm'];
			$channel['overlaycpm']=$rs->fields['overlaycpm'];
			
			if($_SESSION['ADMIN_PRIVILEGES']=="superadmin"){
			  $channel['pubmargin']=100-$rs->fields['margin'];
			  $channel['salesmargin']=$rs->fields['sales_percent'];
			}
			
			session_register("channel");
			$_SESSION['channel']=$channel;
		  }
		}else{
		  $channel=$_SESSION['channel'];
		}
		$smarty->assign("channel", $channel);
		if($_REQUEST['repage']=='generatecode'){
			doForward("$config[baseurl]/pub.php?page=channelbuilder&sub=generatecode&id=".$channel['id']);
		}
		break;
	case 'savechannel':
		if($_REQUEST['cancel_edit']){
		  $_SESSION['channel']['action_pubchnladoptions']=null;
		  doForward("$config[baseurl]/pub.php?page=channelbuilder");
		}
		
		if($_REQUEST['save_channel'] && $_REQUEST['action_savechannel']!=null){
		  $channel=$_SESSION['channel'];
		  if($_SESSION['ADMIN_PRIVILEGES']=="superadmin"  || $_SESSION['ADMIN_PRIVILEGES']=="sales") { 
	  	  	if(sizeof($channel['groups'])>0) $groupSnip = ", groups='".implode(',', $channel['groups'])."'"; 
	  	  	else $groupSnip = ", groups=null";
	  	  }else $groupSnip = "";
		  if(isset($channel['id']) && $channel['id']!='' && $channel['id']>0){
		  	if($channel['action_pubchnladoptions']!=null){
		  	  $sql1="select id, verified, playerurl, allow_sitetargeting from channels where id='$channel[id]' and channels.channel_type='PC' and publisher_id=$_SESSION[PUB_ID]";
			  $rs=$conn->execute($sql1);
			  if($rs==false || $rs->recordcount()<=0) {
			    throw new Exception("Invalid id");
			  }
			  $verified=$rs->fields['verified'];
			  $playerurl=$rs->fields['playerurl'];
			  $allow_sitetargeting=$rs->fields['allow_sitetargeting'];
			  if($verified=='Verified' && $allow_sitetargeting==1){
			  	$sql1 = "update channels set name='$channel[name]', url='$channel[url]', channel_category='$channel[channel_category]', type='$channel[channelType]' $groupSnip where channels.id=$channel[id]";
			  }else{
			    $sql1 = "update channels set name='$channel[name]', url='$channel[url]', channel_category='$channel[channel_category]', type='$channel[channelType]', playerurl='$channel[playerurl]' $groupSnip where channels.id=$channel[id]";
			  }
			  $rs = $conn->execute($sql1);
		  	  if(!$rs) {
			    $msg="Channel url(<b>$channel[url]</b>) already exists Please enter another url...";
			    doForward("$config[baseurl]/pub.php?page=channelbuilder&msg=$msg");
		      }
			  $nameupdated=$conn->Affected_Rows();
			  /*TODO: Exception handling*/
			
			  $sql = "select * from channel_settings where channel_id='$channel[id]'";
			  $rs = $conn->execute($sql);
			  if(!$rs || $rs->recordcount()<=0){
			  	$sql="insert into channel_settings set channel_id='$channel[id]', ad_format='$channel[adtype]', additional_settings='$channel[jsonstring]', deployed='true', finalize_action='true'";
		        $rs = $conn->execute($sql);
		        if($rs==false) {
			      throw new Exception('Unknown Error(insert channel_setting failed).');
		        }
			  }else{
			    if($verified=='Verified' && $playerurl==$channel['playerurl']){
			  	  $sql = "update channel_settings set ad_format='$channel[adtype]', additional_settings='$channel[jsonstring]' where channel_id='$channel[id]'";
			    }if($verified=='Verified' && $playerurl!=$channel['playerurl'] && $allow_sitetargeting==1){
			  	  $sql = "update channel_settings set ad_format='$channel[adtype]', additional_settings='$channel[jsonstring]', deployed='false' where channel_id='$channel[id]'";
			    }else{
			  	  $sql = "update channel_settings set ad_format='$channel[adtype]', additional_settings='$channel[jsonstring]', deployed='true', finalize_action='true' where channel_id='$channel[id]'";
			    }
			  
			    $rs = $conn->execute($sql);
			    $settingupdated=$conn->Affected_Rows();
			  }
			  $_SESSION['channel']['action_pubchnladoptions']=null;
			  doForward("$config[baseurl]/pub.php?page=channelbuilder&changeStatus=true&msg=$msg");
		  	} else {
		  		doForward("$config[baseurl]/pub.php?page=channelbuilder");
		  	}
		  }else{
		  	if($channel['action_pubchnladoptions']!=null){
		  	  $sql = "insert into channels set name = '$channel[name]', channel_category='$channel[channel_category]', url = '$channel[url]', playerurl='$channel[playerurl]', approval='$channel[approval]', publisher_id = '$_SESSION[PUB_ID]', apikey='$channel[apikey]', type='$channel[channelType]', verified='Verified' $groupSnip";
		      $rs=$conn->execute($sql);
		      $id = $conn->Insert_ID();
		      if(!$rs) {
			    $msg="Channel url($channel[url]) already exists Please enter another url...";
			    doForward("$config[baseurl]/pub.php?page=channelbuilder&msg=$msg");
		      }else{
		      	$msg="Channel has been saved successfully.";
		      }
			
		      $sql="insert into channel_settings set channel_id='$id', ad_format='$channel[adtype]', additional_settings='$channel[jsonstring]', deployed='true', finalize_action='true'";
		      $rs = $conn->execute($sql);
		      if($rs==false) {
			    throw new Exception('Unknown Error(insert channel_setting failed).');
		      }
		      
		      ## adding publisher revenue share margin instead of default margin By:- Tanu Aggarwal
		      $sql= $conn->Prepare("select rev_share from revenue_share where publisher_id = ? order by effective_date desc limit 1");
		      $rs = $conn->execute($sql, array($_SESSION[PUB_ID]));
		      if ($rs && $rs->recordcount()>0) {
		      	if ($rs->fields['rev_share'] != '') {
		      		$revShare = $rs->fields['rev_share'];
		      	} else
		      		$revShare = $config[defaultmargin];
		      } else
		      	$revShare = $config[defaultmargin];
		      	
		      ## end
		      
		      $sql = "insert into publishersheet set video_unit_price = '$channel[videocpm]', overlay_unit_price = '$channel[overlaycpm]', branded_unit_price='$channel[brandedcpm]', margin = '$revShare', channel_id='$id', validfrom=now()";
		      $rs = $conn->execute($sql);
		      if($rs==false) {
			    throw new Exception('Unknown Error(insert publishersheet failed).');
		      }
		      
		      $_SESSION['channel']['action_pubchnladoptions']=null;
		    }
		    doForward("$config[baseurl]/pub.php?page=channelbuilder&msg=$msg");
		  }  
		}
		break;
	case 'generatecode':
                
		$channel=$_SESSION['channel'];
		if($_REQUEST['action_generatecode']){ 
                        echo "11";
			if($_REQUEST['dimension']!="custom" && $_REQUEST['dimension']!=""){ 
				$dimension = getRequest('dimension');
				$dim = explode('x', $dimension);
				$_REQUEST['plwidth']=$dim[0];
				$_REQUEST['plheight']=$dim[1];
				$codeParam.="dimension:$dimension|"; 
			}
		  	if($_REQUEST['plwidth']) $codeParam.="plwidth:".getRequest('plwidth')."|";
		  	if($_REQUEST['plheight']) $codeParam.="plheight:".getRequest('plheight')."|";
		  	if(isset($_REQUEST['istp'])) $codeParam.="istp:".getRequest('istp')."|";
		  	if(isset($_REQUEST['isab'])) $codeParam.="isab:".getRequest('isab')."|";
		  	if($_REQUEST['ad_type']) $codeParam.="ad_type:".implode(',',getRequest('ad_type'))."|";
		  	if($_REQUEST['mode']) $codeParam.="mode:".getRequest('mode')."|";
		  	if($_REQUEST['format']) $codeParam.="format:".getRequest('format')."|";
		  	if($_REQUEST['video_category']) $codeParam.="video_category:".implode(',',getRequest('video_category'))."|";
		  	$codeParam=trim($codeParam,'|');
		  	$_SESSION['channel']['codeParam']=$codeParam;
		  	$channel['codeParam']=$codeParam;
		  	$sql = "update channel_settings set codeParam='$codeParam' where channel_id=$channel[id]";
		  	$rs = $conn->execute($sql);
		}

		if($channel['codeParam']){
			$codeParam = explode("|",trim($channel['codeParam'],'|'));
			foreach ($codeParam as $val){
				$param=explode(":",$val);
				if($param[0]=='video_category' || $param[0]=="ad_type"){
					$_REQUEST[$param[0]]=explode(',',$param[1]);
				}elseif($param[0]=='rtmpconnecturl' || $param[0]=="akamaiconnecturl"){
					$_REQUEST[$param[0]]=$param[1].":".$param[2];
				}else{
					$_REQUEST[$param[0]]=$param[1];
				}
			}
			
			if($channel['channelType']=='richmedia'){
				$apikey=$channel['apikey'];
				$dimension=getRequest('plwidth').'x'.getRequest('plheight');
				$istp = getRequest('istp');
				$isab = getRequest('isab');
				if($isab==1)$adsby='true'; else $adsby='false';
				if($istp==1){
				$bannercode='<script src="'.$config['AD_SRV_URL'].'/bannerjs/'.$apikey.'/'.$dimension.'/'.$adsby.'"></script><noscript>
<a href="'.$config['AD_SRV_URL'].'/bannerjs/'.$apikey.'/'.$dimension.'/cl/static/[timestamp]">
<img src="'.$config['AD_SRV_URL'].'/bannerjs/'.$apikey.'/'.$dimension.'/vi/static/[timestamp]" border="0" />
</a><img src="'.$config['AD_SRV_URL'].'/bannerjs/'.$apikey.'/'.$dimension.'/tpvi/static/[timestamp]" border="0" width="0" height="0" /></noscript>';
				}else{
					$bannercode='<script src="'.$config['AD_SRV_URL'].'/bannerjs/'.$apikey.'/'.$dimension.'/'.$adsby.'"></script><noscript>
<a href="'.$config['AD_SRV_URL'].'/bannerjs/'.$apikey.'/'.$dimension.'/cl/static/[timestamp]">
<img src="'.$config['AD_SRV_URL'].'/bannerjs/'.$apikey.'/'.$dimension.'/vi/static/[timestamp]" border="0" /></a></noscript>';
				}
				$_REQUEST['bannercode']=$bannercode;
				$smarty->assign("bannercode",$bannercode);
			}
			
			if(in_array($channel['version'],$vastVersions)){
				
				if($channel['version']=='vast1.0'){
					$script="vastxml1.0"; 
					$postfix="";
				}else if($channel['version']=='vast2.0'){ 
					$script="vastxml";
					$postfix=";tagtype:publisher;ru=[referrer];rand=[TIME_STAMP]";
				} else if($channel['version']=='vast3.0'){ 
					$script="vastxml";
					$postfix=";tagtype:publisher;ver:3.0;ru=[referrer];rand=[TIME_STAMP]";
				} else{
					$script="";
					$postfix="";
				}
				$apikey=$channel['apikey'];
				$adtype=implode('|', $_REQUEST['ad_type']);
				$vidcat=implode('|', $_REQUEST['video_category']);
				$mode=getRequest('mode');
				$feedtype=getRequest('format');
				$vasttag=$config['AD_SRV_URL']."/$script/$apikey/$adtype/category:$vidcat;api_test:$mode;format:$feedtype$postfix";
				$_REQUEST['vasttag']=$vasttag;
				$smarty->assign("vasttag",$vasttag);
			}
		}
		if($_REQUEST['isab']=="") $_REQUEST['isab']=1;
		$smarty->assign("channel", $channel);
		break;
	case 'plpreview':
		$channel=$_SESSION['channel'];
		if($_REQUEST['action_preview']){
		  $channel['ad_format'] = implode('|', getRequest('ad_type'));
		  $channel['width'] = getRequest('plwidth');
		  $channel['height'] = getRequest('plheight');
		  $channel['autostart'] = getRequest('autostart');
		  $channel['fullscreen'] = getRequest('fullscreen');
		  $channel['video_category'] = implode('|', getRequest('video_category'));
		  $player_url=explode(',',$channel['playerurl']);
		  $channel['playerurl']=$player_url[0];
		  if(!$skinArr=$channel['channelskins']->skin) $skinArr=$channel['channelskins']['skin'];
		  if(!$countryArr=$channel['channelskins']->country) $countryArr=$channel['channelskins']['country'];
		  if(array_empty($countryArr)){
		  	$index = array_keys($countryArr, $_SESSION['country_code']);
		  	if(sizeof($index)>0) $channel['skin_vkey']=$skinArr[$index[0]];
		  }else{
		  	$channel['skin_vkey']=$skinArr[0];
		  }

		  $countryList=getCountryList();
		  if(sizeof($skinArr)>0){
		  	foreach($skinArr as $key => $skin_vkey){
		  		if($countryArr[$key]!=""){
		  			$geoSkin["$key-$skin_vkey"]=$countryList[$countryArr[$key]];
		  		}elseif($skin_vkey!=""){
		  			$geoSkin["$key-$skin_vkey"]="Default";
		  			$channel['skin_vkey']=$skin_vkey;
		  		}
		  	}
		  }
		  
		  $smarty->assign("channel", $channel);
		  $smarty->assign('geoSkin', $geoSkin);
		}
		break;
	case 'generate':
		$channel=$_SESSION['channel'];
		if($_REQUEST['action_generatecode']){
		  $channel['ad_format'] = implode('|', getRequest('ad_type'));
		  $stream = getRequest('stream');
		  $rtmpconnecturl = getRequest('rtmpconnecturl');
		  $akamaiconnecturl = getRequest('akamaiconnecturl');

		  if($stream=="http"){
		  	$channel['userparams']="volume=100";
		  	$channel['isAkamai']="false";
		  }elseif ($stream=="rtmp"){
		  	$channel['userparams']="volume=100&streamconnectURL=".$rtmpconnecturl;
		  	$channel['isAkamai']="false";
		  	$channel['connectionurl']=$rtmpconnecturl;
		  }elseif ($stream=="akamai"){
		  	$channel['userparams']="volume=100&streamconnectURL=".$akamaiconnecturl;
		  	$channel['isAkamai']="true";
		  	$channel['connectionurl']=$akamaiconnecturl;
		  }
		  
		  $channel['width'] = getRequest('plwidth');
		  $channel['height'] = getRequest('plheight');
		  $channel['autostart'] = getRequest('autostart');
		  $channel['fullscreen'] = getRequest('fullscreen');
		  $channel['playertype'] = getRequest('playertype');
		  
		  if($stream) $codeParam = "stream:$stream|";
		  if($_REQUEST['ad_type']) $codeParam.="ad_type:".implode(',',getRequest('ad_type'))."|";
		  if($_REQUEST['plwidth']) $codeParam.="plwidth:".getRequest('plwidth')."|";
		  if($_REQUEST['plheight']) $codeParam.="plheight:".getRequest('plheight')."|";
		  if($_REQUEST['autostart']) $codeParam.="autostart:".getRequest('autostart')."|";
		  if($_REQUEST['fullscreen']) $codeParam.="fullscreen:".getRequest('fullscreen')."|";
		  if($_REQUEST['video_category']) $codeParam.="video_category:".implode(',',getRequest('video_category'))."|";
		  if($_REQUEST['playertype']) $codeParam.="playertype:".getRequest('playertype')."|";
		  if($_REQUEST['rtmpconnecturl'] && $stream=="rtmp") $codeParam.="rtmpconnecturl:".getRequest('rtmpconnecturl')."|";
		  if($_REQUEST['akamaiconnecturl'] && $stream=="akamai") $codeParam.="akamaiconnecturl:".getRequest('akamaiconnecturl');
		  $codeParam=trim($codeParam,'|');
		  $_SESSION['channel']['codeParam']=$codeParam;
		  $channel['codeParam']=$codeParam;
		  $sql = "update channel_settings set codeParam='$codeParam' where channel_id=$channel[id]";
		  $rs = $conn->execute($sql);
		  
		  if($channel[playertype]=="wmv"){
		  	$channel['video_fileURL']="http://videos.cricketnirvana.com/Netstorage/Videos/inslt211242009/INvSL_Test2_Day2_MatchSummary_128.wmv";
		  }else{
		  	$channel['video_fileURL']="http://www.youtube.com/v/O7rc-DnJXDA&hl=en_US&fs=1&";
		  }
		  $channel['video_category'] = implode('|', getRequest('video_category'));
		  $smarty->assign("channel", $channel);
		  $codestring = "<html><body>".$smarty->fetch("player/$channel[vidtype]$channel[video_type]$channel[version].tpl")."</body></html>";
		  $smarty->assign("code", $codestring);
		}
		
		if($_REQUEST['action_download']){
		  switch($_REQUEST['action']){
		  	case 'playercode':
		  	  $codestring = getRequest('code');
		  	  header("Content-type: text/html");
			  header("Content-Disposition: attachment; filename=playercode.html");
			  echo $codestring;
			  exit;
		  	  break;
		  	case 'component':
		  	  if($channel['version']=="as2"){
		  	  	$filename=$config['BASE_URL']."/download/as2.0/VdoAd-AS2.swc";
		  	  }else{
		  	  	$filename=$config['BASE_URL']."/download/as3.0/VDOAds v2.2.swc";
		  	  }
		  	   header("Content-type: application/force-download");
			   header('Content-Disposition: inline; filename="' . $filename . '"');
			   header("Content-Transfer-Encoding: Binary");
			   header("Content-length: ".filesize($filename));
			   header('Content-Type: application/octet-stream');
			   header('Content-Disposition: attachment; filename="component.swc"');
			   readfile("$filename"); exit;
		  	  break;
		  	case 'samplefile':
		  	  if($channel['version']=="as2"){
		  	  	$filename=$config['BASE_URL']."/download/as2.0/Sample_files_AS2.0.zip";
		  	  }else{
		  	  	$filename=$config['BASE_URL']."/download/as3.0/Sample_files_AS3.0_v2.2.zip";
		  	  }
		  	   header("Content-type: application/force-download");
			   header('Content-Disposition: inline; filename="' . $filename . '"');
			   header("Content-Transfer-Encoding: Binary");
			   header("Content-length: ".filesize($filename));
			   header('Content-Type: application/octet-stream');
			   header('Content-Disposition: attachment; filename="samplefile.zip"');
			   readfile("$filename"); exit;
		  	  break;
		  	case 'documentation':
		  	  $filename=$config[baseurl]."/download/$channel[version].0/API.pdf";
		  	  header("Content-type: application/force-download");
			  header('Content-Disposition: inline; filename="' . $filename . '"');
			  header("Content-Transfer-Encoding: Binary");
			  header("Content-length: ".filesize($filename));
			  header('Content-Type: application/octet-stream');
			  header('Content-Disposition: attachment; filename="documentation.pdf"');
			  readfile("$filename"); exit;
		  	  break;
		  	case 'complete':
		  	  if($channel['version']=="as2"){
		  	  	$filename=$config['BASE_URL']."/download/as2.0/Package_AS2.0.zip";
		  	  }else{
		  	  	$filename=$config['BASE_URL']."/download/as3.0/Publisher_AS_3.0_v2.2.zip";
		  	  }
		  	   header("Content-type: application/force-download");
			   header('Content-Disposition: inline; filename="' . $filename . '"');
			   header("Content-Transfer-Encoding: Binary");
			   header("Content-length: ".filesize($filename));
			   header('Content-Type: application/octet-stream');
			   header('Content-Disposition: attachment; filename="package.zip"');
			   readfile("$filename"); exit;
		  	  break;
		  }
		}
		break;
	case 'download':
		switch($_REQUEST['type']){
		  case 'sdk':
		  	if($_REQUEST['version']){
			  $filename=$config['BASE_URL']."/download/v2.0/dummy.txt.zip";
		  	}else{
		  	  $filename=$config['BASE_URL']."/download/v3.0/dummy.txt.zip";
		  	}
		
		  	header("Content-type: application/force-download");
	   	  	header('Content-Disposition: inline; filename="' . $filename . '"');
	   	  	header("Content-Transfer-Encoding: Binary");
	   	  	header("Content-length: ".filesize($filename));
	   	  	header('Content-Type: application/octet-stream');
	   	  	header('Content-Disposition: attachment; filename="'.$_REQUEST['version'].'-skin-development-kit.zip"');
	   	  	readfile("$filename"); exit;
	   	  break;	
		}
		break;					
	case 'editpubchnladoptions':
		try{
			if($_REQUEST['action_editchannel']!=null){
			  $channel=$_SESSION['channel'];
			  foreach($channel as $key=>$val){
			  	if($key!='action_pubchnladoptions')
			      $_REQUEST["$key"]=$val;
			  }
			}
			
			if($_REQUEST['action_pubchnladoptions']!=null){
			  /* TODO: add rules */
			  $rules = array(); // stores the validation rules
			  $rules[] = "required,name,Error";
			  $rules[] = "required,channelType,Error";
			  $rules[] = "if:channelType=video,required,vidtype, Error";
			  if($_SESSION['ADMIN_PRIVILEGES']=="superadmin"  || $_SESSION['ADMIN_PRIVILEGES']=="sales"){
			  	$rules[] = "range=0-100,fill_rate,Numbers should be in b/w 0-100";
			  	$rules[] = "digits_only,skipoffset,Skipoffset should be a valid number";
			  }
			  $rules[] = "if:channelType=video,if:vidtype=vod,required,vidsubtype, Error";
			  $rules[] = "if:channelType=video,if:vidtype=vod,if:vidsubtype=flv,required,flvvidpltype, Error";
			  $rules[] = "if:channelType=video,if:vidtype=vod,if:vidsubtype=flv,if:flvvidpltype=vdopiaplayer,required,plversion, Error";
			  $rules[] = "if:channelType=video,if:vidtype=vod,if:vidsubtype=flv,if:flvvidpltype=publisherplayer,required,vodpubpltype, Error";
			  $rules[] = "if:channelType=video,if:vidtype=vod,if:vidsubtype=wmv,required,wmvvidpltype, Error";
			  $rules[] = "if:channelType=video,if:vidtype=livevideo,required,lvvidsubtype, Error";
			  $rules[] = "if:channelType=video,if:vidtype=livevideo,if:lvvidsubtype=flv,required,flvlvvidpltype, Error";
			  $rules[] = "if:channelType=video,if:vidtype=livevideo,if:vidsubtype=flv,if:flvlvvidpltype=vdopiaplayer,required,lvplversion, Error";
			  $rules[] = "if:channelType=video,if:vidtype=livevideo,if:vidsubtype=flv,if:flvlvvidpltype=publisherplayer,required,lvpubpltype, Error";
			  $rules[] = "if:channelType=video,if:vidtype=livevideo,if:lvvidsubtype=wmv,required,wmvlvvidpltype, Error";
			  $rules[] = "if:channelType=video,if:vidtype=games,required,gamesubtype, Error";
			  $rules[] = "if:channelType=richmedia,required,richmediaadformat, Error";
			  			  
			  $error = validateFields($_GET, $rules);
			  
			  if(empty($error)){
			    $id = mysql_escape_string($_REQUEST['id']);
			    $name = mysql_escape_string($_REQUEST['name']);
			    $url = mysql_escape_string($_REQUEST['url']);
				if($_SESSION['ADMIN_PRIVILEGES']=="superadmin"  || $_SESSION['ADMIN_PRIVILEGES']=="sales"){
			    	$run_untargeted = mysql_escape_string($_REQUEST['run_untargeted']);
			    	$fill_rate = mysql_escape_string($_REQUEST['fill_rate']);
			    	$groups = $_REQUEST['groups'];
			    	$skipoffset = mysql_escape_string($_REQUEST['skipoffset']);
				}else{
					if(isset($_SESSION['channel']['run_untargeted']) && $_SESSION['channel']['run_untargeted']!="") $run_untargeted = $_SESSION['channel']['run_untargeted'];
					if(isset($_SESSION['channel']['fill_rate']) && $_SESSION['channel']['fill_rate']!="") $fill_rate = $_SESSION['channel']['fill_rate'];
					if(isset($_SESSION['channel']['skipoffset']) && $_SESSION['channel']['skipoffset']!="") $skipoffset = $_SESSION['channel']['skipoffset'];
				}
			    $channelType = mysql_escape_string($_REQUEST['channelType']);
			    $action_pubchnladoptions = mysql_escape_string($_REQUEST['action_pubchnladoptions']);
			    $channel = array();
			    $json = array();
				
			    channelsetting($channelType, &$channel, &$json, &$adtype);
			    if(isset($run_untargeted) && $run_untargeted!="") { $json['run_untargeted']=$run_untargeted==1?"true":"false"; }
			    if(isset($fill_rate) && $fill_rate!="") { $json['fill_rate']=$fill_rate; }
			    if(isset($skipoffset) && $skipoffset!="") { $json['skipoffset']=$skipoffset; }
			    $additional_settings = implode('->',$json);
			     
			    $jsonstring = json_encode($json);
								
			    $channel['id'] = $id;
			    $channel['name'] = $name;
			    $channel['url'] = $url;
			    if($_SESSION['ADMIN_PRIVILEGES']=="superadmin"  || $_SESSION['ADMIN_PRIVILEGES']=="sales"){ $channel['groups'] = $groups; }
			    $channel['channelType'] = $channelType;
			    $channel['selSkinObj'] = json_encode($channel['channelskins']['skin']);
		  		$channel['selCountryObj'] = json_encode($channel['channelskins']['country']);
			    $channel['jsonstring']=$jsonstring;
			    $channel['action_pubchnladoptions']=$action_pubchnladoptions;
				$channel['playerurl']=playerurlFormat($channel['channelskins']['skin'], $channel['channelskins']['country'], $channel['version']);
			    		    
			    session_register('channel');
			    $_SESSION['channel']=array_merge($_SESSION['channel'], $channel);
				
			    doForward("$config[baseurl]/pub.php?page=channelbuilder&sub=viewchannel&id=$id");
			  }else{
			  	$channelType = mysql_escape_string($_REQUEST['channelType']);
			  	channelsetting($channelType, &$channel, &$json);
			  	$_REQUEST['selSkinObj'] = json_encode($channel['channelskins']['skin']);
		  		$_REQUEST['selCountryObj'] = json_encode($channel['channelskins']['country']);
		  		$_REQUEST['version'] = $channel['version'];
		  	  }
			}
			
			$_SESSION['countryObj']=json_encode(getCountryList());
			$sql="select skin_vkey, title, version from skin where publisher_id=0 or publisher_id={$_SESSION['PUB_ID']}";
			$rs=$conn->execute($sql);
			if($rs && $rs->recordcount()>0){
				while(!$rs->EOF){
					if($rs->fields['version']=='v2'){
						$v2[$rs->fields['skin_vkey']]=$rs->fields['title'];
					}elseif ($rs->fields['version']=='v3'){
						$v3[$rs->fields['skin_vkey']]=$rs->fields['title'];
					}
					$rs->movenext();
				}
				$skinArr=array('v2' => $v2, 'v3' => $v3);
				$skinObj=json_encode($skinArr);
				$_SESSION['skinObj']=$skinObj;
			}
			
			# get all groups
			if($_SESSION['ADMIN_PRIVILEGES']=="superadmin"  || $_SESSION['ADMIN_PRIVILEGES']=="sales"){
				$sql = $conn->Prepare("select id, name from groups where portal=?");
				$rs = $conn->Execute($sql, array('vdopia'));
				if($rs && $rs->recordcount()>0){
					$groups = $rs->getrows();
					foreach($groups as $group) $groupdata[$group['id']]=$group['name'];
					$smarty->assign("groups", $groupdata);
				}
			}
			
		} catch (Exception $e) {
		  $error['globalerror']=$e->getMessage();
		}
		break;
}

$smarty->assign('error', $error);

function channelsetting($channelType, $channel, $json, $adtype=""){
	if(isset($channelType) && $channelType!='' && $channelType=='video'){
	  $channel['vidtype']=$json['vidtype'] = getRequest('vidtype');
	  switch($_REQUEST['vidtype']){
		case 'vod':
		$channel['video_type']=$channel['vidsubtype']=$json['vidsubtype'] = getRequest('vidsubtype');
		switch($_REQUEST['vidsubtype']){
		  case 'flv':
			$channel['player_type']=$channel['flvvidpltype']=$json['flvvidpltype'] = getRequest('flvvidpltype');
			switch($_REQUEST['flvvidpltype']){
			  case 'vdopiaplayer':
			  	$channel['version']=$channel['plversion']=$json['plversion'] = getRequest('plversion');
			  	$channel['adtype']= implode(',', $_REQUEST['vodadformat']); //"preroll,postroll,overlay,branded";
			  	$channel['vodadformat'] = $_REQUEST['vodadformat'];
			  	switch($_REQUEST['plversion']){
			  	  case 'v2':
			  	  	if(sizeof($_REQUEST['vodv2skin'])>0){
			  	  	  $skins['skin'] = $_REQUEST['vodv2skin'];
			  	  	  $skins['country'] = $_REQUEST['vodv2country'];
			  	  	  $channel['channelskins'] = $json['channelskins'] = $skins;
			  	  	}
			  		break;
			  	  case 'v3':
			  	  	if(sizeof($_REQUEST['vodv3skin'])>0){
			  	  	  $skins['skin'] = $_REQUEST['vodv3skin'];
			  	  	  $skins['country'] = $_REQUEST['vodv3country'];
			  	  	  $channel['channelskins'] = $json['channelskins'] = $skins;
			  	  	}
			  	  	break;	
			  	}
			  	break;
			  case 'publisherplayer':
			  	$channel['version']=$channel['vodpubpltype']=$json['vodpubpltype'] = getRequest('vodpubpltype');	
			  	switch($_REQUEST['vodpubpltype']){	
			  	  	case 'as2':
			  	      $channel['adtype']="preroll,postroll,overlay";
			  	      break;
			  	  	case 'as3':
			  	  	  $channel['adtype']="preroll,postroll,midroll,overlay";
			  	  	  break;
			  	  	case 'vast1.0':
			  	  	  $channel['adtype']="preroll";	//,postroll,midroll,overlay,branded
			  	  	  break;
			  	  	case 'vast2.0':
			  	  	  $channel['adtype']="preroll,ytpreroll";	//,postroll,midroll,overlay,branded
			  	  	  break;    
		  	  	  	case 'vast3.0':
		  	  	  	  $channel['adtype']="preroll,ytpreroll";	//,postroll,midroll,overlay,branded
		  	  	  	  break;
			  	  }
			  	break; 	
			}
			break;
		  case 'wmv':
			$channel['player_type']=$channel['wmvvidpltype']=$json['wmvvidpltype'] = getRequest('wmvvidpltype');
			$channel['adtype']="preroll";
			break;
		}
		break;
		case 'livevideo':
		  $channel['video_type']=$channel['lvvidsubtype']=$json['lvvidsubtype'] = getRequest('lvvidsubtype');
		  switch($_REQUEST['lvvidsubtype']){
			case 'flv':
			  $channel['player_type']=$channel['flvlvvidpltype']=$json['flvlvvidpltype'] = getRequest('flvlvvidpltype');
			  switch($_REQUEST['flvlvvidpltype']){
			    case 'vdopiaplayer':
			  	  $channel['version']=$channel['lvplversion']=$json['lvplversion'] = getRequest('lvplversion');
			  	  $channel['adtype']= implode(',', $_REQUEST['adformat']); //"preroll,postroll,overlay,branded";
			  	  $channel['adformat'] = $_REQUEST['adformat'];
			  	  switch($_REQUEST['lvplversion']){
			  		case 'v2':
			  		  if(sizeof($_REQUEST['lvv2skin'])>0){
			  		  	$skins['skin'] = $_REQUEST['lvv2skin'];
			  	  	    $skins['country'] = $_REQUEST['lvv2country'];
			  	  	    $channel['channelskins'] = $json['channelskins'] = $skins;
			  		  }
			  		  break;
			  		case 'v3':
			  		  $channel['useFP'] = $json['useFP'] = $_REQUEST['useFP'];
			  		  $channel['protectDomain'] = $json['protectDomain'] = $_REQUEST['protectDomain'];
			  		  $channel['domains'] = $json['domains'] = $_REQUEST['domains'];
			  		  $channel['category'] = $_REQUEST['category'];
			  		  $channel['channel_category'] = implode(',',$_REQUEST['category']);
			  		  if($_REQUEST['FPColorList']!="" && $_REQUEST['useFP']=="true"){
			  		  	$FPColorList=trim($_REQUEST['FPColorList'], ';');
			  		  	$channel['FPColorList'] = $json['FPColorList'] = $FPColorList;
			  		  	$channel['FPColorChange'] = $json['FPColorChange'] = $_REQUEST['FPColorChange'];
			  		  }
			  		  if($_REQUEST['FPPositionList']!="" && $_REQUEST['useFP']=="true"){
			  		  	$FPPositionList=trim($_REQUEST['FPPositionList'], ';');
			  		  	$channel['FPPositionList'] = $json['FPPositionList'] = $FPPositionList;
			  		  	$channel['FPPositonChange'] = $json['FPPositonChange'] = $_REQUEST['FPPositonChange'];
			  		  }
			  		  if(sizeof($_REQUEST['lvv3skin'])>0){
			  		  	$skins['skin'] = $_REQUEST['lvv3skin'];
			  	  	    $skins['country'] = $_REQUEST['lvv3country'];
			  	  	    $channel['channelskins'] = $json['channelskins'] = $skins;
			  		  }
			  		  break;	
			  	  }
			  	  break;
			  	case 'publisherplayer':
			  	  $channel['version']=$channel['lvpubpltype']=$json['lvpubpltype'] = getRequest('lvpubpltype');	
			  	  switch($_REQUEST['lvpubpltype']){	
			  	  	case 'as2':
			  	      $channel['adtype']="preroll,postroll,overlay";
			  	      break;
			  	  	case 'as3':
			  	  	  $channel['adtype']="preroll,postroll,midroll,overlay";
			  	  	  break;
			  	  	case 'vast1.0':
			  	  	  $channel['adtype']="preroll";	//,postroll,midroll,overlay,branded
			  	  	  break;
			  	  	case 'vast2.0':
			  	  	  $channel['adtype']="preroll,ytpreroll";	//,postroll,midroll,overlay,branded
			  	  	  break;
		  	  	  	case 'vast3.0':
		  	  	  	  $channel['adtype']="preroll,ytpreroll";	//,postroll,midroll,overlay,branded
		  	  	  	  break;
			  	  }
			  	  break;
			    }
			  break;
			case 'wmv':
			  $channel['player_type']=$channel['wmvlvvidpltype']=$json['wmvlvvidpltype'] = getRequest('wmvlvvidpltype');
			  $channel['adtype']="preroll";  
			  break;
		  }
		  break;
		case 'games':
		  $channel['video_type']=$channel['gamesubtype']=$json['gamesubtype'] = getRequest('gamesubtype');
		  $channel['adtype']="preroll";	
		  break;		
	  }
	}elseif (isset($channelType) && $channelType!='' && $channelType=='richmedia'){
	  $channel['richmediaadformat']=$_REQUEST['richmediaadformat'];
	  $channel['adtype']=implode(',', $_REQUEST['richmediaadformat']);
	}elseif (isset($channelType) && $channelType!='' && $channelType=='tracker'){
	  $channel['adtype']='trackerswf,trackerimage,extimage,extswf,dblclik,videobanner';
	}
}

function playerurlFormat($skins, $countries, $version){
  global $config;
  if($version=="v3"){
  	$swfplayer="";
  	$skinpath="";
  	$defaultPlayer="";
  }else{
  	$swfplayer="$config[CDN_URL]/swf/mediaplayer_newskin.swf";
  	$skinpath="?skinPath=";
  	$defaultPlayer="$config[CDN_URL]/swf/mediaplayer.swf";
  }
  
  if(array_empty($skins)>0) {
  	if($swfplayer)$playerurl[] = "$swfplayer";
  	foreach ($skins as $key => $skin){
  	  if($countries[$key]!=""){
	    $playerurl[]=$countries[$key]." $swfplayer$skinpath$config[CDN_URL]/skins/$skin.swf";
  	  }else{
  	  	$playerurl[]="$swfplayer$skinpath$config[CDN_URL]/skins/$skin.swf";
  	  }
    }
  }else {
  	$playerurl[] = "$defaultPlayer";
  }
  return implode(',',$playerurl);
}

function clearRequestVar(){
	$_REQUEST['adformat']='';
	$_REQUEST['vast_tag']='';
	$_REQUEST['priority']='';
	$_REQUEST['tag_id']='';
}

/**
 * Check array is empty or not
 *
 * @param array $arr
 * @return 0 for empty & 1 for has values
 */
function array_empty($arr){
	$flag=0;
	foreach($arr as $key=>$val){
		if($val==""){
			continue;
		}else{
			$flag=1;
			break;
		}
	}
	return $flag;
}
?>