<?php
 
class VastFeedAds{
  private $conn;
  private $smarty;
  private $config;
  
  public function __construct(){
    global $conn, $smarty, $config;
    $this->conn = $conn;
    $this->smarty = $smarty;
    $this->config = $config;
  }
  
  public function processVastFeedAd(){
    $ads=array();
    for($ad=0;$ad<count($_REQUEST['vastfeedurls']);$ad++) {
      if($ad==1) break;
      $ads[$ad]['vastfeedurl']=$_REQUEST['vastfeedurls'][$ad];
      $ads[$ad]['vastfeedname']=$_REQUEST['vastfeednames'][$ad];
      $ads[$ad]['id']=$_REQUEST['ids'][$ad];
    }
    $campaign=$_SESSION['CAMPAIGN'];
    $campaign['name']=$ads[$ad-1]['vastfeedname'];
    $campaign['vastfeedads']=$ads;
    $_SESSION['CAMPAIGN']=$campaign;
    return $ad;
  }
  
  
  public function insertVastFeedAd($campaign_id){
    $vastfeedads=$_SESSION['CAMPAIGN']['vastfeedads'];
    if($vastfeedads != "") {
      foreach ($vastfeedads as $adrow) {
        if($adrow['id']!="") {
      	  $adid_already[]=$adrow['id'];
      	  continue;
        }
        $vastfeedname=mysql_escape_string(strip_tags($adrow['vastfeedname']));
        $vastfeedurl=mysql_escape_string(strip_tags($adrow['vastfeedurl']));
        
        $rs=false;
        $sql=$this->conn->Prepare("insert into ads set ad_format=?, advertiser_id=?, custom_canvas_url=?, branded_img_right_ref_imgname=?, action_type=?");
        $rs=$this->conn->execute($sql, array('video', $_SESSION[ADV_ID], $vastfeedurl ,$vastfeedname, 'vastfeed'));
    		
        if($rs){
          $ad=$this->conn->Insert_ID();
        }
        else{
          trigger_error("VastFeed Ad insertion failed: ".$conn->ErrorMsg(). E_USER_WARNING);
          $ad=-1;
        }
        if ($ad < 0) {
          $cleanup=1;
          break;
        }
        $adid[]=$ad;
      }
    }else{
      $cleanup=1;
    }
    $parameters = array($adid_already, $cleanup, $adid);
    return $parameters;
  }
  
  public function updateAds($vastfeedad){
  	foreach( $vastfeedad as $index => $value ){
		# check, if dirty flag is on
		if($value['dirty']==true ){
			$sqlstr='';
			foreach($value as $key => $val ){
				if( $key=="vastfeedname" )
					$sqlstr.="branded_img_right_ref_imgname='$val',";
				elseif($key=="vastfeedurl" )
					$sqlstr.="custom_canvas_url='$val',";
			}
			
			if($sqlstr!=""){
				$sql="update ads set ".trim($sqlstr,',')." where id='$value[id]' and advertiser_id='$_SESSION[ADV_ID]'";
				$rs = $this->conn->execute($sql);
				if(!$rs){
		      	  trigger_error("Ad is not updated: ".$conn->ErrorMsg(). E_USER_WARNING);
		    	}
			}
		}
	}
  }
}

?>