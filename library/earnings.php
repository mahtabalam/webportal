<?php

include_once('include/datefuncs.php');

require_once('include/smartyBlockFunction.php');
$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);



switch($_REQUEST['sub']){
	case "vdopiaearnings":
	case "myearnings":
		if($_REQUEST['act']==null) {
			$_REQUEST['act']='overallearnings';
		}
		
		if($_REQUEST['start_date']!=''){
			$fromdate = getRequest('start_date',1);
		}else{
			$fromdate='2009-06-01';
		}
		
		if($_REQUEST['end_date']!=''){
			$todate = getRequest('end_date',1);
		}else{
			$todate=date('Y-m-d');
		}
		
		$_REQUEST['start_date']=$fromdate;
		$_REQUEST['end_date']=$todate;
		
		
		switch($_REQUEST['act']) {
			case 'overallearnings':
				
				/*$smarty->assign("monthoptions",$monthoptions);
				$smarty->assign("yearoptions",$yearoptions);
	
				if(!isset($_REQUEST['startmonth']) || !isset($_REQUEST['startyear'] )  || !isset( $_REQUEST['endmonth']) || !isset($_REQUEST['endyear'] ) )
				{
					$lastmonth  = lastNMonths(2);
					$fromdate = $lastmonth[0];
					global $dateformat;
					//$todate = date($dateformat);
					$todate = date('Y-m-d H:i:s');
	
					$_REQUEST['startyear']= date("Y",strtotime($fromdate));
					$_REQUEST['startmonth']=date("m",strtotime($fromdate));
	
					$_REQUEST['endmonth'] = date("m");
					$_REQUEST['endyear'] = date("Y");
	
				}
				else
				{
					$start = begendofmonth_correct($_REQUEST['startmonth'],$_REQUEST['startyear']);
					$end = begendofmonth_correct($_REQUEST['endmonth'],$_REQUEST['endyear']);
					if(timediff($_REQUEST['startmonth'], $_REQUEST['endmonth'], $_REQUEST['startyear'], $_REQUEST['endyear']) < 0){
						$smarty->assign("err" , "Invalid Range selected");
						exit();
					}
					$fromdate = $start[0];
					$todate = $end[1];
					$startmonth=date("M",strtotime($fromdate));
					$endmonth=date("M",strtotime($end[0]));
					
					//$fromdate = $_REQUEST['start_date'];
					//$todate = $_REQUEST['end_date'];
				}*/
	
				$smarty->assign("fromdate", $fromdate);
				$smarty->assign("todate", $todate);
				//$sql = "select sum(pubrevenue)+sum(pubsalesrevenue) as revenues, month(view_date) as month, monthname(view_date) as monthname, year(view_date) as year from channel_campaigns,channels where datediff(view_date , '$fromdate')>=0  and datediff(view_date , '$todate')<=0 and channels.publisher_id = '$_SESSION[PUB_ID]' and channel_campaigns.channel_id = channels.id group by month(view_date), year(view_date) order by year(view_date),month(view_date) asc";
				$smarty->assign("showdomain",$_REQUEST['show_domains']);
				if($_REQUEST['show_domains']!=true) {
					$extable="";
					$excond="";
					$revfields="sum(pubrevenue_advcurrency *ex_rate_advpub)";
					if(isset($_SESSION['PUB_ADV_ID'])) {
						if($_REQUEST['sub']=='vdopiaearnings' ) {
							$extable=",campaign ";
							$excond="and campaign.advertiser_id<>'$_SESSION[PUB_ADV_ID]' and campaign.id=daily_stats.campaign_id ";
						}
						else {
							$revfields="sum(revenue_advcurrency *ex_rate_advpub)";
							$extable=",campaign ";
							$excond="and campaign.advertiser_id='$_SESSION[PUB_ADV_ID]' and campaign.id=daily_stats.campaign_id ";
						}
					}
				$sql="select concat(monthname(date), year(date)
			    	 ), channels.name as name,  $revfields as revenues, ($revfields)/sum(impressions)*1000 as avgcpm , month(date) as month, monthname(date) as monthname, year(date)
			    	 as year,channels.id as id from daily_stats,channels $extable where datediff(date , '$fromdate')>=0
			    	 and datediff(date , '$todate')<=0 and channels.publisher_id = '$_SESSION[PUB_ID]'
			    	 $excond
			    	 and daily_stats.channel_id = channels.id group by channels.id, month(date),
			    	 year(date) order by year(date),month(date) asc";
	
				}
				else{
					$extable="";
					$excond="";
					$revfields="ccmp.impressions/ds.impressions*(ds.pubrevenue_advcurrency*ex_rate_advpub) as rev";
					if(isset($_SESSION['PUB_ADV_ID'])) {
						if($_REQUEST['sub']=='vdopiaearnings' ) {
							$extable="campaign, ";
							$excond="and campaign.advertiser_id<>'$_SESSION[PUB_ADV_ID]' and campaign.id=ccmp.campaign_id ";
						}
						else {
							$revfields="ccmp.impressions/ds.impressions*(ds.revenue_advcurrency*ds.ex_rate_advpub) as rev";
							$extable="campaign, ";
							$excond="and campaign.advertiser_id='$_SESSION[PUB_ADV_ID]' and campaign.id=ccmp.campaign_id ";
						}
					}
	
				$sql="select concat(monthname(date), year(date) ),if(domain='',concat(substring(url,8),'*'),domain) as domain,
				    	  sum(views), sum(revenue_advcurrency *ex_rate_advpub) as revenues, sum(revenue_advcurrency *ex_rate_advpub)/sum(ccmp.impressions)*1000 as avgcpm, month(date) as month,
				    	 monthname(date) as monthname, year(date) as year,channel_id as id from (select channels.url, ccmp.impressions, cc.channel_id, ccmp.domain,
				    	 ds.impressions as views, $revfields , ds.date from $extable channel_campperf as ccmp left join
				    	 daily_stats as ds on ds.channel_id = ccmp.channel_id and ds.date
				    	 = ccmp.date and ds.campaign_id = ccmp.campaign_id inner join channels on channels.id=ccmp.channel_id
				    	 where datediff(date , '$fromdate')>=0
				    	 and datediff(date , '$todate')<=0
				    	 $excond
				    	 and channels.publisher_id = '$_SESSION[PUB_ID]' and ds.date != 'NULL' ) as t1
				    	 group by domain, month(date), year(date) order by year(date),
				    	 month(date) asc";
				}
				
				//echo $sql;
				
				$rs= $reportConn->execute($sql);
				if($rs && $rs->recordcount()>0){
					$data = $rs->getrows();
					$mTotal=array();
					
					foreach ( $data as $key => $val) {
						if(!isset($strdate)) $strdate=$val[0];
						if($val[0]==$strdate){
							$mTotal[$val[0]] += $val['revenues'];
						}
						else {
							$mTotal[$val[0]] += $val['revenues'];
							$strdate=$val[0];
						}
					}
					if($_REQUEST["download"]==true)
					{
						header("Content-type: text/csv");
						header("Content-Disposition: attachment; filename=report.csv");
						if($_REQUEST['show_domains']!=true)
						{
							echo "Month, Channel, Revenue ".$_SESSION[CURRENCYSYM]."";
							$cnt = 3;
							$formattype="channel";
						}
						else
						{
							echo "Month, Domain, Revenue ".$_SESSION[CURRENCYSYM]." \n";
							$cnt = 3;
							$formattype="domain";
						}
						echo "\n";
						for($i=0;$i<count($data);$i++) {
							for ($j=0;$j<$cnt;$j++) {
								if($j>0){
									echo ",";
								}
								if($j==2){
									if($formattype=="domain"){echo "\"".round($data[$i][$j+1],2)."\"";}else{echo "\"".round($data[$i][$j],2)."\"";}
								}else{
									echo "\"".$data[$i][$j]."\"";
								}
							}
							echo "\n";
						}exit;
					}
					else{
						$smarty->assign('data',$data);
						$smarty->assign('mTotal',$mTotal);
					}
				}
	
				break;
	
			case 'monthearnings':
				$smarty->assign("monthoptions",$monthoptions);
				$smarty->assign("yearoptions",$yearoptions);
				if(isset($_REQUEST['channel'])){
	
					$sql1 = "select id,name from channels where publisher_id = '$_SESSION[PUB_ID]'";
					$rs = $conn->execute($sql1);
	
					
					if($rs && $rs->recordcount()>0) {
						$channelids = array();
						while(!$rs->EOF) {
							$channelids[$rs->fields['id']] = $rs->fields['name'];
							$rs->movenext();
						}
						$smarty->assign("channelids",$channelids);
					}
					
					
	
					if(!isset($_REQUEST['startmonth']) || !isset($_REQUEST['startyear'] )  )
					{
	
	
	
						$lastmonth  = lastNMonths(0);
						$fromdate = $lastmonth[0];
						global $dateformat;
						//$todate = date($dateformat);
	
	
						$_REQUEST['startyear']=date("Y",strtotime($fromdate));
						$_REQUEST['startmonth']=date("m",strtotime($fromdate));
	
						$start = begendofmonth_correct($_REQUEST['startmonth'],$_REQUEST['startyear']);
	
						$fromdate = $start[0];
						$todate = date('Y-m-d H:i:s');
						$startdate = date("d",strtotime($fromdate));
						$startmonth=date("m",strtotime($fromdate));
						$startyear=date("y",strtotime($fromdate));
	
	
						$enddate = date("d",strtotime($todate));
						$endmonth=date("m",strtotime($todate));
						$endyear=date("y",strtotime($todate));
	
					}
					else
					{
	
						$start = begendofmonth_correct($_REQUEST['startmonth'],$_REQUEST['startyear']);
	
						$fromdate = $start[0];
						$todate = $start[1];
	
						if($todate > date('Y-m-d') ){
							$todate = date('Y-m-d');
						}
						if($fromdate > date('Y-m-d')){
							$err = "No advertisements have been displayed for this month yet";
							break;
						}
	
	
						$startdate = date("d",strtotime($fromdate));
						$startmonth=date("m",strtotime($fromdate));
						$startyear=date("y",strtotime($fromdate));
	
	
						$enddate = date("d",strtotime($todate));
						$endmonth=date("m",strtotime($todate));
						$endyear=date("y",strtotime($todate));
	
	
					}
					$smarty->assign("fromdate",$fromdate);
					$channelid = $_REQUEST['channel'];
					$smarty->assign("channel",$channelid);
	
	
					$sql1 = "select name from channels where publisher_id = '$_SESSION[PUB_ID]' and id='$channelid'";
					$rs = $conn->execute($sql1);
	
					if($rs && $rs->recordcount()>0) {
						$channelname = $rs->fields['name'];
	
						$smarty->assign("channelname",$channelname);
					}
	
	
	
					$extable="";
					$excond="";
					$revfields="sum(pubrevenue_advcurrency *ex_rate_advpub)";
					if(isset($_SESSION['PUB_ADV_ID'])) {
						if($_REQUEST['sub']=='vdopiaearnings' ) {
							$extable=",campaign ";
							$excond="and campaign.advertiser_id<>'$_SESSION[PUB_ADV_ID]' and campaign.id=daily_stats.campaign_id ";
						}
						else {
							$revfields="sum(revenue_advcurrency *ex_rate_advpub)";
							$extable=",campaign ";
							$excond="and campaign.advertiser_id='$_SESSION[PUB_ADV_ID]' and campaign.id=daily_stats.campaign_id ";
						}
					}
	
					$sql="select 
							$revfields as revenues, 
							date as view_date, 
							if(adtype='preroll' or adtype='postroll' or adtype='midroll', 'video', adtype) as campaign_type, 
							sum(impressions) as views, 
							(($revfields)*1000)/sum(impressions) as ecpm 
						from 
							daily_stats, 
							channels 
							$extable 
						where 
							datediff(date , '$fromdate')>=0  and 
							datediff(date , '$todate')<=0 and 
							channels.publisher_id = '$_SESSION[PUB_ID]' and 
							daily_stats.channel_id = channels.id and 
							channels.id = '$channelid' 
							$excond 
						group by 
							view_date, 
							adtype 
						order by 
							date asc";
									
					$rs= $reportConn->execute($sql);
					if($rs && $rs->recordcount()>0){
						$dailyearnings = $rs->getrows();
	
					}
					$j=0;
					$video_rev =0;
					$video_views = 0;
					$branded_rev =0;
					$branded_views = 0;
					$overlay_rev =0;
					$overlay_views =0;
									
					for($i=0;$i<=Date_Calc::dateDiff($startdate, $startmonth, $startyear, $enddate, $endmonth, $endyear);$i++){
						$tempdate = date('Y-m-d', mktime(0,0,0, $startmonth, $i+1,$startyear));
						while ( ($dailyearnings[$j]['view_date'] == $tempdate) && ($j < count($dailyearnings)) ) {
							if($dailyearnings[$j]['view_date'] == $tempdate){
								$data[$i]['date'] = $tempdate;
								if($dailyearnings[$j]['campaign_type'] == "overlay"){
									$data[$i]['overlayviews'] = $dailyearnings[$j]['views'];
									$data[$i]['overlayrev'] = $dailyearnings[$j]['revenues'];
									$data[$i]['overlayecpm'] = $dailyearnings[$j]['ecpm'];
									$overlay_rev +=$dailyearnings[$j]['revenues'];
									$overlay_views +=  $dailyearnings[$j]['views'];
								}
								if($dailyearnings[$j]['campaign_type'] == "video"){
									$data[$i]['videoviews'] = $dailyearnings[$j]['views'];
									$data[$i]['videorev'] = $dailyearnings[$j]['revenues'];
									$data[$i]['videoecpm'] = $dailyearnings[$j]['ecpm'];
	
									$video_rev +=$dailyearnings[$j]['revenues'];
									$video_views +=  $dailyearnings[$j]['views'];
								}
								if($dailyearnings[$j]['campaign_type'] == "branded"){
									$data[$i]['brandedviews'] = $dailyearnings[$j]['views'];
									$data[$i]['brandedrev'] = $dailyearnings[$j]['revenues'];
									$data[$i]['brandedecpm'] = $dailyearnings[$j]['ecpm'];
									$branded_rev +=$dailyearnings[$j]['revenues'];
									$branded_views +=  $dailyearnings[$j]['views'];
								}
	
								if($data[$i]['overlayrev'] == ""){
									$data[$i]['overlayviews']=0;
									$data[$i]['overlayrev']=0;
									$data[$i]['overlayecpm']=0;
								}
	
								if($data[$i]['videorev'] == ""){
									$data[$i]['videoviews']=0;
									$data[$i]['videorev']=0;
									$data[$i]['videoecpm']=0;
								}
	
								if($data[$i]['brandedrev'] == ""){
									$data[$i]['brandedviews']=0;
									$data[$i]['brandedrev']=0;
									$data[$i]['brandedecpm']=0;
								}
	
								$j++;
	
							}else{
	
								$j++;
							}
						}
						if($data[$i]['date'] == ""){
							$data[$i]['date'] = $tempdate;
							$data[$i]['overlayrev'] = 0;
							$data[$i]['videorev'] = 0;
							$data[$i]['brandedrev'] = 0;
							$data[$i]['overlayviews'] = 0;
							$data[$i]['videoviews'] = 0;
							$data[$i]['brandedviews'] = 0;
							$data[$i]['videoecpm'] = 0;
							$data[$i]['overlayecpm'] = 0;
							$data[$i]['brandedecpm'] = 0;
	
	
						}
					}
	
	
					$smarty->assign('branded_rev',$branded_rev);
					$smarty->assign('overlay_rev',$overlay_rev);
					$smarty->assign('video_rev',$video_rev);
					$smarty->assign('branded_views',$branded_views);
					$smarty->assign('overlay_views',$overlay_views);
					$smarty->assign('video_views',$video_views);
					$smarty->assign('branded_ecpm',($branded_rev*1000)/$branded_views);
					$smarty->assign('overlay_ecpm',($overlay_rev*1000)/$overlay_views);
					$smarty->assign('video_ecpm',($video_rev*1000)/$video_views);
	
	
					$smarty->assign('err',$err);
					$smarty->assign('data',$data);
				}
				break;
	
	
	
			case 'siteearnings':
				if($_REQUEST['action_pubadoptions']!=null){
					/* TODO: ad rules */
					$site['name']=$_REQUEST['name'];
					$site['url']=$_REQUEST['url'];
					$site['apikey'] = md5("".rand(4000000000)."".rand(4000000000)."".rand(4000000000)."".rand(4000000000));
					$api = ", apikey='$site[apikey]'";
					$site['approval']=$_REQUEST['approval'];
					$sql = "insert into channels set name = '$site[name]', url = '$site[url]', approval='$site[approval], publisher_id = '$_SESSION[PUB_ID]'".$api;
					$conn->execute($sql);
					$id = $conn->Insert_ID();
	
					$site['brandedcpm']=$_REQUEST['brandedcpm'];
					$site['videocpm']=$_REQUEST['videocpm'];
					$site['overlaycpm']=$_REQUEST['overlaycpm'];
	
					$sql1 = "select * from publishersheet where  channel_id ='$id' orderby validto DESC";
					$rs = $reportConn->execute($sql1);
					if($rs && $rs->recordcount()>0){
						$lastrecordid = $rs->fields['id'];
						$sql2 = "update publishersheet set validto = now() where id = '$lastrecordid'";
						$rs = $conn->execute($sql2);
					}
	
					$sql = "insert into publishersheet set video_unit_price = '$site[videocpm]', overlay_unit_price = '$site[overlaycpm]', branded_unit_price='$site[brandedcpm]', margin = '$config[defaultmargin]', channel_id=$id, validfrom=now(), validto=now()";
	
					$rs = $conn->execute($sql);
	
	
				}
				break;
		}
		break;
	case "archivedearnings":
		if($_REQUEST['act']==null) {
			$_REQUEST['act']='overallearnings';
		}
		
		if($_REQUEST['start_date']!=''){
			$fromdate = getRequest('start_date',1);
		}else{
			$fromdate='2009-01-01';
		}
		
		if($_REQUEST['end_date']!=''){
			$todate = getRequest('end_date',1);
		}else{
			$todate='2009-05-31';
		}
		
		$_REQUEST['start_date']=$fromdate;
		$_REQUEST['end_date']=$todate;
		
		switch($_REQUEST['act']) {
			case 'overallearnings':
				
				/*$smarty->assign("monthoptions",$monthoptions);
				$smarty->assign("yearoptions",$yearoptions);
	
	
				if(!isset($_REQUEST['startmonth']) || !isset($_REQUEST['startyear'] )  || !isset( $_REQUEST['endmonth']) || !isset($_REQUEST['endyear'] ) )
				{
					$lastmonth  = lastNMonths(2);
					$fromdate = $lastmonth[0];
					global $dateformat;
					//$todate = date($dateformat);
					$todate = date('Y-m-d H:i:s');
	
					$_REQUEST['startyear']=2009; //date("Y",strtotime($fromdate));
					$_REQUEST['startmonth']=01; //date("m",strtotime($fromdate));
	
					$_REQUEST['endmonth'] = 05; //date("m");
					$_REQUEST['endyear'] = 2009; //date("Y");
	
				}
				else
				{
					$start = begendofmonth_correct($_REQUEST['startmonth'],$_REQUEST['startyear']);
					$end = begendofmonth_correct($_REQUEST['endmonth'],$_REQUEST['endyear']);
					if(timediff($_REQUEST['startmonth'], $_REQUEST['endmonth'], $_REQUEST['startyear'], $_REQUEST['endyear']) < 0){
						$smarty->assign("err" , "Invalid Range selected");
						exit();
					}
					$fromdate = $start[0];
					$todate = $end[1];
					$startmonth=date("M",strtotime($fromdate));
					$endmonth=date("M",strtotime($end[0]));
				}*/
	
				$smarty->assign("fromdate", $fromdate);
				$smarty->assign("todate", $todate);
				//$sql = "select sum(pubrevenue)+sum(pubsalesrevenue) as revenues, month(view_date) as month, monthname(view_date) as monthname, year(view_date) as year from channel_campaigns,channels where datediff(view_date , '$fromdate')>=0  and datediff(view_date , '$todate')<=0 and channels.publisher_id = '$_SESSION[PUB_ID]' and channel_campaigns.channel_id = channels.id group by month(view_date), year(view_date) order by year(view_date),month(view_date) asc";
				$smarty->assign("showdomain",$_REQUEST['show_domains']);
				if($_REQUEST['show_domains']!=true) {
					$extable="";
					$excond="";
					$revfields="sum(pubrevenue)+sum(pubsalesrevenue)";
					if(isset($_SESSION['PUB_ADV_ID'])) {
						if($_REQUEST['sub']=='vdopiaearnings' ) {
							$extable=",campaign ";
							$excond="and campaign.advertiser_id<>'$_SESSION[PUB_ADV_ID]' and campaign.id=channel_campaigns.campaign_id ";
						}
						else {
							$revfields="sum(revenue)";
							$extable=",campaign ";
							$excond="and campaign.advertiser_id='$_SESSION[PUB_ADV_ID]' and campaign.id=channel_campaigns.campaign_id ";
						}
					}
				$sql="select concat(monthname(view_date), year(view_date)
			    	 ), channels.name as name,  $revfields as revenues, ($revfields)/sum(views)*1000 as avgcpm , month(view_date) as month, monthname(view_date) as monthname, year(view_date)
			    	 as year,channels.id as id from channel_campaigns,channels $extable where datediff(view_date , '$fromdate')>=0
			    	 and datediff(view_date , '$todate')<=0 and channels.publisher_id = '$_SESSION[PUB_ID]'
			    	 $excond
			    	 and channel_campaigns.channel_id = channels.id group by channels.id, month(view_date),
			    	 year(view_date) order by year(view_date),month(view_date) asc";
	
				}
				else{
					$extable="";
					$excond="";
					$revfields="ccmp.impressions/cc.views*(cc.pubrevenue + cc.pubsalesrevenue) as rev";
					if(isset($_SESSION['PUB_ADV_ID'])) {
						if($_REQUEST['sub']=='vdopiaearnings' ) {
							$extable="campaign, ";
							$excond="and campaign.advertiser_id<>'$_SESSION[PUB_ADV_ID]' and campaign.id=ccmp.campaign_id ";
						}
						else {
							$revfields="ccmp.impressions/cc.views*(cc.revenue) as rev";
							$extable="campaign, ";
							$excond="and campaign.advertiser_id='$_SESSION[PUB_ADV_ID]' and campaign.id=ccmp.campaign_id ";
						}
					}
	
				$sql="select concat(monthname(view_date), year(view_date) ),if(domain='',concat(substring(url,8),'*'),domain) as domain,
				    	  sum(views), sum(rev) as revenues, sum(rev)/sum(impressions)*1000 as avgcpm, month(view_date) as month,
				    	 monthname(view_date) as monthname, year(view_date) as year,channel_id as id from (select channels.url, ccmp.impressions, cc.channel_id, ccmp.domain,
				    	 cc.views as views, $revfields , cc.view_date as view_date from $extable channel_campperf as ccmp left join
				    	 channel_campaigns as cc on cc.channel_id = ccmp.channel_id and cc.view_date
				    	 = ccmp.date and cc.campaign_id = ccmp.campaign_id inner join channels on channels.id=ccmp.channel_id
				    	 where datediff(date , '$fromdate')>=0
				    	 and datediff(date , '$todate')<=0
				    	 $excond
				    	 and channels.publisher_id = '$_SESSION[PUB_ID]' and cc.view_date != 'NULL' ) as t1
				    	 group by domain, month(view_date), year(view_date) order by year(view_date),
				    	 month(view_date) asc";
				}
				
				
				$rs= $reportConn->execute($sql);
				if($rs && $rs->recordcount()>0){
					$data = $rs->getrows();
					$mTotal=array();
					
					foreach ( $data as $key => $val) {
						if(!isset($strdate)) $strdate=$val[0];
						if($val[0]==$strdate){
							$mTotal[$val[0]] += $val['revenues'];
						}
						else {
							$mTotal[$val[0]] += $val['revenues'];
							$strdate=$val[0];
						}
					}
					if($_REQUEST["download"]==true)
					{
						header("Content-type: text/csv");
						header("Content-Disposition: attachment; filename=report.csv");
						if($_REQUEST['show_domains']!=true)
						{
							echo "Month, Channel, Revenue ".$_SESSION[CURRENCYSYM]."";
							$cnt = 3;
							$formattype="channel";
						}
						else
						{
							echo "Month, Domain, Revenue ".$_SESSION[CURRENCYSYM]." \n";
							$cnt = 3;
							$formattype="domain";
						}
						echo "\n";
						for($i=0;$i<count($data);$i++) {
							for ($j=0;$j<$cnt;$j++) {
								if($j>0){
									echo ",";
								}
								if($j==2){
									if($formattype=="domain"){echo "\"".round($data[$i][$j+1],2)."\"";}else{echo "\"".round($data[$i][$j],2)."\"";}
								}else{
									echo "\"".$data[$i][$j]."\"";
								}
							}
							echo "\n";
						}exit;
					}
					else{
						$smarty->assign('data',$data);
						$smarty->assign('mTotal',$mTotal);
					}
				}
	
				break;
	
			case 'monthearnings':
				$smarty->assign("monthoptions",$monthoptions);
				$smarty->assign("yearoptions",$yearoptions);
				if(isset($_REQUEST['channel'])){
	
					$sql1 = "select id,name from channels where publisher_id = '$_SESSION[PUB_ID]'";
					$rs = $reportConn->execute($sql1);
	
					
					if($rs && $rs->recordcount()>0) {
						$channelids = array();
						while(!$rs->EOF) {
							$channelids[$rs->fields['id']] = $rs->fields['name'];
							$rs->movenext();
						}
						$smarty->assign("channelids",$channelids);
					}
					
					
	
					if(!isset($_REQUEST['startmonth']) || !isset($_REQUEST['startyear'] )  )
					{
	
	
	
						$lastmonth  = lastNMonths(0);
						$fromdate = $lastmonth[0];
						global $dateformat;
						//$todate = date($dateformat);
	
	
						$_REQUEST['startyear']=date("Y",strtotime($fromdate));
						$_REQUEST['startmonth']=date("m",strtotime($fromdate));
	
						$start = begendofmonth_correct($_REQUEST['startmonth'],$_REQUEST['startyear']);
	
						$fromdate = $start[0];
						$todate = date('Y-m-d H:i:s');
						$startdate = date("d",strtotime($fromdate));
						$startmonth=date("m",strtotime($fromdate));
						$startyear=date("y",strtotime($fromdate));
	
	
						$enddate = date("d",strtotime($todate));
						$endmonth=date("m",strtotime($todate));
						$endyear=date("y",strtotime($todate));
	
					}
					else
					{
	
						$start = begendofmonth_correct($_REQUEST['startmonth'],$_REQUEST['startyear']);
	
						$fromdate = $start[0];
						$todate = $start[1];
	
						if($todate > date('Y-m-d') ){
							$todate = date('Y-m-d');
						}
						if($fromdate > date('Y-m-d')){
							$err = "No advertisements have been displayed for this month yet";
							break;
						}
	
	
						$startdate = date("d",strtotime($fromdate));
						$startmonth=date("m",strtotime($fromdate));
						$startyear=date("y",strtotime($fromdate));
	
	
						$enddate = date("d",strtotime($todate));
						$endmonth=date("m",strtotime($todate));
						$endyear=date("y",strtotime($todate));
	
	
					}
					$smarty->assign("fromdate",$fromdate);
					$channelid = $_REQUEST['channel'];
					$smarty->assign("channel",$channelid);
	
	
					$sql1 = "select name from channels where publisher_id = '$_SESSION[PUB_ID]' and id='$channelid'";
					$rs = $reportConn->execute($sql1);
	
					if($rs && $rs->recordcount()>0) {
						$channelname = $rs->fields['name'];
	
						$smarty->assign("channelname",$channelname);
					}
	
	
	
					$extable="";
					$excond="";
					$revfields="sum(pubrevenue)+sum(pubsalesrevenue)";
					if(isset($_SESSION['PUB_ADV_ID'])) {
						if($_REQUEST['sub']=='vdopiaearnings' ) {
							$extable=",campaign ";
							$excond="and campaign.advertiser_id<>'$_SESSION[PUB_ADV_ID]' and campaign.id=channel_campaigns.campaign_id ";
						}
						else {
							$revfields="sum(revenue)";
							$extable=",campaign ";
							$excond="and campaign.advertiser_id='$_SESSION[PUB_ADV_ID]' and campaign.id=channel_campaigns.campaign_id ";
						}
					}
	
					$sql="select $revfields as revenues, view_date, campaign_type, sum(views) as views, (($revfields)*1000)/sum(views) as ecpm from channel_campaigns,channels $extable where datediff(view_date , '$fromdate')>=0  and datediff(view_date , '$todate')<=0 and channels.publisher_id = '$_SESSION[PUB_ID]' and channel_campaigns.channel_id = channels.id and channels.id = '$channelid' $excond group by view_date, campaign_type order by view_date asc";
					
					$rs= $reportConn->execute($sql);
					if($rs && $rs->recordcount()>0){
						$dailyearnings = $rs->getrows();
	
					}
					$j=0;
					$video_rev =0;
					$video_views = 0;
					$branded_rev =0;
					$branded_views = 0;
					$overlay_rev =0;
					$overlay_views =0;
					for($i=0;$i<=Date_Calc::dateDiff($startdate, $startmonth, $startyear, $enddate, $endmonth, $endyear);$i++){
						$tempdate = date('Y-m-d', mktime(0,0,0, $startmonth, $i+1,$startyear));
						while ( ($dailyearnings[$j]['view_date'] == $tempdate) && ($j < count($dailyearnings)) ) {
	
	
							if($dailyearnings[$j]['view_date'] == $tempdate){
								$data[$i]['date'] = $tempdate;
								if($dailyearnings[$j]['campaign_type'] == "overlay"){
									$data[$i]['overlayviews'] = $dailyearnings[$j]['views'];
									$data[$i]['overlayrev'] = $dailyearnings[$j]['revenues'];
									$data[$i]['overlayecpm'] = $dailyearnings[$j]['ecpm'];
									$overlay_rev +=$dailyearnings[$j]['revenues'];
									$overlay_views +=  $dailyearnings[$j]['views'];
								}
								if($dailyearnings[$j]['campaign_type'] == "video"){
									$data[$i]['videoviews'] = $dailyearnings[$j]['views'];
									$data[$i]['videorev'] = $dailyearnings[$j]['revenues'];
									$data[$i]['videoecpm'] = $dailyearnings[$j]['ecpm'];
	
									$video_rev +=$dailyearnings[$j]['revenues'];
									$video_views +=  $dailyearnings[$j]['views'];
								}
								if($dailyearnings[$j]['campaign_type'] == "branded"){
									$data[$i]['brandedviews'] = $dailyearnings[$j]['views'];
									$data[$i]['brandedrev'] = $dailyearnings[$j]['revenues'];
									$data[$i]['brandedecpm'] = $dailyearnings[$j]['ecpm'];
									$branded_rev +=$dailyearnings[$j]['revenues'];
									$branded_views +=  $dailyearnings[$j]['views'];
								}
	
								if($data[$i]['overlayrev'] == ""){
									$data[$i]['overlayviews']=0;
									$data[$i]['overlayrev']=0;
									$data[$i]['overlayecpm']=0;
								}
	
								if($data[$i]['videorev'] == ""){
									$data[$i]['videoviews']=0;
									$data[$i]['videorev']=0;
									$data[$i]['videoecpm']=0;
								}
	
								if($data[$i]['brandedrev'] == ""){
									$data[$i]['brandedviews']=0;
									$data[$i]['brandedrev']=0;
									$data[$i]['brandedecpm']=0;
								}
	
								$j++;
	
							}else{
	
								$j++;
							}
						}
						if($data[$i]['date'] == ""){
							$data[$i]['date'] = $tempdate;
							$data[$i]['overlayrev'] = 0;
							$data[$i]['videorev'] = 0;
							$data[$i]['brandedrev'] = 0;
							$data[$i]['overlayviews'] = 0;
							$data[$i]['videoviews'] = 0;
							$data[$i]['brandedviews'] = 0;
							$data[$i]['videoecpm'] = 0;
							$data[$i]['overlayecpm'] = 0;
							$data[$i]['brandedecpm'] = 0;
	
	
						}
					}
	
	
					$smarty->assign('branded_rev',$branded_rev);
					$smarty->assign('overlay_rev',$overlay_rev);
					$smarty->assign('video_rev',$video_rev);
					$smarty->assign('branded_views',$branded_views);
					$smarty->assign('overlay_views',$overlay_views);
					$smarty->assign('video_views',$video_views);
					$smarty->assign('branded_ecpm',($branded_rev*1000)/$branded_views);
					$smarty->assign('overlay_ecpm',($overlay_rev*1000)/$overlay_views);
					$smarty->assign('video_ecpm',($video_rev*1000)/$video_views);
	
	
					$smarty->assign('err',$err);
					$smarty->assign('data',$data);
				}
				break;
	
	
	
			case 'siteearnings':
				if($_REQUEST['action_pubadoptions']!=null){
					/* TODO: ad rules */
					$site['name']=$_REQUEST['name'];
					$site['url']=$_REQUEST['url'];
					$site['apikey'] = md5("".rand(4000000000)."".rand(4000000000)."".rand(4000000000)."".rand(4000000000));
					$api = ", apikey='$site[apikey]'";
					$site['approval']=$_REQUEST['approval'];
					$sql = "insert into channels set name = '$site[name]', url = '$site[url]', approval='$site[approval], publisher_id = '$_SESSION[PUB_ID]'".$api;
					$conn->execute($sql);
					$id = $conn->Insert_ID();
	
					$site['brandedcpm']=$_REQUEST['brandedcpm'];
					$site['videocpm']=$_REQUEST['videocpm'];
					$site['overlaycpm']=$_REQUEST['overlaycpm'];
	
					$sql1 = "select * from publishersheet where  channel_id ='$id' orderby validto DESC";
					$rs = $reportConn->execute($sql1);
					if($rs && $rs->recordcount()>0){
						$lastrecordid = $rs->fields['id'];
						$sql2 = "update publishersheet set validto = now() where id = '$lastrecordid'";
						$rs = $conn->execute($sql2);
					}
	
					$sql = "insert into publishersheet set video_unit_price = '$site[videocpm]', overlay_unit_price = '$site[overlaycpm]', branded_unit_price='$site[brandedcpm]', margin = '$config[defaultmargin]', channel_id=$id, validfrom=now(), validto=now()";
	
					$rs = $conn->execute($sql);
	
	
				}
				break;
		}
		break;			
}
	
?>
