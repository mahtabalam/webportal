<?php
/**
 * 
 * @author abhay
 * This class responsible to insert/update/get records to/from campaign limits
 */
class CampaignLimit{
	private $conn;
	private $smarty;
	
	public function __construct(){
		global $conn, $smarty;
		$this->conn=$conn;
		$this->smarty=$smarty;
	}
	
	public function insert($cid, $campaign){
		# validate campaign_id
		if($cid != (integer)$cid){
			throw new Exception("Invalid campaign id provided.");
		}
		
		# validate campaign info
		if(empty($campaign)){
			throw new Exception("Campaign is empty.");
		}
		
		# Overall campaign limiting
		if(strtolower($campaign['ovrallmaximp'])!="unlimited" || strtolower($campaign['ovralldailyimp'])!="unlimited" || strtolower($campaign['ovrallmaxclk'])!="unlimited" || strtolower($campaign['ovralldailyclk'])!="unlimited"){
			# init sql snips
			$sqlSnip=''; $fldSnip='';
			$sqlSnip="cid=$cid, chid=0";
			
			# Overall maximum impressions
			if(strtolower($campaign['ovrallmaximp'])!="unlimited" && $campaign['ovrallmaximp']>0){
				$sqlSnip.=", maxTotalImps=".$campaign['ovrallmaximp'];
				$fldSnip="maxTotalImps=".$campaign['ovrallmaximp'];
			}else{
				$sqlSnip.=", maxTotalImps=null";
				$fldSnip="maxTotalImps=null";
			}
			
			# Overall daily impressions
			if(strtolower($campaign['ovralldailyimp'])!="unlimited" && $campaign['ovralldailyimp']>0){
				$sqlSnip.=", maxDailyImps=".$campaign['ovralldailyimp'];
				if(strlen($fldSnip)>0) $fldSnip.=", maxDailyImps=".$campaign['ovralldailyimp']; else $fldSnip.=" maxDailyImps=".$campaign['ovralldailyimp'];
			}else{
				$sqlSnip.=", maxDailyImps=null";
				if(strlen($fldSnip)>0) $fldSnip.=", maxDailyImps=null"; else $fldSnip.=" maxDailyImps=null";
			}
			
			# Overall maximum clicks
			if(strtolower($campaign['ovrallmaxclk'])!="unlimited" && $campaign['ovrallmaxclk']>0){
				$sqlSnip.=", maxTotalClicks=".$campaign['ovrallmaxclk'];
				if(strlen($fldSnip)>0) $fldSnip.=", maxTotalClicks=".$campaign['ovrallmaxclk']; else $fldSnip.=" maxTotalClicks=".$campaign['ovrallmaxclk'];
			}else{
				$sqlSnip.=", maxTotalClicks=null";
				if(strlen($fldSnip)>0) $fldSnip.=", maxTotalClicks=null"; else $fldSnip.=" maxTotalClicks=null";
			}
			
			# Overall daily clicks
			if(strtolower($campaign['ovralldailyclk'])!="unlimited" && $campaign['ovralldailyclk']>0){
				$sqlSnip.=", maxDailyClicks=".$campaign['ovralldailyclk'];
				if(strlen($fldSnip)>0) $fldSnip.=", maxDailyClicks=".$campaign['ovralldailyclk']; else $fldSnip.=" maxDailyClicks=".$campaign['ovralldailyclk'];
			}else{
				$sqlSnip.=", maxDailyClicks=null";
				if(strlen($fldSnip)>0) $fldSnip.=", maxDailyClicks=null"; else $fldSnip.=" maxDailyClicks=null";
			}
			
			# insert query
			if(strlen($fldSnip)>0){
				$sql="insert into campaign_limits_rt set $sqlSnip ON DUPLICATE KEY update $fldSnip";
			}else{
				$sql="insert into campaign_limits_rt set $sqlSnip";
			}
			$this->run($sql,'insertupdate');
		}
		
		# filter previous entries
		$sql="select group_concat(chid) as chids from campaign_limits_rt where cid=$cid and chid<>0 group by cid";
		$rows=$this->run($sql, "get");
		if(!empty($rows)){
			$ech=explode(',',$rows[0]['chids']);
			$cch=explode(',',$campaign['cmpgnchannels']);
			$rch=array_diff($ech, $cch);
			$chids=implode(',', $rch);
			$this->run("update campaign_limits_rt set maxTotalImps=null, maxDailyImps=null, maxTotalClicks=null, maxDailyClicks=null  where cid=$cid and chid in($chids)", 'delete');
		}
				
		# Channel wise campaign limiting
		foreach($campaign['cmpgnchannel'] as $key => $chid){
			if(strtolower($campaign['maximp'][$key])!="unlimited" || strtolower($campaign['dailyimp'][$key])!="unlimited" || strtolower($campaign['maxclk'][$key])!="unlimited" || strtolower($campaign['dailyclk'][$key])!="unlimited"){
				# init sql snips
				$sqlSnip=''; $fldSnip='';
				$sqlSnip="cid=$cid, chid=$chid";
				
				# Channel maximum impressions
				if(strtolower($campaign['maximp'][$key])!="unlimited" && $campaign['maximp'][$key]>0){
					$sqlSnip.=", maxTotalImps=".$campaign['maximp'][$key];
					$fldSnip="maxTotalImps=".$campaign['maximp'][$key];
				}else{
					$sqlSnip.=", maxTotalImps=null";
					$fldSnip="maxTotalImps=null";
				}
				
				# Channel daily impressions
				if(strtolower($campaign['dailyimp'][$key])!="unlimited" && $campaign['dailyimp'][$key]>0){
					$sqlSnip.=", maxDailyImps=".$campaign['dailyimp'][$key];
					if(strlen($fldSnip)>0) $fldSnip.=", maxDailyImps=".$campaign['dailyimp'][$key]; else $fldSnip.=" maxDailyImps=".$campaign['dailyimp'][$key];
				}else{
					$sqlSnip.=", maxDailyImps=null";
					if(strlen($fldSnip)>0) $fldSnip.=", maxDailyImps=null"; else $fldSnip.=" maxDailyImps=null";
				}
				
				# Channel maximum clicks
				if(strtolower($campaign['maxclk'][$key])!="unlimited" && $campaign['maxclk'][$key]>0){
					$sqlSnip.=", maxTotalClicks=".$campaign['maxclk'][$key];
					if(strlen($fldSnip)>0) $fldSnip.=", maxTotalClicks=".$campaign['maxclk'][$key]; else $fldSnip.=" maxTotalClicks=".$campaign['maxclk'][$key];
				}else{
					$sqlSnip.=", maxTotalClicks=null";
					if(strlen($fldSnip)>0) $fldSnip.=", maxTotalClicks=null"; else $fldSnip.=" maxTotalClicks=null";
				}
				
				# Channel daily clicks
				if(strtolower($campaign['dailyclk'][$key])!="unlimited" && $campaign['dailyclk'][$key]>0){
					$sqlSnip.=", maxDailyClicks=".$campaign['dailyclk'][$key];
					if(strlen($fldSnip)>0) $fldSnip.=", maxDailyClicks=".$campaign['dailyclk'][$key]; else $fldSnip.=" maxDailyClicks=".$campaign['dailyclk'][$key];
				}else{
					$sqlSnip.=", maxDailyClicks=null";
					if(strlen($fldSnip)>0) $fldSnip.=", maxDailyClicks=null"; else $fldSnip.=" maxDailyClicks=null";
				}
				
				# insert query
				if(strlen($fldSnip)>0){
					$sql="insert into campaign_limits_rt set $sqlSnip ON DUPLICATE KEY update $fldSnip";
				}else{
					$sql="insert into campaign_limits_rt set $sqlSnip";
				}
				$this->run($sql,'insertupdate');
			}
		}
	}
	
	
	public function get($cid, $campaign){
		# validate campaign id
		if($cid != (integer)$cid){
			throw new Exception("Invalid campaign id provided.");
		}
		
		# validate campaign info
		if(empty($campaign)){
			throw new Exception("Campaign is empty.");
		}
		
		# select rows from campaign limits
		$sql="select * from campaign_limits_rt where cid=$cid";
		$rows = $this->run($sql,'get');
		if(!empty($rows)){
			$k=0;
			$campaign['advanced_schedule']="true";
			foreach($rows as $key => $values){
				if($values['chid']==0){
					if($values['maxTotalImps']!="") $campaign['ovrallmaximp']=$values['maxTotalImps']; else $campaign['ovrallmaximp']="Unlimited";
					if($values['maxDailyImps']!="") $campaign['ovralldailyimp']=$values['maxDailyImps']; else $campaign['ovralldailyimp']="Unlimited";
					if($values['maxTotalClicks']!="") $campaign['ovrallmaxclk']=$values['maxTotalClicks']; else $campaign['ovrallmaxclk']="Unlimited";
					if($values['maxDailyClicks']!="") $campaign['ovralldailyclk']=$values['maxDailyClicks']; else $campaign['ovralldailyclk']="Unlimited";
				}else{
					$campaign['cmpgnchannel'][$k]=$values['chid'];
					$campaign['cmpgnchannels'].=$values['chid'].',';
					if($values['maxTotalImps']!="") $campaign['maximp'][$k]=$values['maxTotalImps']; else $campaign['maximp'][$k]="Unlimited";
					if($values['maxDailyImps']!="") $campaign['dailyimp'][$k]=$values['maxDailyImps']; else $campaign['dailyimp'][$k]="Unlimited";
					if($values['maxTotalClicks']!="") $campaign['maxclk'][$k]=$values['maxTotalClicks']; else $campaign['maxclk'][$k]="Unlimited";
					if($values['maxDailyClicks']!="") $campaign['dailyclk'][$k]=$values['maxDailyClicks']; else $campaign['dailyclk'][$k]="Unlimited";
					$k++;
				}
			}
			$campaign['cmpgnchannels']=trim($campaign['cmpgnchannels'],',');
		}
	}
	
	private function run($sql, $action){
		switch($action){
			case 'insertupdate':
				$rs=$this->conn->execute($sql);
				if(!$rs) 
					throw new Exception("InsertUpdate operation failed.");
				break;
			case 'get':
				$rs=$this->conn->execute($sql);
				if(!$rs) 
					throw new Exception("Get operation failed.");
				else
					return $rs->getrows();
				break;
			case 'delete':
				$this->conn->execute($sql);
				break;	
			default:
				throw new Exception("Invalid argument.");
				break;		
		}
	}
}

?>
