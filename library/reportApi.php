<?php
//error_reporting(E_ERROR);
include_once dirname(__FILE__) . '/../include/config.php';
include_once dirname(__FILE__) .'/../common/include/function.php';
error_reporting(0);
include_once dirname(__FILE__) .'/../common/portal/report/reportApp/lib/ReportRequestHandler.class.php';
include_once dirname(__FILE__) .'/../common/portal/report/reportApp/lib/ReportApiUrlReader.class.php';
include_once dirname(__FILE__) .'/../common/portal/report/reportApp/lib/ReportLog.class.php';
include_once dirname(__FILE__) .'/../common/portal/report/reportApp/lib/Cache.class.php';

$cache=Cache::getInstance();

$debugLevel = isset($_GET['debug']) ? "DEBUG" : "ERROR";

if($debugLevel=="ERROR")
	if($cache->get('DEBUG'))
		$debugLevel="DEBUG";
define("REPORT_LOG_LEVEL", $debugLevel);
chkUserLogged();
//var_dump(REPORT_LOG_LEVEL);die;


$logger = ReportLog::getInstance();        
try {        
$urlReader = new ReportApiUrlReader();
$input = $urlReader->extractInput();
$logger->logDebug("STARTING THE REPORT MODULE");
$logger->logDebug("\n INPUT:  ". json_encode($input)  ."\n\n\n\n");
$requestHandler = new ReportRequestHandler($input);
header( "HTTP/1.1 200 OK" );
$response = $requestHandler->process();
echo json_encode($response);
$logger->logDebug("REPONSE SENT SUCCESSFULLY, ROWS: " . $response['meta']['count']);

}catch(Exception $e){
    $logger->logError("Some error has occoured", $e);
    header('HTTP/1.1 500 Internal Server Error');
} 


function chkUserLogged(){
    
    if(isset($_SESSION['ADV_ID'])){
            $_REQUEST["advertiser_id"] = $_SESSION['ADV_ID'];
    }
    /**
     * @todo: remove this !!
     */
    //$_REQUEST["advertiser_id"] = '185';
}