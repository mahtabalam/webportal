<?php
include_once('include/datefuncs.php');
include_once('include/reportLib.php');
include_once('include/function.php');

//echo "<pre>"; print_r($_REQUEST);

switch ($_REQUEST['sub']) {
	case "advusernewreport":
	//=================================================================================
	if($_REQUEST['excelreport']=="yes"){
		$blank[]=array("");
		if($_SESSION['period']=="summary"){
			$colHead =array();
			$colHead[]=ucfirst($_SESSION['reporttype']);
			foreach ($_SESSION['SelectedField'] as $key => $val){
				$colHead[] = $val;
			}
			adv_exportToExcel($_SESSION['header'],$colHead,$_SESSION['data'],$blank,$_SESSION['period'],$_SESSION['SelectedField'],$_SESSION['totalArr']);
		}
		else{
			$colHead =array();
			$colHead[]="Date";
			foreach ($_SESSION['SelectedField'] as $key => $val){
				$colHead[] = $val;
			}
			adv_exportToExcel($_SESSION['header'],$colHead,$_SESSION['data'],$blank,$_SESSION['period'], $_SESSION['SelectedField'], $_SESSION['totalArr']);
		}
	}
	elseif ($_REQUEST['pdfreport']=="yes"){
		$blank[]=array("");
		$colWidth=array(55,40,35,35);
		if($_SESSION['period']=="summary"){
			$colHead[]=ucfirst($_SESSION['reporttype']);
			foreach ($_SESSION['SelectedField'] as $key => $val){
				$colHead[] = $val;
			}adv_exportToPdf($_SESSION['header'],$colHead,$_SESSION['data'],$_SESSION['period'],$_SESSION['SelectedField'], $_SESSION['totalArr']);
		}
		else{
			$colHead[]="Date";
			foreach ($_SESSION['SelectedField'] as $key => $val){
				$colHead[] = $val;
			}
			adv_exportToPdf($_SESSION['header'],$colHead,$_SESSION['data'],$_SESSION['period'],$_SESSION['SelectedField'], $_SESSION['totalArr']);
		}
	}
	else{
		$_SESSION['currentReport']=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$smarty->assign('timeperiodoptions',$timeperiodoptions);
		if(!isset($_REQUEST['dropdown'])){
			$_REQUEST['time']= "dropdown";
		}
		if(!isset($_REQUEST['timeperiod'])){
			$_REQUEST['timeperiod']= "yesterday";
		}

//	    $channellist = array();
	   	
	    //$sql = "select id,name from channels where publisher_id='$_SESSION[PUB_ID]'";
//		$sql = "select channels.name, channels.id from channels left join daily_stats on channels.id=channel_id left join adv_user_campaign on adv_user_campaign.campaign_id=daily_stats.campaign_id where channel_type!='iphone' and adv_user_campaign.adv_user_id='$_SESSION[ADV_USER_ID]' group by daily_stats.channel_id order by channels.name";
//	    $rs = $reportConn->execute($sql);
//	    if($rs && $rs->recordcount()>0){
//	    	$channellist = $rs->getrows();			    	
//	    }
//	    $smarty->assign("channellist",$channellist);
	    //$smarty->assign("reporttype", array("category" => "Category Performance Report","campaign" => "Campaign Performance Report", "channel" => "Channel Performance Report", "country" => "Country Performance Report", "state" => "State Performance Report"));
		//$smarty->assign("reporttype", array("campaign" => "Campaign Performance Report", "channel" => "Channel Performance Report", "country" => "Country Performance Report", "state" => "State Performance Report", "ad" => "Ad Performance Report"));
    //Dont know for how long but xylo campaign channel performance report is removed
    
    $report_type=array(
			"campaign" => "Campaign Performance Report" , 
//			"channel" => "Channel Performance Report", 
//			"country" => "Country Performance Report", 
//			"state" => "State Performance Report", 
			"ad" => "Ad Performance Report");

			if(in_array("r.channel_reporting", $_SESSION['CAPABILITIES'])){
			$report_type['channel']="Channel Performance Report";
			}
			
		$smarty->assign("reporttype",$report_type );
	    
//	    $country=$_GET['country'];
//		$state=$_GET['state'];
//		$smarty->assign("countrycodes", getCountryList());
//	    $smarty->assign("statecodes", getStateList($country));
	    $smarty->assign('country',$country);
	    $smarty->assign('state',$state);
	    
	    $campaignlist = array();
	   
	   //  $sql = "select name, id from campaign where advertiser_id='$_SESSION[ADV_ID]'";
	  	$sql = "select campaign_id as id, campaign.name as name from adv_user_campaign left join campaign on adv_user_campaign.campaign_id = campaign.id where adv_user_id = '$_SESSION[ADV_USER_ID]' and campaign.device!='iphone' order by campaign.name";
	    
	    $rs = $reportConn->execute($sql);
	    if($rs && $rs->recordcount()>0){
	    	$campaignlist = $rs->getrows();    	
	    }
	 
	   
	    $smarty->assign("campaignlist",$campaignlist);
	    if(!isset($_REQUEST['adtype']))$_REQUEST['adtype']='all';
	    if(!isset($_REQUEST['reporttype']))$_REQUEST['reporttype']='campaign';
	    if(!isset($_REQUEST['channel']))$_REQUEST['channel']='all';
	    if(!isset($_REQUEST['campaign']))$_REQUEST['campaign']='all';
//	    if(!isset($_REQUEST['country']))$_REQUEST['country']='all';
//	    if(!isset($_REQUEST['state']))$_REQUEST['state']='all';
	    if(!isset($_REQUEST['period']))$_REQUEST['period']='summary';
		
	    $adtypeoptions = array(
//			array('id'=>'all', 'name' => "All"),
			array('id'=>'video', "name"=>"Video"),
			array('id'=>'branded', 'name' => "Branded Player"),
			array('id'=>'overlay' , 'name' => "Overlay"),
			
//			array('id'=>'copreroll' , 'name' => "Companion Preroll"),
//			array('id'=>'comidroll' , 'name' => "Companion Midroll"),
//			array('id'=>'copostroll' , 'name' => "Companion Postroll")
            array('id'=>'tracker' , 'name' => "Tracker")
		);
		
		
		//========================================
			$fieldlistoptions = array('totimpressions' => "Impression",
				'totclicks'=>"Click",
				'ctr'  => "CTR",
				"talk2me" => "Talk2Me"
			);
			if(in_array('r.cpcvcolumn', $_SESSION['CAPABILITIES'])){
				$fieldlistoptions['cpcv'] 	= 'Complete';
				$fieldlistoptions['cr'] 	= 'CR';
				
			}

		/*
				'uniques' => "Uniques",
				'avgfrequency'  => "Avg Frequency"
		*/	
		$smarty->assign("fieldlistoptions",$fieldlistoptions);
	    //========================================
	    
		$smarty->assign("adtypeoptions",$adtypeoptions);
		$smarty->assign("ddoptions",$ddoptions);
		$smarty->assign("pubreport","true");

		
		$reporttype=getRequest('reporttype');
		$channel= getRequest('channel');
		$campaign = getRequest('campaign');
		$adtype = getRequest('adtype');
//		$country= getRequest('country');
//		$state= getRequest('state');
		$period = getRequest('period');
				
		if(!$_REQUEST['start_date']){
	        $tm=thismonth(); //default date range is this month
	        $fromdate=$tm[0];
	        $todate=$tm[1];
                               
	 		$_REQUEST['start_date']=$fromdate;
			$_REQUEST['end_date']=$todate;
			$startdate = $fromdate;
			$enddate = $todate;
		}
		
		$startdate = getRequest('start_date');
		$enddate = getRequest('end_date');
		
		
		$campaignArr['campaign'] =  $campaign;
		$channelArr['channel'] =  $channel;
//		$countryArr['cat'] =  $country;
//		$stateArr['state'] =  "$country:$state";
		//$stateArr['count'] =  $country;
//		if($country!="all")
//			$countryName = insert_country_names($countryArr);
//		else
//			$countryName = "All";
//		
//		if($state=="all" || $state == "")
//			$stateName = "All";
//		else
//			$stateName = insert_state_names($stateArr);
					
		$campaignName = insert_campaign_names($campaignArr,'excel');
		$channelName = insert_channel_names($channelArr,'excel');
		
		
		$report_header=array();
		$report_header[]=array(ucfirst($reporttype)." Performance Report(" . ucfirst($period) . ")");
		$report_header[]=array('Report Generated on', date('d-m-Y H:i T'));
		$report_header[]=array("Start Date",$startdate);
		$report_header[]=array("End Date",$enddate);
		$report_header[]=array("Channel",ucfirst($channelName));
		$report_header[]=array("Campaign",ucfirst($campaignName));
//		$report_header[]=array("Country",ucfirst($countryName));
//		$report_header[]=array("State",ucfirst($stateName));
		$report_header[]=array("Ad Format",ucfirst($adtype));
		$_SESSION['header'] = $report_header;
			
		
	    if($reporttype=="category")
			$dateField ="hour";
		else
			$dateField ="date";
   		if(isset($startdate) && isset($enddate)) {
			$datequery=" and $dateField>='$startdate' and $dateField<='$enddate'";
		} 
		else{
			$datequery="";
		}
				
//		if($channel=="all")
//		{
//			$sitestring=" ";
//		}
//		else{
//			$sitestring=" and channel_id='$channel'";
//		}
				
		if($campaign=="all"){
			$campaignstring=" ";
			$campaignstringU = " ";
		}else{
			$campaignstring=" and daily_stats.campaign_id='$campaign'";
			$campaignstringU=" and aggregate_ads.campaign_id='$campaign' and campaign.id='$campaign'";
		}
		
		if($adtype=="all")
		{
			$adtypestring=" ";
			
		}else{
		  if($adtype=='video'){
			$adtypestring=" and (adtype='preroll' or adtype='midroll' or adtype='postroll' or adtype='copreroll' or adtype='comidroll' or adtype='copostroll')";
		  }else if($adtype=='tracker'){
		  	$adtypestring=" and (adtype='tracker' or adtype='vdobanner' or adtype='banner')";
		  }else{
		    $adtypestring=" and adtype='$adtype'";
		  }
		}
		
//		if($country=="all"){
//			$countrystring=" ";
//		}else{
//			$countrystring=" and ccode='$country'";
//		}
		
//		if($state=="all"){
//			$provincestring=" ";
//		}else{
//			$provincestring=" and state_code='$state'";
//		}
			switch ($period){
			case "daily":
				$group = " group by ";
				$aggregate=" day($dateField), month($dateField), year($dateField) ";
				$aggregateNames=", $dateField as timeframe ";
				$agcolNames="Date,";
				$orderby=" $dateField";
				$measure = "and measure='duniqvi' ";
				break;
			case "monthly":
				$group = " group by ";
				$aggregate=" month($dateField),year($dateField) ";
				$aggregateNames=", concat_ws(' ', monthname($dateField), year($dateField)) as timeframe ";
				$agcolNames="Timeframe,";
				$orderby=" month($dateField)";
				$measure = "and measure='muniqvi' ";
				break;
			case "quarterly":
				$group = " group by ";
				$aggregate=" quarter($dateField),year($dateField) ";
				$aggregateNames=", concat_ws(' ', concat('Q',quarter($dateField)), year($dateField)) as timeframe ";
				$agcolNames="Timeframe,";
				$orderby=" quarter($dateField)";
				break;
			case "dayofweek":
				$group = " group by ";
				$aggregate=" dayofweek($dateField), year($dateField) ";
				$aggregateNames=", date_format($dateField, '%a') as timeframe ";
				$agcolNames="Day,";
				$orderby=" dayofweek($dateField)";
				break;
			case "weekly":
				$group = " group by ";
				$aggregate=" week($dateField), year($dateField) ";
				$aggregateNames=", concat('Week','-',week($dateField)+1) as timeframe ";
				$agcolNames="Week,";
				$orderby=" week($dateField)";
				$measure = "and measure='wuniqvi' ";
				break;
			case "yearly":
				$group = " group by ";
				$aggregate=" year($dateField) ";
				$aggregateNames=", year($dateField) as timeframe ";
				$agcolNames="Year,";
				$orderby=" year($dateField)";
				break;
			case "summary":
				$group = " group by ";
				$aggregateNames=", 'Summary' as timeframe ";
				$aggregateNames="";
				$orderby="";
				break;
			default:
				$aggregate=" ";
				$aggregateNames="";
				$agcolNames="";
				$orderby="";
				break;
		}
		
		$impr="if(daily_stats.adtype not like 'co%', impressions, 0)";
		$clkr="clicks";
			
		//==========================================================================
			if($_REQUEST['fieldlist']==""){
				$_REQUEST['fieldlist'] = array('totimpressions','totclicks','ctr');
				if(in_array('r.cpcvcolumn', $_SESSION['CAPABILITIES'])){
					$_REQUEST['fieldlist'][] = 'cpcv';
					$_REQUEST['fieldlist'][] = 'cr';
				}
			}		
				
			$SelectedField =array(); 
			if($reporttype =="ad" )
				$SelectedField['adname'] = "Ad Name";
				
			foreach ($_REQUEST['fieldlist'] as $key => $val){
				$SelectedField[$val] = $fieldlistoptions[$val];
			}
			//echo "<pre>";
			//print_r($_REQUEST['fieldlist']);
			/*
			echo "<pre>";
			print_r($_REQUEST['fieldlist']);
			
			echo "<pre>";
			print_r($SelectedField);*/
			//==========================================================================
		switch($reporttype){
			/*case "category":
				if($channel!="all")
					$where_condition.= "and (cm.channel_choice='' or find_in_set($channel,cm.channel_choice)) ";
				if($campaign!="all")
					$where_condition.= "and agc.campaign_id=$campaign ";
				if($adtype!="all")
					$where_condition.= "and ad_format='$adtype' ";
				if($country!="all")
					$where_condition.= "and (cm.geography is null or find_in_set('$country',cm.geography)) ";
				if($state!="all")
				{
					$geostr = "$country".":"."$state";
					$where_condition.= "and (cm.geography is null or find_in_set('$geostr',cm.geography))";
				}	
				$select ="select sum(agc.impressions) as totimpressions, 
		   				sum(agc.clicks) as totclicks, 
		   				(sum(agc.clicks)*100)/sum(agc.impressions) as ctr"; ;
				$from=" from aggregate_category as agc";
				$left_join =" left join campaign as cm on agc.campaign_id=cm.id left join ads on ads.id=agc.ad_id"; 
				$where=" 1 $where_condition"; 
				$group_condition="agc.category";
				$aggregateNames.=", agc.category as name ";
				break;*/
			case "channel":
				$select = "select 
		   				sum($impr) as totimpressions, 
		   				sum(clicks) as totclicks, 
		   				round((sum(clicks)*100)/sum($impr),2) as ctr";  
				$from="from daily_stats";
				$left_join=" left join channels on daily_stats.channel_id=channels.id left join adv_user_campaign on adv_user_campaign.campaign_id = daily_stats.campaign_id left join campaign on daily_stats.campaign_id=campaign.id left join ads on ads.id=daily_stats.ad_id";
				$group_condition=" daily_stats.channel_id";
				$where="adv_user_campaign.adv_user_id = '$_SESSION[ADV_USER_ID]' and campaign.device!='iphone' and ads.id IS NOT NULL and ads.parent_ad_id IS NULL";
				$aggregateNames.=", channels.name ";
				if($orderby!="")
						$orderby="name, ".$orderby;
					else
						$orderby="name";
				break;
			case "campaign":
				$select = "select 
		   				sum($impr) as totimpressions, 
		   				sum(clicks) as totclicks, 
		   				round((sum(clicks)*100)/sum($impr),2) as ctr";
				$completeCond = "";
				if(in_array('r.cpcvcolumn', $_SESSION['CAPABILITIES']) && in_array('cpcv',$_REQUEST['fieldlist'])){
					$completeCond = ", sum(completes) as cpcv ";
				}
				if(in_array('r.cpcvcolumn', $_SESSION['CAPABILITIES']) && in_array('cr',$_REQUEST['fieldlist'])){
					$completeCond .= ", round(((sum(completes)/sum($impr))*100),2) as cr ";
				}
				$select .= $completeCond;
				$from="from daily_stats";
				$left_join=" left join campaign on daily_stats.campaign_id=campaign.id left join adv_user_campaign on adv_user_campaign.campaign_id = daily_stats.campaign_id left join ads on ads.id=daily_stats.ad_id";
				$group_condition=" daily_stats.campaign_id";
				$where="adv_user_campaign.adv_user_id = '$_SESSION[ADV_USER_ID]' and campaign.device!='iphone' and ads.id IS NOT NULL and ads.parent_ad_id IS NULL";
				$aggregateNamesSRI=$aggregateNames.",daily_stats.ad_id as sriid";
				$aggregateNames.=", campaign.name,campaign.id ";
//				if($sitestring==" ")
//					$channelCampaignsql = "and aggregate_ads.channel_id=0";
//				else
//					$channelCampaignsql = "and aggregate_ads.channel_id='$channel'";
					
				$whereUniques=" campaign.id=adv_user_campaign.campaign_id and campaign.id=aggregate_ads.campaign_id and aggregate_ads.ad_id=0 $channelCampaignsql and adv_user_campaign.adv_user_id = '$_SESSION[ADV_USER_ID]' and campaign.device!='iphone'";
				if($orderby!="")
							$sriorderby=" sriid,".$orderby;
					else
						$sriorderby=" sriid";
				if($orderby!="")
						$orderby="name, ".$orderby;
					else
						$orderby="name";
				$fromUniques = "from aggregate_ads, campaign, adv_user_campaign";
				$aggregateNamesU=$aggregateNames;
				
					
				
				break;
			/*case "country":
				$select = "select 
		   				sum($impr) as totimpressions, 
		   				sum(clicks) as totclicks, 
		   				round((sum(clicks)*100)/sum($impr),2) as ctr"; 
				$from="from daily_stats";
				$left_join=" left join campaign on daily_stats.campaign_id=campaign.id left join adv_user_campaign on adv_user_campaign.campaign_id = daily_stats.campaign_id left join ads on ads.id=daily_stats.ad_id";
				$group_condition=" daily_stats.ccode";
				$where="adv_user_campaign.adv_user_id = '$_SESSION[ADV_USER_ID]' and campaign.device!='iphone' and ads.id IS NOT NULL and ads.parent_ad_id IS NULL";
				$aggregateNames.=", if(daily_stats.ccode='','Other',daily_stats.ccode) as name ";
				if($orderby!="")
						$orderby="name, ".$orderby;
					else
						$orderby="name";
				break;
			case "state":
				$select = "select 
		   				sum($impr) as totimpressions, 
		   				sum(clicks) as totclicks, 
		   				round((sum(clicks)*100)/sum($impr),2) as ctr"; 
				$from="from daily_stats";
				$left_join=" left join campaign on daily_stats.campaign_id=campaign.id left join adv_user_campaign on adv_user_campaign.campaign_id = daily_stats.campaign_id left join ads on ads.id=daily_stats.ad_id";
				$group_condition=" daily_stats.state_code";
				$where="adv_user_campaign.adv_user_id = '$_SESSION[ADV_USER_ID]' and campaign.device!='iphone' and ads.id IS NOT NULL and ads.parent_ad_id IS NULL";
				$aggregateNames.=", if(daily_stats.state_code='','Other',daily_stats.state_code) as name ";
				if($orderby!="")
						$orderby="name, ".$orderby;
					else
						$orderby="name";
				$smarty->assign("ctn",$country);
				break;*/
			case "ad":
				$select = "	select 
		   						sum($impr) as totimpressions, 
			   					sum($clkr) as totclicks,
			   					round((sum(clicks)*100)/sum($impr),2) as ctr"; 
				$completeCond = "";
				if(in_array('r.cpcvcolumn', $_SESSION['CAPABILITIES']) && in_array('cpcv',$_REQUEST['fieldlist'])){
					$completeCond = ", sum(completes) as cpcv ";
				}
				if(in_array('r.cpcvcolumn', $_SESSION['CAPABILITIES']) && in_array('cr',$_REQUEST['fieldlist'])){
					$completeCond .= ", round(((sum(completes)/sum($impr))*100),2) as cr ";
				}
				$select .= $completeCond;
				$from="from daily_stats";
				$left_join="left join campaign on campaign.id=daily_stats.campaign_id left join adv_user_campaign on adv_user_campaign.campaign_id = daily_stats.campaign_id left join ads on ads.id=daily_stats.ad_id";
				$group_condition=" daily_stats.campaign_id, daily_stats.ad_id";
				$where="adv_user_campaign.adv_user_id = '$_SESSION[ADV_USER_ID]' and campaign.device!='iphone' and ads.id IS NOT NULL and ads.parent_ad_id IS NULL";
				$aggregateNamesSRI=$aggregateNames.",daily_stats.ad_id as sriid";
				$aggregateNames.=",ads.id, if(ads.branded_img_right_ref_imgname IS NOT NULL, ads.branded_img_right_ref_imgname,'Other') as adname, campaign.name as name";
				
				
				if($sitestring==" ")
						$channelCampaignsql = "and aggregate_ads.channel_id=0";
					else
						$channelCampaignsql = "and aggregate_ads.channel_id='$channel'";
				
				$whereUniques="ads.id=ad_id and aggregate_ads.campaign_id = campaign.id and aggregate_ads.campaign_id!=0 and aggregate_ads.ad_id!=0 $channelCampaignsql and adv_user_campaign.campaign_id=campaign.id and adv_user_campaign.adv_user_id = '$_SESSION[ADV_USER_ID]' and campaign.device!='iphone'";
				
				
				$fromUniques = "from aggregate_ads, ads, campaign, adv_user_campaign";
				$aggregateNamesU.=$aggregateNames;
				
				if($orderby!="")
							$sriorderby=" sriid,".$orderby;
					else
						$sriorderby=" sriid";
				if($orderby!="")
						$orderby=" name, adname, ".$orderby;
				else
					$orderby=" name, adname ";
				
				break;
			default:
				$left_join="";
				$group_condition="";
				$where="";
				$aggregateNames="";
				break;
		}
		
			
		
		if($period == "summary"){
			$sql = "$select $aggregateNames $from $left_join where $where 
		   				$sitestring $campaignstring $adtypestring 
		   				$countrystring $provincestring $datequery  
		   				$group  $aggregate $group_condition
		   			Order By $orderby";
		}
		else{
	    	$sql = "$select $aggregateNames $from $left_join where $where 
		   				$sitestring $campaignstring $adtypestring 
		   				$countrystring $provincestring $datequery  
		   				$group $aggregate, $group_condition
		   			Order By $orderby";	
			
		}
		
		//echo "<br>". $sql;
		
		
		if(isset($reporttype)){
			    
				$rs = $reportConn->execute($sql);		
		}
	
	
		if($rs && $rs->recordcount()>0){
			$summary = $rs->getrows();
		}
		$tarray =array();
			if(in_array("Talk2Me", $SelectedField)){
				foreach ($SelectedField as $key => $val){
					if($key!="talk2me")
						$tarray[$key] = $val;
				}
				$SelectedField = array();
				$SelectedField = $tarray;
				$flagTalk2Me =true;
			}
			else 
				$flagTalk2Me = false;
			//===========Talk2ME Reporting===================================
			if($flagTalk2Me){
				$sri_elements = array();
				$campaign_sri_header = array();
				if($reporttype=="ad"){
					$i = 0;
					foreach ($summary as $kad ){
						$tcid = $kad['cid'];
						$sqlSri = "select * from ads,campaign_members where id=ad_id and parent_ad_id=$kad[id] and event='sri_action_init'";
						$rsSri = $reportConn->execute($sqlSri);
						$summarySri = array();
						if($rsSri && $rsSri->recordcount()>0){
							$summarySri = $rsSri->getrows();
							foreach ($summarySri as $srikey => $srival){
								$sri_details = $srival['sri_details'];
								$sri_id = $srival['id'];
								$obj	=	json_decode($sri_details);
								//$el_name 	=	trim($obj->el_name);
								$el_name 	=	$srival['branded_img_right_ref_imgname'];
								if(trim($el_name))
									$SriHeader[] = trim($el_name);
								$campaign_sri_header[] = $el_name;
								//==========sri Number for sri element=============
								if($period == "summary"){
									$sql = "select sum(clicks) as sriclk $aggregateNamesSRI
										from daily_stats where ad_id=$sri_id $sitestring $campaignstring $adtypestring 
				   						$countrystring $provincestring $datequery  
				   						$group sriid $aggregate
				   						Order By $sriorderby";
								}
								else{
							    	$sql = "select sum(clicks) as sriclk $aggregateNamesSRI
										from daily_stats where ad_id=$sri_id $sitestring $campaignstring $adtypestring 
				   						$countrystring $provincestring $datequery  
				   						$group sriid,  $aggregate
				   						Order By $sriorderby";	
									}
								//echo "<br><br><br>".$sql;
								$el_nameSumamry = array();
								$rs = $reportConn->execute($sql);
								if($rs && $rs->recordcount()>0){
									$el_nameSumamry = $rs->getrows();
								}
								foreach ($summary as $key =>$val){
									foreach ($el_nameSumamry as $skey => $sval){
										if($val['timeframe']==$sval['timeframe'] && $val['id']==$kad[id]){
											$summary[$key][$el_name] = $sval['sriclk'];
										}
									}
								}
								//=================================================
							}
						}else
						$i++;
					}
					$SriHeader = array_unique($SriHeader);
					foreach ($SriHeader as $k =>$v){
						$SelectedField[$v] = $v;
					}
				}
				elseif($reporttype=="campaign"){
					//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					$tempAdsArr = array();
					foreach ($summary as $kad ){
					  	$cid =  $kad[id];
					 	if($temp!=$cid){
						  	$sqlAds = "select ads.* from ads,campaign_members where  campaign_members.ad_id=ads.id and campaign_members.cid=$cid and parent_ad_id IS NOT NULL and event='sri_action_init'";
						  	$rs = $reportConn->execute($sqlAds);
							$AdsArr = array();
						  	if($rs && $rs->recordcount()>0){
								$AdsArr = $rs->getrows();
								foreach ($AdsArr as $k => $v){
									$tempAdsArr[$cid][] = $v;
								}
							}
							$temp=$cid;
						}
					}
				
					foreach ($tempAdsArr as $ckey => $campaignArray){
						$cid= $ckey;
						foreach ($campaignArray as $ck =>$cval){
							$sri_id = $cval['id'];
							$sri_details = $cval['sri_details'];
							$obj	=	json_decode($sri_details);
							//$el_name 	=	trim($obj->el_name);
							$el_name 	=	$cval['branded_img_right_ref_imgname'];
							if(trim($el_name))
								$SriHeader[] = trim($el_name);
							$campaign_sri_header[] = $el_name;
							//==========sri Number for sri element=============
							if($period == "summary"){
								$sql = "select sum(clicks) as sriclk,campaign_id as cid $aggregateNamesSRI
									from daily_stats where ad_id=$sri_id $sitestring $campaignstring $adtypestring 
			   						$countrystring $provincestring $datequery  
			   						$group sriid $aggregate
			   						Order By $sriorderby";
							}
							else{
						    	$sql = "select sum(clicks) as sriclk ,ad_id, campaign_id as cid $aggregateNamesSRI
									from daily_stats where ad_id=$sri_id $sitestring $campaignstring $adtypestring 
			   						$countrystring $provincestring $datequery  
			   						$group sriid,  $aggregate
			   						Order By $sriorderby";	
							}
							$el_nameSumamry = array();
							$rs = $reportConn->execute($sql);
							if($rs && $rs->recordcount()>0){
								$el_nameSumamry = $rs->getrows();
							}
							foreach ($summary as $key =>$val){
								foreach ($el_nameSumamry as $skey => $sval){
									if(($val['timeframe']==$sval['timeframe']) && $val['id']==$cid){
										$summary[$key][$el_name]+= $sval['sriclk'];
									}
								}
							}
						}
					}
					$SriHeader = array_unique($SriHeader);
					foreach ($SriHeader as $k =>$v){
						$SelectedField[$v] = $v;
					}
				//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				}
			}
			//======================Talk2ME Reporting END=========================
			$smarty->assign("SelectedField",$SelectedField);
			$smarty->assign("sriHeader",$SriHeader);
			
		//============Data Array for uniques from aggregate_ads===============
		if($period=="daily" || $period=="weekly" || $period=="monthly"){
				/**/$sqlUniques = "select 
							value as uniques $aggregateNamesU
						$fromUniques
						where 
							$whereUniques $campaignChannel $sitestring $campaignstringU $datequery $measure ";
				
				$rsUniques = $reportConn->execute($sqlUniques);	
				if($rsUniques && $rsUniques->recordcount()>0){
					$summaryUniques = $rsUniques->getrows();
				}
				foreach ($summary as $key =>$val){
					$summary[$key]['uniques'] = "";
					foreach ($summaryUniques as $ukey => $uval){
						if($val['timeframe']==$uval['timeframe'] && $val['id']==$uval['id']){
							$summary[$key]['uniques'] = $uval['uniques'];
							$summary[$key]['avgfrequency'] = ceil($summary[$key]['totimpressions']/$uval['uniques']);
						}
					}
				}
			}
		//====================================================================
		if($period == "summary"){
				$_SESSION['reporttype']=$reporttype;
				$_SESSION['period']='summary';
				$_SESSION['data']=$summary;
				
				//================ Field List Start ===========
					$totalArr = array();
					foreach ($summary as $k => $v){
						foreach ($fieldlistoptions as $key => $val){
							$totalArr[$key]+=$v[$key];
						}
						foreach ($SriHeader as $key => $val){
							$totalArr[$val]+=$v[$val];
						}
					}
					//$totalArr['ctr']=sprintf("%01.2f", $totalArr['totclicks']*100/$totalArr['totimpressions']);
					$totalArr['ctr'] = '';
					$totalArr['cr'] = '';
				//================ Field List End ===========
				$_SESSION['SelectedField']=$SelectedField;
				$_SESSION['totalArr']=$totalArr;
				$smarty->assign("totalArr",$totalArr);
				$smarty->assign("data",$summary);
			}
			else{
				foreach ( $summary as $key => $value ){
					$data[$value['name']][] = $value;
				}
				$_SESSION['reporttype']=$reporttype;
				$_SESSION['period']=$period;
				$_SESSION['data']=$data;
				
				//================ Field List Start ===========
				$tempfldArr = array();
				$tempfldArr =  $fieldlistoptions;
				$tempfldArr['uniques'] = 'Uniques';
				$tempfldArr['avgfrequency'] = 'Avg Frequency';
				foreach ($sri_elements as $k =>$v){
					$tempfldArr[$v] = $v;
				}
				foreach ($data as $k => $v){
					
					foreach ($v as $dkey => $dval){
						foreach ($fieldlistoptions as $key => $val){
							$totalArr[$k][$key]+=$dval[$key];
						}
						foreach ($SriHeader as $key => $val){
							$totalArr[$k][$val]+=$dval[$val];
						}
					}
					//$totalArr[$k]['ctr']=sprintf("%01.2f", $totalArr[$k]['totclicks']*100/$totalArr[$k]['totimpressions']);
					$totalArr['ctr'] = '';
					$totalArr['cr'] = '';
					$totalArr[$k]['avgfrequency']=ceil($totalArr[$k]['totimpressions']/$totalArr[$k]['uniques']);
				}
				//================ Field List End ===========
				$smarty->assign("totalArr",$totalArr);
				$_SESSION['SelectedField']=$SelectedField;
				$_SESSION['totalArr']=$totalArr;
				$smarty->assign("data",$data);
			}
	}
	//=================================================================================	
	break;
}


?>
