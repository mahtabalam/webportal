<?php
include_once('include/datefuncs.php');
include_once "$config[BASE_DIR]/common/portal/campaign/TargetingUI.php";
include_once "$config[BASE_DIR]/common/portal/campaign/CloneCampaign.php";
require_once ABSPATH."/common/portal/campaign/processSRIPortal.php";
require_once ABSPATH."/common/portal/campaign/BT_UI.php";
require_once ABSPATH."/common/portal/campaign/RetargetingUI.php";
require_once ABSPATH."/common/include/channelchoice.php";
include_once(dirname(__FILE__).'/../common/portal/campaign/advertiserDashboard.php');
								


switch ($_REQUEST['sub']) {
	case 'viewcamp':

	    $smarty->assign('statusoptions',$statusoptions);
	    $status = requestParams("status");
	    if($status == ""){
	    	$status = "active";
	    }
	    if($status == ""){
	    	$status = "all";
	    }
	    	$_REQUEST['status']=$status;
	    if($status != "all"){
	    	$status = $conn->qstr($status);
	    	$statusstring = "and status = $status";
	    }
		if($_REQUEST['searchtext']!=""){
			$searchtext = $conn->qstr("%".trim($_REQUEST['searchtext'])."%");
			$nameFilter = "and (id like $searchtext || name like $searchtext || review_status like $searchtext || status like $searchtext)";
		}else $nameFilter ="";
	    
		$sql = $conn->Prepare("SELECT campaign.*, geography.country_codes, geography.state_codes, geography.postalcodes,  group_concat(cast(blocked_campaigns.channel_id as char) separator ',') as channel FROM campaign LEFT JOIN geography ON campaign.id = geography.campaign_id LEFT JOIN blocked_campaigns ON campaign.id = blocked_campaigns.campaign_id LEFT JOIN campaign_UI ON campaign.id = campaign_UI.campaign_id WHERE campaign.device='pc' and campaign.advertiser_id = ? $statusstring $nameFilter group by id order by status asc, id desc");
		$rs = $conn->execute($sql, array($_SESSION[ADV_ID]));
		if($rs && $rs->recordcount()>0){
			$campaignsummary = $rs->getrows();
			
			
			$smarty->assign('campaignsummary',$campaignsummary);
			$smarty->assign('embeded',$embeded);
		}
		break;
	
	case 'clonetrackerads':
		include_once"$config[BASE_DIR]/common/portal/campaign/trackerAdsCloning.php";
		$obj = new TrackerAdsCloning();
		$obj->run();
		$smarty->display("$_REQUEST[page].tpl");
		exit(1);
		break;
		
	case 'setsdatenedate':
		require_once("Date.php");
		if($_REQUEST["action_save"]){
			$ad_start_date = $_REQUEST['start_date'];
			$ad_end_date = $_REQUEST['end_date'];
			$timezone = $_REQUEST['timezone'];
			if($ad_start_date=="" || $ad_end_date==""){
				$smarty->assign("error_msg", "Please enter valid start date and end date. And date cannot be less than campaign start date and cannot be greater than campaign end date.");
			}else{
				# start date
				$ad_start_date.=" 00:00:00";
				$ad_start_date=date("YmdHis",strtotime($ad_start_date));
				$dateObj = new Date($ad_start_date);
				$dateObj->setTZByID("$timezone");		# set local time zone
				$dateObj->convertTZByID("GMT");	# convert to foreign time zone
				$ad_start_date = $dateObj->format("%Y-%m-%e %T");
				# end date
				$ad_end_date.=" 23:59:59";
				$ad_end_date=date("YmdHis",strtotime($ad_end_date));
				$dateObj = new Date($ad_end_date);
				$dateObj->setTZByID("$timezone");		# set local time zone
				$dateObj->convertTZByID("GMT");	# convert to foreign time zone
				$ad_end_date = $dateObj->format("%Y-%m-%e %T");
				# update ads
				$sql = $conn->Prepare("update ads set ad_validfrom=?, ad_validto=? where id=?");
				$rs = $conn->Execute($sql, array($ad_start_date, $ad_end_date, $_REQUEST['ad_id']));
				if($rs) $smarty->assign("msg", "Your changes has been successfully saved.");
				else $smarty->assign("error_msg", "We are unable to process your request. Please try some time later.");
			}
		}
		# reset start date and end date to null
		if($_REQUEST['action_swap']){
			$sql = $conn->Prepare("update ads set ad_validfrom=null, ad_validto=null where id=?");
			$rs = $conn->Execute($sql, array($_REQUEST['ad_id']));
			if($rs) $smarty->assign("msg", "Your changes has been successfully saved.");
			else $smarty->assign("error_msg", "We are unable to process your request. Please try some time later.");
		}
		# get campaign detail
		if(isset($_REQUEST['campaign_id']) && $_REQUEST['campaign_id']!=""){
			$sql = $conn->Prepare("select timezone, validfrom, validto from campaign where id=?");
			$rs = $conn->Execute($sql, array($_REQUEST['campaign_id']));
			if($rs && $rs->recordcount()>0){
				if($rs->fields['validfrom']!="" && $rs->fields['validfrom']!="0000-00-00 00:00:00"){
					$start_date=$rs->fields['validfrom'];
					$dateObj = new Date($start_date);
					$dateObj->setTZByID("GMT");		# set local time zone
					$dateObj->convertTZByID($rs->fields['timezone']);	# convert to foreign time zone
					$data['start_date'] = date("M d Y",strtotime($dateObj->format("%Y-%m-%e %T")));
				}else{
					$data['start_date']=NULL;
				}
				if($rs->fields['validto']!="" && $rs->fields['validto']!="0000-00-00 00:00:00"){
					$end_date=$rs->fields['validto'];
					$dateObj = new Date($end_date);
					$dateObj->setTZByID("GMT");		# set local time zone
					$dateObj->convertTZByID($rs->fields['timezone']);	# convert to foreign time zone
					$data['end_date'] = date("M d Y",strtotime($dateObj->format("%Y-%m-%e %T")));
				}else{
					$data['end_date']=NULL;
				}
	
				$data['timezone'] = $rs->fields['timezone'];
			}
			$smarty->assign("data", $data);
		}
		# get ads start and end date
		if(isset($_REQUEST['ad_id']) && $_REQUEST['ad_id']!=""){
			$sql = $conn->Prepare("select ad_validfrom, ad_validto, timezone from ads join campaign_members cm on ads.id=cm.ad_id join campaign on campaign.id=cm.cid where ads.id=?");
			$rs = $conn->Execute($sql, array($_REQUEST['ad_id']));
			if($rs && $rs->recordcount()>0){
				if(isset($rs->fields['ad_validfrom']) && $rs->fields['ad_validfrom']!="" && $rs->fields['ad_validfrom']!="0000-00-00 00:00:00"){
					$dateObj = new Date(date("YmdHis",strtotime($rs->fields['ad_validfrom'])));
					$dateObj->setTZByID("GMT");		# set local time zone
					$dateObj->convertTZByID($rs->fields['timezone']);	# convert to foreign time zone
					$_REQUEST['start_date'] = date("M d Y",strtotime($dateObj->format("%Y-%m-%e %T")));
				}
				if(isset($rs->fields['ad_validto']) && $rs->fields['ad_validto']!="" && $rs->fields['ad_validto']!="0000-00-00 00:00:00"){
					$dateObj = new Date(date("YmdHis",strtotime($rs->fields['ad_validto'])));
					$dateObj->setTZByID("GMT");		# set local time zone
					$dateObj->convertTZByID($rs->fields['timezone']);	# convert to foreign time zone
					$_REQUEST['end_date'] = date("M d Y",strtotime($dateObj->format("%Y-%m-%e %T")));
				}
			 }
		}
		$smarty->display("$_REQUEST[page].tpl");
		exit(1);
		break;		
	case 'downloadRO':
		$dir      = $config[baseurl]."/files/RO/"; 
		$sql = $conn->Prepare("select id, ROfile from campaign where id=? and advertiser_id=?");
		$rs = $conn->execute($sql, array($_REQUEST[campaign_id], $_SESSION[ADV_ID]));
		if($rs && $rs->recordcount()>0){
			$file = $rs->fields['ROfile'];
		}
		if ($file!="") {
		   header("Content-type: application/force-download");
		   header('Content-Disposition: inline; filename="' . $dir.$file . '"');
		   header("Content-Transfer-Encoding: Binary");
		   header("Content-length: ".filesize($dir.$file));
		   header('Content-Type: application/octet-stream');
		   header('Content-Disposition: attachment; filename="' . $file . '"');
		   readfile("$dir$file");
		} else {
		   echo "No file exist";
		}
		
		
	break;
	case "emailcontent";
		$campaign_id = requestParams("campaign_id");
		if($_POST['sbmt']){
			//echo "<pre>";
			//print_r($_POST);
		} else {
			$sql = $conn->Prepare("select 
						c.channel_choice, 
						c.name, 
						c.track_choice, 
						a.ad_format 
					from 
						campaign c 
						left join campaign_members cm on cm.cid=c.id 
						left join ads a on a.id=cm.ad_id 
					where 
						c.id = ? and 
            a.ad_format='tracker' and
						a.advertiser_id = ? limit 1");
			$rs = $conn->execute($sql, array($campaign_id, $_SESSION[ADV_ID]));
			if($rs && $rs->recordcount()>0){
				//$sql="select publisher_id, group_concat(name) as names, group_concat(id) as ids from channels where id in (".$rs->fields['channel_choice'].") group by publisher_id";
				$sql="select publisher_id, name as names, id as ids from channels where id in (".$rs->fields['channel_choice'].") group by id";
				$rs_ch=$conn->execute($sql);
				$ad_format = $rs->fields['ad_format'];
				$tracker_type = $rs->fields['track_choice'];
				$cname = $rs->fields['name'];
				if($rs_ch && $rs_ch->recordcount()>0){
			         while(!$rs_ch->EOF) {
			            $publisher_id= $rs_ch->fields['publisher_id'];
			            $sqlpub="select concat(fname, ' ', lname, '(',company_name,')') as name from publisher where id=$publisher_id";
			            $rspub=$conn->execute($sqlpub);
			            if($rspub && $rspub->recordcount()>0)
			              $publisher_name=$rspub->fields['name'];
			
			            $channel_ids= $rs_ch->fields['ids'];
			            $channel_names= $rs_ch->fields['names'];
			            $pubinfo['chnames']=$channel_names;
			            $pubinfo['pubname']=$publisher_name;
			            $pubinfo['pubid']=$publisher_id;
						$publisherinfo[$channel_ids]=$pubinfo;
						$rs_ch->movenext();
					}
				}
			}
			
			$smarty->assign('pubinfo',$publisherinfo);
			$smarty->assign('trackerType',$tracker_type);
			$smarty->assign('campaign_name',$cname);
			$smarty->assign('campaign_id',$campaign_id);
		}
		break;
	case 'clonecampaign':
		if(isset($_REQUEST['action_saveclone'])){
			$campaignname=requestParams("campaignname");
			$campaignmodel=requestParams("campaignmodel");
			$sql = $conn->Prepare("select name from campaign where name = ?");
			$rs = $conn->execute($sql, array($campaignname));
			if($rs && $rs->recordcount()>0){
				$cName=$rs->fields['name'];
				$smarty->assign("error", "already_exist");
				$smarty->assign('campaign_id',$_REQUEST['campaign_id']);
				$smarty->assign('randName',$campaignname);
				$smarty->assign('cName',$cName);
			}
			else try {

				$campaign_id = $_REQUEST['campaign_id'];
				$campaignname = requestParams("campaignname");
				$campaignmodel=requestParams("campaignmodel");
				$objClone = new CloneCampaign($campaignname,$campaign_id,$campaignmodel);
				# Call createClone method
				$objClone->run();
				header("Location:$config[baseurl]/adv.php?page=campaign&sub=campaignsummary&msg=Clone+campaign+with+name+$campaignname+successfully+created");
			} catch (Exception  $e) {
          		$error_token=rand();
          		error_log("$error_token: ".$e->getMessage());
				header("Location:$config[baseurl]/adv.php?page=campaign&sub=campaignsummary&msg=Clone+campaign+failed.Please+report+error+token=$error_token");
        	}
		}else {
				$campaign_id = requestParams("id");
				$sql = $conn->Prepare("select c.name,ad.ad_format from campaign c join campaign_members cm  on c.id=cm.cid join ads ad on ad.id=cm.ad_id where c.id = ? and c.advertiser_id =?");
				$rs = $conn->execute($sql, array($campaign_id, $_SESSION[ADV_ID]));
				if($rs && $rs->recordcount()>0){
						$cName=$rs->fields['name'];
						$_REQUEST['ad_format']=$rs->fields['ad_format'];
				}
			
			function randomString($len) { 
			   srand(date("s")); 
			   $possible="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"; 
			   $str=""; 
			   while(strlen($str)<$len) { 
			      $str.=substr($possible,(rand()%(strlen($possible))),1); 
			   } 
			   return($str); 
			} 
			$randStr = randomString(5);
			$smarty->assign('clonecampaign',"clonecampaign");
			$smarty->assign('randName',"campaign".$randStr);
			$smarty->assign('campaign_id',$_REQUEST['id']);
			$smarty->assign('cName',$cName);
		}
		break;
	//============================================
	case 'viewads':
		
		
		$campaign_id = requestParams("campaign_id");
		$sqlblocked = $conn->Prepare("SELECT blocked_campaigns.*, concat(channels.name, ' (', channels.url, ')') as blocked_channels FROM `blocked_campaigns`, channels where blocked_campaigns.campaign_id =? and blocked_campaigns.channel_id = channels.id");
		$rsblocked = $conn->execute($sqlblocked, array($campaign_id));
		if($rsblocked && $rsblocked->recordcount()>0){
			$i=0;
					while(!$rsblocked->EOF) {
						$blockedchannels[$i] = $rsblocked->fields['blocked_channels'];
						$rsblocked->movenext();
						$i++;
					}
					$allblockedchannels = implode(", ", $blockedchannels );
					$alert = "Alert: This campaign is blocked at the sites: ".$allblockedchannels.". Please edit your advertisements if you wish to get these reviewed again";
					$smarty->assign("alert",$alert );
		}

		$sql = $conn->Prepare("select campaign.name, campaign.video_choice,  campaign.track_choice,campaign.overlay_choice, campaign.channel_choice from campaign where id = ? and advertiser_id =? ");
		$rs = $conn->execute($sql, array($campaign_id, $_SESSION[ADV_ID]));
		if($rs && $rs->recordcount()>0){
				$campaign['name']=$rs->fields['name'];
				$campaign['video_choice']=$rs->fields['video_choice'];
				$campaign['trktype']=$rs->fields['track_choice'];
				$campaign['subtype']=$rs->fields['overlay_choice'];
				$campaign['channel_choice']=$rs->fields['channel_choice'];

		}
		$sql = $conn->Prepare("select * from  campaign_members left join ads  on campaign_members.ad_id = ads.id where campaign_members.cid = ? and ads.ad_format<>'sri_interact' and ads.ad_format<>'sri_share' and ads.ad_format<>'sri_respond' and ads.advertiser_id = ?");

		$rs = $conn->execute($sql, array($campaign_id, $_SESSION[ADV_ID]));
		$i=0;
		
					$textads=array();
					$bannerads=array();
					$videoads=array();
					$brandedads=array();
					$trackerimages=array();
					$trackerswfs=array();
					$dblcliktags=array();
					$videobannerads=array();
					$youtubevbads=array();
                                        $ytprerollads=array();
					$overlayswfs = array();
					while(!$rs->EOF) { 
                                           // var_dump($rs); 
						$campaign['type']=$rs->fields['ad_format']."advertisement";
						if($rs->fields['ad_format']=='video')
						{
							$videoad['id']=$rs->fields['id'];
							$videoad['uri']=substr($rs->fields['playlist_ad_ref'],0,strlen($rs->fields['playlist_ad_ref'])-4);
							$videoad['name']=$rs->fields['branded_img_right_ref_imgname'];
							$videoad['companion_banners']=$rs->fields['branded_img_bot_ref_txbody'];
							$videoad['companion_dimensions']=$rs->fields['dimension'];
							$videoad['image']=$rs->fields['branded_color'];
							$videoad['desturl']=$rs->fields['destination_url'];
							$videoad['trackurl']=$rs->fields['tracker_url'];
							$ad_details = (array) json_decode($rs->fields['ad_details']);
							$videoad['vi_0']=$ad_details['vdo_tvi_0'];
							$videoad['vi_25']=$ad_details['vdo_tvi_25'];
							$videoad['vi_50']=$ad_details['vdo_tvi_50'];
							$videoad['vi_75']=$ad_details['vdo_tvi_75'];
							$videoad['ae']=$ad_details['vdo_tae'];
							$sri_details=$rs->fields['sri_details'];
							$videoads[]=$videoad;
						} elseif ($rs->fields['ad_format']=='branded') {
							$brandedad['id']=$rs->fields['id'];
							$brandedad['name']=$rs->fields['branded_img_right_ref_imgname'];
							$brandedad['desturl']=$rs->fields['destination_url'];
							$brandedad['color']=$rs->fields['branded_color'];
							$brandedad['imgurl']=$rs->fields['branded_img_top_ref_txtitle'];
							$brandedad['logo']=$rs->fields['branded_logo_ref'];
							$size = explode('x',$rs->fields['dimension']);
							$brandedad['width']=$size[0];
							$brandedad['height']=$size[1];
							$brandedad['skinreff']=$rs->fields['playlist_ad_ref'];
							$sri_details=$rs->fields['sri_details'];
							$campaign['type']="brandedplayer";
							$brandedads[]=$brandedad;
						}elseif($rs->fields['ad_format']=='ytpreroll'){
                                                    
								$ytprerollad['id']				= $rs->fields['id'];
								$ytprerollad['vdoID']			= $rs->fields['playlist_ad_ref'];
								$ytprerollad['name']			= $rs->fields['branded_img_right_ref_imgname'];
								$ytprerollad['dimension']		= $rs->fields['dimension'];
								$ytprerollad['optdesturl']		= $rs->fields['destination_url'];
								$ytprerollad['trackurl']		= $rs->fields['tracker_url'];
								$ad_details = (array) json_decode($rs->fields['ad_details']);
								$ytprerollad['vi_0']			= $ad_details['vdo_tvi_0'];
								$ytprerollad['vi_25']			= $ad_details['vdo_tvi_25'];
								$ytprerollad['vi_50']			= $ad_details['vdo_tvi_50'];
								$ytprerollad['vi_75']			= $ad_details['vdo_tvi_75'];
								$ytprerollad['ae']				= $ad_details['vdo_tae'];
								$ytprerollad['autoPlay']		= $ad_details['autoPlay'];
                                                                $ytprerollad['autoAfter']		= $ad_details['autoAfter'];
                                                                $ytprerollad['muted']		= $ad_details['muted'];
								$ytprerollads[] = $ytprerollad;  
                                                } elseif ($rs->fields['branded_img_top_ref_txtitle']!="" ) {
							$textad['id']=$rs->fields['id'];
							$textad['title']=$rs->fields['branded_img_top_ref_txtitle'];
							$textad['body']=$rs->fields['branded_img_bot_ref_txbody'];
							$textad['desturl']=$rs->fields['destination_url'];
							$sri_details=$rs->fields['sri_details'];
							$textads[]=$textad;
							$campaign['subtype']="textadvertisement";
						} elseif ($rs->fields['ad_format']=='tracker' || $rs->fields['ad_format']=='banner' || $rs->fields['ad_format']=='vdobanner') {
							if($campaign['trktype']=='dblclik'){
								$dblcliktag['id']=$rs->fields['id'];
								$dblcliktag['imgname']=$rs->fields['branded_img_right_ref_imgname'];
								$dblcliktag['imgsize']=$rs->fields['dimension'];
								$dblcliktag['tracker_url']=$rs->fields['tracker_url'];
								$trackerurls = explode('|', $dblcliktag['tracker_url']);
								$trackerurl = explode('~',$trackerurls[3]);
								$dblcliktag['imgurl']=$trackerurl[1];
								$trackerurl = explode('~',$trackerurls[2]);
								$dblcliktag['ancurl']=$trackerurl[1];
								$dblcliktag['desturl']=$rs->fields['destination_url'];
								$dblcliktag['trktype']=$campaign['trktype'];
								$sri_details=$rs->fields['sri_details'];
								$dblcliktags[]=$dblcliktag;
							}
							elseif($campaign['trktype']=='trackerimage' || $campaign['trktype']=='extimage'){
								$trackerimage['id']=$rs->fields['id'];
								$trackerimage['imgname']=$rs->fields['branded_img_right_ref_imgname'];
								$trackerimage['imgsize']=$rs->fields['dimension'];
                				if($campaign['trktype']=='extimage')
								  $trackerimage['imgurl']=$rs->fields['tracker_url'];
				                else {
									$trackerimage['imgurl']=$rs->fields['playlist_ad_ref'];
				                }
								$trackerimage['desturl']=$rs->fields['destination_url'];
								$trackerimage['trackurl']=$rs->fields['tracker_url'];
								$trackerimage['trktype']=$campaign['trktype'];
								$sri_details=$rs->fields['sri_details'];
								$trackerimages[]=$trackerimage;
							}
							elseif($campaign['trktype']=='videobanner'){
								$videobannerad['id']=$rs->fields['id'];
								$videobannerad['uri']=substr($rs->fields['playlist_ad_ref'],0,strlen($rs->fields['playlist_ad_ref'])-4);
								$videobannerad['videobannerurl']=$rs->fields['branded_logo_ref'];
								$videobannerad['name']=$rs->fields['branded_img_right_ref_imgname'];
								$videobannerad['dimension']=$rs->fields['dimension'];
								$videobannerad['desturl']=$rs->fields['destination_url'];
								$videobannerad['trackurl']=$rs->fields['tracker_url'];
								$ad_details = (array) json_decode($rs->fields['ad_details']);
								$videobannerad['vi_0']=$ad_details['vdo_tvi_0'];
								$videobannerad['vi_25']=$ad_details['vdo_tvi_25'];
								$videobannerad['vi_50']=$ad_details['vdo_tvi_50'];
								$videobannerad['vi_75']=$ad_details['vdo_tvi_75'];
								$videobannerad['ae']=$ad_details['vdo_tae'];
								$sri_details=$rs->fields['sri_details'];
								$videobannerads[]=$videobannerad;
							}elseif($campaign['trktype']=='youtubevb'){
								$youtubevbad['id']				= $rs->fields['id'];
								$youtubevbad['vdoID']			= $rs->fields['playlist_ad_ref'];
								$youtubevbad['name']			= $rs->fields['branded_img_right_ref_imgname'];
								$youtubevbad['dimension']		= $rs->fields['dimension'];
								$youtubevbad['optdesturl']		= $rs->fields['destination_url'];
								$youtubevbad['trackurl']		= $rs->fields['tracker_url'];
								$ad_details = (array) json_decode($rs->fields['ad_details']);
								$youtubevbad['vi_0']			= $ad_details['vdo_tvi_0'];
								$youtubevbad['vi_25']			= $ad_details['vdo_tvi_25'];
								$youtubevbad['vi_50']			= $ad_details['vdo_tvi_50'];
								$youtubevbad['vi_75']			= $ad_details['vdo_tvi_75'];
								$youtubevbad['ae']				= $ad_details['vdo_tae'];
								$youtubevbad['autoPlay']		= $ad_details['autoPlay'];
								$youtubevbads[] = $youtubevbad;  
							} else {
								$trackerswf['id']=$rs->fields['id'];
								$trackerswf['imgname']=$rs->fields['branded_img_right_ref_imgname'];
								$trackerswf['imgsize']=$rs->fields['dimension'];
                				if($campaign['trktype']=='extswf')
								    $trackerswf['imgurl']=$rs->fields['tracker_url'];
                				else {
								    $trackerswf['imgurl']=$rs->fields['playlist_ad_ref'];
                				}
								
                				$trackerswf['desturl']=$rs->fields['destination_url'];
                				$trackerswf['trackurl']=$rs->fields['tracker_url'];
								$trackerswf['trktype']=$campaign['trktype'];
								$sri_details=$rs->fields['sri_details'];
								$trackerswfs[]=$trackerswf;
							}
							$embeded=array();
							$dimension=array();
							$dimension=explode('x',$rs->fields['dimension']);
							$sqlch="select id, url from channels where id in($campaign[channel_choice])";
							$channel = $conn->execute($sqlch);
							
              				$randomnum=gmmktime();
							if($channel && $channel->recordcount()>0){
								$channels = $channel->getrows();
								foreach ( $channels as $key => $ch){
									$embeded[$key]['channel']=$ch['url'];
									$embeded[$key]['channel_id']=$ch['id'];
									$ad_id=$rs->fields['id'];
									$cid=$campaign_id;
									$channel_id=$ch['id'];
									$output='rd';
									$anchor = $dblcliktag['ancurl'];
									$campaign['trktype'];
									//echo "Abhay12: ".$sri_details; exit;
									if($campaign['trktype']=='dblclik'){
									  $embeded[$key]['code']=generateTags($cid, $ad_id, $channel_id, $dblcliktag['tracker_url'], $campaign['trktype'], $dimension, $output, $anchor, $sri_details);
									}elseif($campaign['trktype']=='videobanner'){
										$embeded[$key]['code']=generateTags($cid, $ad_id, $channel_id, $videobannerad['trackurl'], $campaign['trktype'], $dimension, $output, $anchor, $sri_details);
									}elseif($campaign['trktype']=='youtubevb'){
										if($youtubevbad['autoPlay'] == "1" || $youtubevbad['autoPlay'] == "2")
											$autoPlay = 1;
										else 
											$autoPlay = 0;
										$embeded[$key]['code']=generateTags($cid, $ad_id, $channel_id, $youtubevbad['trackurl'], $campaign['trktype'], array('vdoID'=>$youtubevbad['vdoID'],'autoPlay'=>$autoPlay), $output, $anchor, $sri_details);
									}else{
										if($campaign['trktype']=='trackerimage' || $campaign['trktype']=='extimage'){
										  $embeded[$key]['code']=generateTags($cid, $ad_id, $channel_id, $trackerimage['trackurl'], $campaign['trktype'], $dimension, $output, $anchor, $sri_details, "jstag");
										  $embeded[$key]['code'].="\n\nOR\n\n";
										  $embeded[$key]['code'].=generateTags($cid, $ad_id, $channel_id, $trackerimage['trackurl'], $campaign['trktype'], $dimension, $output, $anchor, $sri_details);
										}
										else {
										  $embeded[$key]['code']=generateTags($cid, $ad_id, $channel_id, $trackerswf['trackurl'], $campaign['trktype'], $dimension, $output, $anchor, $sri_details, "jstag");
										  $embeded[$key]['code'].="\n\nOR\n\n";
										  $embeded[$key]['code'].=generateTags($cid, $ad_id, $channel_id, $trackerswf['trackurl'], $campaign['trktype'], $dimension, $output, $anchor, $sri_details);
										  
										}
									}
								}
								$embeded_code[]=$embeded;
							}
						} else {
							if($campaign['subtype']=='banneradvertisement'){
								$bannerad['id']=$rs->fields['id'];
								$bannerad['imgname']=$rs->fields['branded_img_right_ref_imgname'];
								$bannerad['imgurl']=$rs->fields['playlist_ad_ref'];
								$bannerad['desturl']=$rs->fields['destination_url'];
								$bannerad['trackurl']=$rs->fields['tracker_url'];
								$campaign['subtype']="banneradvertisement";
								$bannerads[]=$bannerad;
							}
							elseif($campaign['subtype']=='overlayswf'){
								$overlayswf['id']=$rs->fields['id'];
								$overlayswf['imgname']=$rs->fields['branded_img_right_ref_imgname'];
								$overlayswf['imgurl']=$rs->fields['playlist_ad_ref'];
								$overlayswf['desturl']=$rs->fields['destination_url'];
								$overlayswf['trackurl']=$rs->fields['tracker_url'];
								$overlayswf['subtype']="overlayswf";
								$overlayswfs[]=$overlayswf;
							}
						}
						$rs->movenext();
					}
					$campaign['textads']=$textads;
					$campaign['bannerads']=$bannerads;
					$campaign['videoads']=$videoads;
					$campaign['brandedads']=$brandedads;
					$campaign['trackerswfs']=$trackerswfs;
					$campaign['trackerimages']=$trackerimages;
					$campaign['dblcliktags']=$dblcliktags;
					$campaign['videobannerads']=$videobannerads;
					$campaign['youtubevbads'] = $youtubevbads;
					$campaign['overlayswfs']=$overlayswfs;
                                        $campaign['ytvideoads'] = $ytprerollads; 
					
          			if($campaign['trktype']=='extimage' )
					  $campaign['extinfo']=$trackerimages;
          			if( $campaign['trktype']=='extswf')
					  $campaign['extinfo']=$trackerswfs;
					  
					$smarty->assign('readOnly',"1");
					$smarty->assign('viewonly',"1");
					$smarty->assign('campaign',$campaign);
					$smarty->assign('embeded',$embeded_code);
		break;

	case 'campaignsummary':

		# Filter options
		$smarty->assign('statusoptions',$statusoptions);
		$smarty->assign("countrycodes", getCountryList());

		# status
		$status = requestParams("status");
	    if($status == ""){
	    	$status = "active";
	    }
	    	$_REQUEST['status']=$status;
	    if($status != "all"){
	    	$statusstring = "and c.status = '$status'";
	    }
	    
	    # ad format
	    $ad_format = requestParams("ad_format");
	    if($ad_format == ""){
	      $ad_format = "all";
	    }
	    $_REQUEST['ad_format']=$ad_format;
	    if($ad_format != "all"){
	      $adformatstring = "and ads.ad_format = '$ad_format'";
	    }else{
	      $adformatstring = "";
	    }
	    
	    # country
        $country = requestParams("country");
	    if($country == ""){
	      $country = "all";
	    }
	    $_REQUEST['country']=$country;
	    if($country != "all"){
	      $countrystring = "and find_in_set(geography.country_codes, '$country')";
	    }else{
	      $countrystring = "";
	    }

	    # Approve
		if(isset($_SESSION['ADV_PUB_ID']) || in_array('rw.campaign_live', $_SESSION['CAPABILITIES'])){ 
			if(isset($_REQUEST['action_approve'])){
				$campaignid=$_REQUEST['campaign_id_toapprove'];
				if(!empty($campaignid)){
					$sql=$conn->Prepare("select campaign.name, ads.ad_format, campaign.track_choice, campaign_UI.campaignstatus from campaign left join campaign_members on campaign.id=campaign_members.cid left join ads on ads.id=campaign_members.ad_id left join campaign_UI on campaign.id=campaign_UI.campaign_id where campaign.id=? and campaign.advertiser_id=?");
					$rs = $conn->execute($sql, array($campaignid, $_SESSION[ADV_ID]));
					if($rs && $rs->recordcount()>0){
						$campaign_name=$rs->fields['name'];
						$campaign_status="Approved";
					
						$sql = $conn->Prepare("update campaign set review_status = ? where id=? and review_status = 'Pending Review'");
						$rs = $conn->execute($sql, array($campaign_status, $campaignid));
						$rows =$conn->Affected_Rows();
						if($rows>0){
							$error = "Campaign ".$campaign_name." approved successfully";
		
						}else{
							$error = "Only campaigns in Pending Review can be approved";
						}
					}
				}else{
					$error = "No campaign selected";
				}
			}
		}
		
		if($_REQUEST['action_unexpire']!=null){
			$campaignids=$_REQUEST['campaignids'];
			if(!empty($campaignids)){
				$ids = mysql_escape_string("(".implode( ',', $campaignids ).")");
				$sql = "update campaign set status = 'active' where id in $ids and status = 'expired'";
				$rs = $conn->execute($sql);
				$rows = $conn->Affected_Rows();
				if($rows>0){
					$error = "Campaign(s) resumed successfully";

				}else{
					$error = "Only expired campaigns can be resumed";
				}
			}else{
				$error = "No campaign selected to be resumed";
			}
		}elseif($_REQUEST['action_paused']!=null){
			// "<pre>"; print_r($_GET);
			$campaignids=$_REQUEST['campaignids'];
			if(!empty($campaignids)){
				$ids = mysql_escape_string("(".implode( ',', $campaignids ).")");
				$sql = "update campaign set status = 'paused' where id in $ids and status = 'active'";
				$rs = $conn->execute($sql);
				$rows = $conn->Affected_Rows();
				if($rows>0){
					$error = "Campaign(s) paused successfully";

				}else{
					$error = "Only active campaigns can be paused";
				}
			}else{
				$error = "No campaign selected to be paused";
			}


		}elseif($_REQUEST['action_resumed']!=null){
			$campaignids=$_REQUEST['campaignids'];
			if(!empty($campaignids)){
				$ids = mysql_escape_string("(".implode( ',', $campaignids ).")");
				$sql = "update campaign set status = 'active' where id in $ids and status = 'paused'";
				$rs = $conn->execute($sql);
				$rows = $conn->Affected_Rows();
				if(count($campaignids)==$rows){
					$error = "Campaign(s) resumed successfully";

				}else{
					$error = "Only paused campaigns can be resumed";
				}
			}else{
				$error = "No campaign selected to be resumed";
			}

		}elseif($_REQUEST['action_deleted']!=null){
			if(!empty($campaignids)){
				$campaignids=$_REQUEST['campaignids'];
				$ids = "(".implode( ',', $campaignids ).")";
				$sql = "update campaign set status = 'deleted' where id in $ids";
				$rs = $conn->execute($sql);
				$rows = $conn->Affected_Rows();
				if($rows>0){
					$error = "Campaign deleted successfully";

				}
			}else{
				$error = "No campaign selected to be deleted";
			}

		}elseif($_REQUEST['action_update_budget']!=null){
			$campaignids=$_REQUEST['campaignids'];
      $priceupdate=array();
			if(!empty($campaignids)){
				$budgetErr = " Daily Limit should be less than or equal to the Budget for Campaign(s): ";
				$flag = false;
				$affRws = 0;
				//$ids = "(".implode( ',', $campaignids ).")";
				foreach( $campaignids as $key=>$val )
				{
					$maxTotalImps = 0;
					$maxDailyImps = 0;
					$maxTotalClicks = 0;
					$maxDailyClicks = 0;
					$maxDailyCompletes = 0;
					$maxTotalCompletes = 0;
					$maxDailyLeads = 0;
					$maxTotalLeads = 0;
					
						$budget_amt 	= $_REQUEST['update_budget'.$val];
						$daily_limit 	= $_REQUEST['update_daily_limit'.$val];
						if ($budget_amt < $daily_limit ) {
							$budgetErr .= $_REQUEST["campaignIds-$val"].",";
							$sql = $conn->Prepare("select budget_amt, daily_limit from campaign WHERE id = ?");
							$rs = $conn->execute($sql, array($val));
							if($rs && $rs->recordcount()>0){
								$arr = $rs->getrows();
								$_REQUEST['update_budget'.$val]	= $arr['budget_amt'];
								$_REQUEST['update_daily_limit'.$val] = $arr['daily_limit'];
							}
							$flag = true;
						} else {
						
						//echo " == " . $_REQUEST["model".$val];
						if( $_REQUEST["model".$val] == 'CPM' )
						{
							$price_per_unit = $_REQUEST['update_cpm'.$val] / 1000;
						if($price_per_unit > 0){
	                                      $maxTotalImps = ceil($budget_amt/$price_per_unit);
	                                      $maxDailyImps = ceil($daily_limit/$price_per_unit);
	                                    }
						}elseif($_REQUEST["model".$val] == 'CPCV'){
							$price_per_unit = $_REQUEST['update_cpm'.$val];
							if($price_per_unit > 0){
								$maxTotalCompletes = ceil($budget_amt/$price_per_unit);
								$maxDailyCompletes = ceil($daily_limit/$price_per_unit);
							}
						}elseif($_REQUEST["model".$val] == 'CPL'){
							$price_per_unit = $_REQUEST['update_cpm'.$val];
							if($price_per_unit > 0){
								$maxTotalLeads = ceil($budget_amt/$price_per_unit);
								$maxDailyLeads = ceil($daily_limit/$price_per_unit);
							}
						}
						else { 
							$price_per_unit = $_REQUEST['update_cpm'.$val]; 
							if( $_REQUEST["model".$val] == 'CPC' )
							{
								if($price_per_unit > 0){
	                                      $maxTotalClicks = ceil($budget_amt/$price_per_unit);
	                                      $maxDailyClicks = ceil($daily_limit/$price_per_unit);
	                                    }
							}
						}
						
						$sql = $conn->Prepare("UPDATE campaign SET price_per_unit = ?, budget_amt=?, daily_limit =? WHERE id = ?");
						
						$rs = $conn->execute($sql, array($price_per_unit, $budget_amt, $daily_limit, $val));
						$rows =  $conn->Affected_Rows();
						if ($rows) {
							$campUpadted .= $_REQUEST["campaignIds-$val"].",";
							$affRws++;
						}
					 	if($maxTotalImps==0){ $maxTotalImps = NULL;}
                        if($maxTotalClicks==0){ $maxTotalClicks = NULL;}
                        if($maxDailyClicks==0){ $maxDailyClicks = NULL;}
                        if($maxDailyImps==0){ $maxDailyImps = NULL;}
                        if($maxDailyCompletes==0){ $maxDailyCompletes = NULL;}
                        if($maxTotalCompletes==0){ $maxTotalCompletes = NULL;}
                        if($maxDailyLeads==0){ $maxDailyLeads = NULL;}
                        if($maxTotalLeads==0){ $maxTotalLeads = NULL;}
                        // set maxLimit and daily limit for campaign
                        $sqlLimit = $conn->Prepare("update campaign_limits_rt set maxTotalImps=?,maxDailyImps=?, maxTotalClicks=?, maxDailyClicks=?, maxTotalCompletes=?, maxDailyCompletes=?, maxDailyLeads=?, maxTotalLeads=?  where cid=? and chid='0' ");
                                                 
                        $rsLimit=$conn->execute($sqlLimit, array($maxTotalImps, $maxDailyImps, $maxTotalClicks, $maxDailyClicks, $maxTotalCompletes, $maxDailyCompletes, $maxDailyLeads, $maxTotalLeads, $val));
					}
				}
				
				if($affRws>0){
					$campUpadted = substr($campUpadted,0,-1);
					$error = "Campaign budget updated successfully for Campaign(s): $campUpadted.";
         		 	$priceupdate[$val]['price_per_unit']=$price_per_unit;
          			$priceupdate[$val]['budget_amt']=$budget_amt;
          			$priceupdate[$val]['daily_limit']=$daily_limit;
				}
				if ($flag) {
					$budgetErr = substr($budgetErr,0,-1);
					$error .= $budgetErr.".";
				} else if($affRws == 0){
					$error .= "Nothing has been updated. ";
				}
			}else{
				$error = "No campaign selected to budget update";
			}
		}
		
		
		
		
		
		
		//echo "<pre>"; print_r($_SESSION); exit;

		$dates=processDates($_REQUEST,'timeperiod',$_SESSION['ADV_ID']);
		$_REQUEST['start_date']=date($viewformat, strtotime($dates[0]));
		$_REQUEST['end_date']=date($viewformat, strtotime($dates[1]));
		$smarty->assign("err", $error);
		if(isset($config['DB_PREFIX']) && $config['DB_PREFIX']!=''){
			$db_prefix =$config['DB_PREFIX'];
		}else {
			$db_prefix='';
		}
		if($_REQUEST['time']=='dropdown' && $_REQUEST['timeperiod']=='alltime') {
			$datequery="";
			$datedetails="";
		} else {
			$datequery=" and ((date>='$dates[0]' and date<='$dates[1]') or (date(createdate)>='$dates[0]' and date(createdate)<='$dates[1]'))";
			$datedetails= " and (date(createdate)>='$dates[0]' and date(createdate)<='$dates[1]')";
		}
		
		if($_REQUEST['action_search']!="" && $_REQUEST['searchtext']!=""){
			$searchtext = $conn->qstr("%".trim($_REQUEST['searchtext'])."%");
			$nameFilter = " and (c.id like $searchtext || c.name like $searchtext || c.review_status like $searchtext || c.status like $searchtext) ";
		}else $nameFilter ="";
		
			$sqlb	=	$conn->Prepare("select orcs_list,chid from orcs_lists where chid=0");
			$rsb	=	$conn->execute($sqlb, array());
			$clist_array = explode(",", $rsb->fields['orcs_list']);
			$advertiserDashboard = new advertiserDashboard();
			$camp=$advertiserDashboard->campaignDetails($nameFilter,$datedetails,$statusstring,$adformatstring,$priceupdate,$clist_array,$countrystring);
			//var_dump($camp);
			$val=$advertiserDashboard->campaignStatistics($nameFilter,$datequery,$statusstring);
			$campaignsummary=$advertiserDashboard->mergeCampaignData($camp,$val);
			
	/*	 $sql = $reportConn->Prepare("SELECT 
					campaign.price_per_unit,
					(campaign.price_per_unit * 1000 ) as cpm_price_per_unit,
					campaign.model,
					campaign.name, 
					campaign.id AS cid, 
					campaign.budget_amt, 
					campaign.budget_currency, 
					campaign.daily_limit,
					campaign.validto,
					campaign.priority_level,
					campaign.pacing_type,
					datediff(date(campaign.validto), date(now())) as isday, 
					sum( impressions ) AS timp, 
					sum( clicks ) AS tclicks, 
					sum( total ) *1000 AS total1000, 
					sum(completes) as vdocompletes,
                    round(((sum(completes)/sum( impressions ))*100),2) as cr,
                    sum(leads) as leads,
					campaign.budget_currency, 
					campaign.status , 
					campaign.review_status, 
					channel 
				FROM 
					campaign
					LEFT JOIN campaign_members ON campaign_members.cid = campaign.id
					LEFT JOIN ads ON ads.id = campaign_members.ad_id 
					LEFT JOIN geography ON geography.campaign_id = campaign.id
					LEFT JOIN aggregate_statistics ON ads.id=aggregate_statistics.ad_id
					LEFT JOIN (select distinct(campaign_id), group_concat( cast( blocked_campaigns.channel_id AS char ) SEPARATOR ',' ) AS channel from blocked_campaigns group by campaign_id) t1 on t1.campaign_id=campaign.id
				WHERE 
					campaign.device='pc' and 
					campaign.advertiser_id=?
					$nameFilter 
					$datequery $statusstring $adformatstring $countrystring
				GROUP BY 
					campaign.id 
				order by 
					campaign.status asc, campaign.id desc");

     	//echo $sql; 
		//$sql = "SELECT name, id AS cid, format( budget_amt, 2) AS budget_amt, budget_currency, daily_limit, sum( impressions ) AS timp, sum( clicks ) AS tclicks, format( sum( total ) *1000 , 2) AS total1000, budget_currency, daily_limit, status , review_status, group_concat( cast( blocked_campaigns.channel_id AS char ) SEPARATOR ',' ) AS channel FROM campaign LEFT JOIN aggregate_statistics ON id = campaign_id LEFT JOIN blocked_campaigns ON campaign.id = blocked_campaigns.campaign_id WHERE campaign.device='pc' and advertiser_id=$_SESSION[ADV_ID] $datequery  GROUP BY cid";
		/*$sql = "select name, id as cid,  cast(budget_amt as decimal) as budget_amt, budget_currency, daily_limit, sum(impressions) as timp, sum(clicks) as tclicks, cast(sum(total)*1000 as decimal) as total1000, budget_currency, daily_limit, status, review_status from campaign left join aggregate_statistics on id=campaign_id where advertiser_id=$_SESSION[ADV_ID] $datequery group by cid";*/
	/*	$rs=$reportConn->execute($sql, array($_SESSION[ADV_ID]));
		if($rs && $rs->recordcount()>0){
			$campaignsummary=$rs->getrows();
		}
		
			# daily limit
			foreach( $campaignsummary as $key=>$value ){
				$cid	=	$value["cid"];
				$sqlb	=	"select if(find_in_set('$cid', orcs_list), 'true', 'false') as isLimit from orcs_lists where chid=0";
				$rsb	=	$reportConn->execute($sqlb);
				$rsb->fields['isLimit'];
				if($rsb->fields['isLimit']=="true") {
					$campaignsummary[$key]['limitreached'] = "<b><font color='red'>Limit Exceeded</font></b>";
				} 
				else {
					$campaignsummary[$key]['limitreached'] = "<b><font color='green'>Limit OK</font></b>";
				}
			}*/
		
		$smarty->assign('campaignsummary',$campaignsummary);
                
		break;
	case 'createcampaign':
		doForward("$config[baseurl]/index.php?page=formloader&form=ad");
	case 'adsummary':

		$campaign_id = requestParams("campaign_id");
		$sqlblocked = $conn->Prepare("SELECT blocked_campaigns.*, concat(channels.name, ' (', channels.url, ')') as blocked_channels FROM `blocked_campaigns`, channels where blocked_campaigns.campaign_id =? and blocked_campaigns.channel_id = channels.id");
		$rsblocked = $conn->execute($sqlblocked, array($campaign_id));
		if($rsblocked && $rsblocked->recordcount()>0){
			$i=0;
					while(!$rsblocked->EOF) {
						$blockedchannels[$i] = $rsblocked->fields['blocked_channels'];
						$rsblocked->movenext();
						$i++;
					}
					$allblockedchannels = implode(", ", $blockedchannels );
					$alert = "Alert: This campaign is blocked at the sites: ".$allblockedchannels.". Please edit your advertisements if you wish to get these reviewed again";
					$smarty->assign("alert",$alert );
		}
		if($_REQUEST['action_disabled']!=null){

			$adids=$_REQUEST['ads'];
			if(!empty($adids)){
				$cid = mysql_escape_string($_REQUEST['campaign_id']);
				$ids = mysql_escape_string("(".implode( ',', $adids ).")");
				$sql = "update campaign_members set status = 'disabled' where ad_id in $ids and cid = '$cid' and status = 'enabled'";
				$rs = $conn->execute($sql);
				$rows = $conn->Affected_Rows();
				if(count($adids)==$rows){
					$error = "Ads disabled successfully";

				}else{
					$error = "Only enabled ads can be disabled";
				}
			} else {
				$error = "No ad selected";
			}


		}elseif($_REQUEST['action_enabled']!=null){
			$adids=$_REQUEST['ads'];
			if(!empty($adids)){
				$adids=$_REQUEST['ads'];
				$cid = mysql_escape_string($_REQUEST['campaign_id']);
				$ids = mysql_escape_string("(".implode( ',', $adids ).")");
				$sql = "update campaign_members set status = 'enabled' where ad_id in $ids and cid = '$cid' and status = 'disabled'";
				$rs = $conn->execute($sql);
				$rows = $conn->Affected_Rows();
				if(count($adids)==$rows){
					$error = "Ads enabled successfully";

				}else{
					$error = "Only disabled ads can be enabled";
				}
			} else {
				$error = "No ad selected";
			}

		}
		$dates=processDates($_REQUEST,'timeperiod',$_SESSION['ADV_ID']);
		$_REQUEST['start_date']=date($viewformat, strtotime($dates[0]));
		$_REQUEST['end_date']=date($viewformat, strtotime($dates[1]));
				$smarty->assign("err", $error);

		

        $sqlcampaign_name = $conn->Prepare("select name from campaign where id = ? and advertiser_id = ?");
        $rscampaign_name = $conn->execute($sqlcampaign_name, array($campaign_id, $_SESSION[ADV_ID]));
        if($rscampaign_name && $rscampaign_name->recordcount()>0){
        	$smarty->assign("campaignname", $rscampaign_name->fields['name']);

        }
 $advertiserDashboard = new advertiserDashboard();
        $camp=$advertiserDashboard->adDetails($campaign_id);
        $val=$advertiserDashboard->adStatistics($campaign_id);
        $adsummary=$advertiserDashboard->mergeAdData($camp,$val);
	/*	$sql=$reportConn->Prepare("SELECT ads.*, campaign.track_choice, sum(impressions) as impressions, sum(clicks) as clicks, sum(total)*1000 as total1000 , date, cm.status as status FROM ads,campaign , campaign_members as cm left join aggregate_statistics as ags on ags.ad_id=cm.ad_id and ags.campaign_id =? where ads.ad_format<>'sri_share' and ads.ad_format<>'sri_respond' and ads.ad_format<>'sri_interact' and ads.advertiser_id=? and cm.ad_id = ads.id and cm.cid = ? and campaign.id=? $datequery  group by ads.id order by branded_img_right_ref_imgname");

		
		$rs=$reportConn->execute($sql, array($campaign_id, $_SESSION[ADV_ID], $campaign_id, $campaign_id));
		$i=0;
		while ($rs && !$rs->EOF) {
			$adsummary[$i]['title']=$rs->fields['branded_img_right_ref_imgname'];
			$adsummary[$i]['ad_validfrom']=$rs->fields['ad_validfrom'];
			$adsummary[$i]['ad_validto']=$rs->fields['ad_validto'];
			if($rs->fields['ad_format']=="video") {
				$adsummary[$i]['ad_format']="video";
			}
			elseif($rs->fields['ad_format']=="overlay" && $rs->fields['branded_img_top_ref_txtitle']=="") {
				$adsummary[$i]['ad_format']="Image Overlay";
			}
			elseif($rs->fields['ad_format']=="overlay" && $rs->fields['branded_img_top_ref_txtitle']!="") {
				$adsummary[$i]['ad_format']="Text Overlay";
			}
			elseif($rs->fields['ad_format']=="branded") {
				$adsummary[$i]['ad_format']="Branded Advertisement";
			}
			elseif($rs->fields['ad_format']=="tracker" || $rs->fields['ad_format']=="vdobanner" || $rs->fields['ad_format']=="banner") {
				// enum('trackerswf','trackerimage','extimage','extswf','dblclik','videobanner')  
				if($rs->fields['track_choice']=='videobanner'){
					$adsummary[$i]['ad_format']="Video Banner";
				}elseif ($rs->fields['track_choice']=='trackerswf' || $rs->fields['track_choice']=='extswf'){
					$adsummary[$i]['ad_format']="Tracker Swf";
				}elseif ($rs->fields['track_choice']=='trackerimage' || $rs->fields['track_choice']=='extimage'){
					$adsummary[$i]['ad_format']="Tracker Image";
				}elseif($rs->fields['track_choice']=='dblclik'){
					$adsummary[$i]['ad_format']="Double Click Tag";
				}else{
					$adsummary[$i]['ad_format']="Undefined";
				}
			}
			$adsummary[$i]['id']=$rs->fields['id'];
			$adsummary[$i]['status']=$rs->fields['status'];
			$adsummary[$i]['impressions']=$rs->fields['impressions'];
			$adsummary[$i]['clicks']=$rs->fields['clicks'];
			$adsummary[$i]['total1000']=$rs->fields['total1000'];
			$adsummary[$i]['ctr']=$rs->fields['clicks']/$rs->fields['impressions'];
			$i++;
			$rs->movenext();
		}*/
		$smarty->assign('adsummary',$adsummary);
		break;
	case "ad":
		header("location: $config[baseurl]/index.php?page=formloader&form=ad");
	break;
	case "uploadcreative":
		$ad_id=mysql_real_escape_string($_REQUEST[adid]);
		if($_REQUEST['action_videoad1']){
			$uri=mysql_escape_string(escapeshellcmd(strip_tags($_REQUEST['uri'])));
			$dimension = mysql_escape_string(strip_tags($_REQUEST['dimension']));
			$duration = mysql_escape_string(strip_tags($_REQUEST['duration']));
			$flvsrc="$config[tempfilepath]/$uri.flv";
			$thmbsrc="$config[tempfilepath]/thmb$uri.jpg";
			$adbase=md5($adrow['imgurl'].rand(0,1000000000));
			$flvfile=$adbase.".flv";
			$thmbdest="$config[AD_IMG_DIR]/$adbase.jpg";
			$flvdest="$config[AD_VDO_DIR]/$flvfile";
			$rs=false;
		  
		  	error_log("rename($flvsrc,$flvdest)");
		  	error_log("rename($thmbsrc,$thmbdest)");
			
		  	if(rename($flvsrc,$flvdest) && rename($thmbsrc,$thmbdest)){
		  		$sql=$conn->Prepare("update ads set playlist_ad_ref=?, duration=? where id=?");
		  		$rs=$conn->execute($sql, array($flvfile, $duration, $ad_id));
				if(!$rs) {
					$msg="Creative uploading failed. Please try some time later.";
					rename($flvsrc,$flvdest);
					rename($thmbsrc,$thmbdest);
				}else{
					$msg="Creative uploaded successfully.";
				}
		  	}else{
		  		$msg="Permission Denied.";
		  	}
		}
		$smarty->assign('msg', $msg);
		$smarty->assign('adid', $ad_id);
		$smarty->display("$_REQUEST[page].tpl");
		exit;
	break;
	case "uploadcreativeforvideobanner":
		$ad_id=mysql_real_escape_string($_REQUEST[adid]);
		if($_REQUEST['action_videobannerad1']){
		    //echo "<pre>"; print_r($_REQUEST); exit;
			$uri=mysql_escape_string(escapeshellcmd(strip_tags($_REQUEST['uri'])));
			$alternate_image = mysql_escape_string(escapeshellcmd(strip_tags($_REQUEST['videobannerurl'])));
			$dimension = mysql_escape_string(strip_tags($_REQUEST['dimension']));
			$duration = mysql_escape_string(strip_tags($_REQUEST['duration']));
			$bannerExt = explode('.', $alternate_image);
			$flvsrc="$config[tempfilepath]/$uri.flv";
			$thmbsrc="$config[tempfilepath]/thmb$uri.jpg";
			$altimagesrc="$config[tempfilepath]/$alternate_image";
			$adbase=md5($adrow['imgurl'].rand(0,1000000000));
			$flvfile=$adbase.".flv";
			$altimagefile=$adbase."bnr.".$bannerExt[count($bannerExt)-1];
			$thmbdest="$config[AD_IMG_DIR]/$adbase.jpg";
			$flvdest="$config[AD_VDO_DIR]/$flvfile";
			$altimagedest="$config[AD_IMG_DIR]/$adbase"."bnr.".$bannerExt[count($bannerExt)-1];
			$rs=false;
		  
		  	error_log("rename($flvsrc,$flvdest)");
		  	error_log("rename($thmbsrc,$thmbdest)");
		  	error_log("rename($altimagesrc,$altimagedest)");
		  	
		  	if($alternate_image!=""){
		  	  $sqlSnip="branded_logo_ref='$altimagefile',";
		  	  rename($altimagesrc,$altimagedest);
		  	}
			
		  	if(rename($flvsrc,$flvdest) && rename($thmbsrc,$thmbdest)){
		  		$sql=$conn->Prepare("update ads set playlist_ad_ref=?, $sqlSnip duration=? where id=?");
		  		$rs=$conn->execute($sql, array($flvfile, $duration, $ad_id));
				if(!$rs) {
					$msg="Creative uploading failed. Please try some time later.";
					rename($flvdest,$flvsrc);
					rename($thmbdest,$thmbsrc);
					rename($altimagedest,$altimagesrc);
				}else{
					$msg="Creative uploaded successfully.";
				}
		  	}else{
		  		$msg="Permission Denied.";
		  	}
		}
		$smarty->assign('msg', $msg);
		$smarty->assign('adid', $ad_id);
		$smarty->display("$_REQUEST[page].tpl");
		exit;
	break;
	case "uploadcreativefortrackerimage":
		$ad_id=mysql_real_escape_string($_REQUEST[adid]);
		if($_REQUEST['action_trackerimage']){
			$imageurl=mysql_escape_string(escapeshellcmd(strip_tags($_REQUEST['imgurl'])));
			$extension=mysql_escape_string(escapeshellcmd(strip_tags($_REQUEST['extension'])));
			
			$imagesrc="$config[tempfilepath]/$imageurl";
			$adbase=md5($adrow['imgurl'].rand(0,1000000000));
			$imagefile=$adbase.".".$extension;
			$imagedest="$config[AD_IMG_DIR]/$adbase".".".$extension;
			$rs=false;
		  
		  	error_log("rename($imagesrc,$imagedest)");
			
		  	if(rename($imagesrc,$imagedest)){
		  		$sql=$conn->Prepare("update ads set playlist_ad_ref=? where id=?");
		  		$rs=$conn->execute($sql, array($imagefile, $ad_id));
				if(!$rs) {
					$msg="Creative uploading failed. Please try some time later.";
					rename($imagedest,$imagesrc);
				}else{
					$msg="Creative uploaded successfully.";
				}
		  	}else{
		  		$msg="Permission Denied.";
		  	}
		}
		$smarty->assign('msg', $msg);
		$smarty->assign('adid', $ad_id);
		$smarty->display("$_REQUEST[page].tpl");
		exit;
	break;
	case "uploadcreativefortrackerswf":
		$ad_id=mysql_real_escape_string($_REQUEST[adid]);
		if($_REQUEST['action_trackerswf']){
			$swfurl=mysql_escape_string(escapeshellcmd(strip_tags($_REQUEST['imgurl'])));
			$bckimage = mysql_escape_string(escapeshellcmd(strip_tags($_REQUEST['bckimageurl'])));
			$extension=mysql_escape_string(escapeshellcmd(strip_tags($_REQUEST['extension'])));
			
			$bannerExt = explode('.', $bckimage);
			
			$swfsrc="$config[tempfilepath]/$swfurl";
			$bckimagesrc="$config[tempfilepath]/$bckimage";
			$adbase=md5($adrow['imgurl'].rand(0,1000000000));
			$swffile=$adbase.".".$extension;
			$bckimagefile=$adbase.".".$bannerExt[count($bannerExt)-1];
			$swfdest="$config[AD_SWF_DIR]/$swffile";
			$bckimagedest="$config[AD_IMG_DIR]/$adbase".".".$bannerExt[count($bannerExt)-1];
			$rs=false;
		  
		  	error_log("rename($swfsrc,$swfdest)");
		  	error_log("rename($bckimagesrc,$bckimagedest)");
		  	
		  	if($bckimage!=""){
		  	  $sqlSnip=", branded_img_left_ref='$bckimagefile' ";
		  	  rename($bckimagesrc,$bckimagedest);
		  	}
			
		  	if(rename($swfsrc,$swfdest)){
		  		$sql=$conn->Prepare("update ads set playlist_ad_ref=? $sqlSnip where id=?");
		  		$rs=$conn->execute($sql, array($swffile, $ad_id));
				if(!$rs) {
					$msg="Creative uploading failed. Please try some time later.";
					rename($swfsrc,$swfdest);
					rename($bckimagedest,$bckimagesrc);
				}else{
					$msg="Creative uploaded successfully.";
				}
		  	}else{
		  		$msg="Permission Denied.";
		  	}
		}
		$smarty->assign('msg', $msg);
		$smarty->assign('adid', $ad_id);
		$smarty->display("$_REQUEST[page].tpl");
		exit;
	break;
	case "editcampaign":
		if( $_REQUEST['ajax_update'] !="" ){
			$element=$_REQUEST['ajax_update'];
			$_REQUEST['q'] = trim($_REQUEST['q']);
			$eleVal=trim($_REQUEST['q']);
			$ele=explode("-",$element);
			$element = $ele[0];
			if(empty($eleVal) && $element=="desturl"){
				echo "This is required field.";
				$errorFlag = true;
			} elseif ((!empty($eleVal)) && ($element=="desturl" || $element=="trackurl" || $element == "vi_0" || $element == "vi_25" || $element == "vi_50" || $element == "vi_75" || $element == "ae" || $element == "optdesturl")) {
				if (urlValidator($eleVal)=== true) 
					$errorFlag = false;
				else {
					echo "Please enter a valid URL.";
					$errorFlag = true;
				}
			} else if(!empty($eleVal) && $element=="autoPlay"){
				if ($eleVal != "0" && $eleVal != "1" && $eleVal != "2") {
					echo "Please provide either 0, 1 or 2.";
					$errorFlag = true;
				}
			} 
			
			if (!$errorFlag) {
				$ajaxUpd=$_REQUEST['ajax_update'];
				$q=html_entity_decode(getRequest('q'));
				$spl=explode("-",$ajaxUpd);
				$error=null;
				if($spl[0]=="desturl"){
					if( !preg_match( "/^s?https?:\/\/[-_.!~*'()a-zA-Z0-9;\/?:\@&=+\$,%#]+/", $q ) ){
						$error="Destination Url must have http and host name: $q";
					}
				}
				if($spl[0]=="trackurl" && $q != ''){
					if( !preg_match( "/^s?https?:\/\/[-_.!~*'()a-zA-Z0-9;\/?:\@&=+\$,%#]+/", $q ) ){
						$error="Tracker Url must have http and host name: $q";
					}
				}
				
				if(!isset($error)){
					if($q!=$_SESSION['CAMPAIGN'][$spl[1]][$spl[2]][$spl[0]]) {
						$_SESSION['CAMPAIGN'][$spl[1]][$spl[2]][$spl[0]]=$q;
						$_SESSION['CAMPAIGN'][$spl[1]][$spl[2]]['dirty']=true;
						$_SESSION['CAMPAIGN']['urlUpdate']=true;
						
					}
					if($spl[0]=="autoPlay" && $q != ''){
						if ($q == 0)
							echo "Click To Play";
                                                else if($_REQUEST["withmute"]==1)
                                                    echo "Auto Play";
						else if ($q == 1)
							echo "Auto Play without Sound";
						else
							echo "Auto Play with Sound";
					}else if($spl[0]=="muted" && $q != ''){
                                            if ($q == 0)
							echo "Unmute";
                                            else
                                                        echo "Mute";
                                        } 
                                        else
						echo "$_REQUEST[q]";
				}
				else {
					$_SESSION['CAMPAIGN']['urlUpdate']=false;
					echo "$error";
				}
			}
			exit;
		}
		
		if($_REQUEST['cancel_edit']!="") {
			$name=$_SESSION['CAMPAIGN']['name'];
			session_unregister("CAMPAIGN");
			doForward("$config[baseurl]/adv.php?msg=Campaign+$name+not+edited");
		}
		if(!isset($_REQUEST['id'])) {
			doForward("$config[baseurl]/adv.php");
		}
		
		//==========ro list ========
			$sql = "select id , name from ro where validto>now() order by name";
			$rs = $conn->execute($sql);
		    if($rs && $rs->recordcount()>0){
		    	$ROlist = $rs->getrows();			    	
		    }
		    $campaign['ROlist']=$ROlist;
			$smarty->assign("ROlist",$ROlist);
		
		//===========================

			$clientlist=getClientList();
			$smarty->assign('clientList',$clientlist );
			if(sizeof($clientlist)>0){
				$campaign_id=$_REQUEST['id'];
				$adv_id=$_SESSION['ADV_ID'];
				$query = $conn->Prepare("select u.id, u.fname, u.lname, u.email from adv_user u join adv_user_campaign au on u.id = au.adv_user_id where au.advertiser_id =? and au.campaign_id=?");
				$result = $conn->execute($query,array($adv_id,$campaign_id));
				if($result && $result->recordcount()>0){
					$rows = $result->getrows();
				}
				$smarty->assign('sel_clientList',$rows );
			}
			
			
		if(isset($_SESSION['CAMPAIGN']) && $_REQUEST['id']==$_SESSION['CAMPAIGN']['id']) {
                    $obj = json_decode($_SESSION['CAMPAIGN']['dp_expression']);
                    //print_r($obj);
                    foreach ($obj as $key=>$arr) {
                        foreach ($arr as $k=>$val) {
                                //echo $val;
                            $obj[$key][$k] = json_decode($val);
                        }
                    }
                    $_SESSION['CAMPAIGN']['dp_json_expression_arr'] = $obj;
                    $smarty->assign("campaign", $_SESSION['CAMPAIGN']);
                    $smarty->assign('dp_segCount',count($_SESSION['CAMPAIGN']['dp_json_expression_arr'])*480);
                    $smarty->assign("ROlist",$_SESSION['CAMPAIGN']['ROlist']);
                            //echo "<pre>"; print_r($_SESSION['CAMPAIGN']['videobannerads']);
		}else {
			//$sql="select id, budget_currency, model, if(model='CPM',format(price_per_unit*1000 , 2), format(price_per_model, 2)) as maxcpm, format(daily_limit , 2) as daily_limit, format(budget_amt , 2)  as budget_amt, validfrom,validto,name,video_choice,cat_choice,channel_choice, track_choice, timetargetfrom, timetargetto, review_status, device, if(model='CPM',t1.impressions,t1.clicks) as total_consumed from campaign left join (select sum(impressions) as impressions, sum(clicks) as clicks, campaign_id from aggregate_statistics where campaign_id='$_REQUEST[id]' group by campaign_id) t1 on t1.campaign_id=id where id=$_REQUEST[id] and advertiser_id=$_SESSION[ADV_ID]";
			$sql=$conn->Prepare("select campaign.id,
						campaign.name,
						campaign.id AS cid,
						budget_currency, 
						model, 
						cast(if(model='CPM',price_per_unit*1000, price_per_model) as decimal(65,2)) as maxcpm, 
						daily_limit, 
						budget_amt, 
						campaign.timezone,
						validfrom,
						validto,
						name,
						video_choice,
						vast_choice,
						cat_choice,
						channel_choice,
						channel_set, 
						track_choice, 
						overlay_choice, 
						timetargetfrom, 
						timetargetto, 
						review_status,
						campaign.priority_level,
						campaign.pacing_type,
						ro_id,
						custom_details, 
						device 
					from campaign
					LEFT JOIN campaign_UI ON 
						campaign.id = campaign_UI.campaign_id
					where campaign.id=? 
					and campaign.advertiser_id=?");
			$rs=$conn->execute($sql, array($_REQUEST[id], $_SESSION[ADV_ID]));
			if(!$rs || $rs->recordcount()<=0){
				$_REQUEST['msg']="Unable to find campaign";
			}
			else{
		        $selSQL = $conn->Prepare("select original_expr from campaign_expression where campaign_id = ?");
		        $selRES = $conn->execute($selSQL, array($_REQUEST['id']));
		        //var_dump($selRES); 
		        if ($selRES && $selRES->recordcount() > 0) {
			        require_once ABSPATH . "/common/include/class.dataProvider.php";
		         	$obj = new DataProvider();
		            $campaign['dp_expression'] = $obj->generateJsonFrmOrigExpr($selRES->fields['original_expr']);
			        $obj = json_decode($campaign['dp_expression']);
			        //print_r($obj);
			        foreach ($obj as $key=>$arr) {
			            foreach ($arr as $k=>$val) {
			                //echo $val;
			                $obj[$key][$k] = json_decode($val);
			            }
			        }
			        $campaign['dp_json_expression_arr'] = $obj;                                                                                                                                                                                                                                  
		        }
			 	$smarty->assign('dp_segCount',count($campaign['dp_json_expression_arr'])*480);
				
			 	$campaign['id']=$rs->fields['id'];
				$campaign['timezone'] = $rs->fields['timezone'];
				$campaign['priority_level'] = $rs->fields['priority_level'];
				$campaign['pacing_type'] = $rs->fields['pacing_type'];
				$custom_details=(array) json_decode($rs->fields['custom_details']);
				$campaign['branding_title']=$custom_details['branding_title'];
				$campaign['skipoffset']=$custom_details['skipoffset'];
				
				$sqlgeotargeting = $conn->Prepare("select * from geography where campaign_id = ?");
				$rsgeotargeting=$conn->execute($sqlgeotargeting, array($campaign[id]));
				if($rsgeotargeting && $rsgeotargeting->recordcount()>0){
                   	if($rsgeotargeting->fields['country_codes'] !="" || $rsgeotargeting->fields['state_codes'] !="" || $rsgeotargeting->fields['postalcodes'] !=""){
						$campaign['isgeotargetingall']="choosegeotargeting";
						$campaign['countryconcat'] = $rsgeotargeting->fields['country_codes'];
						$campaign['countries'] = split(",", $campaign['countryconcat']);
						$campaign['stateconcat'] = $rsgeotargeting->fields['state_codes'];

					}else{
						$campaign['isgeotargetingall']="geotargetingall";
						$campaign['countries']="";
						$campaign['countryconcat']="";
					}
				}
				$sqltime = $conn->Prepare("select * from campaign_UI where campaign_id = ?");
				$rstime=$conn->execute($sqltime, array($campaign[id]));
				if($rstime && $rstime->recordcount()>0){
					
					
					$campaign['booked'] = $rstime->fields['booked'];
					$campaign['distribution'] = $rstime->fields['distribution'];
					$campaign['daily_icclimit'] = $rstime->fields['daily_consumption'];
					$campaign['capping'] = $rstime->fields['capping'];
					$campaign['session_capping'] = $rstime->fields['session_capping'];
					$campaign['weight'] = $rstime->fields['weight'];
					if($rstime->fields['resettime']!="" && $rstime->fields['resettime']!=NULL){
						$campaign['hour'] = floor(($rstime->fields['resettime']/60));
						$campaign['minute']=floor(($rstime->fields['resettime']-($campaign['hour']*60)));
						$campaign['second']=floor(($rstime->fields['resettime']-($campaign['hour']*60)-$campaign['minute'])*60);
					}
					$campaign['isUI']="yes";
				}
				else{
					$campaign['isUI']="no";
				}


				$campaign['currency']=insert_3dgt_to_val(array("3dgt" => $rs->fields['budget_currency']));
				$campaign['maxcpm']=$rs->fields['maxcpm'];
				$campaign['dailylimit']=$rs->fields['daily_limit'];
				$campaign['budget']=$rs->fields['budget_amt'];
				$campaign['model']=$rs->fields['model'];
				$campaign['review_status']=$rs->fields['review_status'];
				//$campaign['total_consumed'] = $rs->fields['total_consumed'];
				//====ro details start===============
				$campaign['ro_id'] = $rs->fields['ro_id'];
				$sqlro = $conn->Prepare("select * from ro where id = ?");
				$rsro=$conn->execute($sqlro, array($campaign[ro_id]));
				if($rsro && $rsro->recordcount()>0){
					$campaign['roName']=$rsro->fields['name'];
					$campaign['ro_files'] = $rsro->fields['files'];
					$campaign['rotype'] = "chooseRO";
					
				}
				else{
					$campaign['roName']="";
					$campaign['ro_files'] = "";
					$campaign['rotype'] = "";
				}
				//=======ro detials end================
				require_once("Date.php");
				if($rs->fields['validfrom']!=NULL && $rs->fields['validfrom']!=""){
					$start_date=$rs->fields['validfrom'];
					$campaign['view_start_date']=date("M d Y H:i:s", strtotime($start_date));
					$dateObj = new Date($start_date);
		//			$dateObj->setTZByID($campaign['timezone']);		# set local time zone
	//				$dateObj->convertTZByID("GMT");	# convert to foreign time zone
					$dateObj->setTZByID("GMT");		# set local time zone
					$dateObj->convertTZByID($campaign['timezone']);	# convert to foreign time zone
					$campaign['start_date'] = date("M d Y",strtotime($dateObj->format("%Y-%m-%e %T")));
					$campaign['start_time'] = date("H:i:s",strtotime($dateObj->format("%Y-%m-%e %T")));
				}else{
					$campaign['start_date']=NULL;
				}
				
				if($rs->fields['validto']!=NULL && $rs->fields['validto']!=""){
					$end_date=$rs->fields['validto'];
					$campaign['view_end_date']=date("M d Y H:i:s", strtotime($end_date));
					$dateObj = new Date($end_date);
			//		$dateObj->setTZByID($campaign['timezone']);		# set local time zone
		//			$dateObj->convertTZByID("GMT");	# convert to foreign time zone
					$dateObj->setTZByID("GMT");		# set local time zone
					$dateObj->convertTZByID($campaign['timezone']);	# convert to foreign time zone
					$campaign['end_date'] = date("M d Y",strtotime($dateObj->format("%Y-%m-%e %T")));
					$campaign['end_time'] = date("H:i:s",strtotime($dateObj->format("%Y-%m-%e %T")));
				}else {
					$campaign['end_date']=NULL;
				}
				
				$campaign['name']=$rs->fields['name'];
				$campaign['ROfile']=$rs->fields['rofile_name'];
				$campaign['vidtype']=$rs->fields['video_choice'];
				$campaign['vastfeedvidtype']=$rs->fields['video_choice'];
				$campaign['vasttype']=$rs->fields['vast_choice'];
				$campaign['trktype']=$rs->fields['track_choice'];
				$campaign['subtype']=$rs->fields['overlay_choice'];
				if($rs->fields['cat_choice']!="") {
					$campaign['categoryconcat']=$rs->fields['cat_choice'];
					$campaign['categories']=split(",",$rs->fields['cat_choice']);
					$campaign['istargetall']="choosecategory";
				}else{
					$campaign['istargetall']="categoryall";
				}
				
				//---New code start for channel_set --- added by pankaj
				$channel_set = VdoChannelChoice::bitSetToIntList($rs->fields['channel_set']);
				if($channel_set!="") {
					$campaign['site']=explode(",",$channel_set);
					$campaign['issitetargetingall']="choosesitetargeting";

				}else{
					$campaign['issitetargetingall']="sitetargetingall";
					$campaign['site']=NULL;
				}
				//===end -----
				//---This code will remove after channel_set implementation 
				/*
				if($rs->fields['channel_choice']!="") {

						$campaign['site']=explode(",",$rs->fields['channel_choice']);
						$campaign['issitetargetingall']="choosesitetargeting";

				}else{
							$campaign['issitetargetingall']="sitetargetingall";
							$campaign['site']="";
				}
				*/
				
				$campaign['device']=$rs->fields['device'];
				
				$sql=$conn->Prepare("select * from frequency where campaign_id=?");
				$rs=$conn->execute($sql, array($_REQUEST[id]));
				if(!$rs || $rs->recordcount()<=0){
					$_REQUEST['errmsg']="Unable to find frequency cap";
				} else {
					
						if($rs->fields['unit']=="MONTH") $campaign['frequency_per_month']=$rs->fields['impressions'];
					while(!$rs->EOF) {
						if($rs->fields['unit']=="DAY") $campaign['frequency_per_day']=$rs->fields['impressions'];
						if($rs->fields['unit']=="WEEK") $campaign['frequency_per_week']=$rs->fields['impressions'];
						if($rs->fields['unit']=="MONTH") $campaign['frequency_per_month']=$rs->fields['impressions'];
						$rs->movenext();
					}
				}
				
				# campaign limit inclusion
				include_once(dirname(__FILE__).'/../common/portal/campaign/CampaignLimit.php');
				$cmpgnLimit = new CampaignLimit();
				$cmpgnLimit->get($_REQUEST['id'], &$campaign);
				
				$sql=$conn->Prepare("select ads.* from ads, campaign_members where isnull(ads.parent_ad_id) and ads.ad_format<>'sri_interact' and ads.id=campaign_members.ad_id and ads.advertiser_id=? and campaign_members.cid=?");
				$rs=$conn->execute($sql, array($_SESSION[ADV_ID], $_REQUEST[id]));
				if(!$rs || $rs->recordcount()<=0){
					$_REQUEST['adserr']="Unable to find ads";
				} else {
					$textads=array();
					$bannerads=array();
					$videoads=array();
					$brandedads=array();
					$trackerimages=array();
					$trackerswfs=array();
					$dblcliktags=array();
					$videobannerads=array();
					$youtubevbads=array();
					$vastfeedads=array();
					$swfid_array=array();
					$dim_array=array();
					$campaignBannerArr=array();
					$overlayswfs=array();
                                        $ytprerollads=array();
					$k=0;
                                       
					while(!$rs->EOF) {
						if($rs->fields['ad_format']=="video" && $rs->fields['action_type']=="vastfeed"){
  					      $campaign['type']=$rs->fields['action_type'].$rs->fields['ad_format']."advertisement";
  					    }elseif($rs->fields['ad_format']=='tracker' || $rs->fields['ad_format']=='banner' || $rs->fields['ad_format']=='vdobanner'){
  					    	$campaign['type']="trackeradvertisement";
  					    }elseif($rs->fields['ad_format']=='ytpreroll'){
  					    	$campaign['type']="ytvideoadvertisement"; 
  					    }else{ 
  					    	$campaign['type']=$rs->fields['ad_format']."advertisement"; 
  					    }
  					    
						if($rs->fields['branded_color']) $campaign['campaniontype']=$rs->fields['branded_color'];
						
						if($rs->fields['ad_format']=="video" && $rs->fields['action_type']=="vastfeed"){
						  $vastfeedad['id']=$rs->fields['id'];
						  $vastfeedad['vastfeedname']=$rs->fields['branded_img_right_ref_imgname'];
						  $vastfeedad['vastfeedurl']=$rs->fields['custom_canvas_url'];
						  $vastfeedads[]=$vastfeedad;
						}elseif($rs->fields['ad_format']=='video'){
							$videoad['id']=$rs->fields['id'];
							$videoad['uri']=substr($rs->fields['playlist_ad_ref'],0,strlen($rs->fields['playlist_ad_ref'])-4);
							$videoad['name']=$rs->fields['branded_img_right_ref_imgname'];
							$videoad['desturl']=$rs->fields['destination_url'];
							$videoad['dimension']=$rs->fields['dimension'];
							$videoad['duration']=$rs->fields['duration'];
							$videoad['trackurl']=$rs->fields['tracker_url'];
							$ad_details = (array) json_decode($rs->fields['ad_details']);
							$videoad['vi_0']=$ad_details['vdo_tvi_0'];
							$videoad['vi_25']=$ad_details['vdo_tvi_25'];
							$videoad['vi_50']=$ad_details['vdo_tvi_50'];
							$videoad['vi_75']=$ad_details['vdo_tvi_75'];
							$videoad['ae']=$ad_details['vdo_tae'];
							$videoad['campaniontype']=$rs->fields['branded_color'];
							$swfid_array = explode("," ,$rs->fields['branded_img_bot_ref_txbody']);
							$dim_array = explode("," ,$rs->fields['dimension']); 
							
							$dimstr = "";
							$swfidstr = "";
							
							if($rs->fields['branded_img_bot_ref_txbody']!=""){
								for($j=0;$j<count($swfid_array);$j++) {
									$dimstr.=",".$dim_array[$j];
									$swfidstr.=",".$swfid_array[$j];
									$campaignBannerArr[$k]['new'] = "";
									$campaignBannerArr[$k]['id'] = $rs->fields['id'];
									$campaignBannerArr[$k]['name'] = $rs->fields['branded_img_right_ref_imgname'];
									$campaignBannerArr[$k]['swfid'] = $swfid_array[$j];
									$campaignBannerArr[$k]['dim'] = $dim_array[$j];
									$dim = explode("x",$dim_array[$j]);
									$campaignBannerArr[$k]['width'] = $dim[0];
									$campaignBannerArr[$k]['height'] = $dim[1];
									$k++;
								}
							}
							
							$videoad['dimession'] = substr($dimstr,1);
							$videoad['swfid'] = substr($swfidstr,1);
							$videoad['imgid'] = substr($swfidstr,1);
							
							# fetch interactive details
							$sriDetail = json_decode($rs->fields['sri_details']);
							
							# background banner image
							if($sriDetail->backimg!=""){
								$fileArr=explode('/',$sriDetail->backimg);
								$campaign['backgroundbannerurl']=$fileArr[sizeof($fileArr)-1];
							}
							
							$var=new SRIPortal();
							$isTTM="false";
							$sriads = $var->createSession($_REQUEST[id], &$isTTM);
							
							$campaign['isTTM']=$isTTM;
							$campaign ['shareTitle'] = $sriDetail->srititles[0];
							$campaign['respondTitle'] = $sriDetail->srititles[1];
							$campaign['interactTitle'] =$sriDetail->srititles[2];
							$campaign['adId']= $rs->fields['id'];
							
							$videoads[]=$videoad;
						} elseif ($rs->fields['ad_format']=='branded') {
							$brandedad['id']=$rs->fields['id'];
							$brandedad['name']=$rs->fields['branded_img_right_ref_imgname'];
							$brandedad['desturl']=$rs->fields['destination_url'];
							$brandedad['trackurl']=$rs->fields['tracker_url'];
							$brandedad['color']=$rs->fields['branded_color'];
							$brandedad['imgurl']=$rs->fields['branded_img_top_ref_txtitle'];
							$brandedad['logo']=$rs->fields['branded_logo_ref'];
							$brandedad['dimension']=$rs->fields['dimension'];
							$campaign['type']="brandedplayer";
							$brandedads[]=$brandedad;
						}else if ($rs->fields['ad_format']=='ytpreroll') {
								$ytprerollad['id']				= $rs->fields['id'];
								//$videobannerad['uri']			= substr($rs->fields['playlist_ad_ref'],0,strlen($rs->fields['playlist_ad_ref'])-4);
								//$youtubevbad['videobannerurl']	= $rs->fields['branded_logo_ref'];
								$ytprerollad['vdoID']			= $rs->fields['playlist_ad_ref'];
								$ytprerollad['name']			= $rs->fields['branded_img_right_ref_imgname'];
								$ytprerollad['dimension']		= $rs->fields['dimension'];
								$ytprerollad['optdesturl']		= $rs->fields['destination_url'];
								$ytprerollad['trackurl']		= $rs->fields['tracker_url'];
								$ad_details = (array) json_decode($rs->fields['ad_details']);
								$ytprerollad['vi_0']			= $ad_details['vdo_tvi_0'];
								$ytprerollad['vi_25']			= $ad_details['vdo_tvi_25'];
								$ytprerollad['vi_50']			= $ad_details['vdo_tvi_50'];
								$ytprerollad['vi_75']			= $ad_details['vdo_tvi_75'];
								$ytprerollad['ae']				= $ad_details['vdo_tae'];
								$ytprerollad['autoPlay']		= $ad_details['autoPlay'];
                                                                $ytprerollad['autoAfter']		= $ad_details['autoAfter'];
                                                                $ytprerollad['muted']		= $ad_details['muted'];
								$campaign['adId']= $rs->fields['id'];
                                                                $campaign['type']="ytvideoadvertisement";
								$ytprerollads[] = $ytprerollad;
								//print_r($youtubevbads);exit("kkk");
						}elseif ($rs->fields['ad_format']=='tracker' || $rs->fields['ad_format']=='banner' || $rs->fields['ad_format']=='vdobanner') {
							if($campaign['trktype']=='dblclik'){
								$dblcliktag['id']=$rs->fields['id'];
								$dblcliktag['imgname']=$rs->fields['branded_img_right_ref_imgname'];
								$dblcliktag['imgsize']=$rs->fields['dimension'];
								$dblcliktag['desturl']=$rs->fields['destination_url'];
								$dblcliktag['tracker_url']=$rs->fields['tracker_url'];
								$dblcliktag['dblclicktag']=$rs->fields['tracker_url'];
								$trackerurls = explode('|', $dblcliktag['tracker_url']);
								$trackerurl = explode('~',$trackerurls[3]);
								$dblcliktag['imgurl']=$trackerurl[1];
								$trackerurl = explode('~',$trackerurls[2]);
								$dblcliktag['ancurl']=$trackerurl[1];
								$dblcliktag['trktype']="dblclik";
								$dblcliktags[]=$dblcliktag;
							}elseif($campaign['trktype']=='trackerimage' ||$campaign['trktype']=='extimage'){
								$trackerimage['id']=$rs->fields['id'];
								$trackerimage['imgname']=$rs->fields['branded_img_right_ref_imgname'];
								$trackerimage['imgsize']=$rs->fields['dimension'];
								$trackerimage['desturl']=$rs->fields['destination_url'];
                                $trackerimage['trackurl']=$rs->fields['tracker_url'];
				                if($campaign['trktype']=='extimage')
				                  $trackerimage['imgurl']=$rs->fields['tracker_url'];
				                else
				                  $trackerimage['imgurl']=$rs->fields['playlist_ad_ref'];

								$trackerimage['trktype']=$campaign['trktype'];
								$trackerimages[]=$trackerimage;
							}
							elseif($campaign['trktype']=='videobanner'){
								$videobannerad['id']=$rs->fields['id'];
								$videobannerad['uri']=substr($rs->fields['playlist_ad_ref'],0,strlen($rs->fields['playlist_ad_ref'])-4);
								$videobannerad['videobannerurl']=$rs->fields['branded_logo_ref'];
								$videobannerad['name']=$rs->fields['branded_img_right_ref_imgname'];
								$videobannerad['dimension']=$rs->fields['dimension'];
								$videobannerad['desturl']=$rs->fields['destination_url'];
								$videobannerad['trackurl']=$rs->fields['tracker_url'];
								$ad_details = (array) json_decode($rs->fields['ad_details']);
								$videobannerad['vi_0']=$ad_details['vdo_tvi_0'];
								$videobannerad['vi_25']=$ad_details['vdo_tvi_25'];
								$videobannerad['vi_50']=$ad_details['vdo_tvi_50'];
								$videobannerad['vi_75']=$ad_details['vdo_tvi_75'];
								$videobannerad['ae']=$ad_details['vdo_tae'];
								$sriDetail = json_decode($rs->fields['sri_details']);

								if($sriDetail->topimg!=""){
									$fileArr=explode('/',$sriDetail->topimg);
									$videobannerad['topbannerurl']=$fileArr[sizeof($fileArr)-1];
								}
								if($sriDetail->botimg!=""){
									$fileArr=explode('/',$sriDetail->botimg);
									$videobannerad['bottombannerurl']=$fileArr[sizeof($fileArr)-1];
								}
								if($sriDetail->backimg!=""){
									$fileArr=explode('/',$sriDetail->backimg);
									$campaign['backgroundbannerurl']=$fileArr[sizeof($fileArr)-1];
								}
							    if($sriDetail->advlogo!=""){
									$fileArr=explode('/',$sriDetail->advlogo);
									$campaign['advlogourl']=$fileArr[sizeof($fileArr)-1];
								}
								$campaign['isMuted']=$sriDetail->isMuted;
								$campaign['autoplay']=$sriDetail->autoplay;
								$campaign['gridview']=$sriDetail->gridview;
								$campaign['mouseover']=$sriDetail->mouseover;
								
								$var=new SRIPortal();
								$isTTM="false";
								$sriads = $var->createSession($_REQUEST[id],&$isTTM);
								
								
								$campaign['isTTM']=$isTTM;
								$campaign ['shareTitle'] = $sriDetail->srititles[0];
								$campaign['respondTitle'] = $sriDetail->srititles[1];
								$campaign['interactTitle'] =$sriDetail->srititles[2];
								$campaign['adId']= $rs->fields['id'];
								
								$videobannerads[]=$videobannerad;
							}else if ($campaign['trktype']=='youtubevb') {
								$youtubevbad['id']				= $rs->fields['id'];
								//$videobannerad['uri']			= substr($rs->fields['playlist_ad_ref'],0,strlen($rs->fields['playlist_ad_ref'])-4);
								//$youtubevbad['videobannerurl']	= $rs->fields['branded_logo_ref'];
								$youtubevbad['vdoID']			= $rs->fields['playlist_ad_ref'];
								$youtubevbad['name']			= $rs->fields['branded_img_right_ref_imgname'];
								$youtubevbad['dimension']		= $rs->fields['dimension'];
								$youtubevbad['optdesturl']		= $rs->fields['destination_url'];
								$youtubevbad['trackurl']		= $rs->fields['tracker_url'];
								$ad_details = (array) json_decode($rs->fields['ad_details']);
								$youtubevbad['vi_0']			= $ad_details['vdo_tvi_0'];
								$youtubevbad['vi_25']			= $ad_details['vdo_tvi_25'];
								$youtubevbad['vi_50']			= $ad_details['vdo_tvi_50'];
								$youtubevbad['vi_75']			= $ad_details['vdo_tvi_75'];
								$youtubevbad['ae']				= $ad_details['vdo_tae'];
								$youtubevbad['autoPlay']		= $ad_details['autoPlay'];
								$campaign['adId']= $rs->fields['id'];
								$youtubevbads[] = $youtubevbad;
								//print_r($youtubevbads);exit("kkk");
							} else {
								$trackerswf['id']=$rs->fields['id'];
								$trackerswf['imgname']=$rs->fields['branded_img_right_ref_imgname'];
					                if($campaign['trktype']=='extswf')
					                    $trackerswf['imgurl']=$rs->fields['tracker_url'];
					                else {
					                    $trackerswf['imgurl']=$rs->fields['playlist_ad_ref'];
					                }
					            $trackerswf['bckimageurl']=$rs->fields['branded_img_left_ref'];    
								$trackerswf['desturl']=$rs->fields['destination_url'];
								$trackerswf['trackurl']=$rs->fields['tracker_url'];
								$trackerswf['trktype']="trackerswf";
								$trackerswfs[]=$trackerswf;
							}
						} elseif ($rs->fields['branded_img_top_ref_txtitle']!="" ) {
							$textad['id']=$rs->fields['id'];
							$textad['title']=$rs->fields['branded_img_top_ref_txtitle'];
							$textad['body']=$rs->fields['branded_img_bot_ref_txbody'];
							$textad['desturl']=$rs->fields['destination_url'];
							$textads[]=$textad;
							$campaign['subtype']="textadvertisement";
						} else {
							
							if($campaign['subtype']=='banneradvertisement'){
								$bannerad['id']=$rs->fields['id'];
								$bannerad['imgname']=$rs->fields['branded_img_right_ref_imgname'];
								$bannerad['imgurl']=$rs->fields['playlist_ad_ref'];
								$bannerad['desturl']=$rs->fields['destination_url'];
								$bannerad['trackurl']=$rs->fields['tracker_url'];
								$campaign['subtype']="banneradvertisement";
								$bannerads[]=$bannerad;
							}
							elseif($campaign['subtype']=='overlayswf'){
								$overlayswf['id']=$rs->fields['id'];
								$overlayswf['imgname']=$rs->fields['branded_img_right_ref_imgname'];
								$overlayswf['imgurl']=$rs->fields['playlist_ad_ref'];
								$overlayswf['desturl']=$rs->fields['destination_url'];
								$overlayswf['trackurl']=$rs->fields['tracker_url'];
								$overlayswf['subtype']="overlayswf";
								$overlayswfs[]=$overlayswf;
							}
						}
						$rs->movenext();
					}
					$campaign['textads']=$textads;
					$campaign['bannerads']=$bannerads;
					$campaign['videoads']=$videoads;
					$campaign['brandedads']=$brandedads;
					$campaign['trackerimages']=$trackerimages;
					$campaign['trackerswfs']=$trackerswfs;
					$campaign['dblcliktags']=$dblcliktags;
					$campaign['videobannerads']=$videobannerads;
					$campaign['youtubevbads']=$youtubevbads;
					$campaign['vastfeedads']=$vastfeedads;
					$campaign['sriads']=$sriads;
					$campaign['campaignBannerArr']=$campaignBannerArr;
					$campaign['overlayswfs']=$overlayswfs;
                                        $campaign['ytvideoads']=$ytprerollads;
          			if($campaign['trktype']=='extimage')
            			$campaign['extinfo']=$trackerimages;
          			if($campaign['trktype']=='extswf')
            			$campaign['extinfo']=$trackerswfs;
				}
				session_register("CAMPAIGN");
				$_SESSION["CAMPAIGN"]=$campaign;
				$var=new TargetingUI(); 
				$var->createSession($_REQUEST['id']);
				$BT_UI=new BT_UI(); 
               			$BT_UI->createSession($_REQUEST['id']);
				$RT_UI=new RetargetingUI();
				$RT_UI->createSessionFromDB($_REQUEST['id']);
				$campaign['at']=$_SESSION['CAMPAIGN']['at'];
				$campaign['bt']=$_SESSION['CAMPAIGN']['bt']; 
				 $campaign['rt']=$_SESSION['CAMPAIGN']['rt'];
				 $campaign['rt_text']=$_SESSION['CAMPAIGN']['rt_text'];
				 $campaign['rt_duration']=$_SESSION['CAMPAIGN']['rt_duration'];
				$smarty->assign("campaign", $campaign);
			}
			
		}

	break;
	case "new_campaignsummary":
		if($_REQUEST['excelreport']=="yes"){
			$blank[]=array("");
			$colHead[]=array("RO Name","Ad Unit","Campaign Name","Status","Start Date","End Date","Impression TARGET","Impression Delivered","Clicks","CTR","Remaining Impression","Avg daily delivery","Target daily delivery","Delivery variation");
			performance_exportToExcel($_SESSION['header'],$colHead,$_SESSION['data'],$blank);
		}
		else 
		{
			//======================================================
				$smarty->assign('statusoptions',$statusoptions);
			    $status = requestParams("status");
			    if($status == ""){
			    	$status = "active";
			    }
			    	$_REQUEST['status']=$status;
			    if($status != "all"){
			    	$statusstring = "and campaign.status = '$status'";
			    }
				
				if(isset($_SESSION['ADV_PUB_ID'])){
					if(isset($_REQUEST['action_approve'])){
				
						$campaignid=$_REQUEST['campaign_id_toapprove'];
						if(!empty($campaignid)){
							$sql=$conn->Prepare("select campaign.name, campaign.track_choice, campaign_UI.campaignstatus from campaign, campaign_UI where campaign.id=campaign_UI.campaign_id and id=? and advertiser_id=?");
							$rs = $conn->execute($sql, array($campaignid, $_SESSION[ADV_ID]));
							if($rs && $rs->recordcount()>0){
								$campaign_name=$rs->fields['name'];
								if($rs->fields['track_choice']=="trackerswf" || $rs->fields['track_choice']=="trackerimage" || $rs->fields['track_choice']=="extswf" || $rs->fields['track_choice']=="extimage" || $rs->fields['track_choice']=="dblclik"){
									$campaign_status=$rs->fields['campaignstatus'];
								}else{
									$campaign_status="Approved";
								}
							
								$sql = $conn->Prepare("update campaign set review_status = ? where id=? and review_status = 'Pending Review'");
								$rs = $conn->execute($sql, array($campaign_status, $campaignid));
								$rows =$conn->Affected_Rows();
								if($rows>0){
									$error = "Campaign ".$campaign_name." approved successfully";
				
								}else{
									$error = "Only campaigns in Pending Review can be approved";
								}
								
								
							}
						}else{
							$error = "No campaign selected";
						}
			
			
					}
				}
				
				if($_REQUEST['action_paused']!=null){
					$campaignids=$_REQUEST['campaignids'];
					if(!empty($campaignids)){
						$ids = mysql_escape_string("(".implode( ',', $campaignids ).")");
						$sql = "update campaign set status = 'paused' where id in $ids and status = 'active'";
						$rs = $conn->execute($sql);
						$rows = $conn->Affected_Rows();
						if($rows>0){
							$error = "Campaign(s) paused successfully";
		
						}else{
							$error = "Only active campaigns can be paused";
						}
					}else{
						$error = "No campaign selected to be paused";
					}
		
		
				}elseif($_REQUEST['action_resumed']!=null){
					$campaignids=$_REQUEST['campaignids'];
					if(!empty($campaignids)){
						$ids = mysql_escape_string("(".implode( ',', $campaignids ).")");
						$sql = "update campaign set status = 'active' where id in $ids and status = 'paused'";
						$rs = $conn->execute($sql);
						$rows = $conn->Affected_Rows();
						if(count($campaignids)==$rows){
							$error = "Campaign(s) resumed successfully";
		
						}else{
							$error = "Only paused campaigns can be resumed";
						}
					}else{
						$error = "No campaign selected to be resumed";
					}
		
				}elseif($_REQUEST['action_deleted']!=null){
					if(!empty($campaignids)){
						$campaignids=$_REQUEST['campaignids'];
						$ids = "(".implode( ',', $campaignids ).")";
						$sql = "update campaign set status = 'deleted' where id in $ids";
						$rs = $conn->execute($sql);
						$rows = $conn->Affected_Rows();
						if($rows>0){
							$error = "Campaign deleted successfully";
		
						}
					}else{
						$error = "No campaign selected to be deleted";
					}
		
				}
		
				$dates=processDates($_REQUEST,'timeperiod',$_SESSION['ADV_ID']);
				$_REQUEST['start_date']=date($viewformat, strtotime($dates[0]));
				$_REQUEST['end_date']=date($viewformat, strtotime($dates[1]));
				$smarty->assign("err", $error);
				if($_REQUEST['time']=='dropdown' && $_REQUEST['timeperiod']=='alltime') {
					$datequery="";
				} else {
					$datequery=" and date>='$dates[0]' and date<='$dates[1]'";
				}
				
			//======================================================
					
					 $sql = $reportConn->Prepare("SELECT 
								
								
								campaign.price_per_unit,
								(campaign.price_per_unit * 1000 ) as cpm_price_per_unit,
								campaign.id AS cid,								
					 			campaign.id AS c_id,
								campaign.validfrom,
								campaign.validto,
								model,
								price_per_model, 
								budget_amt, 
								budget_currency, 
								daily_limit, 
								sum( impressions ) AS timp, 
								sum( clicks ) AS tclicks, 
								sum( total ) *1000 AS total1000, 
								budget_currency, 
								campaign.status as cstatus , 
								review_status, 
								campaign_UI.ro_id,
								ro.name as ROName,
								ad_format,
								group_concat( cast( blocked_campaigns.channel_id AS char ) SEPARATOR ',' ) AS channel 
							FROM campaign 
							LEFT JOIN 
								campaign_UI ON 
									campaign.id = campaign_UI.campaign_id 
							LEFT JOIN 
								aggregate_statistics ON 
									campaign.id = aggregate_statistics.campaign_id 
							LEFT JOIN 
								blocked_campaigns ON 
									campaign.id = blocked_campaigns.campaign_id
							LEFT JOIN 
								ro ON 
									campaign_UI.ro_id = ro.id
							LEFT JOIN 
								campaign_members ON 
									campaign.id = campaign_members.cid
							LEFT JOIN 
								ads ON 
									campaign_members.ad_id = ads.id
							WHERE 
								campaign.device='pc' 
								and campaign.advertiser_id=?
								$datequery 
								$statusstring
							GROUP BY 
								c_id 
							order by 
								ro_id DESC,ad_format desc ");
			$rs=$reportConn->execute($sql, array($_SESSION[ADV_ID]));
			if($rs && $rs->recordcount()>0){
				$campaignsummary=$rs->getrows();
				for($i=count($campaignsummary)-1;$i>=0;$i--) {
					if($campaignsummary[$i]['ROName']=="")
						$campaignsummary[$i]['ROName'] ="Other";
					$campaignsummary[$i]['ctr'] = $campaignsummary[$i]['tclicks']*100/$campaignsummary[$i]['timp'];	
					$campaignsummary[$i]['budget_remaining'] = $campaignsummary[$i]['budget_amt']-$campaignsummary[$i]['tot_money_spent'];	
					$campaignsummary[$i]['imp_target'] = round($campaignsummary[$i]['budget_amt']/$campaignsummary[$i]['price_per_model']);	
					$campaignsummary[$i]['imp_remaining'] = round($campaignsummary[$i]['budget_amt']/$campaignsummary[$i]['price_per_model'])-$campaignsummary[$i]['timp'];	
					$totaldays = (strtotime($campaignsummary[$i]['validto']) - strtotime($campaignsummary[$i]['validfrom'])) / (60 * 60 * 24);
					$remaining_days = (strtotime($campaignsummary[$i]['validto']) - strtotime(date("Y-m-d"))) / (60 * 60 * 24);
					$consume_days = (strtotime(date("Y-m-d")) - strtotime($campaignsummary[$i]['validfrom'])) / (60 * 60 * 24);
					$campaignsummary[$i]['remaining_days'] = $remaining_days;	
					$campaignsummary[$i]['avg_daily_delivery'] = $campaignsummary[$i]['timp']/$consume_days;	
					$campaignsummary[$i]['target_daily_delivery'] = round($campaignsummary[$i]['imp_target']/$totaldays);	
					$campaignsummary[$i]['delivery_variation'] =((($campaignsummary[$i]['avg_daily_delivery']-$campaignsummary[$i]['target_daily_delivery'])*100)/$campaignsummary[$i]['target_daily_delivery']); 	
				
				}			
			}
			
			$report_header=array();
			$report_header[]=array('Report Generated on', date('d-m-Y H:i T'));
			$report_header[]=array("Start Date",$startdate);
			$report_header[]=array("End Date",$enddate);
			$report_header[]=array("Channel",ucfirst($channel));
			$report_header[]=array("Campaign",ucfirst($campaign));
			$report_header[]=array("Country",ucfirst($country));
			$report_header[]=array("State",ucfirst($state));
			$report_header[]=array("Ad Format",ucfirst($adtype)); 	
			$_SESSION['header'] = $report_header;
			
			
			$_SESSION['header'] = "Campaign Performance Report";
			$_SESSION['data'] = $campaignsummary;
			$smarty->assign('campaignsummary',$campaignsummary);
		}
	break; 
}


?>
