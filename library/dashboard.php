<?php
ini_set('memory_limit', '64M');
include_once('include/datefuncs.php');
include_once('include/countryMaps.class.php');
require_once('include/smartyBlockFunction.php');
include('./charts/Includes/FusionCharts.php');
require_once dirname(__FILE__) .'/../common/portal/report/ReportHelper.php';

$mapFlag=0; 
$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);

if(isset($config['DB_PREFIX']) && $config['DB_PREFIX']!=''){
	$db_prefix =$config['DB_PREFIX'];
}else {
	$db_prefix='';
}
switch ($_REQUEST['sub']) {
    
    case 'viewdashboard':
        /*
        //generate query for this weekly
        //echo "$config[baseurl]/generatexml.php?type=monthly";
        if ($_REQUEST['pubgraph']=='monthly'){
            $chart= renderChart("$config[baseurl]/charts/FusionCharts/MSLine.swf", "$config[baseurl]/generatexml.php?type=monthly", "", "chart2", 900, 450, false, false);
        }else{
            $chart= renderChart("$config[baseurl]/charts/FusionCharts/MSLine.swf", "$config[baseurl]/generatexml.php?type=weekly", "", "chart1", 900, 450, false, false);
        }
        
        $smarty->assign("chart",$chart);
        */
        $smarty->assign("ddoptions",$ddoptions);
        $dates=processDates($_REQUEST,'timeperiod',$_SESSION['PUB_ID']);
        $_REQUEST['start_date']=date($viewformat, strtotime($dates[0]));
        $_REQUEST['end_date']=date($viewformat, strtotime($dates[1]));
        if($_REQUEST['time']=='dropdown' && $_REQUEST['timeperiod']=='alltime') {
            $datequery=" and date=date_sub(date(now()), interval 1 day)";
        } else {
            $datequery=" and date>='$dates[0]' and date<='$dates[1]'";
        }

        $sql = "select channels.name as sitename, sum(impressions) as impressions, adtype as 'campaign_type' from channels, daily_stats ds where publisher_id='$_SESSION[PUB_ID]' and channels.channel_type!='iphone' and ds.channel_id=channels.id $datequery group by channel_id,adtype";
        
        $rs = $reportConn->execute($sql);
        if($rs && $rs->recordcount()>0){
            $summary = $rs->getrows();

        }
        $smarty->assign("data",$summary);

        break;
    case 'campperf':
        $smarty->assign("ddoptions",$ddoptions);
        $smarty->assign("campperf","true");
		
        $dates=processDates($_REQUEST,'timeperiod',$_SESSION['PUB_ID']);
        
        $_REQUEST['start_date']=date($viewformat, strtotime($dates[0]));
        $_REQUEST['end_date']=date($viewformat, strtotime($dates[1]));
       
        
        if($_REQUEST['time']=='dropdown' && $_REQUEST['timeperiod']=='alltime') {
            $datequery=" and date>='".date('Y-m-d',strtotime($dates[1]. '-1 year'))."' and date<='$dates[1]'";
        } else {
            $datequery=" and date>='$dates[0]' and date<='$dates[1]'";
        }

        $sql="select id, name from channels where publisher_id=".$_SESSION['PUB_ID'];
        $rs = $reportConn->execute($sql);
        if($rs && $rs->recordcount()>0){
            $channels = $rs->getrows();
        }
        $smarty->assign("channels",$channels);

        if($_REQUEST['channel']!=0) {
            $sitestring=" and channels.id=".$_REQUEST['channel'];
        }
 	
 		$sql = "select
			channels.name,
			geography.country_codes, 
			geography.state_codes, 
			concat(sec_to_time(timetargetfrom*60),' GMT to ',sec_to_time(timetargetto*60),' GMT') as timetarget,
			concat(frequency.impressions,' impressions every ',frequency.unit) as frequencytarget,
			sum(ccperf.impressions) as newviews, 
			campaign.name as campaign_name,
			date(campaign.validfrom) as startdate, 
			date(campaign.validto) as enddate, 
			ad_format as campaign_type,
			sum(clicks)/sum(ccperf.impressions)*100 as ctr, 
			campaign.status as status1, 
			if(campaign.review_status = 'Approved','Live','Not live') as status2  
	from 
		{$db_prefix}channel_campperf as ccperf 
		inner join channels on channels.id=ccperf.channel_id
		inner join campaign on ccperf.campaign_id=campaign.id 
		inner join geography on geography.campaign_id = ccperf.campaign_id 
		inner join frequency on frequency.campaign_id = ccperf.campaign_id 
		inner join ads on ccperf.ad_id=ads.id 	
	where 
		publisher_id='$_SESSION[PUB_ID]' and ccperf.channel_id=channels.id  and 
		verified='Verified' and review_status <> 'Test' $sitestring $datequery 
	group by  ccperf.campaign_id,ccperf.channel_id order by status1 asc";
		
        $rs = $reportConn->execute($sql);
        if($rs && $rs->recordcount()>0){
            $summary = $rs->getrows();

        }
        $smarty->assign("summary",$summary);

        break;
    case 'pubreport':
        if($_REQUEST['excelreport']=="yes"){
            $_SESSION['isEI']="no";
            $blank[]=array("");
            if($_SESSION['period']=="summary"){
                if($_SESSION['reporttype']=="inventory")
                    $colHead[]=array("Country","Country Code","Video Views","Ad Plays","Utilization","eCPM ".$_SESSION['CURRENCYSYM']);
                elseif($_SESSION['reporttype']=="newinventory")
                    $colHead[]=array("Region", "Channel", "Ad Type", "Player Loads", "Ad Calls", "Call Drops", "Unfilled Inventory", "Ad Plays", "Utilization", "Efficiency", "eCPM ".$_SESSION['CURRENCYSYM']);    
               $colHead[]=array(ucfirst($_SESSION['reporttype']),"Impressions","Ads Played","Clicks","CTR","EC","ECR","Revenues ".$_SESSION['CURRENCYSYM'],"eCPM ".$_SESSION['CURRENCYSYM'],'Video Completes','CR(%)');
                
                if(($_SESSION['reporttype']=="campaign" && $_SESSION['adtype']=='tracker')){
                	unset($colHead[0][getArrayKey($colHead[0],'EC')]);
                	unset($colHead[0][getArrayKey($colHead[0],'ECR')]);
                }
                if(!($_SESSION['reporttype']=="campaign")){
                	unset($colHead[0][getArrayKey($colHead[0],'EC')]);
                	unset($colHead[0][getArrayKey($colHead[0],'ECR')]);
                }
                if(!in_array('r.cpcvcolumn', $_SESSION['CAPABILITIES'])){
                	unset($colHead[0][getArrayKey($colHead[0],'Video Completes')]);
                	unset($colHead[0][getArrayKey($colHead[0],'CR(%)')]);
                }
                
                if(!($_SESSION['reporttype']=="campaign" && $_SESSION['adtype']=='tracker')){
                	unset($colHead[0][getArrayKey($colHead[0],'Ads Played')]);
                }
                foreach($colHead[0] as $key=>$val){
                	$colhead[0][]=$val;
                } 
                exportToExcel($_SESSION['header'],$colhead,$_SESSION['data'],$blank,$_SESSION['period']);
             
            }
            else{
                if($_SESSION['reporttype']=="inventory")
                    $colHead[]=array("Date","Video Views","Ad Plays","Utilization","eCPM ".$_SESSION['CURRENCYSYM']);
                elseif($_SESSION['reporttype']=="newinventory")
                    $colHead[]=array("Date", "Channel", "Ad Type", "Player Loads", "Ad Calls", "Call Drops", "Unfilled Inventory", "Ad Plays", "Utilization", "Efficiency", "eCPM ".$_SESSION['CURRENCYSYM']);
                $colHead[]=array("Date","Impressions","Ads Played","Clicks","CTR","EC","ECR","Revenues ".$_SESSION['CURRENCYSYM'],"eCPM ".$_SESSION['CURRENCYSYM'],'Video Completes','CR(%)');
                
                if(($_SESSION['reporttype']=="campaign" && $_SESSION['adtype']=='tracker')){
                	unset($colHead[0][getArrayKey($colHead[0],'EC')]);
                	unset($colHead[0][getArrayKey($colHead[0],'ECR')]);
                }
                if(!($_SESSION['reporttype']=="campaign")){
                	unset($colHead[0][getArrayKey($colHead[0],'EC')]);
                	unset($colHead[0][getArrayKey($colHead[0],'ECR')]);
                }
                if(!in_array('r.cpcvcolumn', $_SESSION['CAPABILITIES'])){
                	unset($colHead[0][getArrayKey($colHead[0],'Video Completes')]);
                	unset($colHead[0][getArrayKey($colHead[0],'CR(%)')]);
                }
                
                if(!($_SESSION['reporttype']=="campaign" && $_SESSION['adtype']=='tracker')){
                	unset($colHead[0][getArrayKey($colHead[0],'Ads Played')]);
                }
                foreach($colHead[0] as $key=>$val){
                	$colhead[0][]=$val;
                }
                exportToExcel($_SESSION['header'],$colhead,$_SESSION['data'],$blank,$_SESSION['period']);
            }
        }
        elseif ($_REQUEST['pdfreport']=="yes"){
            $blank[]=array("");
            if($_SESSION['period']=="summary"){
                if($_SESSION['reporttype']=="inventory")
                    $colHead[]=array("Country","Country Code","Video Views","Ads Served","Utilization","eCPM ".$_SESSION['CURRENCYSYM']);
                elseif($_SESSION['reporttype']=="newinventory")
                    $colHead[]=array("Region", "Ad Type","Calls Made", "UI","Ads Served", "Utilization", "Efficiency", "eCPM ".$_SESSION['CURRENCYSYM']);
                $colHead[]=array(ucfirst($_SESSION['reporttype']),"Impressions","Ads Played","Clicks","CTR","EC","ECR","Revenues ".$_SESSION['CURRENCYSYM'],"eCPM ".$_SESSION['CURRENCYSYM'],'Video Completes','CR(%)');
                
                if(($_SESSION['reporttype']=="campaign" && $_SESSION['adtype']=='tracker')){
                	unset($colHead[0][getArrayKey($colHead[0],'EC')]);
                	unset($colHead[0][getArrayKey($colHead[0],'ECR')]);
                }
                if(!($_SESSION['reporttype']=="campaign")){
                	unset($colHead[0][getArrayKey($colHead[0],'EC')]);
                	unset($colHead[0][getArrayKey($colHead[0],'ECR')]);
                }
                if(!in_array('r.cpcvcolumn', $_SESSION['CAPABILITIES'])){
                	unset($colHead[0][getArrayKey($colHead[0],'Video Completes')]);
                	unset($colHead[0][getArrayKey($colHead[0],'CR(%)')]);
                }
               
                if(!($_SESSION['reporttype']=="campaign" && $_SESSION['adtype']=='tracker')){
                	unset($colHead[0][getArrayKey($colHead[0],'Ads Played')]);
                }
                foreach($colHead[0] as $key=>$val){
                	$colhead[0][]=$val;
                }
                //echo "<pre>"; print_r($_SESSION['data']);
                exportToPdf($_SESSION['header'],$colhead,$_SESSION['data'],$_SESSION['period']);
            }
            else{
                if($_SESSION['reporttype']=="inventory")
                    $colHead[]=array("Date","Video Views","Ads Served","Utilization","eCPM ".$_SESSION['CURRENCYSYM']);
                elseif($_SESSION['reporttype']=="newinventory")
                    $colHead[]=array("Date", "Ad Type", "Calls Made", "UI","Ads Served", "Utilization", "Efficiency", "eCPM ".$_SESSION['CURRENCYSYM']);
                 $colHead[]=array("Date","Impressions","Ads Played","Clicks","CTR","EC","ECR","Revenues ".$_SESSION['CURRENCYSYM'],"eCPM ".$_SESSION['CURRENCYSYM'],'Video Completes','CR(%)');
                
                if(($_SESSION['reporttype']=="campaign" && $_SESSION['adtype']=='tracker')){
                	unset($colHead[0][getArrayKey($colHead[0],'EC')]);
                	unset($colHead[0][getArrayKey($colHead[0],'ECR')]);
                }
                if(!($_SESSION['reporttype']=="campaign")){
                	unset($colHead[0][getArrayKey($colHead[0],'EC')]);
                	unset($colHead[0][getArrayKey($colHead[0],'ECR')]);
                }
                if(!in_array('r.cpcvcolumn', $_SESSION['CAPABILITIES'])){
                	unset($colHead[0][getArrayKey($colHead[0],'Video Completes')]);
                	unset($colHead[0][getArrayKey($colHead[0],'CR(%)')]);
                }
                
                if(!($_SESSION['reporttype']=="campaign" && $_SESSION['adtype']=='tracker')){
                	unset($colHead[0][getArrayKey($colHead[0],'Ads Played')]);
                }
                foreach($colHead[0] as $key=>$val){
                	$colhead[0][]=$val;
                }
                exportToPdf($_SESSION['header'],$colhead,$_SESSION['data'],$_SESSION['period']);
            }
        }
        else{
            $_SESSION['currentReport']=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $smarty->assign('timeperiodoptions',$timeperiodoptions);
            if(!isset($_REQUEST['dropdown'])){
                $_REQUEST['time']= "dropdown";
            }
            if(!isset($_REQUEST['timeperiod'])){
                $_REQUEST['timeperiod']= "yesterday";
            }
    
            $reporttypeArr = array("campaign" => "Campaign Summary Report", "channel" => "Channel Summary Report", "country" => "Country Summary Report", "adtype" => "Ad Format Summary Report","inventory" => "Inventory Report", "ad" => "Ad Performance Report", "newinventory" => "New Inventory Report");
//          if(in_array('rw.new_inventory_report', $_SESSION['CAPABILITIES'])){
//              $reporttypeArr['newinventory']="New Inventory Report";
//          }
//          $reporttypeArr['newinventory']="New Inventory Report";
            $smarty->assign("reporttype", $reporttypeArr);
            
            
            if(!isset($_REQUEST['start_date'])){
                
                $fromdate = date("Y-m-d", mktime(0, 0, 0, date("m") , 1, date("Y")));
                $todate = date("Y-m-d", mktime(0, 0, 0, date("m")+1 , date("d")-date("d"), date("Y")));
                                       
                $_REQUEST['start_date']=$fromdate;
                $_REQUEST['end_date']=$todate;
            }
            
            $country=$_GET['country'];
            $state=$_GET['state'];
            $smarty->assign("countrycodes", getCountryList());
            $smarty->assign("statecodes", getStateList($country));
            $smarty->assign('country',$country);
            $smarty->assign('state',$state);
            
            
            if(!isset($_REQUEST['adtype']))$_REQUEST['adtype']='all';
            if(!isset($_REQUEST['reporttype']))$_REQUEST['reporttype']='campaign';
            if(!isset($_REQUEST['channel']))$_REQUEST['channel']='all';
            if(!isset($_REQUEST['campaign']))$_REQUEST['campaign']='all';
            if(!isset($_REQUEST['country']))$_REQUEST['country']='all';
            if(!isset($_REQUEST['state']))$_REQUEST['state']='all';
            if(!isset($_REQUEST['period']))$_REQUEST['period']='summary';
            
            
            $adtypeoptions = array(
                array('id'=>'all', 'name' => "All"),
                array('id'=>'preroll', "name"=>"Preroll"),
                array('id'=>'midroll', "name"=>"Midroll"),
                array('id'=>'postroll' , 'name' => "Postroll"),
                array('id'=>'vdobanner', 'name' => "Video Banner"),
		array('id'=>'banner', 'name' => "Banner"),
		array('id'=>'ytpreroll', 'name' => "Youtube Preroll"),
                array('id'=>'branded', 'name' => "Branded Player"),
                array('id'=>'overlay' , 'name' => "Overlay"),
                array('id'=>'tracker' , 'name' => "Tracker")    
            );
            
            $smarty->assign("adtypeoptions",$adtypeoptions);
            $smarty->assign("ddoptions",$ddoptions);
            $smarty->assign("pubreport","true");

            $reporttype=getRequest('reporttype');
            $channel= getRequest('channel');
            $campaign = getRequest('campaign');
            $adtype = getRequest('adtype');
            $country= getRequest('country');
            $state= getRequest('state');
            $period = getRequest('period');
            $_SESSION['adtype']=$adtype;            
            
            
            $startdate = getRequest('start_date');
            $enddate = getRequest('end_date');
            
            $campaignArr['campaign'] =  $campaign;
            $channelArr['channel'] =  $channel;
            $countryArr['cat'] =  $country;
            $stateArr['state'] =  "$country:$state";
            //$stateArr['count'] =  $country;
            if($country!="all")
                $countryName = insert_country_names($countryArr);
            else
                $countryName = "All";
            
            if($state=="all" || $state == "")
                $stateName = "All";
            else
                $stateName = insert_state_names($stateArr);
                        
            $campaignName = insert_campaign_names($campaignArr,'excel');
            $channelName = insert_channel_names($channelArr,'excel');
            
            $report_header=array();
            $report_header[]=array(ucfirst($reporttype)." Summary Report(" . ucfirst($period) . ")");
            $report_header[]=array('Report Generated on', date('d-m-Y H:i T'));
            $report_header[]=array("Start Date",$startdate);
            $report_header[]=array("End Date",$enddate);
            
            $report_header[]=array("Channel",ucfirst($channelName));
            $report_header[]=array("Campaign",ucfirst($campaignName));
            $report_header[]=array("Country",ucfirst($countryName));
            $report_header[]=array("State",ucfirst($stateName));
            
            
            //$report_header[]=array("Channel",ucfirst($channel));
            //$report_header[]=array("Campaign",ucfirst($campaign));
            //$report_header[]=array("Country",ucfirst($country));
            //$report_header[]=array("State",ucfirst($state));
            $report_header[]=array("Ad Format",ucfirst($adtype));
            $_SESSION['header'] = $report_header;
            
            $channellist = array();
            $sql = "select id,name from channels where publisher_id='$_SESSION[PUB_ID]' and channel_type!='iphone' order by name";
            $channellist=getData($sql,$reportConn);
                  
            $smarty->assign("channellist",$channellist);
             
             $campaignlist = array();
            //$sql = "select name, id from campaign where id in (select distinct(campaign_id) from daily_stats where channel_id in (select id from channels where publisher_id='$_SESSION[PUB_ID]'))";
            $sql = "select campaign.name, campaign.id from campaign left join {$db_prefix}daily_stats on campaign.id=campaign_id left join channels on channels.id=channel_id where channels.publisher_id='$_SESSION[PUB_ID]' and campaign.device!='iphone' and {$db_prefix}daily_stats.date>='$startdate' and {$db_prefix}daily_stats.date<='$enddate'  group by {$db_prefix}daily_stats.campaign_id order by campaign.name";
            $campaignlist =getData($sql,$reportConn);
            $smarty->assign("campaignlist",$campaignlist);
              
            
            if(isset($startdate) && isset($enddate)) {
                $datequery_aa=" and aggregate_ads.date>='$startdate' and aggregate_ads.date<='$enddate' "; 
            } 
            else{
                $datequery_aa="";
            }
                    
            if($channel=="all") 
            {
                $sitestring=" ";
            }
            else{
                $sitestring=" and {$db_prefix}daily_stats.channel_id='$channel'";
            }
                    
            if($campaign=="all"){
                $campaignstring=" ";
            }else{
                $campaignstring=" and {$db_prefix}daily_stats.campaign_id='$campaign'";
            }
            
            if($adtype!="all")
            {
                $adtypestring=" and {$db_prefix}daily_stats.adtype='$adtype'";
                $adtypestring_aa=" and {$db_prefix}daily_stats.ad_format='$adtype'";
            }else{
                $adtypestring=" ";
                $adtypestring_aa=" ";
            }
            
            if($country=="all"){
                $countrystring=" ";
            }else{
                $countrystring=" and {$db_prefix}daily_stats.ccode='$country'";
            }
            
            if($state=="all"){
                $provincestring=" ";
            }else{
                $provincestring=" and  {$db_prefix}daily_stats.state_code='$state'";
            }
            
            if($adtype=="copreroll" || $adtype=="copostroll"){
              $impr="if({$db_prefix}daily_stats.adtype like 'co%', {$db_prefix}daily_stats.impressions, 0)";
              $clkr="if({$db_prefix}daily_stats.adtype like 'co%', {$db_prefix}daily_stats.clicks, 0)";
            }else{
              $impr="if({$db_prefix}daily_stats.adtype not like 'co%', {$db_prefix}daily_stats.impressions, 0)";
              $clkr="if({$db_prefix}daily_stats.adtype not like 'co%', {$db_prefix}daily_stats.clicks, 0)";
            }
            
            if($reporttype=="adtype"){
              $impr="{$db_prefix}daily_stats.impressions";
            }
            else{
              $impr="if({$db_prefix}daily_stats.adtype not like 'co%', {$db_prefix}daily_stats.impressions, 0)";
            }
            
            //ideally number of elements in group_by_array should be equal to number of elements in outpt_array
            $group_by_array=array();
            $output_array=array();
            
            switch ($period){
                case "daily":
                    //$group = " group by ";
                    $group_by_array[]="{$db_prefix}daily_stats.date";
                    $output_array[]="{$db_prefix}daily_stats.date as timeframe";
                    //$aggregate="daily_stats.date";
                    $aggregate_aa=" day({$db_prefix}aggregate_ads.date), month({$db_prefix}aggregate_ads.date), year({$db_prefix}aggregate_ads.date) ";
                    //$aggregateNames=", daily_stats.date as timeframe ";
                    $aggregateNames_aa=", {$db_prefix}aggregate_ads.date as timeframe ";
                    //$agcolNames="Date,";
                    //$orderby="Order By daily_stats.date";
                    $orderby_aa="Order By {$db_prefix}aggregate_ads.date";
                    break;
                case "monthly":
                   // $group = " group by ";
                    $group_by_array[]="month({$db_prefix}daily_stats.date)";
                    $group_by_array[]="year({$db_prefix}daily_stats.date)";
                    $aggregate_aa=" month({$db_prefix}aggregate_ads.date),year({$db_prefix}aggregate_ads.date) ";
                    $output_array[]="concat_ws(' ', monthname({$db_prefix}daily_stats.date), year({$db_prefix}daily_stats.date)) as timeframe";
                    $aggregateNames_aa=", concat_ws(' ', monthname({$db_prefix}aggregate_ads.date), year({$db_prefix}aggregate_ads.date)) as timeframe ";
                   // $agcolNames="Timeframe,";
                    //$orderby="Order By month(daily_stats.date)";
                    $orderby_aa="Order By month({$db_prefix}aggregate_ads.date)";
                    break;
                    case "summary":
                        //$group = " group by ";
                break;
            }
                        
            switch($reporttype){
                case "campaign":
                    $group_by_array[]="{$db_prefix}daily_stats.campaign_id";
					$output_array[]="{$db_prefix}daily_stats.campaign_id as idx_key";                    
                    $output_array[]="campaign.name";
                    $output_array[]="campaign_id as uid";
                    $group_condition_aa=" {$db_prefix}aggregate_ads.campaign_id";
                   
                    $left_join_aa=" left join channels on {$db_prefix}aggregate_ads.channel_id=channels.id left join campaign on {$db_prefix}aggregate_ads.campaign_id=campaign.id left join ads on ads.id={$db_prefix}aggregate_ads.ad_id ";
                    
                    // $where=" parent_ad_id IS NULL and ";
                    //$aggregateNames.=", campaign.name,campaign_id as uid  ";
                    break;
                case "channel":
                    $group_by_array[]="{$db_prefix}daily_stats.channel_id";
                     //$group_by_array[]="daily_stats.adtype";
                    $output_array[]="{$db_prefix}daily_stats.channel_id as idx_key";
                    $output_array[]="channels.name";
                    $output_array[]="channels.id as uid";
                    break;
                    
                case "country":
                     $group_by_array[]="{$db_prefix}daily_stats.ccode";
                    // $group_by_array[]="daily_stats.channel_id";
                    // $group_by_array[]="daily_stats.adtype";
                    $output_array[]="if({$db_prefix}daily_stats.ccode='','Other',{$db_prefix}daily_stats.ccode) as idx_key";
                    $output_array[]="if({$db_prefix}daily_stats.ccode='','Other',{$db_prefix}daily_stats.ccode) as name";
                     $output_array[]="if({$db_prefix}daily_stats.ccode='','Other',{$db_prefix}daily_stats.ccode) as uid ";
                    break;
                case "adtype":
                   // $group_by_array[]="{$db_prefix}daily_stats.channel_id";
                    $group_by_array[]="{$db_prefix}daily_stats.adtype";
                    $output_array[]="{$db_prefix}daily_stats.adtype as idx_key";
                    $output_array[]="{$db_prefix}daily_stats.adtype as name";
                    $output_array[]="{$db_prefix}daily_stats.adtype as uid";
                    break;
                case "ad":
                    $group_by_array[]="{$db_prefix}daily_stats.ad_id";
                   // $group_by_array[]="daily_stats.channel_id";
                    //$group_by_array[]="daily_stats.adtype";
                    $output_array[]="{$db_prefix}daily_stats.ad_id as idx_key";
                    $output_array[]="if(ads.branded_img_right_ref_imgname!='',ads.branded_img_right_ref_imgname,'Null') as name";
                    $output_array[]="if({$db_prefix}daily_stats.ad_id='','Other',{$db_prefix}daily_stats.ad_id) as uid";
                    break;  
                default:
                    break;
            }
            
            # New inventory report
            if($reporttype=="newinventory"){
                $countryLists = getCountryList();
                $region = $_REQUEST['region'];
                if($region) $region=implode(',', $region); else $region='Overall';
                
                # Include InventoryReport class
                include_once(dirname(__FILE__).'/../common/portal/report/InventoryReport.php'); 
                
                # Create an object of InventoryReport
                $inventoryReport = new InventoryReport();
                
                # Call InventoryReport method
                $summary = $inventoryReport->report($period, $adtype, $startdate, $enddate,$_SESSION['PUB_ID'], $channel, $region, "PC");
                if($period == "summary"){
                    $_SESSION['reporttype']=$reporttype;
                    $_SESSION['period']='summary';
                    $_SESSION['data']=$summary;
                    $smarty->assign("data",$summary);
                }
                else{
                    foreach ( $summary as $key => $value ){
                        $data[$value['region']][] = $value;
                    }
                    $_SESSION['reporttype']=$reporttype;
                    $_SESSION['period']=$period;
                    $_SESSION['data']=$data;
                    $smarty->assign("data",$data);
                }
            }elseif($reporttype=="inventory")
            {
                $disabled  = "disabled";
                $smarty->assign("disabled",$disabled);
                $smarty->assign("countrycodes", getCountryList());
                if($channel=="all"){
                    $sitestring1="";
                    $sitestring2="";
                }else{
                    $sitestring1=" and ac.channel_id='$channel'";
                    $sitestring2=" and ds.channel_id='$channel'";
                }
                if($campaign=="all"){
                    $campaignstring=" ";
                }else{
                    $campaignstring=" and ds.campaign_id='$campaign'";
                }
                if($country=="all"){
                    $countrystring1=" ";
                    $countrystring2=" ";
                }else{
                    $countrystring1=" and ac.ccode='$country'";
                    $countrystring2=" and ds.ccode='$country'";
                }
                if($state=="all"){
                    $provincestring=" ";
                }else{
                    $provincestring=" and ds.state_code='$state'";
                }
                if($adtype!="all")
                {
                    $adtypestring=" and ds.adtype='$adtype'";
                }else{
                    $adtypestring=" ";
                }
                
                if($period!="summary")
                {
                    //$group_by2 = "day(ds.date),month(ds.date),year(ds.date),";
                    //$group_by1 = "day(ac.date),month(ac.date),year(ac.date),";
                    $date_str = "and t1.date=t2.sdate";
                    switch ($period){
                        case "daily":
                            $group_by2 = "day(ds.date),month(ds.date),year(ds.date),";
                            $group_by1 = "day(ac.date),month(ac.date),year(ac.date),";
                            $dateFrame=", date";
                            $orderby="Order By date";
                            break;
                        case "monthly":
                            $group_by2 = "month(ds.date),year(ds.date),";
                            $group_by1 = "month(ac.date),year(ac.date),";
                            $dateFrame=", concat_ws(' ', monthname(date), year(date)) as date ";
                            
                            $agcolNames="Timeframe,";
                            $orderby="Order By month(date)";
                            break;
                        case "quarterly":
                            
                            $group_by1=" quarter(ac.date),year(ac.date), ";
                            $group_by2=" quarter(ds.date),year(ds.date), ";
                            $dateFrame=", concat_ws(' ', concat('Q',quarter(date)), year(date)) as date ";
                            $agcolNames="Timeframe,";
                            $orderby="Order By quarter(date)";
                            break;
                        case "dayofweek":
                            $group_by1=" dayofweek(ac.date), year(ac.date), ";
                            $group_by2=" dayofweek(ds.date), year(ds.date), ";
                            $dateFrame=", date_format(date, '%a') as date ";
                            $agcolNames="Day,";
                            $orderby="Order By dayofweek(date)";
                            break;
                        case "weekly":
                            $group_by1=" week(ac.date), year(ac.date), ";
                            $group_by2=" week(ds.date), year(ds.date), ";
                            $dateFrame=", concat('Week','-',week(date)+1) as date ";
                            $agcolNames="Week,";
                            $orderby="Order By week(date)";
                            break;
                        case "yearly":
                            $group_by1=" year(ac.date), ";
                            $group_by2=" year(ds.date) ,";
                            $dateFrame=", year(date) as timeframe ";
                            $agcolNames="Year,";
                            $orderby="Order By year(date)";
                            break;
                        case "summary":
                            $group_by1="";
                            $group_by2="";
                            $dateFrame=", date";
                            $agcolNames="Year,";
                            $orderby="Order By year(date)";
                            break;
                        default:
                            $aggregate=" ";
                            $aggregateNames="";
                            $agcolNames="";
                            $orderby="";
                            $date_str ="";
                            break;
                    }
                }
                
            $sql = "select if(ccode='','Other',ccode) as ccode $dateFrame,total_call,total_views,round((total_views*100/total_call),4) as ut, revenue, round(revenue*1000/total_views,4) as eCPM  from
                             (
                                (
                                    select ac.ccode, 
                                    ac.date,
                                    sum(ac.calls) as total_call 
                                    from aggregated_call ac 
                                    left join channels c 
                                    on c.id=ac.channel_id 
                                    left join publisher p 
                                    on p.id=c.publisher_id 
                                    where c.channel_type!='iphone' and ac.date>='$startdate' and ac.date<='$enddate' $sitestring1 $countrystring1 and p.id='$_SESSION[PUB_ID]' 
                                    group by $group_by1 ac.ccode
                                ) t1 
                                left join 
                                    (
                                        select ds.ccode as country_code,
                                        ds.date as sdate, 
                                        sum(if(ds.adtype not like 'co%', ds.impressions, 0)) as total_views,
                                        sum(ds.pubrevenue_advcurrency) as revenue 
                                        from daily_stats ds 
                                        left join channels c 
                                        on ds.channel_id=c.id 
                                        left join publisher p 
                                        on p.id=c.publisher_id 
                                        where c.channel_type!='iphone' and ds.date>='$startdate' and ds.date<='$enddate' $campaignstring $sitestring2 $countrystring2 $provincestring $adtypestring and p.id='$_SESSION[PUB_ID]' 
                                        group by $group_by2 ds.ccode 
                                    )t2 
                                    on t1.ccode=t2.country_code $date_str 
                                ) $orderby";
                    
                    //echo $sql;
                    if(isset($reporttype)){
                        $rs = $reportConn->execute($sql);
                    }
                    $i=0;
                    $countries = getCountryList();
                    if($rs && $rs->recordcount()>0){
                        $mapFlag=1;
                        while(!$rs->EOF) {
                            $obj = new CountryCodeMaps;
                            $Country_code = $rs->fields['ccode'];
                            $Country_ID = $obj->getNumber_Maps($Country_code);
                            if($rs->fields['ccode']!="Other"){
                                if($countries[$rs->fields['ccode']]){
                                    $cname=ucfirst($countries[$rs->fields['ccode']]);
                                }
                                else{
                                    $cname=ucfirst($rs->fields['ccode']);
                                }
                            }
                            else{
                                $cname=ucfirst($rs->fields['ccode']);
                            }
                            $summary[$i]['ccode']=$rs->fields['ccode'];
                            $summary[$i]['date']=$rs->fields['date'];
                            $summary[$i]['cname']=$cname;
                            $total_calls = $rs->fields['total_call'];
                            $summary[$i]['total_call']=$rs->fields['total_call'];
                            $summary[$i]['total_views']=$rs->fields['total_views'];
                            $summary[$i]['ut']=$rs->fields['ut'];
                            $summary[$i]['revenue']=$rs->fields['revenue'];
                            $summary[$i]['eCPM']=$rs->fields['eCPM'];
                            $rs->movenext();
                            $i++;
                        }
                    }
                    if($period == "summary"){
                    $_SESSION['reporttype']=$reporttype;
                    $_SESSION['period']='summary';
                    $_SESSION['data']=$summary;
                    $smarty->assign("data",$summary);
                }
                else{
                    foreach ( $summary as $key => $value ){
                        $data[$value['ccode']][] = $value;
                    }
                    $_SESSION['reporttype']=$reporttype;
                    $_SESSION['period']=$period;
                    $_SESSION['data']=$data;
                    $smarty->assign("data",$data);
                }
                //===============================Map===========================
                //ABhay
                $sqlMaps = "select ac.ccode, 
                                    sum(ac.calls) as total_call 
                                    from aggregated_call ac 
                                    left join channels c 
                                    on c.id=ac.channel_id 
                                    left join publisher p 
                                    on p.id=c.publisher_id 
                                    where c.channel_type!='iphone' and ac.date>='$startdate' and ac.date<='$enddate' and p.id='$_SESSION[PUB_ID]' 
                                    group by ac.ccode";
                    
                    //echo $sqlMaps;
                    if(isset($reporttype)){
                        $rsMaps = $reportConn->execute($sqlMaps);       
                    }
                    
                    # Declare strXML to store dataXML of the map    
                    $strXML="";
                    
                    # Opening MAP element
                    $strXML = "<map showLabels='0' includeNameInLabels='1' borderColor='FFFFFF' fillAlpha='80' showBevel='0' legendPosition='Bottom' >";
                    
                    # Setting Color ranges : 4 color ranges for population ranges
                    $strXML .= "<colorRange>";
                    $strXML .= "<color minValue='1' maxValue='50000' displayValue='Video Views :Below 50K' color='CC0001' />";
                    $strXML .= "<color minValue='50000' maxValue='250000' displayValue='Video Views :50K - 250M' color='FFD33A' />";
                    $strXML .= "<color minValue='250000' maxValue='1000000' displayValue='Video Views :250K - 1M' color='069F06' />";
                    $strXML .= "<color minValue='1000000' maxValue='10000000' displayValue='Video Views :1M-10M' color='ABF456' />";
                    $strXML .= "<color minValue='10000000' maxValue='10000000000' displayValue='Video Views : >10M' color='ABCDEF' />";
                    $strXML .= "</colorRange><data>";
                    $i=0;
                    $countries = getCountryList();
                    if($rsMaps && $rsMaps->recordcount()>0 && $mapFlag==1){
                        while(!$rsMaps->EOF) {
                            $obj = new CountryCodeMaps;
                            $Country_code = $rsMaps->fields['ccode'];
                            $Country_ID = $obj->getNumber_Maps($Country_code);                          
                            $total_calls = $rsMaps->fields['total_call'];
                            $strXML .= "<entity id='" .$Country_ID. "' value='" .$total_calls. "' />";
                            $rsMaps->movenext();
                            $i++;
                        }
                    }
                     # closing  data element    
                    $strXML .= "</data>";
                    # closing map element
                    $strXML  .= "</map>";
                    //echo "<textarea rows='9' cols='80'>$strXML</textarea>";
                    if($adtype=="branded")
                        $tips="yes";
                    $map=renderMap("$config[baseurl]/charts/FusionCharts/FCMap_NewWorld.swf","",$strXML,"firstMap", 850, 460,false,false);
                    $smarty->assign("map",$map);
                    $smarty->assign("tips",$tips);
            }//enventory end                        
            //===================================================================
            else {
                // added by Pankaj 
              //  $group_condition.= " ,daily_stats.ccode ";
               // $aggregateNames.=" ,daily_stats.ccode as ccode";
            	$helper = new ReportHelper($reportConn);
            	$helper->createPriceConfigTable($startdate,$enddate, 'pc');
            	$helper->createPriceConfigTable($startdate,$enddate, 'pc1',false);
                $revfields="pubrevenue_advcurrency*ex_rate_advpub ";
                 $revold="sum(if(pc.channel_id is NULL ,$revfields,if(pc.model='CPM',impressions*pc.price_per_unit,if(pc.model='CPCV',completes*pc.price_per_unit,clicks*pc.price_per_unit))))";
            
                
                $sumimpressions="sum($impr)";
                $ds_click="if(ads.parent_ad_id IS NULL,{$db_prefix}daily_stats.clicks,0)";
                $totclicks="sum($ds_click)";
                $effective_clicks="sum({$db_prefix}daily_stats.clicks)";
                
                $rev="sum(if(pc1.channel_id is null, if(pc.channel_id is null,pubrevenue_advcurrency*ex_rate_advpub,if(pc.model='CPM',
					  impressions*pc.price_per_unit, if(ads.parent_ad_id is null,{$db_prefix}daily_stats.clicks,0)*pc.price_per_unit)),
					  if(pc1.model='CPM', impressions*pc1.price_per_unit, if(ads.parent_ad_id is
					  null,{$db_prefix}daily_stats.clicks,0)*pc1.price_per_unit)))";
                 
             $sql = "select  
                                $sumimpressions as totimpressions, 
                                $totclicks as totclicks, 
                                ($totclicks*100)/$sumimpressions as ctr,
                                $effective_clicks as ec,
                                ($effective_clicks*100)/$sumimpressions as ecr, 
                                $rev as rev,
                                $revold as rev_old, 
                                sum(completes) as vdocompletes,
                                round(((sum(completes)/$sumimpressions)*100),2) as cr,
                                ($rev*1000/$sumimpressions) as eCPM
                                ,".implode(",",$output_array)." 
                            from 
                                {$db_prefix}daily_stats join channels on {$db_prefix}daily_stats.channel_id=channels.id join campaign on {$db_prefix}daily_stats.campaign_id=campaign.id join ads on ads.id={$db_prefix}daily_stats.ad_id
                                 left join
						 			tmp.pc as pc on (pc.adtype = {$db_prefix}daily_stats.adtype and ({$db_prefix}daily_stats.date>=pc.start_date and {$db_prefix}daily_stats.date <= pc.end_date) and {$db_prefix}daily_stats.channel_id=pc.channel_id) 
									left join tmp.pc1 as pc1 on ({$db_prefix}daily_stats.ccode = pc1.ccode and pc1.adtype = {$db_prefix}daily_stats.adtype and ({$db_prefix}daily_stats.date>=pc1.start_date and {$db_prefix}daily_stats.date <= pc1.end_date) and {$db_prefix}daily_stats.channel_id=pc1.channel_id)              
                            where 
                                $where channels.publisher_id = '$_SESSION[PUB_ID]' and channels.channel_type!='iphone'  
                                $sitestring $campaignstring $adtypestring 
                                $countrystring $provincestring and {$db_prefix}daily_stats.date>='$startdate' and {$db_prefix}daily_stats.date<='$enddate'";
            if(count($group_by_array)>0){ 
                $sql.=" group by ".implode(",",$group_by_array);
            }
                $data=$summary=getData($sql,$reportConn);
                $helper->dropPriceConfigTable('pc');
                $helper->dropPriceConfigTable('pc1');
                $data_main=array();
                
               $dat="";
                foreach($data as $key => $val){
                	$dat=$val['uid'];
                	if($period=="summary"){
                    $data_main[$val['idx_key']]=$val;
                	}
                	else{
                		 $data_main[$val['idx_key']][]=$val;
                	}
                }
                $data=$data_main;                

                # merge column ads played
                if($adtype=='tracker' && $reporttype=='campaign'){
                  if($period!="summary" && $reporttype=='campaign'){
                    $where="";
                    $group_cond = " $group_condition_aa, $aggregate_aa ";
                  }else{
                    $group_cond = " $aggregate_aa $group_condition_aa ";
                  }
                 $trkSql="select
                    {$db_prefix}aggregate_ads.campaign_id, {$db_prefix}aggregate_ads.ad_id,
                    sum(if(campaign.track_choice='videobanner',if({$db_prefix}aggregate_ads.measure='vi_0',{$db_prefix}aggregate_ads.value,0),0)) as ads_played
                    $aggregateNames_aa
                  from 
                    {$db_prefix}aggregate_ads
                    $left_join_aa 
                  where 
                    $where channels.publisher_id = '$_SESSION[PUB_ID]' and channels.channel_type!='iphone' 
                    $sitestring $campaignstring $adtypestring_aa 
                    $datequery_aa $group  
                    $group_cond
                  $orderby_aa";
                  $trkRs = $reportConn->execute($trkSql);
                   
                  if($trkRs && $trkRs->recordcount()>0){
                    $trkData = $trkRs->getrows();
                    //echo "<pre>"; print_r($trkData); print_r($summary);
                    if($period=="summary"){
                      //foreach($summary as $skey => $sval){  data=summary
                        foreach($data as $skey => $sval){
                        foreach($trkData as $tkey => $tval){
                          if($sval['campaign_id']==$tval['campaign_id']){
                            $data[$skey]['ads_played']=$tval['ads_played'];
                          }
                        }
                      }
                    }else{
                      foreach($data as $skey => $sval){
                        foreach($trkData as $tkey => $tval){
                          if($sval['timeframe']==$tval['timeframe'] && $sval['campaign_id']==$tval['campaign_id']){
                            $data[$skey]['ads_played']=$tval['ads_played'];
                          }
                        }
                      }
                    }
                  }
                }
              
                $_SESSION['reporttype']=$reporttype;
                $_SESSION['period']=$period;
                //var_dump($data);//exit;
                $_SESSION['data']=$data;
                //var_dump($_SESSION);
                $smarty->assign("data",$data);
            }
        }
    break;
}
 
function getData($sql,$conn){
    require_once ABSPATH."/common/adserving/redis/RedisLocalCache.php";
    $RedisLocalCache=new RedisLocalCache();
    $key="pubrep:".md5($sql);
    $data = false; unserialize($RedisLocalCache->getKeyVal($key));
    if(isset($data)==false||$data==false){
         $rs = $conn->execute($sql);   
         $conn->SetFetchMode(ADODB_FETCH_ASSOC);
         if($rs && $rs->recordcount()>0){
            $data = $rs->getrows();
            //$RedisLocalCache->setKeyValDB($key, serialize($data));
          }
    }
    return $data;
}
?>
