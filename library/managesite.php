<?php

include_once('include/datefuncs.php');



switch ($_REQUEST['sub']) {
	case 'site':

		if($_REQUEST['action_verify']!="") {
			$siteid=mysql_escape_string($_REQUEST['siteid']);
			if($_SESSION['ADMIN_PRIVILEGES']=="superadmin"  || $_SESSION['ADMIN_PRIVILEGES']=="sales"){
					$sql="update channels set verified='Verified' where id=$siteid";
					$conn->execute("$sql");
						
			}else{
					
					$sql="select url, apikey from channels where id=$siteid";
					$rs=$conn->execute($sql);
					$url=completeUrl($rs->fields['url']);
					//$vfile=$url."/dev/pankaj_adserver/vdopia".$rs->fields['apikey'].".html";
					$vfile=$url."/vdopia".$rs->fields['apikey'].".html";
					$handle=fopen("$vfile","r");
					if($handle==null) {
						$_REQUEST['msg']="Verification failed: $vfile does not exist";
					}
					else {
						$_REQUEST['msg']='Verification Successful';
						$sql="update channels set verified='Verified' where id=$siteid";
						$conn->execute("$sql");
					}
			
			}
			
			
			
		}

		$sql = "select channels.id as id, name, url, apikey, verified, approval, cast(video_unit_price*1000 as decimal) as videocpm, cast(branded_unit_price * 1000 as decimal)  as brandedcpm, cast(overlay_unit_price * 1000 as decimal) as overlaycpm from channels left join publishersheet on channels.id=publishersheet.channel_id and isnull(publishersheet.validto) where publisher_id = '$_SESSION[PUB_ID]' order by name";
		$rs = $conn->execute($sql);
		if($rs && $rs->recordcount()>0){
			$data = $rs->getrows();
			$smarty->assign('data',$data);
		}
		
		if($_REQUEST['action_paused']!=null){
			if(!empty($campaignids)){
				$campaignids=$_REQUEST['campaignids'];
				$ids = "(".implode( ',', $campaignids ).")";
				$sql = "update campaign set status = 'paused' where id in $ids and status = 'active'";
				$rs = $conn->execute($sql);
				$rows = $conn->Affected_Rows();
				if($rows>0){
					$error = "Campaign paused successfully";

				}else{
					$error = "Only active campaigns can be paused";
				}
			}
		}
		break;

	case 'pubadoptions':
		try {
			
			/*echo "<pre>";
			print_r($_REQUEST);
			exit;*/
			if($_REQUEST['action_pubadoptions']!=null){
				/* TODO: add rules */
				$site['name']=mysql_escape_string($_REQUEST['name']);
				$site['url']=mysql_escape_string(canonicalizeHostUrl($_REQUEST['url']));
				$site['apikey'] = md5("".rand(0,4000000000)."".rand(0,4000000000)."".rand(0,4000000000)."".rand(0,4000000000));
				$site['approval']=$_REQUEST['approval'];

				$sql = "insert into channels set name = '$site[name]', url = '$site[url]', approval='$site[approval]', publisher_id = '$_SESSION[PUB_ID]', apikey='$site[apikey]'";
				$conn->execute($sql);
				$id = $conn->Insert_ID();

				if($id<=0) {
					throw new Exception("Conflicting URL Please retry");
				}

				$site['brandedcpm']=$_REQUEST['brandedcpm']/1000.0;
				$site['videocpm']=$_REQUEST['videocpm']/1000.0;
				$site['overlaycpm']=$_REQUEST['overlaycpm']/1000.0;

				$sql = "insert into publishersheet set video_unit_price = '$site[videocpm]', overlay_unit_price = '$site[overlaycpm]', branded_unit_price='$site[brandedcpm]', margin = '$config[defaultmargin]', channel_id=$id, validfrom=now()";
				$rs = $conn->execute($sql);
				if($rs==false) {
					throw new Exception('Unknown Error');
				}
				doForward("$config[baseurl]/pub.php?page=managesite");
			}
		} catch (Exception  $e) {
			$error['globalerror']=$e->getMessage();
			$smarty->assign('error', $error);
		}
		break;
	case 'editpubadoptions':
		try{
			if ($_REQUEST['action_pubadoptions']!=null) {
				/* TODO: add rules */
				$id=mysql_escape_string($_REQUEST['id']);
				$site['name']=mysql_escape_string($_REQUEST['name']);
				$site['url']=mysql_escape_string(canonicalizeHostUrl($_REQUEST['url']));

				$sql1="select id from channels where id=$id and publisher_id=$_SESSION[PUB_ID]";
				$rs=$conn->execute($sql1);
				if($rs==false || $rs->recordcount()<=0) {
					throw new Exception("Invalid id");
				}

				$sql1 = "update channels set name='$site[name]' where channels.id=$id";
				$rs = $conn->execute($sql1);
				$nameupdated=$conn->Affected_Rows();
				/*TODO: Exception handling*/

				$site['brandedcpm']=$_REQUEST['brandedcpm']/1000.0;
				$site['videocpm']  =$_REQUEST['videocpm']/1000.0;
				$site['overlaycpm']=$_REQUEST['overlaycpm']/1000.0;

				$sql = "select * from publishersheet where  channel_id ='$id' and isnull(validto)";
				$rs=$conn->execute($sql);
				if(!$rs || $rs->recordcount()<=0) {
					throw new Exception("Invalid campaign");
				}elseif($rs && $rs->recordcount()>0){
					$margin=$rs->fields['margin'];
					$sales_percent=$rs->fields['sales_percent'];
					
				}
				if($site['brandedcpm']!=$rs->fields['branded_unit_price'] ||
				$site['overlaycpm']!=$rs->fields['overlay_unit_price'] ||
				$site['videocpm']!=$rs->fields['video_unit_price']) {
					$sql = "update publishersheet set validto=now() where channel_id='$id' and isnull(validto)";
					$rs = $conn->execute($sql);

					$sql = "insert into publishersheet set video_unit_price = '$site[videocpm]', overlay_unit_price = '$site[overlaycpm]', branded_unit_price='$site[brandedcpm]', margin = '$margin', sales_percent = '$sales_percent', channel_id=$id, validfrom=now()";
					$rs = $conn->execute($sql);
					$msg="Changes successfully saved for $site[name]";

				} else {
					if($nameupdated==0)
						$msg="No changes made";
					else {
						$msg="Changes successfully saved for $site[name]";
					}
				}
				doForward("$config[baseurl]/pub.php?page=managesite&msg=$msg");
			}
			
			
			if ($_REQUEST['action_sitemargin']!=null) {
				/* TODO: add rules */
				$id=mysql_escape_string($_REQUEST['id']);
				$pubmargin = mysql_escape_string($_REQUEST['pubmargin']);
				$salesmargin = mysql_escape_string($_REQUEST['salesmargin']);				
				$sql1="select id from channels where id=$id and publisher_id=$_SESSION[PUB_ID]";
				$rs=$conn->execute($sql1);
				if($rs==false || $rs->recordcount()<=0) {
					throw new Exception("Invalid id");
				}

				
				$sql = "select * from publishersheet where  channel_id ='$id' and isnull(validto)";
				$rs=$conn->execute($sql);
				$site = array();
				if(!$rs || $rs->recordcount()<=0) {
					throw new Exception("Invalid campaign");
				}elseif($rs && $rs->recordcount()>0){
					$site['videocpm']=$rs->fields['video_unit_price'];
					$site['overlaycpm']=$rs->fields['overlay_unit_price'];
					$site['brandedcpm']=$rs->fields['branded_unit_price'];
				}
				if($pubmargin!=$rs->fields['margin'] ||
				$salesmargin!=$rs->fields['sales_percent']) {
					
					$sql = "update publishersheet set validto=now() where channel_id='$id' and isnull(validto)";
					$rs = $conn->execute($sql);

					$sql = "insert into publishersheet set video_unit_price = '$site[videocpm]', overlay_unit_price = '$site[overlaycpm]', branded_unit_price='$site[brandedcpm]', margin = '$pubmargin', sales_percent = '$salesmargin',channel_id=$id, validfrom=now()";
					$rs = $conn->execute($sql);
					$msg="Changes successfully saved for $site[name]";

				} else {
					if($nameupdated==0)
						$msg="No changes made";
					else {
						$msg="Changes successfully saved";
					}
				}
				doForward("$config[baseurl]/pub.php?page=managesite&msg=$msg");
			}
			$sql="select name, margin as pubmargin, sales_percent as salesmargin, url, apikey, cast(video_unit_price*1000 as decimal) as videocpm, cast(branded_unit_price * 1000 as decimal)  as brandedcpm, cast(overlay_unit_price * 1000 as decimal) as overlaycpm from publishersheet, channels where channels.id=$_REQUEST[id] and publishersheet.channel_id=channels.id and isnull(publishersheet.validto) limit 1";
			$rs = $conn->execute($sql);
			$rows=$rs->getRows();
			$_REQUEST=array_merge($_REQUEST, $rows[0]);
		} catch (Exception $e) {
			$error['globalerror']=$e->getMessage();
		}
		$smarty->assign('error', $error);
		break;
}
?>
