<?php
include_once('include/datefuncs.php');

switch ($_REQUEST['sub']) {
	case 'viewclient':
		$adv_id = $_SESSION['ADV_ID'];
		//Deletion of client by advertiser(logged advertiser)
		if(isset($_REQUEST['action_deleteclient'])){
			$adv_user_id = $_REQUEST['client_id'];
			$sql = "DELETE au,c FROM adv_user au LEFT JOIN adv_user_campaign c ON au.id = c.adv_user_id where au.advertiser_id='$adv_id' and au.id='$adv_user_id';";
			$rs = $conn->execute($sql);
			$rows =  $conn->Affected_Rows();
			if($rows>0){
				doForward("$config[baseurl]/adv.php?page=clients&msg=Client+has+been+removed+successfully+from+your+account."); 
			}
			else 
				doForward("$config[baseurl]/adv.php?page=clients"); 
		}

    $campaignurl="$config[baseurl]/adv.php?page=campaign&sub=viewads&campaign_id=";  
		
		//List of all client list for particular advvertiser(logged advertiser)
		$sql = "select 
						u.id,
						u.fname, 
						u.lname,
						u.username,  
						u.email, 
						( 
							select 
								group_concat(' <a href=\"$campaignurl',campaign.id, '\">', name ,'<\\a>') 
							from 
								campaign 
							where 
								id in 
								( 
									select 
										campaign_id 
									from 
										adv_user_campaign c 
									where 
										c.adv_user_id = u.id and 
										c.advertiser_id='$adv_id' 
								) and device!='iphone' and device!='mobile'
						) as campaigns 
					from 
						adv_user u 
					where 
						u.advertiser_id = '$_SESSION[ADV_ID]'";
		$rs=$conn->execute($sql);
		if($rs && $rs->recordcount()>0){    
			$clientdetails = $rs->getrows();
		}
		$smarty->assign('clientdetails',$clientdetails);
		break;
	case 'createclient':
			$adv_id = $_SESSION['ADV_ID'];
			$advertiser_name = $_SESSION['FNAME'];
			$smarty->assign('advertiser_name',$advertiser_name);
			//For edit adv_user case fetching the details about abu_user(Advertiser Client)
			if(isset($_REQUEST['edit_id'])){
				$adv_user_id = $_REQUEST['edit_id'];
				$sql = "select id,username,fname,lname,email from adv_user  where advertiser_id='$adv_id' and id='$adv_user_id'";
				$rs=$conn->execute($sql);
				while(!$rs->EOF) {
					$clientInfo['id']=$rs->fields['id'];
					$clientInfo['fname']=$rs->fields['fname'];
					$clientInfo['lname']=$rs->fields['lname'];
					$clientInfo['email']=$rs->fields['email'];	
					$clientInfo['username']=$rs->fields['username'];
					$rs->movenext();
				}
				$smarty->assign('clientInfo',$clientInfo);
				
				#list of selected campaign by advertiser user
				$sql = "select campaign_id from adv_user_campaign where adv_user_id='$adv_user_id' and advertiser_id='$adv_id'";
				$rs=$conn->execute($sql);
				while(!$rs->EOF) {
					$selected_campaign[]=$rs->fields['campaign_id'];
					$rs->movenext();
				}
				$selected_campaign=implode(',',$selected_campaign);	
				$smarty->assign('campaignTarget',$selected_campaign);				
				$smarty->assign('BtnValue',"Update Client");
				$smarty->assign('isedit',"1");
				$smarty->assign('adv_user_id',$adv_user_id);
			}
			else
			{							
				$temppass='vdopia123';
				$smarty->assign('temppass',$temppass);
				$smarty->assign('BtnValue',"Create Client");				 
				$smarty->assign('isedit',"0");	
			}
			$sql = "SELECT id, name,status FROM campaign where advertiser_id = '$adv_id' and device='pc' order by name asc";
			$rs = $conn->execute($sql);
			$rs->recordcount(); $i=0;
			while(!$rs->EOF) {
				$campaign[$i]['id']=$rs->fields['id'];
				$campaign[$i]['name']=$rs->fields['name'];	
				$campaign[$i++]['status']=$rs->fields['status'];
				$rs->movenext();
			}
			$smarty->assign('campaignList',$campaign);			
			if(isset($_REQUEST['action_createclient']))
			{
				$adv_user_id = $_REQUEST['update_id'];
				if($_REQUEST['action_createclient']=="Update Client")
				{
					$firstname = trim($_REQUEST['firstname']);
					$lastname = trim($_REQUEST['lastname']);
					$email = trim($_REQUEST['email']);
					$username = trim($_REQUEST['username']);
					$query="select * from adv_user where id!='$adv_user_id' and username = '$username' and portal_type = 'online'";
					$res=$conn->execute($query);
					if($res && $res->recordcount()>0){
						$msg = "User with username $username already exists.";
					}
					else{
						$sql="update adv_user set fname	= '$firstname', lname = '$lastname',email = '$email',username = '$username' where id ='$adv_user_id'";
						$rs = $conn->execute($sql);
						$sql = "delete from adv_user_campaign where advertiser_id='$adv_id' and adv_user_id='$adv_user_id'";
						$rs = $conn->execute($sql);					
						$campaign_arr = explode(',', $_REQUEST['campaign']);
						$i=0;
						if(sizeof($_REQUEST['campaign'])>0)
						{
							while($campaign_arr[$i]!=""){
								$sql="insert into adv_user_campaign set
										adv_user_id 	= '$adv_user_id',
										advertiser_id 	= '$adv_id',
										campaign_id 	= '$campaign_arr[$i]'";
								$rs=$conn->execute($sql);	
								$i++;
							}
						}
						doForward("$config[baseurl]/adv.php?page=clients&sub=viewclient&msg=Client+has+been+updated+successfully."); 
					}	
				}
				else{
					$email = $_REQUEST['email'];
					$email = mysql_escape_string($email);
					$username = $_REQUEST['username'];
					$username = mysql_escape_string($username);
					$sql = "select * from  adv_user  where username='$username' and portal_type='online'";
					$rs = $conn->execute($sql);
					if($rs && $rs->recordcount()>0){
						$msg = "User with username $username already exists.";
					}
					else {
						//If user with this email does not exist.
						if($_REQUEST['password']!=""){
							$pass = md5($_REQUEST['password']);
							$emailPass = $_REQUEST['password'];
						
						}else{
							$pass = md5($_REQUEST['tempPass']);
							$emailPass = $_REQUEST['tempPass'];
						}	
						$firstname = trim($_REQUEST['firstname']);
						$lastname = trim($_REQUEST['lastname']);
						$email = trim($_REQUEST['email']);
						$username = trim($_REQUEST['username']);
						$secretKey=md5($_REQUEST['email'].$adv_id.'online');
						$sql="insert into adv_user set
								username = '$username',
								email	=	'$email',
								advertiser_id = '$adv_id',
								portal_type = 'online',
								fname	=	'$firstname',
								lname	=	'$lastname',
								passwd	=	'$pass',
								secretKey =  '$secretKey'";
		
						$rs=$conn->execute($sql);
						$adv_user_id=$conn->Insert_ID();
						$smarty->assign('emailtemplate',"emailtemplate1");
						$msg = "User with username $username added in your account and account detail has been sent to $email.";				
													
						if($adv_user_id > 0) {
							
							//add adv_user campaign into adv_user_campaign table
							$campaign_arr = explode(',',$_REQUEST['campaign']);
							$i=0;
							if(sizeof($_REQUEST['campaign'])>0)
							{
								while($campaign_arr[$i]!=""){
									$sql="insert into adv_user_campaign set
											adv_user_id 	= '$adv_user_id',
											advertiser_id 	= '$adv_id',
											campaign_id 	= '$campaign_arr[$i]'";
									$rs=$conn->execute($sql);	
									$i++;
								}
							}
							
							$smarty->assign('receiver_fname',$firstname);				
							$smarty->assign('username',$username);
							$smarty->assign('password',$emailPass);	
							$subj = "Vdopia Client Account";
							$email_path = $config['baseurl'];
							$body = $smarty->fetch("emails/clientemail.tpl");
							mail($email,$subj,$body,$config['support_headers']);
						
						}
						doForward("$config[baseurl]/adv.php?page=clients&sub=viewclient&msg=$msg"); 	
					}
				}
				$smarty->assign('msg',$msg);
			}			
			break;
	}
?>
