<?php

include_once('include/datefuncs.php');
include_once('include/reportLib1.php');

$reporttypeoptions=array(
'category' => 'Category Performance',
'geography' => 'Geographical Targeting Performance',
'url' => 'Channel Performance'
);

function insert_reporttype_options($a) {
	global $reporttypeoptions;
	return $reporttypeoptions[$a['reporttype']];
}


switch ($_REQUEST['sub']) {
	case 'advusercreatereport':
		$sql = "select campaign_id, campaign.name as name from adv_user_campaign left join campaign on adv_user_campaign.campaign_id = campaign.id where adv_user_id = '$_SESSION[ADV_USER_ID]'";
		error_log($sql);
		$rs = $conn->execute($sql);
		if($rs && $rs->recordCount()>0){
				$campaigns = $rs->getrows();
		}
		
		$smarty->assign('campaigns',$campaigns);
		$smarty->assign('scheduleoptions',$scheduleoptions);
		$smarty->assign('timeperiodoptions',$timeperiodoptions);
		$smarty->assign('options',$options);
		$smarty->assign('yearoptions',$yearoptions);
		$smarty->assign('monthoptions',$monthoptions);
		$smarty->assign('quarteroptions',$quarteroptions);
		$smarty->assign('reporttypeoptions',$reporttypeoptions);

		if($_REQUEST['action_createreport']!=null){
			$campaign_id = mysql_escape_string($_REQUEST['campaigns']);
			$reporttype = mysql_escape_string($_REQUEST['reporttype']);
			$timeperiod =  mysql_escape_string($_REQUEST['timeperiod']);
			$scheduleopt =  mysql_escape_string($_REQUEST['scheduleopt']);

			$dateStr = array();
            $adv_id = getuseradvid($campaign_id);
			$sql = createreportsql1($_REQUEST,$campaign_id,$_SESSION['CURRENCYSYM'],$adv_id,$colnames,$dateStr);
			$rs=$conn->execute($sql);

			$smarty->assign("startperiod", $dateStr[0]);
			$smarty->assign("endperiod", $dateStr[1]);

			if($rs!=false) {
				if($_REQUEST['schedule']=="true") {
					$dropdown=mysql_escape_string($_REQUEST[$timeperiod]);
					$reportname=mysql_escape_string($_REQUEST['reportname']);
					$scheduleopt=mysql_escape_string($_REQUEST['scheduleopt']);
					$sql="insert into saved_report set
						advertiser_id='$_SESSION[ADV_ID]',
						reporttype='$reporttype',
						timeperiodoptions='$timeperiod',
						schedule = '$scheduleopt',
						reportname='$reportname',
						startperiod='$values[0]',
						endperiod='$values[1]',
						dropdown='$dropdown'";

					$rs1=$conn->execute($sql);
					if(!$rs1)
						$smarty->assign('error', "Error Saving Scheduled Report");
					else
						$_REQUEST['msg']="Saved Scheduled Report";
				}

				$smarty->assign('reportname', $reporttypeoptions[$_REQUEST['reporttype']]);
				$coldropdown = array_slice($colnames, count($colnames)-3, count($colnames));
				$smarty->assign('columnname', $colnames);
				$smarty->assign('coldropdown', $coldropdown);
				$_REQUEST['sub']="showreport";
				/*Massage geotargeting data*/
				$data=$rs->getRows();
				fixData($_REQUEST['reporttype'], $data, $colnames);
				$smarty->assign('data', $data);

			} else {
				/*TODO: improve error reporting*/
				$smarty->assign('error', "Invalid request");
			}
		}else {
			$_REQUEST['time']='dropdown';
			$_REQUEST['reporttype']='category';
		}
		break;
	case "delete":
		if($_REQUEST['id']!="")
		{
			$sql="delete from saved_report where id=$_REQUEST[id] and advertiser_id=$_SESSION[ADV_ID]";
			$rs=$conn->execute($sql);
			if(mysql_affected_rows()<=0)
			{$msg="Error changing schedule";}
			else
			{$msg="";}
			doForward("$config[baseurl]/adv.php?page=report&sub=savedreports&msg=$msg");
		}
		break;
	case "savedreports":
		$smarty->assign('scheduleoptions',$scheduleoptions);
		if($_REQUEST['updated']!="" && $_REQUEST['id']!="")
		{
			$sql="update saved_report set schedule='$_REQUEST[updated]' where id=$_REQUEST[id] and advertiser_id=$_SESSION[ADV_ID]";
			$rs=$conn->execute($sql);
			if(mysql_affected_rows()<=0)
			{echo "Error changing Schedule";exit();}
			else
			{echo "success";exit();}
		}
		if($_REQUEST['sched_id']=="" && $_REQUEST['id']==""){
			$sql="SELECT id,advertiser_id,reportname,reporttype,timeperiodoptions,startperiod,endperiod,dropdown,schedule,(select max(runtime) from reports where schedule_id=s.id) as lastrun FROM `saved_report` as s  where s.advertiser_id='$_SESSION[ADV_ID]'";
			$rs=$conn->execute($sql);
			if($rs && $rs->recordcount()>0) {
				$smarty->assign('data',$rs->getrows());
			}
		} else if($_REQUEST['sched_id']!="") {
			$sched_id=mysql_escape_string($_REQUEST['sched_id']);
			$sql="select reportname from saved_report where id='$sched_id' and advertiser_id='$_SESSION[ADV_ID]'";
			$rs=$conn->execute($sql);
			if($rs && $rs->recordcount()) {
				$smarty->assign('reportname',$rs->fields['reportname']);
				$sql="Select * from reports where schedule_id='$sched_id' and advertiser_id='$_SESSION[ADV_ID]'";
				$rs=$conn->execute($sql);
				if($rs && $rs->recordcount()) {
					$smarty->assign('data',$rs->getRows());
				}
			}
		} else {
			$id=mysql_escape_string($_REQUEST['id']);
			$sql="Select `sql`, `colnames`, `reporttype` from reports left join saved_report as sr on sr.id=schedule_id where reports.id='$id' and reports.advertiser_id='$_SESSION[ADV_ID]'";
			$rs=$conn->execute($sql);
			if($rs && $rs->recordcount()>0) {
				$colnames=explode(',',$rs->fields['colnames']);
				$smarty->assign('columnname', $colnames );
				$coldropdown = array_slice($colnames, count($colnames)-5, count($colnames));
				$smarty->assign('coldropdown', $coldropdown);

				$_REQUEST['sub']="showreport";
				$rs1=$conn->execute($rs->fields['sql']);
				if($rs1 && $rs1->recordcount()>0) {
					$data=$rs1->getrows();

					fixData($rs->fields['reporttype'], $data, $colnames);

					if($_REQUEST['download']=='true') {
						header("Content-type: text/csv");
						header("Content-Disposition: attachment; filename=report.csv");

						echo $rs->fields['colnames'];
						echo "\n";
						for($i=0;$i<count($data);$i++) {
							for ($j=0;$j<count($colnames);$j++) {
								if($j>0){
									echo ",";
								}
								echo "\"".$data[$i][$j]."\"";
							}
							echo "\n";
						}
						exit();
					} else {
						$smarty->assign('data', $data);
					}
				}
			} else {
				/*TODO: improve error reporting*/
				$smarty->assign('error', "Invalid request");
			}
		}
		break;
}


?>
