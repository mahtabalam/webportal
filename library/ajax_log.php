<?php
//ini_set('display_errors',on);
//error_reporting(E_ALL);
include_once(dirname(__FILE__)."/../include/config.php");
include_once(dirname(__FILE__)."/../include/function.php");
include_once(dirname(__FILE__)."/../common/portal/tools/misc/logReader.php");


include_once(dirname(__FILE__)."/../common/portal/report/reportApp/lib/ReportConfigs.class.php");

$reportConfig = ReportConfigs::getInstance()->getMainConfig();

if (!is_admin_loggedin())
	die('You are not a valid user for this action,please contact admin');
$data=$_REQUEST;
$file= $reportConfig['path']['log_file'];


$obj=new logReader($data,$file);

if($data['action']=='start')
	$obj->readLogs();
else {
	$obj->clearLogs();

}
