<?php
// DO NOT USE THIS FILE - it will be removed soon. Use include/sri.php instead.

//{$baseurl}/adserver/trk/<method>/<cid>/<adid>/<chid>
function createTracker($baseURL, $method, $cid, $adid, $chid){
	return "$baseURL/adserver/trk/$method/$cid/$adid/$chid";
}

class SriElement {
	/* Columns for the element in the ads table ( cols with x's are mapped unchanged, y's are mapped to well-defined keys)
	 x id
		x parent_ad_id
		x element_id
		x action
		x device_set
		x ad_format
		x advertiser_id
		y playlist_ad_ref
		y branded_img_left_ref
		branded_img_right_ref_imgname
		branded_img_top_ref_txtitle
		branded_img_bot_ref_txbody
		y branded_logo_ref
		branded_color
		x destination_url
		x duration
		x dimension
		x tracker_url
		x action_type
	 */

	const DICT_KEY_EVENT_LIST 		= 'event_list';
	const DICT_KEY_ELEMENT_ID 		= 'element_id';
	const DICT_KEY_CAMPAIGN_ID 		= 'campaign_id';
	const DICT_KEY_CHANNEL_ID 		= 'channel_id';
	const DICT_KEY_AD_ID			= 'id';
	const DICT_KEY_ACTION_TYPE		= 'action_type';
	const DICT_KEY_EVENT			= 'event';
	const DICT_KEY_LEFT_IMG			= 'branded_img_left_ref';
	const DICT_KEY_TXTITLE			= 'branded_img_top_ref_txtitle';
	const DICT_KEY_TRK_URL			= 'tracker_url';
	const DICT_KEY_AD_FORMAT		= 'ad_format';

	const OUT_KEY_TITLE				= 'title';
	const OUT_KEY_LABEL				= 'label';
	const OUT_KEY_ICON_REF			= 'iconURL';
	const OUT_KEY_MSG				= 'msg';
	const OUT_KEY_URL				= 'url';
	const OUT_KEY_INIT_TRK			= 'inittrk';
	const OUT_KEY_COMPL_TRK			= 'comptrk';
	const OUT_KEY_ACTION_TYPE		= 'actionType';
	const OUT_KEY_EM_BODY        	= 'emailBody';
	const OUT_KEY_EM_TO        		= 'emailTo';
	const OUT_KEY_PHONE_NUM        	= 'phoneNo';
	const OUT_KEY_SUBJECT        	= 'subject';

	const ACTION_FOLLOW_URL			= 'followURL';
	const ACTION_SEND_MAIL			= 'sendMail';
	const ACTION_MAKE_CALL			= 'makePhoneCall';
	const ACTION_SEND_SMS			= 'sendSMS';
	const ACTION_REPLAY				= 'replay';


	/* Columns -> object dictionary key mappings that are valid for all element types */
	protected static $UNIVERSAL_KEY_MAPPINGS = array(
		'branded_img_right_ref_imgname' => SriElement::OUT_KEY_LABEL,
		'branded_logo_ref' 				=> SriElement::OUT_KEY_TITLE,
		'playlist_ad_ref' 				=> SriElement::OUT_KEY_ICON_REF,
		'action_type'					=> SriElement::OUT_KEY_ACTION_TYPE,
		'branded_img_bot_ref_txbody'	=> SriElement::OUT_KEY_URL);

	protected static $followURL_KEY_MAP = array(
	SriElement::DICT_KEY_LEFT_IMG 		=> SriElement::OUT_KEY_MSG);

	protected static $sendMail_KEY_MAP = array(
	SriElement::DICT_KEY_LEFT_IMG 		=> SriElement::OUT_KEY_MSG,
	SriElement::DICT_KEY_TRK_URL 		=> SriElement::OUT_KEY_EM_BODY,
	SriElement::DICT_KEY_TXTITLE 		=> SriElement::OUT_KEY_EM_TO);

	protected static $makePhoneCall_KEY_MAP = array(
	SriElement::DICT_KEY_TXTITLE 		=> SriElement::OUT_KEY_PHONE_NUM);

	protected static $sendSMS_KEY_MAP = array(
	SriElement::DICT_KEY_TXTITLE 		=> SriElement::OUT_KEY_PHONE_NUM,
	SriElement::DICT_KEY_LEFT_IMG 		=> SriElement::OUT_KEY_MSG);

	protected static $replay_KEY_MAP = array(
	);

	protected static $INTERNAL_KEYS = array();

	/* Object dictionary containing key-value pairs */
	protected $elementDict;

	/* Construct an element given the corresponding database row content */
	public function __construct(&$dbArr, $campaignId, $channelId){
		/* Basic validation */
		if(
		!isset($dbArr[SriElement::DICT_KEY_AD_ID]) ||
		!isset($dbArr[SriElement::DICT_KEY_AD_ID]) ||
		!isset($dbArr[SriElement::DICT_KEY_EVENT]) ||
		!isset($dbArr[SriElement::DICT_KEY_ACTION_TYPE])){
			throw new Exception("The ads db record must have values for the columns 'element_id', 'id' and 'action_type' - $colName.");
		}
		/* Copy the element dictionary */
		$this->elementDict = $dbArr;

		/* Put in the associated action => id mapping */
		$newArr = &$this->elementDict;
		$newArr[SriElement::DICT_KEY_EVENT_LIST] =
		array($dbArr[SriElement::DICT_KEY_EVENT] => $dbArr[SriElement::DICT_KEY_AD_ID]);
		/* Put in the campaignId and channelId */
		$newArr[SriElement::DICT_KEY_CAMPAIGN_ID] = $campaignId;
		$newArr[SriElement::DICT_KEY_CHANNEL_ID] = $channelId;
	}

	/* Add an element to an element array -
	 *  1. If an element with matching element_id exists, then add the action for this element to the existing element
	 *  2. Otherwise, add the element to the element array
	 */
	public static function updateElementArray(&$elementArr, $element){
		/* Validate $element */
		if(count(array_keys($element->elementDict[SriElement::DICT_KEY_EVENT_LIST])) != 1){
			throw new Exception("Invalid input to updateElementArray - $element must have 1 id.");
		}
		foreach($elementArr as &$arrElement){
			/* check if elements match */
			if($element->elementDict[SriElement::DICT_KEY_ELEMENT_ID] === $arrElement->elementDict[SriElement::DICT_KEY_ELEMENT_ID]){
				/* Add the action from the element to the element already in array */
				$actions = &$arrElement->elementDict[SriElement::DICT_KEY_EVENT_LIST];
				$elementActionDict = $element->elementDict[SriElement::DICT_KEY_EVENT_LIST];
				$elementActions = array_keys($elementActionDict);
				$elementIds = array_values($elementActionDict);
				/* We checked above that there's only one key-value pair in the dictionary */
				$actions[$elementActions[0]] = $elementIds[0];
				return;
			}
		}
		/* We didn't find a match, add the element to the array */
		array_push($elementArr, $element);
	}

	public function getOutputDict($baseUrl, $adUrl, $adTitle){
		if(empty($baseUrl) || empty($adUrl) || empty($adTitle)){
			throw new Exception('Valid inputs needs to be provided');
		}

		$outputDict = array();
		/* Create the common keys */
		foreach(self::$UNIVERSAL_KEY_MAPPINGS as $elementDictKey => $outputDictKey){
			if(!array_key_exists($elementDictKey, $this->elementDict)){
				throw new Exception("The element's dictionary must have a value for the key - $colName.");
			}
			$outputDict[$outputDictKey] = $this->elementDict[$elementDictKey];
		}
			
		/* Create the trackers */
		foreach($this->elementDict[SriElement::DICT_KEY_EVENT_LIST] as $event => $adId){
			switch($event){
				case 'sri_action_init':
					$outputDict[SriElement::OUT_KEY_INIT_TRK] = createTracker($baseUrl, 'cl', $this->elementDict[SriElement::DICT_KEY_CAMPAIGN_ID], 
						$adId, $this->elementDict[SriElement::DICT_KEY_CHANNEL_ID]);
					break;
				case 'sri_action_done':
					$outputDict[SriElement::OUT_KEY_COMPL_TRK] = createTracker($baseUrl, 'cl', $this->elementDict[SriElement::DICT_KEY_CAMPAIGN_ID], 
						$adId, $this->elementDict[SriElement::DICT_KEY_CHANNEL_ID]);
					break;
				default:
					// TODO: Log an error
					break;
			}
		}
			
		/* Do the action-specific processing */
		$mapName = $this->elementDict[SriElement::DICT_KEY_ACTION_TYPE]."_KEY_MAP";
		if(!isset(SriElement::$$mapName) || !is_array(SriElement::$$mapName)){
			trigger_error("Could not process this action type :".
			$this->elementDict[SriElement::DICT_KEY_ACTION_TYPE], E_USER_WARNING);
		} else {
			foreach(SriElement::$$mapName as $dictCol => $outCol){
				$outputDict[$outCol] = $this->elementDict[$dictCol];
			}
		}
		/* perform string substitutions */
		$this->performLevel1Substitutions($outputDict, SriElement::OUT_KEY_MSG, $adUrl, $adTitle);
		$this->performLevel1Substitutions($outputDict, SriElement::OUT_KEY_EM_BODY, $adUrl, $adTitle);
		$this->performLevel1Substitutions($outputDict, SriElement::OUT_KEY_URL, $adUrl, $adTitle);
			
		$this->performLevel2Substitutions($outputDict, SriElement::OUT_KEY_URL);

		/* Remove extraneous keys */
		foreach(SriElement::$INTERNAL_KEYS as $key){
			unset($outputDict[$key]);
		}
		return $outputDict;
	}

	protected function performLevel1Substitutions(&$outDict, $key, $adUrl, $adTitle){
		/* If this entry doesn't exist - we don't need to do anything */
		if(!array_key_exists($key, $outDict)){
			return;
		}
			
		$encodedAdUrl = rawurlencode($adUrl);
		$encodedAdTitle = rawurlencode($adTitle);
			
		$str = &$outDict[$key];
		$str = str_replace("*u", $adUrl, $str);
		$str = str_replace("*t", $adTitle, $str);
		$str = str_replace("*U", $encodedAdUrl, $str);
		$str = str_replace("*T", $encodedAdTitle, $str);
		return;
	}

	protected function performLevel2Substitutions(&$outDict, $key){
		/* If this entry doesn't exist - we don't need to do anything */
		if(!array_key_exists(SriElement::OUT_KEY_URL, $outDict)){
			return;
		}
		$keySpecialCharMaps = array(
		SriElement::OUT_KEY_MSG => array("*m","*M"),
		SriElement::OUT_KEY_EM_BODY => array("*b","*B"),
		SriElement::OUT_KEY_EM_TO => array("*r","*R"),
		SriElement::OUT_KEY_PHONE_NUM => array("*p","*P"));
		$str = &$outDict[$key];
			
		foreach($keySpecialCharMaps as $rKey => $rSpChars){
			$rStr = "";
			if(array_key_exists($rKey, $outDict)){
				$rStr = $outDict[$rKey];
			}

			$encodedRStr = rawurlencode($rStr);
			$str = str_replace($rSpChars[0], $rStr, $str);
			$str = str_replace($rSpChars[1], $encodedRStr, $str);
		}
		$str = htmlentities($str);
		return;
	}

	protected function processStrReplacements(&$str, $adUrl, $adTitle){

	}
}

class SriAdElements {

	const SHARE 	= 'share';
	const RESPOND 	= 'respond';
	const INTERACT 	= 'interact';

	protected static $SRI_KEYS_TYPES_MAP = array(
		 'sri_share' => SriAdElements::SHARE, 
		 'sri_respond' => SriAdElements::RESPOND, 
		 'sri_interact' => SriAdElements::INTERACT);

	protected $elementLists = array();
	/* Construct a set of ad elements given a parent ad_id */
	/* The caller already knows the campaign and channel id */
	public function __construct($conn, $parentAdId, $campaignId, $channelId){
		foreach(array_values(SriAdElements::$SRI_KEYS_TYPES_MAP) as $type){
			$this->elementList[$type] = array();
		}

		if(empty($conn) || !is_callable(array($conn,'execute'))){
			throw new Exception('Valid connection needs to be provided');
		}
		if(!isset($parentAdId) || ($parentAdId != (integer)$parentAdId)){
			throw new Exception('Valid parent_ad_id needs to be provided');
		}
		if(!isset($campaignId) || ($campaignId != (integer)$campaignId)){
			throw new Exception('Valid campaignId needs to be provided');
		}
		if(!isset($channelId) || ($channelId != (integer)$channelId)){
			throw new Exception('Valid channelId needs to be provided');
		}

		try{
			$sql = "SELECT * FROM ads,campaign_members WHERE campaign_members.cid=$campaignId AND ads.parent_ad_id=$parentAdId AND ads.id=campaign_members.ad_id AND campaign_members.status='enabled'";
			$rs= $conn->execute($sql);
			$rsElements = $rs->getrows();
		} catch(Exception $e){
			throw new Exception('Problem in getting or processing database results. '. $e->getMessage());
		}
		foreach($rsElements as $element){
			if(array_key_exists(SriElement::DICT_KEY_AD_FORMAT, $element) && !is_null($element['ad_format']) &&
			array_key_exists($element['ad_format'], SriAdElements::$SRI_KEYS_TYPES_MAP)){
				$elementArr = &$this->elementList[SriAdElements::$SRI_KEYS_TYPES_MAP[$element['ad_format']]];
				try{
					SriElement::updateElementArray($elementArr, new SriElement($element, $campaignId, $channelId));
				} catch(Exception $e){
					/* Log and continue */
					trigger_error("Could not add an SRI element".$e->getMessage(), E_USER_WARNING);
				}
			}
		}
	}

	public function createSriDict($baseUrl, $adUrl, $adTitle){
		$outputDict = array('components' => array());
		$componentsDict = &$outputDict['components'];
		foreach(array_values(SriAdElements::$SRI_KEYS_TYPES_MAP) as $type){
			$componentsDict[$type] = array();
			$elementArr = $this->elementList[$type];
			$outputArr = &$componentsDict[$type];
			foreach($elementArr as $element){
				try{
					array_push($outputArr, $element->getOutputDict($baseUrl, $adUrl, $adTitle));
				} catch(Exception $e){
					/* Log and continue */
					trigger_error("Could not add an SRI element".$e->getMessage(), E_USER_WARNING);
				}
			}
		}
		return $outputDict;
	}

	public function isEmpty(){
		foreach(array_values(SriAdElements::$SRI_KEYS_TYPES_MAP) as $type){
			if(count($this->elementList[$type]) > 0){
				return false;
			}
		}
		return true;
	}
}

// Allow testing of this class from the command line.
if (!empty($argc) && strstr($argv[0], basename(__FILE__))) {
	require_once('../include/adodb/adodb.inc.php');
	$conn = &ADONewConnection('mysql');
	$conn->PConnect('localhost', 'prashant', '', 'adserver_prashant');
	$sriAdElements = new SriAdElements($conn, 978, 805, 666);
	if(!$sriAdElements->isEmpty()){
		$pList = $sriAdElements->createSriDict(
			"http://pvb.vdopia.com/dev/adserver_prashant/trk",
			"http://www.vdopia.com",
			"Best ad ever");
		require_once('plist.php');
		$pList['topimg'] = 'http://cdn.vdopia.com/files/icons/browser-icon.png';
		$pList['botimg'] = 'http://cdn.vdopia.com/files/icons/browser-icon.png';
		$pList['backimg'] = 'http://cdn.vdopia.com/files/icons/browser-icon.png';
		print(PList::createPListForObj($pList, false));
 		$reqVer = '2_3_9';
        $baseVer = strstr($reqVer, '3', TRUE);
        print("*************$reqVer - $baseVer\n");
	}
}
?>