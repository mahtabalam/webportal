<?php
include_once('include/datefuncs.php');
include_once('include/reportLib.php');
include_once('include/zip.class.php');
require_once ABSPATH . "/common/portal/report/Advertiser_Report.php";
require_once ABSPATH . "/common/portal/report/ReportingObject.php";
//ini_set('display_errors',1); 
//error_reporting(E_ALL);
require_once ABSPATH . "/common/portal/report/temporaryTables/TemporaryTableFactory.class.php";
require_once dirname(__FILE__) .'/../common/portal/report/ReportHelper.php';

$reporttypeoptions=array(
'category' => 'Category Performance',
'geography' => 'Geographical Targeting Performance',
'url' => 'Domain Performance'
);

if(isset($config['DB_PREFIX']) && $config['DB_PREFIX']!=''){
	$db_prefix =$config['DB_PREFIX'];
}else {
	$db_prefix='';
}

$enableTemporaryTables = false;
if(isset($config['ENABLE_TEMPORARY_TABLES']) && $config['ENABLE_TEMPORARY_TABLES'] == true ){
    $enableTemporaryTables = true;
}


function insert_reporttype_options($a) {
	global $reporttypeoptions;
	return $reporttypeoptions[$a['reporttype']];
}

switch ($_REQUEST['sub']) {
      case "report_new" :
		//===Export Excel Report====
		if ($_REQUEST ['excelreport'] == "yes") {
			$blank [] = array ("" );
			$colHead=array();
			foreach( $_SESSION ['SelectedField'] as $value){
			$colHead[] =$value;
			}
			AdvertiserReporting::adv_exportToExcel_ivdopia( $_SESSION ['header'], $colHead, $_SESSION ['data'], $blank, $_SESSION ['period'], $_SESSION ['SelectedField'], $_SESSION ['totalArr'] );
		} else {
				//=== edit schedule query start=====
				if($_REQUEST['editid']!="" && $_REQUEST ['run'] == "" && $_REQUEST['act']=='edit'){
					 	$sql="select 
										id,
										name,
										queryParam,
										prefix,
										date_format(sdate, '%Y-%m-%d') as sdate,
										date_format(edate, '%Y-%m-%d') as edate,
										frequency,
										format,
										status,
										mailto,
										email,
										subject,
										message,
										description,
										header,
										extraParm
							from saved_query
							where id=$_REQUEST[editid] 
							and advertiser_id=$_SESSION[ADV_ID]";
						$rs=$conn->execute($sql);
						if(!$rs || $rs->recordcount()<=0){
							$_REQUEST['msg']="Unable to find query";
						}
						else{
							//$_REQUEST ['daterange'] = $rs->fields['sdate']." to ".$rs->fields['edate'];
							$_REQUEST['query_name'] = $rs->fields['name'];
							$_REQUEST['description'] = $rs->fields['description'];
							$_REQUEST['fprefix'] = $rs->fields['prefix'];
							$_REQUEST['start_date'] = $rs->fields['sdate'];
							$_REQUEST['end_date'] = $rs->fields['edate'];
							$_REQUEST['status'] = $rs->fields['status'];
							$_REQUEST['mailto'] = explode(",",$rs->fields['mailto']);
							$_REQUEST['subject'] = $rs->fields['subject'];
							$_REQUEST['message'] = $rs->fields['message'];
							$emailarr = explode("|",$rs->fields['email']);
							$_REQUEST['to_email'] = $emailarr[0];
							$_REQUEST['cc_email'] = $emailarr[1];
							$_REQUEST['bcc_email'] = $emailarr[2];
							$frequencyarr = explode(":",$rs->fields['frequency']);
							$_REQUEST['frequency'] = $frequencyarr[0]; 
							if($frequencyarr[0]=="weekly")
								$_REQUEST['weekday'] = $frequencyarr[1];
							else
								$_REQUEST['monthday'] = $frequencyarr[1]; 
							$reportingObject = unserialize($rs->fields['queryParam']) ;
							$_REQUEST ['daterange'] = AdvertiserReporting::getDateMysqlToPortal($reportingObject->start_date)." to ".AdvertiserReporting::getDateMysqlToPortal($reportingObject->end_date);
							$_REQUEST ['channel'] = $reportingObject->channel;
							$_REQUEST ['reporttype'] = $reportingObject->reporttype;
							$_REQUEST ['campaign'] = $reportingObject->campaign;
							$_REQUEST ['country'] = $reportingObject->country;
							$_REQUEST ['state'] = $reportingObject->state;
							$_REQUEST ['campaign_status'] = $reportingObject->campaign_status;
							$_REQUEST ['period'] = $reportingObject->period;
							$_REQUEST ['country_show_in_column'] = $reportingObject->country_show_in_column;
							$_REQUEST ['state_show_in_column'] = $reportingObject->state_show_in_column;
							$_REQUEST ['channel_show_in_column'] = $reportingObject->channel_show_in_column;
							$_REQUEST ['extra_columns'] = $reportingObject->extra_columns;
							$_REQUEST ['metrics'] = $reportingObject->metrics;
							$_REQUEST ['ScheduleDate'] = $reportingObject->ScheduleDate;
						}
					}
					
					
					
					//================Run Query Start to view data on portal ===
					$channellist = array ();
					//have to check if we are setting this value in config

					$portal = "vdopia";
			
					if($_REQUEST['editid']=="" || $_REQUEST ['run']=='Run'){
						$_REQUEST['runSave'] = "viewOnScreen";
					}else{
						$_REQUEST['runSave'] = "schedule";
					}
					//setting report type
					if (! $_REQUEST ['reporttype'])
						$_REQUEST ['reporttype'] = 'campaign';
					
					//by default show only last 7 days data
					if (isset ( $_REQUEST ['daterange'] ) == false) {
						$startdate = AdvertiserReporting::getDateMysqlToPortal ( date ( "Y-m-d", mktime ( 0, 0, 0, date ( "m" ), date ( "d" ) - 7, date ( "Y" ) ) ) );
						$enddate = AdvertiserReporting::getDateMysqlToPortal ( date ( "Y-m-d", mktime ( 0, 0, 0, date ( "m" ), date ( "d" ) - 1, date ( "Y" ) ) ) );
						$_REQUEST ['daterange'] = "$startdate to $enddate";
					} else {
						$date_arr = explode ( "to", $_REQUEST ['daterange'] );
						array_walk ( $date_arr, create_function ( '&$val', '$val = trim($val);' ) );
						$startdate = $enddate = $date_arr [0];
						if (count ( $date_arr ) == 2) {
							$enddate = $date_arr [1];
						}
					}
					$advertiser_report = new AdvertiserReporting ( $_SESSION ['ADV_ID'], $portal, AdvertiserReporting::getDatePortalToMysql ( $startdate ), AdvertiserReporting::getDatePortalToMysql ( $enddate ),$_REQUEST );
					if($_REQUEST['reporttype']=="apps") unset($_REQUEST['channel_show_in_column']);
					
			if (isset ( $_REQUEST ['run'] ) && $_REQUEST ['run'] != "" && $_REQUEST ['run']=='Run') {
						$advertiser_report = new AdvertiserReporting ( $_SESSION ['ADV_ID'], $portal,  AdvertiserReporting::getDatePortalToMysql ( $startdate ),  AdvertiserReporting::getDatePortalToMysql ( $enddate ),$_REQUEST );
						$summary = $advertiser_report->processObject();
						
						if($_REQUEST['reporttype']=="deep"){
							$smarty->assign("deep",true);
						}
						$smarty->assign ( "totalArr", $_SESSION ['totalArr'] );
						$smarty->assign ( "data", $summary );

					
			//////================Save query in db ===============		
			}elseif (isset ( $_REQUEST ['run'] ) && $_REQUEST ['run'] != "" && $_REQUEST ['run']=='Save') {
						
						$ReportingObj = new ReportingObject ( $_SESSION ['ADV_ID'], $portal, AdvertiserReporting::getDatePortalToMysql ( $startdate ), AdvertiserReporting::getDatePortalToMysql ( $enddate ),$_REQUEST );
						$SerializedObj = serialize($ReportingObj);

						$query_name			=	mysql_escape_string(strip_tags($_REQUEST['query_name']));	
						$description		=	mysql_escape_string(strip_tags($_REQUEST['description']));	
						$fprefix				=	mysql_escape_string(strip_tags($_REQUEST['fprefix']));
						$sdate				=	mysql_escape_string(strip_tags($_REQUEST['start_date']));	
						$edate				=	mysql_escape_string(strip_tags($_REQUEST['end_date']));
						
						if ($_REQUEST['frequency']=="weekly")
							$frequency		=	"weekly:$_REQUEST[weekday]";
						elseif ($_REQUEST['frequency']=="monthly")
							$frequency		=	"monthly:$_REQUEST[monthday]";
						else
							$frequency 		= $_REQUEST['frequency'];
							
						$format				=	mysql_escape_string(strip_tags($_REQUEST['format']));		
						$status				=	mysql_escape_string(strip_tags($_REQUEST['status']));
						$mailto	 			= 	implode(",",$_REQUEST['mailto']);
						$emailStr 			=	$_REQUEST['to_email']."|".$_REQUEST['cc_email']."|".$_REQUEST['bcc_email'];
						$emailStr			=	mysql_escape_string(strip_tags($emailStr));
						$subject			=	mysql_escape_string(strip_tags($_REQUEST['subject']));	
						$message			=	mysql_escape_string(strip_tags($_REQUEST['message']));
						
						
						if(isset($_REQUEST['editid']) && $_REQUEST['editid']!=""){
							$prefix="update saved_query";
							$postfix=" where id=$_REQUEST[editid] and advertiser_id=$_SESSION[ADV_ID]";
						}else {
							$prefix="insert into saved_query";
							$postfix=",cdate =	now()";
						}
						$sql	=	"$prefix set
										advertiser_id		=	'$_SESSION[ADV_ID]',
										name				=	'$query_name',
										query				=	'$sqlstr',
										queryParam			=	'$SerializedObj',
										description			=	'$description',
										prefix				=	'$fprefix',
										sdate				=	'$sdate',
										edate				=	'$edate',
										frequency			=	'$frequency',
										format				=	'$format',
										status				=	'$status',
										mailto				=	'$mailto',
										email				=	'$emailStr',
										subject				=	'$subject',
										message				=	'$message'";
						
						
					 	$sql.= " $postfix";
					//exit;
						$rs = $conn->execute($sql);
						header("Location:$config[baseurl]/adv.php?page=report&sub=viewreports&msg=Query+$query_name+successfully+saved");	
									
						
						
						
					}
					$channellist = $advertiser_report->fetchChannelList ();
					$campaignlist = $advertiser_report->fetchCampaignList ();
					$placementlist = $advertiser_report->fetchPlacementList ();
                         if($_REQUEST['country_codes']!="") {
	                         $geoTarget=trim($_REQUEST['country_codes'],",").',';
	                         $_REQUEST['country_codes']=trim($_REQUEST['country_codes'],",");
                         }
                            if($_REQUEST['state_codes']!=""){
	                            $geoTarget.=trim($_REQUEST['state_codes'],",").',';
	                            $_REQUEST['state_codes']=trim($_REQUEST['state_codes'],",");
                            }
                            
                            $geoTarget=trim($geoTarget,',');
                            $smarty->assign('geoTarget', trim($geoTarget,','));
					// $_SESSION['SelectedField'] = $fieldlistoptions;
					$extraColumns=$advertiser_report->getExtraColumns();
					$smarty->assign ( "extra_columns", $extraColumns );
					$portalfieldlistoptions=$advertiser_report->getPortalFieldList();
					$report_types = $advertiser_report->getReportType ();
					//assinging values to smarty
					$fieldlistoptions = $advertiser_report->getFieldList ($_REQUEST ['reporttype']);
					$smarty->assign ( "SelectedField", $fieldlistoptions );
					$smarty->assign ( "portalfieldlistoptions", $portalfieldlistoptions );
					$smarty->assign ( 'timeperiodoptions', $timeperiodoptions );
					$smarty->assign ( "channellist", $channellist );
					$smarty->assign ( 'campaign_status', $_REQUEST ['campaign_status'] );
					$smarty->assign ( "campaignlist", $campaignlist );
					$smarty->assign ( "placementlist", $placementlist );
					$smarty->assign ( "countrycodes", getCountryList () );
					$country = $advertiser_report->country;
					if (count($country)==1 && in_array ( "all", $country ) === false) {
						$smarty->assign ( "statecodes", getStateList ( $country[0] ) );
					}
					
					$smarty->assign ( 'country', $country );
					$smarty->assign ( 'state', $state );
					$smarty->assign ( 'reporttype', $report_types );
					$smarty->assign ( 'report_caption', $report_types [$reporttype] );
					$adtypeoptions = array (array ('id' => 'all', 'name' => "All" ), array ('id' => 'preapp', "name" => "Pre-App Video" ), array ('id' => 'inapp', "name" => "In-App video" ), array ('id' => 'vdobanner', "name" => "Mini Video" ), array ('id' => 'banner', 'name' => "Banner" ), array ('id' => 'preinter', 'name' => "Pre-App Initerstitial" ), array ('id' => 'ininter', 'name' => "In-App Interstitial" ), array ('id' => 'tracker', 'name' => "Tracker" ) ); //array('id'=>'logo' , 'name' => "Logo")
					//$smarty->assign ( "fieldlistoptions", $fieldlistoptions );
					$smarty->assign ( "adtypeoptions", $adtypeoptions );
			}
		break;
	//show all created schedule report
		case "viewreports" :
				$sql = "SELECT
									id,name,
									date_format(sdate, '%Y-%m-%d') as sdate,
									date_format(edate, '%Y-%m-%d') as edate,
									email,status,
									date_format(cdate, '%Y-%m-%d') as cdate
								from 
									saved_query
								where advertiser_id ='$_SESSION[ADV_ID]'
						order by id desc"  ;
						$rs = $conn->execute($sql);
						if($rs && $rs->recordcount()>0){
							$result = $rs->getrows();
							$smarty->assign('data',$result);
						}
		break;
	//delete shedule report
		case "delReport" :
				//=======delete query start =========
				if($_REQUEST['delid']!=""){
						$sql="delete from saved_query where id=$_REQUEST[delid] and advertiser_id=$_SESSION[ADV_ID]";
						$rs=$conn->execute($sql);
						if($conn->Affected_Rows()<=0){
							$msg="Error deleting in query";
						}
						else{
							$msg="Saved query has been deleted successfully";
						}
						header("Location:$config[baseurl]/adv.php?page=report&sub=viewreports&msg=$msg");
						
					}
					
					//================Delete Query end===
		break;
		case "togglestatus" : 
			//=======delete query start =========
				if($_REQUEST['id']!=""){
						if($_REQUEST['status']=='inactive'){
							$st = 'active';
						}else{
							$st = 'inactive';
						}
						$sql="update saved_query set status='$st' where id=$_REQUEST[id] and advertiser_id=$_SESSION[ADV_ID] and status='$_REQUEST[status]'";
						$rs=$conn->execute($sql);
						if($conn->Affected_Rows()<=0){
							$msg="Error change status in query";
						}
						else{
							$msg="Saved query has been updated successfully";
						}
						header("Location:$config[baseurl]/adv.php?page=report&sub=viewreports&msg=$msg");
					}
					//================Delete Query end===
		break;
		case "advreport":
		//=================================================================================
                
                $arrTempTable = getAllTempTables();
                if($enableTemporaryTables)    
                    $metaTablePrefix = 'tmp.';  
                else
                    $metaTablePrefix = "";
		if($_REQUEST['excelreport']=="yes"){
			$blank[]=array("");
			if($_SESSION['period']=="summary"){
				$colHead =array();
				//$colHead[]=array(ucfirst($_SESSION['reporttype']),"Total Impressions","Clicks","CTR");
				if($_SESSION['reporttype']=='ad'){
					$colHead[]='Campaign';
				}else $colHead[]=ucfirst($_SESSION['reporttype']);
				
				foreach ($_SESSION['SelectedField'] as $key => $val){
					$colHead[] = $val;
				}
				//adv_exportToExcel($_SESSION['header'],$colHead,$_SESSION['data'],$blank,$_SESSION['period']);
				adv_exportToExcel($_SESSION['header'],$colHead,$_SESSION['data'],$blank,$_SESSION['period'],$_SESSION['SelectedField'],$_SESSION['totalArr']);
			}
			else{
				//$colHead[]=array("Date","Total Impressions","Clicks","CTR","Total Amounts ".$_SESSION['CURRENCYSYM'],"eCPM ".$_SESSION['CURRENCYSYM']);
				$colHead =array();
				//$colHead[]=array(ucfirst($_SESSION['reporttype']),"Total Impressions","Clicks","CTR");
				$colHead[]="Date";
				//$colHead[]=ucfirst($_SESSION['reporttype']);
				foreach ($_SESSION['SelectedField'] as $key => $val){
					$colHead[] = $val;
				}
				//adv_exportToExcel($_SESSION['header'],$colHead,$_SESSION['data'],$blank,$_SESSION['period']);
				adv_exportToExcel($_SESSION['header'],$colHead,$_SESSION['data'],$blank,$_SESSION['period'], $_SESSION['SelectedField'], $_SESSION['totalArr']);
			}
		}
		elseif ($_REQUEST['pdfreport']=="yes"){
			$blank[]=array("");
			$colWidth=array(55,40,35,35);
			//, $_SESSION['SelectedField'], $_SESSION['totalArr']
			if($_SESSION['period']=="summary"){
				//$colHead[]=array(ucfirst($_SESSION['reporttype']),"Total Impressions","Clicks","CTR");
				if($_SESSION['reporttype']=='ad'){
					$colHead[]='Campaign';
				}else $colHead[]=ucfirst($_SESSION['reporttype']);
				foreach ($_SESSION['SelectedField'] as $key => $val){
					$colHead[] = $val;
				}
				//adv_exportToPdf($_SESSION['header'],$colHead,$_SESSION['data'],$_SESSION['period']);
				adv_exportToPdf($_SESSION['header'],$colHead,$_SESSION['data'],$_SESSION['period'],$_SESSION['SelectedField'], $_SESSION['totalArr']);
			}
			else{
				//$colHead[]=array("Date","Total Impressions","Clicks","CTR");
				$colHead[]="Date";
				//$colHead[]=ucfirst($_SESSION['reporttype']);
				foreach ($_SESSION['SelectedField'] as $key => $val){
					$colHead[] = $val;
				}
				adv_exportToPdf($_SESSION['header'],$colHead,$_SESSION['data'],$_SESSION['period'],$_SESSION['SelectedField'], $_SESSION['totalArr']);
			}
		}
		else{
			$_SESSION['currentReport']=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			$smarty->assign('timeperiodoptions',$timeperiodoptions);
			if(!isset($_REQUEST['dropdown'])){
				$_REQUEST['time']= "dropdown";
			}
			if(!isset($_REQUEST['timeperiod'])){
				$_REQUEST['timeperiod']= "yesterday";
			}
			
			if(!$_REQUEST['start_date']){
				$tm=thismonth(); //default date range is this month
				$fromdate=$tm[0];
				$todate=$tm[1];
				 
				$_REQUEST['start_date']=$fromdate;
				$_REQUEST['end_date']=$todate;
				$startdate = $fromdate;
				$enddate = $todate;
			}else{
				$fromdate=$_REQUEST['start_date'];
				$todate=$_REQUEST['end_date'];
			}
		    $channellist = array();
                    
                    if($enableTemporaryTables){
                        $arrTempTable['channels']->create();
                        $arrTempTable['campaign']->create();
                    }
		    $sql = "select channels.name, channels.id from {$metaTablePrefix}channels left join {$db_prefix}daily_stats on channels.id=channel_id left join {$metaTablePrefix}campaign on campaign.id=campaign_id where {$db_prefix}daily_stats.date between '$fromdate' and '$todate'  and campaign.advertiser_id='$_SESSION[ADV_ID]' and channels.channel_type!='iphone' group by {$db_prefix}daily_stats.channel_id order by channels.name";
		    $rs = $reportConn->execute($sql);
                    if($enableTemporaryTables){
                        $arrTempTable['channels']->drop();
                        $arrTempTable['campaign']->drop();
                    }
		    if($rs && $rs->recordcount()>0){
		    	$channellist = $rs->getrows();			    	
		    }
		    $smarty->assign("channellist",$channellist);
		    //$smarty->assign("reporttype", array("category" => "Category Performance Report","campaign" => "Campaign Performance Report", "channel" => "Channel Performance Report", "country" => "Country Performance Report", "state" => "State Performance Report"));
			
		    //,"crossChannel" => "Cross Channel Report"adUniques
		    //,"campaignUniques" => "Campaign Uniques Performance Report","adUniques" => "Ad Uniques Performence Report", "channelUniques" => "Channel Uniques Performance Report",
		    if((is_admin_loggedin() || is_sales_admin_loggedin()))
		    	$smarty->assign("reporttype", array("campaign" => "Campaign Performance Report", "channel" => "Channel Performance Report", "country" => "Country Performance Report", "state" => "State Performance Report", "ad" => "Ad Performance Report"));
		    else
		    	$smarty->assign("reporttype", array("campaign" => "Campaign Performance Report", "channel" => "Channel Performance Report", "country" => "Country Performance Report", "state" => "State Performance Report", "ad" => "Ad Performance Report"));
		    $country=$_GET['country'];
			$state=$_GET['state'];
			$smarty->assign("countrycodes", getCountryList());
		    $smarty->assign("statecodes", getStateList($country));
		    $smarty->assign('country',$country);
		    $smarty->assign('state',$state);
		    
		    $campaignlist = array();
		   
		   $sql = "select name, id from campaign where advertiser_id='$_SESSION[ADV_ID]' and device='pc' order by name";
		   $rs = $reportConn->execute($sql);
		   if($rs && $rs->recordcount()>0){
		     $campaignlist = $rs->getrows();
		   }
		    
		    //$_REQUEST['campaign']=$campaignlist;
		    //echo "<pre>";
		    //print_r($campaignlist);
		    
		    $smarty->assign("campaignlist",$campaignlist);
		    if(!isset($_REQUEST['adtype']))$_REQUEST['adtype']='all';
		    if(!isset($_REQUEST['reporttype']))$_REQUEST['reporttype']='campaign';
		    if(!isset($_REQUEST['channel']))$_REQUEST['channel']='all';
		    if(!isset($_REQUEST['campaign']))$_REQUEST['campaign']=array('all');
		    if(!isset($_REQUEST['country']))$_REQUEST['country']='all';
		    if(!isset($_REQUEST['state']))$_REQUEST['state']='all';
		    if(!isset($_REQUEST['period']))$_REQUEST['period']='summary';
		    
		    
		    //====Cross Channel Report always fetch on weekly basis=======
		    if($_REQUEST['reporttype'] == "crossChannel")
		    	$_REQUEST['period'] = "weekly";
		    
		    
		    //======================================
			$adtypeoptions = array(
			array('id'=>'all', 'name' => "All"),
			array('id'=>'preroll', "name"=>"Preroll"),
			array('id'=>'midroll', "name"=>"Midroll"),
			array('id'=>'postroll', 'name' => "Postroll"),
			array('id'=>'ytpreroll', "name"=>"Youtube Preroll"),
			array('id'=>'branded', 'name' => "Branded Player"),
			array('id'=>'overlay', 'name' => "Overlay"),
			array('id'=>'tracker', 'name' => "Tracker"),
			array('id'=>'vdobanner', 'name' => "Video Banner"),
			array('id'=>'banner', 'name' => "Banner"),
			array('id'=>'copreroll', 'name' => "Companion Preroll"),
			array('id'=>'comidroll', 'name' => "Companion Midroll"),
			array('id'=>'copostroll', 'name' => "Companion Postroll")
			);
			
			$sym=$_SESSION['CURRENCYSYM'];
			# TODO:: form new array for field options
			if($period!="summary" && ($_REQUEST['reporttype']=="channel")){
				$fieldlistoptions = array('totimpressions' => "Impression", 'totclicks'=>"Click", 'ctr'  => "CTR", 'rev' => "Publisher Revenue $sym", 'eCPM' => "Publisher eCPM $sym", "talk2me" => "Talk2Me");
			}else{
				$fieldlistoptions = array('totimpressions' => "Impression", 'totclicks'=>"Click", 'ctr'  => "CTR","talk2me" => "Talk2Me");
			}
			if(is_admin_loggedin() || is_sales_admin_loggedin()  || (in_array('r.cpcvcolumn', $_SESSION['CAPABILITIES']))){
				$fieldlistoptions["vdocompletes"]="Video Completes";
				$fieldlistoptions["cr"]="CR(%)";
			}elseif(in_array('r.cpcvcolumn', $_SESSION['CAPABILITIES'])){
				$fieldlistoptions["vdocompletes"]="Video Completes";
				$fieldlistoptions["cr"]="CR(%)";
			}
			
			//========================================
			/*if((is_admin_loggedin() || is_sales_admin_loggedin() || in_array('r.cpcvcolumn', $_SESSION['CAPABILITIES']))){	
				if($period!="summary" && ($_REQUEST['reporttype']=="campaign" || $_REQUEST['reporttype']=="channel")){
					$fieldlistoptions = array(
					'totimpressions' => "Impression",
					'totclicks'=>"Click",
					'ctr'  => "CTR",
					'rev' => "Revenue",
					'eCPM' => "eCPM",
					'ec'=>"EC",
					'ecr'  => "ECR",
					'vdocompletes'=> "Video Completes",
					'cr'=> "CR(%)",	
					"talk2me" => "Talk2Me"
					);
				}
				else{
					$fieldlistoptions = array('totimpressions' => "Impression",
					'totclicks'=>"Click",
					'ctr'  => "CTR",
					'ec'=>"EC",
					'ecr'  => "ECR", 
					'vdocompletes'=> "Video Completes",
					'cr'=> "CR(%)",
					"talk2me" => "Talk2Me"
					);
				}
			}else if(is_admin_loggedin() || is_sales_admin_loggedin() || in_array('r.cplcolumn', $_SESSION['CAPABILITIES'])){
				if($period!="summary" && ($_REQUEST['reporttype']=="campaign" || $_REQUEST['reporttype']=="channel")){
					$fieldlistoptions = array(
							'totimpressions' => "Impression",
							'totclicks'=>"Click",
							'ctr'  => "CTR",
							'rev' => "Revenue",
							'eCPM' => "eCPM",
							'ec'=>"EC",
							'ecr'  => "ECR",
							'leads' => "Leads",
							"talk2me" => "Talk2Me"
					);
				}
				else{
					$fieldlistoptions = array('totimpressions' => "Impression",
							'totclicks'=>"Click",
							'ctr'  => "CTR",
							'ec'=>"EC",
							'ecr'  => "ECR",
							'leads'=> "Leads",
							"talk2me" => "Talk2Me"
					);
				}
			}
			else{
				$fieldlistoptions = array('totimpressions' => "Impression",
					'totclicks'=>"Click",
					'ctr'  => "CTR", 
						'ec'=>"EC",
						'ecr'  => "ECR",
					"talk2me" => "Talk2Me"
				);
			}*/
				
			$smarty->assign("fieldlistoptions",$fieldlistoptions);
		    //========================================
			$smarty->assign("adtypeoptions",$adtypeoptions);
			$smarty->assign("ddoptions",$ddoptions);
			$smarty->assign("pubreport","true");
			$reporttype=getRequest('reporttype');
			$channel= getRequest('channel');
			$campaign = getRequest('campaign');
			$adtype = getRequest('adtype');
			$country= getRequest('country');
			$state= getRequest('state');
			$period = getRequest('period');
						
			
			if(isset($_REQUEST['submit']) && $_REQUEST['submit']!=""){
				$startdate = getRequest('start_date');
				$enddate = getRequest('end_date');
				
				$campaignArr['campaign'] =  $campaign;
				$channelArr['channel'] =  $channel;
				$countryArr['cat'] =  $country;
				$stateArr['state'] =  "$country:$state";
				//$stateArr['count'] =  $country;
				
				if($country!="all")
					$countryName = insert_country_names($countryArr);
				else
					$countryName = "All";
				
				if($state=="all" || $state == "")
					$stateName = "All";
				else
					$stateName = insert_state_names($stateArr);
				
				$campaignName = insert_campaign_names($campaignArr,'excel');
				$channelName = insert_channel_names($channelArr,'excel');
				
				
				
				$report_header=array();
				$report_header[]=array(ucfirst($reporttype)." Performance Report(" . ucfirst($period) . ")");
				$report_header[]=array('Report Generated on', date('d-m-Y H:i T'));
				$report_header[]=array("Start Date",$startdate);
				$report_header[]=array("End Date",$enddate);
				$report_header[]=array("Channel",ucfirst($channelName));
				$report_header[]=array("Campaign",ucfirst($campaignName));
				$report_header[]=array("Country",ucfirst($countryName));
				$report_header[]=array("State",ucfirst($stateName));
				$report_header[]=array("Ad Format",ucfirst($adtype));
				$_SESSION['header'] = $report_header;
					
				
			    if($reporttype=="category")
					$dateField ="hour";
				else
					$dateField ="date";
				
	
	      $datequery=" and $dateField>='$startdate' and $dateField<='$enddate' ";
						
				if($channel=="all" || $reporttype=="category")
				{
					$sitestring=" ";
				}
				else{
					$sitestring=" and {$db_prefix}daily_stats.channel_id='$channel'";
					$sitestringU=" and {$db_prefix}daily_stats.channel_id='$channel'";
				}
				
				$campaign_ids=implode(',',$campaign);
				if($campaign[0]=="all" || $reporttype=="category"){
					$campaignstring=" ";
				}
				else{
					$campaignstring=" and {$db_prefix}daily_stats.campaign_id in ($campaign_ids)";
					$campaignstringU=" and {$db_prefix}daily_stats.campaign_id in ($campaign_ids)";
				}
				
				if($adtype=="all" || $reporttype=="category")
				{
					$adtypestring=" ";
					
				}else{
					$adtypestring=" and {$db_prefix}daily_stats.adtype='$adtype'";
				}
				
				if($country=="all" || $reporttype=="category"){
					$countrystring=" ";
				}else{
					$countrystring=" and {$db_prefix}daily_stats.ccode='$country'";
				}
				
				if($state=="all" || $reporttype=="category"){
					$provincestring=" ";
				}else{
					$provincestring=" and state_code='$state'";
				}
					switch ($period){
					case "daily":
						$group = " group by ";
						$aggregate=" day($dateField), month($dateField), year($dateField) ";
						$aggregateNames=", $dateField as timeframe ";
						$dateSnip=" $dateField ";
						$agcolNames="Date,";
						$orderby="$dateField";
						$measure = "and measure='duniqvi' ";
						break;
					case "monthly":
						$group = " group by ";
						$aggregate=" month($dateField),year($dateField) ";
						$aggregateNames=", concat_ws(' ', monthname($dateField), year($dateField)) as timeframe ";
						$dateSnip=" concat_ws(' ', monthname($dateField), year($dateField)) ";
						$agcolNames="Timeframe,";
						$orderby=" month($dateField)";
						$measure = "and measure='muniqvi' ";
						break;
					case "quarterly":
						$group = " group by ";
						$aggregate=" quarter($dateField),year($dateField) ";
						$aggregateNames=", concat_ws(' ', concat('Q',quarter($dateField)), year($dateField)) as timeframe ";
						$dateSnip=" concat_ws(' ', concat('Q',quarter($dateField)), year($dateField)) ";
						$agcolNames="Timeframe,";
						$orderby=" quarter($dateField)";
						break;
					case "dayofweek":
						$group = " group by ";
						$aggregate=" dayofweek($dateField), year($dateField) ";
						$aggregateNames=", date_format($dateField, '%a') as timeframe ";
						$dateSnip=" date_format($dateField, '%a') ";
						$agcolNames="Day,";
						$orderby=" dayofweek($dateField)";
						break;
					case "weekly":
						$group = " group by ";
						$aggregate=" week($dateField), year($dateField) ";
						$aggregateNames=", concat('Week','-',week($dateField)+1) as timeframe ";
						$dateSnip=" concat('Week','-',week($dateField)+1) ";
						$agcolNames="Week,";
						$orderby=" week($dateField)";
						$measure = "and measure='wuniqvi' ";
						break;
					case "yearly":
						$group = " group by ";
						$aggregate=" year($dateField) ";
						$aggregateNames=", year($dateField) as timeframe ";
						$dateSnip=" year($dateField) ";
						$agcolNames="Year,";
						$orderby=" year($dateField)";
						break;
					case "summary":
						$group = " group by ";
						$aggregateNames=", 'Summary' as timeframe ";
						$aggregateNames="";
						$orderby="";
						break;
					default:
						$aggregate=" ";
						$aggregateNames="";
						$agcolNames="";
						$orderby="";
						break;
				}
				
				if($adtype=="copreroll" || $adtype=="copostroll"){
					$impr="if({$db_prefix}daily_stats.adtype like 'co%', impressions, 0)";
					$clkr="if({$db_prefix}daily_stats.adtype like 'co%', clicks, 0)";
				}else{
					$impr="if({$db_prefix}daily_stats.adtype not like 'co%', impressions, 0)";
					$clkr="if({$db_prefix}daily_stats.adtype not like 'co%', clicks, 0)";
				}
				//==========================================================================
				
				if($_REQUEST['fieldlist']==""){
					if($period!="summary" && ($_REQUEST['reporttype']=='channel')){
						$_REQUEST['fieldlist'] = array('totimpressions', 'totclicks', 'ctr', 'rev', 'eCPM',"talk2me");
					}else{
						$_REQUEST['fieldlist'] =array('totimpressions', 'totclicks', 'ctr',"talk2me");
					}
					if(is_admin_loggedin() || is_sales_admin_loggedin()  || (in_array('r.cpcvcolumn', $_SESSION['CAPABILITIES']))){
						$_REQUEST['fieldlist']["vdocompletes"]="vdocompletes";
						$_REQUEST['fieldlist']["cr"]="cr";
					}elseif(in_array('r.cpcvcolumn', $_SESSION['CAPABILITIES'])){
						$_REQUEST['fieldlist']["vdocompletes"]="vdocompletes";
						$_REQUEST['fieldlist']["cr"]="cr";
					}
					/*
					if(is_admin_loggedin() || is_sales_admin_loggedin() || (in_array('r.cpcvcolumn', $_SESSION['CAPABILITIES']) && in_array('r.cplcolumn', $_SESSION['CAPABILITIES']))){
						$_REQUEST['fieldlist'] = array('totimpressions','totclicks','ctr','ec','ecr','rev','eCPM','vdocompletes','cr','leads');
					}elseif(in_array('r.cpcvcolumn', $_SESSION['CAPABILITIES'])){
						$_REQUEST['fieldlist'] = array('totimpressions','totclicks','ctr','ec','ecr','rev','eCPM','vdocompletes','cr');
					}
					else if(in_array('r.cplcolumn', $_SESSION['CAPABILITIES'])){
						$_REQUEST['fieldlist'] = array('totimpressions','totclicks','ctr','ec','ecr','rev','eCPM','leads');
					}
					else{
						$_REQUEST['fieldlist'] = array('totimpressions','totclicks','ctr','ec','ecr');
					}*/
				}
				$SelectedField =array(); 
				//$SelectedFieldUniques =array();
				if($reporttype =="ad" ){
					$SelectedField['adname'] = "Ad Name";
				}   
				foreach ($_REQUEST['fieldlist'] as $key => $val){
					if($_REQUEST['reporttype']=='campaign'){
						$SelectedField[$val] = $fieldlistoptions[$val];
					}else{
						if($fieldlistoptions[$val]!='EC' && $fieldlistoptions[$val]!='ECR') {
							$SelectedField[$val] = $fieldlistoptions[$val];
						}
					}
				}
				if(is_admin_loggedin() || is_sales_admin_loggedin() || in_array('r.cpcvcolumn', $_SESSION['CAPABILITIES'])){
					$vdo=",sum(completes) as vdocompletes,round(((sum(completes)/sum($impr))*100),2) as cr,";
				}else{
					$vdo=',';
				}
				/*if(is_admin_loggedin() || is_sales_admin_loggedin() || in_array('r.cplcolumn', $_SESSION['CAPABILITIES'])){
					$leads=",sum(leads) as leads,";
				}else{
					$leads=',';
				}*/
				//echo "<pre>";
				//print_r($SelectedField);
				//print_r($fieldlistoptions);
				//==========================================================================
                                $usedTempTable = array();
				$helper = new ReportHelper($reportConn);
				switch($reporttype){
					case "category":
						if($channel!="all")
							$where_condition.= "and (cm.channel_choice='' or find_in_set($channel,cm.channel_choice)) ";
						if($campaign!="all")
							$where_condition.= "and agc.campaign_id=$campaign ";
						if($adtype!="all")
							$where_condition.= "and ad_format='$adtype' ";
						if($country!="all")
							$where_condition.= "and (cm.geography is null or find_in_set('$country',cm.geography)) ";
						if($state!="all")
						{
							$geostr = "$country".":"."$state";
							$where_condition.= "and (cm.geography is null or find_in_set('$geostr',cm.geography))";
						}	
						$select ="select sum(agc.impressions) as totimpressions, 
				   				sum(agc.clicks) as totclicks, 
				   				round((sum(agc.clicks)*100)/sum(agc.impressions),2) as ctr"; 
				   				//sum(agc.total)as rev, 
				   				//(sum(agc.total)*1000)/sum(agc.impressions) as ecpm";
						$from=" from aggregate_category as agc";
						$left_join =" left join {$metaTablePrefix}campaign as cm on agc.campaign_id=cm.id left join {$metaTablePrefix}ads on ads.id=agc.ad_id"; 
						$usedTempTable['campaign'] =true;
                                                $usedTempTable['ads'] = true;
                                                $where=" 1 $where_condition"; 
						$group_condition="agc.category";
						$aggregateNames.=", agc.category as name ";
						break;
					case "channel":
						$helper->createPriceConfigTable($startdate,$enddate, 'pc');
						$helper->createPriceConfigTable($startdate,$enddate, 'pc1',false);
						$select = "select 
				   				sum($impr) as totimpressions, 
				   				sum(clicks) as totclicks, 
				   				round((sum(clicks)*100)/sum($impr),2) as ctr
				   				$vdo
				   				sum(if(pc1.channel_id is NULL, if(pc.channel_id is NULL,pubrevenue_advcurrency,
								if(pc.model='CPM',impressions*(pc.price_per_unit*((c1.ex_rate)/(c2.ex_rate))),if(ads.parent_ad_id IS NULL,{$db_prefix}daily_stats.clicks,0)*pc.price_per_unit*((c1.ex_rate)/(c2.ex_rate)))),if(pc1.model='CPM',impressions*pc1.price_per_unit*((c1.ex_rate)/(c2.ex_rate)),if(ads.parent_ad_id IS NULL,{$db_prefix}daily_stats.clicks,0)*pc1.price_per_unit*((c1.ex_rate)/(c2.ex_rate))))) as rev, 
				   				(sum(if(pc1.channel_id is NULL, if(pc.channel_id is NULL,pubrevenue_advcurrency,
								if(pc.model='CPM',impressions*(pc.price_per_unit*((c1.ex_rate)/(c2.ex_rate))),if(ads.parent_ad_id IS NULL,{$db_prefix}daily_stats.clicks,0)*pc.price_per_unit*((c1.ex_rate)/(c2.ex_rate)))),if(pc1.model='CPM',impressions*pc1.price_per_unit*((c1.ex_rate)/(c2.ex_rate)),if(ads.parent_ad_id IS NULL,{$db_prefix}daily_stats.clicks,0)*pc1.price_per_unit*((c1.ex_rate)/(c2.ex_rate)))))*1000/sum($impr)) as eCPM";
						$from="from {$db_prefix}daily_stats";
                                                $c1 = $metaTablePrefix ? "{$metaTablePrefix}c1": "currency";
                                                $c2 = $metaTablePrefix ? "{$metaTablePrefix}c2": "currency";
						$left_join=" left join {$metaTablePrefix}channels on {$db_prefix}daily_stats.channel_id=channels.id left join {$metaTablePrefix}campaign on {$db_prefix}daily_stats.campaign_id=campaign.id left join publisher on publisher.id=channels.publisher_id left join advertisersheet on
				advertisersheet.advertiser_id=campaign.advertiser_id left join {$metaTablePrefix}ads on ads.id={$db_prefix}daily_stats.ad_id  left join
						 			tmp.pc as pc on (pc.adtype = {$db_prefix}daily_stats.adtype and ({$db_prefix}daily_stats.date>=pc.start_date and {$db_prefix}daily_stats.date <= pc.end_date) and {$db_prefix}daily_stats.channel_id=pc.channel_id) 
									left join tmp.pc1 as pc1 on ({$db_prefix}daily_stats.ccode = pc1.ccode and pc1.adtype = {$db_prefix}daily_stats.adtype and ({$db_prefix}daily_stats.date>=pc1.start_date and {$db_prefix}daily_stats.date <= pc1.end_date) and {$db_prefix}daily_stats.channel_id=pc1.channel_id)
left join $c1 as c1 on c1.updated = {$db_prefix}daily_stats.date and c1.code = advertisersheet.currency
left join $c2 as c2 on c2.updated = {$db_prefix}daily_stats.date and c2.code = publisher.currency ";
						$usedTempTable['currency1'] = true;
                                                $usedTempTable['currency2'] = true;
                                                $usedTempTable['channels'] = true;
                                                $usedTempTable['campaign'] = true;
                                                $usedTempTable['ads'] = true;
                                                /* 
						 This is for Uniques repopting and also for publisher/Campaign report
						$group_condition=" daily_stats.channel_id,campaign_id";
						*/
						// For one row for each apps  
						$group_condition=" {$db_prefix}daily_stats.channel_id";
						$where="campaign.advertiser_id = '$_SESSION[ADV_ID]' and campaign.device!='iphone' and ads.id IS NOT NULL and ads.parent_ad_id IS NULL";
						$aggregateNamesSRI=$aggregateNames.",{$db_prefix}daily_stats.ad_id as sriid";
						$aggregateNames.=", channels.id , if(channels.name IS NOT NULL, channels.name,'Other') as name ,campaign.name as campaignName ";
						
						$whereUniques="campaign.id={$db_prefix}aggregate_ads.campaign_id and {$db_prefix}aggregate_ads.channel_id =channels.id and {$db_prefix}aggregate_ads.ad_id=0 and {$db_prefix}aggregate_ads.channel_id!=0 and campaign.advertiser_id='$_SESSION[ADV_ID]' and campaign.device!='iphone'";
						$aggregateNamesU=$aggregateNames;
						//$aggregateNamesU.=$aggregateNames;
						$fromUniques = "from {$db_prefix}aggregate_ads, campaign, channels ";
						if($orderby!="")
								$sriorderby=" sriid,".$orderby;
						else
							$sriorderby=" sriid";
						if($orderby!="")
								$orderby="name, campaignName,".$orderby;
						else
							$orderby="name, campaignName";
						
						
						break;
					case "campaign":
						  $select = "select 
				   				sum($impr) as totimpressions, 
				   				sum(clicks) as totclicks $vdo 
				   				round((sum(clicks)*100)/sum($impr),2) as ctr
				   				"; 
							/*Notes:=> Uniques and avg frequency need to be fectch from daily_stats*/
				   				//sum(clicks) as ec
				   				//round((sum(clicks)*100)/sum($impr),2) as ecr
				   				//sum(revenue_advcurrency)as rev, 
				   				//(sum(revenue_advcurrency)*1000)/sum(impressions) as ecpm";
						$from="from {$db_prefix}daily_stats";
						$left_join=" left join {$metaTablePrefix}campaign on {$db_prefix}daily_stats.campaign_id=campaign.id left join {$metaTablePrefix}ads on ads.id={$db_prefix}daily_stats.ad_id";
						$usedTempTable['campaign'] = true;
                                                $usedTempTable['ads'] = true;
                                                $group_condition=" {$db_prefix}daily_stats.campaign_id";
						$where="campaign.advertiser_id = '$_SESSION[ADV_ID]' and campaign.device!='iphone' and ads.id IS NOT NULL and ads.parent_ad_id IS NULL";
						$aggregateNamesSRI=$aggregateNames.",{$db_prefix}daily_stats.ad_id as sriid";
						$aggregateNames.=", campaign.name,campaign.id ";
						
						
						if($sitestring==" ")
							$channelCampaignsql = "and {$db_prefix}aggregate_ads.channel_id=0";
						else
							$channelCampaignsql = "and {$db_prefix}aggregate_ads.channel_id='$channel'";
						
						$whereUniques="campaign.id=campaign_id and {$db_prefix}aggregate_ads.ad_id=0 $channelCampaignsql and campaign.advertiser_id = '$_SESSION[ADV_ID]' and campaign.device!='iphone' ";
						$aggregateNamesU=$aggregateNames;
						
						if($orderby!="")
								$sriorderby=" sriid,".$orderby;
						else
							$sriorderby=" sriid";
						if($orderby!="")
							$orderby="name, ".$orderby;
						else
							$orderby="name";
						$fromUniques = "from {$db_prefix}aggregate_ads, campaign";
						
						
						break;
					case "country":
						$select = "select 
				   				sum($impr) as totimpressions, 
				   				sum(clicks) as totclicks 
				   				$vdo
				   				round((sum(clicks)*100)/sum($impr),2) as ctr"; 
				   				//sum(revenue_advcurrency)as rev, 
				   				//(sum(revenue_advcurrency)*1000)/sum(impressions) as ecpm";
						$from="from {$db_prefix}daily_stats";
						$left_join=" left join {$metaTablePrefix}campaign on {$db_prefix}daily_stats.campaign_id=campaign.id left join {$metaTablePrefix}ads on ads.id={$db_prefix}daily_stats.ad_id";
						$usedTempTable['campaign'] = true;
                                                $usedTempTable['ads'] = true;
                                                $group_condition=" {$db_prefix}daily_stats.ccode";
						$where="campaign.advertiser_id = '$_SESSION[ADV_ID]' and campaign.device!='iphone' and ads.id IS NOT NULL and ads.parent_ad_id IS NULL";
						$aggregateNames.=", if({$db_prefix}daily_stats.ccode='','Other',{$db_prefix}daily_stats.ccode) as name ";
						if($orderby!="")
							$orderby="name, ".$orderby;
						else
							$orderby="name";
							
						break;
					case "state":
						$select = "select 
				   				sum($impr) as totimpressions, 
				   				sum(clicks) as totclicks 
				   				$vdo
				   				round((sum(clicks)*100)/sum($impr),2) as ctr"; 
				   				//sum(revenue_advcurrency)as rev, 
				   				//(sum(revenue_advcurrency)*1000)/sum(impressions) as ecpm";
						$from="from {$db_prefix}daily_stats";
						$left_join=" left join {$metaTablePrefix}campaign on {$db_prefix}daily_stats.campaign_id=campaign.id left join {$metaTablePrefix}ads on ads.id={$db_prefix}daily_stats.ad_id";
						$usedTempTable['campaign'] = true;
                                                $usedTempTable['ads'] = true;
                                                $group_condition=" {$db_prefix}daily_stats.state_code";
						$where="campaign.advertiser_id = '$_SESSION[ADV_ID]' and campaign.device!='iphone' and ads.id IS NOT NULL and ads.parent_ad_id IS NULL";
						$aggregateNames.=", if({$db_prefix}daily_stats.state_code='','Other',{$db_prefix}daily_stats.state_code) as name, if({$db_prefix}daily_stats.ccode='','Other',{$db_prefix}daily_stats.ccode) as country_name  ";
						
						if($orderby!="")
							$orderby="name, ".$orderby;
						else
							$orderby="name";
						$smarty->assign("ctn",$country);
						break;
					case "ad":
						$select = "	select 
				   						sum($impr) as totimpressions, 
				   						sum($clkr) as totclicks
				   						$vdo
				   						round((sum(clicks)*100)/sum($impr),2) as ctr,
										sri_details,
										campaign.id as cid"; 
						$from="from {$db_prefix}daily_stats";
						$left_join="left join {$metaTablePrefix}campaign on campaign.id={$db_prefix}daily_stats.campaign_id left join {$metaTablePrefix}ads on ads.id={$db_prefix}daily_stats.ad_id";
						$usedTempTable['campaign'] = true;
                                                $usedTempTable['ads'] = true;
                                                $group_condition=" {$db_prefix}daily_stats.campaign_id, {$db_prefix}daily_stats.ad_id";
						$where="campaign.advertiser_id = '$_SESSION[ADV_ID]' and campaign.device!='iphone' and ads.id IS NOT NULL and ads.parent_ad_id IS NULL";
						$aggregateNamesSRI=$aggregateNames.",{$db_prefix}daily_stats.ad_id as sriid";
						$aggregateNames.=",ads.id, if(ads.branded_img_right_ref_imgname IS NOT NULL, ads.branded_img_right_ref_imgname,'Other') as adname, campaign.name as name";
						$whereUniques="ads.id=ad_id and {$db_prefix}aggregate_ads.campaign_id = campaign.id and campaign.advertiser_id = '$_SESSION[ADV_ID]' and campaign.device!='iphone'";
						
						if($sitestring==" ")
							$channelCampaignsql = "and {$db_prefix}aggregate_ads.channel_id=0";
						else
							$channelCampaignsql = "and {$db_prefix}aggregate_ads.channel_id='$channel'";
						
						$whereUniques="ads.id=ad_id and {$db_prefix}aggregate_ads.campaign_id = campaign.id and campaign_id!=0 and ad_id!=0 $channelCampaignsql and campaign.advertiser_id = '$_SESSION[ADV_ID]' and campaign.device!='iphone'";
						$fromUniques = "from {$db_prefix}aggregate_ads, ads, campaign";
						//$campaignChannel = "and campaign_id!=0 and ad_id!=0 and channel_id=0 ";
						$aggregateNamesU.=$aggregateNames;
						
						if($orderby!="")
								$sriorderby=" sriid,".$orderby;
						else
							$sriorderby=" sriid";
						
						if($orderby!="")
								$orderby=" name, adname, ".$orderby;
						else
							$orderby=" name, adname ";
						break;
					default:
						$left_join="";
						$group_condition="";
						$where="";
						$whereNuiques="";
						$aggregateNames="";
						$groupByUniques = "";
						break;
				}
				
				if($period == "summary"){
					 $sql = "$select $aggregateNames $from $left_join where $where 
				   				$sitestring $campaignstring $adtypestring 
				   				$countrystring $provincestring $datequery  
				   				$group  $aggregate $group_condition
				   			Order By $orderby";
				}
				else{
			    	$sql = "$select $aggregateNames $from $left_join where $where 
				   				$sitestring $campaignstring $adtypestring 
				   				$countrystring $provincestring $datequery  
				   				$group $aggregate, $group_condition 
				   			Order By  $orderby";	
					
				}
				if(isset($reporttype)){
                                                $tTableFactiory = TemporaryTableFactory::getInstance(); 
                                                //creating all temporary tables
                                                if($enableTemporaryTables){
                                                    foreach(array_keys($usedTempTable) as $key){
                                                        $usedTempTable[$key] = $tTableFactiory->createTemporaryTableByKey($key);
                                                        //echo $usedTempTable[$key]->getStatement();
                                                        $usedTempTable[$key]->create();
                                                     }
                                                }
                                                //removing all temporary tables
                                                //echo $sql;
						$rs = $reportConn->execute($sql);	
                                                if($enableTemporaryTables){
                                                    foreach($usedTempTable as $tTable){
                                                        if(is_object($tTable))
                                                            $tTable->drop();
                                                    }
                                                }
                                                
				}
				if($reporttype == 'channel'){
					$helper->dropPriceConfigTable('pc');
					$helper->dropPriceConfigTable('pc1');
				}
				if($rs && $rs->recordcount()>0){
					$summary = $rs->getrows();
				}
				$final_data=array();
				if($reporttype=="country"){
					foreach($summary as $key=>$value){
						$temp['cat']=$value['name'];
						$value['name']=insert_country_names ( $temp);
						$final_data[$key]=$value;
						//var_dump($value['name']);
					}
						
					$summary=$final_data;
				}
				if($reporttype=="state"){
					foreach($summary as $key=>&$value){
                        $temp['cat']=$value['country_name'];
                        $value['country_name']=@insert_country_names ( $temp );
                        if(strlen($value['country_name'])!=0){
                      	 $temp['state']="$temp[cat]:$value[name]";
                       	 $value['name']=insert_state_names ( $temp );
                        	$final_data[$key]=$value;
		                        
		          		}
					}		
					$summary=$final_data;
				}
				# Engagement clicks & Engagement click ratio
				$_SESSION['reporttype']=$reporttype;
				$_SESSION['period']='summary';
				$data=$_SESSION['data']=$summary;
				//var_dump($data);
				if($reporttype=='campaign'){
        		  foreach($data as $key => $value){
        		    $cmpgn_id[]=$value['id'];
        		  }
        		  $cmpgn_ids = implode(',',$cmpgn_id);
        		  $sql="select group_concat(ad_id) as ad_ids from campaign_members cm, ads where cm.ad_id=ads.id and ads.parent_ad_id IS NOT NULL and cid in($cmpgn_ids)";
        		  $rsa = $reportConn->Execute($sql);
                  if($rsa&&$rsa->recordcount()>0){
                    $ad_ids = $rsa->fields['ad_ids'];
                    if($period == "summary"){
        			  $sqlSri = "select campaign_id, sum(clicks) as sriclk from {$db_prefix}daily_stats where ad_id in ($ad_ids) $sitestring $campaignstring 
         			  $countrystring $provincestring $datequery $group campaign_id $aggregate;";
        			  $rsSri=$reportConn->execute($sqlSri);
               		  if($rsSri && $rsSri->recordcount()>0){
               		    $sri_rows = $rsSri->getrows();
               		  }
        			}
        			else{
        		      $sqlSri = "select campaign_id, sum(clicks) as sriclk, date as timeframe from {$db_prefix}daily_stats where ad_id in ($ad_ids) $sitestring $campaignstring 
         			  $datequery $countrystring $provincestring $group campaign_id, $aggregate";
        			  $rsSri=$reportConn->execute($sqlSri);
               		  if($rsSri && $rsSri->recordcount()>0){
               		    $sri_rows = $rsSri->getrows();
               		  }
        			}
                  }
        
                  foreach($data as $dk => $drow){
                    foreach($sri_rows as $sk => $srow){
                      if($period == "summary"){
                        if($drow['id']==$srow['campaign_id']){
                         		 $data[$dk]['ec']+=$srow['sriclk'];
                         	 	$data[$dk]['ecr']=sprintf("%01.2f", $data[$dk]['ec']*100/$data[$dk]['totimpressions']);
                        }
                      }else{
                        if($drow['id']==$srow['campaign_id'] && $drow['timeframe']==$srow['timeframe']){
                         		 $data[$dk]['ec']+=$srow['sriclk'];
                         	 	$data[$dk]['ecr']=sprintf("%01.2f", $data[$dk]['ec']*100/$data[$dk]['totimpressions']);
                        }
                      }
                    }
                  }
				}elseif($reporttype=='ad'){
					;
				}
				$summary=$data; 
				unset($data);
				
				/* New Talk2Me Report Begin	*/
				$tarray =array();
				if(in_array("Talk2Me", $SelectedField)){
					foreach ($SelectedField as $key => $val){
						if($key!="talk2me")
							$tarray[$key] = $val;
					}
					$SelectedField = array();
					$SelectedField = $tarray;
					$Talk2Me =true;
				}
				else $Talk2Me = false;
				if($Talk2Me){
					$SriHeader = array();
					require_once ABSPATH.'/common/portal/report/Talk2Me.php';
					$talk2me = new Talk2Me();
					$talk2me->run(&$summary, &$SelectedField, &$SriHeader);
					//echo "<pre>"; print_r($SriHeader);
					
					$smarty->assign("SelectedField",$SelectedField);
					$smarty->assign("sriHeader",$SriHeader);
				}
				/* New Talk2Me Report End */
				$smarty->assign("SelectedField",$SelectedField);
				$smarty->assign("sriHeader",$SriHeader);
				
				if($period=="daily" || $period=="weekly" || $period=="monthly"){
					//============Data Array for uniques from aggregate_ads===============
					$sqlUniques = "select 
								value as uniques $aggregateNamesU
							$fromUniques
							where 
								$whereUniques $campaignChannel $sitestring $campaignstringU $datequery $measure ";
					
					$rsUniques = $reportConn->execute($sqlUniques);	
					if($rsUniques && $rsUniques->recordcount()>0){
						$summaryUniques = $rsUniques->getrows();
					}
					foreach ($summary as $key =>$val){
						$summary[$key]['uniques'] = "";
						foreach ($summaryUniques as $ukey => $uval){
							if($val['timeframe']==$uval['timeframe'] && $val['id']==$uval['id']){
								$summary[$key]['uniques'] = $uval['uniques'];
								$summary[$key]['avgfrequency'] = ceil($summary[$key]['totimpressions']/$uval['uniques']);
							}
						}
					}
				}
				//====================================================================
				if($period == "summary"){
					$_SESSION['reporttype']=$reporttype;
					$_SESSION['period']='summary';
					$_SESSION['data']=$summary;
					
					//================ Field List Start ===========
						$totalArr = array();
						foreach ($summary as $k => $v){
							foreach ($fieldlistoptions as $key => $val){
							if($key!='cr' && $key!='ctr' && $key!='ecr' && $key!='eCPM') $totalArr[$key]+=$v[$key];
                                                                }
								
							foreach ($SriHeader as $key => $val){
								$totalArr[$val]+=$v[$val];
							}
						}
					//	$totalArr['ctr']=sprintf("%01.2f", $totalArr['totclicks']*100/$totalArr['totimpressions']);
					//	$totalArr['ecr']=sprintf("%01.2f", $totalArr['ec']*100/$totalArr['totimpressions']);
					//================ Field List End ===========
					
					$_SESSION['SelectedField']=$SelectedField;
					$_SESSION['totalArr']=$totalArr;
					$smarty->assign("totalArr",$totalArr);
					$smarty->assign("data",$summary);
				}
				else{
					foreach ( $summary as $key => $value ){
						if($reporttype=="crossChannel")
							$data[$value['timeframe']][] = $value;
						else
							$data[$value['name']][] = $value; 
	
						if(!in_array($value['timeframe'],$alltimeFrame))
								$alltimeFrame[] = $value['timeframe'];
							
					}
					
					//==========Total Aggrgation for multiple campaign start===========
						if(is_admin_loggedin()){
							sort($alltimeFrame);
							foreach($alltimeFrame as $tkey =>$tval){
								unset($tempAllVal);
								foreach ($data as $k =>$val){
									foreach($val as $arrk => $arrv){						
										if($tval==$arrv['timeframe']){
											 
											foreach($SelectedField as $fkey=>$fval){
												$tempAllVal[$fkey]+=$arrv[$fkey];
											}
											$tempAllVal['timeframe']=$tval;
										}    
									}
								}
								$data['all'][$tkey] = $tempAllVal;
								$data['all'][$tkey]['ctr'] = sprintf("%01.2f", $data['all'][$tkey]['totclicks']*100/$data['all'][$tkey]['totimpressions']);
								if($uniqueReportFlag==true){
									//$data['all'][$tkey]['avgfrequency']=ceil($data['all'][$tkey]['totimpressions']/$data['all'][$tkey]['value']);
								}
							}
						}
						//==========Total Aggrgation for multiple campaign End===========
						
					
					$_SESSION['reporttype']=$reporttype;
					$_SESSION['period']=$period;
					$_SESSION['data']=$data;
					
					//================ Field List Start ===========
					
					$tempfldArr = array();
					$tempfldArr =  $fieldlistoptions;
					$tempfldArr['uniques'] = 'Uniques';
					$tempfldArr['avgfrequency'] = 'Avg Frequency';
					foreach ($sri_elements as $k =>$v){
						$tempfldArr[$v] = $v;
					}
					//print_r($data);exit;
					foreach ($data as $k => $v){
						
						foreach ($v as $dkey => $dval){
							foreach ($tempfldArr as $key => $val){
								if($key!='cr' && $key!='ctr' && $key!='ecr' && $key!='eCPM')
									$totalArr[$k][$key]+=$dval[$key];
							}
							foreach ($SriHeader as $key => $val){
								$totalArr[$k][$val]+=$dval[$val];
							}
						}
						
					//	$totalArr[$k]['ctr']=sprintf("%01.2f", $totalArr[$k]['totclicks']*100/$totalArr[$k]['totimpressions']);
					//	$totalArr[$k]['ecr']=sprintf("%01.2f", $totalArr[$k]['ec']*100/$totalArr[$k]['totimpressions']);
						$totalArr[$k]['avgfrequency']=ceil($totalArr[$k]['totimpressions']/$totalArr[$k]['uniques']);
						
					}
					//================ Field List End ===========
					$smarty->assign("totalArr",$totalArr);
	
					$_SESSION['SelectedField'] = $SelectedField;
					$_SESSION['totalArr']=$totalArr;
					$smarty->assign("data",$data);
				}
			}
		}
		//=================================================================================	
		break;
      	case "newadvreport":
	            require_once ABSPATH . "/common/portal/report/reportApp/ReportController.class.php";
		    $controller = new ReportController();
		    $controller->dispatch();
		    break;
}



function getAllTempTables(){
    $factory = TemporaryTableFactory::getInstance();
    
    return array(
        'channels' => $factory->createTemporaryTableByKey('channels'),
        'campaign' => $factory->createTemporaryTableByKey('campaign'),
        'ads' => $factory->createTemporaryTableByKey('ads'),
    );
    
    
    
    
}
?>
