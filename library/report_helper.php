<?php
include_once("../include/config.php");

$aggregate="";
switch ($category) {
	case "daily":
		$aggregate="date(hour),";
		$comparator="date(hour)";
		break;
	case "monthly":
		default:
		$aggregate="month(hour),year(hour),";
		$comparator="date(hour)";
		break;
	case "quarterly":

		$aggregate="quarter(hour),year(hour),";
		$comparator="date(hour)"; //May want to change this
		break;
	case "dayofweek":
		$aggregate="dayofweek(hour),";
		$comparator="date(hour)";
		break;
	case "yearly":
		$aggregate="year(hour),";
		$comparator="date(hour)";
		break;
	case "alltime":
		$aggregate="";
		$comparator="date(hour)"; //May want to change this
		break;
}

switch($reportype) {
	case "category":
		$extrafields="campaign_id,category";
		$table='aggregate_category';
		break;
	case "geography":
		$extrafields="campaign_id,city, country";
		$table='aggregate_geography';
		break;
	case "domain":
	default:
		$extrafields="campaign_id,domain";
		$table='aggregate_domain';
		break;
}


$sql="select sum(impressions)as timp, sum(total) as total, sum(clicks) as tclicks, $aggregate $extrafields from $table where $comparator <= '2007-12-1' and $comparator >= '2007-10-1' group by $aggregate $extrafields";

echo "$sql<br>\n";
$rs=$conn->execute($sql);


print_r($rs->getRows());

?>