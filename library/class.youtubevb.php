<?php

class YoutubeVBAds{
  	private $conn;
  	private $smarty;
  	private $config;
	
	public function __construct(){
    	global $conn, $smarty, $config;
    	$this->conn 	= $conn;
    	$this->smarty 	= $smarty;
    	$this->config 	= $config;
  	}
  
	public function processYoutubevbAd($campaign, $adformat, $adid_already){
	  	$youtubevbads	= $campaign['youtubevbads'];
	  	//print_r($campaign['youtubevbads']);exit;
	  	foreach ($youtubevbads as $adrow) {
			/* This will be used in case of edit campaign when no changes being done in ads details*/
	  		if($adrow['id']!="") {
	  			$adid_already[]=$adrow['id'];
	  			continue;
	  		}
	  		/* end */  
			$branded_img_bot_ref_txbody ="";
			$vdoID			= mysql_escape_string(strip_tags($adrow['vdoID']));
			$name			= mysql_escape_string(strip_tags($adrow['name']));
			$dimension 		= '300x250'; 
			$trackurl		= mysql_escape_string($adrow['trackurl']);
			$autoPlay		= mysql_escape_string($adrow['autoPlay']);
			$optdesturl		= mysql_escape_string($adrow['optdesturl']);
			if(isset($adrow['vi_0'])&&$adrow['vi_0']!="") $ad_details["vdo_tvi_0"]=mysql_escape_string(strip_tags($adrow['vi_0']));
			if(isset($adrow['vi_25'])&&$adrow['vi_25']!="") $ad_details["vdo_tvi_25"]=mysql_escape_string(strip_tags($adrow['vi_25']));
			if(isset($adrow['vi_50'])&&$adrow['vi_50']!="") $ad_details["vdo_tvi_50"]=mysql_escape_string(strip_tags($adrow['vi_50']));
			if(isset($adrow['vi_75'])&&$adrow['vi_75']!="") $ad_details["vdo_tvi_75"]=mysql_escape_string(strip_tags($adrow['vi_75']));
			if(isset($adrow['ae'])&&$adrow['ae']!="") $ad_details["vdo_tae"]=mysql_escape_string(strip_tags($adrow['ae']));
			if(isset($adrow['autoPlay'])&&$adrow['autoPlay']!="") $ad_details["autoPlay"]=mysql_escape_string(strip_tags($adrow['autoPlay']));
			if(sizeof($ad_details)>0){
				$ad_details = json_encode($ad_details);
				$ad_details = str_replace('\/','/',$ad_details);
			} else $ad_details='';
			
			//exit("hjhjhj");
			$fieldset = array(
					'ad_format'=>$adformat, 
					'playlist_ad_ref'=>$vdoID, 
					'advertiser_id'=>$_SESSION[ADV_ID], 
					'branded_img_right_ref_imgname'=>$name, 
					'tracker_url'=>$trackurl, 
					'dimension'=>$dimension,
					'destination_url'=>$optdesturl
					);
			if(isset($ad_details) && $ad_details!="")
				$fieldset['ad_details'] = $ad_details;
			else
				$fieldset['ad_details'] = NULL;
			
			//print_r($fieldset);exit;
			$ad = $this->insertYoutubeAd($fieldset);
			
			return $ad;
			
		}
	} 
	/**
	 * This function will accept an array containing the key as field name and value will be the value we want to store 
	 */
	public function insertYoutubeAd($fieldset) {
		
		$sql="insert into ads set" ;
		foreach ($fieldset as $key=>$val) {
			$sql .= " $key='".$val."',";
			
		}
		$sql = substr($sql,0,-1);
		$rs = $this->conn->execute($sql);
		
		if($rs)
		{
			$ad = $this->conn->Insert_ID();
			//$parent_ad_id = $ad;
		
		} else
			$ad=-1;
		return $ad;
	}
	
	public function updateYoutubeAds($campaign) {
		
		$youtubevbads = $campaign['youtubevbads'];
		foreach( $youtubevbads as $index_key => $value ){
			# check, if dirty flag is on
			if($value['dirty']==true )
			{
				$sqlstr='';
				foreach($value as $key => $val ){
					if( $key=="name" ){
						$sqlstr.="branded_img_right_ref_imgname='$val',";
					}elseif($key=="vdoID" ){
						$sqlstr.="playlist_ad_ref='$val',";
					}elseif($key=="trackurl" ){
						$sqlstr.="tracker_url='$val',";
					}elseif($key=="optdesturl" ){
						$sqlstr.="destination_url='$val',";
					}elseif($key=="vi_0"){
						if(isset($val) && $val!="") $ad_details['vdo_tvi_0']=$val;
					}elseif($key=="vi_25"){
						if(isset($val) && $val!="") $ad_details['vdo_tvi_25']=$val;
					}elseif($key=="vi_50"){
						if(isset($val) && $val!="") $ad_details['vdo_tvi_50']=$val;
					}elseif($key=="vi_75"){
						if(isset($val) && $val!="") $ad_details['vdo_tvi_75']=$val;
					}elseif($key=="ae"){
						if(isset($val) && $val!="") $ad_details['vdo_tae']=$val;
					}elseif($key=="autoPlay"){
						if(isset($val) && $val!="") $ad_details['autoPlay']=$val;
					}
				}
				if(isset($ad_details) && sizeof($ad_details)>0){
					$ad_details = json_encode($ad_details);
					$ad_details = str_replace('\/','/',$ad_details);
				}else $ad_details='';
				if($ad_details!='') $sqlstr.="ad_details='$ad_details',"; else $sqlstr.="ad_details=NULL";
				if($sqlstr!=""){
					$sql="update ads set ".trim($sqlstr,',')." where id='$value[id]' and advertiser_id='$_SESSION[ADV_ID]'";
					$this->conn->execute($sql);
				}
			}
		}
	}
  
}

?>