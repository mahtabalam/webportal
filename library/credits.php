<?php

include_once('include/datefuncs.php');

require_once('include/smartyBlockFunction.php');
$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);

if(isset($config['DB_PREFIX']) && $config['DB_PREFIX']!=''){
	$db_prefix =$config['DB_PREFIX'];
}else {
	$db_prefix='';
}

switch ($_REQUEST['sub']) {
	case 'buycredits':
 
			$smarty->assign('paypalemail',$config['paypalemail']);
			$sql = $conn->Prepare("select * from billinginfo where adv_id = ?");
			$rs = $conn->execute($sql, array($_SESSION[ADV_ID]));
			if($rs && $rs->recordcount()>0){
				$billinfo = $rs->getrows();
				$smarty->assign('billinfo',$billinfo);
			}

			$sql = $conn->Prepare("select credit_amt as currentcredits from adv_credit where adv_id = ?");
			$rs = $conn->execute($sql, array($_SESSION[ADV_ID]));
			if($rs && $rs->recordcount()>0){
				$currentcredits = $rs->fields['currentcredits'];
				$smarty->assign('currentcredits',$currentcredits);
			}
			$sql = $conn->Prepare("select currency from advertisersheet where advertiser_id = ?");
			$rs = $conn->execute($sql, array($_SESSION[ADV_ID]));
			if($rs && $rs->recordcount()>0){
				$currency = $rs->fields['currency'];
				$smarty->assign('currency',$currency);
			}
			$sql = "SELECT cast(daily_limit*datediff(curdate( ),validto) as decimal) as est_cost1, cast(daily_limit*30 as decimal) as est_cost2, cast((budget_amt-tot_money_spent) as decimal) as rem_budget, name, id FROM campaign WHERE datediff( validfrom, curdate( ) )  < 31 and datediff(  curdate( ), validto )  > 0  and advertiser_id = '$_SESSION[ADV_ID]'";
			if(is_array($_REQUEST['ids'])&&$_REQUEST['ids']!="" ) {
				$ids=mysql_escape_string(implode(',',$_REQUEST['ids']));
				$sql.=" and id in ($ids)";
			}

			$rs= $conn->execute($sql);
			if($rs && $rs->recordcount()>0){
				$campaign= $rs->getrows();
				$smarty->assign('campaign',$campaign);
				$smarty->assign('totalamount',$totalamount);
				$smarty->assign('end',$end);
			}
		//Paused
	break;
	case "creditsummary" :
			$smarty->assign("monthoptions",$monthoptions);
			$smarty->assign("yearoptions",$yearoptions);
			$totalamt = 0;
			$total = 0;
			$smarty->assign("totalamt",$totalamt);
			$smarty->assign("total",$total);

			$sql = $conn->Prepare("select * from billinginfo where adv_id = ?");
			$rs = $conn->execute($sql, array($_SESSION[ADV_ID]));
			if($rs && $rs->recordcount()>0){
				$billinfo = $rs->getrows();
				$smarty->assign('billinfo',$billinfo);
			}

			$sql = $conn->Prepare("select credit_amt as currentcredits from adv_credit where adv_id = ?");
			$rs = $conn->execute($sql, array($_SESSION[ADV_ID]));
			if($rs && $rs->recordcount()>0){
				$currentcredits = $rs->fields['currentcredits'];
				$smarty->assign('currentcredits',$currentcredits);
			}
			$sql = $conn->Prepare("select currency from advertisersheet where advertiser_id = ?");
			$rs = $conn->execute($sql, array($_SESSION[ADV_ID]));
			if($rs && $rs->recordcount()>0){
				$currency = $rs->fields['currency'];
				$smarty->assign('currency',$currency);
			}
			$curmonth = date("m");
			$curyear = date("y");
			$smarty->assign('curmonth',$curmonth);$smarty->assign('curyear',$curyear);
			if(!isset($_REQUEST['startmonth']) || !isset($_REQUEST['startyear'] )  || !isset( $_REQUEST['endmonth']) || !isset($_REQUEST['endyear'] ) )
			{
				$lastmonth  = lastNMonths(2);
				$fromdate = $lastmonth[0];
				global $dateformat;
				//$todate = date($dateformat);
				$todate = date('Y-m-d H:i:s');
				$enddate = date('Y-m-d',strtotime($todate));

				$_REQUEST['startyear']=date("Y",strtotime($fromdate));
				$_REQUEST['startmonth']=date("m",strtotime($fromdate));

				$_REQUEST['endmonth'] = date("m");
				$_REQUEST['endyear'] = date("Y");

			}
			else
			{
				$start = begendofmonth($_REQUEST['startmonth'],$_REQUEST['startyear']);
				$end = begendofmonth($_REQUEST['endmonth'],$_REQUEST['endyear']);
				if(timediff($_REQUEST['startmonth'], $_REQUEST['endmonth'], $_REQUEST['startyear'], $_REQUEST['endyear']) < 0){
					$smarty->assign("err" , "Invalid Range selected");
					break;
				}
				$fromdate = $start[0];
				$todate = $end[1];
				$startmonth=date("M",strtotime($fromdate));
				$endmonth=date("M",strtotime($end[0]));
			}
			$sql = $conn->Prepare("SELECT transaction_id, payment_type, date_format(payment_time,'%d %b %Y' ) as date, monthname(payment_time) as month ,  year(payment_time) as year  , amount-tax as amount, tax, status, amount-tax as credits  FROM rx_transactions WHERE datediff(payment_time , ?)>=0  and datediff(payment_time , ?)<=0 and adv_id = ? order by payment_time ASC");
	
			$rs=$conn->execute($sql, array($fromdate, $todate, $_SESSION[ADV_ID]));
			if($rs && $rs->recordcount()>0){
				$trans_summary = $rs->getrows();
				//$smarty->assign('trans_summary',$trans_summary);
			}

			$sql = $conn->Prepare("SELECT monthname(month) as month, year(month) as year ,credit_amt  FROM adv_credit_statement WHERE datediff(month , ?)>=0  and datediff(month , ?)<=0 and adv_id = ? order by year(month),month(month) asc");
			$rs= $conn->execute($sql, array($fromdate, $todate, $_SESSION[ADV_ID]));
			if($rs && $rs->recordcount()>0){
				$previouscredits = $rs->getrows();
				//$smarty->assign('previouscredits',$previouscredits);
			}

			$sql = $reportConn->Prepare("SELECT sum( total ) as credit_spent , monthname( date ) as month ,year(date) as year
			FROM {$db_prefix}aggregate_statistics as stat join campaign as c on c.id = stat.campaign_id where datediff(date , ?)>=0  and datediff(date , ?)<=0  and c.advertiser_id = ? GROUP BY month( date ),year(date) ");
			$rs=$reportConn->execute($sql, array($fromdate, $todate, $_SESSION[ADV_ID]));
			if($rs && $rs->recordcount()>0){
				$spent_summary = $rs->getrows();
				//$smarty->assign('spent_summary',$spent_summary);
			}
			$j=0;$k=0;$total=0;$totalspent=0;$i=0;
			if(isset($previouscredits))
				$monthyear_prev  = $previouscredits[$i]['month']." ".$previouscredits[$i]['year'];
			$monthyear_trans  = $trans_summary[$j]['month']." ".$trans_summary[$j]['year'];
			if(  (strtotime("$monthyear_trans" )< strtotime("$monthyear_prev") ) || (isset($trans_summary) && !isset($previouscredits))  )
			{
				$temp[0]['month']=$trans_summary[$j]['month'];
				$temp[0]['year']=$trans_summary[$j]['year'];
				$temp[0]['credit_amt']=0;
				if(isset($previouscredits))
					$previouscredits=array_merge($temp,$previouscredits);
				else
					$previouscredits=$temp;
			}

			for($i=0;$i<count($previouscredits);$i++)
			{
				$monthyear = $previouscredits[$i]['month']." ".$previouscredits[$i]['year'];
				$summary[$i]['monthyear'] = $monthyear;
				$summary[$i]['previous'] = $previouscredits[$i]['credit_amt'];
				$cnt =0;$totalpurchased=0;
				while($trans_summary[$j]['month']." ".$trans_summary[$j]['year'] == $monthyear && $trans_summary[$j]['month']!='')
				{
					if($trans_summary[$j]['status']=='Completed')
					{
						if($trans_summary[$j]['payment_type']=='credit') {
							$trans_summary[$j]['status']='Credit';
						} else if($trans_summary[$j]['payment_type']=='cash') {
							$trans_summary[$j]['status']='Cash-Purchase';
						} else if($trans_summary[$j]['payment_type']=='cheque') {
							$trans_summary[$j]['status']='Cheque-Purchase';
						} else
							$trans_summary[$j]['status']='Purchased';
					}
					$summary[$i] ['trans_summary'][$cnt] = $trans_summary[$j];
					$total = $total + $trans_summary[$j]['amount'] + $trans_summary[$j]['tax'];
					$totalpurchased = $totalpurchased + $trans_summary[$j]['amount'] + $trans_summary[$j]['tax'];
					$cnt++;$j++;
				}
				if($spent_summary[$k]['month']." ".$spent_summary[$k]['year'] == $monthyear)
				{
					$summary[$i]['spent'] = $spent_summary[$k]['credit_spent'];
					$totalspent=$totalspent+$summary[$i]['spent'] ;
					$k++;
				}
				else
					$summary[$i]['spent'] = 0;
				$summary[$i]['totalpurchased']=$totalpurchased;
			}
			$smarty->assign('summary',$summary);
			$smarty->assign('total',$total);
			$smarty->assign('totalspent',$totalspent);

	break;
}
?>
