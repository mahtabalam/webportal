<?php
include_once('include/config.php');
include_once('include/datefuncs.php');
include_once('include/function.php');

switch(strtolower($_REQUEST['tag'])){
    case "richmediatag";
      if($_REQUEST['action_download']){
        $codestring = getRequest('code');
        $filename="richmedia-tags-".gmmktime();
    	header("Content-type: text/html");
  	    header("Content-Disposition: attachment; filename=$filename.html");
  	    echo $codestring;
  	    exit;
      }
      if($_REQUEST['action_generate']){
        $cid=$_REQUEST['campaign_id'];
	    $ad_id=$_REQUEST['adid'];
	    $channel_id=$_REQUEST['ch_id'];
	    $banner_type=$_REQUEST['banner_type'];
	    $isdfp=$_REQUEST['isdfp'];
	    
	    # select query
		$sql="select tracker_url, dimension from ads where id=$ad_id";
		
		# execute query
		$rs=$conn->execute($sql);
		
		# generate video tag
		if($rs && $rs->recordcount()>0){
		  $trackerurl=$rs->fields['tracker_url'];
		  $dimension=explode('x',$rs->fields['dimension']);
		  $randomnum=gmmktime();
	      $noscript_trackerurl=tagExt($trackerurl, $randomnum);
	      if($isdfp=="yes"){
	        $macro_tstamp="%n";
	        $macro_ctu="%c"; 
	      }else{
	        $macro_tstamp="[timestamp]";
	        $macro_ctu="[insertclicktag]";
	      }
	      if($banner_type=="normal"){
	        $src=$config['AD_SRV_URL']."/js/$cid/$ad_id/$channel_id/$dimension[0]x$dimension[1]~$macro_tstamp~$macro_ctu";
	      }else{
	        $src=$config['BASE_URL']."/advertisements/richMedia_API/index/$cid/$ad_id/$channel_id/$banner_type/right;;0;0;/$dimension[0]x$dimension[1]~$macro_tstamp~$macro_ctu";
	      }
	      $preText="Never implement a JUMP tag without a corresponding AD tag, as this will result in no impressions or clicks being counted for the associated tag set.\n\nTo ensure proper cache-busting, replace [timestamp] with a dynamically generated random number.\nTo ensure proper Click tracking in your server replace [insertclicktag] with your AdServer's macro. \n\n";
		  $scriptTag="
<SCRIPT type='text/javascript' language='JavaScript' src='".$src."'>
</SCRIPT> 
<NOSCRIPT>
<img src='".$noscript_trackerurl."' width='0' height='0' border='0' /> 
<a target='_blank' href='".$config['AD_SRV_URL']."/tracker.php?m=cl&ci=".$cid."&ai=".$ad_id."&chid=".$channel_id."&ou=".$output."&rand=".$randomnum."'> 
<img src='".$config['AD_SRV_URL']."/tracker.php?m=vi&ci=".$cid."&ai=".$ad_id."&chid=".$channel_id."&ou=".$output."&rand=".$randomnum."' width='".$dimension[0]."' height='".$dimension[1]."' border='0' /> </a> 
</NOSCRIPT>";
		  $scriptTag=$preText.$scriptTag;
		  $smarty->assign("script",$scriptTag);
		}
      }
      $smarty->display("tags/$_REQUEST[tag].tpl");
    break;
	case "imagetag":
		$cid=$_REQUEST[cid];
		$ad_id=$_REQUEST[adid];

		# generate image tag	
		$code='<img src="'.$config['AD_SRV_URL'].'/conv/vi/'.$cid.'/'.$ad_id.'/CHID/tracker/na?ts='.time().'" border="0" width="0" height="0" />';
		
		$smarty->assign('tracking_tag', $code);
		$smarty->display("tags/$_REQUEST[tag].tpl");
		break;
	case "videotag":
		$cid=$_REQUEST[cid];
		$ad_id=$_REQUEST[adid];
		$channel_id=$_REQUEST['chid'];
		
		if(isset($_SESSION[ADV_PUB_ID])){
			$sq1channels = "select * from channels where verified='Verified' and publisher_id='$_SESSION[ADV_PUB_ID]' and channel_type='PC' order by name";
		}else{
			$sq1channels = "select * from channels where allow_sitetargeting='1' and verified='Verified' and channel_type='PC' order by publisher_id, name";
		}
		$rschannels=$conn->execute($sq1channels);
		if($rschannels && $rschannels->recordcount()>0){

			$sites= $rschannels->getrows();
			$smarty->assign('sites',$sites);
		}
		
		# select query
		$sql="select ads.playlist_ad_ref as fileurl, ads.id as ad_id, ads.branded_img_right_ref_imgname as adname, campaign.video_choice from ads left join 
		campaign_members on campaign_members.ad_id=ads.id left join campaign on campaign.id=
		campaign_members.cid where campaign.id=$cid and ads.parent_ad_id is null and ads.id=$ad_id";
		
		# execute query
		$rs=$conn->execute($sql);
		
		# generate video tag
		if($rs && $rs->recordcount()>0){
			$code="";
			$adRows=$rs->getrows();
			foreach($adRows as $adKey => $adVal){
				$rand=rand();
				$code.="<strong>Ad Name: $adVal[adname]</strong>";
				$adtype=explode(',',$adVal[video_choice]);
				foreach($adtype as $typVal){
					$code.="<p style='padding-top:0px; font-size:12px; font-face:Verdana'><strong>Ad Format: Video($typVal):</strong> <br /> <strong>File Url:</strong>&nbsp;&nbsp; $config[advdourl]/$adVal[fileurl] <br />";
					$code.="<strong>0% Playback:</strong>&nbsp;&nbsp; ".$config['AD_SRV_URL']."/trk/vi/$cid/$adVal[ad_id]/channel_id/vdopia/$typVal~$rand~~ <br />";
					$code.="<strong>0% Playback:</strong>&nbsp;&nbsp; ".$config['AD_SRV_URL']."/trk/vi_0/$cid/$adVal[ad_id]/channel_id/vdopia/$typVal~$rand~~ <br />";
					$code.="<strong>50% Playback:</strong>&nbsp;&nbsp; ".$config['AD_SRV_URL']."/trk/vi_50/$cid/$adVal[ad_id]/channel_id/vdopia/$typVal~$rand~~ <br />";
					$code.="<strong>100% Playback:</strong>&nbsp;&nbsp; ".$config['AD_SRV_URL']."/trk/ae/$cid/$adVal[ad_id]/channel_id/vdopia/$typVal~$rand~~ <br />";
					$code.="<strong>Click Tracker:</strong>&nbsp;&nbsp; ".$config['AD_SRV_URL']."/trk/cl/$cid/$adVal[ad_id]/channel_id/vdopia/$typVal~$rand~~ </p";
				}
			}
		}
		
		# assign to smarty
		$smarty->assign('videotag', $code);
		$smarty->display("tags/$_REQUEST[tag].tpl");
		break;
	case "uploadcreative":
		$ad_id=mysql_real_escape_string($_REQUEST[adid]);
		if($_REQUEST['action_videoad1']){
			$campaign = $_SESSION['CAMPAIGN'];
			$videoads= $campaign['videoads'];
			$uri=mysql_escape_string(escapeshellcmd(strip_tags($_REQUEST['uri'])));
			$dimension = mysql_escape_string(strip_tags($_REQUEST['dimension']));
			$duration = mysql_escape_string(strip_tags($_REQUEST['duration']));
			$flvsrc="$config[tempfilepath]/$uri.flv";
			$thmbsrc="$config[tempfilepath]/thmb$uri.jpg";
			$adbase=md5($adrow['imgurl'].rand(0,1000000000));
			$flvfile=$adbase.".flv";
			$thmbdest="$config[AD_IMG_DIR]/$adbase.jpg";
			$flvdest="$config[AD_VDO_DIR]/$flvfile";
			$rs=false;
		  
		  	error_log("rename($flvsrc,$flvdest)");
		  	error_log("rename($thmbsrc,$thmbdest)");
			
		  	if(rename($flvsrc,$flvdest) && rename($thmbsrc,$thmbdest)){
		  		$sql="update ads set playlist_ad_ref='$flvfile' where id=$ad_id";
		  		$rs=$conn->execute($sql);
				if(!$rs) {
					$msg="Creative uploading failed. Please try some time later.";
					rename($flvsrc,$flvdest);
					rename($thmbsrc,$thmbdest);
				}else{
					foreach($videoads as &$ad){
						if($ad['id']==$ad_id){
							$ad['uri']=$flvfile;
							$ad['duration']=$duration;
							$ad['dimension']=$dimension;
							$ad['dimession']=$dimension;
						}
					}
					$campaign['videoads']=$videoads;
					$_SESSION['CAMPAIGN']=$campaign;
					$msg="Creative uploaded successfully.";
				}
		  	}else{
		  		$msg="Permission Denied.";
		  	}
		}
		$smarty->assign('msg', $msg);
		$smarty->assign('adid', $ad_id);
		$smarty->display("tags/$_REQUEST[tag].tpl");
		break;
	default:
	break;
}
?>
