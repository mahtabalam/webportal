<?php

class ProcessList {
	private $conn;
	private $secretPhrase;
	private $user;
	private $time;
	private $selectString;
	
	public function __construct($dbuser){
		global $config;
		if (isset($_REQUEST['type']) && $_REQUEST['type'] == 'adhoc') {
			$database = "Database_QB_Report";
		} elseif (!isset($_REQUEST['type']) || (isset($_REQUEST['type']) && $_REQUEST['type'] == '')) {
			$database = "Database_Report5";
		}
		
		$host = SConfig::get ( $database, "host" );
		$user = SConfig::get ( $database, "user_name" );
		$pwd = SConfig::get ( $database, "password" );
		$this->conn = mysql_pconnect($host, $user, $pwd);
		$this->secretPhrase = 'Lbt#FonW%ahB?ndN*';
		$this->user = $dbuser;
		$this->time = 120;
		$this->selectString = " and INFO regexp '^select'";
	}
	
	private function getProcessList(){
		$sql = "Select ID,round(TIME/60,0) as TIME,COMMAND,STATE,INFO from information_schema.ProcessList where USER='".$this->user."' and TIME>=".$this->time.$this->selectString; 
		$rs=mysql_query($sql);
		if($rs && mysql_num_rows($rs)>0){
			while($row = mysql_fetch_assoc($rs)){
				$fetch[]=$row;
			}
			mysql_free_result($rs);
			return json_encode($fetch);
		}
	}
	
	private function killQuery(){
		$id=$_REQUEST['id'];
		$sql = "Select ID,TIME,STATE,INFO from information_schema.ProcessList where USER='".$this->user."' and TIME>=".$this->time." and ID=$id".$this->selectString;
		$rs = mysql_query($sql);
		if($rs && mysql_num_rows($rs)>0){
			while($row = mysql_fetch_assoc($rs)){
				$fetch['query']=$row;
			}
			$query = "Kill $id";
			$res= mysql_query($query);
			if($res === False){
				$fetch['msg']="Please try again.....";
			}else{
				$fetch['msg']="Mysql process id: $id has been killed";
			}
			return json_encode($fetch);
		}else{
			$fetch['msg']="You do not have permission to kill this query";
			return json_encode($fetch);
		}
	}
	
	public function run($secretkey){
		if(md5($this->secretPhrase)==$secretkey){
			switch($_REQUEST['action']){
				case 'list':
					$response = $this->getProcessList();
					break;
				case 'kill':
					$response = $this->killQuery();
					break;
				default:
					$response = json_encode(array("msg"=>"Authentication failed"));
					break;
			}
		}else $response = json_encode(array("msg"=>"Authentication failed"));
		return $response;
	}
	
}

?>
