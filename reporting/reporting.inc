<?php
class REPORTING{
  	const MSG_REQUESTER_REQUIRED	=	"200: Requester not specified";
	const MSG_AUTHENTICATION_FAILD 	=	"200: Authentication failed";
	const MSG_APIKEY_REQUIRED 		=	"201: No apiKey supplied";
	const MSG_EMAIL_REQUIRED 		=	"201: No Email supplied";
	const MSG_APIKEY_NOT_EXIST 		=	"201: Apikey does not exist in your account";
	const MSG_DATE_REQUIRED 		=	"202: No date supplied";
	const MSG_ENDDATE_REQUIRED 		=	"202: End date is less than date";
	const MSG_DURATION_REQUIRED 	=	"202: Duration is greater than 31 days";
	const MSG_SUMMARY_ENDDATE_REQUIRED 			=	"203: For summary end-date is required";
}


if($config['baseurl']=="http://i2.vdopia.com/dev/pankaj_adserver" || $config['baseurl']=="http://i2.vdopia.com/dev/sandbox_adserver"){
  $reporting = array('0' => array('PUB_ID' => '214', 'token'=> '65a634754249bd59e3055a6686e8dc5a'));
  $advreporting = array('0' => array('ADV_ID' => '179', 'token'=> '1634754249bd599e3055a6686e8d7c5a'));
}else{
  $reporting = array(
                 # Nexage
                 '0' => array('PUB_ID' => '1969', 'token'=> '8d8f47f8bac808b36eb5290efbd15a59'),
                 # Test channels
                 '1' => array('PUB_ID' => '1075', 'token'=> 'f1cb1f1ed12ce5f9b74781e0521ba3c4'),
                 # AdMarvel channels : manoj@admarvel.com
                 '2' => array('PUB_ID' => '1629', 'token'=> 'f2949e6be05c7791ebe4da2307374f3c'),
                 # SGN channels
                 '3' => array('PUB_ID' => '479', 'token'=> '0607ca0abb3477a13b61c4a3bdaa1954'),
                 # Amobee channels
                 '4' => array('PUB_ID' => '2734', 'token'=> '28c2dd6a6fea46e4ce59c0c480f5d0c2'),
                 # Burstly
                 '5' => array('PUB_ID' => '1842', 'token'=> '5ede193f17b6b3e1657665fad0b25f03'),
                 # Moblyng
                 '6' => array('PUB_ID' => '2789', 'token'=> 'd5f0a799785385ea7e188df5855c3322'),
                 # Smaato
                 '7' => array('PUB_ID' => '2834', 'token'=> '02cddf81963ddb10da663c62821c6bbd'),
                 # MLS
                 '8' => array('PUB_ID' => '2762', 'token'=> '9320be5c9b01a69335a936bf29f2257c'),
                 # Kargo
                 '9' => array('PUB_ID' => '1984', 'token'=> '99e968a1029256a19503f0301673acfe'),
                 # Digital Chocolate
                 '10' => array('PUB_ID' => '3210', 'token'=> '46f94bb6bdd6aba1a01742da649c861e'),
                 # EA Games
                 '11' => array('PUB_ID' => '3446', 'token'=> 'aa4518d9615fe63c84878d615696d117'),
                 # CBS Interactive
                 '12' => array('PUB_ID' => '2433', 'token'=> 'ed45aae91149bcc256f5539d74fe3daf'),
                 # Cleervoyance
                 '13' => array('PUB_ID' => '4083', 'token'=> '439c914ae1336ae9ac74cc20824b19ed'),
                 # swagbucks.com
                 '14' => array('PUB_ID' => '3886', 'token'=> '3b8c3520d4f5b1db16af90f0301f8e18'),
                 # Demand media
                 '15' => array('PUB_ID' => '3955', 'token'=> 'bf7cde2eaebfb1807ca2f0a1db3f0715'),
                 # Trion
                 '16' => array('PUB_ID' => '2188', 'token'=> 'e16c8153206a0c2938e2be863c3ce694'),
  				 #OpenX old
                 '17' => array('PUB_ID' => '4375', 'token'=> '1065bf7d0e0bfb25e7cd6114aadc7e0e'),
                 # vdopia@openx.com  OpenX new
                 '18' => array('PUB_ID' => '5128', 'token'=> 'a0043fa984295a2e7949a191bedcb554'),
  				 # adam@admarvel.com
                 '19' => array('PUB_ID' => '1637', 'token'=> 'e560bcee967f615ae4393d0ce23d35fc'),
  				 #GDeLeon@us.univision.com
                 '20' => array('PUB_ID' => '2123', 'token'=> '3b67956508395d97ea20cb641a237745'),
  				 #cr@tapit.com
                 '21' => array('PUB_ID' => '5708', 'token'=> '8e9b023d20919ff10794576115e82cdd'),
  				 #DGrayson@lsnmobile.com
                 '22' => array('PUB_ID' => '1192', 'token'=> '1476ed3bb0e289dac8f81e728e9250d7'),
  				 #jwilson@photobucket.com
                 '23' => array('PUB_ID' => '6345', 'token'=> '03c959e20e5a06fa98baf86d22b3e2f7'),
  				 #dave@jamloop.com
                 '24' => array('PUB_ID' => '6192', 'token'=> 'bbd94f757c0f88becac159d395305596'),
  				 #dilpesh.parmar@outfit7.com
                 '25' => array('PUB_ID' => '6826', 'token'=> '40625b97dc9a0b7767609bae2ab65b97'),
			#alonr@inner-active.com
		 '26' => array('PUB_ID' => '6074', 'token'=> 'bcb96e5b974925c824121bf2cbc038c9'),
		 	//traffic@admarvel.com
		 '27' => array('PUB_ID' => '1629', 'token'=>'beee5cf5c9b81ab93b8ec7f2e09a021d'),
		 	//ty@adaptivem.com
		 '28' => array('PUB_ID' => '6974', 'token'=>'7514584c61dad0d113c079ca4ab01a77')
                 );
	$advreporting = array(
                 # pankaj@vdopia.com
                 '0' => array('ADV_ID' => '179', 'token'=> '1634754249bd599e3055a6686e8d7c5a')
                 );
}
?> 
