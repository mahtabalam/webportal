<?php
//http://i2.vdopia.com/dev/pankaj_adserver/reporting/advreporting.php?token=1634754249bd599e3055a6686e8d7c5a&email=pankaj@gmail.com&date=2011-02-01&end-date=2011-02-29&reportDuration=daily 
include_once('../include/config.php');
include_once('../adserver/servfuncs.php');
include_once('reporting.inc');
if(!isset($_SERVER['HTTPS'])){
	header('Location: https://'.$_SERVER["HTTP_HOST"].$_SERVER['REQUEST_URI']);
}
$token	=	getPost('token', REPORTING::MSG_REQUESTER_REQUIRED,1);
$date = getPost('date', REPORTING::MSG_DATE_REQUIRED,1);
$endDate = getPost('end-date', REPORTING::MSG_DATE_REQUIRED,0);
$reportDuration = getPost('reportDuration', REPORTING::MSG_DATE_REQUIRED,0);
$email = getPost('email', REPORTING::MSG_EMAIL_REQUIRED,1);
if($reportDuration=="")
	$reportDuration = "daily";
if($reportDuration=="")
	$reportDuration = "daily";
if($reportDuration=="summary" && $endDate==""){
	$responce_arr['status'] ="rejected"; 
	$responce_arr['message'] =REPORTING::MSG_SUMMARY_ENDDATE_REQUIRED; 
	$responce_arr['email'] =""; 
	echo $error = json_encode($responce_arr);
	exit;
}
if($endDate!=""){
	$noOfDays = round((strtotime($endDate) - strtotime($date))/86400);
	if($noOfDays < 0){
		$responce_arr['status'] ="rejected"; 
		$responce_arr['message'] =REPORTING::MSG_ENDDATE_REQUIRED; 
		$responce_arr['email'] =""; 
		echo $error = json_encode($responce_arr);
			exit;
	}
	if($noOfDays > 31){
		$responce_arr['status'] ="rejected"; 
		$responce_arr['message'] =REPORTING::MSG_DURATION_REQUIRED; 
		$responce_arr['email'] =""; 
		echo $error = json_encode($responce_arr);
		exit;
	}
}
// IF authentication failed, throw error
$isToken=0;
foreach($advreporting as $advertiser){
  if($advertiser['token']==strtolower(trim($token))){
	$isToken = 1;
	$adv_id = $advertiser['ADV_ID'];
	break;
  }
}
if($isToken==0){
		$response_arr['status'] ="rejected"; 
		$response_arr['message'] =REPORTING::MSG_AUTHENTICATION_FAILD; 
		$response_arr['requester'] =""; 
		$jsonResponse = json_encode($response_arr);
		echo $jsonResponse;
}		
else{
	if($email!=""){
		$sql = "select id,fname,email from advertiser where email='$email' and id='$adv_id'";
		$rs = $reportConn->execute($sql);
		// IF channels not belong to REPORTING then throw error 
		if(!$rs || $rs->recordcount()<=0){
			$response_arr['status'] ="rejected"; 
			$response_arr['message'] =REPORTING::MSG_AUTHENTICATION_FAILD; 
			$response_arr['email'] ="$email";
			$jsonResponse = json_encode($response_arr);
			echo $jsonResponse;
			exit;
		}
	}
	
	if($endDate!="")
		$datequery=" ds.date >= '$date' and ds.date <= '$endDate'";
	else
		$datequery = " ds.date = '$date'";
		
  	$sql = "select 
  					sum(impressions) as totimpressions, 
  					sum(clicks) as totclicks, 
  					round((sum(clicks)*100)/sum(impressions),2) as ctr, 
  					sum(clicks) as ec, round((sum(clicks)*100)/sum(impressions),2) as ecr , 
  					campaign.name,campaign.id ,ds.date
  			from daily_stats ds
  			left join 
  				campaign on ds.campaign_id=campaign.id 
  			left join 
  				ads on ads.id=ds.ad_id 
  			where campaign.advertiser_id = $adv_id 
  				and campaign.device='iphone' 
  				and ads.id IS NOT NULL 
  				and ads.parent_ad_id IS NULL 
  				and $datequery 
  			group by 
  				ds.campaign_id 
  			Order By name ";
    
	$rs=$reportConn->execute($sql);
	$summary = $rs->getRows();
	//=======
	 $data = array();
	/* if($reportDuration=="summary" || $reportDuration==""){
		$data = $summary;
	 }else{
	 	$data = $summary;
	 }*/
	 $data = $summary;
	//=====
	$reports = array('reports' => array());
	if(sizeof($data)>0){
		foreach ($data as $i =>$val){
			$reports['reports'][$i]['name'] = $val['name'];
			$reports['reports'][$i]['date'] = $date;
			if($reportDuration=="summary"){
				$reports['reports'][$i]['end-date'] = $endDate;
				$reports['reports'][$i]['type'] = "summary";
				$date_arr = explode("-",$endDate);
			}
			else
				$date_arr = explode("-",$date);
			
				
			$currentTimestamp = time();
			$requestedTimeStamp =mktime(0, 0, 0, $date_arr[1], $date_arr[2], $date_arr[0]); 
			if($currentTimestamp > ($requestedTimeStamp + 29*60*60)){
				$complete="true";
			}else{
				$complete="false";
			}
			$reports['reports'][$i]['complete'] = $complete;
			$reports['reports'][$i]['Impressions'] = $val['totimpressions'];
			$reports['reports'][$i]['Clicks'] = $val['totclicks'];
			$reports['reports'][$i]['CTR'] = $val['ctr'];
		}
	}else{
		$reports['reports'] ="false";
	}
	//echo "<pre>";
	//print_r($reports);
	$jsonResponse = json_encode($reports);
	echo $jsonResponse;
}
?>
