<?php
//http://i2.vdopia.com/dev/pankaj_adserver/reporting/reporting.php?token=65a634754249bd59e3055a6686e8dc5a&date=2012-02-13&end-date=2012-02-13&reportDuration=daily 
include_once('../include/config.php');
include_once('../adserver/servfuncs.php');
include_once('reporting.inc');
require_once dirname(__FILE__) .'/../common/portal/report/ReportHelper.php';
set_time_limit(900);
ini_set('memory_limit', '128M');
if(!isset($_SERVER['HTTPS'])){
	header('Location: https://'.$_SERVER["HTTP_HOST"].$_SERVER['REQUEST_URI']);
}
$token	=	getPost('token', REPORTING::MSG_REQUESTER_REQUIRED,1);
$date = getPost('date', REPORTING::MSG_DATE_REQUIRED,1);
$endDate = getPost('end-date', REPORTING::MSG_DATE_REQUIRED,0);
$reportDuration = getPost('reportDuration', REPORTING::MSG_DATE_REQUIRED,0);
$apikey = getPost('apikey', REPORTING::MSG_APIKEY_REQUIRED,0);
if($reportDuration=="")
	$reportDuration = "daily";
if($reportDuration=="summary" && $endDate==""){
	$responce_arr['status'] ="rejected"; 
	$responce_arr['message'] =REPORTING::MSG_SUMMARY_ENDDATE_REQUIRED; 
	$responce_arr['adFeedKey'] =""; 
	echo $error = json_encode($responce_arr);
	exit;
}
if($endDate!=""){
	$noOfDays = round((strtotime($endDate) - strtotime($date))/86400);
	if($noOfDays < 0){
		$responce_arr['status'] ="rejected"; 
		$responce_arr['message'] =REPORTING::MSG_ENDDATE_REQUIRED; 
		$responce_arr['adFeedKey'] =""; 
		echo $error = json_encode($responce_arr);
			exit;
	}
	if($noOfDays > 31){
		$responce_arr['status'] ="rejected"; 
		$responce_arr['message'] =REPORTING::MSG_DURATION_REQUIRED; 
		$responce_arr['adFeedKey'] =""; 
		echo $error = json_encode($responce_arr);
		exit;
	}
}
// IF authentication failed, throw error
$isToken=0;
foreach($reporting as $developer){
  if($developer['token']==strtolower(trim($token))){
	$isToken = 1;
	$pub_id = $developer['PUB_ID'];
	break;
  }
}

if($isToken==0){
		$response_arr['status'] ="rejected"; 
		$response_arr['message'] =REPORTING::MSG_AUTHENTICATION_FAILD; 
		$response_arr['requester'] =""; 
		$jsonResponse = json_encode($response_arr);
		echo $jsonResponse;
}		
else{
	
	if($apikey!=""){
		$sql = "select channels.id,name,channel_type from channels where channels.apikey='$apikey' and channels.publisher_id='$pub_id'";
		$rs = $reportConn5->execute($sql);
		// IF channels not belong to REPORTING then throw error 
		if(!$rs || $rs->recordcount()<=0){
			$response_arr['status'] ="rejected"; 
			$response_arr['message'] =REPORTING::MSG_APIKEY_NOT_EXIST; 
			$response_arr['apikey'] ="$apikey";
			$jsonResponse = json_encode($response_arr);
			echo $jsonResponse;
			exit;
		}else{
		  //  Else filtering at channel level
		  $chid = $rs->fields['id'];
		  $ctype = $rs->fields['channel_type'];
		  $chSnip=" and ds.channel_id=$chid";
		}
	}else{ 
		$sql="select type from publisher where id=$pub_id";
		$rs = $reportConn5->execute($sql);
		if($rs && $rs->recordcount()<=0){
			$ctype = $rs->fields['type'];
		}
		$chSnip=" and ch.publisher_id=$pub_id";
	}
	if(isset($config['REPORTING_SERVICE']) && $config['REPORTING_SERVICE']==true){
		if($ctype=='online' || $ctype=='PC') $db_prefix ='online_';
		else $db_prefix ='mobile_';
	}else {
		$db_prefix='';
	}
	if($endDate!=""){
		$datequery=" ds.date >= '$date' and ds.date <= '$endDate' ";
	}else
		$datequery = " ds.date = '$date'";
	
	$helper = new ReportHelper($reportConn5);
	$helper->createPriceConfigTable($date,$endDate, 'pc');
	$helper->createPriceConfigTable($date,$endDate, 'pc1',false);
    $sqlSnip="ds.pubrevenue_advcurrency*ds.ex_rate_advpub";
  	$sql = "select ds.date,
				ds.channel_id,
				sum(ds.impressions) as impressions,
				sum(ds.clicks) as clicks,
				ds.adtype,
				ds.date,
				ch.apikey,
				ch.name,
			round(sum(if(pc1.channel_id is null, if(pc.channel_id is null,pubrevenue_advcurrency*ex_rate_advpub,if(pc.model='CPM',
					  impressions*pc.price_per_unit, if(ads.parent_ad_id is null,ds.clicks,0)*pc.price_per_unit)),
					  if(pc1.model='CPM', impressions*pc1.price_per_unit, if(ads.parent_ad_id is
					  null,ds.clicks,0)*pc1.price_per_unit))),2) as revenue
			from {$db_prefix}daily_stats ds
				left join channels ch on ch.id=ds.channel_id left join campaign on ds.campaign_id=campaign.id join ads on ads.id=ds.ad_id
				 left join
						 			tmp.pc as pc on (pc.adtype = ds.adtype and (ds.date>=pc.start_date and ds.date <= pc.end_date) and ds.channel_id=pc.channel_id) 
									left join tmp.pc1 as pc1 on (ds.ccode = pc1.ccode and pc1.adtype = ds.adtype and (ds.date>=pc1.start_date and ds.date <= pc1.end_date) and ds.channel_id=pc1.channel_id)			 			
			where 
				$datequery $chSnip and
				ds.adtype is not null and ds.adtype<>'' 
			group by 
				ds.channel_id, ds.adtype,date,ds.ccode
				order by ds.channel_id,date,adtype";
	$rs=$reportConn5->execute($sql);
	$helper->dropPriceConfigTable('pc');
	$helper->dropPriceConfigTable('pc1');
	$summary = $rs->getRows();
	//=======
	$data = array();
	//$reportDuration=="summary" || $reportDuration==""
foreach($summary as $row){
	         $uniqKeys[$row['apikey']]=1;
	    }
		foreach($summary as $row){
	            $uniqAdForamts[$row['adtype']]=1;
	    }
	    
foreach($summary as $row){
	            $uniqDates[$row['date']]=1;
	        }
	        
	 if($reportDuration=="summary" || $reportDuration==""){
		
		 foreach ($uniqKeys as $Ukey =>$Kval){
				foreach($uniqAdForamts as $formatKey => $Fval){
					$temp = array();
					$valid=0;
					foreach ($summary as $row){
						if(($row['apikey']==$Ukey && $row['adtype']==$formatKey) && $row['apikey']!=""){
							$temp['channel_id']= $row['channel_id'];
							$temp['apikey']= $row['apikey'];
							$temp['adtype']= $row['adtype'];
							$temp['date']= $row['date'];
							$temp['impressions']+= $row['impressions'];
							$temp['clicks']+= $row['clicks'];
							$temp['revenue']+= $row['revenue'];
							$valid=1;
						}
					}
					if(sizeof($temp)>0){
						$data[] = $temp;
					}	
				}
			}
		
	 }else{
		 foreach ($uniqKeys as $Ukey =>$Kval){
				foreach($uniqAdForamts as $formatKey => $Fval){
					foreach($uniqDates as $dateKey => $Dval){
						$temp = array();
						$valid=0;
						foreach ($summary as $row){
							if(($row['apikey']==$Ukey && $row['adtype']==$formatKey && $row['date']==$dateKey) && $row['apikey']!=""){
								$temp['channel_id']= $row['channel_id'];
								$temp['apikey']= $row['apikey'];
								$temp['adtype']= $row['adtype'];
								$temp['date']= $row['date'];
								$temp['impressions']+= $row['impressions'];
								$temp['clicks']+= $row['clicks'];
								$temp['revenue']+= $row['revenue'];
								$valid=1;
							}
						}
						if(sizeof($temp)>0){
							$data[] = $temp;	
						}
					}
				}
			}
	 }
	//=====
	$reports = array('reports' => array());
	if(sizeof($data)>0){
		foreach ($data as $i =>$val){
			$reports['reports'][$i]['apikey'] = $val['apikey'];
			$reports['reports'][$i]['date'] = $val['date'];
			if($reportDuration=="summary"){
				$reports['reports'][$i]['end-date'] = $endDate;
				$reports['reports'][$i]['type'] = "summary";
				$date_arr = explode("-",$endDate);
			}
			else
				$date_arr = explode("-",$val['date']);
				
			$currentTimestamp = time();
			$requestedTimeStamp =mktime(0, 0, 0, $date_arr[1], $date_arr[2], $date_arr[0]); 
			if($currentTimestamp > ($requestedTimeStamp + 29*60*60)){
				$complete="true";
			}else{
				$complete="false";
			}
			$reports['reports'][$i]['adtype'] = $val['adtype'];
			$reports['reports'][$i]['complete'] = $complete;
			$reports['reports'][$i]['impressions'] = $val['impressions'];
			$reports['reports'][$i]['clicks'] = $val['clicks'];
			$reports['reports'][$i]['revenue'] = $val['revenue'];
		}
	}else{
		$reports['reports'] ="false";
	}
	$jsonResponse = json_encode($reports);
	echo $jsonResponse;
}
?>
