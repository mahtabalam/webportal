<?php

include_once('include/config.php');

/*Define tolerances for each image type*/
$validsizes=array(
	"brandeduploader" => array(
		"width" => array ("min"=> 480, "max"=>480),
		"height"=> array ("min"=> 370, "max"=>370)
	),
	"trackeruploader" => array(
		"width" => array ("min"=> 0, "max"=>9999),
		"height"=> array ("min"=> 0, "max"=>9999)
	)
);


function checkFileExtension($fname) {
	global $config;

	$allowedFormats=array(
		"swf" => 'imagecreatefromswf'
	);
	
	$swfGenerate = array(
		"jpg" => 'jpeg2swf',
		"jpeg" => 'jpeg2swf',
		"gif" => 'gif2swf',
		"png" => 'png2swf',
		"swf" => 'swf2swf',
		/*"wbmp" => imagecreatefromwbmp,
		"bmp" => imagecreatefromwbmp,
		"xbm" => imagecreatefromxbm,
		"xpm" => imagecreatefromxpm*/
	);

	$p=$fname;
	$pos=strrpos($p,".");
	$ph=strtolower(substr($p,$pos+1,strlen($p)-$pos));


	if(!isset($allowedFormats[$ph]))
		throw new Exception("Invalid file extension");

	return "success";
}

$filepath=escapeshellcmd($config['tempfilepath']."/".$_REQUEST['id']);
$filename=$_REQUEST['file'];

try {
if(file_exists($filepath)){
	$result = checkFileExtension($filename);
	echo $result;
}
else
	throw new Exception( "failure:$_REQUEST[id]");
}
catch (Exception $e){
	echo $e->getMessage();
}


?>