<?php

require_once realpath(dirname(__FILE__)).'/fileFormat.php';
//ini_set('display_errors',1);
/*
 * This file create pdf file and take actions of downloading it to browser, saving it to server and emailing it
 */
class Excel extends xls {

	
	function __construct($authorInfo) {
		
		parent::__construct($authorInfo);

	}
	

/*
 * this function will download the created file with given name in browser of user
 */
	private function download($filename) {
	
		header('Content-Type: application/vnd.ms-excel');
	    	header('Content-Disposition: attachment; filename='.$filename.'.xlsx');
		echo $this->getContents();
			
}

/*
 * This function will process given input and create excel and take required action as suggested
 */

	public function makeExcel($content,$header='',$action='Download',$name='example') {
		
		$name=preg_replace('/[^aA-zZ0-9\_\-]/', '', $name);
		
		$this->makeFile($content,$header);
		$this->createWriter('Excel2007');
		
		switch(strtolower($action)) {
			case 'download':
				$this->download($name);
				break;	
			case 'email':
			case 'mail':
				$this->sendAsEmail($name,'excel');
				break;

		}

	}
}

$arr=Array
(
		0 => Array
		(
				'buyable' => 2,
				'nodeID' => 344,
				'nodeName' => 'ROOT',
				'parentID' => 344
		),
		1 => Array
		(
				'buyable' => 1,
				'nodeID' => 3,
				'nodeName' => 'In-Market',
				'parentID' => 344
		)
	);
$headers=array(
	"CPM-Report"=>"123",
	"NAME"=>"Krishna Balram"

);
$obj=new Excel();
$obj->makeExcel($arr,$headers,'download','fff');
die;


