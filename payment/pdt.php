<?php

include_once('../include/config.php');
include_once('../common/include/function.php');


$filename = "$config[CONF_DIR]/pdt.log";

$logger=new KLogger("PDT Log", $filename, KLogger::DEBUG, $config['statusmailto'], $config['support_headers']);

if($_REQUEST['action_promotion']=="true") {
	global $conn;
	session_start();
	$promcode = mysql_escape_string($_REQUEST['promcode']);
	$sql = "select id,value,no_use_remaining as rem,startdate,enddate from promotion where promotion_code='$promcode' and datediff(startdate,curdate())<0 and ( datediff(enddate,curdate())>0 OR isnull(enddate)) ";
	$rs = $conn->execute($sql);
	if($rs && $rs->recordCount()>0 && $rs->fields['rem']!=0)
	{
		//$sql="update set no_use_remaining=no_use_remaining-1";
		//$rs1 = $conn->execute($sql);
		//Using only value not checking for type no of users and other fields
		$promid = $rs->fields['id'];
		$value = $rs->fields('value');
		$startdate=$rs->fields['startdate'];
		$enddate=$rs->fields['enddate'];
		$advid= $_SESSION['ADV_ID'];
		$rem = $rs->fields['rem'];
		$trans_id=mysql_escape_string($_REQUEST['trans_id']);
		$sql = "select count(*) as count from rx_transactions where parent_trans_id=(select paypal_transaction_id from rx_transactions where transaction_id=$trans_id) and promotion_id='$promid'";
		$rs=$conn->execute($sql);
		////Revisit Promotion portion....not verified
		if($rs && $rs->recordCount()>0 && $rs->fields['count']==0)
		{
			$randompaypalid="pro".generatePass(17);
			//$sql = "select amount,paypal_trans_id,currency,payment_type,last_name,residence_country, payer_email, first_name, adv_id,payer_id  from rx_transactions where transaction_id = $trans_id";
			$sql = "insert into rx_transactions (parent_trans_id, payment_time,fee,status,amount,currency, promotion_id ,payment_type,last_name, residence_country, payer_email,first_name, adv_id,payer_id,paypal_transaction_id  )  (select paypal_transaction_id, now(),0, 'promotion',$value*amount, currency, $promid,payment_type,last_name,residence_country, payer_email, first_name, adv_id,payer_id,'". $randompaypalid ." ' from rx_transactions where transaction_id = $trans_id and datediff(payment_time, $startdate)>0 and adv_id=$advid)"  ;
			$rs = $conn->execute($sql);
			if(mysql_insert_id()>0)
			{
				$lastid=mysql_insert_id();
				$sql = "update adv_credit set credit_amt=credit_amt+(select amount-tax from rx_transactions where transaction_id=$lastid), last_transaction_id=$lastid";
				$rs=$conn->execute($sql);
				if(mysql_affected_rows()>0)
				{
					$msg= "Promotion Code successfully applied";
					if($rem>0)
					{
						$sql = "update promotion set no_use_remaining=no_use_remaining-1 where id=$promid";
						$rs=$conn->execute($sql);
					}
				}
				else
				$msg= "Failed to apply promotion";
			}
			else
			$msg= "Failed to apply promotion";
		}
		else
		$msg= "Promotion Already applied";
	}
	else
	{
		$msg= "Incorrect Promotion Code or Promotion expired";
	}
	$msg=urlencode($msg);
	$url="$config[baseurl]/adv.php?msg=$msg";
	header("location:$url");
} else {
	// read the post from PayPal system and add 'cmd'
	$req = 'cmd=_notify-synch';
	$tx_token = $_GET['tx'];
	$auth_token = $config['paypalauth'];
	$req .= "&tx=$tx_token&at=$auth_token";

	// post back to PayPal system to validate
	$header .= "POST /cgi-bin/webscr HTTP/1.0\r\n";
	$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
	$header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
	//$fp = fsockopen ($config['paypalhost'], 80, $errno, $errstr, 30);
	// If possible, securely post back to paypal using HTTPS
	// Your PHP server will need to be SSL enabled
	$fp = fsockopen ("ssl://$config[paypalhost]", 443, $errno, $errstr, 30);

	if (!$fp) {
		// HTTP ERROR
	} else {
		fputs ($fp, $header . $req);
		// read the body data
		$res = '';
		$headerdone = false;
		while (!feof($fp)) {
			$line = fgets ($fp, 1024);
			if (strcmp($line, "\r\n") == 0) {
				// read the header
				$headerdone = true;
			}
			else if ($headerdone)
			{
				// header has been read. now read the contents
				$res .= $line;
			}
		}

		// parse the data
		$lines = explode("\n", $res);
		$keyarray = array();
		if (strcmp ($lines[0], "SUCCESS") == 0) {
			for ($i=1; $i<count($lines);$i++){
				list($key,$val) = explode("=", $lines[$i]);
				$keyarray[urldecode($key)] = urldecode($val);
			}
			if( ($keyarray['payment_status']=='Completed' || $keyarray['payment_status']=='Pending') && $keyarray['receiver_email']==$config['paypalemail'])
			{
				$firstname = $keyarray['first_name'];
				$lastname = $keyarray['last_name'];
				$itemname = $keyarray['item_name'];
				$amount = $keyarray['mc_gross'];
				$actualCredit = $keyarray['mc_gross'] - $keyarray['tax'];
				if(!isset($keyarray['mc_fee']))
				$keyarray['mc_fee']=0.0;

				global $conn;
				$time = date('Y-m-d H:i:s ',strtotime("$keyarray[payment_date]"));
				$sql = "insert into rx_transactions
				 (paypal_transaction_id,payment_time,amount,currency,payment_type,last_name,
				  residence_country, tax, payer_email, first_name, adv_id, fee, status, payer_id,parent_trans_id,pending_reason,reason_code)
				 values ('$keyarray[txn_id]',   '$time'  ,$keyarray[mc_gross],'$keyarray[mc_currency]','$keyarray[payment_type]',
				 '$keyarray[last_name]','$keyarray[residence_country]','$keyarray[tax]','$keyarray[payer_email]','$keyarray[first_name]',$keyarray[item_number],$keyarray[mc_fee],'$keyarray[payment_status]','$keyarray[payer_id]','$keyarray[parent_txn_id]','$keyarray[pending_reason]','$keyarray[reason_code]')"; 

				$rs = $conn->execute($sql);
				if($conn->Insert_ID()>0 && $keyarray['payment_status']=='Completed')
				{
					$lastid = $conn->Insert_ID();
					$sql="insert into adv_credit (adv_id, credit_amt,last_transaction_id) values ('$keyarray[item_number]',$actualCredit,$lastid) on duplicate key update credit_amt=credit_amt +$actualCredit,last_transaction_id=$lastid";
					//$sql=mysql_escape_string($sql);
					$rs = $conn->execute($sql);
					if($conn->Affected_Rows()>0)
					$logger->LogInfo(" Updating credit for ".$keyarray['item_number']." by $actualCredit");
					else{
						$logger->LogError( "Error updating credit for ".$keyarray['item_number']." query=$sql");
						$fail=1;
					}
				}
				$sql = "select transaction_id from rx_transactions where paypal_transaction_id = '$keyarray[txn_id]' ";
				$rs = $conn->execute($sql);
				if($rs && $rs->recordCount()>0)
				{
					$trans_id = $rs->fields['transaction_id'];
					$msg="<p><h3>Thank you for your purchase!</h3></p>";
					$msg.="<b>Payment Details</b><br>";$msg.="<ul><li>Name: $firstname $lastname</li>";
					$msg.="<li>Amount: $amount</li></ul><br>";
					if($keyarray['payment_status']=='Pending')
					$msg.="Your payment is still being processed by Paypal.<br>Your credits would be updated on receipt of the payment";
				}
				elseif(!$rs || $rs->recordCount()<=0 || $fail==1)
				{
					$msg="<p><h3>Thank you for your purchase!</h3></p>";
					$msg.="There has been an error processing your payment.<br>";
					$msg.="Please contact $config[support_email] with the payment time and your email address";
				}
			}
			$msg.="Your transaction has been completed<br> You may log into your account at <a href='https://www.paypal.com'>www.paypal.com</a> to view details of this transaction.<br>";
			//$msg.="<br><br> <script type='text/javascript' > function show </script>
			$msg.="<a href='#' onclick='$(\"#promform\").show()'>Click here</a> to apply a promotion code to your transaction<br>";
			$msg.="<form id='promform' method='post' action='$baseurl/payment/pdt.php' style='display:none'>  <input id='promcode' name='promcode' />
	<input id='trans_id' name='trans_id' value='$trans_id' type='hidden' />
<input name='action_promotion' type='hidden' value='true'/>
			<input class='submit' type='submit' value='Submit'/>
</form>";
		}
		else if (strcmp ($lines[0], "FAIL") == 0) {
			$msg.="Payment Failed";
		}

		$logger->LogInfo( print_r($_REQUEST,true)."\n".$res);
	}

	$baseurl=$config['baseurl'];
	$msg=urlencode($msg);
	//$msg="Your payment is being processed by Paypal. Please check this page in an hour to check the status of your payment";
	$url="$config[baseurl]/adv.php?page=credits&sub=creditsummary&msg=$msg";
	header("location:$url");
	//doForward(urlencode($url));
}
?>



