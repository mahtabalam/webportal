<?php
require_once('include/config.php');
require_once('include/csv.php');

if(strtolower($_SERVER['HTTP_HOST'])=="i2.vdopia.com"){
	# Lead tracker
	$prerollURL = "http://i2.vdopia.com/dev/pankaj_adserver/adserver/tracker.php?m=cl&ci=982&ai=1202&chid=21&ou=rd&rand=".rand(9, 999999);
	$overlayURL = "http://i2.vdopia.com/dev/pankaj_adserver/adserver/tracker.php?m=cl&ci=982&ai=1203&chid=21&ou=rd&rand=".rand(9, 999999);
	
	# Without lead
	$wtURL = "http://i2.vdopia.com/dev/pankaj_adserver/adserver/tracker.php?m=cl&ci=982&ai=1203&chid=21&ou=rd&rand=".rand(9, 999999);
}else{
	# Lead tracker
	$prerollURL = "http://serve.vdopia.com/adserver/trk/cl/1358/2499/21/vdopia/~1268139335~[insertclicktag]~";
	$overlayURL = "http://serve.vdopia.com/adserver/trk/cl/1358/2499/21/vdopia/~1268139335~[insertclicktag]~";
	
	# Without lead
	$wtURL = "http://serve.vdopia.com/adserver/trk/cl/1358/2500/21/vdopia/~1268139335~[insertclicktag]~";
}

$csv = new CsvManager(array('Initial','First Name','Last Name','E-mail','Address','Address 2','Zip Code','City','State','Company','Phone','Site'), $config['CSV_DIR']);
if($_REQUEST[actionSubmit]=="true"){
	if($_REQUEST['vt']!=""){
		if($_REQUEST['wt']=="false"){
			$initial=$_REQUEST['initial'];
			$firstname=$_REQUEST['firstname'];
			$lastname=$_REQUEST['lastname'];
			$address=$_REQUEST['address1'];
			$address2=$_REQUEST['address2'];
			$zipcode=$_REQUEST['zipcode'];
			$city=$_REQUEST['city'];
			$state=$_REQUEST['state'];
			$company=$_REQUEST['company'];
			$email=$_REQUEST['email'];
			$phone=$_REQUEST['phone'];
			$site=$_REQUEST['ps'];
			
			$csv->writecsv(array("$initial","$firstname","$lastname","$email","$address","$address2","$zipcode","$city","$state","$company","$phone","$site"));
			if(strtolower($_REQUEST['vt'])=='pre'){
				echo "url=".urlencode($prerollURL);
			}elseif (strtolower($_REQUEST['vt'])=='ovr'){
				echo "url=".urlencode($overlayURL);
			}
		}else{
			echo "url=".urlencode($wtURL);
		}
	}
}

?>