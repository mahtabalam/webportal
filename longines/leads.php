<?php
require_once('include/config.php');
require_once('include/csv.php');

$csv = new CsvManager(array('Initial','First Name','Last Name','E-mail','Address','Address 2','Zip Code','City','State','Company','Phone', 'Site'), $config['CSV_DIR']);

if($_REQUEST['action_download']){
	$csv->download("longineLeads.csv");
	
	$file = $config['CSV_DIR']."longineLeads.csv";  
	$content = fopen ($hn, $file);  
	header("Expires: 0");  
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");  
	header("Cache-Control: no-store, no-cache, must-revalidate");  
	header("Cache-Control: post-check=0, pre-check=0", false);  
	header("Pragma: no-cache");  
	header("Content-type: application/octet-stream");  
	
	// tell file size  
	header('Content-length: '.filesize($file));  
	
	// set file name  
	header('Content-disposition: attachment; filename='.basename($file));  
	readfile($file);  

	@unlink($config['CSV_DIR']."longineLeads.csv");
	// Exit script. So that no useless data is output-ed.  
	exit;
}
	

$data = $csv->readcsv();




?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Longines: Fill the form to get free Longines Brochure</title>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color:#021229;
}
.topBG {
background-image:url(images/topbg.jpg);
background-repeat:repeat-x;
}
.centerBG {
background-color:#021229;
background-image:url(images/centerbg.jpg);
background-repeat:no-repeat;
height:auto;
}
.bottomBG {
background-color:#021229;
}
.headerText {
	font-family:Arial;
	font-size:10;
	font-style:normal;
	font-weight:bold;
	color:#FFFFFF;
}

.normalText {
	font-family:Arial;
	font-size:12px;
	font-style:normal;
	font-weight:normal;
	color:#FFFFFF;
}

.errorText {
	font-family:Arial;
	font-size:10;
	font-style:normal;
	font-weight:bold;
	color:#FF0000;
	padding-left:100px;
}
.bottomBG1 {
background-color:#1d1d1d;
}
.style1 {background-color: #1d1d1d; color: #FFFFFF; }
.footerText {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-style: normal;
	font-weight: normal;
	color: #FFFFFF;
	padding-left: 50px;
	word-spacing: 0px;
	background-color: #1d1d1d;
}


-->
</style>

</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="topBG"><img src="images/rai-primaluna-ar5-pl1_01.jpg" border="0" /></td>
  </tr>
  <tr><td bgcolor="#FFFFFF" height="1"></td></tr>
  <tr>
    <td height="24" class="footerText" align="left"><form name="frm1" action="" method="POST"><input type="submit" name="action_download" id="button" value="Download CSV File" /></form></td>
  </tr>
  <tr>
    <td style="padding-left:50px;">
      <table width="78%" border="0" cellspacing="0" border="1" cellpadding="2">
        <tr>
          <td width="5%" height="22" class="headerText">SNO.</td>
          <td height="22" class="headerText">Initial</td>
          <td height="22" class="headerText">First Name</td>
          <td height="22" class="headerText">Last Name</td>
          <td height="22" class="headerText">E-mail</td>
          <td height="22" class="headerText">Address</td>
          <td height="22" class="headerText">Address 2</td>
          <td height="22" class="headerText">Zip Code</td>
          <td height="22" class="headerText">City</td>
          <td height="22" class="headerText">State</td>
          <td height="22" class="headerText">Company</td>
          <td height="22" class="headerText">Phone</td>
          <td height="22" class="headerText">Site</td>
        </tr>
        <?php if(sizeof($data)>0) { ?>
	        <?php foreach($data as $key => $row) { ?>
	        	<tr>
	          		<td height="22" class="normalText"><?php echo $key + 1; ?></td>
	          		<td height="22" class="normalText"><?php echo $row['Initial']; ?></td>
	          		<td height="22" class="normalText"><?php echo $row['First Name']; ?></td>
	          		<td height="22" class="normalText"><?php echo $row['Last Name']; ?></td>
	          		<td height="22" class="normalText"><?php echo $row['E-mail']; ?></td>
	          		<td height="22" class="normalText"><?php echo $row['Address']; ?></td>
	          		<td height="22" class="normalText"><?php echo $row['Address 2']; ?></td>
	          		<td height="22" class="normalText"><?php echo $row['Zip Code']; ?></td>
	          		<td height="22" class="normalText"><?php echo $row['City']; ?></td>
	          		<td height="22" class="normalText"><?php echo $row['State']; ?></td>
	          		<td height="22" class="normalText"><?php echo $row['Company']; ?></td>
	          		<td height="22" class="normalText"><?php echo $row['Phone']; ?></td>
	          		<td height="22" class="normalText"><?php echo $row['Site']; ?></td>
	        	</tr>
	        <?php } ?>
	   <?php }else{ ?>
	   		<tr>
          		<td height="15" colspan="13" class="errorText" align="center">There is no leads recorded.</td>
        	</tr>
	   <?php } ?>     
      </table>
        </form>
    </td>
  </tr>
  <tr>
    <td height="24" class="footerText" align="left"><form name="frm1" action="" method="POST"><input type="submit" name="action_download" id="button" value="Download CSV File" /></form></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
