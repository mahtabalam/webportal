<?php
ini_set("auto_detect_line_endings", 1);

class CsvManager{
	protected $colHead;
	protected $dir;
	
	public function __construct($colHead, $dir){
		$this->colHead=$colHead;
		$this->dir=$dir;
	}
	
	public function readcsv(){
		if (($handle = fopen($this->dir."leads.csv", "r")) !== FALSE) {
    		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        		$num = count($data);
        		for ($i=0; $i < $num; $i++) {
            		$colData[$this->colHead[$i]]=$data[$i];
        		}
        		$list[]=$colData;
    		}
    		fclose($handle);
		}
		return $list;
	}
	
	public function writecsv($data){
		$fp = fopen($this->dir.'leads.csv', 'a+');
    	fputcsv($fp, $data);
		fclose($fp);
	}
	
	public function download($filename){
		$data = $this->readcsv();
		$fp = fopen($this->dir.$filename, 'a+');
		fputcsv($fp, $this->colHead);
		foreach($data as $row){
			fputcsv($fp, $row);
		}
	}
}

?>