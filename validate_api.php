<?php
include_once('include/config.php');
global $conn;

header("Content-type: text/xml");
$api_key=mysql_escape_string($_REQUEST['key']);
$sql = "select 
			channels.id,
			channels.name,
			channels.verified,
			concat(fname,' ',lname) as publisher_name,
			email 
		from 
			channels, 
			publisher 
		where 
			channels.apikey='$api_key' and channels.publisher_id=publisher.id";
$rs = $conn->execute($sql);


echo "<validate version='1' xmlns='http://xspf.org/ns/0/'>\n";
echo "<title>Validate API Keys</title>\n";



if(!$rs || $rs->recordcount()<=0){
	echo "<message>API Key does not exist.</message>\n";
	echo "</validate>\n";
}	
else
{
	echo "<apikey>".$api_key."</apikey>\n";
	echo "<channel>".$rs->fields[name]."</channel>\n";
	echo "<verified>".$rs->fields[verified]."</verified>\n";
	echo "<publishername>".$rs->fields[publisher_name]."</publishername>\n";
	echo "<email>".$rs->fields[email]."</email>\n";
	echo "</validate\n>";
}
	

?>