<?php

 include_once dirname ( __FILE__ ) . "/Utils.php";
 include_once dirname ( __FILE__ ) . "/config.php";
 
$conn->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
$conn->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY,true);


class Pub_earnings{

	private $sql=array('create'=>"
create table if not exists %TABLE% ( 
pub_email varchar(128),
adv_email varchar(128),
pub_currency varchar(3) ,
pub_id  int, 
pub_company varchar(150),
channel_id bigint,
channel_name varchar(128), 
channel_type enum('iphone','PC'),  
adv_currency varchar(3),
ex_rate_advpub float, 
campaign_id bigint, 
campaign_name varchar(64) , 
model enum ('CPM','CPC','CPCV','CPL','CPA','MONTHLY'),
device enum('pc','iphone') ,
adv_revenue_pub_currency double, 
adv_revenue_adv_currency double,    
total_impressions bigint, 
total_clicks bigint,    
effective_clicks bigint,
completes bigint,
cr bigint,
pub_revenue_without_channel_price_override double, 
pub_revenue double,
primary key ( pub_id,channel_id,campaign_id), 
index portal (channel_type,device) );
"
,
       
		'insert'=>"
insert into %TABLE% 
select  
p.email as pub_email,
a.email as adv_email,
p.currency as Publisher_Currency, 
p.id as Publisher_ID, 
p.company_name as Publisher_Company,
ch.id as Channel_ID, 
ch.name as Channel_Name, 
ch.channel_type as channel_type,
advas.currency as Advertiser_Currency,    
ds.ex_rate_advpub as Exrate,
ds.campaign_id as Campaign_ID,
cpm.name as campaign_name,
cpm.model as Model, 
cpm.device as device, 
sum(revenue_advcurrency*ex_rate_advpub) as Advertiser_Revenue, 
sum(revenue_advcurrency) as Revenue, 
sum(if(ds.adtype not like 'co%', ds.impressions, 0)) as Total_Impressions, 
sum(if(ads.parent_ad_id IS NULL,ds.clicks,0)) as Total_Clicks, 
sum(ds.clicks) as Effective_Clicks,
sum(ds.completes) as completes,
round(((sum(ds.completes)/sum(if(ds.adtype not like 'co%', ds.impressions, 0)))*100),2) as cr,					
sum(pubrevenue_advcurrency*ex_rate_advpub) as pub_revenue_without_cp_override,
sum(if(pc1.channel_id is null, if(pc.channel_id is null,pubrevenue_advcurrency*ex_rate_advpub,if(pc.model='CPM',
impressions*pc.price_per_unit, if(ads.parent_ad_id is null,ds.clicks,0)*pc.price_per_unit)),
if(pc1.model='CPM', impressions*pc1.price_per_unit, if(ads.parent_ad_id is
null,ds.clicks,0)*pc1.price_per_unit)))  as Publisher_Revenue 
from %TABLE_PREFIX%daily_stats ds
join channels ch on ds.channel_id=ch.id  
join publisher p on ch.publisher_id=p.id
join campaign cpm on ds.campaign_id=cpm.id  
join ads on ads.id=ds.ad_id 
join advertiser a on a.id=cpm.advertiser_id 
join advertisersheet advas on advas.advertiser_id=cpm.advertiser_id
left join pc on (pc.adtype = ds.adtype and (ds.date>=pc.start_date and ds.date <= pc.end_date) and ds.channel_id=pc.channel_id) 
left join pc1 on (ds.ccode = pc1.ccode and pc1.adtype = ds.adtype and (ds.date>=pc1.start_date and ds.date <= pc1.end_date) and ds.channel_id=pc1.channel_id)
where  ds.date>='%START_DATE%' and ds.date<='%END_DATE%'  and a.acc_type='external' and p.acc_type='external'
group by ds.campaign_id, channel_id  
Order By ds.campaign_id, channel_id 
;
"
,
'drop'=>	"drop table if exists %TABLE%"	 );

	private $conn;
	private $table_prefix="pub_earnings";
	public function __construct(){
		global $conn,$config;
		$this->conn=$conn;
		if(isset($config['DB_PREFIX']) && $config['DB_PREFIX']!='') {
    		$this->db_prefix =$config['DB_PREFIX'];
    	}else $this->db_prefix='';
	}

	public function createPriceConfigTable($startDt, $endDt, $tableName = 'pc', $forAllCountry = true){
		$countryCodeChk = $forAllCountry ? "ccode = '0'" : "ccode != '0'";
	
		$sql = <<<SQL
                            CREATE TEMPORARY TABLE IF NOT EXISTS $tableName (PRIMARY KEY(channel_id,ccode,adtype,start_date,end_date,model)) select start_date, end_date, ccode, price_per_unit, model, channel_id, adtype from price_config inner join price_config_member on
price_config.id=price_config_member.price_config_id where $countryCodeChk and (
                           (start_date <= "$startDt" and end_date >= "$startDt")
                        or ( start_date <= "$endDt" and end_date >= "$endDt" )
                        or ( start_date >= "$startDt" and end_date <= "$endDt" )
                        or ( start_date <= "$startDt" and end_date >= "$endDt" )
	
                        )
	
	
SQL;
	
	
                $rs = $this->conn->exec($sql);
				if(!$rs){
					throw new Exception (" Unable to create Temporary pc and pc1 table ");
				}
	return $rs;
	}
	public function dropPriceConfigTable($tableName){
		$dSql="DROP TEMPORARY TABLE IF EXISTS $tableName";
		$drs = $this->conn->exec($dSql);
		if($_REQUEST['debug']=="on"){
			if($drs) echo "Catch Dropped!!!!<br />"; else echo $this->conn->ErrorMsg()."<br />";
		}
	}
	public function createTable($tableName){
		$sql=str_replace("%TABLE%",$tableName,$this->sql['create']);
		$rs=$this->conn->exec($sql);
		//$rs->fetchAll();
		//if($rs==FALSE)
		//	throw new Exception ("Error in creating $tableName table ". $this->conn->ErrorMsg());

		return;

	}

	private function dropTableIfExists($table){
		$sql=str_replace("%TABLE%",$table,$this->sql['drop']);
		$rs=$this->conn->exec($sql);
		//if($rs==FALSE)
		//	throw new Exception ("Error in dropping table $table ".$this->conn->ErrorMsg());

		return;
	}



	private function getYearMonth($date){
		return array("year"=>date("Y",strtotime($date)),"month"=>date("m",strtotime($date)));
	}

	private function getTableName($year,$month){
		return $this->table_prefix."_".$year."_".$month;
	}

	private function insertIntoTable($table,$start_date,$end_date,$db_prefix){
		$sql=str_replace("%TABLE%",$table,$this->sql['insert']);
		$sql=str_replace("%START_DATE%",$start_date,$sql);
		$sql=str_replace("%END_DATE%",$end_date,$sql);
		$sql=str_replace("%TABLE_PREFIX%",$db_prefix,$sql);
		$rs=$this->conn->exec($sql);
//		echo $sql."\n";
	//	$rs->execute();
	//	$rs->fetchAll();
		//echo $sql."\n";
		//if($rs==FALSE)
		//	throw new Exception ("Error in inserting into $table, $start_date,$end_date : ".$this->conn->ErrorMsg());

		return;
	}

	private function renameTables($new,$curr,$old){
		$sql="rename table $curr to $old, $new to $curr";
		//echo $sql."\n";
		
		$rs=$this->conn->exec($sql);
	//	if($rs==FALSE)
	//		throw new Exception ( "Error in renaming tables ".$this->conn->ErrorMsg());

	}
	
	public function checkDate($date) {
    if (date('Y-m-d', strtotime($date)) == $date) {
        return true;
    } else {
        return false;
    }
	}
	public function verify($start_date,$end_date){
		if($start_date<'2013-08-01'){
			throw new Exception (" Invalid start date. This script is valid only for months starting from August'2013  : $start_date ");
		}
		if(!$this->checkDate($start_date))
			throw new Exception (" Invalid start date : $start_date ");
		
		if(!$this->checkDate($end_date))
			throw new Exception ("Invalid end date : $end_date ");
		
		$start_day=substr($start_date,-2);
		if($start_day!="01")
			throw new Exception (" Start_date has to be the first of the month : $start_day");
		
		if(substr($start_date,0,7)!=substr($end_date,0,7))
			throw new Exception (" Start_date and End_date should be of the same month and year ");
		
		if($end_date<$start_date)
			throw new Exception (" End_date has to be greater than or equal to start_date");
		
		return true;
		
	}
	public function buildTable($start_date,$end_date){
		echo "Building Pub Earnings from $start_date to $end_date \n";
		$this->createPriceConfigTable($start_date,$end_date, 'pc');
		$this->createPriceConfigTable($start_date,$end_date, 'pc1',false);
		$start_time=time();
		$this->verify($start_date, $end_date);
		$ym=$this->getYearMonth($start_date);
		$tn=$this->getTableName($ym['year'],$ym['month']);
		$new_table=$tn."_new";
		$this->dropTableIfExists($new_table);
		$this->createTable($new_table);
		$this->insertIntoTable($new_table,$start_date,$end_date,$this->db_prefix);
		$this->dropPriceConfigTable('pc');
		$this->dropPriceConfigTable('pc1');
		$old_table=$tn."_old";
		$this->createTable($tn);
		$this->dropTableIfExists($old_table);
		$this->renameTables($new_table,$tn,$old_table);
		$this->dropTableIfExists($old_table);
		$end_time=time();
		echo "Pub Earnings complete from $start_date to $end_date. Time taken :  ".number_format(($end_time-$start_time)/60,2)." mins\n" ;
		return $tn;
	}

}
if (! empty ( $argc ) && strstr ( $argv [0], basename ( __FILE__ ) )) {
	error_reporting(-1);
	$ob=new Pub_earnings();
	$start_date=$argv[1];
	$end_date=$argv[2];
	$ob->buildTable($start_date,$end_date);
	}




?>
