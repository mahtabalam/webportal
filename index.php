<?php
session_start();
ini_set("max_input_vars", '10000');

include_once(dirname(__FILE__)."/include/config.php");
include_once(dirname(__FILE__)."/include/function.php");
include_once(dirname(__FILE__)."/common/include/validation.php");
include_once(dirname(__FILE__)."/common/portal/campaign/TargetingUI.php");
require_once(dirname(__FILE__)."/common/portal/campaign/processSRIPortal.php");
include_once(dirname(__FILE__)."/common/portal/campaign/BT_UI.php");
include_once(dirname(__FILE__)."/common/portal/campaign/RetargetingUI.php");
require_once(dirname(__FILE__)."/common/include/channelchoice.php");
//redirectSite();

/*if(($_SESSION['country_code']=="US" || $_SESSION['country_code']=="CA") && ($_SERVER['HTTP_HOST']=="www.vdopia.com" || $_SERVER['HTTP_HOST']=="vdopia.com") &&  $_SERVER['QUERY_STRING']==""){
	header("Location: http://mobile.vdopia.com/");
	exit();
}

if($_SERVER['HTTP_HOST']=="ads.vdopia.com"){
	header("Location: http://mobile.vdopia.com/");
	exit();
}*/   
if($_REQUEST['page']=="") {
	$_REQUEST['page']="index";
	$channel=requestParams('channel',true);

	if($channel!="") {
		$sql=$conn->Prepare("select id, name, tagline from channels where pretty_name=?");
		$rs=$conn->execute($sql, array($channel));
		if($rs && $rs->recordcount()>0) {
			session_register('TARGETCHANNEL');
			session_register('TARGETCHANNELNAME');
			$_SESSION['TARGETCHANNEL']=$rs->fields['id'];
			$_SESSION['TARGETCHANNELNAME']=$rs->fields['name'];
			$_REQUEST['page'] = "adv";
			$smarty->assign('channel', $channel);
			$tagline = $rs->fields['tagline'];
			$smarty->assign('tagline', $tagline);

			//doForward("$config[BASE_URL]/?page=adv");
		}
	}

	//
}


setup_tabs($smarty);
$smarty->assign('tabselected', $_REQUEST[ 'page' ]);


		
switch ($_REQUEST['page']) {
	case 'publishers':
		chk_login();
		$submenu=array('overview'=>'Overview'
		,'requirements'=>'Requirements'
		,'adgallery'=>'Ad Gallery'
		,'pubapi'=>'API');
		break;
	case 'adgallery':
		chk_login();
		if(!isset($_REQUEST['sub'])) $_REQUEST['sub']='customergallery';
		//include_once("library/credits.php");

		break;
	case 'advertisers':
		chk_login();
		$submenu = array('overview'=>'Overview'
		, 'brandadvert'=>'Brand Advertising'
		, 'adgallery'=>'Ad Gallery'
		, 'targeting'=>'Targeting');
		break;
	case 'about':
		chk_login();
		$submenu = array('overview'=>'Overview'
		, 'careers'=>'Careers');
		break;
	case 'advuserforgotpassword':
		chk_login();
		require_once(dirname(__FILE__).'/include/smartyBlockFunction.php');
		include_once(dirname(__FILE__)."/common/portal/captcha/securimage.php");
		$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);
		$captchaObj = new Securimage();
		$secret_phrase = "Gi35Kafn";
		$error = array();
	
		if(isset($_REQUEST['action_forgotpassword'])) {
			$rules = array(); 		# stores the validation rules
				
			# validation rules
			$rules[] = "required,email,<br>Please enter your email address.";
			
			$error = validateFields($_POST, $rules);
			
			if ($captchaObj->check($_REQUEST['captcha']) === false) { $captcha_msg="Please enter correct code."; $error['captcha']=""; }
			if (empty($error)){
				$email = getRequest( 'email', true );
				$sql=$conn->Prepare("select id, fname, lname,email from adv_user where email=?");
				$rs = $conn->Execute($sql,array($email));
				if($rs && $rs->recordcount()>0){
					$timestamp = time();
					$secret_key = md5($secret_phrase.$rs->fields['email'].$timestamp);
					$apikey = md5($rs->fields['id']);
					$smarty->assign("receiver_fname", $rs->fields['fname']);
					$smarty->assign("receiver_lname", $rs->fields['lname']);
					$smarty->assign("link", $config['BASE_URL']."/index.php?page=formloader&form=advuserresetpass&sec=$secret_key&rt=$timestamp&ak=$apikey");
					$subj = "Your Request";
					$email_path = $config['baseurl'];
					$body = $smarty->fetch("emails/forgotpassemail.tpl");

					mail($_REQUEST['email'],$subj,$body,$config['support_headers']);
					$msg = "Password reset link has been sent to your email address. Please check your email and reset your password.";
					doForward("$config[baseurl]/index.php?page=advuserlogin&error=$msg");
					exit("error=$msg");
				}else{
					$msg = "Sorry, we have no record for this email address";
					doForward("$config[baseurl]/index.php?page=advuserlogin&error=$msg");
					exit("error=$msg");
				}
			}
		}
		$smarty->assign("error",$error);
		$smarty->assign("captcha_msg", $captcha_msg);
		break;
	case 'forgotpass':
		chk_login();
		require_once(dirname(__FILE__).'/include/smartyBlockFunction.php');
		include_once(dirname(__FILE__)."/common/portal/captcha/securimage.php");
		$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);
		$captchaObj = new Securimage();
		$secret_phrase = "Gi35Kafn";
		$error = array();
		
		if($_REQUEST['action_forgotpass']!="" && ($_REQUEST['type']=='advertiser' || $_REQUEST['type']=='publisher') ) {
			$rules = array(); 		# stores the validation rules
			
		  	# validation rules
		  	$rules[] = "required,type,<br>This field is required.";
		  	$rules[] = "required,email,<br>Please enter your email address.";
		  	
			$error = validateFields($_POST, $rules);
  			if ($captchaObj->check($_REQUEST['captcha']) == false) { $captcha_msg="Please enter correct captcha."; $error['captcha']=""; }
  			
  			$smarty->assign("error",$error);
  			$smarty->assign("captcha_msg", $captcha_msg);
  
  			if (empty($error)){
				$tablename = $_REQUEST['type'];
				$email = getRequest( 'email', true );
				
				$sql = $conn->Prepare("select id, fname, lname, apikey, email from $tablename where email=? and type='online'");
				$rs = $conn->execute($sql, array($email));
				if($rs && $rs->recordcount()>0){
					$timestamp = time();
					$secret_key = md5($secret_phrase.$rs->fields['email'].'online'.$timestamp);
					$apikey = $rs->fields['apikey'];
					$smarty->assign("receiver_fname", $rs->fields['fname']);
					$smarty->assign("receiver_lname", $rs->fields['lname']);
					
					$smarty->assign("link", $config['BASE_URL']."/index.php?page=resetpassword&t=$tablename&sec=$secret_key&rt=$timestamp&ak=$apikey");
	
					$subj = "Your Request";
					$email_path = $config['baseurl'];
					$body = $smarty->fetch("emails/forgotpassemail.tpl");
	
					mail($_REQUEST['email'],$subj,$body,$config['support_headers']); 
					$msg = "Password reset link has been sent to your email address. Please check your email and reset your password.";
					if($tablename=="advertiser"){
						doForward("$config[baseurl]/index.php?page=advlogin&msg=$msg");
					}
					elseif($tablename=="publisher"){
						doForward("$config[baseurl]/index.php?page=publogin&msg=$msg");
	
					}
				}else{
					$msg = "Sorry, we have no record for this email address";
					$smarty->assign("msg",$msg);
				}
  			}
		}
		break;
	case 'resetpassword':
		chk_login();
		require_once('include/smartyBlockFunction.php');
		$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);
		$secret_phrase = "Gi35Kafn";
		$error = array();
		
		if(!isset($_POST['action_gi35kafn']) && $_POST['action_gi35kafn']==""){
			# check, if sec exist
			if(!isset($_REQUEST['sec']) || $_REQUEST['sec']==""){
				exit("Invalid url. Please <a href='".$config['BASE_URL']."/index.php?page=forgotpass'>ckick here</a> to get password reset link again.");
			}else $secret_key=$_REQUEST['sec'];
			
			# check, if rt exist
			if(!isset($_REQUEST['rt']) || $_REQUEST['rt']==""){
				exit("Invalid url. Please <a href='".$config['BASE_URL']."/index.php?page=forgotpass'>ckick here</a> to get password reset link again.");
			}else $request_time = $_REQUEST['rt'];
			
			# check, if ak exist
			if(!isset($_REQUEST['ak']) || $_REQUEST['ak']==""){
				exit("Invalid url. Please <a href='".$config['BASE_URL']."/index.php?page=forgotpass'>ckick here</a> to get password reset link again.");
			} else $apikey = $_REQUEST['ak'];
			
			# check, if t exist
			if(!isset($_REQUEST['t']) || $_REQUEST['t']==""){
				exit("Invalid url. Please <a href='".$config['BASE_URL']."/index.php?page=forgotpass'>ckick here</a> to get password reset link again.");
			} else $table = mysql_escape_string($_REQUEST['t']);
			
			# check expiry time
			if((round((time() - $_REQUEST['rt'])/60, 0))>=1440){
				exit("This url has been expired. This url is valid for 12 hour. Please <a href='".$config['BASE_URL']."/index.php?page=forgotpass'>ckick here</a> to get password reset link again.");
			}
			
			# check valid secret key
			$sql = $conn->Prepare("select email, type from $table where apikey=?");
			$rs = $conn->Execute($sql, array($apikey));
			if($rs && $rs->recordcount()>0){
				$_REQUEST['email'] = $rs->fields['email'];
				$_REQUEST['type1'] = $table;
				$confirm_secret_key = md5($secret_phrase.$rs->fields['email'].'online'.$request_time);
				if($secret_key!=$confirm_secret_key){
					exit("Secret key is not matching. Please <a href='".$config['BASE_URL']."/index.php?page=forgotpass'>ckick here</a> to get password reset link again.");
				}
			}else{
				exit("Sorry, we are unable to find you. Please <a href='".$config['BASE_URL']."/index.php?page=forgotpass'>ckick here</a> to get password reset link again.");
			}
		}
		
		if($_REQUEST['action_gi35kafn']!="") {
			$rules = array(); 		# stores the validation rules
			
		  	# validation rules
		    $rules[] = "required,password,<br>Please enter a password.";
		    $rules[] = "length>4,password,<br>Your password must be at least 5 characters long";
		    $rules[] = "length>4,confirm_password,<br>Your password must be at least 5 characters long";
		    $rules[] = "same_as,confirm_password,password,<br>Please ensure the passwords you enter are the same.";
		  	
			$error = validateFields($_POST, $rules);
  			$smarty->assign("error",$error);
			if(empty($error)){
				$tablename = mysql_escape_string(getRequest('t'));
				$apikey = mysql_escape_string(getRequest('ak'));
				$password = mysql_escape_string(getRequest('password', true));
				$pass = md5($password);
				$sql= $conn->Prepare("update $tablename set passwd=? where apikey=? and type='online'");
				$rs = $conn->execute($sql, array($pass, $apikey));
				if($rs){
					$msg = "Your password has been reset.";
					if($tablename=="advertiser"){
						doForward("$config[baseurl]/index.php?page=advlogin&msg=$msg"); 
					}
					elseif($tablename=="publisher"){
						doForward("$config[baseurl]/index.php?page=publogin&msg=$msg");
					}
				}else{
					$msg = "Sorry, we are not able to process your request at this time. Try again......";
					$smarty->assign("msg",$msg);
				}
			}else{
				$_REQUEST['type1']=$_REQUEST['t'];
			}
		}
		break;
	case 'formloader':
                require_once('include/smartyBlockFunction.php');
		$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);
		$smarty->assign("form", $_REQUEST['form'].".tpl");
		switch ($_REQUEST['form']) {
			case 'advuserresetpass':
				chk_login();
				$secret_phrase = "Gi35Kafn";
				$error = array();
				
				if(!isset($_POST['action_gi35kafn']) ){
					# check, if sec exist
					if(!isset($_REQUEST['sec']) /*|| $_REQUEST['sec']==""*/){
						exit("Invalid url. Please <a href='".$config['BASE_URL']."/index.php?page=advuserforgotpassword'>click here</a> to get password reset link again.");
					}else $secret_key=$_REQUEST['sec'];
			
					# check, if rt exist
					if(!isset($_REQUEST['rt']) ){
						exit("Invalid url. Please <a href='".$config['BASE_URL']."/index.php?page=advuserforgotpassword'>click here</a> to get password reset link again.");
					}else $request_time = $_REQUEST['rt'];
			
								# check, if ak exist
					if(!isset($_REQUEST['ak']) ){
						exit("Invalid url. Please <a href='".$config['BASE_URL']."/index.php?page=advuserforgotpassword'>click here</a> to get password reset link again.");
					} else $apikey = $_REQUEST['ak'];
			
								# check expiry time
					if((round((time() - $_REQUEST['rt']), 0))>=43200){ // 12*3600
						exit("This url has been expired. This url is valid for 12 hour. Please <a href='".$config['BASE_URL']."/index.php?page=advuserforgotpassword'>ckick here</a> to get password reset link again.");
					}
			
					# check valid secret key
					$sql = $conn->Prepare("select email from adv_user where md5(id)=?");
					$rs = $conn->Execute($sql, array($apikey));
					if($rs && $rs->recordcount()>0){
						$_REQUEST['email'] = $rs->fields['email'];
						$confirm_secret_key = md5($secret_phrase.$rs->fields['email'].$request_time);
						
						if($secret_key!=$confirm_secret_key){
						   exit("Secret key is not matching. Please <a href='".$config['BASE_URL']."/index.php?page=advuserforgotpassword'>click here</a> to get password reset link again.");
						}
					}else{
						exit("Sorry, we are unable to find you. Please <a href='".$config['BASE_URL']."/index.php?page=advuserforgotpassword'>click here</a> to get password reset link again.");
					}
				}
		
				if(isset($_REQUEST['action_gi35kafn'])) {
					$rules = array(); 		# stores the validation rules
					
					# validation rules
					$rules[] = "required,password,<br>Please enter a password.";
					$rules[] = "length>4,password,<br>Your password must be at least 5 characters long";
			        $rules[] = "length>4,confirm_password,<br>Your password must be at least 5 characters long";
			        $rules[] = "same_as,confirm_password,password,<br>Please ensure the passwords you enter are the same.";
			
					$error = validateFields($_POST, $rules);
				   
					if(empty($error)){
						$apikey = mysql_escape_string(getRequest('ak'));
						$password = mysql_escape_string(getRequest('password', true));
						$pass = md5($password);
					    $sql= $conn->Prepare("update adv_user set passwd='$pass' where md5(id)=?");
						$rs = $conn->execute($sql,array($apikey));
  						if($rs){
    						$msg = "Your password has been reset.";
    						doForward("$config[baseurl]/index.php?page=advuserlogin&error=$msg");
    						exit("error=$msg");
  						}else{
   							$msg = "Sorry, we are not able to process your request at this time. Try again......";
   							doForward("$config[baseurl]/index.php?page=advuserlogin&error=$msg");
   							exit("error=$msg");
  						}
					}
					$smarty->assign("error", $error);
			 	}
				break;							    			
			case "ad":
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				$smarty->assign("editFormat", $_REQUEST['editFormat']);
				if($_REQUEST['editFormat']=="editFormat"){
					$vidtype= explode(",",$_SESSION["CAMPAIGN"]['vidtype']);
					$vastfeedvidtype= explode(",",$_SESSION["CAMPAIGN"]['vastfeedvidtype']);
                                        //$ytvidtype = explode(",",$_SESSION["CAMPAIGN"]['ytvidtype']);
                                        $ytvidtype = array("preroll"); 
                                        /**
                                         * @todo preroll is default for ytpreroll change here if more types are added
                                         */
                                }else{
					$vidtype = array("preroll","midroll","postroll");
					$vastfeedvidtype = array("preroll","midroll","postroll");
                                        $ytvidtype = array("preroll");
				}
				$smarty->assign('vidtype',$vidtype);
				$smarty->assign('vastfeedvidtype',$vastfeedvidtype);
                                $smarty->assign('ytvidtype', $ytvidtype);
                                if($_REQUEST['editFormat']!="editFormat")
					unset($_SESSION['CAMPAIGN']);
				if(isset($_REQUEST['action_wizard'])) {
					$rules = array(); // stores the validation rules
					$rules[] = "required,type,Error.";
					$rules[] = "if:type=overlayadvertisement,required,subtype, Error";
					$rules[] = "if:type=videoadvertisement,required,vidtype, Error";
                                        $rules[] = "if:type=ytvideoadvertisement,required,ytvidtype, Error";
					$rules[] = "if:type=vastfeedvideoadvertisement,required,vastfeedvidtype, Error";
					$rules[] = "if:type=vastfeedvideoadvertisement,required,vasttype, Error";
					$rules[] = "if:type=trackeradvertisement,required,trktype, Error";

					/*$rules[] = "valid_url,desturl,Error";*/
					$error = validateFields($_POST, $rules);
                                        if (empty($error)) {

						$adtype=$_REQUEST['type'];
						$subtype=$_REQUEST['subtype'];
						$trktype=$_REQUEST['trktype'];
						$vidtype=join(",",$_REQUEST['vidtype']);
                                                $ytvidtype=join(",",$_REQUEST['ytvidtype']);
						$vastfeedvidtype=join(",",$_REQUEST['vastfeedvidtype']);
						$vasttype = $_REQUEST['vasttype'];

						$campaign=$_SESSION['CAMPAIGN'];
						$campaign['type'] = $adtype;
						$campaign['subtype'] = $subtype;
						$campaign['vidtype'] = $vidtype;
                                                $campaign['ytvidtype'] = $ytvidtype;
						$campaign['vastfeedvidtype'] = $vastfeedvidtype;
						$campaign['vasttype'] = $vasttype;
                                                $campaign['trktype'] = $trktype;
						$campaign['richmedia']=$_REQUEST['richmedia'];
						session_register("CAMPAIGN");
						setupCurrency();
						
						$_SESSION["CAMPAIGN"]=$campaign;
						if($_REQUEST['editFormat']=="editFormat")
							doForward("$config[baseurl]/index.php?page=formloader&form=ad");
						if($adtype=="videoadvertisement"){
							header("Location: $config[baseurl]/index.php?page=formloader&form=videoad1");
							exit();
						}elseif($adtype=="ytvideoadvertisement"){
							header("Location: $config[baseurl]/index.php?page=formloader&form=ytvideoad");
                                                        exit();
                                                }
                                                elseif($adtype=="vastfeedvideoadvertisement"){
							header("Location: $config[baseurl]/index.php?page=formloader&form=vastfeed");
							exit();
						} elseif($adtype=="overlayadvertisement"){
							if($subtype=="banneradvertisement"){
								header("Location: $config[baseurl]/index.php?page=formloader&form=overlaybannerad1");
								exit();
							}elseif($subtype=="textadvertisement"){
								header("Location: $config[baseurl]/index.php?page=formloader&form=overlaytextad1");
								exit();
							}
							elseif($subtype=="overlayswf"){
								header("Location: $config[baseurl]/index.php?page=formloader&form=overlayswf");
								exit();
							}
						} elseif($adtype=="trackeradvertisement"){
							if($trktype=="trackerswf"){
								header("Location: $config[baseurl]/index.php?page=formloader&form=trackerswf");
								exit();
							}elseif($trktype=="trackerimage"){
								header("Location: $config[baseurl]/index.php?page=formloader&form=trackerimage");
								exit();
							}if($trktype=="extswf"){
								header("Location: $config[baseurl]/index.php?page=formloader&form=extswf");
								exit();
							}elseif($trktype=="extimage"){
								header("Location: $config[baseurl]/index.php?page=formloader&form=extimage");
								exit();
							}elseif($trktype=="dblclik"){
								header("Location: $config[baseurl]/index.php?page=formloader&form=dblclik");
								exit();
							}elseif($trktype=="videobanner"){
								header("Location: $config[baseurl]/index.php?page=formloader&form=videobanner");
								exit();
							}elseif($trktype=="youtubevb"){
								header("Location: $config[baseurl]/index.php?page=formloader&form=youtubevb");
								exit();
							}

						} elseif ($adtype == "brandedplayer") {
							header("Location: $config[baseurl]/index.php?page=formloader&form=brandedplayer");
							exit();
						}

					}
				}
				break;
			case "vastfeed":
			  if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
			  if($_REQUEST['action_vastfeed']!="") {
  			    include_once(dirname(__FILE__).'/library/VastFeedAd.php');
  			    $vastAdObj = new VastFeedAds();
  			    $ads=$vastAdObj->processVastFeedAd();
  			    if($ads==0){
              	  $error="Please enter atleast one Ad to proceed";
              	  break;
                }else{
                  doForward("$config[baseurl]/index.php?page=formloader&form=target");
                }
			  }
              $smarty->assign("step", "createad");
              break;	
			case "overlaytextad1":
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				if($_REQUEST['action_textadtable']!="") {
					$textads=array();
					for($i=0;$i<count($_REQUEST['bodys']);$i++) {
						$textads[$i]['body']=$_REQUEST['bodys'][$i];
						$textads[$i]['title']=$_REQUEST['titles'][$i];
						$textads[$i]['desturl']=$_REQUEST['desturls'][$i];
						$textads[$i]['id']=$_REQUEST['ids'][$i];

					}
					$campaign=$_SESSION['CAMPAIGN'];
					$campaign['textads']=$textads;
					session_register('CAMPAIGN');
					$_SESSION['CAMPAIGN']=$campaign;
					if($i==0){
						$error="Please enter atleast one Ad to proceed";
						break;
					}
					doForward("$config[baseurl]/index.php?page=formloader&form=target");
				}

				$smarty->assign("step", "createad");
				break;
			
			case "overlaybannerad1":
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				if($_REQUEST['action_banneradtable']!="") {
					$bannerads=array();
					for($i=0;$i<count($_REQUEST['imgnames']);$i++) {
						$bannerads[$i]['imgurl']=$_REQUEST['imgurls'][$i];
						$bannerads[$i]['imgname']=$_REQUEST['imgnames'][$i];
						$bannerads[$i]['desturl']=$_REQUEST['desturls'][$i];
						$bannerads[$i]['trackurl']=$_REQUEST['trackurls'][$i];
						$bannerads[$i]['id']=$_REQUEST['ids'][$i];

					}

					$campaign=$_SESSION['CAMPAIGN'];
					$campaign['bannerads']=$bannerads;
					session_register('CAMPAIGN');
					$_SESSION['CAMPAIGN']=$campaign;
					if($i==0){
						$error="Please enter atleast one Ad to proceed";
						break;
					}
					doForward("$config[baseurl]/index.php?page=formloader&form=target");
				}
				$smarty->assign("step", "createad");
				break;
				
			case "overlayswf":
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				if($_REQUEST['action_overlayswf']!="") {
					$overlayswfs=array();
					for($i=0;$i<count($_REQUEST['imgnames']);$i++) {
						$overlayswfs[$i]['imgurl']=$_REQUEST['imgurls'][$i];
						$overlayswfs[$i]['imgname']=$_REQUEST['imgnames'][$i];
						//$overlayswfs[$i]['imgsize']=$_REQUEST['imgsizes'][$i];
						//$overlayswfs[$i]['extension']=$_REQUEST['extensions'][$i];
						$overlayswfs[$i]['desturl']=$_REQUEST['desturls'][$i];
						$overlayswfs[$i]['trackurl']=$_REQUEST['trackurls'][$i];
						$overlayswfs[$i]['id']=$_REQUEST['ids'][$i];

					}
					$campaign=$_SESSION['CAMPAIGN'];
					$campaign['overlayswfs']=$overlayswfs;
					session_register('CAMPAIGN');
					$_SESSION['CAMPAIGN']=$campaign;
					if($i==0){
						$error="Please enter atleast one Ad to proceed";
						break;
					}
					doForward("$config[baseurl]/index.php?page=formloader&form=target");
				}

				$smarty->assign("step", "createad");
				break;	
				
				
				
			case "trackerswf":
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				if($_REQUEST['action_trackerswf']!="") {
					$trackerswfs=array();
					for($i=0;$i<count($_REQUEST['imgnames']);$i++) {
						$trackerswfs[$i]['imgurl']=$_REQUEST['imgurls'][$i];
						$trackerswfs[$i]['imgname']=$_REQUEST['imgnames'][$i];
						$trackerswfs[$i]['imgsize']=$_REQUEST['imgsizes'][$i];
						$trackerswfs[$i]['extension']=$_REQUEST['extensions'][$i];
						$trackerswfs[$i]['desturl']=$_REQUEST['desturls'][$i];
						$trackerswfs[$i]['trackurl']=$_REQUEST['trackurls'][$i];
						$trackerswfs[$i]['bckimageurl']=$_REQUEST['bckimageurls'][$i];
						$trackerswfs[$i]['id']=$_REQUEST['ids'][$i];

					}

					$campaign=$_SESSION['CAMPAIGN'];
					$campaign['trackerswfs']=$trackerswfs;
					session_register('CAMPAIGN');
					$_SESSION['CAMPAIGN']=$campaign;
					if($i==0){
						$error="Please enter atleast one Ad to proceed";
						break;
					}
					doForward("$config[baseurl]/index.php?page=formloader&form=target");
				}

				$smarty->assign("step", "createad");
				break;
			case "trackerimage":
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				if($_REQUEST['action_trackerimage']!="") {
					$trackerimages=array();
					for($i=0;$i<count($_REQUEST['imgnames']);$i++) {
						$trackerimages[$i]['imgurl']=$_REQUEST['imgurls'][$i];
						$trackerimages[$i]['imgname']=$_REQUEST['imgnames'][$i];
						$trackerimages[$i]['imgsize']=$_REQUEST['imgsizes'][$i];
						$trackerimages[$i]['extension']=$_REQUEST['extensions'][$i];
						$trackerimages[$i]['desturl']=$_REQUEST['desturls'][$i];
						$trackerimages[$i]['trackurl']=$_REQUEST['trackurls'][$i];
						$trackerimages[$i]['id']=$_REQUEST['ids'][$i];

					}

					$campaign=$_SESSION['CAMPAIGN'];
					$campaign['trackerimages']=$trackerimages;
					session_register('CAMPAIGN');
					$_SESSION['CAMPAIGN']=$campaign;
					if($i==0){
						$error="Please enter atleast one Ad to proceed";
						break;
					}
					doForward("$config[baseurl]/index.php?page=formloader&form=target");
				}
				$smarty->assign("step", "createad");
				break;	
			case "extswf":
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				if($_REQUEST['action_trackerswf']!="") {
					$extswf=array();
					for($i=0;$i<count($_REQUEST['imgnames']);$i++) {
						$extswf[$i]['imgurl']=$_REQUEST['imgurls'][$i];
						$extswf[$i]['imgname']=$_REQUEST['imgnames'][$i];
						$extswf[$i]['imgsize']=$_REQUEST['imgsizes'][$i];
						$extswf[$i]['extension']=$_REQUEST['extensions'][$i];
						$extswf[$i]['desturl']=$_REQUEST['desturls'][$i];
						$extswf[$i]['trackurl']=$_REQUEST['trackurls'][$i];
						$extswf[$i]['id']=$_REQUEST['ids'][$i];

					}

					$campaign=$_SESSION['CAMPAIGN'];
					$campaign['extinfo']=$extswf;
					session_register('CAMPAIGN');
					$_SESSION['CAMPAIGN']=$campaign;
					if($i==0){
						$error="Please enter atleast one Ad to proceed";
						break;
					}
					doForward("$config[baseurl]/index.php?page=formloader&form=target");
				}
				$smarty->assign("step", "createad");
				break;	
        case "extimage":
        	if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				if($_REQUEST['action_trackerimage']!="") {
					$extimage=array();
					for($i=0;$i<count($_REQUEST['imgnames']);$i++) {
						$extimage[$i]['imgurl']=$_REQUEST['imgurls'][$i];
						$extimage[$i]['imgname']=$_REQUEST['imgnames'][$i];
						$extimage[$i]['imgsize']=$_REQUEST['imgsizes'][$i];
						$extimage[$i]['extension']=$_REQUEST['extensions'][$i];
						$extimage[$i]['desturl']=$_REQUEST['desturls'][$i];
						$extimage[$i]['trackurl']=$_REQUEST['trackurls'][$i];
						$extimage[$i]['id']=$_REQUEST['ids'][$i];
					}

					$campaign=$_SESSION['CAMPAIGN'];
					$campaign['extinfo']=$extimage;
					session_register('CAMPAIGN');
					$_SESSION['CAMPAIGN']=$campaign;
					if($i==0){
						$error="Please enter atleast one Ad to proceed";
						break;
					}
					doForward("$config[baseurl]/index.php?page=formloader&form=target");
				}
				$smarty->assign("step", "createad");
				break;
			case "videobanner":
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				if($_REQUEST['action_videobanneradtable']!="") {
					$videobannerads=array();
					for($i=0;$i<count($_REQUEST['names']);$i++) {
						$videobannerads[$i]['uri']=$_REQUEST['uris'][$i];
						$videobannerads[$i]['videobannerurl']=$_REQUEST['videobannerurls'][$i];
						$videobannerads[$i]['topbannerurl']=$_REQUEST['topbannerurls'][$i];
						$videobannerads[$i]['bottombannerurl']=$_REQUEST['bottombannerurls'][$i];
						$videobannerads[$i]['name']=$_REQUEST['names'][$i];
						$videobannerads[$i]['desturl']=$_REQUEST['desturls'][$i];
						$videobannerads[$i]['trackurl']=$_REQUEST['trackurls'][$i];
						$videobannerads[$i]['vi_0']=$_REQUEST['vi_0s'][$i];
						$videobannerads[$i]['vi_25']=$_REQUEST['vi_25s'][$i];
						$videobannerads[$i]['vi_50']=$_REQUEST['vi_50s'][$i];
						$videobannerads[$i]['vi_75']=$_REQUEST['vi_75s'][$i];
						$videobannerads[$i]['ae']=$_REQUEST['aes'][$i];
						$videobannerads[$i]['id']=$_REQUEST['ids'][$i];
						$videobannerads[$i]['isTTM']=$_REQUEST['isTTM'];
					}
					$campaign=$_SESSION['CAMPAIGN'];
					$campaign['videobannerads']=$videobannerads;
					session_register('CAMPAIGN');
					$_SESSION['CAMPAIGN']=$campaign;
					if($i==0){
						$error="Please enter atleast one Ad to proceed";
						break;
					}
					
					if($_REQUEST['isTTM']=='false'){
						doForward("$config[baseurl]/index.php?page=formloader&form=sriad&dashboardurl=".urlencode($_REQUEST['dashboardurl']));
					}elseif($_REQUEST['isTTM']=='true'){
						doForward("$config[baseurl]/index.php?page=formloader&form=sriad&dashboardurl=".urlencode($_REQUEST['dashboardurl']));
					}else{
						doForward("$config[baseurl]/index.php?page=formloader&form=sriad");
					}
				}
				$smarty->assign("step", "createad");
				break;
			case "youtubevb":
					if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
					if($_REQUEST['action_youtubevbadtable']!="") {
						$youtubevbads = array();
						//print_r($_REQUEST);exit;
						for($i=0;$i<count($_REQUEST['names']);$i++) {
							$youtubevbads[$i]['vdoID']			= $_REQUEST['vdoIDs'][$i];
							$youtubevbads[$i]['name']			= $_REQUEST['names'][$i];
							$youtubevbads[$i]['optdesturl']		= $_REQUEST['optdesturls'][$i];
							$youtubevbads[$i]['trackurl']		= $_REQUEST['trackurls'][$i];
							$youtubevbads[$i]['vi_0']			= $_REQUEST['vi_0s'][$i];
							$youtubevbads[$i]['vi_25']			= $_REQUEST['vi_25s'][$i];
							$youtubevbads[$i]['vi_50']			= $_REQUEST['vi_50s'][$i];
							$youtubevbads[$i]['vi_75']			= $_REQUEST['vi_75s'][$i];
							$youtubevbads[$i]['ae']				= $_REQUEST['aes'][$i];
							$youtubevbads[$i]['id']				= $_REQUEST['ids'][$i];
							$youtubevbads[$i]['autoPlay']		= $_REQUEST['autoPlays'][$i];
							//$youtubevbads[$i]['isTTM']			= $_REQUEST['isTTM'];
						}
						$campaign = $_SESSION['CAMPAIGN'];
						$campaign['youtubevbads'] = $youtubevbads;
						session_register('CAMPAIGN');
						$_SESSION['CAMPAIGN'] = $campaign;
						if($i==0){
							$error="Please enter atleast one Ad to proceed";
							break;
						}
						doForward("$config[baseurl]/index.php?page=formloader&form=target");
							
					}
					$smarty->assign("step", "createad");
					break;
			case "interactive":
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				if($_REQUEST['action_interactive']!=""){
					$rules = array(); // stores the validation rules
					$rules[] = "required,ttm,Error.";
					
					$error = validateFields($_POST, $rules);
					if(empty($error)){
						if($_REQUEST['ttm']=="true"){
							doForward("$config[baseurl]/index.php?page=formloader&form=sri&dashboardurl=".urlencode($_REQUEST['dashboardurl']));
						}else{
							$_REQUEST['desturl']=$_REQUEST['dashboardurl'];
							doForward("$config[baseurl]/index.php?page=formloader&form=target");
						}
					}
				}
				if($_SESSION['CAMPAIGN']['isTTM']) $_REQUEST['ttm']=$_SESSION['CAMPAIGN']['isTTM'];
				else $_REQUEST['ttm']="false";
				$smarty->assign("step", "createad");
				break;	
			case "sriad":
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				if($_REQUEST['action_sriad']!=""){
					$rules = array(); // stores the validation rules
					$rules[] = "required,ttm,Error.";
					$rules[] = "required,autoplay,Error.";
					$rules[] = "required,muted,Error.";
					$rules[] = "required,gridview,Error.";
					$rules[] = "required,mouseover,Error.";
					
					$error = validateFields($_POST, $rules);
					
					if(empty($error)){
						$_SESSION['CAMPAIGN']['isMuted']=mysql_escape_string($_REQUEST['muted']);
						$_SESSION['CAMPAIGN']['autoplay']=mysql_escape_string($_REQUEST['autoplay']);
						$_SESSION['CAMPAIGN']['gridview']=mysql_escape_string($_REQUEST['gridview']);
						$_SESSION['CAMPAIGN']['mouseover']=mysql_escape_string($_REQUEST['mouseover']);
						if($_REQUEST['ttm']=="true"){
							doForward("$config[baseurl]/index.php?page=formloader&form=sri&dashboardurl=".urlencode($_REQUEST['dashboardurl']));
						}else{
							$_REQUEST['desturl']=$_REQUEST['dashboardurl'];
							doForward("$config[baseurl]/index.php?page=formloader&form=target");
						}
					}
				}
				if($_SESSION['CAMPAIGN']['isMuted']) $_REQUEST['muted']=$_SESSION['CAMPAIGN']['isMuted'];
				else $_REQUEST['muted']="false";
				if($_SESSION['CAMPAIGN']['autoplay']) $_REQUEST['autoplay']=$_SESSION['CAMPAIGN']['autoplay'];
				else $_REQUEST['autoplay']="true";
				if($_SESSION['CAMPAIGN']['gridview']) $_REQUEST['gridview']=$_SESSION['CAMPAIGN']['gridview'];
				else $_REQUEST['gridview']="true";
				if($_SESSION['CAMPAIGN']['mouseover']) $_REQUEST['mouseover']=$_SESSION['CAMPAIGN']['mouseover'];
				else $_REQUEST['mouseover']="false";
				if($_SESSION['CAMPAIGN']['isTTM']) $_REQUEST['ttm']=$_SESSION['CAMPAIGN']['isTTM'];
				else $_REQUEST['ttm']="false";
				$smarty->assign("step", "createad");
				break;
			case "sri":
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				if($_REQUEST['action_sri']!=''){
					$rules = array(); // stores the validation rules
					//$rules[] = "required,sri_share,Error.";
					//$rules[] = "if:sri_share=sri_share,required,shareTitle, Error";
					//$rules[] = "if:sri_respond=sri_respond,required,respondTitle, Error";
					//$rules[] = "if:sri_interact=sri_interact,required,interactTitle, Error";
					
					$error = validateFields($_POST, $rules);
					$var=new SRIPortal();
					$srielements=$_SESSION ['CAMPAIGN'] ['sriads'];
				
					
					$srielements=$var->processRequest($srielements);
					
					//echo "<pre>"; print_r($srielements);
					$sriAd ['sri_share'] = $_REQUEST ['sri_share'];
					$sriAd ['sri_respond'] = $_REQUEST ['sri_respond'];
					$sriAd ['sri_interact'] = $_REQUEST ['sri_interact'];
					$sriAd ['shareTitle'] = $_REQUEST ['shareTitle'];
					$sriAd ['respondTitle'] = $_REQUEST ['respondTitle'];
					$sriAd ['interactTitle'] = $_REQUEST ['interactTitle'];
					$sriAd ['adId'] = $_REQUEST ['adId'];
					$sriAd ['backgroundbannerurl'] = $_REQUEST ['backgroundbannerurl'];
					$sriAd ['advlogourl'] = $_REQUEST ['advlogourl'];  
					
					$_SESSION ['CAMPAIGN'] ['sriads'] = $srielements;
					$_SESSION ['CAMPAIGN'] ['isTTM'] = 'true';
					$_SESSION ['CAMPAIGN'] ['backgroundbannerurl'] = $_REQUEST ['backgroundbannerurl'];
					$_SESSION ['CAMPAIGN'] ['advlogourl'] = $_REQUEST ['advlogourl'];
					$_SESSION ['CAMPAIGN'] ['shareTitle'] = $_REQUEST ['shareTitle'];
					$_SESSION ['CAMPAIGN'] ['respondTitle'] = $_REQUEST ['respondTitle'];
					$_SESSION ['CAMPAIGN'] ['interactTitle'] = $_REQUEST ['interactTitle'];
					//echo "<pre>"; print_r($_SESSION ['CAMPAIGN']); exit;
					doForward("$config[baseurl]/index.php?page=formloader&form=target");
				}
				
				/* Getting the list of SRI elements. e.g. to present at the portal */
				$manager = new SRIPortal(array(SRI::TGT_FLASH_VID_BANNER));
				$elSpecs = $manager->getElementSpecs();
				$jscript='';
				//echo "<pre>"; print_r($elSpecs);
				foreach($elSpecs as $element){
					$dat[SRI::DICT_KEY_EL_DISPLAY_NAME]=$element[SRI::DICT_KEY_EL_DISPLAY_NAME];
					$data[$element[SRI::DICT_KEY_EL_NAME]]=$dat;
				}
				
				$sriAd=$_SESSION['CAMPAIGN']['sriads'];
				//echo "<pre>"; print_r($sriAd); 
				$_REQUEST ['backgroundbannerurl'] = $_SESSION ['CAMPAIGN'] ['backgroundbannerurl'];
				$_REQUEST ['advlogourl'] = $_SESSION ['CAMPAIGN'] ['advlogourl'];
				$_REQUEST ['shareTitle'] = $_SESSION ['CAMPAIGN'] ['shareTitle'];
				$_REQUEST ['respondTitle'] = $_SESSION ['CAMPAIGN'] ['respondTitle'];
				$_REQUEST ['interactTitle'] = $_SESSION ['CAMPAIGN'] ['interactTitle'];
				$_REQUEST ['adId'] = $_SESSION ['CAMPAIGN'] ['adId'];
				
				//echo "<pre>"; print_r($_REQUEST);
				$smarty->assign("data", $data);
				$smarty->assign("fields", $fields);
				$smarty->assign("sriad", $sriad);
				$smarty->assign("error", $error);
				$smarty->assign("jscript", $jscript);
				$smarty->assign("step", "createad");
				break;		
			case "dblclik":
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				if($_REQUEST['action_dblclik']!="") {
						$strarr = preg_split("/[\s><]+/", $_REQUEST['dblcliktag']);
						$urltype = array("iframe","jscript","anchor","image");
						$urlctr=0;
						foreach($strarr as $key => $value){
              				$values=split("=", $value,2);
              				if(count($values)==1) {
               					$value=strtolower($value);
                				if($value=='iframe') {
                  					$urlt='iframe';
                				} else if($value=='script') {
                  					$urlt='jscript';
                				} else if($value=='a') {
                  					$urlt='anchor';
                				} else if($value=='img') {
                  					$urlt='image';
                				} 
              				} else if(count($values)==2) {
                				$values[1]=preg_replace(array("/^['\"]/","/['\"]$/","/abr=!ie[0-9]?;/"),array('','',''),$values[1]);
                				switch(strtolower($values[0])) {
                  					case 'src':
									case 'href':
										$urls[]=$urlt.'~'.$values[1];
									break;
									case 'width':
										$width=$values[1];
									break;
									case 'height':
										$height=$values[1];
									break;
								}
              				}
						}
           
			            if($width&&$height) {
			                $dimension="${width}x$height"; 
			            } else { 
			            	$error="Unable to parse tag";
			            } 
			            if(count($urls)!=4){ 
			            	$error="Unable to parse tag";
			            } 
            			//echo "Srikanth $dimension";
						
						$tracker_url = implode('|', $urls);
						$dblcliktag['tracker_url']=$tracker_url;
						$trackerurls = explode('|', $dblcliktag['tracker_url']);
						$trackerurl = explode('~',$trackerurls[3]);
						$dblcliktag['imgurl']=$trackerurl[1];
						$trackerurl = explode('~',$trackerurls[2]);
						$dblcliktag['ancurl']=$trackerurl[1];
						$dblcliktag['imgname']=$_REQUEST['imgname'];
						$dblcliktag['imgsize']=$dimension;
						$dblcliktag['desturl']=$_REQUEST['desturl'];
						$dblcliktag['id']='';//$_REQUEST['ids'][$i];
					
					$campaign=$_SESSION['CAMPAIGN'];
          if($campaign['dblcliktags']!=null) {
            $dblcliktags=$campaign['dblcliktags'];
          } else {
            $dblcliktags=array();
          }
					$dblcliktags[]=$dblcliktag;
					$campaign['dblcliktags']=$dblcliktags;
					
					//print_r($campaign['dblcliktags']);
					session_register('CAMPAIGN');
          			if($error=="")
					  $_SESSION['CAMPAIGN']=$campaign;
					
				} else if($_REQUEST['nextbtn']=='true'){
              doForward("$config[baseurl]/index.php?page=formloader&form=target");
              exit;
        }
				$smarty->assign("step", "createad");
			case "brandedplayer":
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				if($_REQUEST['action_brandedadtable']!="") {
					$brandedads=array();
					for($i=0;$i<count($_REQUEST['imgnames']);$i++) {
						$brandedads[$i]['imgurl']=$_REQUEST['imgurls'][$i];
						$brandedads[$i]['logo']=$_REQUEST['logos'][$i];
						$brandedads[$i]['name']=$_REQUEST['imgnames'][$i];
						$brandedads[$i]['dimension']=$_REQUEST['imgsizes'][$i];
						$brandedads[$i]['desturl']=$_REQUEST['desturls'][$i];
						$brandedads[$i]['trackurl']=$_REQUEST['trackurls'][$i];
						$brandedads[$i]['color']=$_REQUEST['colors'][$i];
						$brandedads[$i]['id']=$_REQUEST['ids'][$i];
					}
					$campaign=$_SESSION['CAMPAIGN'];
					$campaign['brandedads']=$brandedads;
					session_register('CAMPAIGN');
					$_SESSION['CAMPAIGN']=$campaign;
					if($i==0){
						$error="Please enter atleast one Ad to proceed";
						break;
					}
					doForward("$config[baseurl]/index.php?page=formloader&form=target");
				}
				$smarty->assign("step", "createad");
				break;
			case "videoad1":
                                if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				if($_REQUEST['action_videoadtable']!="") {
					$videoads=array();
					for($i=0;$i<count($_REQUEST['names']);$i++) {
						$videoads[$i]['uri']=$_REQUEST['uris'][$i];
						$videoads[$i]['name']=$_REQUEST['names'][$i];
						$videoads[$i]['desturl']=$_REQUEST['desturls'][$i];
						$videoads[$i]['trackurl']=$_REQUEST['trackurls'][$i];
						$videoads[$i]['vi_0']=$_REQUEST['vi_0s'][$i];
						$videoads[$i]['vi_25']=$_REQUEST['vi_25s'][$i];
						$videoads[$i]['vi_50']=$_REQUEST['vi_50s'][$i];
						$videoads[$i]['vi_75']=$_REQUEST['vi_75s'][$i];
						$videoads[$i]['ae']=$_REQUEST['aes'][$i];
						$videoads[$i]['duration']=$_REQUEST['durations'][$i];
						$videoads[$i]['dimension']=$_REQUEST['dimensions'][$i];
						$videoads[$i]['id']=$_REQUEST['ids'][$i];
					}
					if(sizeof($_SESSION['CAMPAIGN']['campaignBannerArr'])>0){
						$companionBanner['videoads']=$_SESSION['CAMPAIGN']['videoads'];
						$companion_type = $_SESSION['CAMPAIGN']['campaniontype'];
						$companionBanner['campaignBannerArr']=$_SESSION['CAMPAIGN']['campaignBannerArr'];
						foreach($videoads as $key => $ad){
							foreach($companionBanner['videoads'] as $comBanner){
								if($ad['id']==$comBanner['id']){
									$videoads[$key]['campaniontype']=$comBanner['campaniontype'];
									$videoads[$key]['dimession']=$comBanner['dimession'];
									$videoads[$key]['swfid']=$comBanner['swfid'];
									$videoads[$key]['imgid']=$comBanner['imgid'];
									break;
								}else{
									$videoads[$key]['campaniontype']=$companion_type;
								}
							}
						}
					}
					
					$campaign=$_SESSION['CAMPAIGN'];
					$campaign['videoads']=$videoads;
					$campaign['campaniontype']=$companion_type;
					$campaign['campaignBannerArr']=$companionBanner['campaignBannerArr'];
					session_register('CAMPAIGN');
					$_SESSION['CAMPAIGN']=$campaign;
					
					if($i==0){
						$error="Please enter atleast one Ad to proceed";
						break;
					}
					//echo "<pre>"; print_r($_SESSION['CAMPAIGN']); exit;
					if($campaign['campaniontype']=="image" && !$_REQUEST['isTTM']){
						header("Location: $config[baseurl]/index.php?page=formloader&form=imagecampanion&dashboardurl=".urlencode($_REQUEST['dashboardurl']));
						exit();
					}elseif($campaign['campaniontype']=="swf" && !$_REQUEST['isTTM']){
						header("Location: $config[baseurl]/index.php?page=formloader&form=swfcampanion&dashboardurl=".urlencode($_REQUEST['dashboardurl']));
						exit();
					}elseif($campaign['campaniontype']=="both" && !$_REQUEST['isTTM']){
						header("Location: $config[baseurl]/index.php?page=formloader&form=swfimagecampanion&dashboardurl=".urlencode($_REQUEST['dashboardurl']));
						exit();
					}else{
						if($_REQUEST['isTTM']=='false'){
							doForward("$config[baseurl]/index.php?page=formloader&form=interactive&dashboardurl=".urlencode($_REQUEST['dashboardurl']));
							exit();
						}elseif($_REQUEST['isTTM']=='true'){
							doForward("$config[baseurl]/index.php?page=formloader&form=interactive&dashboardurl=".urlencode($_REQUEST['dashboardurl']));
							exit();
						}else{
							doForward("$config[baseurl]/index.php?page=formloader&form=campanionad");
							exit();
						}
					}
				}
				$smarty->assign("step", "createad");
				break;
                        case "ytvideoad":
                                        if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
                                        if($_REQUEST['action_youtubevbadtable']!="") {
                                                $ads = array();
						for($i=0;$i<count($_REQUEST['names']);$i++) {
							$ads[$i]['vdoID']			= $_REQUEST['vdoIDs'][$i];
							$ads[$i]['name']			= $_REQUEST['names'][$i];
							$ads[$i]['optdesturl']		= $_REQUEST['optdesturls'][$i];
							$ads[$i]['trackurl']		= $_REQUEST['trackurls'][$i];
							$ads[$i]['vi_0']			= $_REQUEST['vi_0s'][$i];
							$ads[$i]['vi_25']			= $_REQUEST['vi_25s'][$i];
							$ads[$i]['vi_50']			= $_REQUEST['vi_50s'][$i];
							$ads[$i]['vi_75']			= $_REQUEST['vi_75s'][$i];
							$ads[$i]['ae']				= $_REQUEST['aes'][$i];
							$ads[$i]['id']				= $_REQUEST['ids'][$i];
							$ads[$i]['autoPlay']                    = $_REQUEST['autoPlays'][$i];
                                                        $ads[$i]['autoAfter']                    = $_REQUEST['playAfterTime'][$i];
							$ads[$i]['muted']                    = $_REQUEST['muted'][$i];
							//$youtubevbads[$i]['isTTM']			= $_REQUEST['isTTM'];
						}
                                                if(isset($_REQUEST['fetch_table'])){
                                                    $smarty->assign("campaign", array("ytvideoads"=>$ads));
                                                    $smarty->assign("readOnly", true);
                                                    $smarty->display("tables/{$_REQUEST['fetch_table']}.tpl");
                                                    die;
                                                }
                                                $campaign = $_SESSION['CAMPAIGN'];
						$campaign['ytvideoads'] = $ads;
						session_register('CAMPAIGN');
						$_SESSION['CAMPAIGN'] = $campaign;
						if($i==0){
							$error="Please enter atleast one Ad to proceed";
							break;
						}
                                                doForward("$config[baseurl]/index.php?page=formloader&form=target");
							
					}
					$smarty->assign("step", "createad");
					break;
                        //=============Campanion  banenr type start=========
				case "campanionad":
					if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
					if(isset($_REQUEST['action_campanion'])) {
						$rules = array(); // stores the validation rules
						$rules[] = "required,type,Error.";
						
						/*$rules[] = "valid_url,desturl,Error";*/
						$error = validateFields($_POST, $rules);
						
						if (empty($error)) {
							$campaniontype=$_REQUEST['type'];
							$campaign=$_SESSION['CAMPAIGN'];
							$campaign['campaniontype'] = $campaniontype;
							session_register("CAMPAIGN");
							$_SESSION['CAMPAIGN']=$campaign;
							
							if($campaniontype=="image"){
								header("Location: $config[baseurl]/index.php?page=formloader&form=imagecampanion");
								exit();
							}elseif($campaniontype=="swf"){
								header("Location: $config[baseurl]/index.php?page=formloader&form=swfcampanion");
								exit();
							}elseif($campaniontype=="both"){
								header("Location: $config[baseurl]/index.php?page=formloader&form=swfimagecampanion");
								exit();
							} elseif ($campainiontype=="none") {
                				doForward("$config[baseurl]/index.php?page=formloader&form=interactive");
              				}
							doForward("$config[baseurl]/index.php?page=formloader&form=campanionbannerad");
						}
					}
				$smarty->assign("step", "createad");
			break;
			
			case "imagecampanion":
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				if($_REQUEST['action_campanionbanner']!="") {
					$campaignBannerArr=array();
					$ads_array = $_POST['adnames'];
					$swfid_array = $_POST['imgurls'];
					$dim_array = $_POST['bannersizes'];
					$new_entry_array = $_POST['new_entry'];
					$k=0;
					for($i=0;$i<count($_SESSION['CAMPAIGN']['videoads']);$i++) {
						$dimstr ="";
						$swfidstr = "";
						$_SESSION['CAMPAIGN']['videoads'][$i]['swfid'] ="";
						$_SESSION['CAMPAIGN']['videoads'][$i]['dimession'] ="";
						for($j=0;$j<count($ads_array);$j++) {
							if($_SESSION['CAMPAIGN']['videoads'][$i]['name'] == $ads_array[$j]){
								$dimstr.=",".$dim_array[$j];
								$swfidstr.=",".$swfid_array[$j];
								$campaignBannerArr[$k]['id'] = $_SESSION['CAMPAIGN']['videoads'][$i]['id'];
								$campaignBannerArr[$k]['name'] = $_SESSION['CAMPAIGN']['videoads'][$i]['name'];
								$campaignBannerArr[$k]['swfid'] = $swfid_array[$j];
								$campaignBannerArr[$k]['dim'] = $dim_array[$j];
								$campaignBannerArr[$k]['new'] = $new_entry_array[$j];
								$dim = explode("x",$dim_array[$j]);
								$campaignBannerArr[$k]['width'] = $dim[0];
								$campaignBannerArr[$k]['height'] = $dim[1];
								$k++;
							}
						}
						$_SESSION['CAMPAIGN']['videoads'][$i]['dimession'] = substr($dimstr,1);
						$_SESSION['CAMPAIGN']['videoads'][$i]['swfid'] = substr($swfidstr,1);
						$_SESSION['CAMPAIGN']['videoads'][$i]['imgid'] = substr($swfidstr,1);
						$_SESSION['CAMPAIGN']['videoads'][$i]['campaniontype'] = "image";
						$_SESSION['CAMPAIGN']['campaniontype']="image";
					}
					$_SESSION['CAMPAIGN']['campaignBannerArr'] = $campaignBannerArr;
					if($_REQUEST['dashboardurl']!='') $_REQUEST['desturl']=$_REQUEST['dashboardurl'];
					doForward("$config[baseurl]/index.php?page=formloader&form=interactive");
				}
				for($i=0;$i<count($_SESSION['CAMPAIGN']['videoads']);$i++) {
					
					$ads_arr[] = $_SESSION['CAMPAIGN']['videoads'][$i]['name'];
				}
				$smarty->assign("adsArr", $ads_arr);
				$smarty->assign("step", "createad");
				break;	
				
			case "swfcampanion":
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				if($_REQUEST['action_campanionbanner']!="") {
					$campaignBannerArr=array();
					$ads_array = $_POST['adnames'];
					$swfid_array = $_POST['imgurls'];
					$dim_array = $_POST['bannersizes'];
					$new_entry_array = $_POST['new_entry'];
					$k=0;
					for($i=0;$i<count($_SESSION['CAMPAIGN']['videoads']);$i++) {
						$dimstr ="";
						$swfidstr = "";
						$_SESSION['CAMPAIGN']['videoads'][$i]['swfid'] ="";
						$_SESSION['CAMPAIGN']['videoads'][$i]['dimession'] ="";
						for($j=0;$j<count($ads_array);$j++) {
							if($_SESSION['CAMPAIGN']['videoads'][$i]['name'] == $ads_array[$j]){
								$dimstr.=",".$dim_array[$j];
								$swfidstr.=",".$swfid_array[$j];
								$campaignBannerArr[$k]['id'] = $_SESSION['CAMPAIGN']['videoads'][$i]['id'];
								$campaignBannerArr[$k]['name'] = $_SESSION['CAMPAIGN']['videoads'][$i]['name'];
								$campaignBannerArr[$k]['swfid'] = $swfid_array[$j];
								$campaignBannerArr[$k]['dim'] = $dim_array[$j];
								$campaignBannerArr[$k]['new'] = $new_entry_array[$j];
								$dim = explode("x",$dim_array[$j]);
								$campaignBannerArr[$k]['width'] = $dim[0];
								$campaignBannerArr[$k]['height'] = $dim[1];
								$k++;
							}
						}
						$_SESSION['CAMPAIGN']['videoads'][$i]['dimession'] = substr($dimstr,1);
						$_SESSION['CAMPAIGN']['videoads'][$i]['swfid'] = substr($swfidstr,1);
						$_SESSION['CAMPAIGN']['videoads'][$i]['campaniontype'] = "swf";
						$_SESSION['CAMPAIGN']['campaniontype']="swf";
					}
					$_SESSION['CAMPAIGN']['campaignBannerArr'] = $campaignBannerArr;
					if($_REQUEST['dashboardurl']!='') $_REQUEST['desturl']=$_REQUEST['dashboardurl'];
					doForward("$config[baseurl]/index.php?page=formloader&form=interactive");
				}
				for($i=0;$i<count($_SESSION['CAMPAIGN']['videoads']);$i++) {
					
					$ads_arr[] = $_SESSION['CAMPAIGN']['videoads'][$i]['name'];
				}
				$smarty->assign("adsArr", $ads_arr);
				$smarty->assign("step", "createad");
				break;
				
			case "swfimagecampanion":
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				if($_REQUEST['action_campanionbanner']!="") {
					$campaignBannerArr=array();
					$ads_array = $_POST['adnames'];
					$imgurls = $_POST['imgurls'];
					$dim_array = $_POST['bannersizes'];
					$new_entry_array = $_POST['new_entry'];
					$swfurls = $_POST['swfurls'];
					//echo"<pre>"; print_r($_POST); exit;
					$k=0;
					for($i=0;$i<count($_SESSION['CAMPAIGN']['videoads']);$i++) {
						$dimstr ="";
						$swfidstr = "";
						$imgidstr = "";
						$_SESSION['CAMPAIGN']['videoads'][$i]['swfid'] ="";
						$_SESSION['CAMPAIGN']['videoads'][$i]['imgid'] ="";
						$_SESSION['CAMPAIGN']['videoads'][$i]['dimession'] ="";
						for($j=0;$j<count($ads_array);$j++) {
							if($_SESSION['CAMPAIGN']['videoads'][$i]['name'] == $ads_array[$j]){
								$dimstr.=",".$dim_array[$j];
								$swfidstr.=",".$swfurls[$j];
								$imgidstr.=",".$imgurls[$j];
								$campaignBannerArr[$k]['id'] = $_SESSION['CAMPAIGN']['videoads'][$i]['id'];
								$campaignBannerArr[$k]['name'] = $_SESSION['CAMPAIGN']['videoads'][$i]['name'];
								$campaignBannerArr[$k]['swfid'] = $swfurls[$j];
								$campaignBannerArr[$k]['imgid'] = $imgurls[$j];
								$campaignBannerArr[$k]['dim'] = $dim_array[$j];
								$campaignBannerArr[$k]['new'] = $new_entry_array[$j];
								$dim = explode("x",$dim_array[$j]);
								$campaignBannerArr[$k]['width'] = $dim[0];
								$campaignBannerArr[$k]['height'] = $dim[1];
								$k++;
							}
						}
						$_SESSION['CAMPAIGN']['videoads'][$i]['dimession'] = substr($dimstr,1);
						$_SESSION['CAMPAIGN']['videoads'][$i]['swfid'] = substr($swfidstr,1);
						$_SESSION['CAMPAIGN']['videoads'][$i]['imgid'] = substr($imgidstr,1);
						$_SESSION['CAMPAIGN']['videoads'][$i]['campaniontype'] = "both";
						$_SESSION['CAMPAIGN']['campaniontype']="both";
						
					}
					//echo"<pre>"; print_r($_SESSION['CAMPAIGN']); exit;
					
					$_SESSION['CAMPAIGN']['campaignBannerArr'] = $campaignBannerArr;
					if($_REQUEST['dashboardurl']!='') $_REQUEST['desturl']=$_REQUEST['dashboardurl'];
					doForward("$config[baseurl]/index.php?page=formloader&form=interactive");
				}
				//echo"<pre>"; print_r($_SESSION['CAMPAIGN']); exit;
				for($i=0;$i<count($_SESSION['CAMPAIGN']['videoads']);$i++) {
					
					$ads_arr[] = $_SESSION['CAMPAIGN']['videoads'][$i]['name'];
				}
				$smarty->assign("adsArr", $ads_arr);
				$smarty->assign("step", "createad");
				break;			
			//==============Campanion  banenr type end =======		
			
			//================companion Banner Start=========
			case "campanionbannerad":
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				if($_REQUEST['action_campanionbanner']!="") {
					$campaignBannerArr=array();
					$ads_array = $_POST['adnames'];
					$swfid_array = $_POST['imgurls'];
					$dim_array = $_POST['bannersizes'];
					$new_entry_array = $_POST['new_entry'];
					$k=0;
					for($i=0;$i<count($_SESSION['CAMPAIGN']['videoads']);$i++) {
						$dimstr ="";
						$swfidstr = "";
						$_SESSION['CAMPAIGN']['videoads'][$i]['swfid'] ="";
						$_SESSION['CAMPAIGN']['videoads'][$i]['dimession'] ="";
						for($j=0;$j<count($ads_array);$j++) {
							if($_SESSION['CAMPAIGN']['videoads'][$i]['name'] == $ads_array[$j]){
								$dimstr.=",".$dim_array[$j];
								$swfidstr.=",".$swfid_array[$j];
								$campaignBannerArr[$k]['id'] = $_SESSION['CAMPAIGN']['videoads'][$i]['id'];
								$campaignBannerArr[$k]['name'] = $_SESSION['CAMPAIGN']['videoads'][$i]['name'];
								$campaignBannerArr[$k]['swfid'] = $swfid_array[$j];
								$campaignBannerArr[$k]['dim'] = $dim_array[$j];
								$campaignBannerArr[$k]['new'] = $new_entry_array[$j];
								$dim = explode("x",$dim_array[$j]);
								$campaignBannerArr[$k]['width'] = $dim[0];
								$campaignBannerArr[$k]['height'] = $dim[1];
								$k++;
							}
						}
						$_SESSION['CAMPAIGN']['videoads'][$i]['dimession'] = substr($dimstr,1);
						$_SESSION['CAMPAIGN']['videoads'][$i]['swfid'] = substr($swfidstr,1);
						
					}
					$_SESSION['CAMPAIGN']['campaignBannerArr'] = $campaignBannerArr;
					
					doForward("$config[baseurl]/index.php?page=formloader&form=sriad");
				}
				for($i=0;$i<count($_SESSION['CAMPAIGN']['videoads']);$i++) {
					
					$ads_arr[] = $_SESSION['CAMPAIGN']['videoads'][$i]['name'];
				}
				$smarty->assign("adsArr", $ads_arr);
				$smarty->assign("step", "createad");
				break;	
			//================companion Banner End===========	
				
				
			case "target":
				include_once(dirname(__FILE__).'/include/datefuncs.php');
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				$error = array();							
				if($_REQUEST['action_target']!="") {
					include_once('include/datefuncs.php');
					$campaign=$_SESSION['CAMPAIGN'];
			     if (isset($_REQUEST['dp_expression']) && $_REQUEST['dp_expression'] != "") {
               $campaign['dp_expression']=$_REQUEST['dp_expression'];
           }
           else if ($_REQUEST['dp_expression'] == "") {
               $campaign['dp_expression']="";
	   }

          $campaign['istargetall']=$_REQUEST['iscategoryall'];
					if($_REQUEST['iscategoryall']=="choosecategory"){
						$campaign['categories']= $_REQUEST['categoriesselected'];

						$comma="";
						$i=0;
						$categoryconcat = "";
						for($i=0; $i< count($campaign['categories']); $i++){
							if($i!=0){
								$comma = ",";
							}
						$categoryconcat =$categoryconcat.$comma.$campaign['categories'][$i];

						}
						$campaign['categoryconcat']=	$categoryconcat;

					}else {
						$campaign['categories']="";
					}
					//geotargeting
					$campaign['isgeotargetingall']=$_REQUEST['isgeotargetingall'];
					if($_REQUEST['isgeotargetingall']=="choosegeotargeting"){
						$campaign['countryconcat']	= trim($_REQUEST['country_codes'],',');
						$campaign['stateconcat']	= trim($_REQUEST['state_codes'],',');
						$campaign['postalconcat']	= trim($_REQUEST['postalcodes'],',');
					} 
					//sitetargeting
					$campaign['issitetargetingall']=$_REQUEST['issitetargetingall'];
					if($campaign['issitetargetingall']==null){
						$campaign['issitetargetingall']='sitetargetingall';
					}
					
					if($_REQUEST['issitetargetingall']=="choosesitetargeting"){
						if($_SESSION['CAMPAIGN']['site']!="") {
							sort($_SESSION['CAMPAIGN']['site']);
							$campaign['site'] = $_SESSION['CAMPAIGN']['site']; 
						}
					}else {
						$campaign['site']="";
					}
					
					//if ($_SESSION['ADMIN_ID'] != "")
						$campaign['device']=$_REQUEST['device'];
					//else
					//	$campaign['device']="pc";

					/*# error tracking
					error_log( 'Starttime :' . $campaign['starttime_gmt'] . 'endtime : ' . $campaign['endtime_gmt'] );*/
					
					session_register('CAMPAIGN');
					$_SESSION['CAMPAIGN']=$campaign;
					if($_REQUEST['iscategoryall']==null){
						$error['cat']="Please choose your site targeting";
						break;
					}
					if($_REQUEST['isgeotargetingall']==null){
						$error['geo']="Please choose your Geographical targeting";
						break;
					}
					
					
					//Advance Targeting
					$var=new TargetingUI(); $var->run($error);
					$BT_UI=new BT_UI(); $BT_UI->run();
					$RT_UI=new RetargetingUI();$RT_UI->run();
					if(empty($error)){
						doForward("$config[baseurl]/index.php?page=formloader&form=priceinfo");
					}else{
						$smarty->assign("errormsg", json_encode($error));
					}
				} 
					unset($_REQUEST['action_target']);
					$sql = $conn->Prepare("select * from category order by category_name");
					$rs=$conn->execute($sql, array());
					if($rs && $rs->recordcount()>0){
						$category= $rs->getrows();
						$smarty->assign('category',$category);
					}

				    if(isset($_SESSION[ADMIN_ID])){
    				  $sql = $conn->Prepare("select id, name from groups where portal=?");
    				  $rsg = $conn->Execute($sql, array('vdopia'));
    				  if($rsg&&$rsg->recordcount()>0){
    				    $groups = $rsg->getrows();
    					$smarty->assign("groups", $groups);
    				  }
					}
					
					if(isset($_SESSION[ADV_PUB_ID])){
						$sq1channels = "select * from channels where verified='Verified' and publisher_id='$_SESSION[ADV_PUB_ID]' and channel_type='PC' order by name";
					}else{
						$sq1channels = "select * from channels where allow_sitetargeting='1' and verified='Verified' and channel_type='PC' order by publisher_id, name";
					}
					$rschannels=$conn->execute($sq1channels);
					if($rschannels && $rschannels->recordcount()>0){

						$sites= $rschannels->getrows();
						$smarty->assign('sites',$sites);
					}

					if(isset($_SESSION['CAMPAIGN'])) {
						$campaign=$_SESSION['CAMPAIGN'];
						if(is_array($campaign['categories']) && count($campaign['categories'])>0) {
							$_REQUEST['iscategoryall']="choosecategory";
							$_REQUEST['categoriesselected']=$campaign['categories'];
						} elseif($campaign['categories']!= "" ) {
							$_REQUEST['iscategoryall']="choosecategory";
							$_REQUEST['categoriesselected']=explode(",",$campaign['categories']);
						} else {
							$_REQUEST['iscategoryall']="categoryall";
						}
						//if(is_array($campaign['countries']) && count($campaign['countries'])>0) {
						if($campaign['countryconcat']!="" || $campaign['stateconcat']!="" || $campaign['postalcodes']!="") {
							$_REQUEST['isgeotargetingall']="choosegeotargeting";
							$_REQUEST['country']=$campaign['countries'];
							$_REQUEST['countryconcat']=$campaign['countryconcat'];
							$_REQUEST['stateconcat']=$campaign['stateconcat'];
							$_REQUEST['postalcodes']=$campaign['postalcodes'];
							
							if($campaign['countryconcat']!="") $geoTarget=$campaign['countryconcat'].',';
							if($campaign['stateconcat']!="") $geoTarget.=$campaign['stateconcat'].',';
							if($campaign['postalcodes']!="") $geoTarget.=$campaign['postalcodes'].',';
							
							$smarty->assign('geoTarget', trim($geoTarget,','));
							
						} else {
							$_REQUEST['isgeotargetingall']="geotargetingall";
						}
						if(is_array($campaign['site']) && count($campaign['site'])>0) {
							$_REQUEST['issitetargetingall']="choosesitetargeting";
							$_REQUEST['site']=$campaign['site'];
							$smarty->assign('channelTarget', implode(',', $campaign['site']));
						} elseif($_SESSION['TARGETCHANNEL']!="") {
							$campaign['issitetargetingall']=$_REQUEST['issitetargetingall']="choosesitetargeting";
							$campaign['site']=$_REQUEST['site']=array($_SESSION['TARGETCHANNEL']);
							$_SESSION['CAMPAIGN']=$campaign;
						} else {
							$_REQUEST['issitetargetingall']="sitetargetingall";
						}

						if($campaign['device']!="")
						{
							$_REQUEST['device']=$campaign['device'];
						}
						else
							$_REQUEST['device']='pc';
					}
					//advance targeting
				
				$obj=new TargetingUI(); $obj->run($error);
				$BT_UI=new BT_UI(); $BT_UI->run();
				$RT_UI=new RetargetingUI();$RT_UI->run();
				
				$smarty->assign("countrycodes", getCountryList());
				break;
			case "sendtag":
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				if((is_admin_loggedin() || is_advertiser_loggedin())){
          			//Input: publisherid, campaign_id,  cc(optional), subject(optional)
					$cc=getRequest("cc");
					$subject=getRequest("subject");

          $campaign_name="";

          $cid=getRequest('campaign_id', 1);
          $pubid=getRequest('publisher_id', 1);
          $chid=getRequest('channel_id', 1);
		  $fromname=getRequest("fromname");
		  $toname=getRequest("toname");
		  $to=getRequest("to");

          //making sure the right permissions are present
          if(is_admin_loggedin()) {
            $sqlname="select name,channel_choice, track_choice, date_format(validfrom, '%D %b %y') as validfrom, date_format(validto, '%D %b %y') as validto from campaign where id='$cid'";
            $sqlads="select id, tracker_url, dimension, branded_img_right_ref_imgname as name, sri_details from campaign_members, ads where cid='$cid' and campaign_members.status='enabled' and ad_format='tracker' and campaign_members.ad_id=ads.id";
            }  else if (is_advertiser_loggedin() ) {
            $sqlname="select name,channel_choice, track_choice, date_format(validfrom, '%D %b %y') as validfrom, date_format(validto, '%D %b %y') as validto from campaign where id='$cid' and advertiser_id='$_SESSION[ADV_ID]'";
            $sqlads="select id, tracker_url, dimension, branded_img_right_ref_imgname as name, sri_details from campaign_members,ads where cid='$cid' and campaign_members.status='enabled' and ads.advertiser_id='$_SESSION[ADV_ID]' and ad_format='tracker' and campaign_members.ad_id=ads.id";
          }

          $adslist['id']=0;
          $adslist['tracker_url']=1;
          $adslist['dimension']=2;
          $adslist['name']=3;
          $adslist['sri_details']=4;
          
      


          $rs=$conn->execute($sqlname);
          if($rs&&$rs->recordcount()>0) {
            $campaign_name=$rs->fields['name'];
            $channel_choice=$rs->fields['channel_choice'];
            $tracker_choice=$rs->fields['track_choice'];
            $validfrom=$rs->fields['validfrom'];
            $validto=$rs->fields['validto'];
            if($tracker_choice=="youtubevb") 
            	$smarty->assign('youtubetrkr', $tracker_choice);
            if($tracker_choice=="videobanner") $tracker="Video Banner"; else $tracker="Rich Media Banner";
            $smarty->assign('trackertype', $tracker);
            $sqlgeo=$conn->Prepare("select * from geography where campaign_id=?");
            $rs=$conn->execute($sqlgeo, array($cid));
            if($rs && $rs->recordcount()>0)
              $smarty->assign('countries', $rs->fields['country_codes']);
          }    
          else break;
          

          $rsads=$conn->execute($sqlads);
          $ads=$rsads->getRows();
          
          $sqlto=$conn->Prepare("select concat(fname,' ',lname,'(',company_name,')') as name , email from publisher where id=?");
          $sqlfrom=$conn->Prepare("select concat(fname,' ',lname) as name, email from advertiser where id=?");
          $sqlchannel="select name,id,apikey from channels where id in ($chid) and publisher_id='$pubid'";

		  
          $rs=$conn->execute($sqlto, array($pubid));
          if($rs&&$rs->recordcount()>0) {
            if($toname=='') $toname=$rs->fields['name'];
            if($to=='') $to=$rs->fields['email'];
          }
          else break;

          $rs=$conn->execute($sqlfrom, array($_SESSION[ADV_ID]));
          if($rs&&$rs->recordcount()>0) {
            if($fromname=='') $fromname=$rs->fields['name'];
            $from=$rs->fields['email'];
          }
          else break;



          $rs=$conn->execute($sqlchannel);
          if($rs&&$rs->recordcount()>0) {
            $count=0;
            $smarty->assign("campaign_name", "$campaign_name");
            $smarty->assign("geo_info", "$geo_info");
            $smarty->assign("num_placements", count($ads));
            $smarty->assign("num_channels", $rs->recordcount());
            $smarty->assign("validfrom", $validfrom);
            $smarty->assign("validto", $validto);


            while ($rs && !$rs->EOF) {

              $channel_name=$rs->fields['name'];
              $chid=$rs->fields['id'];
              $apikey=$rs->fields['apikey'];

              $filename=str_replace(' ','_',$channel_name);
              if($filename!='') $filename.='_';
              $filename.=str_replace(' ','_',$campaign_name);

              $type_name[$count] = "text|".$filename;
              $attachments[$count] = $filename;

              $smarty->assign("channel_name", $channel_name);
              $smarty->assign("apikey", $apikey);

              $content[$count]   = $smarty->fetch("emails/attachments/campaigninfo.tpl");
              $content[$count].="\n\n======================================Placements begin====================================================\n";

              for($j=0;$j<count($ads);$j++) {
              	if($ads[$j][$adslist['dimension']]) $dimension=explode('x',$ads[$j][$adslist['dimension']]); else $dimension=explode('x',$ads[$j][$adslist['dimension']]='300x250');
                $dblcliktag['tracker_url']=$ads[$j][$adslist['tracker_url']];
                $trackurl=$ads[$j][$adslist['tracker_url']];
                $trackerurls = explode('|', $dblcliktag['tracker_url']);
                $trackerurl = explode('~',$trackerurls[3]);
                $dblcliktag['imgurl']=$trackerurl[1];
                $trackerurl = explode('~',$trackerurls[2]);
                $dblcliktag['ancurl']=$trackerurl[1];
                $output='rd';
                $anchor = $dblcliktag['ancurl'];
                
                $tag1=generateTags($cid, $ads[$j][$adslist['id']], $chid, $trackurl, $tracker_choice, $dimension, $output, $anchor, $ads[$j]['sri_details'], 'iframe');
                $tag2=generateTags($cid, $ads[$j][$adslist['id']], $chid, $trackurl, $tracker_choice, $dimension, $output, $anchor, $ads[$j]['sri_details'], 'javascript');
                $tag3=generateTags($cid, $ads[$j][$adslist['id']],  $chid, $trackurl, $tracker_choice, $dimension, $output, $anchor, $ads[$j]['sri_details'], 'fullhtml');
                $tag4=generateTags($cid, $ads[$j][$adslist['id']],  $chid, $trackurl, $tracker_choice, $dimension, $output, $anchor, $ads[$j]['sri_details'], 'jstag');

                $smarty->assign('tag1', $tag1);
                $smarty->assign('tag2', $tag2);
                $smarty->assign('tag3', $tag3);
                $smarty->assign('tag4', $tag4);
                
                $smarty->assign('name', $ads[$j][$adslist['name']]);
                $smarty->assign('dimension', $ads[$j][$adslist['dimension']]);
                $content[$count].=$smarty->fetch("emails/attachments/$tracker_choice.tpl");
              }
              if($filename=="$_REQUEST[name]") {
                  header('Content-type:text/plain');
                  echo "$content[$count]";
                  exit();
              }
              $count++;
              $rs=$rs->movenext();
            }
          }
          else 
            break;

          $comments=getRequest("comments");

          $smarty->assign("pname", $toname);
          $smarty->assign("pemail", $to);
          $smarty->assign("from", $from);
          $smarty->assign("fromname", $fromname);
          $smarty->assign("campaign_name", $campaign_name);
          $smarty->assign("trackerType", $tracker_choice);

          $smarty->assign("channel_name", "");
          $smarty->assign("apikey", "");


          $smarty->assign("comments", $comments);
          // all the following will have to be generated here !!!!!
         /*
          $smarty->assign("dimension", $_REQUEST['dimension']);
          $smarty->assign("imp_url", $_REQUEST['imp_url']);
          $smarty->assign("click_url", $_REQUEST['click_url']);
          $smarty->assign("trackerType", $_REQUEST['trackertype']);
          */
          $body = $smarty->fetch("emails/tagemail.tpl");

					# send email
          if($_REQUEST['action_send']!="") {
            if(sendMultipleAttachment($to, $fromname, "advertisers@vdopia.com", $subject, $body, $content, $type_name, $cc)){
              $smarty->assign("msg","Email has been sent.");
            }
            else{
              $smarty->assign("msg","Unable to send the message due to some internal error, please contact Vdopia administrator.");
            }
          }  else {
              $smarty->assign('body',$body);
              $smarty->assign('attachments',$attachments);
              $smarty->assign('content',$content);
          }
        }
				break;
			case 'priceinfo':
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				setupCurrency();
				if(isset($_SESSION['CURRENCYVAL'])) {
					$_REQUEST['currency']=$_SESSION['CURRENCYVAL'];
					$_POST['currency']=$_SESSION['CURRENCYVAL'];
				}
				
				$error = array();
				require_once(dirname(__FILE__).'/common/portal/campaign/Priority.php');
				require_once(dirname(__FILE__).'/common/portal/campaign/PacingType.php');
				$prObj = new Priority();
				$smarty->assign('priority', $prObj->getPriority());
				$smarty->assign('timezone', getTimeZoneList());
				$smarty->assign('hour',getTime('hour'));
				$smarty->assign('minute',getTime('minute'));
				$smarty->assign('second',getTime('second'));
				$ptObj = new PacingType();
				$smarty->assign('pacing_type', $ptObj->getPacingType());
				
				if($_REQUEST['action_priceinfo']!=""){

					$rules = array(); // stores the validation rules
					if($_SESSION['CAMPAIGN']['type']=="trackeradvertisement"){
						// standard form fields
						$rules[] = "required,model,Error.";
						$rules[] = "required,timezone,Error";
						$rules[] = "required,start_date,Error";
						$rules[] = "required,end_date,Error";
					}else{
						// standard form fields
						$rules[] = "required,currency,Currency is required.";
						$rules[] = "required,model,Pricing Model is required.";
						$rules[] = "required,maxcpm,CPM is required";
						$rules[] = "range>=0,maxcpm,CPM should be positive";
						$rules[] = "valid_float,maxcpm,CPM should be valid number";
						
						$rules[] = "required,budget,Total budget is should be valid number";
						$rules[] = "range>=0,budget,Total budget should be positive";
						$rules[] = "valid_float,budget,Total budget should be valid number";
						
						$rules[] = "required,dailylimit,Daily limit is required";
						$rules[] = "valid_float,dailylimit,Daily limit should be valid number";
						$rules[] = "range>=0,dailylimit,Daily limit should be positive";
						
						$rules[] = "required,timezone,Timezone is required";
						$rules[] = "required,end_date,End date is required";
						$rules[] = "required,start_date,Start date is required";
					}
							
					$error = validateFields($_POST, $rules);
					
					if(($_REQUEST['budget'] < $_REQUEST['dailylimit'])){
						$error['budget']="Budget should be greater than or equal to daily limit.";
					}
					# check if, start date & time greater than end date & time
					$startTimestamp = mktime($_REQUEST['st_hour'], $_REQUEST['st_minute'], $_REQUEST['st_second'], date("m", strtotime($_REQUEST['start_date'])), date("d", strtotime($_REQUEST['start_date'])), date("Y", strtotime($_REQUEST['start_date'])));
					$endTimestamp = mktime($_REQUEST['et_hour'], $_REQUEST['et_minute'], $_REQUEST['et_second'], date("m", strtotime($_REQUEST['end_date'])), date("d", strtotime($_REQUEST['end_date'])), date("Y", strtotime($_REQUEST['end_date'])));
					if($endTimestamp<$startTimestamp) $error['st_second'] = "Start date and time should be less than or equal to end date & time.";
					 
					$smarty->assign("error", $error);
					// if there were errors, re-populate the form fields
					if(empty($error)){
						$campaign=$_SESSION['CAMPAIGN'];
						$campaign['currency'] = $_REQUEST['currency'];
						$campaign['model'] = $_REQUEST['model'];
            			if($campaign['model'] =='')
              				$campaign['model']='CPM'; //Default
              			
              			if($_REQUEST['advanced_schedule']=="true"){
              				$campaign['ovrallmaximp']=$_REQUEST['ovrallmaximp'];
              				$campaign['ovralldailyimp']=$_REQUEST['ovralldailyimp'];
              				$campaign['ovrallmaxclk']=$_REQUEST['ovrallmaxclk'];
              				$campaign['ovralldailyclk']=$_REQUEST['ovralldailyclk'];
              				$campaign['ovralldailycomp']=$_REQUEST['ovralldailycomp'];
              				$campaign['ovrallmaxcomp']=$_REQUEST['ovrallmaxcomp'];
              			//	$campaign['ovralldailyleads']=$_REQUEST['ovralldailyleads'];
              			//	$campaign['ovrallmaxleads']=$_REQUEST['ovrallmaxleads'];
              				
              				
              				$campaign['maximp']=$_REQUEST['maximp'];
              				$campaign['dailyimp']=$_REQUEST['dailyimp'];
              				$campaign['maxclk']=$_REQUEST['maxclk'];
              				$campaign['dailyclk']=$_REQUEST['dailyclk'];
              				$campaign['dailycomp']=$_REQUEST['dailycomp'];
              				$campaign['maxcomp']=$_REQUEST['maxcomp'];
              				//$campaign['dailyleads']=$_REQUEST['dailyleads'];
              				//$campaign['maxleads']=$_REQUEST['maxleads'];
              				
              				
              				$campaign['cmpgnchannel']=$_REQUEST['cmpgnchannel'];
              				$campaign['cmpgnchannels']=implode(',',$campaign['cmpgnchannel']);
              			}

              			$campaign['maxcpm'] = $_REQUEST['maxcpm'];
						$campaign['dailylimit'] = $_REQUEST['dailylimit'];
						$campaign['budget'] = $_REQUEST['budget']; 
						$campaign['frequency_per_day'] = $_REQUEST['frequency_per_day'];
						$campaign['frequency_per_week'] = $_REQUEST['frequency_per_week'];
						$campaign['frequency_per_month'] = $_REQUEST['frequency_per_month'];
						$campaign['distribution'] = $_REQUEST['distribution'];
						$campaign['priority_level'] = $_REQUEST['priority_level'];
						$campaign['pacing_type'] = $_REQUEST['pacing_type'];
						$campaign['capping'] = $_REQUEST['capping'];
						$campaign['session_capping'] = $_REQUEST['session_capping'];
						$campaign['resettime'] = ($_REQUEST['hour']*60)+$_REQUEST['minute']+($_REQUEST['second']/60);
						
						if($_REQUEST['timezone']!=NULL && $_REQUEST['timezone']!=""){
							$campaign['timezone'] = $_REQUEST['timezone'];
						}else{
							$campaign['timezone']="GMT";
						}
						if($_REQUEST['st_hour']!="" && $_REQUEST['st_minute']!="" && $_REQUEST['st_second']!=""){
							$starttime=$_REQUEST['st_hour'].':'.$_REQUEST['st_minute'].':'.$_REQUEST['st_second'];
						}else{
							$starttime="00:00;00";
						}
						if($_REQUEST['et_hour']!="" && $_REQUEST['et_minute']!="" && $_REQUEST['et_second']!=""){
							$endtime=$_REQUEST['et_hour'].':'.$_REQUEST['et_minute'].':'.$_REQUEST['et_second'];
						}else{
							$endtime="23:59:59";
						}
						require_once("Date.php");
						if($_REQUEST['start_date']!=NULL && $_REQUEST['start_date']!=""){
							$campaign['start_date'] = $_REQUEST['start_date'];
							$campaign['start_time'] = $starttime;
						
							$start_date = date("Ymd", strtotime($campaign['start_date']));
							$dateObj = new Date($start_date); 
			//				$dateObj->setTZByID("GMT");		# set local time zone
							$dateObj->setTZByID($campaign['timezone']);		# set local time zone
			//				$dateObj->convertTZByID($campaign['timezone']);	# convert to foreign time zone
							$dateObj->convertTZByID("GMT");	# convert to foreign time zone
							$campaign['view_start_date']=date("M d Y H:i:s", strtotime($dateObj->format("%Y-%m-%e %T")));
							
						}else{
							$campaign['start_date']=date("M d Y");
						}
						unset($dateObj);
						if($_REQUEST['end_date']!=NULL && $_REQUEST['end_date']!=""){
							$campaign['end_date'] = $_REQUEST['end_date'];
							$campaign['end_time'] = $endtime;
							$end_date = date("YmdHis", strtotime($campaign['end_date'].' '.$campaign['end_time']));
							$dateObj = new Date($end_date);
					//		$dateObj->setTZByID("GMT");		# set local time zone
				//			$dateObj->convertTZByID($campaign['timezone']);	# convert to foreign time zone
							$dateObj->setTZByID($campaign['timezone']);		# set local time zone
							$dateObj->convertTZByID("GMT");	# convert to foreign time zone
							$campaign['view_end_date']=date("M d Y H:i:s", strtotime($dateObj->format("%Y-%m-%e %T")));
						}else{
							$campaign['end_date']=NULL;
						}
						
						if($_REQUEST['distribution']=="Daily" || $_REQUEST['distribution']=="Automatic"){
							$campaign['daily_icclimit'] = $_REQUEST['daily_icclimit'];
							$campaign['weight'] = "";
						}
						else{
							$campaign['daily_icclimit'] = "";
							$campaign['weight'] = $_REQUEST['weight'];
						}
						
						switch($campaign['model']){
							case 'CPM':
								$campaign['booked']=$_REQUEST['imp_booked'];
								break;
							case 'CPC':
								$campaign['booked']=$_REQUEST['cli_booked'];
								break;
							case 'CPL':
								$campaign['booked']=$_REQUEST['cnv_booked'];
								break;
						}
						
						if($campaign['booked']!="" && $campaign['end_date']!=""){
							if($_SESSION['ADV_PUB_ID']!=""){
								$campaign['review_status'] = 'Priority';
							}else{
								$campaign['review_status'] = 'Pending Review';
							}
						}else{
							if($_REQUEST['review_status']!=''){
								$campaign['review_status'] = $_REQUEST['review_status'];
							}else{
								$campaign['review_status'] = 'Pending Review';
							}
						}
						
						session_register("CAMPAIGN");
						$_SESSION["CAMPAIGN"]=$campaign;
						
						doForward("$config[baseurl]/index.php?page=savecampaign");
					}
				} else {
					if(isset($_SESSION['CAMPAIGN'])) {
						
						$_REQUEST=array_merge($_REQUEST,$_SESSION['CAMPAIGN']);
						if($_SESSION['CAMPAIGN']['start_time']!=''){
							$time=explode(':',$_SESSION['CAMPAIGN']['start_time']);
							$_REQUEST['st_hour']=$time[0];
							$_REQUEST['st_minute']=$time[1];
							$_REQUEST['st_second']=$time[2];
						}else{
							$_REQUEST['st_hour']="00";
							$_REQUEST['st_minute']="00";
							$_REQUEST['st_second']="00";
						}
						
						if($_SESSION['CAMPAIGN']['end_time']!=''){
							$e_time=explode(':',$_SESSION['CAMPAIGN']['end_time']);
							$_REQUEST['et_hour']=$e_time[0];
							$_REQUEST['et_minute']=$e_time[1];
							$_REQUEST['et_second']=$e_time[2];
						}else{
							$_REQUEST['et_hour']="23";
							$_REQUEST['et_minute']="59";
							$_REQUEST['et_second']="59";
						}
					}
					if(!isset($_SESSION['CAMPAIGN']['timezone'])) $_REQUEST['timezone'] = $_SESSION['ADV_TIMEZONE'];
				}
				$smarty->assign("error", $error);
			break;
				/*case "billinginfoform":
				chk_advertiser_login();
				$cardoptions = array(
				'CREDIT_CARD_UNKNOWN'=> "select card type",
				'VISA'=> "Visa",
				'AMEX'=> "American Express",
				'MASTERCARD'=> "MasterCard",
				'DISCOVER'=> "Discover"
				);

				$smarty->assign("cardoptions", $cardoptions);
				if(chk_advertiser_login()){
				$sql = $conn->Prepare("select * from billinginfo where id=?");
				$conn->execute($sql, array($_SESSION[ADV_ID]));
				if($rs && $rs->recordcount()>0){
				$billinfo= $rs->getrows();
				$smarty->assign('billinfo',$billinfo);
				}
				}
				if(isset($_REQUEST['action_billinginfo'])) {
				$rules = array(); // stores the validation rules
				$rules[] = "required,card_number,Error.";
				$rules[] = "required,card_type,Error.";
				$rules[] = "required,exp_month,Error.";
				$rules[] = "required,exp_year,Error.";
				$rules[] = "required,fname,Error.";
				$rules[] = "required,lname,Error.";
				$rules[] = "required,baddress1,Error.";
				$rules[] = "required,bcountry,Error.";

				$error = validateFields($_POST, $rules);
				if (empty($error)) {

				$card_type=$_REQUEST['card_type'];
				$card_number=$_REQUEST['card_number'];
				$cvv=$_REQUEST['cvv'];
				$fname=$_REQUEST['fname'];
				$lname=$_REQUEST['lname'];
				$exp_month=$_REQUEST['exp_month'];
				$exp_year=$_REQUEST['exp_year'];
				$baddress1=$_REQUEST['baddress1'];
				$baddress2=$_REQUEST['baddress2'];
				$bcity=$_REQUEST['bcity'];
				$bstate=$_REQUEST['bstate'];
				$bzipcode=$_REQUEST['bzipcode'];										$bcountry=$_REQUEST['bcountry'];
				$bphone=$_REQUEST['bphone'];
				$bfax=$_REQUEST['bfax'];

				$sql= "insert into billinginfo set id='$_SESSION[ADV_ID]', card_type='$card_type', card_number = '$card_number', cvv='$cvv', fname='$fname', lname='$lname', exp_month='$exp_month', exp_year='$exp_year', baddress1='$baddress1', baddress2='$baddress2', bcity='$bcity', bcountry='$bcountry', bstate='$bstate', bzipcode='$bzipcode', bphone='$bphone', bfax='$bfax'  on duplicate key update card_type='$card_type', card_number = '$card_number', cvv='$cvv', fname='$fname', lname='$lname', exp_month='$exp_month', exp_year='$exp_year', baddress1='$baddress1', baddress2='$baddress2', bcity='$bcity', bcountry='$bcountry', bstate='$bstate', bzipcode='$bzipcode', bphone='$bphone', bfax='$bfax'";
				$conn->execute($sql);
				}
				}
				break;*/
			default:
				if($_SESSION["ADV_ID"]=='') doForward($config['BASE_URL']);
				if($_REQUEST['action_bannerad1']!=""){
					$campaign['type']= $_SESSION["CAMPAIGN"]['type'];
					$campaign['subtype']= $_SESSION["CAMPAIGN"]['subtype'];
					$campaign['name'] = $_REQUEST['imgname'];
					$campaign['desturl'] = $_REQUEST['desturl'];
					session_register("CAMPAIGN");
					$_SESSION["CAMPAIGN"]=$campaign;
				}
			break;
		}
		$smarty->assign("error", $error);
		break;
	case 'signupform':
		chk_login();
		if(is_advertiser_loggedin()) {
			doForward("$config[baseurl]/adv.php");
		}
		require_once('common/portal/login/allowLogin.php');
		if(isset($_SESSION['CAMPAIGN']['currency'])) {
			$_REQUEST['currency']=$_SESSION['CAMPAIGN']['currency'];
		}
		//$smarty->assign("countrycodes", getCountryCodes($_REQUEST));
		$smarty->assign("countrycodes", getCountryList());
		$smarty->assign("timezone",getTimeZoneList());
		require_once('include/smartyBlockFunction.php');
		$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);
		break;
	case 'index':
		chk_login();
		require_once('include/smartyBlockFunction.php');
		require_once('common/portal/login/LoginFuncs.php');
		$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);
		$accounttype = mysql_escape_string($_REQUEST['accounttype']);
		
		if($_REQUEST['action_login']!=""){
			if($accounttype!=""){
				if( $accounttype=="publisher") {
					$rules = array();
					$rules[] = "required,email,Email is required.";
					$rules[] = "valid_email,email,Please enter valid email";
					$rules[] = "required,password,Password is required";
				
					$error = validateFields($_POST, $rules);
					$smarty->assign("error", $error);
				
					if(empty($error)){
						try {
							$email=getRequest('email', true);
							$password=getRequest('password', true);
							
							# create an object of LoginFuncs
							$login = new LoginFuncs();
							
							if($login->PublisherLogin($email, $password, 'online')){
								doForward("$config[baseurl]/pub.php");
							}else{
								throw new Exception('Login Failed.');
							}
						} catch(Exception $e){
							;
						}
						$smarty->assign("error_msg","true");
					}
				} elseif (is_publisher_loggedin() ) {
					doForward("$config[baseurl]/pub.php");
				}elseif( $accounttype=="advertiser") {
					$rules = array();
					$rules[] = "required,email,Email is required.";
					$rules[] = "valid_email,email,Please enter valid email";
					$rules[] = "required,password,Password is required";
					
					$error = validateFields($_POST, $rules);
					
					$smarty->assign("error", $error);
				
					if(empty($error)){
						try {
							$email=getRequest('email', true);
							$password=getRequest('password', true);
							
							# create an object of LoginFuncs
							$login = new LoginFuncs();
							
							if($login->AdvertiserLogin($email, $password, 'online')){
								doForward("$config[baseurl]/adv.php");
							}else{
								throw new Exception('Login Failed.');
							}
						} catch(Exception $e){
							;
						}
						$smarty->assign("error_msg","true");
					}	
				} elseif (is_advertiser_loggedin() ) {
					doForward("$config[baseurl]/adv.php");
				}
			}else{
				$smarty->assign("berror","Please choose an account type");
			}
		}
		break;
	case 'login':
		chk_login();
		require_once('include/smartyBlockFunction.php');
		require_once('common/portal/login/LoginFuncs.php');
		$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);
		$accounttype = mysql_escape_string($_REQUEST['accounttype']);

		if($_REQUEST['action_login']!=""){
			if($accounttype!=""){
				if( $accounttype=="publisher") {
					$rules = array();
					$rules[] = "required,email,Email is required.";
					$rules[] = "valid_email,email,Please enter valid email";
					$rules[] = "required,password,Password is required";
					
					$error = validateFields($_POST, $rules);
					$smarty->assign("error", $error);
				
					if(empty($error)){
						try {
							$email=getRequest('email', true);
							$password=getRequest('password', true);
							
							# create an object of LoginFuncs
							$login = new LoginFuncs();
							
							if($login->PublisherLogin($email, $password, 'online')){
								doForward("$config[baseurl]/pub.php");
							}else{
								throw new Exception('Login Failed.');
							}
						} catch(Exception $e){
							;
						}
						$smarty->assign("error_msg","true");
					}	
				} elseif (is_publisher_loggedin() ) {
					doForward("$config[baseurl]/pub.php");
				}elseif( $accounttype=="advertiser") {
					$rules = array();
					$rules[] = "required,email,Email is required.";
					$rules[] = "valid_email,email,Please enter valid email";
					$rules[] = "required,password,Password is required";
					
					$error = validateFields($_POST, $rules);
					$smarty->assign("error", $error);
					
					if(empty($error)){
						try {
							$email=getRequest('email', true);
							$password=getRequest('password', true);
							
							# create an object of LoginFuncs
							$login = new LoginFuncs();
							
							if($login->AdvertiserLogin($email, $password, 'online')){
								doForward("$config[baseurl]/adv.php");
							}else{
								throw new Exception('Login Failed.');
							}
						} catch(Exception $e){
							;
						}
						$smarty->assign("error_msg","true");
					}	
				} elseif (is_advertiser_loggedin() ) {
					doForward("$config[baseurl]/adv.php");
				}
			}else{
				$smarty->assign("berror","Please choose an account type");
			}
		}
		break;
	case 'advlogin':
		chk_login();
		require_once('include/smartyBlockFunction.php');
		require_once('common/portal/login/LoginFuncs.php');
		$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);
		if( $_REQUEST['action_advsignin']!="") {
			$rules = array();
			$rules[] = "required,email,Email is required.";
			$rules[] = "valid_email,email,Please enter valid email";
			$rules[] = "required,password,Password is required";
			
			$error = validateFields($_POST, $rules);
			$smarty->assign("error", $error);
			
			if(empty($error)){
				try {
					$email=getRequest('email', true);
					$password=getRequest('password', true);
					
					# create an object of LoginFuncs
					$login = new LoginFuncs();
					
					if($login->AdvertiserLogin($email, $password, 'online')){
						doForward("$config[baseurl]/adv.php");
					}else{
						throw new Exception('Login Failed.');
					}
				} catch(Exception $e){
					;
				}
				$smarty->assign("error_msg","true");
			} 
		}elseif (is_advertiser_loggedin() ) {
			doForward("$config[baseurl]/adv.php");
		}
		break;
	case 'advuserlogin':
		chk_login();
		require_once('include/smartyBlockFunction.php');
		require_once('common/portal/login/LoginFuncs.php');
		$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);
		if($_REQUEST['action_advusersignin']!="") {
			$rules = array();
			$rules[] = "required,email,Username is required.";
			$rules[] = "required,password,Password is required";
			
			$error = validateFields($_POST, $rules);
			$smarty->assign("error", $error);
			
			if(empty($error)){
				try {
					$email=getRequest('email', true);
					$password=getRequest('password', true);
					
					# create an object of LoginFuncs
					$login = new LoginFuncs();
					
					if($login->AdvUserLogin($email, $password, 'online')){
						doForward("$config[baseurl]/adv_user.php");
					}else{
						throw new Exception('Login Failed.');
					}
				} catch(Exception $e){
					;
				}
				$smarty->assign("error_msg","true");
			} 
		}elseif (is_adv_user_loggedin() ) {
			doForward("$config[baseurl]/adv_user.php");
		}
		break;
	case 'publogin':
		chk_login();
		require_once('include/smartyBlockFunction.php');
		require_once('common/portal/login/LoginFuncs.php');
		$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);
		if( $_REQUEST['action_pubsignin']!="") {
			$rules = array();
			$rules[] = "required,email,Email is required.";
			$rules[] = "valid_email,email,Please enter valid email";
			$rules[] = "required,password,Password is required";
			
			$error = validateFields($_POST, $rules);
			$smarty->assign("error", $error);
				
			if(empty($error)){
				try {
					$email=getRequest('email', true);
					$password=getRequest('password', true);
					
					# create an object of LoginFuncs
					$login = new LoginFuncs();
					
					if($login->PublisherLogin($email, $password, 'online')){
						doForward("$config[baseurl]/pub.php");
					}else{
						throw new Exception('Login Failed.');
					}
				} catch(Exception $e){
					;
				}
				$smarty->assign("error_msg","true");
			}
		}elseif (is_publisher_loggedin() ) {
			doForward("$config[baseurl]/pub.php");
		}
		break;
	case 'contactus':
		chk_login();
		$smarty->assign("countrycodes", getCountryCodes($_REQUEST));
		require_once ('include/smartyBlockFunction.php');
		$smarty->register_block ("fillInFormValues", "smartyFillInFormValues", false);
		
	    if($_REQUEST ['action_contactus']!=""){
	
	      if (empty($error)){
	
	        $name=requestParams('name');
	        $email=requestParams("email");
	        $company_name=requestParams('company_name');
	        $phone=requestParams('phone');
	        $industry=requestParams('industry');
	        $country=requestParams('country');
	
	        $subadv="User Information";
	        $subj = "Your Information";
	        $bodyadv = "<b>User Information is Given Below :</b>"."<br 
	          />"."<b>Name</b> = $name"."<br />"."<b>Email</b> = 
	          $email"."<br />"."<b>Company Name</b> = 
	          $company_name"."<br />"."<b>Phone</b> = $phone"."<br 
	          />"."<b>Budget</b> = $budget"."<br />"."<b>Country</b> = 
	          $country";
	        $body ="Dear 
	          ".strtoupper($name) ." :<br/><br/> Thank you for your interest in our network. Someone from our sales team will contact you immediately to discuss your goals.<br/><br/>Cheers,<br/>Mike";
	        if($config['sandbox']!='true'){
	          if(mail($_REQUEST['email'],$subj,$body,$config['support_headers'])){
	
	            $smarty->assign("msg","Thank you for your interest in 
	                Vdopia. Vdopia is the largest Video Advertising Network 
	                targeting South Asians worldwide. <br />We appreciate 
	                your business. A sales person would be contacting you 
	                within 48 hours.<br />Thanks <br />Vdopia Sales Team.");
	            mail("advertisers@vdopia.com",$subadv,$bodyadv,$config['support_headers']);
	          }
	          else{
	
	            $smarty->assign("msg","Please try after some time.");
	          }
	        }		
	
	      }
	
	    }
/*Code Ended	*/	
		
		break;
	case 'logout':
		session_unregister('ADV_ID');
		session_unregister('PUB_ID');
		session_unregister('LNAME');
		session_unregister('FNAME');
		session_unregister('EMAIL');
		header("location: $config[baseurl]");
		break;
	case 'advhome':
		chk_advertiser_login();
		break;
	case 'pubhome':
		chk_publisher_login();
		break;
	case 'savecampaign':
    try {
                chk_advertiser_login(true);
		
		$campaign=$_SESSION['CAMPAIGN'];
		
		/*echo "<pre>";
		print_r($campaign);
		exit;*/
		
		//==========ro list ========
		$sql = "select id , name from ro where validto>now() order by name";
		$rs = $conn->execute($sql);
	    if($rs && $rs->recordcount()>0){
	    	$ROlist = $rs->getrows();			    	
	    }
		$smarty->assign("ROlist",$ROlist);
		
		//===========================
		$smarty->assign('clientList', getClientList());
		setupCurrency();
		 $obj = json_decode($_SESSION['CAMPAIGN']['dp_expression']);
		 //print_r($obj);
		 foreach ($obj as $key=>$arr) {
			 foreach ($arr as $k=>$val) {
				//echo $val;
				 $obj[$key][$k] = json_decode($val);
			 }
		                                                                                                                                            }

		 $_SESSION['CAMPAIGN']['dp_json_expression_arr'] = $obj;                                                             
		 $smarty->assign('dp_segCount',count($_SESSION['CAMPAIGN']['dp_json_expression_arr'])*480);

		
		if($_REQUEST['action_savecampaign']!="") {
			function insertUpdateCampaign(&$campaign){
				global $config;
				global $conn, $reportConn, $_SESSION;
				
				$currency=insert_val_to_3dgt(array("value" => "$campaign[currency]"));
				$model=mysql_escape_string(strip_tags($campaign['model']));;
				$price_per_model=mysql_escape_string(strip_tags($campaign['maxcpm']));
				$dailylimit=mysql_escape_string(strip_tags($campaign['dailylimit']));
				$budget=mysql_escape_string(strip_tags($campaign['budget']));
				$name=mysql_escape_string(strip_tags($_REQUEST['campaignname']));
				$campaign_status=mysql_escape_string(strip_tags($_REQUEST['campaign_status']));
				$vidtype=mysql_escape_string(strip_tags($campaign['vidtype']));
				$vastfeedvidtype=mysql_escape_string(strip_tags($campaign['vastfeedvidtype']));
				$vasttype=mysql_escape_string(strip_tags($campaign['vasttype']));
				$trktype=mysql_escape_string(strip_tags($campaign['trktype']));
				$subtype=mysql_escape_string(strip_tags($campaign['subtype']));
				$review_status=mysql_escape_string(strip_tags($campaign['review_status']));
				$booked=mysql_escape_string(strip_tags($campaign['booked']));
				$distribution=mysql_escape_string(strip_tags($campaign['distribution']));
				$daily_consumption=mysql_escape_string(strip_tags($campaign['daily_icclimit']));
				$priority_level=mysql_escape_string(strip_tags($campaign['priority_level']));
				$pacing_type = mysql_escape_string(strip_tags($campaign['pacing_type']));
				$capping=mysql_escape_string(strip_tags($campaign['capping']));
				$session_capping=mysql_escape_string(strip_tags($campaign['session_capping']));
				$resettime=mysql_escape_string(strip_tags($campaign['resettime']));
				$frequency_per_day=mysql_escape_string(strip_tags($campaign['frequency_per_day']));
				$frequency_per_week=mysql_escape_string(strip_tags($campaign['frequency_per_week']));
				$frequency_per_month=mysql_escape_string(strip_tags($campaign['frequency_per_month']));
				$branding_title=mysql_escape_string(strip_tags($_REQUEST['branding_title']));
				$timezone = mysql_escape_string(strip_tags($campaign['timezone']));
				$start_time=mysql_escape_string(strip_tags($campaign['start_time']));
				$end_time=mysql_escape_string(strip_tags($campaign['end_time']));
				$skipoffset=mysql_escape_string(strip_tags($_REQUEST['skipoffset']));
				if($distribution=="None"){
					$weight=mysql_escape_string(strip_tags($campaign['weight']));
				}else{
					$weight=""; 
				}
				
				if($campaign['start_date']!=""){
					$start_date=mysql_escape_string(strip_tags($campaign['start_date']));
				}else{
					$start_date=date("M d Y");
				}
				
				if($campaign['end_date']!=""){
					$end_date=mysql_escape_string(strip_tags($campaign['end_date']));
				}else{
					$end_date=NULL;
				}
				
				if(isset($campaign['id'])){
					$prefix="update campaign";
					//$postfix=", status='Pending Review' where id=$campaign[id] and advertiser_id=$_SESSION[ADV_ID]";
					$postfix=" where id=$campaign[id] and advertiser_id=$_SESSION[ADV_ID]";
					if($_SESSION['ADMIN_ID']!=""){
					  $name="$name";
					}else{
					  $name="$campaign[name]";
					}
					$geographyprefix="insert into geography set ";
					$geographypostfix = ",";

					$timeprefix="insert into campaign_UI set ";
					$timepostfix = ",";
				}else {
					$prefix="insert into campaign";
					$postfix="";

					$geographyprefix="insert into geography set ";
					$geographypostfix = ",";

					$timeprefix="insert into campaign_UI set ";
					$timepostfix = ",";
				}

				/*final verifications*/
				if($campaign['type']!='trackeradvertisement'){
					if($price_per_model == "" || $start_date=="" || $name=="" ) {
						/*Redir to beginning*/
						header("location: $config[baseurl]/index.php?page=formloader&form=ad");
						/*TODO give some message*/
						exit();
					}
				}

				require_once("Date.php");
				if( $start_date!=NULL && $start_date!="") {
					$start_date.=$start_time;
					$start_date=date("YmdHis",strtotime($start_date));
					$dateObj = new Date($start_date);
			//		$dateObj->setTZByID("GMT");		# set local time zone
		//			$dateObj->convertTZByID("$timezone");	# convert to foreign time zone
					$dateObj->setTZByID("$timezone");		# set local time zone
					$dateObj->convertTZByID("GMT");	# convert to foreign time zone
					$startdateSql="validfrom='".$dateObj->format("%Y-%m-%e %T")."', ";
				} else {
					$startdateSql="validfrom=NULL,";
				}
				
				if( $end_date!=NULL && $end_date!=""){
				    $end_date.=$end_time; 
					$end_date=date("YmdHis",strtotime($end_date));
					$dateObj = new Date($end_date);
		//			$dateObj->setTZByID("GMT");		# set local time zone
	//				$dateObj->convertTZByID("$timezone");	# convert to foreign time zone
					$dateObj->setTZByID("$timezone");		# set local time zone
					$dateObj->convertTZByID("GMT");	# convert to foreign time zone
					$enddateSql="validto='".$dateObj->format("%Y-%m-%e %T")."', ";
				} else {
					$enddateSql="validto=NULL,";
				}
				
				if($budget!="" && $budget!=NULL){
					$budgetSql="budget_amt='$budget'";
				} else {
					$budgetSql="budget_amt=NULL";
				}
				
				if($_SESSION[ADV_PUB_ID]!=""){
					if($review_status!=""){
						$sqlSnip="review_status='$review_status', ";
					}
					else{
						$sqlSnip="";
					}
				}
				else{
					$sqlSnip="";
				} 
				
				//======== insert RO details in to RO table=================
				if($_REQUEST['rotype']=="chooseRO"){
					$ro_id=mysql_escape_string(strip_tags($_REQUEST['campaign_RO']));
					$ROconcat = "ro_id=$ro_id,";
				}
				else
					$ROconcat = "";
					
					
				//==========================================================	
				if(isset($branding_title)&&$branding_title!=""){
				  $custom_details['branding_title']=$branding_title;
				}else{
				  $custom_details['branding_title']="Ads By Vdopia";
				}
				if(isset($skipoffset) && $skipoffset!=''){
					$custom_details['skipoffset']=$skipoffset;
				}
				
				$custom_details = json_encode($custom_details);		
				
				$sql="$prefix set
					advertiser_id='$_SESSION[ADV_ID]',
					$budgetSql,
					price_per_model='$price_per_model',
					budget_currency='$currency',
					model='$model',
					daily_limit='$dailylimit',
					priority_level='$priority_level',
					pacing_type='$pacing_type',
					custom_details='$custom_details',
					timezone='$timezone',
					$startdateSql $enddateSql $sqlSnip
					name='$name'";
			
				if($campaign['istargetall']=="choosecategory")
				{
					$comma="";
					$i=0;
					$categoryconcat = "";
					for($i=0; $i< count($campaign['categories']); $i++){
						if($i!=0){
							$comma = ",";
						}
						$categoryconcat =$categoryconcat.$comma.$campaign['categories'][$i];

					}
				}

				if($campaign['type']=="videoadvertisement")
					if($vidtype!="") $sql.=",video_choice='$vidtype'";
				if($campaign['type']=="vastfeedvideoadvertisement")
					if($vastfeedvidtype!="") $sql.=",video_choice='$vastfeedvidtype',vast_choice='$vasttype'";
				if($campaign['type']=="ytvideoadvertisement")
					$sql.=",vast_choice='vpaid' ";
				if($trktype!="") $sql.=",track_choice='$trktype'";
				if($subtype!="") $sql.=",overlay_choice='$subtype'";
				if($campaign['istargetall']=="choosecategory"){
					$categorycon=mysql_escape_string($categoryconcat);
					$sql.=",cat_choice='$categorycon'";
				}else{
					$sql.=",cat_choice=NULL";
				}

				//=======This blcok of code will remove once channel_set updation finalize ======
				/*
				if(isset($_SESSION[ADV_PUB_ID])){
					if($campaign['issitetargetingall']=="choosesitetargeting"){
						$site=mysql_escape_string(strip_tags(implode(',',$campaign['site'])));
						$sql1 = "select count(*) as count from channels where publisher_id='$_SESSION[ADV_PUB_ID]' and verified='Verified' and channel_type='PC' and id in ($site)";
						$rs=$conn->execute($sql1);
						if($rs && $rs->recordcount()>0){
							$total = $rs->fields[count];
							if($total== count($campaign['site'])){
								$sql.=",channel_choice='$site'";
								error_log("Reached good place ".$sql1);
							}else{
                throw new Exception("The sites selected do not belong to your account, please retry");
							}
						}else{
							throw new Exception("Invalid sites, please retry");
						}
					}
					else{	
						$sql1 = "select group_concat(cast(id as char)) as channelids from channels where verified='Verified' and publisher_id='$_SESSION[ADV_PUB_ID]' and channel_type='PC'";
						$rs = $conn->execute($sql1);
						if($rs && $rs->recordcount()>0){
							$channelids=$rs->fields['channelids'];
						}
						error_log("Reached good place ".$sql);
						$sql.=",channel_choice='$channelids'";
						error_log($sql);
				}	
				}else{
					if($campaign['issitetargetingall']=="choosesitetargeting"){
						$site=mysql_escape_string(strip_tags(implode(',',$campaign['site'])));
						$sql.=",channel_choice='$site'";
					}
					else{
							$sql.=",channel_choice=NULL";
					}
				}
				*/
				//========
				
				
        $sql.=" ,device='$campaign[device]'";
				

				$sql.= " $postfix";
                                $rs=$conn->execute($sql);
				$inserted_id=$conn->Insert_ID();
				
				
				//=======updating chanenl_set in campaign table start ======
				if(($inserted_id && $inserted_id!=0) || ($campaign['id'] && $campaign['id']!=0)){
					$cid = ($inserted_id)?$inserted_id:$campaign['id'];
					$client_ids = $_REQUEST['client_id'];
					if(sizeof($client_ids)>0){
						$adv_id=$_SESSION['ADV_ID'];
						$query_delete=$conn->Prepare("delete from adv_user_campaign where campaign_id=? and advertiser_id =? ");
						$result_delete=$conn->execute($query_delete,array($cid,$adv_id));
						foreach($client_ids as $client){
							$query_insert=$conn->Prepare("insert into adv_user_campaign set adv_user_id=? , advertiser_id=? , campaign_id=?");
							$result_insert=$conn->execute($query_insert,array($client,$adv_id,$cid));
						}
					}
						if(isset($_SESSION[ADV_PUB_ID])){
							if(isset($_SESSION['ADMIN_ID'])){
								if($campaign['issitetargetingall']=="choosesitetargeting"){
									if(is_array($campaign['site'])){
										$site=implode(',', $campaign['site']);
									}else{
										$site=$campaign['site'];
									}
									$channel_set = VdoChannelChoice::intListToBitSet($site);
								}
								else{
									$channel_set=NULL;
									$site=NULL;
								}
							}else{
								if($campaign['issitetargetingall']=="choosesitetargeting"){
									$site=mysql_escape_string(strip_tags(implode(',',$campaign['site'])));
									$sql1 = "select count(*) as count from channels where publisher_id='$_SESSION[ADV_PUB_ID]' and channel_type='PC' and id in ($site)";
									$rs=$conn->execute($sql1);
									if($rs && $rs->recordcount()>0){
										$total = $rs->fields[count];
										if($total== count($campaign['site'])){
											$channel_set = VdoChannelChoice::intListToBitSet($site);
										}else{
										throw new Exception("The sites selected do not belong to your account, please retry");
										
										}
									}else{
										throw new Exception("Invalid sites, please retry");
										
									}
								}
								else{	
									$sql1 ="select group_concat(cast(channels.id as char)) as channelids from channels where channels.verified='Verified' and publisher_id='$_SESSION[ADV_PUB_ID]' and channel_type='PC'";
									$rs = $conn->execute($sql1);
									if($rs && $rs->recordcount()>0){
										$site=$rs->fields['channelids'];
									}
									$channel_set = VdoChannelChoice::intListToBitSet($site);
								}	
							}
						}else{
							if($campaign['issitetargetingall']=="choosesitetargeting"){
								if(is_array($campaign['site'])){
									$site=implode(',', $campaign['site']);
								}else{
									$site=$campaign['site'];
								}
								$channel_set = VdoChannelChoice::intListToBitSet($site);
							}
							else{
									$channel_set=NULL;
									$site=NULL;
							}
						}
					$sql = $conn->Prepare("update campaign set channel_choice=?, channel_set=? where id=?");
  					$rs = $conn->Execute($sql, array($site, $channel_set, $cid));
  					//exit;
				}
				//=======updating chanenl_set in campaign table end ======
		        /* adding expression if set in campaign_expression table: by tanu aggarwal */
		        if (isset($campaign['dp_expression']) && $campaign['dp_expression'] != "") {
			        require_once ABSPATH . "/common/include/class.dataProvider.php";
			        $obj = new DataProvider();
			        $origExpr = $obj->generateOrigExpr($campaign['dp_expression']);
			        if(isset($campaign['id'])){
			        	$obj->updateCampaignExpression($origExpr, $campaign['id']);
			        } else { 
			        	$obj->saveCampaignExpression($origExpr, $cid);
			        }
		        }                                                                                                                                      
		        else if (isset($campaign['dp_expression']) && $campaign['dp_expression'] == "") {
		        	if(isset($campaign['id'])){
			          require_once ABSPATH . "/common/include/class.dataProvider.php";
			          $obj = new DataProvider();
			          $obj->deleteCampaignExpression($campaign['id']);
		          	}
		        }

				# calculate price per unit and price norm for this campaign
				$price_per_unit=0;
				$price_norm=0;
				switch($model){
					case "CPC":
						if($inserted_id!=""){
							getCPCNormPrice($price_per_unit, $price_norm, $price_per_model, $currency);
						}else{
							getCPCNormPrice($price_per_unit, $price_norm, $price_per_model, $currency, $campaign['id']);
							if($price_per_unit==0 || $price_norm==0) getCPCNormPrice($price_per_unit, $price_norm, $price_per_model, $currency);
						}
						if($price_per_model > 0){
                        	$campaign['ovrallmaxclk'] = ceil($budget/$price_per_model);
                            $campaign['ovralldailyclk'] = ceil($dailylimit/$price_per_model);
                        }
					break;
					case "CPL":
						if($inserted_id!=""){
							getCPLNormPrice($price_per_unit, $price_norm, $price_per_model, $currency);
						}else{
							getCPLNormPrice($price_per_unit, $price_norm, $price_per_model, $currency, $campaign['id']);
							if($price_per_unit==0 || $price_norm==0) getCPLNormPrice($price_per_unit, $price_norm, $price_per_model, $currency);
						}
					break;
					case "CPCV":
						$price_per_unit = $price_per_model;
						if($inserted_id!=""){
							getCPCVNormPrice($price_per_unit, $price_norm, $price_per_model, $currency);
						}else{
							getCPCVNormPrice($price_per_unit, $price_norm, $price_per_model, $currency, $campaign['id']);
						}
						
						if($price_per_model > 0){
							$campaign['ovrallmaxcomp'] = ceil($budget/$price_per_model);
							$campaign['ovralldailycomp'] = ceil($dailylimit/$price_per_model);
						}
					break;
					
					case "CPM":
						$price_per_unit = $price_per_model/1000;
						$sql=$reportConn->Prepare("select (?/(select ex_rate from currency where currency.code=? order by updated desc limit 0,1)) as price_norm");
						$rs=$reportConn->execute($sql, array($price_per_unit, $currency));
						$price_norm = $rs->fields[price_norm];
						if($price_per_unit > 0){
                        	$campaign['ovrallmaximp'] = ceil($budget/$price_per_unit);
                            $campaign['ovralldailyimp'] = ceil($dailylimit/$price_per_unit); 
                        }
					break;
					default:
						die("Model type does not exist.");
					break;	
				}
				
				# campaign limit inclusion
				try{
					include_once(dirname(__FILE__).'/common/portal/campaign/CampaignLimit.php');
					$cmpgnLimit = new CampaignLimit();
					
					if(isset($campaign['id']) || $campaign[id]>0){
						$cmpgn_id = $campaign['id'];
					}else{
						$cmpgn_id=$inserted_id;
					}
					$cmpgnLimit->insert($cmpgn_id, $campaign);
				}catch(Exception $e){
					if($conn->errorno()==1142  && in_array('rw.ro',$_SESSION['CAPABILITIES'])){ 	# added by abhay to catch readonly users action
						header("Location: ".$config['BASE_URL']."/adv.php?page=readonly");
						exit(1);
					}else{
						exit($conn->errormsg());
					}
				}
				
				if($inserted_id && $inserted_id!=0 || ($campaign['id'] && $campaign['id']!=0)){
					$cid = ($inserted_id)?$inserted_id:$campaign['id'];

					$sql=$conn->Prepare("delete from frequency where campaign_id=?");
					$conn->execute($sql, array($cid));
					
					
					# frequency per day
					if($frequency_per_day && $frequency_per_day!=0){
						$sql = "insert into frequency set campaign_id='$cid', impressions='$frequency_per_day', unit='DAY' on duplicate key update impressions='$frequency_per_day'";
						$conn->execute($sql);
					}
					
					# frequency per week
					if($frequency_per_week && $frequency_per_week!=0){
						$sql = "insert into frequency set campaign_id='$cid', impressions='$frequency_per_week', unit='WEEK' on duplicate key update impressions='$frequency_per_week'";
						$conn->execute($sql);
					}
					
					# frequency per month
					if($frequency_per_month && $frequency_per_month!=0){
						$sql = "insert into frequency set campaign_id='$cid', impressions='$frequency_per_month', unit='MONTH' on duplicate key update impressions='$frequency_per_month'";
						$conn->execute($sql);
					}
				}
				
				if($inserted_id && !isset($campaign['id'])){
					$cid=$inserted_id;
					$sql2= $conn->Prepare("update `campaign` set price_norm=?, price_per_unit=? where id=?");
					$rs2 = $conn->execute($sql2, array($price_norm, $price_per_unit, $cid));
				}
				elseif(isset($campaign['id'])){
					$cid=$campaign['id'];
					$sql2= $conn->Prepare("update `campaign` set price_norm=?, price_per_unit=? where id=?");
					$rs2 = $conn->execute($sql2, array($price_norm, $price_per_unit, $cid));
				}
				else
					$cid=-1;
				if($cid != -1){
					if($campaign['isgeotargetingall']=="choosegeotargeting"){
						$sql1 = $conn->Prepare("delete from geography where campaign_id = ?");
						$rs1 = $conn->execute($sql1, array($cid));
					       $countrycon=trim(mysql_escape_string($campaign[countryconcat]));
                        $stateconcat=trim(mysql_escape_string($campaign[stateconcat]));
                        $postalconcat=trim(mysql_escape_string($campaign[postalconcat]));
                        $nogeoselction=false;
                        if(strlen($countrycon)==0&&strlen($stateconcat)==0&&strlen($postalconcat)==0){
                            $nogeoselction=true;
                        }
                        if($nogeoselction==false){
                        $sql1 = $geographyprefix." country_codes = '$countrycon',state_codes = '$stateconcat',postalcodes = '$postalconcat' ".$geographypostfix."campaign_id = '$cid'";
                        $rs1 = $conn->execute($sql1);
                        if($conn->Affected_Rows()==0){
                            $error = "Errors with inserting geographical targeting information";
                        }
                        }
					}else{
						$sql1 = $conn->Prepare("delete from geography where campaign_id = ?");
						$rs1 = $conn->execute($sql1, array($cid));

					}
					
					$sql1 = $conn->Prepare("delete from campaign_UI where campaign_id = ?");
					$rs1 = $conn->execute($sql1, array($cid));

					
					$booked=mysql_escape_string($campaign[booked]);
					$distribution=mysql_escape_string($campaign[distribution]);
					$daily_icclimit=mysql_escape_string($campaign[daily_icclimit]);
					$capping=mysql_escape_string($campaign[capping]);
					$session_capping=mysql_escape_string($campaign[session_capping]);
					$resettime=mysql_escape_string($campaign[resettime]);
					
					
						$sql_snip.="timezone = NULL,";
						$sql_snip.="starttime = NULL,";
						$sql_snip.="endtime = NULL,";
					
					if($booked!=""){
						$sql_snip.="booked = '$booked',";
					}else{
						$sql_snip.="booked = NULL,";
					}
					if($distribution!=""){
						$sql_snip.="distribution = '$distribution',";
					}else{
						$sql_snip.="distribution = 'Daily',";
					}
					if($daily_icclimit!=""){
						$sql_snip.="daily_consumption = '$daily_icclimit',";
					}else{
						$sql_snip.="daily_consumption = NULL,";
					}
					if($weight!=""){
						$sql_snip.="weight = '$weight',";
					}else{
						$sql_snip.="weight = NULL,";
					}
					if($capping!=""){
						$sql_snip.="capping = '$capping',";
					}else{
						$sql_snip.="capping = '$capping',";
					}
					if($session_capping!=""){
						$sql_snip.="session_capping = '$session_capping',";
					}else{
						$sql_snip.="session_capping = '0',";
					}
					if($resettime!=""){
						$sql_snip.="resettime = '$resettime',";
					}else{
						$sql_snip.="resettime = '0',";
					}
					if($campaign_status!=""){
						$sql_snip.="campaignstatus = '$campaign_status'";
					}else{
						$sql_snip.="campaignstatus = '$review_status'";
					}
					
					$sql1 = $timeprefix.$sql_snip.$timepostfix.$ROconcat."campaign_id = '$cid'";
					$rs1 = $conn->execute($sql1);
					if($conn->Affected_Rows()==0){
							$error = "Errors with inserting time targeting information";
					}
					
					
					
						//advance targeting
					$var=new TargetingUI(); $var->saveInDB($cid);
					$BT_UI=new BT_UI(); $BT_UI->saveInDB($cid);
					$RT_UI=new RetargetingUI();$RT_UI->saveInDB($cid);
				    /*if($campaign['istimeall']=="choosetime"){
						
						$sql1 = $conn->Prepare("delete from campaign_UI where campaign_id = ?");
						$rs1 = $conn->execute($sql1, array($cid));

						$timezone=mysql_escape_string($campaign[timezone]);
						$starttime=mysql_escape_string($campaign[starttime]);
						$endtime=mysql_escape_string($campaign[endtime]);
						$sql1 = $timeprefix." timezone = '$timezone',  starttime = '$starttime', endtime = '$endtime'".$timepostfix."campaign_id = '$cid'";
						$rs1 = $conn->execute($sql1);
						if(mysql_affected_rows()==0){
								$error = "Errors with inserting time targeting information";
						}
					}else{
						$sql1 = $conn->Prepare("delete from campaign_UI where campaign_id = ?");
						$rs1 = $conn->execute($sql1, array($cid));

					}*/
					
					

				}

				return $cid;
			}

			function emptyCampaign($cid, &$adid_already) {
				global $conn;
				$sql = $conn->Prepare("select ad_id from campaign_members cm left join ads a on cm.ad_id=a.id where cid=? and isnull(a.parent_ad_id)"); // join to remove dummy ads from ad list
				$rs=$conn->execute($sql, array($cid));
				$count = 0;
				if($rs && $rs->recordcount()>0){
					while(!$rs->EOF) {
							$adidsall[]=$rs->fields['ad_id'];
							$count++;
							$rs->movenext();
						}

				}
				sort($adid_already);
				sort($adidsall);

				if(sizeof($adid_already)>0 && sizeof($adidsall)>0){
					$deleted_ids = array_diff($adidsall,$adid_already);
					foreach ($deleted_ids as $key => $val){
							$sql=$conn->Prepare("update campaign_members set status='invalid' where cid=? and ad_id=?");
							$conn->execute($sql, array($cid, $val));	
					}
				}else{
					trigger_error("Error: adid_already - ".print_r($adid_already, true).", adidsall - ".print_r($adidsall, true), E_USER_WARNING);
				}
			}
			function checkAdDirtyAndUpdate($cid, &$adid, &$adid_already) {
				
				global $conn;
				
				if(count($adid)==0) {
					$sql=$conn->Prepare("select ad_id from campaign_members where cid=?");
					$rs=$conn->execute($sql, array($cid));
					$count = 0;
					if($rs && $rs->recordcount()>0) {
						while(!$rs->EOF) {
							$adids[]=$rs->fields['ad_id'];
							$count++;
							$rs->movenext();
						}
					}
					if ($count!=count($adid_already))
						if (count(array_diff($adid_already,$adids))==0)
							return false;
				}
        		$sql=$conn->Prepare("update campaign set review_status='Pending Review' where id=?");
				$rs=$conn->execute($sql, array($cid));
				return true;
			}

			function cleanupCampaign($cid, $adid) {
				global $conn;
				foreach($adid as $ad) {
					if($ad > 0){
						$sql=$conn->Prepare("delete from ads where id=?");
						$conn->execute($sql, array($ad));
					}
				}
				$sql=$conn->Prepare("delete from campaign where id=?");
				$conn->execute($sql, array($cid));
				emptyCampaign($cid);
			}
			try{
				$cid=insertUpdateCampaign($campaign);
			}catch(Exception $e){
				if($conn->errorno()==1142  && in_array('rw.ro',$_SESSION['CAPABILITIES'])){ 	# added by abhay to catch readonly users action
					header("Location: ".$config['BASE_URL']."/adv.php?page=readonly");
					exit(1);
				}else{
					exit($conn->errormsg());
				}
			}
			
			
			$adid_already = array();
			$adid=array();
                        switch ($campaign['type']) {
				case 'videoadvertisement':
					$videoads=$campaign['videoads'];
					$campaignBannerArr=$campaign['campaignBannerArr'];
					$sriads=$campaign['sriads'];
					if($campaign['backgroundbannerurl']!="") $sriBackground=$campaign['backgroundbannerurl'];
					if($campaign['advlogourl']!="") $advLogo=$campaign['advlogourl'];
					if($campaign['shareTitle']!="") $shareTitle=$campaign['shareTitle'];
					if($campaign['respondTitle']!="") $respondTitle=$campaign['respondTitle'];
					if($campaign['interactTitle']!="") $interactTitle=$campaign['interactTitle'];
	
					# companion banners
					if($videoads != "") {
						foreach ($videoads as $adKey => $cads){
							if($cads[id]!="")
							{
								$companionBanners ="";
								$campanion_already = array();
								$swfids = explode(',', $cads['swfid']);
								$imgids = explode(',', $cads['imgid']);
								$campaniontype = mysql_escape_string(strip_tags($cads['campaniontype']));
								foreach($swfids as $key => $cswfid){
									$swffilename=$config['AD_SWF_DIR']."/$cswfid.swf";
									if($imgids[$key]==""){
										$imgfilename=$config['AD_IMG_DIR']."/$cswfid";
									}else{
										$imgfilename=$config['AD_IMG_DIR']."/$imgids[$key]";
									}
									
									if(file_exists($swffilename) && $campaniontype=="swf"){
										$campanion_already[]=$cswfid;
									}else{
										if($cswfid!="" && $campaniontype=="swf"){
											$cimgfile=md5(time().$key.$adKey);
											$frompath=$config['tempfilepath']."/".$cswfid;
											$toswfpath=$config['AD_SWF_DIR']."/$cimgfile.swf";
											rename($frompath,$toswfpath);
											$campanion_already[]=$cimgfile; 	
										}
									}

									if(file_exists($imgfilename) && $campaniontype=="image"){
										if($imgids[$key]==""){
											$campanion_already[]=$cswfid;
										}else{
											$campanion_already[]=$imgids[$key];
										}
									}else{
										if($cswfid!="" && $campaniontype=="image"){
											$cimgfile=md5(time().$key.$adKey);
											$frompath=$config['tempfilepath']."/".$cswfid;
											$toimgpath=$config['AD_IMG_DIR']."/$cimgfile";
											rename($frompath,$toimgpath); 
											$campanion_already[]=$cimgfile;	
										}
									}

									if((file_exists($swffilename) || file_exists($imgfilename)) && $campaniontype=="both"){
										$campanion_already[]=$cswfid;
									}else{
										if($cswfid!="" && $campaniontype=="both"){
											$cimgfile=md5(time().$key.$adKey);
											$swffrompath=$config['tempfilepath']."/".$cswfid.".swf";
											$imgfrompath=$config['tempfilepath']."/".$imgids[$key];
											$imgfile=md5(time());
											$toimgpath=$config['AD_IMG_DIR']."/$cimgfile";
											rename($imgfrompath,$toimgpath);
											$toswfpath=$config['AD_SWF_DIR']."/$cimgfile.swf";
											rename($swffrompath,$toswfpath);
											$campanion_already[]=$cimgfile; 	
										}
									}
								}
								
								$companionBanners=implode(',', $campanion_already);
								foreach($campaignBannerArr as $combanner){
									if($cads['id']==$combanner['id']){
										$flag=1; 
										break;
									}else $flag=0;
								}
								if($flag==1){
									$sqlc = "update ads set dimension='$cads[dimession]', branded_img_bot_ref_txbody='$companionBanners' where id='$cads[id]'";
								}else{
									$sqlc = "update ads set dimension='', branded_img_bot_ref_txbody='' where id='$cads[id]'";
								}
								$rs=$conn->execute($sqlc);
							}
						}
						
						# sri elements 
						foreach ($videoads as $adrow) {
							if($adrow['id']!="") {
								$adid_already[]=$adrow['id'];
								$sql=$conn->Prepare("SELECT sri_details FROM ads WHERE id=?");
								$rs=$conn->execute($sql, array($adrow[id]));
								if($rs && $rs->recordcount()>0){
									$objJson=json_decode($rs->fields['sri_details']);
									$fileArr=explode('/',$objJson->backimg);
									$bannerURL = $fileArr[sizeof($fileArr)-1];
									
									# background banner image
									if($sriBackground!=$bannerURL){
										if($sriBackground!=""){
											$adbase=md5('imgurl'.rand(0,1000000000));
											$sriBackgroundsrc="$config[tempfilepath]/$sriBackground";
											$ext=explode('.',$sriBackground);
											$sriBackgrounddest="$config[AD_IMG_DIR]/$adbase"."bck."."$ext[1]";
											$sriBackgroundjson="$config[CDN_URL]/files/images/$adbase"."bck."."$ext[1]";
											if(file_exists("$config[AD_IMG_DIR]/$sriBackground")){
												copy("$config[AD_IMG_DIR]/$sriBackground", $sriBackgrounddest);
											}else{
												copy($sriBackgroundsrc, $sriBackgrounddest);
											}
										}
									}else{
										$sriBackgroundjson=$objJson->backimg;
									}
									
									$fileArr=explode('/',$objJson->advlogo);
									$logoURL = $fileArr[sizeof($fileArr)-1];
									
								    # Advertiser Logo
									if($advLogo!=$logoURL){
										if($advLogo!=""){
											$adbase=md5('imgurl'.rand(0,1000000000));
											$advLogosrc="$config[tempfilepath]/$advLogo";
											$ext=explode('.',$advLogo);
											$advLogodest="$config[AD_IMG_DIR]/$adbase"."logo.".$ext[count($ext)-1];
											$advLogojson="$config[CDN_URL]/files/images/$adbase"."logo.".$ext[count($ext)-1];
											if(file_exists("$config[AD_IMG_DIR]/$advLogo")){
												copy("$config[AD_IMG_DIR]/$advLogo", $advLogodest);
											}else{
												copy($advLogosrc, $advLogodest);
											}
										}
									}else{
										$advLogojson=$objJson->advlogo;
									}
									
									if($campaign['alpha']!="") $alpha=$campaign['alpha']; else $icon_align=$objJson->icon_align;
									if($campaign['icon_align']!="") $icon_align=$campaign['icon_align']; else $alpha=$objJson->alpha;
									if($shareTitle=="") $shareTitle=$objJson->srititles[0];
									if($respondTitle=="") $respondTitle=$objJson->srititles[1];
									if($interactTitle=="") $interactTitle=$objJson->srititles[2];
									$sriDetailsArray=array(
										'backimg' => "$sriBackgroundjson",
									    'advlogo' => "$advLogojson",
										'srititles' => array("$shareTitle","$respondTitle","$interactTitle"),
										'sriorder' => array("share", "respond", "interact"),
                						'alpha' => "0.5");
										$sriDetailsJson = mysql_real_escape_string(json_encode($sriDetailsArray));
										
									if($campaign['isTTM']=="true"){
										$sql=$conn->Prepare("UPDATE ads SET sri_details=? WHERE id=?");
										$rsUpdate=$conn->execute($sql, array($sriDetailsJson, $adrow[id]));
										if($rsUpdate){
											$sql=$conn->Prepare("update campaign set review_status='Pending Review' where id=?");
											$rs=$conn->execute($sql, array($campaign[id]));
										}
									}
								}
								if(sizeof($sriads)>0){
									$var=new SRIPortal();
									$var->updateSRI($sriads,$adrow[id], $_SESSION[ADV_ID], $campaign[id]);
								}
								continue;
							}
							$branded_img_bot_ref_txbody ="";
							$uri=mysql_escape_string(escapeshellcmd(strip_tags($adrow['uri'])));
							$name=mysql_escape_string(strip_tags($adrow['name']));
							$dimension = mysql_escape_string(strip_tags($adrow['dimession']));
							if(!$dimension) $dimension = mysql_escape_string(strip_tags($adrow['dimension']));
							$duration = mysql_escape_string(strip_tags($adrow['duration']));
							$campaniontype = mysql_escape_string(strip_tags($adrow['campaniontype']));
							$desturl=mysql_escape_string($adrow['desturl']);
							$trackurl=mysql_escape_string($adrow['trackurl']);
							if(isset($adrow['vi_0'])&&$adrow['vi_0']!="") $ad_details["vdo_tvi_0"]=mysql_escape_string(strip_tags($adrow['vi_0']));
							if(isset($adrow['vi_25'])&&$adrow['vi_25']!="") $ad_details["vdo_tvi_25"]=mysql_escape_string(strip_tags($adrow['vi_25']));
							if(isset($adrow['vi_50'])&&$adrow['vi_50']!="") $ad_details["vdo_tvi_50"]=mysql_escape_string(strip_tags($adrow['vi_50']));
							if(isset($adrow['vi_75'])&&$adrow['vi_75']!="") $ad_details["vdo_tvi_75"]=mysql_escape_string(strip_tags($adrow['vi_75']));
							if(isset($adrow['ae'])&&$adrow['ae']!="") $ad_details["vdo_tae"]=mysql_escape_string(strip_tags($adrow['ae']));
							if(sizeof($ad_details)>0){
								$ad_details = json_encode($ad_details);
								$ad_details = str_replace('\/','/',$ad_details);
							}else $ad_details='';
							
							if($adrow['swfid']!=""){
								//========================================================
								$tempswfid = explode(",",$adrow['swfid']);
								$tempimgid = explode(",",$adrow['imgid']);
								
								
								foreach ($tempswfid as $key => $swfid) {
									$rand = rand(0,1000000000);
									$frompath=$config['tempfilepath']."/".$swfid;
									
									$imgfile=md5($adrow['imgurl'].$rand);
																		
									if($campaniontype=="image"){
										$toimgpath=$config['AD_IMG_DIR']."/$imgfile";
										rename($frompath,$toimgpath); 	
									}
									elseif ($campaniontype=="swf"){
										$toswfpath=$config['AD_SWF_DIR']."/$imgfile.swf";
										rename($frompath,$toswfpath);
									}
									elseif($campaniontype=="both"){
										$swffrompath=$config['tempfilepath']."/".$swfid.".swf";
										$imgfrompath=$config['tempfilepath']."/".$tempimgid[$key];
										$imgfile=md5($adrow['imgurl'].$rand);
										$toimgpath=$config['AD_IMG_DIR']."/$imgfile";
										rename($imgfrompath,$toimgpath);
										$toswfpath=$config['AD_SWF_DIR']."/$imgfile.swf";
										rename($swffrompath,$toswfpath);
										
									}
									$branded_img_bot_ref_txbody.=",".$imgfile; 
								}
								$branded_img_bot_ref_txbody=substr($branded_img_bot_ref_txbody,1); 
								$branded_img_bot_ref_txbody=mysql_escape_string(strip_tags($branded_img_bot_ref_txbody));
								//===========================================================		
							}
							
							$flvsrc="$config[tempfilepath]/$uri.flv";
							$thmbsrc="$config[tempfilepath]/thmb$uri.jpg";
							$adbase=md5($adrow['imgurl'].rand(0,1000000000));
							$flvfile=$adbase.".flv";
							$thmbdest="$config[AD_IMG_DIR]/$adbase.jpg";
							$flvdest="$config[AD_VDO_DIR]/$flvfile";
							$rs=false;
							
							# background banner image
							$sriBackgroundsrc="$config[tempfilepath]/$sriBackground";
							if($sriBackground!=""){
								$ext=explode('.',$sriBackground);
								$sriBackgrounddest="$config[AD_IMG_DIR]/$adbase"."bck."."$ext[1]";
								$sriBackgroundjson="$config[CDN_URL]/files/images/$adbase"."bck."."$ext[1]";
								if(file_exists("$config[AD_IMG_DIR]/$sriBackground")){
									copy("$config[AD_IMG_DIR]/$sriBackground", $sriBackgrounddest);
								}else{
									copy($sriBackgroundsrc, $sriBackgrounddest);
								}
							}
							
						    # advetiser logo
							$advLogosrc="$config[tempfilepath]/$advLogo";
							if($advLogo!=""){
								$ext=explode('.',$advLogo);
								$advLogodest="$config[AD_IMG_DIR]/$adbase"."logo.".$ext[count($ext)-1];
								$advLogojson="$config[CDN_URL]/files/images/$adbase"."logo.".$ext[count($ext)-1];
								if(file_exists("$config[AD_IMG_DIR]/$advLogo")){
									copy("$config[AD_IMG_DIR]/$advLogo", $advLogodest);
								}else{
									copy($advLogosrc, $advLogodest);
								}
							}
							
							if($campaign['isTTM']=="true") {
								$sriDetailsArray=array(
									'backimg' => "$sriBackgroundjson",
								    'advlogo' => "$advLogojson",
									'srititles' => array("$shareTitle","$respondTitle","$interactTitle"),
									'sriorder' => array("share", "respond", "interact"),
                					'alpha' => "0.5");
								$sriDetailsJson = mysql_real_escape_string(json_encode($sriDetailsArray));
								$sriDetailsJson=" sri_details='$sriDetailsJson', ";
							}else{
								$sriDetailsJson="";
							}
							$sriBackgroundjson='';
							$advLogojson='';
							if(isset($ad_details) && $ad_details!="") $sqlAd_details=", ad_details='$ad_details'"; else $sqlAd_details=", ad_details=NULL";
							if(rename($flvsrc, $flvdest) && rename($thmbsrc, $thmbdest)) {
										$sql="insert into ads set ad_format='video',																					advertiser_id=$_SESSION[ADV_ID],
											playlist_ad_ref='$flvfile',
											branded_img_right_ref_imgname='$name',
											destination_url='$desturl',
											tracker_url='$trackurl',
											dimension='$dimension',
											duration='$duration',
											$sriDetailsJson
											branded_color='$campaniontype',
											branded_img_bot_ref_txbody='$branded_img_bot_ref_txbody' $sqlAd_details";
								

								$rs=$conn->execute($sql);
								if(!$rs) {
									rename($flvdest,$flvsrc);
									rename($thmbdest,$thmbsrc);
								}
								$sriDetailsJson='';
							}
							if($rs)
							{
								$ad=$conn->Insert_ID();
								$parent_ad_id=$ad;
								if(sizeof($sriads)>0){
									$var=new SRIPortal();
									$var->insertSRI($sriads,$parent_ad_id, $_SESSION[ADV_ID], $cid);
								}
								$type='video';
								exec("./s3-upload.sh $flvdest $config[s3_bucket_name] ". $config['s3directory'][$type]);
							}
							else
							$ad=-1;

							if ($ad < 0) {
								$cleanup=1;
								break;
							}
							$adid[]=$ad;
						}	
					} else
					$cleanup=1;
					break;
                                        
                                case 'ytvideoadvertisement':
                                    if($campaign['ytvideoads'] != "") {
                                            include_once(dirname(__FILE__).'/library/class.youtubeads.php');
                                            $ytOBJ = new YoutubeAds();
                                            $adid = array();
                                            $ad = $ytOBJ->process(&$campaign, &$adid_already);
                                            if ($ad < 0) {
                                                    $cleanup=1;
                                                    break;
                                            }
                                            $adid[]=$ad;
                                    } else
                                            $cleanup=1;
                                    break;
                                    
                                    
                                    
                                    
				case 'vastfeedvideoadvertisement':
                  include_once(dirname(__FILE__).'/library/VastFeedAd.php');
                  $vastAdObj = new VastFeedAds();
                  $parameters = $vastAdObj->insertVastFeedAd($cid);
                  list($adid_already, $cleanup, $adid) = $parameters;
				  break;  	
				case 'brandedplayer':
					$brandedads=$campaign['brandedads'];
					if($brandedads != "") {
            			error_log(print_r($brandedads, true));
						foreach ($brandedads as $adrow) {
							if($adrow['id']!="" ||$adrow['imgurl']=="") {
								$adid_already[]=$adrow['id'];
								continue;
							}
							$name		= strip_tags($adrow['name']);
							$frompath1	= $config['tempfilepath']."/".$adrow['imgurl'];
							$imgfile1	= md5($adrow['imgurl'].rand(0,1000000000));
							$topath1	= $config['AD_IMG_DIR']."/$imgfile1";
							$toswfpath1	= $config['AD_SWF_DIR']."/$imgfile1.swf";
							$skinpath	= $config['AD_SWF_URL']."/$imgfile1.swf";
							$desturl	= $adrow['desturl'];
							$trackurl	= $adrow['trackurl'];
							$dimension	= strip_tags($adrow['dimension']);
							$rs=false;
              				error_log("rename($frompath1,$toswfpath1)");
							if(rename($frompath1,$toswfpath1)) {
								$sql= $conn->Prepare("insert into ads set ad_format='branded',
											advertiser_id=?,
											playlist_ad_ref=?,
											branded_img_right_ref_imgname=?,
											dimension=?,
											destination_url=?,
											tracker_url=?");
								$rs=$conn->execute($sql, array($_SESSION[ADV_ID], $skinpath, $name, $dimension, $desturl, $trackurl));
								if(!$rs) {
									rename($toswfpath1,$frompath1.".swf");
									//rename($topath1,$frompath1);
									//rename($topath2,$frompath2);
									//rename($toswfpath1,$frompath1.".swf");
									//rename($toswfpath2,$frompath2.".swf");
								}
							}
							if($rs){
								$ad=$conn->Insert_ID();
								/*$type='image';
								exec("./s3-upload.sh $topath1 $config[s3_bucket_name] ". $config['s3directory'][$type]);
								exec("./s3-upload.sh $topath2 $config[s3_bucket_name] ". $config['s3directory'][$type]);*/
								$type='swf';
								exec("./s3-upload.sh $toswfpath1 $config[s3_bucket_name] ". $config['s3directory'][$type]);
//								exec("./s3-upload.sh $toswfpath2 $config[s3_bucket_name] ". $config['s3directory'][$type]);

							}
							else
							$ad=-1;
							if ($ad < 0) {
								$cleanup=1;
								break;
							}
							$adid[]=$ad;
						}
					}else
					$cleanup=1;
					break;
				case 'overlayadvertisement':
					switch ($campaign['subtype']) {
						case 'textadvertisement':
							$textads=$campaign['textads'];
							if($textads != "") {
								foreach ($textads as $adrow) {
									if($adrow['id']!="") {
										$adid_already[]=$adrow['id'];
										continue;
									}
									$title	= strip_tags($adrow['title']);
									$body	= strip_tags($adrow['body']);
									$desturl= $adrow['desturl'];
									$trackurl= $adrow['trackurl'];
									$sql= $conn->Prepare("insert into ads set ad_format='overlay',													advertiser_id=$_SESSION[ADV_ID],
											branded_img_top_ref_txtitle=?,
											branded_img_bot_ref_txbody=?,
											destination_url=?,
											tracker_url=?");
									$rs= $conn->execute($sql, array($title, $body, $desturl, $trackurl));
									exit;
									if($rs)
									{
									$ad=$conn->Insert_ID();
									}
									else
									$ad=-1;
									if ($ad < 0) {
										$cleanup=1;
										break;
									}
									$adid[]=$ad;
								}
							}else
							$cleanup=1;
							break;
						case 'banneradvertisement':
							$bannerads=$campaign['bannerads'];
							if($bannerads != "") {
								foreach ($bannerads as $adrow) {

									if($adrow['id']!="") {
										$adid_already[]=$adrow['id'];
										continue;
									}
									$frompath	= $config['tempfilepath']."/".$adrow['imgurl'];
									$imgfile	= md5($adrow['imgurl'].rand(0,1000000000));
									$topath		= $config['AD_IMG_DIR']."/$imgfile";
									$toswfpath	= $config['AD_SWF_DIR']."/$imgfile.swf";

									$name		= strip_tags($adrow['imgname']);
									$desturl	= $adrow['desturl'];
									$trackurl	= $adrow['trackurl'];
									$rs=false;
									if(rename($frompath,$topath) && rename($frompath.".swf",$toswfpath)) {
										$sql= $conn->Prepare("insert into ads set ad_format='overlay',
											advertiser_id=?,
											playlist_ad_ref=?,
											branded_img_right_ref_imgname=?,
											destination_url=?,
											tracker_url=?");
										$rs=$conn->execute($sql, array($_SESSION[ADV_ID], $imgfile, $name, $desturl, $trackurl));
										if(!$rs){
											rename($topath,$frompath);
											rename($toswfpath,$frompath.".swf");
										}
									}
									if($rs)
									{
										$ad=$conn->Insert_ID();
										$type='image';
										exec("./s3-upload.sh $topath $config[s3_bucket_name] ". $config['s3directory'][$type]);
										$type='swf';
										exec("./s3-upload.sh $toswfpath $config[s3_bucket_name] ". $config['s3directory'][$type]);
									}
										else
									$ad=-1;
									if ($ad < 0) {
										$cleanup=1;
										break;
									}
									$adid[]=$ad;
								}
							}else
							$cleanup=1;
							break;
							//============overlay swf start==================
							case 'overlayswf':
							$overlayswfs=$campaign['overlayswfs'];
//							echo "<pre>";
//							print_r($overlayswfs);
//							exit;
							if($overlayswfs!= "") {
								foreach ($overlayswfs as $adrow) {

									if($adrow['id']!="") {
										$adid_already[]=$adrow['id'];
										continue;
									}
									//$extension=mysql_escape_string(strip_tags($adrow['extension']));
									$frompath	= $config['tempfilepath']."/".$adrow['imgurl'];
									$imgfile 	= md5($adrow['imgurl'].rand(0,1000000000));
									//$topath=$config['AD_IMG_DIR']."/$imgfile.$extension";
									$toswfpath	= $config['AD_SWF_DIR']."/$imgfile.swf";

									$name		= strip_tags($adrow['imgname']);
									$dimension	= strip_tags($adrow['imgsize']);
									$desturl	= strip_tags($adrow['desturl']);
									$trackurl	= $adrow['trackurl'];
									$rs=false;
									
									if(rename($frompath, $toswfpath)) {
										$sql=$conn->Prepare("insert into ads set ad_format='overlay',
											advertiser_id=?,
											playlist_ad_ref=?,
											branded_img_right_ref_imgname=?,
											destination_url=?,
											tracker_url=?");
										$rs=$conn->execute($sql, array($_SESSION[ADV_ID], $imgfile, $name, $desturl, $trackurl));
										if(!$rs){
											//rename($topath,$frompath);
											rename($toswfpath,$frompath.".swf");
										}
									}

									if($rs)
									{
										$ad=$conn->Insert_ID();
										/*$type='image';
										exec("./s3-upload.sh $topath $config[s3_bucket_name] ". $config['s3directory'][$type]);*/
										$type='swf';
										exec("./s3-upload.sh $toswfpath $config[s3_bucket_name] ". $config['s3directory'][$type]);
									}
										else
									$ad=-1;
									if ($ad < 0) {
										$cleanup=1;
										break;
									}
									$adid[]=$ad;
								}
							}else
							$cleanup=1;
							break;
							//========overlay swf end =============
						default:
							$cleanup=1;
					}
					break;
				case 'trackeradvertisement':
					switch ($campaign['trktype']) {
            			case 'extswf':
            			case 'extimage':
							$trackerinfo=$campaign['extinfo'];
							if($campaign['richmedia']=='richmedia') $adformat='banner'; else $adformat='tracker';
							if($trackerinfo!= "") {
								foreach ($trackerinfo as  $adrow) {

									if($adrow['id']!="") {
										$adid_already[]=$adrow['id'];
										continue;
									}
									$imgfile	= $adrow['imgurl'];
									$name		= strip_tags($adrow['imgname']);
									$dimension	= strip_tags($adrow['imgsize']);
									$desturl	= $adrow['desturl'];
									$trackurl	= $adrow['trackurl'];
									$rs=false;
									
                  $sql=$conn->Prepare("insert into ads set ad_format=?,
                    advertiser_id=?,
                    tracker_url=?,
                    branded_img_right_ref_imgname=?,
                    dimension=?,
                    destination_url=?");
										$rs=$conn->execute($sql, array($adformat, $_SESSION[ADV_ID], $imgfile, $name, $dimension, $desturl));
//										if(!$rs){
//											//rename($topath,$frompath);
//											rename($toswfpath,$frompath.".swf");
//										}

									if($rs) {
										$ad=$conn->Insert_ID();
									}
									else
									  $ad=-1;
									if ($ad < 0) {
										$cleanup=1;
										break;
									}
									$adid[]=$ad;
								}
							}else
							$cleanup=1;
							break;
						case 'dblclik':
							$campaign=$_SESSION['CAMPAIGN'];
							$dblcliktags=$campaign['dblcliktags'];
							if($campaign['richmedia']=='richmedia') $adformat='banner'; else $adformat='tracker';
							if($dblcliktags != "") {	
								foreach($dblcliktags as $adrow){
									if($adrow['id']!="") {
										$adid_already[]=$adrow['id'];
										continue;
									}
									
									$sql=$conn->Prepare("insert into ads set ad_format=?,
										advertiser_id=?,
										playlist_ad_ref='',
										tracker_url=?,
										branded_img_right_ref_imgname=?,
										dimension=?,
										destination_url=?");
									
									$rs=$conn->execute($sql, array($adformat, $_SESSION[ADV_ID], $adrow[tracker_url], $adrow[imgname], $adrow[imgsize], $adrow[desturl]));
									if($rs){
										$ad=$conn->Insert_ID();
									}
									else{
										$ad=-1;
									}
									
									if ($ad < 0) {
										$cleanup=1;
										break;
									}
									$adid[]=$ad;
								}
							}else{
								$cleanup=1;
							}
							break;	
						case 'trackerswf':
							$trackerswfs=$campaign['trackerswfs'];
							if($campaign['richmedia']=='richmedia') $adformat='banner'; else $adformat='tracker';
							if($trackerswfs != "") {
								foreach ($trackerswfs as $adrow) {

									if($adrow['id']!="") {
										$adid_already[]=$adrow['id'];
										continue;
									}
									$extension	= strip_tags($adrow['extension']);
									$frompath	= $config['tempfilepath']."/".$adrow['imgurl'];
									$imgfile	= md5($adrow['imgurl'].rand(0,1000000000));
									$toswfpath	= $config['AD_SWF_DIR']."/$imgfile.$extension";
									
									if($adrow['bckimageurl']!=""){
									  $frombckimageurl	= $config['tempfilepath']."/".$adrow['bckimageurl'];
									  $extnsn			= explode('.',$frombckimageurl);
									  $tobckimageurl	= $config['AD_IMG_DIR']."/$imgfile".".".$extnsn[count($extnsn)-1];
									  rename($frombckimageurl, $tobckimageurl);
									  $snip=" branded_img_left_ref='$imgfile.".$extnsn[count($extnsn)-1]."', ";
									}
									

									$name		= strip_tags($adrow['imgname']);
									$dimension	= strip_tags($adrow['imgsize']);
									$desturl	= $adrow['desturl'];
									$trackurl	= $adrow['trackurl'];
									$rs=false;
									
									if(rename($frompath, $toswfpath)) {
									$sql="insert into ads set ad_format='$adformat',
											advertiser_id=$_SESSION[ADV_ID],
											playlist_ad_ref='$imgfile.$extension',
											$snip
											branded_img_right_ref_imgname='$name',
											dimension='$dimension',
											destination_url='$desturl',
											tracker_url='$trackurl'";
										$rs=$conn->execute($sql);
										if(!$rs){
											//rename($topath,$frompath);
											rename($toswfpath,$frompath.".swf");
										}
									}

									if($rs)
									{
										$ad=$conn->Insert_ID();
										/*$type='image';
										exec("./s3-upload.sh $topath $config[s3_bucket_name] ". $config['s3directory'][$type]);*/
										$type='swf';
										exec("./s3-upload.sh $toswfpath $config[s3_bucket_name] ". $config['s3directory'][$type]);
									}
										else
									$ad=-1;
									if ($ad < 0) {
										$cleanup=1;
										break;
									}
									$adid[]=$ad;
								}
							}else
							$cleanup=1;
							break;
						case 'trackerimage':
							$trackerimages=$campaign['trackerimages'];
							if($campaign['richmedia']=='richmedia') $adformat='banner'; else $adformat='tracker';
							if($trackerimages != "") {
								foreach ($trackerimages as $adrow) {

									if($adrow['id']!="") {
										$adid_already[]=$adrow['id'];
										continue;
									}
									$extension	= strip_tags($adrow['extension']);
									$frompath	= $config['tempfilepath']."/".$adrow['imgurl'];
									$imgfile	= md5($adrow['imgurl'].rand(0,1000000000));
									$topath		= $config['AD_IMG_DIR']."/$imgfile.$extension";
									$toswfpath	= $config['AD_SWF_DIR']."/$imgfile.swf";

									$name		= strip_tags($adrow['imgname']);
									$dimension  = strip_tags($adrow['imgsize']);
									$desturl	= $adrow['desturl'];
									$trackurl	= $adrow['trackurl'];
									$rs=false;
									if(rename($frompath,$topath) && rename($frompath.".swf",$toswfpath)) {
									 $imgPATH = $imgfile.".".$extension;
										 $sql=$conn->Prepare("insert into ads set ad_format=?,
											advertiser_id=?,
											playlist_ad_ref=?,
											branded_img_right_ref_imgname=?,
											dimension=?,
											destination_url=?,
											tracker_url=?");
										$rs=$conn->execute($sql, array($adformat, $_SESSION[ADV_ID], $imgPATH, $name, $dimension, $desturl, $trackurl));
										if(!$rs){
											rename($topath,$frompath);
											rename($toswfpath,$frompath.".swf");
										}
									}
								
									if($rs)
									{
										$ad=$conn->Insert_ID();
										$type='image';
										exec("./s3-upload.sh $topath $config[s3_bucket_name] ". $config['s3directory'][$type]);
										$type='swf';
										exec("./s3-upload.sh $toswfpath $config[s3_bucket_name] ". $config['s3directory'][$type]);
									}else{
										$ad=-1;
									}
									if ($ad < 0) {
										$cleanup=1;
										break;
									}
									$adid[]=$ad;
								}
							}else
							$cleanup=1;
							break;
						case 'videobanner':
							$videobannerads=$campaign['videobannerads'];
							$sriads=$campaign['sriads'];
							if($campaign['richmedia']=='richmedia') $adformat='vdobanner'; else $adformat='tracker';
							if($campaign['backgroundbannerurl']!="") $sriBackground=$campaign['backgroundbannerurl'];
							if($campaign['advlogourl']!="") $advLogo=$campaign['advlogourl'];
							if($campaign['isMuted']!="") $isMuted=$campaign['isMuted'];
							if($campaign['autoplay']!="") $autoplay=$campaign['autoplay'];
							if($campaign['gridview']!="") $gridview=$campaign['gridview'];
							if($campaign['mouseover']!="") $mouseover=$campaign['mouseover'];
							if($campaign['shareTitle']!="") $shareTitle=$campaign['shareTitle'];
							if($campaign['respondTitle']!="") $respondTitle=$campaign['respondTitle'];
							if($campaign['interactTitle']!="") $interactTitle=$campaign['interactTitle'];
							if($videobannerads != "") { 
								foreach ($videobannerads as $adrow) {
									if($adrow['id']!="") {
										$adid_already[]=$adrow['id'];
										$sql=$conn->Prepare("SELECT sri_details FROM ads WHERE id=?");
										$rs=$conn->execute($sql, array($adrow[id]));
										if($rs && $rs->recordcount()>0){
											$objJason=json_decode($rs->fields['sri_details']);
											$fileArr=explode('/',$objJson->backimg);
											$bannerURL = $fileArr[sizeof($fileArr)-1];
											
											# background banner image
											if($sriBackground!=$bannerURL){
												if($sriBackground!=""){
													$adbase=md5('imgurl'.rand(0,1000000000));
													$sriBackgroundsrc="$config[tempfilepath]/$sriBackground";
													$ext=explode('.',$sriBackground);
													$sriBackgrounddest="$config[AD_IMG_DIR]/$adbase"."bck.".$ext[count($ext)-1];
													$sriBackgroundjson="$config[CDN_URL]/files/images/$adbase"."bck.".$ext[count($ext)-1];
													if(file_exists("$config[AD_IMG_DIR]/$sriBackground")){
														copy("$config[AD_IMG_DIR]/$sriBackground", $sriBackgrounddest);
													}else{
														copy($sriBackgroundsrc, $sriBackgrounddest);
													}
												}
											}else{
												$sriBackgroundjson=$objJson->backimg;
											}
											
											$fileArr=explode('/',$objJson->advlogo);
											$logoURL = $fileArr[sizeof($fileArr)-1];
											
										    # Advertiser Logo
											if($advLogo!=$logoURL){
												if($advLogo!=""){
													$adbase=md5('imgurl'.rand(0,1000000000));
													$advLogosrc="$config[tempfilepath]/$advLogo";
													$ext=explode('.',$advLogo);
													$advLogodest="$config[AD_IMG_DIR]/$adbase"."logo.".$ext[count($ext)-1];
													$advLogojson="$config[CDN_URL]/files/images/$adbase"."logo.".$ext[count($ext)-1];
													if(file_exists("$config[AD_IMG_DIR]/$advLogo")){
														copy("$config[AD_IMG_DIR]/$advLogo", $advLogodest);
													}else{
														copy($advLogosrc, $advLogodest);
													}
												}
											}else{
												$advLogojson=$objJson->advlogo;
											}
											
											if($shareTitle=="") $shareTitle=$objJason->srititles[0];
											if($respondTitle=="") $respondTitle=$objJason->srititles[1];
											if($interactTitle=="") $interactTitle=$objJason->srititles[2];
											$sriDetailsArray=array(
												'backimg' => "$sriBackgroundjson",
											    'advlogo' => "$advLogojson",
												'isMuted' => "$isMuted",
												'autoplay' => "$autoplay",
											    'gridview' => "$gridview",
											    'mouseover' => "$mouseover",
												'srititles' => array("$shareTitle","$respondTitle","$interactTitle"),
												'sriorder' => array("share", "respond", "interact"),
                								'alpha' => 0.5);
												$sriDetailsJson = json_encode($sriDetailsArray);
											
											$sql= $conn->Prepare("UPDATE ads SET sri_details=? WHERE id=?");
											$rsUpdate=$conn->execute($sql, array($sriDetailsJson, $adrow[id]));
											if($rsUpdate){
												$sql= $conn->Prepare("update campaign set review_status='Pending Review' where id=?");
												$rs=$conn->execute($sql, array($campaign[id]));
											}
										}
										if(sizeof($sriads)>0){
											$var=new SRIPortal();
											$var->updateSRI($sriads,$adrow[id], $_SESSION[ADV_ID], $campaign[id]);
										}
										continue;
									}
									
									$branded_img_bot_ref_txbody ="";
									$uri=mysql_escape_string(escapeshellcmd(strip_tags($adrow['uri'])));
									$videobannerurl=mysql_escape_string(escapeshellcmd(strip_tags($adrow['videobannerurl'])));
									$name=mysql_escape_string(strip_tags($adrow['name']));
									$dimension = '300x250'; //mysql_escape_string(strip_tags($adrow['dimension']));
									$desturl=mysql_escape_string($adrow['desturl']);
									$trackurl=mysql_escape_string($adrow['trackurl']);
									if(isset($adrow['vi_0'])&&$adrow['vi_0']!="") $ad_details["vdo_tvi_0"]=mysql_escape_string(strip_tags($adrow['vi_0']));
									if(isset($adrow['vi_25'])&&$adrow['vi_25']!="") $ad_details["vdo_tvi_25"]=mysql_escape_string(strip_tags($adrow['vi_25']));
									if(isset($adrow['vi_50'])&&$adrow['vi_50']!="") $ad_details["vdo_tvi_50"]=mysql_escape_string(strip_tags($adrow['vi_50']));
									if(isset($adrow['vi_75'])&&$adrow['vi_75']!="") $ad_details["vdo_tvi_75"]=mysql_escape_string(strip_tags($adrow['vi_75']));
									if(isset($adrow['ae'])&&$adrow['ae']!="") $ad_details["vdo_tae"]=mysql_escape_string(strip_tags($adrow['ae']));
									if(sizeof($ad_details)>0){
										$ad_details = json_encode($ad_details);
										$ad_details = str_replace('\/','/',$ad_details);
									}else $ad_details='';
							
									$flvsrc="$config[tempfilepath]/$uri.flv";
									$thmbsrc="$config[tempfilepath]/thmb$uri.jpg";
									$bannersrc="$config[tempfilepath]/$videobannerurl";
									$bannerExt=explode('.',$videobannerurl);
									$adbase=md5($adrow['imgurl'].rand(0,1000000000));
									$flvfile=$adbase.".flv";
									$bannerfile=$adbase."bnr.".$bannerExt[count($bannerExt)-1];
									$thmbdest="$config[AD_IMG_DIR]/$adbase.jpg";
									$bannerdest="$config[AD_IMG_DIR]/$bannerfile";
									$flvdest="$config[AD_VDO_DIR]/$flvfile";
									$rs=false;
									
									# background banner url
									$sriBackgroundsrc="$config[tempfilepath]/$sriBackground";
									if($sriBackground!=""){
										$ext=explode('.',$sriBackground);
										$sriBackgrounddest="$config[AD_IMG_DIR]/$adbase"."bck.".$ext[count($ext)-1];
										$sriBackgroundjson="$config[CDN_URL]/files/images/$adbase"."bck.".$ext[count($ext)-1];
										if(file_exists("$config[AD_IMG_DIR]/$sriBackground")){
											copy("$config[AD_IMG_DIR]/$sriBackground", $sriBackgrounddest);
										}else{
											copy($sriBackgroundsrc, $sriBackgrounddest);
										}
									}
									
								    # background banner url
									$advLogosrc="$config[tempfilepath]/$advLogo";
									if($advLogo!=""){
										$ext=explode('.',$sriBackground);
										$advLogodest="$config[AD_IMG_DIR]/$adbase"."logo.".$ext[count($ext)-1];
										$advLogojson="$config[CDN_URL]/files/images/$adbase"."logo.".$ext[count($ext)-1];
										if(file_exists("$config[AD_IMG_DIR]/$advLogo")){
											copy("$config[AD_IMG_DIR]/$advLogo", $advLogodest);
										}else{
											copy($advLogosrc, $advLogodest);
										}
									}
									
									
									if($sriBackgroundsrc!=""  && $shareTitle!="" && $respondTitle!="" && $interactTitle!="") {
										$sriDetailsArray=array(
											'backimg' => "$sriBackgroundjson",
											'advlogo' => "$advLogojson",
											'isMuted' => "$isMuted",
											'autoplay' => "$autoplay",
										    'gridview' => "$gridview",
											'mouseover' => "$mouseover",
											'srititles' => array("$shareTitle","$respondTitle","$interactTitle"),
											'sriorder' => array("share", "respond", "interact"),
                							'alpha' => 0.5);
										$sriDetailsJson = mysql_real_escape_string(json_encode($sriDetailsArray));
									}else{
									    $sriDetailsArray=array(
											'isMuted' => "$isMuted",
											'autoplay' => "$autoplay",
									    	'gridview' => "$gridview",
											'mouseover' => "$mouseover");
										$sriDetailsJson = mysql_real_escape_string(json_encode($sriDetailsArray));
									}
								
									if(!$sriDetailsJson) $sriDetailsJson=''; else $sriDetailsJson=" sri_details='$sriDetailsJson', "; 
									$topbannerjson='';
									$bottombannerjson='';
									$sriBackgroundjson='';
									$advLogojson='';
									if(isset($ad_details) && $ad_details!="") $sqlAd_details=", ad_details='$ad_details'"; else $sqlAd_details=", ad_details=NULL";
									if(rename($flvsrc, $flvdest) && rename($thmbsrc, $thmbdest) && rename($bannersrc, $bannerdest)) {
										$sql="insert into ads set ad_format='$adformat',																					
										advertiser_id=$_SESSION[ADV_ID],
										playlist_ad_ref='$flvfile',
										branded_img_right_ref_imgname='$name',
										destination_url='$desturl',
										tracker_url='$trackurl',
										dimension='$dimension', 
										$sriDetailsJson
										branded_logo_ref='$bannerfile' $sqlAd_details";
										
										$rs=$conn->execute($sql);
										if(!$rs) {
											rename($flvdest,$flvsrc);
											rename($thmbdest,$thmbsrc);
											rename($bannerdest,$bannersrc);
										}
										$sriDetailsJson='';
									}
									
									
									if($rs)
									{
										$ad=$conn->Insert_ID();
										$parent_ad_id=$ad;
										if(sizeof($sriads)>0){
											$var=new SRIPortal();
											$srielements=$_SESSION ['CAMPAIGN'] ['sriads'];
											$var->insertSRI($srielements,$parent_ad_id, $_SESSION[ADV_ID], $cid);		}
										$type='video';
										exec("./s3-upload.sh $flvdest $config[s3_bucket_name] ". $config['s3directory'][$type]);
									}
									else
										$ad=-1;
	
									if ($ad < 0) {
										$cleanup=1;
										break;
									}
									$adid[]=$ad;
								}
							}else
							$cleanup=1;
							break;	
							case 'youtubevb':   
								$adformat='tracker';  
								if($campaign['youtubevbads'] != "") {
									include_once(dirname(__FILE__).'/library/class.youtubevb.php');
									$ytOBJ = new YoutubeVBAds();
									$adid = array();
									$ad = $ytOBJ->processYoutubevbAd(&$campaign, $adformat, &$adid_already);
									if ($ad < 0) {
										$cleanup=1;
										break;
									}
									$adid[]=$ad;
								} else
									$cleanup=1;
								break;
						default:
							$cleanup=1;
					}
					break;
				default:
					$cleanup=1;
			}
			checkAdDirtyAndUpdate($cid, $adid, $adid_already);
			emptyCampaign($cid, $adid_already);
			
			function updateDirtyAds(&$campaign) {
				global $conn;
				switch ($campaign['type']) {
					case "videoadvertisement":
						$videoads = $campaign['videoads'];
						foreach( $videoads as $index => $value ){
							# check, if dirty flag is on
							if($value['dirty']==true )
							{
								$sqlstr='';
								foreach($value as $key => $val ){
									if( $key=="name" )
										$sqlstr.="branded_img_right_ref_imgname='$val',";
									elseif($key=="desturl" )
										$sqlstr.="destination_url='$val',";
									elseif($key=="trackurl" )
										$sqlstr.="tracker_url='$val',";
									elseif($key=="vi_0"){
										if(isset($val) && $val!="") $ad_details['vdo_tvi_0']=$val;	
									}elseif($key=="vi_25"){
										if(isset($val) && $val!="") $ad_details['vdo_tvi_25']=$val;
									}elseif($key=="vi_50"){
										if(isset($val) && $val!="") $ad_details['vdo_tvi_50']=$val;
									}elseif($key=="vi_75"){
										if(isset($val) && $val!="") $ad_details['vdo_tvi_75']=$val;
									}elseif($key=="ae"){
										if(isset($val) && $val!="") $ad_details['vdo_tae']=$val;
									}	
								}
								if(isset($ad_details) && sizeof($ad_details)>0){
									$ad_details = json_encode($ad_details);
									$ad_details = str_replace('\/','/',$ad_details);
								}else $ad_details='';
								if($ad_details!='') $sqlstr.="ad_details='$ad_details',"; else $sqlstr.="ad_details=NULL";
								if($sqlstr!=""){
									$sql="update ads set ".trim($sqlstr,',')." where id='$value[id]' and advertiser_id='$_SESSION[ADV_ID]'";
									$conn->execute($sql);
									$ad_details = array();
								}
							}
						}
						break;
					case "vastfeedvideoadvertisement":
					  include_once(dirname(__FILE__).'/library/VastFeedAd.php');
					  $vastfeedads = $campaign['vastfeedads'];
				      $vastAdObj = new VastFeedAds();
				      $vastAdObj->updateAds($vastfeedads);
					  break;
                                      case 'ytvideoadvertisement':
									include_once(dirname(__FILE__).'/library/class.youtubeads.php');
									$ytOBJ = new YoutubeAds();
									$ytOBJ->updateYoutubeAds(&$campaign);
									break;	
					case 'brandedplayer':
						$brandedads = $campaign['brandedads'];
						foreach( $brandedads as $index_key => $value ){
							# check, if dirty flag is on
							if($value['dirty']==true )
							{
								$sqlstr='';
								foreach($value as $key => $val ){
									if( $key=="name" )
										$sqlstr.="branded_img_right_ref_imgname='$val',";
									elseif($key=="desturl" )
										$sqlstr.="destination_url='$val',";
									elseif($key=="trackurl" )
										$sqlstr.="tracker_url='$val',";
									elseif($key=="imgsize" )
										$sqlstr.="dimension='$val',";
									elseif($key=="dimession" )
										$sqlstr.="dimension='$val',";
									elseif($key=="swfid" )
										$sqlstr.="branded_img_bot_ref_txbody='$val',";
								}
								
								if($sqlstr!=""){
									$sql="update ads set ".trim($sqlstr,',')." where id='$value[id]' and advertiser_id='$_SESSION[ADV_ID]'";
									$conn->execute($sql);
								}
							}
						}
						break;
					case 'overlayadvertisement': {
						switch ($campaign['subtype']) {
							case 'textadvertisement':
								$textads = $campaign['textads'];
								foreach( $textads as $index_key => $value ){
									# check, if dirty flag is on
									if($value['dirty']==true )
									{
										$sqlstr='';
										foreach($value as $key => $val ){
											if( $key=="name" )
												$sqlstr.="branded_img_right_ref_imgname='$val',";
											elseif($key=="desturl" )
												$sqlstr.="destination_url='$val',";
											elseif($key=="trackurl" )
												$sqlstr.="tracker_url='$val',";
										}
										
										if($sqlstr!=""){
											$sql="update ads set ".trim($sqlstr,',')." where id='$value[id]' and advertiser_id='$_SESSION[ADV_ID]'";
											$conn->execute($sql);
										}
									}
								}
								break;
							case 'banneradvertisement':
								$bannerads = $campaign['bannerads'];
								foreach( $bannerads as $index_key => $value ){
									# check, if dirty flag is on
									if($value['dirty']==true )
									{
										$sqlstr='';
										foreach($value as $key => $val ){
											if( $key=="imgname" )
												$sqlstr.="branded_img_right_ref_imgname='$val',";
											elseif($key=="desturl" )
												$sqlstr.="destination_url='$val',";
											elseif($key=="trackurl" )
												$sqlstr.="tracker_url='$val',";
										}
										
										if($sqlstr!=""){
											$sql="update ads set ".trim($sqlstr,',')." where id='$value[id]' and advertiser_id='$_SESSION[ADV_ID]'";
											$conn->execute($sql);
										}
									}
								}
								break;
								case 'overlayswf':
								$overlayswfs = $campaign['overlayswfs'];
//								echo "<pre>";
//								print_r($overlayswfs);
//								exit;
								foreach( $overlayswfs as $index_key => $value ){
									# check, if dirty flag is on
									if($value['dirty']==true )
									{
										$sqlstr='';
										foreach($value as $key => $val ){
											if( $key=="imgname" )
												$sqlstr.="branded_img_right_ref_imgname='$val',";
											elseif($key=="desturl" )
												$sqlstr.="destination_url='$val',";
											elseif($key=="trackurl" )
												$sqlstr.="tracker_url='$val',";
										}
										
										if($sqlstr!=""){
											$sql="update ads set ".trim($sqlstr,',')." where id='$value[id]' and advertiser_id='$_SESSION[ADV_ID]'";
											$conn->execute($sql);
										}
									}
								}
								break;
						}
					}
					break;
				case 'trackeradvertisement': {
						switch ($campaign['trktype']) {
							case 'videobanner':
								$videobannerads = $campaign['videobannerads'];
								foreach( $videobannerads as $index_key => $value ){
									# check, if dirty flag is on
									if($value['dirty']==true )
									{
										$sqlstr='';
										foreach($value as $key => $val ){
											if( $key=="name" ){
												$sqlstr.="branded_img_right_ref_imgname='$val',";
											}elseif($key=="desturl" ){
												$sqlstr.="destination_url='$val',";
											}elseif($key=="trackurl" ){
												$sqlstr.="tracker_url='$val',";
											}elseif($key=="vi_0"){
												if(isset($val) && $val!="") $ad_details['vdo_tvi_0']=$val;	
											}elseif($key=="vi_25"){
												if(isset($val) && $val!="") $ad_details['vdo_tvi_25']=$val;
											}elseif($key=="vi_50"){
												if(isset($val) && $val!="") $ad_details['vdo_tvi_50']=$val;
											}elseif($key=="vi_75"){
												if(isset($val) && $val!="") $ad_details['vdo_tvi_75']=$val;
											}elseif($key=="ae"){
												if(isset($val) && $val!="") $ad_details['vdo_tae']=$val;
											}	
										}
										if(isset($ad_details) && sizeof($ad_details)>0){
											$ad_details = json_encode($ad_details);
											$ad_details = str_replace('\/','/',$ad_details);
										}else $ad_details='';
										if($ad_details!='') $sqlstr.="ad_details='$ad_details',"; else $sqlstr.="ad_details=NULL";
										if($sqlstr!=""){
											$sql="update ads set ".trim($sqlstr,',')." where id='$value[id]' and advertiser_id='$_SESSION[ADV_ID]'";
											$conn->execute($sql);
										}
									}
								}
								break;
								case 'youtubevb':
									include_once(dirname(__FILE__).'/library/class.youtubevb.php');
									$ytOBJ = new YoutubeVBAds();
									$ytOBJ->updateYoutubeAds(&$campaign);
									break;		
							case 'trackerswf':
								$trackerswfs = $campaign['trackerswfs'];
								foreach( $trackerswfs as $index_key => $value ){
									# check, if dirty flag is on
									if($value['dirty']==true )
									{
										$sqlstr='';
										foreach($value as $key => $val ){
											if( $key=="imgname" )
												$sqlstr.="branded_img_right_ref_imgname='$val',";
											elseif($key=="desturl" )
												$sqlstr.="destination_url='$val',";
											elseif($key=="trackurl" )
									            $sqlstr.="tracker_url='$val',";	
										}
										
										if($sqlstr!=""){
											$sql="update ads set ".trim($sqlstr,',')." where id='$value[id]' and advertiser_id='$_SESSION[ADV_ID]'";
											$conn->execute($sql);
										}
									}
								}
								break;
							case 'trackerimage':
								$trackerimages = $campaign['trackerimages'];
								foreach( $trackerimages as $index_key => $value ){
									# check, if dirty flag is on
									if($value['dirty']==true )
									{
										$sqlstr='';
										foreach($value as $key => $val ){
											if( $key=="imgname" )
												$sqlstr.="branded_img_right_ref_imgname='$val',";
											elseif($key=="desturl" )
												$sqlstr.="destination_url='$val',";
											elseif($key=="trackurl" )
									            $sqlstr.="tracker_url='$val',";	
										}
										
										if($sqlstr!=""){
											$sql="update ads set ".trim($sqlstr,',')." where id='$value[id]' and advertiser_id='$_SESSION[ADV_ID]'";
											$conn->execute($sql);
										}
									}
								}
								break;
                case 'extswf':
                case 'extimage':
					$trackerinfo = $campaign['extinfo'];
					foreach( $trackerinfo as $index_key => $value ){
						# check, if dirty flag is on
						if($value['dirty']==true )
						{
							$sqlstr='';
							foreach($value as $key => $val ){
								if( $key=="imgname" )
									$sqlstr.="branded_img_right_ref_imgname='$val',";
								elseif($key=="desturl" )
									$sqlstr.="destination_url='$val',";
								elseif($key=="trackurl" )
									$sqlstr.="tracker_url='$val',";	
							}
							
							if($sqlstr!=""){
								$sql="update ads set ".trim($sqlstr,',')." where id='$value[id]' and advertiser_id='$_SESSION[ADV_ID]'";
								$conn->execute($sql);
							}
						}
					}
					break;
				/*case 'extimage':
					$trackerinfo = $campaign['extinfo'];
					foreach( $trackerinfo as $index_key => $value ){
						# check, if dirty flag is on
						if($value['dirty']==true )
						{
							$sqlstr='';
							foreach($value as $key => $val ){
								if( $key=="imgname" )
									$sqlstr.="branded_img_right_ref_imgname='$val',";
								elseif($key=="desturl" )
									$sqlstr.="destination_url='$val',";
								elseif($key=="trackurl" )
									$sqlstr.="tracker_url='$val',";	
							}
							
							if($sqlstr!=""){
								$sql="update ads set ".trim($sqlstr,',')." where id='$value[id]' and advertiser_id='$_SESSION[ADV_ID]'";
								$conn->execute($sql);
							}
						}
					}
					break;*/
				}
			}
		break;		
	}
}
			
			if($cleanup!=1) {
				updateDirtyAds($campaign);
			}
			
			foreach ($adid as $ad) {
				$sql=$conn->Prepare("insert into campaign_members set cid=?, ad_id=?");
				$rs=$conn->execute($sql, array($cid, $ad));
			}
			
			
				/*foreach ($adid_already as $ad) {
				$sql="insert into campaign_members (cid,ad_id) select '$cid',id from ads where id=$ad and advertiser_id=$_SESSION[ADV_ID]";
				$rs=$conn->execute($sql);
			}*/
			if($cleanup==1){
				$error = "Error in campaign creation, please edit and create again.";
				if($_SESSION['CAMPAIGN']['id']=="")
					cleanupCampaign($cid, $adid);
			}else {
				$urlMsg='';
				if(key_exists("urlUpdate",$_SESSION['CAMPAIGN'])) {
					$urlUpdate = $_SESSION['CAMPAIGN']['urlUpdate'];
					if(!$urlUpdate==true){
						$urlMsg = ".+But+Destination+Url+not+edited";
					}
					else {
						$urlMsg="";
					}
				}
				$_SESSION['TARGETCHANNEL']="";
				$_SESSION['TARGETCHANNELNAME']="";
				$_SESSION['CAMPAIGN']="";
				if(isset($campaign['id']))
				$name=$campaign['name'];
				else
				$name=$_REQUEST['campaignname'];
				session_unregister('CAMPAIGN');
				session_unregister('TARGETCHANNEL');
				session_unregister('TARGETCHANNELNAME');
				//doForward("$config[baseurl]/adv.php?alertbuycredit=true&msg=Campaign+$name+successfully+saved");
				//exit();
                                header("Location:$config[baseurl]/adv.php?alertbuycredit=true&msg=Campaign+$name+successfully+saved$urlMsg$pausedMsg");
			}
			break;

		}
//==============================================
		/*$j=0;
		foreach($campaign['videoads'] as $key => $value)
		{
			$swfArr = explode(",", $value['swfid']);
			$dimArr = explode(",",$value['dimession']);
			for($i=0;$i<count($swfArr);$i++)
			{
				$campaignBannerArr[$j]['name'] = $campaign['videoads'][$key]['name'];
				$campaignBannerArr[$j]['swfid'] = $swfArr[$i];
				$campaignBannerArr[$j]['dim'] = $dimArr[$i];
				$dim = explode("x",$dimArr[$i]);
				$campaignBannerArr[$j]['width'] = $dim[0];
				$campaignBannerArr[$j]['height'] = $dim[1];
				$j++;
			}
		
			
			
			
		}
		//echo "<pre>";
		//print_r($campaignBannerArr);
		$campaign['campaignBannerArr'] = $campaignBannerArr;
		$smarty->assign("campaignBannerArr", $campaignBannerArr);*/
		//print_r($_SESSION['CAMPAIGN']['campaignBannerArr']);
//=======================================================		
    } catch(Exception $e) {
      $error=$e->getMessage();
    }
		$smarty->assign("campaign", $campaign);
		$smarty->assign("error", $error);
		break;
	default:
		chk_login();
		break;
}   

if(isset($_REQUEST['fetch_table'])){
    $smarty->display("tables/{$_REQUEST['fetch_table']}.tpl");
    die;
}
    
if($_REQUEST['inframe']!="true")
{
	$smarty->assign('submenu',$submenu);
	if(is_advertiser_loggedin())
	$smarty->display("advheader.tpl");
	elseif (is_publisher_loggedin())
	$smarty->display("pubheader.tpl");
	else
	$smarty->display("header.tpl");
}

$smarty->display("$_REQUEST[page].tpl");
if($_REQUEST['inframe']!="true")
{
	$smarty->assign('submenu',$submenu);
	if(!(is_advertiser_loggedin() || is_publisher_loggedin())) $smarty->display("footer.tpl");
}
	
?>
