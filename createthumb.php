<?php
session_start();
include_once('include/config.php');
include_once('include/function.php');


function checkFileSizeExtension($path, $fname) {
	global $config;
	$allowedFormats=array(
		"mpg",
		"avi",
		"mpeg",
		"wmv",
		"rm",
		"dat",
		"mov",
		"rv",
		"mp4",
		"flv",
		"m4v",
		"f4v",
		"3gp"
	);

	$p=$fname;
	$pos=strrpos($p,".");
	$ph=strtolower(substr($p,$pos+1,strlen($p)-$pos));

	$found=false;
	foreach($allowedFormats as $ext) {
		if($ext==$ph)
			$found=true;
	}
	if($found!=true)
		throw new Exception("Invalid file extension");

	return $ph;
}

function createThumb($srcname,$destname,$maxwidth,$maxheight)
{
	$oldimg = $srcname;//$config['basepath']."/photo/".$srcname;
	$newimg = $destname;//$config['basepath']."/photo/".$destname;

	$imagedata = GetImageSize($oldimg);
	$imagewidth = $imagedata[0];
	$imageheight = $imagedata[1];
	$imagetype = $imagedata[2];

	$shrinkage = 1;
	if ($imagewidth > $maxwidth)
	{
		$shrinkage = $maxwidth/$imagewidth;
	}
	if($shrinkage !=1)
	{
		$dest_height = $shrinkage * $imageheight;
		$dest_width = $maxwidth;
	}
	else
	{
		$dest_height=$imageheight;
		$dest_width=$imagewidth;
	}
	if($dest_height > $maxheight)
	{
		$shrinkage = $maxheight/$dest_height;
		$dest_width = $shrinkage * $dest_width;
		$dest_height = $maxheight;
	}
	if($imagetype==2)
	{
		$src_img = @imagecreatefromjpeg($oldimg);
		$dst_img = @imageCreateTrueColor($dest_width, $dest_height);
		ImageCopyResampled($dst_img, $src_img, 0, 0, 0, 0, $dest_width, $dest_height, $imagewidth, $imageheight);
		imagejpeg($dst_img, $newimg, 100);
		imagedestroy($src_img);
		imagedestroy($dst_img);
	}
	elseif ($imagetype == 3)
	{
		$src_img = imagecreatefrompng($oldimg);
		$dst_img = imageCreateTrueColor($dest_width, $dest_height);
		ImageCopyResampled($dst_img, $src_img, 0, 0, 0, 0, $dest_width, $dest_height, $imagewidth, $imageheight);
		imagepng($dst_img, $newimg, 100);
		imagedestroy($src_img);
		imagedestroy($dst_img);
	}
	else
	{
		$src_img = imagecreatefromgif($oldimg);
		$dst_img = imageCreateTrueColor($dest_width, $dest_height);
		ImageCopyResampled($dst_img, $src_img, 0, 0, 0, 0, $dest_width, $dest_height, $imagewidth, $imageheight);
		imagejpeg($dst_img, $newimg, 100);
		imagedestroy($src_img);
		imagedestroy($dst_img);
	}
}

function createVideoThumbnails($video, $prefix, $ext)
{
	global $config;
	$fc = 0;
	for($i=4;$i<=8;$i+=8)
	{
		if($ext=="flv"){
			mkdir("$config[tempfilepath]/$prefix");
			$cmd = $config['ffmpeg']. " -i $video -an -ss ".$i." -vframes 2 -y $config[tempfilepath]/$prefix/0000000%d.jpg";
		}
		else {
			$cmd = $config['mplayer']." $video -ss ".$i." -nosound -vo jpeg:outdir=".$config['tempfilepath']."/".$prefix." -frames 2";
		}
        //echo $cmd;
        trigger_error("Thumbnail command: ".$cmd, E_USER_WARNING);
		exec($cmd, $output);

		if($fc==0)
			$fd=$config['tempfilepath']."/".$prefix.".jpg";
		else
			$fd=$config['tempfilepath']."/".$prefix."_".$fc.".jpg";

		$ff = $config['tempfilepath']."/".$prefix."/00000002.jpg";

		if(!file_exists($ff)) {
			$ff = $config['tempfilepath']."/".$prefix."/00000001.jpg";

			if(!file_exists($ff))
				throw new Exception("Unable to create thumbnails");
		}

		//createThumb($ff,$fd,$config['thumb_max_width'],$config['thumb_max_height']);

		copy($ff, $fd);
		$fc++;
	}
}

function checkDuration($path, $max, $ext) {
//GET DURATION
	global $config;

	if($ext=="flv") {
		 exec("$config[ffmpeg] -i $path  -t 0.000001 -y /tmp/random.avi 2>&1", $p);
		 //print_r($p);
		 foreach ($p as $row) {
		 	if(preg_match("/Duration: (.*?),.*/",$row,$output)) {
		 		$durations=explode(":",$output[1]);
		 		$duration=$durations[0]*3600+$durations[1]*60+$durations[2];
		 	}
		 }
	} else{
		exec("$config[mplayer] -vo null -ao null -frames 0 -identify $path", $p);
		while(list($k,$v)=each($p)) {
			if($length=strstr($v,'ID_LENGTH='))
			break;
		}
		$lx = explode("=",$length);
		$duration = $lx[1];
	}

	if($duration==0 || $duration < 0){
		throw new Exception("Invalid file encoding");
	}
	if($duration>$max) { 
		$secs=intval($duration*10);
		$secs=$secs/10;
		throw new Exception("Video duration ($secs sec) is longer than  allowed duration of $max sec");
	}
	return $duration;
}

function get_video_dimensions($video = false) {
	global $config;  
	if (file_exists ( $video )) {  
		$command = $config['ffmpeg'] . ' -i ' . $video . ' -vstats 2>&1';  
		$output = shell_exec ( $command );
		$regex = "/([0-9]{1,4})x([0-9]{1,4})/";  
		if (preg_match($regex, $output, $regs)) { 
			$vals = (explode ( 'x', $regs [0] ));  
			$width = $vals [0] ? $vals [0] : null;  
			$height = $vals [1] ? $vals [1] : null;  
			return array ('width' => $width, 'height' => $height );  
		} else {  
			return false;  
		}  
	} else {  
		return false;  
	}    
}

function convertVideo($videopath, $id, $ext, $duration) {
	global $config;
	//$duration=20; //Seconds
	$flvpath="$config[tempfilepath]/$id.flv";
	$flvpathtemp="$config[tempfilepath]/$id.x.flv";
	//Encoding $duration
	if($ext=="flv"){
		copy($videopath, $flvpathtemp);
	}else {
		exec("$config[mencoder] $videopath -o $flvpathtemp -ss 0 -endpos $duration -of lavf -oac mp3lame -lameopts abr:br=40:mode=3 -ovc lavc -lavcopts vcodec=flv:vbitrate=300:mbd=2:mv0:trell:v4mv:cbp:last_pred=3 -ofps 12 -srate 22050 -vf scale=320:240");
	}
	
		
	//echo "$config[metainject] -Uvk $flvpathtemp $flvpath";
	exec("$config[metainject] -Uvk $flvpathtemp $flvpath");
	@unlink("$flvpathtemp");
	
	
	# ===================video for iphone========================
	$mp4fname = "$config[tempfilepath]/$id._s.mp4";
	$dim = get_video_dimensions($videopath);
	$width = 480;
	$height = $dim['height']/$dim['width']*$width;
        if(!$height){
          $height = 270;
           trigger_error("Bad dimensions received ".$dim['width']."x".$dim['height'], E_USER_WARNING);
        }
	// change command by Milchar
	$excmdsound = "$config[ffmpeg] -i $videopath -vcodec libx264 -vpre medium -vpre ipod640 -b 300k -bt 50k -acodec copy -s ".$width."x".$height." -qscale 9 $mp4fname";
	exec($excmdsound, $output, $returnvalue);
	trigger_error("_s.mp4 conversion: ".$excmdsound."\n Output: ".$output, E_USER_WARNING);
	
	
	/*
	 $excmdsound="export LD_LIBRARY_PATH=/usr/local/lib;mencoder $videopath -o $mp4fname\
       -oac faac -faacopts mpeg=4:object=2:raw:br=128 \
      -vf scale=480:-10,harddup \
      -of lavf -lavfopts format=mp4 \
      -ovc x264 -x264encopts nocabac:level_idc=30:bframes=0:global_header:threads=auto:subq=5:frameref=6:partitions=all:trellis=1:chroma_me:me=umh:bitrate=500 $bframe";
    */
	
	// PP: Just copy the _s file _hq file for now - will add back HQ conversion if needed
	$_smp4fname = "$config[tempfilepath]/$id._s.mp4";
	$mp4fname="$config[tempfilepath]/$id._hq.mp4";
	$excmdhq = "cp $_smp4fname $mp4fname";
	trigger_error("_hq.mp4 conversion -- Just copy for now: ".$excmdhq, E_USER_WARNING);
	exec($excmdhq, $output, $returnvalue);
    

   # ==================video for android ========================
       $_smp4fname = "$config[tempfilepath]/$id._s.mp4";
       $_amp4fname = "$config[tempfilepath]/$id._a.mp4";
	$excmda = "cp $_smp4fname $_amp4fname";
	trigger_error("_a.mp4 conversion -- Just copy for now: ".$excmda, E_USER_WARNING);
	exec($excmda, $output, $returnvalue);
       /* Just copy the _s file to _a for now - will add back MP4Box if needed
       $inputfname="$id._s.mp4";
       $vidtrack=NULL;
       $audtrack=NULL;
       $mp4fname = "$id._a.mp4";
       $output=NULL;
       $excmdsound="cd $config[tempfilepath] && /usr/local/bin/MP4Box -raw 1 $inputfname && cd -";
       exec($excmdsound, $output, $returnvalues);
       if(strstr($output[0], "AVC")!=FALSE){
         $vidtrack="$id._s_track1.h264";
       }else if(strstr($output[0], "AAC")!=FALSE){
         $audtrack="$id._s_track1.aac";
       }
       $output=NULL;
       $excmdsound="cd $config[tempfilepath] && /usr/local/bin/MP4Box -raw 2 $inputfname && cd -";
       exec($excmdsound, $output, $returnvalues);
       if(strstr($output[0], "AVC")!=FALSE){
         $vidtrack="$id._s_track2.h264";
       } else if(strstr($output[0], "AAC")!=FALSE){
         $audtrack="$id._s_track2.aac";
       }
       $excmdsound="cd $config[tempfilepath] && /usr/local/bin/MP4Box ";
       if($audtrack)          $excmdsound.="-add $audtrack ";
       if($vidtrack)
         $excmdsound.="-add $vidtrack ";
       $excmdsound.="$mp4fname && cd -";
       exec($excmdsound, $output, $returnvalues);
       */
  # ================== End- Video for Android ==================
    

	if(!file_exists($flvpath)){
		throw new Exception("Unable to convert video");
	}
}

try{
	$id=$_REQUEST[id];
	$filename=$_REQUEST['filename'];
	if(isset($argv)&&isset($argv[1])&&isset($argv[2])){
		$id=$argv[1];
		$filename=$argv[2];
	}
	$videopath=escapeshellcmd($config['tempfilepath']."/$id");
	if(!file_exists($videopath))
		throw new Exception("Upload failed");

	$ext=checkFileSizeExtension($videopath,$filename);
	$videopathdest="$videopath.$ext";
	if(is_admin_loggedin()){
		$duration = checkDuration($videopath, 300, $ext);
	}else{
		$duration = checkDuration($videopath, 60, $ext);
	}
	createVideoThumbnails($videopath, escapeshellcmd("thmb$id"), $ext);
	convertVideo($videopath, escapeshellcmd($id), $ext, $duration);
	echo "success|".$duration;
}catch (Exception $e) {
	echo $e->getMessage();
}
?>
