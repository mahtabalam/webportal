<?php
/*
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
jqUploader serverside example: (author : pixeline, http://www.pixeline.be)

when javascript is available, a variable is automatically created that you can use to dispatch all the possible actions

This file examplifies this usage: javascript available, or non available.

1/ a form is submitted
1.a javascript is off, so jquploader could not be used, therefore the file needs to be uploaded the old way
1.b javascript is on, so the file, by now is already uploaded and its filename is available in the $_POST array sent by the form

2/ a form is not submitted, and jqUploader is on
jqUploader flash file is calling home! process the upload.



+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

*/
	include("include/validation.php");

	include_once('include/config.php');

session_start();

if ($_GET['jqUploader'] == 1) {
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // 2. performing jqUploader flash upload
    $uploadFile = $config['AD_RO_DIR']."/".$_REQUEST['id'].'.'.extension($_FILES['Filedata']['name']);;
    if ($_FILES['Filedata']['name']) {
        if (move_uploaded_file ($_FILES['Filedata']['tmp_name'], $uploadFile)) {
            // delete the file
            //  @unlink ($uploadFile);
            echo $uploadFile;
        }
    } else {
        if ($_FILES['Filedata']['error']) {
            echo $_FILES['Filedata']['error'];
        }
    }
}


function extension($file){
	$ext = explode('.', $file);
	return $ext[1];
}
// /////////////////// HELPER FUNCTIONS
function myHtml($bodyHtml)
{

    ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>jqUploader demo - Result</title>
<link rel="stylesheet" type="text/css" media="screen" href="style.css"/>
</head>
<body>
<?php echo $bodyHtml;

?>
</body>
</html>
<?php
}
?>
