<?php 
$portal=$_REQUEST['portal'];
if($portal=='i'){
    $portal="IVDOPIA";
}
else{
    $portal="VDOPIA";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Opt Out of <?php echo $portal;?> Cookie</title>
<script type="text/javascript">
function optout() {
    var vd_expires=new Date();
    var del_vd_expires=new Date();
    vd_expires.setTime((vd_expires.getTime())+31536000000);
    del_vd_expires.setTime((del_vd_expires.getTime())-31536000000);
    document.cookie="VDOUNIQ=OPT_OUT;expires="+vd_expires.toGMTString()+";domain=.vdopia.com;path=/";
    document.cookie="_rt=0;expires="+del_vd_expires.toGMTString()+";domain=.vdopia.com;path=/"; 
    location.reload(true);
}

</script>
</head>
<body>

<p>
<strong>
Opt Out of the <?php echo $portal;?> Cookie
</strong><br/><br/>
Your Cookie Status:<br/>
<strong>
<?php 
$cookie=$_COOKIE['VDOUNIQ'];
if(isset($cookie)==false){
echo "You have not opted out.";
}
else if($cookie=="OPT_OUT"){
	echo "You have opted out and have no $portal cookie.";
}
else{
	echo "You have an active $portal cookie and have not opted out.";
	
}
 
?>
</strong>
<a onclick="optout();">
  <button style="cursor: pointer; cursor: hand;width:160;height:24; background-color:#97cfc3">Opt Out Now
  </button></a>
<!-- <img  onclick="optout();" style="cursor: pointer; cursor: hand;" name="optOutBut" src="optOutNowButMO.gif"/>
-->

</p>
<p>
<strong>
Opting out of the <?php echo $portal;?> Cookie does not mean that you will stop seeing ads.
</strong><br/>

Opting out of the <?php echo $portal;?> Cookie means that you will no longer see ads that are selected for you, based on your browsing
behavior.

</p>
<p>
<strong>
The <?php echo $portal;?> Opt Out tool is cookie based.
</strong><br/>

If you have cookies blocked, or have blocked cookies from <?php echo $portal;?>, the Opt Out tool will not work. The Opt Out Tool
sets a value in your <?php echo $portal;?> cookie that tells the <?php echo $portal;?> ad server to serve you any ad, rather than an advertisement that
is customized for you. The opt out tool cannot set this value if you have no <?php echo $portal;?> cookie.

</p>
<p>
<strong>
What it means to allow <?php echo $portal;?> cookies.
</strong><br/>

If you let <?php echo $portal;?> set a cookie in your internet browser, the <?php echo $portal;?> ad server can ensure that you do not see certain ads
multiple times per day. The ad server can also deliver ads to you that are appropriate to your timezone or
geographical location, that are from some of your favorite site that you visit often, or based on your browsing patterns
and behaviors.

</p>
<p>

The <?php echo $portal;?> cookie is not a program and cannot execute anything on your computer. The <?php echo $portal;?> cookie can simply help
the <?php echo $portal;?> ad server find the most appropriate ads for you.

</p>
<p>
<strong>
The <?php echo $portal;?> cookie collects no personally identifiable information.
</strong><br/>

<?php echo $portal;?>, the <?php echo $portal;?> ad server and the <?php echo $portal;?> cookie do not use, collect, or in any other way gather or store any personally
identifiable information about anyone, ever. Learn more about Personally Identifiable Information (PII).

</p>
<p>
<strong>
Concerns? Contact us.
</strong><br/>
We understand that privacy and security on the internet is of utmost importance. Consumers should trust and feel
comfortable using the vast, complex, fun and growing tools and resources on the web. If you have any questions
about the <?php echo $portal;?> technology, our practices or privacy policy, contact us at privacy@<?php echo $portal;?>.com.
</p>
<p>
</body>
</html>