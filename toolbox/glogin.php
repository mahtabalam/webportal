<?php
session_start();
include_once("../include/config.php");
include_once("../include/function.php");

function goBack($msg) {
  echo "<html>";
  echo "<head>";
  echo "<script>";
  if(!isset($msg)) {
  echo "history.back();";
  } else {
    ?>
    function goBack() {
      history.back();
    }
    <?php
    echo "setTimeout('goBack()',4)";
  }
  echo "</script>";
  echo "</head>";
  echo "<body>";
  echo "$msg";
  echo "</body>";
  echo "</html>";
  exit;
}

if(isset($_REQUEST['email']) ) {

  $account=getRequest('accounttype', true);
  $email=getRequest('email', true);
  $password=getRequest('password', true);
  
  $sql="select publisher.id, fname, lname, email, currency, advertiser_id, overlay, branded, video  from publisher left join publisherpricing on publisher_id=publisher.id and isnull(validto) where email='$email' and passwd=md5('$password') and type='online'";
	$rs=$conn->execute($sql);
  if($account=='publisher') {
    if($rs->recordcount()>0) {
      session_register('PUB_ID');
      session_register('FNAME');
      session_register('LNAME');
      session_register('EMAIL');
      session_register('CURRENCY3DGT');
      session_register('CURRENCYVAL');
      session_register('CURRENCYSYM');

      if(isset($rs->fields['advertiser_id'])) {
        session_register('PUB_ADV_ID');
        $_SESSION['PUB_ADV_ID']=$rs->fields['advertiser_id'];
      }


      $_SESSION['PUB_ID']=$rs->fields['id'];
      $_SESSION['FNAME']=$rs->fields['fname'];
      $_SESSION['LNAME']=$rs->fields['lname'];
      $_SESSION['EMAIL']=$rs->fields['email'];
      $_SESSION['CURRENCY3DGT']=$rs->fields['currency'];
      $_SESSION['CURRENCYVAL']=insert_3dgt_to_val(array("3dgt" =>$rs->fields['currency']));
      $_SESSION['CURRENCYSYM']=insert_val_to_currency(array("value" => $_SESSION['CURRENCYVAL']));

      if(isset($_SESSION['CAMPAIGN']['currency'])) {
        $_SESSION['CAMPAIGN']['currency']=$_SESSION['CURRENCYVAL'];
      }

      if(isset($rs->fields['advertiser_id'])) {
        session_register('PUB_ADV_ID');
        session_register('OVERLAY_PRICING');
        session_register('VIDEO_PRICING');
        session_register('BRANDED_PRICING');
        $_SESSION['PUB_ADV_ID']=$rs->fields['advertiser_id'];
        $_SESSION['ADV_ID']=$_SESSION['PUB_ADV_ID'];
        $_SESSION['ADV_PUB_ID']=$_SESSION['PUB_ID'];
        $_SESSION['OVERLAY_PRICING']=split(':',$rs->fields['overlay']);
        $_SESSION['BRANDED_PRICING']=split(':',$rs->fields['video']);
        $_SESSION['VIDEO_PRICING']=split(':',$rs->fields['branded']);
      }

      $sql="update publisher set lastlogin=now() where id=$_SESSION[PUB_ID]";
      $conn->execute($sql);
    } else {
      $msg="Invalid Login";
    }
  } else if($accounttype=='advertiser') {
    /*TODO add verification code/rules*/
    try {
      $email=getRequest('email', true);
      $password=getRequest('password', true);
      $sql="select 
        advertiser.id, 
        fname, 
        lname, 
        email, 
        currency, 
        publisher_id
          from 
          advertiser, 
        advertisersheet 
          where 
          advertiser.id = advertisersheet.advertiser_id and 
          email = '$email' and 
          passwd = md5('$password') and type='online'";
      $rs=$conn->execute($sql);
      if($rs->recordcount()>0) {
        session_register('ADV_ID');
        session_register('FNAME');
        session_register('LNAME');
        session_register('EMAIL');
        session_register('CURRENCY3DGT');
        session_register('CURRENCYVAL');
        session_register('CURRENCYSYM');

        if(isset($rs->fields['publisher_id'])) {
          session_register('ADV_PUB_ID');
          $_SESSION['ADV_PUB_ID']=$rs->fields['publisher_id'];
        }

        $_SESSION['ADV_ID']=$rs->fields['id'];
        $_SESSION['FNAME']=$rs->fields['fname'];
        $_SESSION['LNAME']=$rs->fields['lname'];
        $_SESSION['EMAIL']=$rs->fields['email'];
        $_SESSION['CURRENCY3DGT']=$rs->fields['currency'];
        $_SESSION['CURRENCYVAL']=insert_3dgt_to_val(array("3dgt" =>$rs->fields['currency']));
        $_SESSION['CURRENCYSYM']=insert_val_to_currency(array("value" => $_SESSION['CURRENCYVAL']));

        if(isset($_SESSION['CAMPAIGN']['currency'])) {
          $_SESSION['CAMPAIGN']['currency']=$_SESSION['CURRENCYVAL'];
        }
        $sql="update advertiser set lastlogin=now() where id=$_SESSION[ADV_ID]";
        $conn->execute($sql);
        doForward("$config[baseurl]/adv.php");
      } else {
        $msg="Invalid Login";
      }
    } catch(Exception $e){
    }
  }

  if(isset($_REQUEST['rd']))
  {  
    if($_REQUEST['rd']=='back') {
      goBack($msg);
    }
    header("Location: ".$_REQUEST['rd']);
     exit;
  }
  
} 

if(isset($_REQUEST['logout'])) {
  session_destroy();
  if($_REQUEST['rd']=='back') {
    goBack();
  } 
  exit;
}

header("Content-Type: text/javascript");
if(is_publisher_loggedin() || is_advertiser_loggedin()) {

  if(is_publisher_loggedin())
    $id=$_SESSION['PUB_ID']." p";
  if(is_advertiser_loggedin())
    $id=$_SESSION['ADV_ID']." a";

  $username="$_SESSION[FNAME] $_SESSION[LNAME]";
  $time=gmdate('m/d/Y H:i:s')." $id";
  $cred=md5($username.$time."hehe");

  echo "var iVUserName='$username';";
  echo "var iVAuth='$time';";
  echo "var iVCred='$cred';";

} else {
  echo "var iVUserName='Not Loggedin';";
}
?>
