#! /bin/bash
cat /var/log/httpd/access_log | awk '{print $4}' | grep "/Nov/" | cut -d\[ -f2 | cut -d: -f1,2 | uniq -c | awk '{ print $2, "," , $1 }'
