<?php

set_time_limit(0); // This line sets the amount of time the script will execute for before it stops working. I've set this to 0 so there won't be any time-out errors.


	if($_REQUEST['type']=='img') {
    $uploadDir = "files/images/"; // Put in the directory that you will be uploading to. Don't forget the last '/'
	}elseif ($_REQUEST['type']=='vdo') {
		$uploadDir = "files/videos/";
	} else {
		header("HTTP/1.1 404 Not Found");
		error_log("Exiting Upload");
		exit();
	}
	//error_log("upload invoked");

    $filename = ereg_replace("[^A-Za-z0-9.]", "", $_FILES['Filedata']['name']); // Here I am taking the file name (accessed by $_FILES['Filedata']['name']) and I'm replacing any non alphanumeric characters. All that should be left is the file name, a dot and the extension.

    $uploadFile = $uploadDir . $filename; // This puts the directory and file name together in a variable. Handy for later on to keep your code easy to read.

    if(move_uploaded_file($_FILES['Filedata']['tmp_name'], $uploadFile)) { // This actually moves the file to the directory and file name specified by the $uploadFile variable.

    	chmod($uploadFile, 0644); // Here I'm changing the permissions of the file so that we can do other things to it later on, like re-sizing images or being able to delete it from your FTP client.

    }else {
    	header("HTTP/1.1 404 Not Found");
    }
?>
