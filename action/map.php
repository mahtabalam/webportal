<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<meta name = "viewport" content = "width = device-width">
<meta name = "viewport" content = "initial-scale = 1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head> 
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/> 
    <?php echo "<title>Local Search - ". $_REQUEST['q'] ."</title> "; ?>
    <!--
    * Load the Maps API
    * AJAX Search API
    * Local Search Control
    *
    * Note: IF you copy this sample, make sure you make the following
    * changes:
    * a) replace &key=internal with &key=YOUR-KEY
    * b) Path Prefix to gmlocalsearch.* should be
    *    http://www.google.com/uds/solutions/localsearch/
    * c) Path prefix to ../../api?file=uds.js and to ../../css/gsearch.css
    *    should be http://www.google.com/uds
    --> 
    <script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAOjiF7zXmC62bG-DpB7OTeBRvoJJ4Nan2cPNl1W1posy0NYlakRT1p9T8TJ7cJXshubm6w1Atk3ZaXw" type="text/javascript"></script> 
    <script src="http://www.google.com/uds/api?file=uds.js&v=1.0&key=ABQIAAAAOjiF7zXmC62bG-DpB7OTeBRvoJJ4Nan2cPNl1W1posy0NYlakRT1p9T8TJ7cJXshubm6w1Atk3ZaXw" type="text/javascript"></script> 
    <script src="http://www.google.com/uds/solutions/localsearch/gmlocalsearch.js" type="text/javascript"></script> 
    <style type="text/css"> 
      @import url("http://www.google.com/uds/css/gsearch.css");
      @import url("http://www.google.com/uds/solutions/localsearch/gmlocalsearch.css");
      #map {
        border : 2px solid #979797;
        width : 98%;
        height : 400px;
      }
    </style> 
 
    <script type="text/javascript"> 
      var query = "<?= $_REQUEST['q'] ?>";
      var map = null;
      //<![CDATA[

	addEventListener('load', function() {
		setTimeout(hideAddressBar, 0);
	}, false);
	function hideAddressBar() {
		window.scrollTo(0, 1);
	}

      function useLocation(position){
        var initialLocation = new GLatLng(position.coords.latitude,position.coords.longitude);
        load(initialLocation);
      }

      function useDefault(){
        var newyork = new GLatLng(40.69847032728747, -73.9514422416687);
        load(newyork);
        alert("Location not available. Using New York.");
      }

      function getLocation(){
        if(navigator.geolocation) {
          // Allow up 100 minutes old info
          navigator.geolocation.getCurrentPosition(useLocation, useDefault, {maximumAge:6000000});
        } else {
          // Browser doesn't support Geolocation
          useDefault();
        }
      }

      function load(location) {
        if (GBrowserIsCompatible()) {
          // Create and Center a Map
          map = new GMap2(document.getElementById("map"));
          map.disablePinchToZoom();
          map.setCenter(location, 10);
          map.addControl(new GSmallZoomControl3D());
          //map.addControl(new GMapTypeControl());
 
          /* Metal Mode */
          // set up pins, use the metalset
          var pins = new Array();
          pins["kml"] = "metalblue";
          pins["local"] = "metalred";
 
          var labels = new Array();
          labels["kml"] = "metalblue";
          labels["local"] = "metalred";
 
          // then in options pass:
          // pins : pins, labels : labels
          /**/
          var options = {
            listingTypes : GlocalSearch.TYPE_BLENDED_RESULTS,
            Xpins : pins,
            Xlabels : labels
          }
          var lsearch = new google.maps.LocalSearch(options);
          map.addControl(lsearch);
          lsearch.execute(query);
        }
      }
      GSearch.setOnLoadCallback(getLocation);
      //]]>
    </script> 
  </head> 
  <body onunload="GUnload()"> 
    <div id="map"></div> 
  </body> 
</html> 
