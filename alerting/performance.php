#!/usr/bin/php
<?
include_once('config.php');

//Options: (intended to be once queried daily)
//-m: Measure
//    ads: Video ads requested, video ads served, banner ads requested, banner ads served -- daily
//    publishers: signed up publishers, active pubishers(requests recieved), ads served publishers, total publishers, served (enabled - active) -- daily
//    publishers: enabled publishers, active pubishers(requests recieved), ads served publishers, total publishers, served (enabled - active) -- daily
//    advertisers: active advertisers, total advertisers -- hourly
//    adsbycategory: Category=>served , no thresholds -- hourly
//    adsbycountry: Country=>served , no thresholds  -- daily
//    averages: bannerad cpms, videoad cpms, branded cpms -- daily
//    revenues: revenues yesterday, warning, critical : compared to day before-- daily
//
//-r: Region: required when measure=ads, ignored in other cases
//    "NRI": World - India
//    "ROW": World - India - US
//    "IN" : India
//    "US" : US

$warning = array( 
  "IN" => array (
    "preroll"   => 20,   //Ratio of video ad requests to preroll ads: checked for measure = ads
    "postroll"  => 5,   //Ratio of video ad requests to postroll ads: checked for measure = ads
    "midroll"   => 0,   //Ratio of video ad requests to midroll ads: checked for measure = ads
    "overlay"   => 20,   //Ratio of video ad requests to overlay ads: checked for measure = ads
    "tracker"   => 30,   //Ratio of banner ad requests to banner ads: checked for measure = ads
    "branded"   => 20    //Ratio of banner ad requests to banner ads: checked for measure = ads
  ),
  "US" => array (
    "preroll"   => 10,   //Ratio of video ad requests to preroll ads: checked for measure = ads
    "postroll"  => 3,    //Ratio of video ad requests to postroll ads: checked for measure = ads
    "midroll"   => 0,    //Ratio of video ad requests to midroll ads: checked for measure = ads
    "overlay"   => 10,   //Ratio of video ad requests to overlay ads: checked for measure = ads
    "tracker"   => 10,   //Ratio of banner ad requests to banner ads: checked for measure = ads
    "branded"   => 10    //Ratio of banner ad requests to banner ads: checked for measure = ads
  ),
  "ROW" => array (
    "preroll"   => 0,   //Ratio of video ad requests to preroll ads: checked for measure = ads
    "postroll"  => 0,   //Ratio of video ad requests to postroll ads: checked for measure = ads
    "midroll"   => 0,   //Ratio of video ad requests to midroll ads: checked for measure = ads
    "overlay"   => 0,   //Ratio of video ad requests to overlay ads: checked for measure = ads
    "tracker"   => 0,   //Ratio of banner ad requests to banner ads: checked for measure = ads
    "branded"   => 0    //Ratio of banner ad requests to banner ads: checked for measure = ads
  ),

  //Measured across the world ivdopia ads
  "banner"    => 50,   //Ratio of banner ad requests to banner ads: checked for measure = ads
  "preapp"    => 20,   //iVdopia ratio of preapp ad plays to session: checked for measure = ads
  "inapp"     => 5,    //iVdopia ratio of preapp ad plays to session: checked for measure = ads

  "activepub" => 20,   //Ratio of active publishers (whose requests are flowing in) to signed up publishers: checked for measure = publishers
  "adspub"    => 90    //Ratio of ads served publishers to active publishers: checked for measure = publishers
);

$critical = array(

  "IN" => array (
    "preroll"   => 10,   //Ratio of video ad requests to preroll ads: checked for measure = ads
    "postroll"  => 3,    //Ratio of video ad requests to postroll ads: checked for measure = ads
    "midroll"   => 0,    //Ratio of video ad requests to midroll ads: checked for measure = ads
    "overlay"   => 10,   //Ratio of video ad requests to overlay ads: checked for measure = ads
    "tracker"   => 15,   //Ratio of banner ad requests to banner ads: checked for measure = ads
    "branded"   => 10    //Ratio of banner ad requests to banner ads: checked for measure = ads
  ),
  "US" => array (
    "preroll"   => 5,   //Ratio of video ad requests to preroll ads: checked for measure = ads
    "postroll"  => 2,   //Ratio of video ad requests to postroll ads: checked for measure = ads
    "midroll"   => 0,   //Ratio of video ad requests to midroll ads: checked for measure = ads
    "overlay"   => 5,   //Ratio of video ad requests to overlay ads: checked for measure = ads
    "tracker"   => 5,   //Ratio of banner ad requests to banner ads: checked for measure = ads
    "branded"   => 5   //Ratio of banner ad requests to banner ads: checked for measure = ads
  ),
  "ROW" => array (
    "preroll"   => 0,   //Ratio of video ad requests to preroll ads: checked for measure = ads
    "postroll"  => 0,   //Ratio of video ad requests to postroll ads: checked for measure = ads
    "midroll"   => 0,   //Ratio of video ad requests to midroll ads: checked for measure = ads
    "overlay"   => 0,   //Ratio of video ad requests to overlay ads: checked for measure = ads
    "tracker"   => 0,   //Ratio of banner ad requests to banner ads: checked for measure = ads
    "branded"   => 0   //Ratio of banner ad requests to banner ads: checked for measure = ads
  ),


  "banner"    => 25,   //iVdopia ratio of banner ad plays to sessions: checked for measure = ads
  "preapp"    => 10,   //iVdopia ratio of preapp ad plays to session: checked for measure = ads
  "inapp"     => 3,   //iVdopia ratio of preapp ad plays to session: checked for measure = ads

  "activepub" => 10,
  "adspub"    => 80
);

$states  = array(
  "STATE_OK" => 0,
  "STATE_WARNING" => 1,
  "STATE_CRITICAL" => 2,
  "STATE_UNKNOWN" => 3
);

$options = getopt("m:p:r:");


$state=$states["STATE_OK"];
$yesterday=date("Y-m-d", mktime(-28,0,0, date("m"), date('d'), date('Y')));

$statusString="";
$perfString="";
if(isset($config['DB_PREFIX']) && $config['DB_PREFIX']!=''){
	$db_prefix =$config['DB_PREFIX'];
}else {
	$db_prefix='';
}
switch($options['m']){
  case "ads":
    $cselect="ROW";
    $dsselector="";
        switch($options['r']) {
          case "IN":
          case "US":
            $cselect=$options['r'];
            $dsselector="and ccode='$options[r]' ";
            break;
          case "NRI":
            $dsselector="and ccode<>'IN' ";
            break;
          case "ROW":
            $dsselector="and ccode<>'US' and ccode<>'IN' ";
            break;
          default:
            break;
        }
       $ads=array();
       $sqlservedbytype="select sum(impressions) as ads, adtype from {$db_prefix}daily_stats where  date='$yesterday'  $dsselector group by adtype";
       $rs=$reportConn->execute($sqlservedbytype);
       
       while($rs && !$rs->EOF) {
        $ads[$rs->fields['adtype']] = $rs->fields['ads'];
        $rs->movenext();
       }

       $slots=array();
       //very inaccurate but thats all we have right now.
       $sqlforvideoadslots="select sum(calls) as slots from {$db_prefix}aggregated_call where date='$yesterday'  $dsselector";
       $rs=$reportConn->execute($sqlforvideoadslots);
       if($rs){
         $slots['preroll']=$rs->fields['slots']+0;
         $slots['postroll']=$rs->fields['slots']+0;
         $slots['overlay']=$rs->fields['slots']+0;
         $slots['midroll']=$rs->fields['slots']+0;
         $slots['branded']=$rs->fields['slots']+0;
       }

       $sqlfortrackerslots="select sum(value) as slots from {$db_prefix}aggregate_app where measure='ts'     and date='$yesterday' ";
       $rs=$reportConn->execute($sqlfortrackerslots);
       if($rs){
         $slots['tracker']=$rs->fields['slots']+0;
       }

       $sqlforprerollslots="select sum(value) as slots from {$db_prefix}aggregate_app where measure='pres'   and date='$yesterday' ";
       $rs=$reportConn->execute($sqlforprerollslots);
       if($rs){
         $slots['preroll']+=$rs->fields['slots'];
       }

       $sqlforpostrollslots="select sum(value) as slots from {$db_prefix}aggregate_app where measure='posts' and date='$yesterday' ";
       $rs=$reportConn->execute($sqlforpostrollslots);
       if($rs){
         $slots['postroll']+=$rs->fields['slots'];
       }

       $sqlformidrollslots="select sum(value) as slots from {$db_prefix}aggregate_app where measure='mids'  and date='$yesterday' ";
       $rs=$reportConn->execute($sqlformidrollslots);
       if($rs){
         $slots['midroll']+=$rs->fields['slots'];
       }

       $sqlforoverlayslots="select sum(value) as slots from {$db_prefix}aggregate_app where measure='overs'  and date='$yesterday' ";
       $rs=$reportConn->execute($sqlforoverlayslots);
       if($rs){
         $slots['overlay']+=$rs->fields['slots'];
       }

       $sqlforbrandedslots="select sum(value) as slots from {$db_prefix}aggregate_app where measure='brands'  and date='$yesterday' ";
       $rs=$reportConn->execute($sqlforbrandedslots);
       if($rs){
         $slots['branded']+=$rs->fields['slots']+0;
       }


       if($options['r']=='US') {
         $sqlforsessionsiphone="select sum(value) as sess from  {$db_prefix}aggregate_app where date='$yesterday'  and measure='ai' and channel_id in (select id from channels where channel_type='iphone')";
         $rs=$reportConn->execute($sqlforsessionsiphone);
         if($rs){
           $isessions=$rs->fields['sess']+0;
         }

         $sqlforsessiontimeiphone="select sum(value) as engagement from  {$db_prefix}aggregate_app where date='$yesterday'  and measure='ai_t' and channel_id in (select id from channels where channel_type='iphone')";
         $rs=$reportConn->execute($sqlforsessiontimeiphone);
         if($rs){
           $engagement=$rs->fields['engagement']+0;
         }

         $slots['preapp']=$isessions;
         $slots['inapp']=$isessions;
         $slots['banner']=(integer)($engagement/30);

       }

      foreach ( $slots as $type=>$value ) {
        if($type == 'preapp' || $type=='inapp' || $type=='banner') {
          $warninglevel = (integer)($warning [$type]*$value/100);
          $criticallevel= (integer)($critical[$type]*$value/100);
        } else {
          
          $warninglevel = (integer)($warning [$cselect][$type]*$value/100);
          $criticallevel= (integer)($critical[$cselect][$type]*$value/100);
        }
        $adsserved=$ads[$type]+0;
        $perfString.=" $type=".$adsserved.";$warninglevel;$criticallevel;0;$value;";

        if( $adsserved < $criticallevel) {
           $statusString.="Critical:$type ";
           if( $state < $states['STATE_CRITICAL']  ) {
              $state=$states['STATE_CRITICAL'];
           }
        } else if($adsserved < $warninglevel) {
          $statusString.="Warning:$type ";
          if( $state < $states['STATE_WARNING']  ) {
              $state=$states['STATE_WARNING'];
           }
        }
      }

    break;
}

if($state==$states['STATE_UNKNOWN'])
    echo "UNKNOWN - ";
if($state==$states['STATE_CRITICAL'])
    echo "CRITICAL - ";
if($state==$states['STATE_WARNING'])
    echo "WARNING - ";
if($state==$states['STATE_OK'])
    echo "OK ";

echo $statusString . " | " . $perfString ."\n";

exit($state);
?>
