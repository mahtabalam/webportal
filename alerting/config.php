<?
$config = array();

if($_SERVER['SERVER_NAME'] !=null)
{
	echo "Invalid call";
	exit();
}

$config['BASE_DIR']	='/var/www/html/dev/pankaj_adserver';
$config['BASE_URL']	='http://i2.vdopia.com/dev/pankaj_adserver';

$config['SMARTY_DIR']	='/var/www/smarty/pankaj_adserver';

$config['CONF_DIR']	='/var/www/configs/pankaj_adserver';
$config['CONF_FILE']   = $config['CONF_DIR'].'/conf.ini';

require_once($config['BASE_DIR'].'/smarty/libs/Smarty.class.php');
require_once($config['BASE_DIR'].'/classes/SError.php');
require_once($config['BASE_DIR'].'/classes/SConfig.php');
require_once($config['BASE_DIR'].'/include/adodb/adodb.inc.php');
require_once($config['BASE_DIR'].'/include/phpmailer/class.phpmailer.php');
require_once($config['BASE_DIR'].'/classes/SEmail.php');

$smarty=new Smarty();
$smarty->compile_check = true;
$smarty->debugging = false;
$smarty->template_dir = $config['SMARTY_DIR'].'/templates';
$smarty->compile_dir = $config['SMARTY_DIR'].'/templates_c';
$smarty->cache_dir = $config['SMARTY_DIR'].'/cache';
$smarty->config_dir = $config['SMARTY_DIR'].'/configs';

$config['sandbox']	='true';

$config['support_email']	='Vdopia Support Team<debugsupport@vdopia.com>';
$config['noreply']	='debugno-reply@vdopia.com';
$config['supportcopy']	='debugsupport+sent@vdopia.com';

$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= "From: $config[support_email] " . "\r\n";
$headers .= "Bcc: $config[supportcopy]" . "\r\n";
$headers .= "Reply-To: $config[noreply]" . "\r\n" .    'X-Mailer: PHP/' . phpversion();
$config['support_headers'] = $headers;


$DBTYPE = 'mysql';
$DBHOST = SConfig::get("Database", "host");
$DBUSER = SConfig::get("Database", "user_name");
$DBPASSWORD = SConfig::get("Database", "password");
$DBNAME = SConfig::get("Database", "db_name");

$conn = &ADONewConnection($DBTYPE);
$conn->PConnect($DBHOST, $DBUSER, $DBPASSWORD, $DBNAME);

$config['baseurl']=$config['BASE_URL'];

$sql = "SELECT * from sconfig";
$rsc = $conn->Execute($sql);

if($rsc){while(!$rsc->EOF)
{
$field = $rsc->fields['soption'];
$config[$field] = $rsc->fields['svalue'];
@$rsc->MoveNext();
}}
$config['statusmailto'] = "techmails@vdopia.com";

$config['feedtables']=array("aggregate_statistics", "aggregate_geography", "aggregate_category", "aggregate_domain", "aggregated_call" , "channel_campaigns",  "revenue","channel_campperf");


$SERVER_NAME=system('hostname');

function is_testing() {
	global $config,$_REQUEST;
	return ($config['sandbox']=="true" && $_REQUEST['test12233223']=="hello");
}


?>
