<?php

require_once realpath(dirname(__FILE__)).'/include/config.php';
require_once realpath(dirname(__FILE__)).'/excelClasses/PHPExcel.php';
require_once realpath(dirname(__FILE__)).'/common/include/function.php';

/*
 * Base class for Creating Excel,Pdf etc file formats
  */
class xls {
	
	/*
	 * $lastRow points to last written row of file created
	 */
	protected $objPHPExcel,$objWriter,$lastRow;
	
	/*
	 * constructor for header of excel or pdf to create
	 */
	function __construct($author='') {

	$this->objPHPExcel = new PHPExcel();
	$this->objPHPExcel->getProperties()->setCreator($author['name']);
	$this->objPHPExcel->getProperties()->setLastModifiedBy($author['last_modified']);
	$this->objPHPExcel->getProperties()->setTitle($author['title']);
	$this->objPHPExcel->getProperties()->setSubject($author['subject']);
	$this->objPHPExcel->getProperties()->setDescription($author['description']);
	$this->objPHPExcel->setActiveSheetIndex(0);
	
		$this->lastRow=0;
	}	
	
	/*
	 * Add headers vertically for extra info about the contents of excel or pdf created
	 */
	private function addHeader($header) {
	
	$index=$this->lastRow;	
	foreach($header as $key=>$value) {
		$this->objPHPExcel->getActiveSheet()->setCellValue('A'.($index+1),$key);
		$this->objPHPExcel->getActiveSheet()->setCellValue('B'.($index+1),$value);
		$index++;

	}	
	$this->objPHPExcel->getActiveSheet()->setCellValue('A'.($index+1),'');
	$index++;

	$this->lastRow=$index;


	}
	
	/*
	 * Adding row to the file being created(Excel or pdf)
	 */
	private function addRows($rows) {
	
	$toAdd=array_keys($rows[0]);

	$col=0;
	foreach($toAdd as $add) {
	$this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1+$this->lastRow, $add);	
	$col++;
	}
	$this->lastRow++;

       foreach($rows as $row) {
	$col=0;
	foreach($row as $value) {
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1+$this->lastRow, $value);
        $col++; 
        }
	$this->lastRow++;

	}


	}	
	
	/*
	 * create file writer for the created file 
	 */
	protected function createWriter($format) {

         $this->objWriter =PHPExcel_IOFactory::createWriter($this->objPHPExcel,$format);

	}
/*
 * Get contents of file being created
 */
	protected function getContents() {
	
		ob_start();
		$this->objWriter->save("php://output");
		$contents=ob_get_contents();
		ob_end_clean();
		return $contents;

	}

	/*
	 * Save created file in with given name
	 */
	protected function saveFile($name) {
		
		$this->objWriter->save($name);
	}
	
	/*
	 * Send Created file as an attachment with an Email
	 */
	protected function sendAsEmail($fileName,$fileFormat='excel') {
		global $conn;
		$rs=$conn->Execute("select Login,Name from admin where id=".$_SESSION['ADMIN_ID']." LIMIT 1");
		$result=$rs->GetRows();
		$personName=$result[0]['Name'];
		$recipient=$result[0]['Login']."@vdopia.com";
		
		$from = "service@vdopia.com";
	        $subject = "Report";
		$message = "Hi ".$personName.", This is the report asked by you";
		sendAttachment($recipient, 'Vdopia Report Service', $from, $subject, $message, $this->getContents(), $fileFormat.'|'.$fileName,  "","");
	

	}
	/*
	 * This function add contents to file created as per given input
	 */
	protected function makeFile($content,$header=''){
	
		$this->addHeader($header);		
		$this->addRows($content);	
	
	
	}
	
		
}
