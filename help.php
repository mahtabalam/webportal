<?php

include_once('include/config.php');
include_once('include/function.php');
setup_tabs($smarty);


$help=array("Publisher" =>
array (
	"Getting Started" => array("How do I get Started?" => "pubgettingstarted",
							   "What is default currency?" => "currency",
							   "Who can signup?" => "publisherwho"),

	"Setting up a site" => array("How can I setup my website?" => "nameurl" ,
								"What do you mean by minimum CPM?" => "mincpm",
								"What is API key?" => 'apikey',
								"How does Vdopia verify my website?" => "verifysite",
								"How do I integrate Vdopia APIs to display advertisements on my website?" => "integration"
								),

	"Managing a site" => array(
						"How can I change my site settings?" => 'editchannel',
						'When do my site setting changes take effect?' => 'changetimes'
						),


	'Review Ads' => array(
						'What advertisements can I review?' => 'reviewwhat',
						'When do I start seeing advertisements to review?' => 'reviewwhen'
				),
	'Earnings' => array(
						'When do I get paid?' => 'whenpaid'
				),
	'Account and Payment' => array(
						'How do I setup my Personal/Payment Information?' => 'editperspay',
						'Can I edit my Default Currency? ' => 'editdefaultcurrency',
						'What do you mean by Minimum Payment Amount?' => 'minamt'
			)
	), "Advertiser" =>
array (
	"Getting Started" => array("How do I get Started?" => "advgettingstarted",
							   "What is default currency?" => "advcurrency"),

	"Creating a campaign" => array("What are the different ad formats that Vdopia supports?" => "adformats" ,
								"How do I target my ads?" => "targeting",
								"What are the advertising terms I should know?" => 'advterms',
								"When does my campaign starts running?" => "startingcampaign",
								"How much do I pay for each impressions?" => "payperimpression"
								),

	"Managing a campaign" => array(
						"How do I edit my campaign?" => 'editcampaign',
						'When does any changes to my campaign take effect?' => 'changeseffect'
						),


	'Reports' => array(
						'What reports can I view?' => 'report',
						'Can I schedule my reports to run automatically at regular time intervals?' => 'schedulereport'
				),
	'Credits' => array(
						'What do you mean by credits?' => 'credits', 
						
						'How many credits do I need to purchase?' => 'howmanycredits'
				),
	'Account and Payment' => array(
						'How do I edit my Personal Information?' => 'editpersinfo',
						'Can I edit my Default Currency? ' => 'editdefaultcurrency'
						
			)
	)

	);

$start=$help;

for ($i=0;$i<count($_REQUEST['questions']);$i++) {
	$start=$start[$_REQUEST['questions'][$i]];
}
$smarty->assign('start', $start);
if(is_advertiser_loggedin())
	$smarty->display("advheader.tpl");
elseif (is_publisher_loggedin())
	$smarty->display("pubheader.tpl");
else
	$smarty->display("header.tpl");
if(is_array($start)){
	$smarty->display('helpquestions.tpl');
} else {
	$smarty->display('showanswers.tpl');
}
$smarty->display('footer.tpl');

?>