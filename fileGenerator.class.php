<?php
ini_set('display_errors',1);
require_once realpath(dirname(__FILE__)).'/excelClasses/PHPExcel.php';
require_once realpath(dirname(__FILE__)).'/common/include/function.php';
require_once realpath(dirname(__FILE__)).'/excelException.php';
/*
 * Base class for Creating Excel,Pdf etc file formats
  */
 abstract class FileGenerator {
	
	/*
	 * $lastRow points to last written row of file created
	 */
	protected $objPHPExcel,$objWriter,$lastRow;
	
	/*
	 * constructor for header of excel or pdf to create
	 * @param Array $author 'name' => Robin Goyal, 'last_modified' =>
	 */
	function __construct($author='') {

	$this->objPHPExcel = new PHPExcel();
	$this->objPHPExcel->getProperties()->setCreator($author['name']);
	$this->objPHPExcel->getProperties()->setLastModifiedBy($author['last_modified']);
	$this->objPHPExcel->getProperties()->setTitle($author['title']);
	$this->objPHPExcel->getProperties()->setSubject($author['subject']);
	$this->objPHPExcel->getProperties()->setDescription($author['description']);
	$this->objPHPExcel->setActiveSheetIndex(0);
	
		$this->lastRow=0;
	}	
	
	/*
	 * Add headers vertically for extra info about the contents of excel or pdf created
	 */
	private function addHeader($header) {
	
	$index=$this->lastRow;	
	foreach($header as $key=>$value) {
		$this->objPHPExcel->getActiveSheet()->setCellValue('A'.($index+1),$key);
		$this->objPHPExcel->getActiveSheet()->setCellValue('B'.($index+1),$value);
		$index++;

	}	
	$this->objPHPExcel->getActiveSheet()->setCellValue('A'.($index+1),'');
	$index++;

	$this->lastRow=$index;


	}
	
	/*
	 * Adding row to the file being created(Excel or pdf)
	 */
	private function addRows($rows) {
	
		if(!is_array($rows)) {
			throw new excelException('No data into file',0);
			return;
	}
		$toAdd=array_keys($rows[0]);

	$col=0;
	foreach($toAdd as $add) {
	$this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1+$this->lastRow, $add);	
	$col++;
	}
	$this->lastRow++;

       foreach($rows as $row) {
	$col=0;
	foreach($row as $value) {
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1+$this->lastRow, $value);
        $col++; 
        }
	$this->lastRow++;

	}


	}	
	
	/*
	 * create file writer for the created file 
	 */
	protected function createWriter($format) {

         $this->objWriter =PHPExcel_IOFactory::createWriter($this->objPHPExcel,$format);

	}
/*
 * Get contents of file being created
 */
	protected function getContents() {
		ob_start();
		$this->objWriter->save("php://output");
		$contents=ob_get_contents();
		ob_end_clean();
		return $contents;

	}

	/*
	 * Save created file in with given name
	 * @param : $name is filename without extensions
	 */
	protected function saveFile($name) {
		
		$this->objWriter->save($name);
	}
	
	/*
	 * Send Created file as an attachment with an Email
	 * @param : $fileName is name of file without extension
	 * $params are mail parameters as array ("recipient"=>"","from"=>"","subject"=>"","message"=>"")
	 */
	protected function sendAsEmail($fileName,$fileFormat,$params)
	
	{

		if($fileFormat=='')
			$fileFormat='excel';
		
		if(!isset($params['recipient']))
			$recipient='';
		else $recipient=$params['recipient'];
		
		if(!isset($params['from']) || $params['from']=='')
			$from='service@vdopia.com';
		else  $from=$params['from'];

		if(!isset($params['subject']) || $params['subject']=='')
			$subject='Report';
		else $subject=$params['subject'];
		if(!isset($params['message']) ||$params['message']=='')
			$message='Hi ,here is the report asked by you';
		else $message=$params['message'];

		/*global $conn;

		$rs=$conn->Execute("select Login,Name from admin where id=".$_SESSION['ADMIN_ID']." LIMIT 1");
		$result=$rs->GetRows();
		$personName=$result[0]['Name'];
		$recipient=$result[0]['Login']."@vdopia.com";
		 */
		if($recipient==''||!filter_var($recipient, FILTER_VALIDATE_EMAIL)||!filter_var($from, FILTER_VALIDATE_EMAIL))
			throw new excelException('Invalid recipient id or sender id',0);


		else sendAttachment($recipient, 'Report Service', $from, $subject, $message, $this->getContents(), $fileFormat.'|'.$fileName,  "","");
	

	}
	/*
	 * This function add contents to file created as per given input
	 */
	protected function makeFile($content,$header=''){
	
		$this->addHeader($header);		
		$this->addRows($content);	
	
	
	}
	
		
}
