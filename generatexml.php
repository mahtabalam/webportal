<?php
header("Content-type: text/xml");
include_once('include/config.php');
include_once('include/function.php');
function getOneWeekData($conn, $pubId, $curdate, $weekOffset=0){
	global $reportConn;
if($weekOffset==2){
		$fromdate = "and date>=date_sub('$curdate',interval 15+WEEKDAY('$curdate') DAY)";
		$todate = "and date<=date_sub('$curdate',interval 9+WEEKDAY('$curdate') DAY)";
	}
	elseif($weekOffset==1){
		$fromdate = "and date>=date_sub('$curdate',interval 8+WEEKDAY('$curdate') DAY)";
		$todate = "and date<=date_sub('$curdate',interval 2+WEEKDAY('$curdate') DAY)";	
	}else{
		$fromdate = "and date>=date_sub('$curdate',interval 1+WEEKDAY('$curdate') DAY)";
		$todate = "";	
	}
	$revfields="pubrevenue_advcurrency*ex_rate_advpub ";
	$sql ="select
     sum(if(cp1.channel_id is NULL, if(cp.channel_id is NULL, pubrevenue_advcurrency*ex_rate_advpub,if(cp.model='CPM',impressions*cp.price_per_unit,if(cp.model='CPCV',completes*cp.price_per_unit,if(ads.parent_ad_id IS NULL,daily_stats.clicks,0)*cp.price_per_unit))),if(cp1.model='CPM',impressions*cp1.price_per_unit,if(cp1.model='CPCV,completes*cp1.price_per_unit,if(ads.parent_ad_id IS NULL,daily_stats.clicks,0)*cp.price_per_unit)))) as pubrevenue,
     substr(dayname(date),1,3) as day
   from daily_stats 
	 		left join channels on daily_stats.channel_id=channels.id
	 		left join campaign on daily_stats.campaign_id=campaign.id
			left join ads on daily_stats.ad_id=ads.id
 			left join channel_price as cp on (cp.ccode ='0' and cp.adtype = daily_stats.adtype and (daily_stats.date>=cp.start_date and daily_stats.date < cp.end_date) and daily_stats.channel_id=cp.channel_id and cp.model=campaign.model) 
			left join channel_price as cp1 on (daily_stats.ccode = cp1.ccode and cp1.adtype = daily_stats.adtype and (daily_stats.date>=cp1.start_date and daily_stats.date < cp1.end_date) and daily_stats.channel_id=cp1.channel_id and cp1.model=campaign.model)			 			
   where
     channels. publisher_id=$pubId and channels.channel_type!='iphone'
     and daily_stats.channel_id=channels.id
     $fromdate 
     $todate
   group by date,channels.id,daily_stats.adtype,daily_stats.ccode";
   $rs = $reportConn->execute($sql);
   while(!$rs->EOF) {
   $retArr[$rs->fields['day']]+= number_format($rs->fields['pubrevenue'],2,'.','');
		$rs->movenext();
   }
   return $retArr;
}

chk_publisher_login();

$curdate = date("Y-m-d");
$cur=$_SESSION['CURRENCYSYM'];
$xml="<?xml version='1.0' encoding='UTF-8'?>";

switch ($_REQUEST['type']){
	case 'weekly':
		$day_arr = array("Sun" =>0, "Mon" =>0, "Tue" =>0,"Wed" =>0,"Thu" =>0,"Fri" =>0,"Sat" =>0);
		$xml.="<chart caption='Weekly Revenue Graphs' xAxisName='Week' yAxisName='Revenue' showValues='0' numberPrefix='".$cur."'>";

		$xml.="<categories>";
		$xml.= "<category label='Sun'/>";   
		$xml.= "<category label='Mon'/>";
		$xml.= "<category label='Tue'/>";
		$xml.= "<category label='Wed'/>";   
		$xml.= "<category label='Thu'/>";   
		$xml.= "<category label='Fri'/>";   
		$xml.= "<category label='Sat'/>";   
		$xml.= " </categories>\n";

		/*Category Code ends Here*/
		/*For thisweek data code start here*/
		//=======================================	
                $revenue_arr = getOneWeekData($conn, $_SESSION['PUB_ID'], $curdate, 0);
		$result = array_merge($day_arr, $revenue_arr);
		$xml.="<dataset seriesName='This Week'>";
		foreach($result as $value) {
			$xml.= sprintf(" <set value = '%s' />", $value);
		}
		$xml.= "</dataset>";

		//========================================    

		//==================Last week=====================     
                $revenue_Lweekarr = getOneWeekData($conn, $_SESSION['PUB_ID'], $curdate, 1);
		$thisweek = array_merge($day_arr, $revenue_Lweekarr);


		$xml.="<dataset seriesName='Last Week'>";
		foreach($thisweek as $value) {
			$xml.= sprintf(" <set value = '%s' />", $value);
		}
		$xml.= "</dataset>";

		//==========================end last week========================

		//=====================Last week ago==========================    
                $revenue_SLweekarr = getOneWeekData($conn, $_SESSION['PUB_ID'], $curdate, 2);
		$lastweek = array_merge($day_arr, $revenue_SLweekarr);


		$xml.="<dataset seriesName='Two Weeks'>";
		foreach($lastweek as $value) {
			$xml.= sprintf(" <set value = '%s' />", $value);
		}
		$xml.= "</dataset>";
		/* */       
		//====================================================          
		$xml.="<styles>";
		$xml.="<definition>";
		$xml.="<style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />";
		$xml.="</definition>";
		$xml.="<application>";
		$xml.="<apply toObject='Canvas' styles='CanvasAnim' />";
		$xml.="</application>";   
		$xml.="</styles>";
		$xml.="</chart>";

		//$conn->close();

		break;
	case 'monthly':
		$curfullyear=date("Y");
		$curyear=date("y");
		$curmonth=date("m");
		$NOD=date("t");
		for($i=1;$i<=$NOD;$i++)
		{
			if($i<10)
				$str = "0"."$i-$curmonth-$curyear";
			else
				$str = "$i-$curmonth-$curyear";
			$mon_array["$str"]=0;
		}
		$i=1;
		$cur=$_SESSION['CURRENCYSYM'];
		$xml.="<chart caption='Monthly Revenue Graphs' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='".$cur."' >";
		//====================Category Code===================================
		$xml.="<categories>";
		for($i=1;$i<=30;$i++)
		{
			if ($i==1 || $i%5==0){
				$xml.="<category label='$i' />";
			}
			else {
				$xml.="<category label='' />";
			}
		}

		$xml.="</categories>"; 
		//==================End Category Code======================================    

		//====================For ThisMonth data code start here===================	
		$revfields="pubrevenue_advcurrency*ex_rate_advpub";
		$sql = "SELECT
					sum(if(cp1.channel_id is NULL, if(cp.channel_id is NULL, pubrevenue_advcurrency*ex_rate_advpub,if(cp.model='CPM',impressions*cp.price_per_unit,if(cp.model='CPCV',completes*cp.price_per_unit,if(ads.parent_ad_id IS NULL,daily_stats.clicks,0)*cp.price_per_unit))),if(cp1.model='CPM',impressions*cp1.price_per_unit,if(cp1.model='CPCV',completes*cp1.price_per_unit,if(ads.parent_ad_id IS NULL,daily_stats.clicks,0)*cp1.price_per_unit)))) as pubrevenue,
					date_format(date,'%d-%m-%y') as view_date
				from daily_stats 
	 		left join channels on daily_stats.channel_id=channels.id
			left join campaign on daily_stats.campaign_id=campaign.id
			left join ads on daily_stats.ad_id=ads.id
 			left join channel_price as cp on (cp.ccode ='0' and cp.adtype = daily_stats.adtype and (daily_stats.date>=cp.start_date and daily_stats.date < cp.end_date) and daily_stats.channel_id=cp.channel_id and cp.model=campaign.model) 
			left join channel_price as cp1 on (daily_stats.ccode = cp1.ccode and cp1.adtype = daily_stats.adtype and (daily_stats.date>=cp1.start_date and daily_stats.date < cp1.end_date) and daily_stats.channel_id=cp1.channel_id and cp1.model=campaign.model)
			where
					channels.publisher_id=".$_SESSION['PUB_ID'] ." 
					and daily_stats.channel_id=channels.id 
					and year(date)=$curfullyear 
					and month(date)=$curmonth 
				 group by date,channels.id,daily_stats.adtype,daily_stats.ccode";
		//sum(pubrevenue_advcurrency*ex_rate_advpub )as rev
		$rs = $reportConn->execute($sql);
		while(!$rs->EOF) {
			$monthdate =$rs->fields['view_date'];
			$revenue_arr["$monthdate"]+= number_format($rs->fields['pubrevenue'],2,'.','');
			$rs->movenext();
		}

		$thismonth = array_merge($mon_array, $revenue_arr);


		$xml.="<dataset seriesName='This Month'>";
		foreach($thismonth as $value) {
			$xml.= sprintf(" <set value = '%s' />", $value);
		}
		$xml.= "</dataset>";

		//=================End this month=======================    

		//==================Last month=====================     
		$lastdays=date("t",strtotime(date("Y-m-d",strtotime("-1 months"))));
		$lastmonth=date("m",strtotime(date("Y-m-d",strtotime("-1 months"))));
		$lastyear=date("y",strtotime(date("Y-m-d",strtotime("-1 months"))));
		for($i=1;$i<=$lastdays;$i++){
			if($i<10)
				$str = "0"."$i-$lastmonth-$lastyear";
			else
				$str = "$i-$lastmonth-$lastyear";
			$lastmontharray["$str"]=0;
		}
		$revfields="pubrevenue_advcurrency*ex_rate_advpub ";
		$sql = "SELECT
				sum(if(cp1.channel_id is NULL, if(cp.channel_id is NULL, pubrevenue_advcurrency*ex_rate_advpub,if(cp.model='CPM',impressions*cp.price_per_unit,if(cp.model='CPCV',completes*cp.price_per_unit,if(ads.parent_ad_id IS NULL,daily_stats.clicks,0)*cp.price_per_unit))),if(cp1.model='CPM',impressions*cp1.price_per_unit,if(cp1.model='CPCV',completes*cp1.price_per_unit,if(ads.parent_ad_id IS NULL,daily_stats.clicks,0)*cp1.price_per_unit)))) as pubrevenue,
					date_format(date,'%d-%m-%y') as view_date
				from daily_stats 
	 		left join channels on daily_stats.channel_id=channels.id
			left join campaign on daily_stats.campaign_id=campaign.id
			left join ads on daily_stats.ad_id=ads.id
 			left join channel_price as cp on (cp.ccode ='0' and cp.adtype = daily_stats.adtype and (daily_stats.date>=cp.start_date and daily_stats.date < cp.end_date) and daily_stats.channel_id=cp.channel_id and cp.model=campaign.model) 
			left join channel_price as cp1 on (daily_stats.ccode = cp1.ccode and cp1.adtype = daily_stats.adtype and (daily_stats.date>=cp1.start_date and daily_stats.date < cp1.end_date) and daily_stats.channel_id=cp1.channel_id and cp1.model=campaign.model)
			where  
				channels.publisher_id=".$_SESSION['PUB_ID'] ." 
				and daily_stats.channel_id=channels.id 
				and SUBSTRING(date FROM 1 FOR 7) =   SUBSTRING(CURRENT_DATE - INTERVAL 1 MONTH FROM 1 FOR 7) 
			 group by date,channels.id,daily_stats.adtype,daily_stats.ccode";
		$rs = $reportConn->execute($sql);
		while(!$rs->EOF) {
			$lastmonthdate =$rs->fields['view_date'];
			$revenue_Lweekarr["$lastmonthdate"]+= number_format($rs->fields['pubrevenue'],2,'.','');
			$rs->movenext();
		}
		$lastmonth = array_merge($lastmontharray, $revenue_Lweekarr);
		//print_r($lastmonth);
		$xml.="<dataset seriesName='Last Month'>";
		foreach($lastmonth as $value) {
			$xml.= sprintf(" <set value = '%s' />", $value);
		}
		$xml.= "</dataset>";

		//==========================end last month========================

		//=====================Last week ago==========================  
		$Totaldays=date("t",strtotime(date("Y-m-d",strtotime("-2 months"))));
		$Slastmonth=date("m",strtotime(date("Y-m-d",strtotime("-2 months"))));
		$Slastyear=date("y",strtotime(date("Y-m-d",strtotime("-2 months"))));

		for($i=1;$i<=$Totaldays;$i++)	{
			if($i<10)
				$str = "0"."$i-$Slastmonth-$Slastyear";
			else
				$str = "$i-$Slastmonth-$Slastyear";
			$Slast_array["$str"]=0;
		}
		$revfields="pubrevenue_advcurrency*ex_rate_advpub ";
		$sql = "SELECT
		sum(if(cp1.channel_id is NULL, if(cp.channel_id is NULL, pubrevenue_advcurrency*ex_rate_advpub,if(cp.model='CPM',impressions*cp.price_per_unit,if(cp.model='CPCV',completes*cp.price_per_unit,if(ads.parent_ad_id IS NULL,daily_stats.clicks,0)*cp.price_per_unit))),if(cp1.model='CPM',impressions*cp1.price_per_unit,if(cp1.model='CPCV',completes*cp1.price_per_unit,if(ads.parent_ad_id IS NULL,daily_stats.clicks,0)*cp1.price_per_unit)))) as pubrevenue,
		date_format(date,'%d-%m-%y') as view_date
		from daily_stats
		left join channels on daily_stats.channel_id=channels.id
		left join campaign on daily_stats.campaign_id=campaign.id
		left join ads on daily_stats.ad_id=ads.id
		left join channel_price as cp on (cp.ccode ='0' and cp.adtype = daily_stats.adtype and (daily_stats.date>=cp.start_date and daily_stats.date < cp.end_date) and daily_stats.channel_id=cp.channel_id and cp.model=campaign.model)
		left join channel_price as cp1 on (daily_stats.ccode = cp1.ccode and cp1.adtype = daily_stats.adtype and (daily_stats.date>=cp1.start_date and daily_stats.date < cp1.end_date) and daily_stats.channel_id=cp1.channel_id and cp1.model=campaign.model)
		where
		channels.publisher_id=".$_SESSION['PUB_ID'] ."
		and daily_stats.channel_id=channels.id
		and  SUBSTRING(date FROM 1 FOR 7) =   SUBSTRING(CURRENT_DATE - INTERVAL 2 MONTH FROM 1 FOR 7)
		group by date,channels.id,daily_stats.adtype,daily_stats.ccode
		";
		
		$rs = $reportConn->execute($sql);
		while(!$rs->EOF) {
			$monthdate =$rs->fields['view_date'];
			$revenue_SLweekarr["$monthdate"]+= number_format($rs->fields['pubrevenue'],2,'.','');
			$rs->movenext();
		}
		$lastmonthago = array_merge($Slast_array, $revenue_SLweekarr);
		$xml.="<dataset seriesName='Two Months'>";
		foreach($lastmonthago as $value) {
			$xml.= sprintf(" <set value = '%s' />", $value);
		}
		$xml.= "</dataset>";
		/* */  


		//======================End last week ago==============================          
		$xml.="<styles>";
		$xml.="<definition>";
		$xml.="<style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />";
		$xml.="</definition>";
		$xml.="<application>";
		$xml.="<apply toObject='Canvas' styles='CanvasAnim' />";
		$xml.="</application>";   
		$xml.="</styles>";
		$xml.="</chart>";
		break;
}

echo $xml;

 ?>
