<?php
session_start();
include_once(dirname(__FILE__)."/include/config.php");
include_once(dirname(__FILE__)."/include/function.php");
include_once(dirname(__FILE__)."/common/include/validation.php");
include_once(dirname(__FILE__)."/common/portal/captcha/securimage.php");
require_once(dirname(__FILE__)."/include/smartyBlockFunction.php");

$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);
setup_tabs($smarty);

$smarty->assign('timezone', getTimeZoneList());

if($_REQUEST['action_signup']!=""){
  # import the validation library
  $captchaObj = new Securimage();
  $rules = array(); // stores the validation rules

  # standard form fields
  $rules[] = "required,type,<br>This field is required.";
  $rules[] = "required,email,<br>Please enter your email address.";
  $rules[] = "valid_email,email,<br>Please enter a valid email address.";
  $rules[] = "required,password,<br>Please enter a password.";
  $rules[] = "length>4,password,<br>Your password must be at least 5 characters long";
  $rules[] = "length>4,confirm_password,<br>Your password must be at least 5 characters long";
  $rules[] = "same_as,confirm_password,password,<br>Please ensure the passwords you enter are the same.";
  $rules[] = "required,firstname,<br>First Name is required.";
  $rules[] = "digits_only,mobile,<br>Please enter only digits.";
  $rules[] = "length<16,mobile,<br>Your mobile number cannot be more than 15 characters long";
  $rules[] = "required,captcha,<br>Please enter code here";
  $rules[] = "required,agree,<br>Please accept our policy.";
  $rules[] = "required,currency,<br>Please select currency.";


  if (isset($_SESSION['CAMPAIGN']['currency'])) {
		$_REQUEST['currency']=$_SESSION['CAMPAIGN']['currency'];
		$_POST['currency']=$_SESSION['CAMPAIGN']['currency'];
  }

  $error = validateFields($_POST, $rules);
  if ($captchaObj->check($_REQUEST['captcha']) == false) { $captcha_msg="Please enter correct code."; $error['captcha']=""; }
  
  $smarty->assign("error",$error);
  $smarty->assign("captcha_msg", $captcha_msg);
  $_REQUEST['formErrors']=$error;

  # if there were errors, re-populate the form fields
  if (empty($error)){
	$pass=md5($_REQUEST['password']);
	$email=requestParams("email");
	$firstname=requestParams('firstname');
	$currency=requestParams('currency');
	$timezone=requestParams('timezone');
	$mobile=requestParams('mobile');
	$tabletype = requestParams('type');
	$portaltype = requestParams('portal_type');
	$agree= requestParams('agree');
	$apikey = md5($email.'online'.time());
	if($tabletype=="advertiser"){
		$activationStr = "activated='yes',";
		$timezoneStr = "timezone='$timezone',";
		$sqlsnip="";
	}else{
		$activationStr = "";
		$timezoneStr = "";
		$sqlsnip=",payment_cycle=60";// it is in days
	}
	$sql=$conn->Prepare("insert into $tabletype set
						email=?,
						fname=?,
						apikey=?,
						mobile=?,
						passwd=?,
						type='online',
						$activationStr $timezoneStr
						addtime=now(),
						lastlogin=now()
                        $sqlsnip"); 
		$rss=$conn->Execute($sql, array($email, $firstname, $apikey, $mobile, $pass));
		$userid=$conn->Insert_ID();
		if($rss && $userid > 0) {
			$subj = "Vdopia Registration Confirmation";
			$smarty->assign("receiver_fname", $firstname);
			$smarty->assign("email", $email);
			
			if($tabletype == "advertiser") {
				$body = $smarty->fetch("emails/welcomeemailadvertiser.tpl");
				if($config['sandbox']!='true')
						mail($_REQUEST['email'],$subj,$body,$config['support_headers']);
		
				$currency=insert_val_to_3dgt(array("value"=>"$_REQUEST[currency]"));
				
				$sql=$conn->Prepare("insert into advertisersheet set advertiser_id=?, currency=?");
				$rs=$conn->execute($sql, array($userid, $currency));
				doForward("$config[baseurl]/index.php?page=advlogin&msg=Congratulations! Your account has been created successfully, please login to continue&email=$email");
			} else {
				
				$body = $smarty->fetch("emails/welcomeemailpublisher.tpl");
				if($config['sandbox']!='true')
						mail($_REQUEST['email'],$subj,$body,$config['support_headers']);
				$currency=insert_val_to_3dgt(array("value"=>"$_REQUEST[currency]"));
				
				$sql=$conn->Prepare("update publisher set currency=? where id=?");
				$rs=$conn->execute($sql, array($currency, $userid));
				
				## insert default revenue share which is 50%- by tanu aggarwal
				require_once ABSPATH."/common/library/class.revShare.php";
				$effectiveDate = date('Y-m');
				$effectiveDay = '01';
				$createdAt = date('Y-m-d H:i:s');
				
				$revShareObj = new RevenueShare();
				$effDate = $effectiveDate."-".$effectiveDay;
				$response = $revShareObj->insertRevShare('50', $effDate, $userid);
				
				## end

				doForward("$config[baseurl]/index.php?page=publogin&msg=Congratulations! Your account has been created successfully, please login to continue&email=$email");
			}
			exit();
		}else {
			$smarty->assign("repeat", "true");
		}
  }
}

$smarty->display("header.tpl");
$smarty->display("signupform.tpl");
$smarty->display("footer.tpl");
