<?php include_once("../include/datefuncs.php");
if(is_admin_loggedin()){
  switch ($_REQUEST['action']) {
    case 'rosummary':
    		if($_REQUEST['act_rosummary']!="" && $_REQUEST['act_rosummary']=="rosummary"){
          		$fromdate 	= $_REQUEST['fromdate'];
          		$todate 	= $_REQUEST['todate'];
        	}
        	else{
          		$fromdate = date("Y-m-d", mktime(0, 0, 0, date("m")-1 , 1, date("Y")));
          		$todate = date("Y-m-d", mktime(0, 0, 0, date("m")+1 , date("d")-date("d"), date("Y")));
          		$_REQUEST['fromdate']=$fromdate;
          		$_REQUEST['todate']=$todate;
        	}
			$datequery=" and date>='$dates[0]' and date<='$dates[1]'";	
			//======================================================
				$sql = "SELECT 
					campaign.name, 
					campaign.budget_amt,
					campaign.price_per_unit, 
					campaign.id AS c_id,
					DATE_FORMAT(campaign.validfrom, '%Y-%m-%d') validfrom,
					DATE_FORMAT(campaign.validto, '%Y-%m-%d') as validto,
					sum( impressions ) AS timp, 
					sum( clicks ) AS tclicks, 
					round((sum(clicks)*100)/sum(impressions),2) as ctr,
					ro.id as roid,
					ro.name as ROName,
					ro.vdopia_ro_number,
					ro.agency_name,
					ro.client_name,
					ro.brand_name,
					ad_format 
				FROM ro  
				LEFT JOIN 
					campaign_UI ON
					 ro.id = campaign_UI.ro_id 
					 LEFT JOIN  campaign ON
						campaign.id = campaign_UI.campaign_id 
				LEFT JOIN 
					daily_stats ON 
						campaign.id = daily_stats.campaign_id 
				LEFT JOIN 
					campaign_members ON 
						campaign.id = campaign_members.cid
				LEFT JOIN 
					ads ON 
						campaign_members.ad_id = ads.id
				WHERE 
					  ro.validfrom>='$fromdate' and ro.validto<='$todate'
				GROUP BY 
					ro.id, c_id 
				order by 
					ro_id DESC,ad_format desc ";
					
			$rs=$reportConn->execute($sql);
			if($rs && $rs->recordcount()>0){
				$rosummary=$rs->getrows();
				for($i=count($rosummary)-1;$i>=0;$i--) {
					if($rosummary[$i]['ROName']=="")
						$rosummary[$i]['ROName'] ="Other";
					$rosummary[$i]['ctr'] = $rosummary[$i]['tclicks']*100/$rosummary[$i]['timp'];	
					$rosummary[$i]['budget_remaining'] = $rosummary[$i]['budget_amt']-$rosummary[$i]['tot_money_spent'];	
					$rosummary[$i]['imp_target'] = round($rosummary[$i]['budget_amt']/$rosummary[$i]['price_per_unit']);	
					$rosummary[$i]['imp_remaining'] = round($rosummary[$i]['budget_amt']/$rosummary[$i]['price_per_unit'])-$rosummary[$i]['timp'];	
					$totaldays = (strtotime($rosummary[$i]['validto']) - strtotime($rosummary[$i]['validfrom'])) / (60 * 60 * 24);
					$remaining_days = (strtotime($rosummary[$i]['validto']) - strtotime(date("Y-m-d"))) / (60 * 60 * 24);
					$consume_days = (strtotime(date("Y-m-d")) - strtotime($rosummary[$i]['validfrom'])) / (60 * 60 * 24);
					$rosummary[$i]['remaining_days'] = $remaining_days;	
					$rosummary[$i]['avg_daily_delivery'] = $rosummary[$i]['timp']/$consume_days;	
					$rosummary[$i]['target_daily_delivery'] = round($rosummary[$i]['imp_target']/$totaldays);	
					$rosummary[$i]['delivery_variation'] =((($rosummary[$i]['avg_daily_delivery']-$rosummary[$i]['target_daily_delivery'])*100)/$rosummary[$i]['target_daily_delivery']); 	
									
					if(!isset($strname)) $strname=$rosummary[$i]['ROName'];
					   if($rosummary[$i]['ROName']==$strname){
				         	$totalrosummary[$rosummary[$i]['ROName']]['timp_target'] += $rosummary[$i]['imp_target'];    
					   		$totalrosummary[$rosummary[$i]['ROName']]['timp_delivered'] += $rosummary[$i]['timp'];
				            $totalrosummary[$rosummary[$i]['ROName']]['tclicks'] += $rosummary[$i]['tclicks'];
				          }
				          else {
				            $totalrosummary[$rosummary[$i]['ROName']]['timp_target'] += $rosummary[$i]['imp_target'];    
					   		$totalrosummary[$rosummary[$i]['ROName']]['timp_delivered'] += $rosummary[$i]['timp'];
				            $totalrosummary[$rosummary[$i]['ROName']]['tclicks'] += $rosummary[$i]['tclicks'];
				           $rosummary[$i]['ROName'];
				            
				          }
					}			
				}
			
			$report_header=array();
			$report_header[]=array('Report Generated on', date('d-m-Y H:i T'));
			$report_header[]=array("Start Date",$startdate);
			$report_header[]=array("End Date",$enddate);
			$report_header[]=array("Channel",ucfirst($channel));
			$report_header[]=array("Campaign",ucfirst($campaign));
			$report_header[]=array("Country",ucfirst($country));
			$report_header[]=array("State",ucfirst($state));
			$report_header[]=array("Ad Format",ucfirst($adtype)); 	
			$_SESSION['header'] = $report_header;
			
			
			$_SESSION['header'] = "RO Performance Report";
			$_SESSION['data'] = $rosummary;
			$smarty->assign('data',$rosummary);
			$smarty->assign('totalrosummary',$totalrosummary);
			//echo "<pre>";
			//print_r($totalrosummary);
    	
      break;
    case 'createro':
			
			if($_REQUEST['ro_action']!="")
			{
			// import the validation library
			  $rules = array(); // stores the validation rules
			
			  // standard form fields
			  $rules[] = "required,roname,<br>This field is required.";
			  $rules[] = "required,start_date,<br>This field is required.";
			  $rules[] = "required,end_date,<br>This field is required.";
			 
			  if(empty($error)){
				  $error = validateFields($_POST, $rules);
				  $smarty->assign("error",$error);
				  $_REQUEST['formErrors']=$error;
				  //echo "<pre>";
				  //print_r($_REQUEST);
				    $flag=TRUE;
				  if($_REQUEST['rofile']!=""){
					  $files_arr = explode(",", $_REQUEST['rofile']);
					
					  foreach($files_arr as $k =>$v){
					  	$frompath=$config['tempfilepath']."/".$v;
					  	$topath=$config['AD_RO_DIR']."/".$v;
						 // exit;
						  if(!rename($frompath,$topath)){
							$flag= FALSE;
							break;
						  }
					  }
				  }
				  if($flag==TRUE)
				  {
				  	//$vdopia_ro_number = "vdopia#".time();
				  	$roname = requestParams("roname");
					$start_date=requestParams('start_date');
					$end_date=requestParams('end_date');
					$agency_name=requestParams('agency_name');
					$agency_contact = requestParams('agency_contact');
					$agency_ro_number=requestParams('agency_ro_number');
					$client_name=requestParams('client_name');
					$brand_name=requestParams('brand_name');
					$budget=requestParams('budget');
					$currency=requestParams('currency');
					$agency_commission=requestParams('agency_commission');
					$volume_discount=requestParams('volume_discount');
					$net_ro_amount=requestParams('net_ro_amount');
					$sales_person=requestParams('sales_person');
					$region=requestParams('region');
					$category = requestParams('category');
					$advertiser_id = requestParams('advertiser_id');
					$rofile = requestParams('rofile');
					
					if( $start_date!=NULL && $start_date!="") {
						$start_date=date("Ymd",strtotime($start_date));
						$startdateSql="'$start_date', ";
					} else {
						$startdateSql="NULL,";
					}
					
					if( $end_date!=NULL && $end_date!=""){
						$end_date=date("Ymd",strtotime($end_date));
						$enddateSql="'$end_date', ";
					} else {
						$enddateSql="NULL,";
					}
					
					$sql = "insert into field_entries set field_name = 'ro_agency_name', field_value = '$agency_name' on duplicate key update field_name = 'ro_agency_name',field_value = '$agency_name', last_used	= now()";
					$rs=$conn->execute($sql);
					$sql = "insert into field_entries set field_name = 'ro_client_name',field_value = '$client_name' on duplicate key update field_name = 'ro_client_name', field_value = '$client_name', last_used	= now()";
					$rs=$conn->execute($sql);
					$sql = "insert into field_entries set field_name = 'ro_brand_name',field_value = '$brand_name' on duplicate key update field_name = 'ro_brand_name', field_value = '$brand_name', last_used	= now()";
					$rs=$conn->execute($sql);
					$sql = "insert into field_entries set field_name = 'ro_sales_person', field_value = '$sales_person' on duplicate key update field_name = 'ro_sales_person', field_value = '$sales_person', last_used= now()";
					$rs=$conn->execute($sql);
					$roid = requestParams("roid");
					if($_REQUEST['ro_action']=="create"){
						$prefix= "insert into ro set";
						$postfix = ", advertiser_id = '$advertiser_id'";
						$msg = "RO with name $roname has been successfully created";
					}
					elseif($_REQUEST['ro_action']=="update"){
						
						$prefix= "update ro set";
						$postfix = ",last_update=now() where id=$roid";
						$msg = "RO with name $roname has been successfully saved";
					}
					if($rofile!="")
						$rosql = ",files = '$rofile'";
					else
						$rosql = " ";
					
						$sql = "$prefix
							name = '$roname',
							validfrom = $startdateSql
							validto = $enddateSql
							agency_name= '$agency_name',
							agency_contact = '$agency_contact',
							agency_ro_number ='$agency_ro_number',
							agency_commission ='$agency_commission',
							volume_discount = '$volume_discount',
							net_ro_amount = '$net_ro_amount',
							client_name ='$client_name',
							brand_name ='$brand_name',
							sales_person ='$sales_person',
							currency ='$currency',
							budget ='$budget',
							region = '$region',
							category = '$category'
							$rosql
							$postfix";
					//exit;
					$rs=$conn->execute($sql);
					//exit;
					if($rs) {
						doForward("$config[baseurl]/vadmins/index.php?page=tools&sub=managero&msg=$msg");		
					}else {
						$msg = "Some problem with RO creation please try again";
						doForward("$config[baseurl]/vadmins/index.php?page=tools&sub=managero&msg=$msg");
					}
					
				  }
			  }
			}
			
  			if(isset($_REQUEST['id']) && $_REQUEST['id']!=""){
	    		$roid = requestParams("id");
	    		$sql = "select * from ro where id='$roid'";
				$rs = $conn->execute($sql);
				if($rs && $rs->recordcount()>0){
					//If user already attached with logged advertiser.
					while(!$rs->EOF) {
						$_REQUEST['roname'] = $rs->fields['name'];
						$_REQUEST['start_date'] = date("M d Y",strtotime($rs->fields['validfrom']));
						$_REQUEST['end_date'] = date("M d Y",strtotime($rs->fields['validto']));
						$_REQUEST['agency_name'] = $rs->fields['agency_name'];
						$_REQUEST['agency_contact'] = $rs->fields['agency_contact'];
						$_REQUEST['agency_ro_number'] = $rs->fields['agency_ro_number'];
						$_REQUEST['client_name'] = $rs->fields['client_name'];
						$_REQUEST['brand_name'] = $rs->fields['brand_name'];
						$_REQUEST['currency'] = $rs->fields['currency'];
						$_REQUEST['budget'] = $rs->fields['budget'];
						$_REQUEST['agency_commission'] = $rs->fields['agency_commission'];
						$_REQUEST['volume_discount'] = $rs->fields['volume_discount'];
						$_REQUEST['net_ro_amount'] = $rs->fields['net_ro_amount'];
						$_REQUEST['sales_person'] = $rs->fields['sales_person'];
						$_REQUEST['region'] = $rs->fields['region'];
						$_REQUEST['rofiles'] = $rs->fields['files'];
						$_REQUEST['category'] = $rs->fields['category'];
						$rs->movenext();
					}	
				}
	    		$smarty->assign('roid',$roid);
	    		$smarty->assign('ro_action_value',"update");
	    	}
	    	else{
	    		$vdopia_ro_number = "vdopia#".time();
	    		$smarty->assign('ro_action_value',"create");
	    	}
			$sql = "select concat(fname,' ' ,lname) as name ,email, id from advertiser where type='online' order by name";    
		    $rs = $conn->execute($sql);
		     if($rs && $rs->recordcount()>0){
		    		$advlist = $rs->getrows();
		    		
		    }
		    // Fetch ageny name entered in database for auto fill
  			$sql = "select field_value from field_entries where field_name='ro_agency_name' order by field_value";    
		    $rs = $conn->execute($sql);
		     if($rs && $rs->recordcount()>0){
		    		$autofill_agency_name = $rs->getrows();		
		    }
		    foreach ($autofill_agency_name as $k =>$val ) {
		     	$agency_name[] = $val['field_value'];
		    }    
		    $json_agency_name = json_encode($agency_name);
		     $smarty->assign("json_agency_name",$json_agency_name);
		     
		    // Fetch Client name entered in database for auto fill
  			$sql = "select field_value from field_entries where field_name='ro_client_name' order by field_value";    
		    $rs = $conn->execute($sql);
		     if($rs && $rs->recordcount()>0){
		    		$autofill_client_name = $rs->getrows();		
		    }
		    foreach ($autofill_client_name as $k =>$val ) {
		     	$client_name[] = $val['field_value'];
		    }    
		    $json_client_name = json_encode($client_name);
		     $smarty->assign("json_client_name",$json_client_name);
		     
		    // Fetch Brand name entered in database for auto fill
  			$sql = "select field_value from field_entries where field_name='ro_brand_name' order by field_value";    
		    $rs = $conn->execute($sql);
		     if($rs && $rs->recordcount()>0){
		    		$autofill_brand_name = $rs->getrows();		
		    }
		    foreach ($autofill_brand_name as $k =>$val ) {
		     	$brand_name[] = $val['field_value'];
		    }    
		    $json_brand_name = json_encode($brand_name);
		     $smarty->assign("json_brand_name",$json_brand_name);
		     
		     
		      // Fetch sales person name entered in database for auto fill
  			$sql = "select field_value from field_entries where field_name='ro_sales_person' order by field_value";    
		    $rs = $conn->execute($sql);
		     if($rs && $rs->recordcount()>0){
		    		$autofill_sales_name = $rs->getrows();		
		    }
		    foreach ($autofill_sales_name as $k =>$val ) {
		     	$sales_name[] = $val['field_value'];
		    }    
		    $json_sales_name = json_encode($sales_name);
		    $smarty->assign("json_sales_name",$json_sales_name);
		    $smarty->assign("advlist",$advlist);
			$smarty->assign("regionoptions",$regionoptions);
			$smarty->assign("categoryoptions",$categoryoptions);
      		
      break;
      case 'viewro':
	   	 	$id = requestParams("id");
  			$sql = "select * from ro where id='$id'";
			$rs=$conn->execute($sql);
			if($rs && $rs->recordcount()>0){
			$rodetails = $rs->getrows();
				$smarty->assign("rodetails",$rodetails);
			}
			//echo "<pre>";
			//print_r($rodetails);
		break;
      		
      break;
      case 'editRO':
      		echo "vdopia Helth"; 
      break;
      case 'roreport':
  			if($_REQUEST['roreport_action']!="" && $_REQUEST['roreport_action']=="roreport"){
          		$fromdate 	= $_REQUEST['fromdate'];
          		$todate 	= $_REQUEST['todate'];
        	}
        	else{
          		$fromdate = date("Y-m-d", mktime(0, 0, 0, date("m")-1 , 1, date("Y")));
          		$todate = date("Y-m-d", mktime(0, 0, 0, date("m")+1 , date("d")-date("d"), date("Y")));
          		$_REQUEST['fromdate']=$fromdate;
          		$_REQUEST['todate']=$todate;
        	}
    		$sql = "select id ,name from ro";
		    $rs = $conn->execute($sql);
		     if($rs && $rs->recordcount()>0){
		    	$rolist = $rs->getrows();
		     	$smarty->assign("rolist",$rolist);		
		    }
        	$roid = $_REQUEST['ro'];
        	$dateField = "date";
        	
        	$period = $_REQUEST['period'];
  			switch ($_REQUEST['period']){
				case "daily":
					$group = " group by ";
					$aggregate=", day($dateField), month($dateField), year($dateField) ";
					$aggregateNames=", $dateField as timeframe ";
					$agcolNames="Date,";
					$orderby=", $dateField DESC";
					break;
				case "monthly":
					$group = " group by ";
					$aggregate=", month($dateField),year($dateField) ";
					$aggregateNames=", concat_ws(' ', monthname($dateField), year($dateField)) as timeframe ";
					$agcolNames="Timeframe,";
					$orderby=", $dateField DESC";
					break;
				case "quarterly":
					$group = " group by ";
					$aggregate=", quarter($dateField),year($dateField) ";
					$aggregateNames=", concat_ws(' ', concat('Q',quarter($dateField)), year($dateField)) as timeframe ";
					$agcolNames="Timeframe,";
					$orderby=", quarter($dateField), year($dateField)";
					break;
				case "dayofweek":
					$group = " group by ";
					$aggregate=", dayofweek($dateField), year($dateField) ";
					$aggregateNames=", date_format($dateField, '%a') as timeframe ";
					$agcolNames="Day,";
					$orderby=", dayofweek($dateField), year($dateField)";
					break;
				case "weekly":
					$group = " group by ";
					$aggregate=", week($dateField), year($dateField) ";
					$aggregateNames=", concat('Week','-',week($dateField)+1) as timeframe ";
					$agcolNames="Week,";
					$orderby=", week($dateField), year($dateField)";
					$measure = "and measure='wuniqvi' ";
					break;
				case "yearly":
					$group = " group by ";
					$aggregate=", year($dateField) ";
					$aggregateNames=", year($dateField) as timeframe ";
					$agcolNames="Year,";
					$orderby=", year($dateField)";
					break;
				case "summary":
					$group = " group by ";
					$aggregateNames=", 'Summary' as timeframe ";
					$aggregateNames="";
					$orderby="";
					break;
				default:
					$aggregate=" ";
					$aggregateNames="";
					$agcolNames="";
					$orderby="";
					break;
			}
        		$sql = "SELECT 
					campaign.name,  
					campaign.id AS cid,
					sum( impressions ) AS totimpressions, 
					sum( clicks ) AS totclicks, 
					round((sum(clicks)*100)/sum(impressions),2) as ctr,
					sum(revenue_advcurrency *ex_rate_advpub) as revenue, 
          			sum(revenue_advcurrency *ex_rate_advpub*1000)/sum(impressions) as eCPM, 
					ro.id as roid,
					ro.name as ROName
					$aggregateNames
					
				FROM ro  
				LEFT JOIN campaign_UI ON
					 	ro.id = campaign_UI.ro_id 
				LEFT JOIN  campaign ON
						campaign.id = campaign_UI.campaign_id 
				LEFT JOIN daily_stats ON 
						campaign.id = daily_stats.campaign_id 
				WHERE 
					daily_stats.date>='$fromdate' 
					and daily_stats.date<='$todate'
					and campaign_UI.ro_id IS NOT NULL
					and ro.id='$roid'
				$group ro.id, cid $aggregate
				order by 
					ROName,campaign.name DESC $orderby ";	
					    
				$rs = $reportConn->execute($sql);		
				if($rs && $rs->recordcount()>0){
					$summary = $rs->getrows();
				}
				if($period == "summary"){
					
					$_SESSION['period']='summary';
					$_SESSION['data']=$summary;
					$smarty->assign("data",$summary);
				}
			else{
				foreach ( $summary as $key => $value ){
						$data[$value['name']][] = $value; 	
				}
				
				$_SESSION['period']=$period;
				$_SESSION['data']=$data;
				$smarty->assign("data",$data);
			}
        		
      break;
  }
} else {
	doForward("$config[BASEURL]/vadmins/?page=login&desturl=$_SERVER[REQUEST_URI]");
}
?>