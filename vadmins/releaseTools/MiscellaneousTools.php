<?php

class MiscellaneousTools{
  /**
   * Verify third party trackers - Miscellaneous Tools
   * @param $method
   * @return unknown_type
   */
  public function verifytrackers($method){
    $this->run($method);
  }
  
  
  /**
   * This function execute the task that defined into each tools
   * @param $method
   * @return unknown_type
   */
  private function run($method){
    require_once ABSPATH . "/common/portal/tools/miscellaneousTools/misc/$method.php";
    $obj = new $method();
    $obj->$method();
  }
}

?>
