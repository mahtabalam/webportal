<?php
require_once ABSPATH . "/common/portal/VdopiaPortalObject.php";

class verifytrackers extends VdopiaPortalObject {
  private $cacheBusting=array("<%RAND%>", "[timestamp]");
  private $newWindoUrl="http://newwindow.vdopia.com";
  
  public function __construct(){
    parent::__construct();
  }
  
  public function verifytrackers(){
    global $config;
    if($_REQUEST['action_verifytracker']){
      $imp_tracker_url=$_REQUEST['imp_tracker_url'];
      $clk_tracker_url=$_REQUEST['clk_tracker_url'];
      
      if(isset($clk_tracker_url) && strlen($clk_tracker_url)>0){
        $linkedStatus=$this->verifyLinkedTrackers($clk_tracker_url); 
      }else{
        $linkedStatus=0;
      }
      
      $this->smarty->assign("isVerifiedImp",$this->verifyTimestamp($_REQUEST['imp_tracker_url']));
      $this->smarty->assign("isVerifiedClk",$this->verifyTimestamp($_REQUEST['clk_tracker_url']));
      $this->smarty->assign("imp_tracker_url", $imp_tracker_url);
      $this->smarty->assign("clk_tracker_url", $clk_tracker_url);
      $this->smarty->assign("preview", $clk_tracker_url);
      $this->smarty->assign("linkedStatus", $linkedStatus);
      $this->smarty->assign("newWindowUrl", $this->createNewWindow($clk_tracker_url));
      $this->smarty->assign("cacheBusting",implode(',',$this->cacheBusting));
    }else{
      $this->smarty->assign("preview", $config['baseurl']."/tips.php?id=click_tracker");
      $this->smarty->assign("cacheBusting",implode(',',$this->cacheBusting));
    }
  }
  
  private function verifyTimestamp($url){
    $isVerified=0;
    foreach ($this->cacheBusting as $key => $val){
      $pos = strpos($url, $val);
      if($pos){    # i assume cachebusting token never will be on 0th position of tracker url
        $isVerified=1;
        break;
      }
    }
    return $isVerified;
  }
  
  private function verifyLinkedTrackers($clk_url){
    $httpStatus=$this->getHttpStatus($clk_url);
    if($httpStatus==204){
      return 1;
    }elseif($httpStatus==302){
      return 0;
    }
    return 2;
  }
  
  private function getHttpStatus($url){
    $ch = curl_init();
    $timeout = 10;
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
    $data = curl_exec($ch);
    $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    return $httpStatus;
  }
  
  private function createNewWindow($url){
    return $this->newWindoUrl."/?u=$url";
  }
}

?>
