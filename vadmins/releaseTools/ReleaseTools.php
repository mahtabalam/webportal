<?php
session_start();
include_once '../../include/config.php';

require_once ABSPATH . "/common/portal/VdopiaPortalObject.php";


class ReleaseTools extends VdopiaPortalObject {

  protected $releaseDirectories;
  protected $fromRsyncPrefix;
  protected $toDirectoryBase;
  protected $toDirectoryBaseLn;
protected $fromDirectoryBase;
protected $toRsyncPrefix=array();
 protected $rsyncuser="future5";
   protected $rsyncpassword="Vdopiaf5rs122";
  public function __construct() {
    parent::__construct ();
    global $config;
    $rsyncuser="future5";
    $rsyncpassword="Vdopiaf5rs122";

	
    $this->fromRsyncPrefix="RSYNC_PASSWORD=$rsyncpassword rsync  --progress --verbose --stats --compress -R --recursive --links --exclude \"*bak\" --exclude \"*.svn\"      $rsyncuser@i2.vdopia.com::";
    if(isset($config['BACKUP_MACHINES']))
    {
      foreach($config['BACKUP_MACHINES'] as $vals)
      {
        $this->toRsyncPrefix[]="RSYNC_PASSWORD=$rsyncpassword rsync  --progress --verbose --stats --compress  -R --recursive --links --exclude \"*bak\" --exclude \"*.svn\" ";
      }
    }
   $this->toDirectoryBase=$config['BASE_DIR'].'/advertisements';
    $this->toDirectoryBaseLn=$config['BASE_DIR'].'/a';
    $this->fromDirectoryBase=$config['BASE_DIR'].'/advertisements';    
    //Note:: The below thing needs to be released only when "admin" table contains "email_id" field.
 //   $sql = "select releaseDirectories,email_id from admin where id='$_SESSION[ADMIN_ID]'";
    $sql = "select releaseDirectories,Login from admin where id='$_SESSION[ADMIN_ID]'";
    
    $rs=$this->conn->execute($sql);
    $this->releaseDirectories=NULL;
    
    if($rs && $rs->recordcount()>0 && $rs->fields['releaseDirectories']!=NULL) {
     $_SESSION['email_id']=($rs->fields['Login']?$rs->fields['Login']."@vdopia.com":'');
      $this->releaseDirectories=split(',', $rs->fields['releaseDirectories']);
      for($i=0; $i < count($this->releaseDirectories); $i++){
         $this->releaseDirectories[$i] = trim($this->releaseDirectories[$i]);
      }
    }
    else
      $this->smarty->assign('msg', 'No Directories allocated');
    
    $releaseToolData = array();
    $releaseToolData['fromDirectoryBase'] = $this->fromDirectoryBase;
    if(!isset($_SESSION['releaseToolData']))
    $_SESSION['releaseToolData'] = $releaseToolData;

  }

  private function getDirectory( $prefixURL, $baseDir,$path){
    $retval=array();

    $ignore = array( 'cgi-bin', '.', '..' , 'Thumbs.db'); 
    // Directories to ignore when listing output. Many hosts 
    // will deny PHP access to the cgi-bin. 

    $dh = @opendir( $path );
    	 
    // Open the directory to the handle $dh 

    while( false !== ( $file = readdir( $dh ) ) ){ 
      // Loop through the directory 

      if( !in_array( $file, $ignore ) ){ 
        // Check that this file is not to be ignored 

        if( is_dir( "$path/$file" ) ){ 
          // Its a directory, so we need to keep reading down... 
		
          $retval=array_merge($retval, $this->getDirectory( $prefixURL, $baseDir, "$path/$file" )); 
          // Re-call this same function but on a new directory. 
          // this is what makes function recursive. 

        } 
	else { 
          $retval[]=str_replace($baseDir,$prefixURL,  $path)."/$file";

        } 

      } 

    } 

    closedir( $dh ); 
    // Close the directory handle 
    return $retval;
  } 

  private function checkDirName($releaseDir){
    $pattern = '/^\//'; 
    if(preg_match($pattern, $releaseDir)>0){
      return "Matched leading / - $releaseDir";
    }
    $pattern = '/\.\./'; 
    if(preg_match($pattern, $releaseDir)>0){
      if(stripos($releaseDir,"js")===false)
      return "Matched .. - $releaseDir";
    }
    $pattern = '/ /'; 
    if(preg_match($pattern, $releaseDir)>0){
      return "Matched space - $releaseDir";
    }
    if($releaseDir==''){
      return "Matched blank - $releaseDir";
    } 
    if($releaseDir=='.'){
      return "Matched current - $releaseDir";
    } 
    return NULL;
  }

  private function iterateFiles(DirectoryIterator $it,$parentString) {

    $ignore = array( 'cgi-bin', '.', '..' , 'Thumbs.db'); 
    $result="";
    $data="";
    $havingChildren="";
    $x=0;
    $y=0; 
    foreach ($it as $key => $child) {

      if ($child->isDot()) {
        continue;
      }

      $name = $child->getBasename();
      if(in_array( $name, $ignore ) || substr($name, 0, 1) == '.')
        continue;
      $fullName = $child->getPathname();
      if ($child->isDir()) {
        if($y!=0)
          $havingChildren.=","; 
        $y++;
        $subit = new DirectoryIterator($child->getPathname());
        $path = ($parentString=='')?$name:$parentString.'/'.$name;
        $havingChildren.="{\"data\": {\"title\":\"$name\"},\"attr\":{\"mdata\":\"$path\"},\"status\":\"closed\"";

        $temp=$this->iterateFiles($subit,$path);
        if(strlen($temp))
          $havingChildren.=",\"children\":[$temp]}";  
        else
          $havingChildren.="}";

      } else {
       if($x!=0)
        {   
          $data.=",";
        }
        $x++;
        global $config;
      //  $url = "/dev/pankaj_adserver/images/_drive.png";
        $url = $config['IMG_URL']."/_drive.png";
        $path = ($parentString=='')?$name:$parentString.'/'.$name;  
        $data.="{\"data\":";
        $data.="{\"title\":\"$name\",\"icon\":\"$url\"},";
        $data.="\"attr\":{\"mdata\":\"$path\"},";
        $data.="\"status\":\"closed\"}";
      }
    }

    if(strlen($data))
    {
      $result.=$data;
      if(strlen($havingChildren))
        $result.=","; 
    }
    if(strlen($havingChildren))
      $result.=$havingChildren;  
    return $result;
  }

  private function iterateDirectory(DirectoryIterator $it,$parentString) {

    $ignore = array( 'cgi-bin', '.', '..' , 'Thumbs.db'); 
    $result="";
    $data="";
    $havingChildren="";
    $x=0;
    $y=0; 
    foreach ($it as $key => $child) {

      if ($child->isDot()) {
        continue;
      }

      $name = $child->getBasename();
      if(in_array( $name, $ignore ) || substr($name, 0, 1) == '.')
        continue;
      $fullName = $child->getPathname();
      if ($child->isDir()) {
        if($y!=0)
          $havingChildren.=","; 
        $y++;
        $subit = new DirectoryIterator($child->getPathname());
        $path = ($parentString=='')?$name:$parentString.'/'.$name;
        
        $havingChildren.="{\"data\":\"$name\",\"attr\":{\"mdata\":\"$path\"},\"status\":\"closed\"";

        $temp=$this->iterateDirectory($subit,$path);
        if(strlen($temp))
          $havingChildren.=",\"children\":[$temp]}";  
        else
          $havingChildren.="}";

      } else {
      /*  if($x!=0)
        {   
          $data.=",";
        }
        $x++;
        $path = ($parentString=='')?$name:$parentString.'/'.$name;  
        $data.="{\"data\":";
        $data.="\"$name\",";
        $data.="\"attr\":{\"mdata\":\"$path\"},";
        $data.="\"status\":\"closed\"}";*/
      }
    }

/*    if(strlen($data))
    {
      $result.=$data;
      if(strlen($havingChildren))
        $result.=","; 
    }*/
    if(strlen($havingChildren))
      $result.=$havingChildren;  
    return $result;
  }
  

public function getDbData()
{
  $login = $_REQUEST['login'];
 
  $sql = "select releaseDirectories from admin where Login='$login'";
  $rs = $this->conn->execute($sql);
  if($rs && $rs->recordcount()>0)
  {

    return $rs->fields['releaseDirectories'];
  }
  return '';
}
  
  public function getJsonEncodedDirectory($path,$iterateFiles=false)
  {
    $it=new DirectoryIterator($path); 
    $str="";
    $fullname=$_REQUEST['releaseDirectory'];
    $name=$it->getPath();
    $str.="[{\"data\":\"$fullname\",\"attr\":{\"mdata\":\"$fullname\"},\"status\":\"closed\",\"children\":[";
    if($iterateFiles===true)
      $str.=$this->iterateFiles($it,$fullname);
    else
    $str.=$this->iterateDirectory($it,$fullname);
    
    $str.="]";
    $str.="}]";
    return $str;  
  }

 public function run() {
   $this->smarty->assign('releaseDirectories',$this->releaseDirectories);
   $this->smarty->assign('output','Kindly select a file or a directory to publish'); 
   if($_REQUEST['doPublish']=='Publish' 
        && $_REQUEST['releaseDirectoryName1']!='' 
        && in_array($_REQUEST['releaseDirectoryName1'], $this->releaseDirectories)==TRUE) 
    {
		
 	if($_REQUEST['selectedFiles1']=='' || $_REQUEST['selectedFiles1']=='selectedFiles')
	{
		$this->smarty->assign('output',"Kindly select a file or a directory to publish");
		
	}
	    	
       
  else {
	
	$selectedFiles = split(',',$_REQUEST['selectedFiles1']);
//	$finalFilesToBeReleased = $this->getFilesDirectoriesToPublish($selectedFiles);
	if(count($selectedFiles)>0)
	{	
 //   int prev =0;
    global $config;
    $output="";
    ob_start();
    $command = array();
    for($j=0;$j<count($selectedFiles);$j++)
    {
      if($j<count($selectedFiles)-1)
      {
        if($j%10==0)
        {
          $cmd=$this->fromRsyncPrefix;
          $cmd.="'";
          $cmd.="future5/".$selectedFiles[$j]." ";
          unset($command);
          foreach($this->toRsyncPrefix as $key=>$paths)
          {
            $command[] = $paths." ".$selectedFiles[$j]." ";

          }
          
       //   prev++;

        }
        else if($j%10==9)
        {
          $cmd.="future5/".$selectedFiles[$j]."'";
          $cmd.=" ".$this->toDirectoryBase. " 2>&1";
      //    ob_start();
          passthru("$cmd",$return);
          $output.=ob_get_contents();
          foreach($this->toRsyncPrefix as $key=>$paths)
          {
            $command[$key] .=" ".$selectedFiles[$j]." ".$this->rsyncuser."@".$config['BACKUP_MACHINES'][$key]."::future5";
            passthru("cd $this->fromDirectoryBase ; " .$command[$key]." >/tmp/tmp.txt");
          }
         
       //   ob_end_clean();
          $output.="\nReturn=$return\n";

        }
        else
        {
          $cmd.="future5/".$selectedFiles[$j]." ";
          foreach($this->toRsyncPrefix as $key=>$val)
          {
            $command[$key] .=" ".$selectedFiles[$j]." ";
          }
        }


      }
      else	
      {
        if($j%10==0)
        {
          $cmd=$this->fromRsyncPrefix;
          $cmd.="'";
          unset($command);
          foreach($this->toRsyncPrefix as $key=>$val)
          {
            $command[]=$val;
          }
        }
        $cmd.="future5/".trim($selectedFiles[$j]) ."'";
        foreach($this->toRsyncPrefix as $key=>$val)
        {
          $command[$key] .=" ".$selectedFiles[$j]." "; 
        }
      }
    }
    $cmd.=" ".$this->toDirectoryBase. " 2>&1";
//	  ob_start();
		passthru("$cmd",$return);
		$output.=ob_get_contents();
    
		ob_end_clean();
		$output.="\nReturn=$return\n";
    foreach($this->toRsyncPrefix as $key=>$val)
    {
      $command[$key] .= $this->rsyncuser."@".$config['BACKUP_MACHINES'][$key]."::future5";
      passthru("cd $this->fromDirectoryBase; ".$command[$key]." >/tmp/tmp.txt");
    }
   	$this->smarty->assign('output', $output);
	}
	else
		$this->smarty->assign('output',"No valid files were selected");
      }
   } 
  
    if($_REQUEST['doPurge']=='Purge' 
       && $_REQUEST['releaseDirectoryName2']!='' 
        && in_array($_REQUEST['releaseDirectoryName2'], $this->releaseDirectories)==TRUE ) {
      global $config;
      $checkStr = $this->checkDirName($_REQUEST['releaseDirectoryName2']); 
      if( $checkStr != NULL ){
        $this->smarty->assign('output', "Bad directory name, no '..', leading /, spaces allowed.".$checkStr);
      }
      else  
      {

        if($_REQUEST['selectedDirs1']=='' || $_REQUEST['selectedDirs1']=='selectedDirs')
        {
          $this->smarty->assign('output',"Kindly select directories to purge");
        }

        else {

          $selectedDirs = split(',',$_REQUEST['selectedDirs1']);
         //NETDNA Purge 
          

          for($j=0;$j<count($selectedDirs);$j++)
          {
            $this->netDnaPurgeURLs($this->getDirectory($config['NETDNA_CDN_URL'], $config['BASE_DIR'], $this->toDirectoryBase."/".$selectedDirs[$j]));
            $this->netDnaPurgeURLs($this->getDirectory($config['NETDNA_CDN_URL'], $config['BASE_DIR'], $this->toDirectoryBaseLn."/".$selectedDirs[$j]));
          }

          //AKAMAI Purge  
          $output.="\nThe Results of Akamai Purge:\n";
          for($j=0;$j<count($selectedDirs);$j++)
          {
            $output.=$this->uploadFile(str_replace($config['BASE_DIR'], $config['AK_CDN_URL'],$this->toDirectoryBase."/".$selectedDirs[$j]));
            $output.=$this->uploadFile(str_replace($config['BASE_DIR'], $config['AK_CDN_URL'],$this->toDirectoryBaseLn."/".$selectedDirs[$j]));
          }
          
          $fullDirsA=array();$fullDirsAdv=array();
          foreach($selectedDirs as $sd){
            $fullDirsA[]=$this->toDirectoryBaseLn."/".$sd."/*";
            $fullDirsAdv[]=$this->toDirectoryBase."/".$sd."/*";
          }

          require_once ABSPATH . "/common/bin/ECPurge.php";
          $ECob=new EdgeCastPurge();
          $output.="\nThe Results of EdgeCast Purge:\n";
              $output.=$ECob->purge(str_replace($config['BASE_DIR'], $config['EC_CDN_URL'],$fullDirsA)); 
              $output.=$ECob->purge(str_replace($config['BASE_DIR'], $config['EC_CDN_URL'],$fullDirsAdv));
         
          require_once ABSPATH . "/common/bin/BGPurge.php";
          $BGob=new BitGravityPurge();
          $output.="\nThe Results of BitGravity Purge:\n";
              $output.=$BGob->purge(str_replace($config['BASE_DIR'], $config['BG_CDN_URL'],$fullDirsA));
              $output.=$BGob->purge(str_replace($config['BASE_DIR'], $config['BG_CDN_URL'],$fullDirsAdv));

         $outFooter="\n\nYou can use the following urls to access the content on CDN. Please note that these are directory urls, therefore you need to append the filename in them to access a file.\n";
         $inUrls=str_replace($config['BASE_DIR'], $config['CDN_URL_IN'],$fullDirsA);
         $nonInUrls=str_replace($config['BASE_DIR'], $config['CDN_URL_NON_IN'],$fullDirsA);
         $outFooter.="\nIndia's CDN URLs \n";
          foreach($inUrls as $iu)
              $outFooter.=$iu."\n";
         $outFooter.="\nOutside India CDN URLs \n";
            foreach($nonInUrls as $iu)
               $outFooter.=$iu."\n";
          $output.=$outFooter; 

          $this->smarty->assign('output', $output);
        }

      }
    }


    if($_REQUEST['doPurgeFiles']=='Purge' 
       && $_REQUEST['releaseDirectoryName3']!='' 
        && in_array($_REQUEST['releaseDirectoryName3'], $this->releaseDirectories)==TRUE ) {
      global $config;
      $checkStr = $this->checkDirName($_REQUEST['releaseDirectoryName3']); 
      if( $checkStr != NULL ){
        $this->smarty->assign('output', "Bad directory name, no '..', leading /, spaces allowed.".$checkStr);
      }
      else  
      {
      	$selectedDirs = split(',',$_REQUEST['selectedFiles2']);
        if($_REQUEST['selectedFiles2']=='' || $_REQUEST['selectedFiles2']=='selectedFiles')
        {
          $this->smarty->assign('output',"Kindly select files to purge");
        }elseif(sizeof($selectedDirs)>10) {
        	$msg="You can select only 10 objects at a time. Please try again...";
        	$this->smarty->assign('output',$msg);
        }else {

         // $selectedDirs = split(',',$_REQUEST['selectedFiles2']);
         //NETDNA Purge 
         
          $listOfFiles=array();
          for($j=0;$j<count($selectedDirs);$j++)
          {
            $listOfFiles[]=str_replace($config['BASE_DIR'],$config['NETDNA_CDN_URL'],$this->toDirectoryBase."/".$selectedDirs[$j]);
            $listOfFiles[]=str_replace($config['BASE_DIR'],$config['NETDNA_CDN_URL'],$this->toDirectoryBaseLn."/".$selectedDirs[$j]);
            //   $this->netDnaPurgeURLs($this->getDirectory($config['NETDNA_CDN_URL'], $config['BASE_DIR'], $this->toDirectoryBase."/".$selectedDirs[$j]))."\n";
            //  $this->netDnaPurgeURLs($this->getDirectory($config['NETDNA_CDN_URL'], $config['BASE_DIR'], $this->toDirectoryBaseLn."/".$selectedDirs[$j]))."\n";
          }
          $this->netDnaPurgeURLs($listOfFiles);
          unset($listOfFiles);
            
          //AKAMAI Purge  
          $output.="\nThe Results of Akamai Purge:\n";
          for($j=0;$j<count($selectedDirs);$j++)
          {
            $listOfFiles[]=str_replace($config['BASE_DIR'], $config['AK_CDN_URL'],$this->toDirectoryBase."/".$selectedDirs[$j]);
            $listOfFiles[]=str_replace($config['BASE_DIR'], $config['AK_CDN_URL'],$this->toDirectoryBaseLn."/".$selectedDirs[$j]);
            //   $output.=$this->uploadFile(str_replace($config['BASE_DIR'], $config['AK_CDN_URL'],$this->toDirectoryBase."/".$selectedDirs[$j]));
            //   $output.=$this->uploadFile(str_replace($config['BASE_DIR'], $config['AK_CDN_URL'],$this->toDirectoryBaseLn."/".$selectedDirs[$j]));
          }
          $output.=$this->purgeURLs($listOfFiles);

           $fullDirsA=array();$fullDirsAdv=array();
          foreach($selectedDirs as $sd){
            $fullDirsA[]=$this->toDirectoryBaseLn."/".$sd;
            $fullDirsAdv[]=$this->toDirectoryBase."/".$sd;
          }
          
          require_once ABSPATH . "/common/bin/ECPurge.php";
          $ECob=new EdgeCastPurge();
          $output.="\nThe Results of EdgeCast Purge:\n";
              $output.=$ECob->purge(str_replace($config['BASE_DIR'], $config['EC_CDN_URL'],$fullDirsA));
              $output.=$ECob->purge(str_replace($config['BASE_DIR'], $config['EC_CDN_URL'],$fullDirsAdv));

          require_once ABSPATH . "/common/bin/BGPurge.php";
          $BGob=new BitGravityPurge();
          $output.="\nThe Results of BitGravity Purge:\n";
              $output.=$BGob->purge(str_replace($config['BASE_DIR'], $config['BG_CDN_URL'],$fullDirsA));
              $output.=$BGob->purge(str_replace($config['BASE_DIR'], $config['BG_CDN_URL'],$fullDirsAdv));
         
         $outFooter="\n\nYou can use the following urls to access the content on CDN\n";
         $inUrls=str_replace($config['BASE_DIR'], $config['CDN_URL_IN'],$fullDirsA);
         $nonInUrls=str_replace($config['BASE_DIR'], $config['CDN_URL_NON_IN'],$fullDirsA);
         $outFooter.="\nIndia's CDN URLs \n";
          foreach($inUrls as $iu)
              $outFooter.=$iu."\n";
         $outFooter.="\nOutside India CDN URLs \n";     
            foreach($nonInUrls as $iu)
               $outFooter.=$iu."\n";
          $output.=$outFooter;
          $this->smarty->assign('output', $output);
        }

      }
    }
    
    if($_REQUEST['doUpdate']=='Update'  && $_REQUEST['loginName']!="" && getPrivilegesFor("updatingDirectory")) {
      $sql="update admin set releaseDirectories='".mysql_escape_string($_REQUEST['directories'])."' where login='".mysql_escape_string($_REQUEST['loginName'])."'";
      $rs=$this->conn->execute($sql);
      $output="Updated " .  $this->conn->Affected_Rows(). " rows";
      if($this->conn->Affected_Rows() > 0)
      {
          $this->releaseDirectories = explode(',',$_REQUEST['directories']);
          $this->smarty->assign('releaseDirectories',$this->releaseDirectories);
      }
      
      $this->smarty->assign('output', $output);
    }

 /*   if($this->releaseDirectories!=NULL){
   //   $formdata="<form method='POST'>Select directory to publish<select name='releaseDirectory'>";
    //  $formdata.="<option value='Select Directory'>Select Directory</option>";
	
      $formdata="Select directory to publish<select name='releaseDirectory' id='releaseDirectory'>";
      $formdata.="<option value='Select Directory'>Select Directory</option>";
      foreach($this->releaseDirectories as $key=>$value) {
        $formdata.="<option value='$value'>$value</option>";
      }
      $formdata.="</select>";
      $formdata.="&nbsp;&nbsp;<button type='button' name='doBrowseFiles' id='BrowseFiles'>BrowseFilesToPublish</button>";
      $formdata.="&nbsp;&nbsp;<button type='button' name='doBrowseFilesForPurge' id='BrowseForPurge'>BrowseDirectoriesToPurge</button>";
      
      $formdata.="<div id='dialog' title='FileBrowser' style='visibility:hidden'>";
      $formdata.="<div id='child1' align='center'>";
     $formdata.="<img src='releaseTools/snoopy-14.gif' id='progress'/>";
      $formdata.="<p style='text-align:center'>Waiting for the request to complete!!</p>";
      $formdata.="</div>";
      $formdata.="<div id='child2'></div>";
      $formdata.="</div>";
    
      $formdata.="<div id='dialog1' title='FileBrowser' style='visibility:hidden'>";
      $formdata.="<div id='child3' align='center'>";
     $formdata.="<img src='releaseTools/snoopy-14.gif' id='progress1'/>";
      $formdata.="<p style='text-align:center'>Waiting for the request to complete!!</p>";
      $formdata.="</div>";
      $formdata.="<div id='child4'></div>";
      $formdata.="</div>";
      
      $formdata.="<div id='alertBox' title='selectedFiles'  style='visibility:hidden'>";
      $formdata.="</div>";
      $formdata.="<br>";
      $formdata.="<form method='POST'>";
      $formdata.="<input type='submit' name='doPublish' value='Publish'/>";
      $formdata.="<input type='submit' name='doPurge' value='Purge'/>";
      $formdata.="<input id='selectedFiles' type='hidden' name='selectedFiles' value='selectedFiles'/>";
      $formdata.="<input id='selectedDirs' type='hidden' name='selectedDirs' value='selectedDirs'/>";
      $formdata.="<input id='releaseDirectoryName' type='hidden' name='releaseDirectoryName' value='releaseDirectoryName'/>";
      	 	
   //   $formdata.="<input id='target' type='submit' name='doBrowseFiles' value='BrowseFiles'/>";
     $formdata.="</form>";
      $this->smarty->assign('formdata', $formdata);
    }
*/
   if(getPrivilegesFor("updatingDirectory")) {

    //  $sql="select login, releaseDirectories from admin";
      $sql="select login from admin";
      $rs=$this->conn->execute($sql);
      
      if($rs && $rs->recordcount()>0)
      {
        $login = $rs->getrows();
      $this->smarty->assign('login', $login);
      }
   /*   $formdata="<table>";
      while($rs && !$rs->EOF) {
        $formdata.="<form method='POST'><tr><td>";
        $formdata.=$rs->fields['login']."</td><td><input type='text' name='directories' value='".$rs->fields['releaseDirectories']."'/>";
        $formdata.="<input type='hidden' name='login' value='".$rs->fields['login']."' />";
        $formdata.="<input type='submit' value='Update'/>";
        $formdata.="<input name='doUpdate' value='Yes' type='hidden'/></td></tr></form>";
        $rs->movenext();
      }
      $formdata.="</table>";*/
   //   $this->smarty->assign('adminformdata', $formdata);
    }
  }

}
// Request to return the json data format for the recursive listing of the files of the selected directory by making a server call to i2's GetJsonData.php
if(isset($_REQUEST['getJsonData']) && isset($_REQUEST['releaseDirectory'])) 	
{
  $url="http://i2.vdopia.com/dev/pankaj_adserver/vadmins/GetJsonData.php"; //for time being this is being called from i2	
  //$url=$config['WWW_URL']."/vadmins/GetJsonData.php";
  $fieldString = "releaseDirectory=".$_REQUEST['releaseDirectory'];
  $ch = curl_init();
  curl_setopt($ch,CURLOPT_URL,$url);
  curl_setopt($ch,CURLOPT_HEADER,0);
  curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
  curl_setopt($ch,CURLOPT_POST,1);
  curl_setopt($ch,CURLOPT_POSTFIELDS,$fieldString);
  $data=curl_exec($ch);
  curl_close($ch);

//	$data = $releaseObject->getJsonEncodedDirectory($path);
//  sleep(3);
	echo $data;
}

// To retrieve the directory structure in json format
if(isset($_REQUEST['getJsonDataDir']) && isset($_REQUEST['releaseDirectory']))   
{
  $releaseObject = new ReleaseTools();
//  $releaseObject=unserialize($_SESSION['releaseObject']);
  $path = $_SESSION['releaseToolData']['fromDirectoryBase'];    
  $path .= '/'.$_REQUEST['releaseDirectory'];// set the proper path .

  $data = $releaseObject->getJsonEncodedDirectory($path);
//  sleep(3);
  echo $data;
}

if(isset($_REQUEST['getJsonDataFiles']) && isset($_REQUEST['releaseDirectory']))   
{
  $releaseObject = new ReleaseTools();
//  $releaseObject=unserialize($_SESSION['releaseObject']);
  $path = $_SESSION['releaseToolData']['fromDirectoryBase'];    
  $path .= '/'.$_REQUEST['releaseDirectory'];// set the proper path .

  $data = $releaseObject->getJsonEncodedDirectory($path,true);
//  sleep(3);
  echo $data;
}


if(isset($_REQUEST['getDirectory']) && isset($_REQUEST['login']))
{

  $releaseObject = new ReleaseTools();
  $data =$releaseObject->getDbData();
  echo $data;

}


?>
