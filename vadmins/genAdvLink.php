<?php

include_once '../include/config.php';
require_once("$config[BASE_DIR]/include/function.php");
include_once("../adserver/servfuncs.php");
require_once('adminfuncs.php');


if(is_admin_loggedin()) {
	$validfor=$_POST['validfor'];
	$redirurl=$_POST['redirurl'];
	$cc=$_POST['country_code'];
	if($validfor>14400)
		echo "Invalid maximum";
	else if($validfor!="" && $redirurl!="" && $cc!="") {
		$baseurl="http://serve.vdopia.com/adserver/advlink.php?r=".urlencode($redirurl)."&c=$cc";

		$now=gmmktime();
		$then=$now+$validfor;

		if(strchr($baseurl,'?')!=null) {
			$tosign=substr($baseurl, strpos($baseurl,'?')+1);
			$tosign="$tosign&v=$then";
			$sign=simpleSign($tosign);
			echo "The following url is valid for $validfor seconds<br>";
			echo "The following url redirects to $redirurl <br>";
			echo "The following url will show ads in country $cc<br>";
			echo "Please test and send the following URL:<br>";
			echo "<center><i>$baseurl&v=$then&t=$sign</i></center>";
		} else {
			echo "Q not in place cant really help you\n";
		}
	} else {
		echo "<center><strong>Please fill in the required values correctly</strong></center>";
	}
} else {
	doForward("$config[BASE_URL]/vadmins");
}
?>

<form method='POST' >
Please enter country code:  (e.g. IN for India, GB for United Kingdom, US for United States)
<br>
<input type='text' name='country_code' value="<?php echo $cc;?>" />
<br>
Please enter the final url:
<br>
<input type='text' name='redirurl' value="<?php echo $redirurl;?>" />
<br>
Please enter how long from now (in seconds) this should be valid for maximum of 4 hours:
<br>
<input type='text' name='validfor' value="<?php echo $validfor;?>" />
<br>
<input type='submit'/>
</form>
