<?php
session_start();
include_once '../include/config.php';
require_once("$config[BASE_DIR]/include/validation.php");
require_once("$config[BASE_DIR]/include/function.php");
require_once('adminfuncs.php');
require_once("$config[BASE_DIR]/include/smartyBlockFunction.php");
$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);

if(is_admin_loggedin()) {
	
	$cid=getRequest('campaign_id');
	$sql="select campaign.name, if(date(validfrom) > date(now()), 'Yet Not Started', 'OK' ) as startstat, if(date(validto) < date(now()), 'Expired', 'OK' ) as endstat, date_format(campaign.validfrom,'%d %M %Y') as validfrom, date_format(campaign.validto, '%d %M %Y') as validto, country_codes, state_codes, postalcodes, status, review_status, credit_amt, channel_choice, price_norm from campaign left join adv_credit on adv_credit.adv_id=campaign.advertiser_id left join geography on geography.campaign_id=campaign.id where campaign.id='$cid'";
    
	$rs=$conn->execute($sql);
	if($rs&& $rs->recordcount()>0) {
		$sqlb="select if(find_in_set('$cid', orcs_list), 'true', 'false') as isLimit from orcs_lists where chid=0";
		$rsb=$conn->execute($sqlb);
		echo "<p align='center'><b><u>Current Date: ".date('d M Y')."</u></b></p>";
		echo "<br>Checking all this campaign(<b>".$rs->fields['name'].": ".$rs->fields['price_norm']."</b>) campaign life span: <b>".$rs->fields['validfrom']." to ".$rs->fields['validto']."</b> is targeted to:<br><br>";
		
		# status
		if($rs->fields['status']!='active') {
			echo "<b><font color='red'>Status not fine: ". $rs->fields['status']."</font></b><br>";
		} else {
			echo "<b><font color='green'>Status OK: ". $rs->fields['status']."</font></b><br>";
		}
		
		# review status
		if($rs->fields['review_status']!='Approved') {
			echo "<b><font color='red'>Review Status Invalid for live site: ". $rs->fields['review_status']."</font></b><br>";
		} else {
			echo "<b><font color='green'>Review Status Status OK: ". $rs->fields['status']."</font></b><br>";
		}
		
		# budget
		if($rsb->fields['isLimit']=="true") {
			echo "<b><font color='red'>Checking budget, ... Exceeded</font></b><br>";
		} else {
			echo "<b><font color='green'>Checking budget, ... Budget OK</font></b><br>";
		}
		
		# daily limit
		if($rsb->fields['isLimit']=="true") {
			echo "<b><font color='red'>Daily Limit Exceeded</font></b><br>";
		} else {
			echo "<b><font color='green'>Daily Limit OK</font></b><br>";
		}
		
		# credit amount
		if($rs->fields['credit_amt']<5*$rs->fields['price_per_unit']) {
			echo "<b><font color='red'>Credits exhausted</font></b><br>";
		}elseif($rs->fields['credit_amt'] <= 0 ){
			echo "<b><font color='red'>Credits exhausted</font></b><br/>";
			echo "<b><font color='red'>Credit Amount: ".$rs->fields['credit_amt']."</font></b><br/>";
		} else {
			echo "<b><font color='green'>Credits OK</font></b><br>";
		}
		
		# start date
		if($rs->fields['startstat']=="OK") $clr="<b><font color='green'>"; else $clr="<b><font color='red'>";
		echo "<br>$clr Start Date Status: ".$rs->fields['startstat']."</font></b><br>";
		
		# end date
		if($rs->fields['endstat']=="OK") $clr="<b><font color='green'>"; else $clr="<b><font color='red'>";
		echo "$clr End Date Status: ".$rs->fields['endstat']."</font></b><br><br>";
		
		# geography
		echo "<b>Geography country:". $rs->fields['country_codes']."<br>";
		echo "Geography states:". $rs->fields['state_codes']."<br>";
		echo "Geography postalcodes:". $rs->fields['postalcodes']."</b><br>";

		$sql1="select distinct(ad_format) as adformat from ads,campaign_members where ads.id=campaign_members.ad_id and campaign_members.cid=$cid";
		$rs1=$conn->execute($sql1);

		if($rs1 && $rs1->recordcount()>0) {
			$adformat=$rs1->fields['adformat'];
		}

		$sql="select campaign_limits_rt.*, channels.name From campaign_limits_rt left join channels on chid=channels.id where cid=$cid";
		$rs3=$conn->execute($sql);

		function print_why(&$array) {
			for($i=0;$i<count($array);$i++) {
				$channel_id=$array[$i]['chid'];
				if($channel_id=="") {
					$channel_id='NULL';
				}
				if($array[$i]['maxTotalImps']!='' &&  $array[$i]['maxTotalImps']<$array[$i]['currTotalImps']) {
					$why[$channel_id]=$array[$i][name]." Exceeded total allocated impressions(CPM): Fix Budget<br>";
				}
				if($array[$i]['maxTotalClicks']!='' &&  $array[$i]['maxTotalClicks']<$array[$i]['currTotalClicks']) {
					$why[$channel_id]=$array[$i][name]." Exceeded total allocated clicks(CPC): Fix Budget<br>";
				}

				if($array[$i]['maxDailyImps']!='' &&  $array[$i]['maxDailyImps']<$array[$i]['currDailyImps']) {
					$why[$channel_id]=$array[$i][name]." Exceeded allocated daily impressions(CPM): Fix Budget<br>";
				}
				if($array[$i]['maxDailyClicks']!='' &&  $array[$i]['maxDailyClicks']<$array[$i]['currDailyClicks']) {
					$why[$channel_id].=$array[$i][name]." Exceeded allocated daily clicks(CPC): Fix Budget<br>";
				}
			}
			return $why;
		}

		$campaign_limits=$rs3->getRows();
		$why=print_why($campaign_limits);
		echo $why['NULL'];


		if($rs->fields['channel_choice']!=null) {
			$sql2="select name,apikey, channels.id, ${adformat}_unit_price, margin, ((${adformat}_unit_price*100/(100-margin))/(select ex_rate from currency where code in (select currency from publisher where id=channels.publisher_id) order by updated desc limit 0,1)) as minprice_norm from channels left join  publishersheet on isnull(validto) and channel_id=channels.id where channels.id in(".$rs->fields['channel_choice'].")";
			$rs2=$conn->execute($sql2);
			while($rs2 && !$rs2->EOF ) {
				echo $why[$rs2->fields['id']];
				if($rs2->fields['minprice_norm']>$rs->fields['price_norm']) {
					echo "Problem with pricing on channel:". $rs2->fields['name']. "(".$rs2->fields['apikey'].") margin:" . $rs2->fields['margin']. "${adformat}_unit_price:".$rs2->fields["${adformat}_unit_price"]." minprice_norm:".$rs2->fields['minprice_norm']."<br>";
				} else {
					echo "Pricing OK for :".$rs2->fields['name']. "(".$rs2->fields['apikey'].")<br>";
				}
				$rs2->movenext();
			}
		} else {
			echo "Will not check all the channels<br>";

		}

		
		
	}
} else {
	doForward("$config[BASEURL]/vadmins/?page=login&desturl=$_SERVER[REQUEST_URI]");
}

?>

