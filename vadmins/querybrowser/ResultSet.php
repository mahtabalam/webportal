<?php

class ResultSet{
	
	private $query;
	private $columNum;
	private $rowNum;
	private $resultSet;
	private $columnMetaData;
	private $smarty;
	
	public function __construct(&$smarty){
		$this->smarty=$smarty;
		$this->resultSet=array();
	}
	
	/**
	 * main function to be called to parse the result set recieved from the query
	 */
	public function parseData(&$result){
				if(!$result)return;
				$this->columnNum=$result->FieldCount();
				$this->columnMetaData=$this->getColumnMetaData($result,$this->columnNum);
				$this->rowNum=$result->RecordCount();
				$row=0;
				while (!$result->EOF) {
					
					$this->resultSet[$row]=array();
					$count=0;
					foreach($result->fields as $key=>$value){
						$this->resultSet[$row][$count]=$value;
						$count++;
					}
					$row++;
					$result->MoveNext();
				}
				$result->Close();
				$this->assignSmarty();
				
	}
	
	private function assignSmarty(){
			$this->smarty->assign('columnNum',$this->columnNum);
				$this->smarty->assign('rowNum',$this->rowNum);
				$this->smarty->assign('metaData',$this->columnMetaData);
				$this->smarty->assign('resultset',$this->resultSet);
				$this->smarty->assign('rowNum',$this->rowNum);
				
	}

	
	/**
	 * This will return the name of the columns as defined in the sql query or from the table
	 * @return array of the column names
	 */
	private function getColumnMetaData($recordSet,$columnNum)
	{
		$columnMetaData=array();			
		for($i=0;$i<$columnNum;$i++){
		 $columnMetaData[$i]=$recordSet->FetchField($i)->name;	
		}
		return $columnMetaData;
	}
}