<?php 

class QueryBrowser{


	private $error;
	private $data;
	/**
fetchin these values from the global variable
**/
	private $connection;
	private $smarty;
	
	private $cacheData;
	public  $tableName;
	private $queryConn;
	
	private function createConnectionReport() {
		$DBTYPE = 'mysql';
		$DBHOST = SConfig::get ( "Database_QB_Report", "host" );
		$DBUSER = SConfig::get ( "Database_QB_Report", "user_name" );
		$DBPASSWORD = SConfig::get ( "Database_QB_Report", "password" );
		$DBNAME = SConfig::get ( "Database_QB_Report", "db_name" );
		$conn = &ADONewConnection ( $DBTYPE );
		$conn->PConnect ( $DBHOST, $DBUSER, $DBPASSWORD, $DBNAME );
		$this->tableName = savedqueriesreport;
		return $conn;
	}
	   
    private function createConnectionLive() {
        $DBTYPE = 'mysql';
        $DBHOST = SConfig::get ( "Database_QB", "host" );
        $DBUSER = SConfig::get ( "Database_QB", "user_name" );
        $DBPASSWORD = SConfig::get ( "Database_QB", "password" );
        $DBNAME = SConfig::get ( "Database_QB", "db_name" );
        
        $conn = &ADONewConnection ( $DBTYPE );
        $conn->PConnect ( $DBHOST, $DBUSER, $DBPASSWORD, $DBNAME );
        $this->tableName = savedquerieslive;
        return $conn;
    }
	
	public function __construct(){
		global $smarty,$conn;
		$this->smarty=&$smarty;
		$this->queryConn=$conn;		
	}
	
	
	private function setConnection($dbType) {
		if ($dbType == 'live') {
			$this->connection = $this->createConnectionLive ();
			$this->cacheData = &$_SESSION ['querybrowserlive'];
		} else {
			$this->connection = $this->createConnectionReport ();
			$this->cacheData = &$_SESSION ['querybrowserreport'];
		}
		$this->connection->SetFetchMode ( ADODB_FETCH_NUM );
	}
	
	private function showQueries(){
		$searchtext=$_REQUEST['searchtext'];
		if($_REQUEST['action_search']!=''){
			if($searchtext!=''){
				$sqlsnip=" where Name like '%$searchtext%'";
			}else $sqlsnip="";
		}
		 $sql=$this->connection->Prepare("SELECT $this->tableName.id,$this->tableName.query,$this->tableName.queryname,admin.Name,$this->tableName.userid FROM $this->tableName LEFT JOIN admin ON $this->tableName.userid=admin.id$sqlsnip;");
		$result = $this->connection->Execute($sql);
		if (!$result){
			$this->smarty->assign('error',$this->connection->ErrorMsg());
			exit;
		}else{
			include_once("ResultSet.php");
			if($result->recordcount()>0){
			$res=$result->getrows();
			foreach($res as $key => $queries){
				foreach($queries as $idx => $val){
					if($idx==0) $index="id";
					if($idx==1) $index="query";
					if($idx==2) $index="name";
					if($idx==3) $index="username";
					if($idx==4) $index="userid";
					if($idx==1)
					{
						$tempArray[$index]=htmlentities($val);
					}
					else
					{
					  $tempArray[$index]=$val;
					}
				}
				$res[$key]=$tempArray;
				}
			}
		}
		$this->smarty->assign('savedqueries',$res);
	}
	public function run($dbType){ 
		
		$flag=0;
		$this->setConnection($dbType);
		$columnNum=0;$rowNum=0;
	    $metaData=array();
		$resultset=array();
		$tableinfo=$this->getTables($this->connection);	
		
		//have to add checkj credentials.May be we can add this in the steup tabs.so that query broswer is not displayed to those we dont want to show
		if(isset($_REQUEST['runsubmit'])){
			$sql=$this-> clearQuery($_REQUEST['query']);
			$sqs=substr($sql,-400);
			$sqlLog = "insert into query_log set tstamp=now(),admin_id=".$_SESSION ['ADMIN_ID'].",query='".addslashes($sqs)."',success=0";
			$this->queryConn->Execute($sqlLog);
		    $result = $this->connection->Execute($sql);
			if (!$result){
				$this->smarty->assign('error',$this->connection->ErrorMsg());
			}else{
				include_once("ResultSet.php");
				$resultSet=new ResultSet($this->smarty);
				$resultSet->parseData($result);
				$sql=htmlentities($sql);
				$this->addQueryToCache($sql);
			}
			$this->smarty->assign('query',$_REQUEST['query']);
			
		}
		$this->smarty->assign('tables',$tableinfo);
		$this->smarty->assign('previousQuery',array_slice($this->cacheData['QB_previousQueries'],0,5));
		
		global $config;
		$this->smarty->assign('baseurl',$config['BASE_URL']);
		
		if(isset($_REQUEST['editsubmit'])){
			$_SESSION['editFlag']=1;
			$_SESSION['editQuery']=$this->clearQuery($_REQUEST['query']);
			$_SESSION['id']=$_REQUEST['id'];
			
		}
		if(isset($_REQUEST['savesubmit'])){
		$val = $_REQUEST['query'];
		$user = $_SESSION[ADMIN_ID];
	    $queryname = $_POST["QueryName"];
	    if($_SESSION['editFlag'] == 1)
	    {
	    	$_SESSION['editFlag']=0;
	    	$editQuery = $_SESSION['editQuery'];
	    	$id = $_SESSION['id'];
	    	
	    	$sql="delete from $this->tableName where  id = $id  and userid = $_SESSION[ADMIN_ID];";
	    	$result = $this->queryConn->Execute($sql);
	    
	    }
	    $sqlval = mysql_real_escape_string($val);
	    if($sqlval!='Run your query')
	    {
	    	$sql="insert into $this->tableName (query,userid,queryname) VALUES ('$sqlval','$user',' $queryname')";
	    	$result = $this->queryConn->Execute($sql);
			if (!$result){
				$this->smarty->assign('error',$this->connection->ErrorMsg());
			}else{
				$this->smarty->assign('error',"Query saved successfully");
			}
	    }
		
		}
		
		if(isset($_REQUEST['delsubmit'])){
			$delquery=$this-> clearQuery($_REQUEST['query']);
			$id=$_REQUEST['id'];
			$sql="delete from $this->tableName where id = $id and userid = $_SESSION[ADMIN_ID]";
		    $result = $this->queryConn->Execute($sql);
			if (!$result){
				$this->smarty->assign('error',$this->connection->ErrorMsg());
				exit;
			}else{
				$this->smarty->assign('error',"Query deleted Successfully");
			}
		 }
		$this->showQueries();
}
	private function clearQuery($query){
		$query= preg_replace('/[\r\n\s]+/xms', ' ', trim($query));
            $query=rtrim($query,";");
            return $query;
	}
	
	public function getResult($query){
		$query=$this->clearQuery($query);
		$result = $this->connection->Execute($query);
		return $result->GetRows();
	}
	
	/**
	 * We will store the curent offset n the session
	 *
	 * 
	 **/
	private function fetchResults($sql){
	
	}
	/**
	 * This will return the name of the columns as defined in the sql query or from the table
	 * @return array of the column names
	 */
	private function getColumnMetaData($recordSet,$columnNum)
	{
		$columnMetaData=array();			
		for($i=0;$i<$columnNum;$i++){
		 $columnMetaData[$i]=$recordSet->FetchField($i)->name;	
		}
		return $columnMetaData;
	}
	
	/**
	 * This function returns an array of the tables of hecurrent DB.
	 *
	 * @param unknown_type $connection
	 * @return unknown
	 */
	private function getTables($connection){
		if(isset($this->cacheData['meta_tables'])==false){
			$this->cacheData['meta_tables']=$connection->MetaTables('TABLES');
		}
		return  $this->cacheData['meta_tables'];
	}
	
	
	/**
	 * it will store at return previous queries executed the a particular user on a session
	 *
	 * @param unknown_type $query
	 */
	private function addQueryToCache($query){
		$sessionQuery=&$this->cacheData['QB_previousQueries'];
		if(!isset($sessionQuery)){
			$sessionQuery=array();
			$sessionQuery[$query]=time();
		}
		else{
			$sessionQuery[$query]=time();
			arsort($sessionQuery);
			if(count($sessionQuery)>10){
				//$sessionQuery=arsort($sessionQuery);
				array_pop($sessionQuery);
			}
		}
		return $sessionQuery;
		
	}
}

?>
