<?php

include_once(dirname(__FILE__)."/../include/config.php");
include_once(dirname(__FILE__)."/../include/function.php");
include_once(dirname(__FILE__)."/../common/include/fancy.php");
//require_once ABSPATH . "/common/portal/VdopiaPortalObject.php";
/*
global $portal, $adTypearr;
	//$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);
    
	
	if($_REQUEST['id'])
	{
		
		if( $portal == 'ivdopia' )
	    {	//'tracker'=>'Tracker',
	   		$adTypeArr = array('banner'=>'Banner','vdobanner'=>'Mini Video','preinter'=>'Pre-App Initerstitial','ininter'=>'In-App Interstitial','preapp'=>'Preapp','inapp'=>'Inapp','html'=>'HTML','static'=>'Static','leadervdo'=>'LeaderVDO');
	    }
	    else 
	    {
	    	$adTypeArr = array('preroll'=>'Preroll Video','midroll'=>'Midroll Video','postroll'=>'Post Video','overlay'=>'Overlay','branded'=>'Branded');	
	    }
	    //var_dump($_SESSION);
	    $priceArr = array('CPM'=>'CPM','CPC'=>'CPC');
	    $countrylist = getCountryList();
		$smarty->assign("countrylist", $countrylist);
		$smarty->assign("adTypeArr", $adTypeArr);
		$smarty->assign("pricelist",$priceArr);
		//$smarty->assign(ti)
	$title = 'Enter a New Price Configuration';
		$showCPM = false;
		if($_REQUEST['id'] != -1)
		{
			$smarty->assign('showlogs',true);
			$showCPM = true;
			$channelList= getChannelsList();
			$smarty->assign("channellist",$channelList);
			$adtypeList = getAdTypeList();
			$smarty->assign("adtypeList",$adtypeList);
			$checkAll = checkAll($adTypeArr);
			$smarty->assign("checkAll",$checkAll);
			$channelprice = getPC();
			$smarty->assign("channelprice", $channelprice);
			$title = 'Edit Price Configuration';
		}
		$smarty->assign('title',$title);
		$smarty->assign('showCPM',$showCPM);
		$smarty->display('vadmins/tools/editchannelmanageprice.tpl');
	}
	else if($_REQUEST['rsid'])
	{
		$revshare = getrevshare();
		$smarty->assign("revshare", $revshare);
		
		$dateCur = new DateTime();
		
		$monthArr[] = $dateCur->format('Y').'-'.$dateCur->format('m');
		if($dateCur->format('j') < 16)
			$monthArr[] = date("Y",strtotime("-1 months")).'-'.date("m",strtotime("-1 months"));
		$smarty->assign("monthArr", $monthArr);
		$smarty->display('vadmins/tools/editrevenueshare.tpl');
	}
	else if($_REQUEST['logpcid'])
	{
		$logs = GetLogs();
		if(isset($_SESSION['undo']) && isset($_SESSION['isedit']) && isset($_SESSION['undoaction']))
		{
			$actionId = $logs[0]['action_id'];
			
			if($_SESSION['undoaction'] == $actionId)
			{
				$logs[0]['showundo'] = true;
			}
			
		}
		$smarty->assign("logs", $logs);
		
		$smarty->display('vadmins/tools/showlogs.tpl');
	}
	else if($_REQUEST['logpubid'])
	{
		$logs = GetrsLogs();
		
		$smarty->assign("logs", $logs);
		
		$smarty->display('vadmins/tools/showlogs.tpl');
	}
	


function getrevshare(){
	global $reportConn;
	
	$id = $_REQUEST['rsid'];
	$sql ="select rs.id, rev_share, email, publisher_id as pubid from revenue_share as rs inner join publisher as p on p.id = rs.publisher_id ";
	$sql .= " where rs.id = ".$id;
	$rs=$reportConn->execute($sql);

	if($rs && $rs->recordcount()>0){
		$date = new DateTime();
		while(!$rs->EOF) 
		{
			$r['price'] = $rs->fields['rev_share'];
			$r['id'] 	 = $rs->fields['id'];
			$r['cur'] = $date->format('Y').'-'.$date->format('m');
			$r['email'] = $rs->fields['email'];
			$r['pubid'] = $rs->fields['pubid'];
			break;
		}
	}
  	return $r;
}

function getChannelsList(){
	global $reportConn;
	
	$id = $_REQUEST['id'];
	$sql ="select channels.id, name, email, url from channels ,publisher where publisher_id=publisher.id and channels.id in ( select channel_id from price_config_member where price_config_id = ".$id." ) ";
	$sql .= " order by name  ";
	//echo $sql;
	$rs=$reportConn->execute($sql);
	
	$match = array('!','?','*','"','\\','<','>');

	if($rs && $rs->recordcount()>0){
		while(!$rs->EOF) {
			$name = preg_replace( '/[^[:print:]]/', '-',str_replace($match, '-', $rs->fields['name']));
			$ChannelList[$rs->fields['id']] = $name."(".$rs->fields['email'].")";
			$rs->movenext();
		}
	}
		//var_dump($ChannelList);
  	return $ChannelList;
  	
	
}


function checkAll($adTypeArr)
{
	global $reportConn;
	
	$ad = implode(',', array_keys($adTypeArr));
	$ad = str_replace(",","','",$ad);
	$id = $_REQUEST['id'];
	$sql ="select distinct adtype from price_config_member where price_config_id = ".$id;
	$sql	.=" and adtype in  ( '".$ad."' ) ";
			
	$rs=$reportConn->execute($sql);
	if($rs && $rs->recordcount()>0){
		while(!$rs->EOF) 
		{
		
			$Ad[] = $rs->fields['adtype'];
			$rs->movenext();
		}
		
	}
	if(count($Ad) == count($adTypeArr))
		return 1;
	else
		return 0;
}


function getAdTypeList()
{
	global $reportConn;
	
	$id = $_REQUEST['id'];
	$sql ="select distinct adtype from price_config_member where price_config_id = ".$id." ";
	$rs=$reportConn->execute($sql);
	if($rs && $rs->recordcount()>0){
		while(!$rs->EOF) 
		{
			$Ad[] = $rs->fields['adtype'];
			$rs->movenext();
		}
		
	}
  	return $Ad;
  	
	
}


function getPC(){
	global $reportConn;
	global $portal;
	
	$id = $_REQUEST['id'];
	$sql ="select id, price_per_unit, start_date, end_date, model, price_config.ccode, ";
	$sql .= " ( select p.email from publisher as p inner join channels as c on c.publisher_id = p.id ";
	$sql .= " inner join price_config_member as pcm on pcm.channel_id = c.id where pcm.price_config_id = ".$id." limit 1 ) as email from price_config where id = ".$id." ";
	//echo $sql;
	$rs=$reportConn->execute($sql);

	if($rs && $rs->recordcount()>0){
		
		while(!$rs->EOF) 
		{
			
			$pc['id'] 	 = $rs->fields['id'];
			//echo $rs->fields['start_date'];
			
			$pc['sdate'] = substr($rs->fields['start_date'], 5, 2).'/'.substr($rs->fields['start_date'], 0, 4);
			$pc['edate'] = substr($rs->fields['end_date'], 5, 2).'/'.substr($rs->fields['end_date'], 0, 4);
			$pc['email'] = $rs->fields['email'];
			$pc['ccode'] = $rs->fields['ccode'];
			$pc['model'] = $rs->fields['model'];
			$pc['price'] = $rs->fields['price_per_unit'];
			if($pc['model'] == 'CPM')
				$pc['price'] = $rs->fields['price_per_unit']*1000;
			break;
			
		}
	}
	if($portal=='vdopia')
		{
			$publisherType = 'online';
        }
        else
        {
          $publisherType = 'iphone';
        }
	
	$sql = " select p.currency from revenue_share as rs"
		    ." inner join publisher as p on p.id = rs.publisher_id "
		    ." where p.email ='".$pc['email']."' and type = '".$publisherType."'"
		    ." order by effective_date asc limit 1 ";
  
    $revshare = '($)';
    $rs = $reportConn->Execute($sql);
	if($rs && $rs->recordcount()>0)
	{
		while(!$rs->EOF)
		{
			if($rs->fields['currency'] == 'USD')
				$revshare = '($)';
			else
      			$revshare = '('.$rs->fields['currency'].')';
      		break;
		}
    }
    $pc['curr'] = $revshare;
  	return $pc;
}


function GetLogs()
{
	global $reportConn;
	
	$id = $_REQUEST['logpcid'];
	$sql = "select pl.id, pl.description, a.Login, pl.date_time_added, al.action_id from portal_logs as pl inner join job_type as jt on jt.id = pl.job_type_id inner join admin as a on a.id = pl.user_id left join action_log as al on al.log_id = pl.id where pl.job_id = ".$id." and jt.name = 'Channel Price' order by pl.date_time_added desc limit 7 ";
	$rs= $reportConn->execute($sql);

	if($rs && $rs->recordcount()>0)
	{
		while(!$rs->EOF) 
		{
			$pc['What'] = $rs->fields['description'];
			$pc['id'] 	 = $rs->fields['id'];
			$pc['Who'] = $rs->fields['Login'];
			$pc['Date'] = $rs->fields['date_time_added'];
			$pc['action_id'] = $rs->fields['action_id'];
			$logList[]= $pc;
			$rs->movenext();
		}
	}
  	return $logList;
}


function GetrsLogs()
{
	global $reportConn;
	
	$id = $_REQUEST['logpubid'];
	$sql = "select pl.id, pl.description, a.Login, pl.date_time_added from portal_logs as pl inner join job_type as jt on jt.id = pl.job_type_id inner join admin as a on a.id = pl.user_id where pl.job_id = ".$id." and jt.name = 'Revenue Share' order by pl.date_time_added desc limit 7 ";
	$rs= $reportConn->execute($sql);

	if($rs && $rs->recordcount()>0){
		
		while(!$rs->EOF) 
		{
			$pc['What'] = $rs->fields['description'];
			$pc['id'] 	 = $rs->fields['id'];
			$pc['Who'] = $rs->fields['Login'];
			$pc['Date'] = $rs->fields['date_time_added'];
			//$pc['action_id'] = $rs->fields['action_id'];
			$logList[]= $pc;
			$rs->movenext();
		}
	}
  	return $logList;
}
*/

?> 