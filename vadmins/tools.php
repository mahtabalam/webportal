<?php

if(is_admin_loggedin()){
  switch ($_REQUEST['sub']) {
    case 'managero':
     	if(!isset($_REQUEST['action'])) $_REQUEST['action']='rosummary';
     	include_once("managero.php");
      break;
    case 'test':
     	if(!isset($_REQUEST['action'])) $_REQUEST['action']='rosummary';
     	include_once("test.php");
      break;
    
    case 'vdopiahealth':
      		echo "vdopia Health";
      break;
      case 'querybrowserreport':
		include_once 'querybrowser/QueryBrowser.php';
		$queryBrowser = new QueryBrowser();
		$queryBrowser->run("report");
		break;
    case 'querybrowserlive':
        include_once 'querybrowser/QueryBrowser.php';
        $queryBrowser = new QueryBrowser();
        $queryBrowser->run("live"); 
      break;
      case 'uniqueReporting':
      	include_once 'uniqueReporting/index.php';
      	$uniqueReporting = new UniqueReporting();
      	$uniqueReporting->run();
      	break;
      case 'fraudReporting':
      	include_once 'fraudReporting/FraudReporting.php';
      	$fraudReporting=new FraudReporting();
      	$fraudReporting->run();
      	break;
      case 'vdoflo':
      	include_once 'vdoflo/vdofloStatusReporting.php';
      	$vdofloStatusReporting=new VdoFloStatusReport();
      	$vdofloStatusReporting->run();
      	break;	
      case 'release':
       include_once 'releaseTools/ReleaseTools.php';
       $releaseTools=new ReleaseTools();
  //     $_SESSION['releaseObject'] = serialize($releaseTools);
       $releaseTools->run();
       break;
      case 'salesupload':
       include_once (dirname(__FILE__) . '/salesupload/salesupload.php');
       $salesUpload=new SalesUpload();
       $salesUpload->run();
       break; 
      case 'default_campaigns':
      	  $type= "pc";
	      include_once ABSPATH.'/common/portal/campaign/DefaultCampaigns.php';
	      $defaultCampaigns=new DefaultCampaigns();
	      $defaultCampaigns->run($type);
	  break;
       case 'copyChannels':
	      include_once ABSPATH.'/common/portal/campaign/OpsTools.php';
	      $OpsTools=new OpsTools();
	      $type= "pc";
	      $OpsTools->CopyChannels($type);
	      break;
	    case 'addRemoveChannelCampaign':
	      include_once ABSPATH.'/common/portal/campaign/OpsTools.php';
	      $type= "pc";
	      $OpsTools=new OpsTools(); 
	      $OpsTools->addRemoveChannelCampaign($type);
	    break;
	   case 'screenshoturl':
	    	include_once ABSPATH.'/common/vadmins/screenshoturl.php';
	    	$obj = new ScreenshotUrl();
	    	$obj->run();
	    	break;
      case 'miscellaneous':
        $method=getRequest('misc', 0);
        if ($method=='channelmanageprice') doForward($config["BASE_URL"]."/vadmins/index.php?page=tools&sub=miscellaneous&misc=newchannelmanageprice");
	if($method=='logReader') $_REQUEST['page']='queryReporting';
	
	require_once ABSPATH . "/common/portal/tools/MiscellaneousTools.php"; 
        $miscellaneousTools=new MiscellaneousTools();
        if(method_exists($miscellaneousTools, $method)){
          $miscellaneousTools->$method($method);
          $smarty->assign("isMethod",1);
        }else{
          $smarty->assign("isMethod",0);
        }
      break;
      case "vdoquality":
                if($_SESSION['ADMIN_PRIVILEGES']=="superadmin"){
                        include_once ABSPATH.'/common/portal/tools/vdoquality.php';
			$smarty->assign("cdn_url",$config['CDN_URL']);
                        $obj = new vdoquality();
                        $obj->run();
                }else $smarty->assign("msg", "Permission Denied.");
        break;
      case 'managevadminusers':
      	if($_SESSION['ADMIN_PRIVILEGES']=="superadmin"){
	      	include_once ABSPATH.'/common/portal/tools/managevadminusers.php';
	      	if(!isset($_REQUEST['action']) && $_REQUEST['action']=="") $_REQUEST['action'] = 'list';
	      	$obj = new ManageVadminUsers();
	      	$obj->run();
      	}else $smarty->assign("msg", "Permission Denied.");
      	break;
      case 'addcapabilitytousers':
      	if($_SESSION['ADMIN_PRIVILEGES']=="superadmin"){
	      	include_once ABSPATH.'/common/portal/tools/addcapabilitytousers.php';
	      	$obj = new AddCapabilityToUsers();
	      	$obj->run();
      	}else $smarty->assign("msg", "Permission Denied.");
      	break;	
      case 'killmysqlqueries':
      		if($_SESSION['ADMIN_PRIVILEGES']=="superadmin"){
      			include_once ABSPATH.'/common/portal/tools/killmysqlqueries.php';
      			if(!isset($_REQUEST['action']) && $_REQUEST['action']=="") $_REQUEST['action'] = 'list';
      			$obj = new KillMysqlQueries();
      			$obj->run();
      		}else $smarty->assign("msg", "Permission Denied.");
      		break;
      case "killmysqlqueriesoutput":
      			
      		break;
      
      case 'getcampaigns':
	     	include_once ABSPATH.'/common/portal/campaign/GetCampaigns.php';
	     	$type = 'pc';
	     	$cli = false;
	     	$getCampaign=new GetCampaigns();
	      	$getCampaign->run($_REQUEST,$type,$cli);
      break;
      case 'search-terms';
      require_once ABSPATH ."/vadmins/search_terms_web_tool.php";
      exit; 
      case 'bt_dashboard';
      doForward($config["BASE_URL"]."/vadmins/bt_dashboard/bt_dashboard.php");
      exit; 
      case 'Accesslogs_Graphs':
      require_once  ABSPATH ."/vadmins/accesslog_graphs/PHP/DBExample/Startpage.php";
      exit;
      break;
      case 'Reportmachine_console':
      require_once  ABSPATH ."/vadmins/Reportmachine_console/Startpage.php";
      exit;
      case 'Refurl_Report':
      require_once 'refurlcount/refurl/Startpage.php';
      	$refurl=new Refurl();
      	$refurl->run();
      	break;
	 case 'channel_bt_settings':
      require_once ABSPATH.'/common/portal/channel/channel_bt_settings.php';;
      	$chan_bt=new Channel_BT_Settings();
      	$chan_bt->run();
      	break;
     case 'vpaid_certification':
     	require_once './vpaidCertification/VPAID_Player.php';
      	$vpald_plr=new VPAID_Player();
      	$vpald_plr->init();
      	break;
  }
} else {
	doForward("$config[BASEURL]/vadmins/?page=login&desturl=$_SERVER[REQUEST_URI]");
}
?>
