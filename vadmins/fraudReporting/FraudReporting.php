<?php

require_once ABSPATH . "/common/portal/VdopiaPortalObject.php";

class FraudReporting extends VdopiaPortalObject {
	
	private $startDate;
	private $endDate;
	private $timeFrame = "summary";
	private $campaign_id;
	private $channel_id;
	private $channellist;
	private $campaignlist;
	private $timeFrameArray = array ("summary", "daily", "weekly", "monthly" );
	private $db_prefix;
	
	public function __construct() {
		parent::__construct ();global $config;
		$this->startDate = &$_SESSION ['fraudReport'] ['startDate'];
		$this->endDate = &$_SESSION ['fraudReport'] ['endDate'];
		$this->timeFrame = &$_SESSION ['fraudReport'] ['timeFrame'];
		if (isset ( $this->timeFrame ) == false) {
			$this->timeFrame = "summary";
		}
		$this->campaign_id = &$_SESSION ['fraudReport'] ['campaign_id'];
		$this->channel_id = &$_SESSION ['fraudReport'] ['channel_id'];
	
		$this->campaignlist=&$_SESSION ['fraudReport'] ['campaignlist'] ;
		$this->channellist=&$_SESSION ['fraudReport'] ['channellist'] ;
		if (isset ( $this->startDate ) == false) {
			$this->endDate = date ( "Y-m-d" );
			$this->startDate = $this->getMonth ( $this->endDate );
		}
		if(isset($this->campaignlist)==false){
			$this->channellist= $this->getChannels ( $this->startDate, $this->endDate, true );
			 $this->campaignlist = $this->getCampaigns ( $this->startDate, $this->endDate, true );	
		}
		if(isset($config['DB_PREFIX']) && $config['DB_PREFIX']!='') {
			$this->db_prefix=$config['DB_PREFIX'];
		}else $this->db_prefix='';
	}
	
	private function getMonth($date) {
		$time = strtotime ( $date );
		$date = date ( "Y-m-d", mktime ( 0, 0, 0, date ( "m", $time ), date ( "d", $time ) - date ( "j", $time ) + 1, date ( "Y", $time ) ) );
		return $date;
	}
	
	private function getPeriod($period, &$periodDate, $tableName, $dateColumn) {
		switch ($period) {
			case "monthly" :
				$this->timeFrame = "monthly";
				$periodDate = "date_sub(date($tableName.$dateColumn),interval dayofmonth(date($tableName.$dateColumn))-1 day)";
				break;
			case "weekly" :
				$this->timeFrame = "weekly";
				$periodDate = "date_sub(date($tableName.$dateColumn),interval (weekday(date($tableName.$dateColumn))+1)%7 day)";
				break;
			case "daily" :
				$this->timeFrame = "daily";
				$periodDate = " date($tableName.$dateColumn) ";
				break;
		}
	
	}
	
	private function getData($channel_id, $campaign_id, $period) {
		if ($channel_id != 0) {
			$channelSql = "and aa.channel_id=$channel_id";
		}
		if ($campaign_id != 0) {
			$campaignSql = "and aa.campaign_id=$campaign_id";
		}
		$periodDateAA = "";
		$periodDateDS = "";
		
		$this->getPeriod ( $this->timeFrame, &$periodDateAA, "aa", "date" );
		$this->getPeriod ( $this->timeFrame, &$periodDateDS, "d", "date" );
		
		 $sql = "select $periodDateAA as date,sum(if(aa.measure='f_cl',aa.value,0)) as fraud_clicks,ds.clicks,sum(if(aa.measure='f_vi',aa.value,0)) as fraud_impressions,ds.impressions,ch.name as channel_name,cp.name as campaign_name,aa.channel_id,aa.campaign_id
		from {$this->db_prefix}aggregate_ads aa 
		join channels ch on aa.channel_id=ch.id 
		join campaign cp on aa.campaign_id=cp.id 
		left outer join 
			(select $periodDateDS as dsdate,campaign_id,channel_id,sum(impressions) as impressions,sum(clicks) clicks 
			from {$this->db_prefix}daily_stats d where d.date between '$this->startDate' and '$this->endDate' group by $periodDateDS,campaign_id,channel_id	
			)ds
			on $periodDateAA=ds.dsdate and ds.campaign_id=aa.campaign_id and ds.channel_id=aa.channel_id
		where aa.date between '$this->startDate' and '$this->endDate' 
		and (measure='f_vi' or measure='f_cl') 
		$channelSql $campaignSql
		group by $periodDateAA,aa.channel_id,aa.campaign_id order by aa.date";
		if ($this->timeFrame == "summary") {
			$sql = $this->summarySql ( $channelSql, $campaignSql );
		}
		$rs = $this->reportConn->execute ( $sql );
		//print_r($sql);
		if ($rs && $rs->recordcount () > 0) {
			$result = $rs->getrows ();
			return $result;
		}
	}
	
	private function summarySql($channelSql, $campaignSql) {
		$sql = "select '-' as date,sum(if(aa.measure='f_cl',aa.value,0)) as fraud_clicks,ds.clicks,sum(if(aa.measure='f_vi',aa.value,0)) as fraud_impressions,ds.impressions,ch.name as channel_name,cp.name as campaign_name,aa.channel_id,aa.campaign_id
		from {$this->db_prefix}aggregate_ads aa 
		join channels ch on aa.channel_id=ch.id 
		join campaign cp on aa.campaign_id=cp.id 
		left outer join 
			(select campaign_id,channel_id,sum(impressions) as impressions,sum(clicks) clicks 
			from {$this->db_prefix}daily_stats d where d.date between '$this->startDate' and '$this->endDate' group by campaign_id,channel_id	
			)ds
			on  ds.campaign_id=aa.campaign_id and ds.channel_id=aa.channel_id
		where aa.date between '$this->startDate' and '$this->endDate' 
		and (measure='f_vi' or measure='f_cl') 
		$channelSql $campaignSql
		group by aa.channel_id,aa.campaign_id order by aa.date";
		return $sql;
	}
	
	private function getChannels($start_date, $end_date, $flag = false) {
		$sql = "select ch.name, ch.id from channels ch  join {$this->db_prefix}aggregate_ads aa on ch.id=aa.channel_id and aa.date between '$start_date' and '$end_date'  and (aa.measure='f_vi' or aa.measure='f_cl')  group by ch.id order by ch.name";
		$rs = $this->reportConn->execute ( $sql );
		//echo $sql;
		$str = "";
		if (! $rs) {
			return;
		}
		while ( ! $rs->EOF ) {
			if ($flag == true) {
				$channellist = $rs->getRows ();
				return $channellist;
			}
			$str .= "<option value=\"" . $rs->fields [1] . "\">" . $rs->fields [0] . "</option>";
			$rs->MoveNext ();
		}
		return $str;
	}
	
	private function getCampaigns($start_date, $end_date, $flag = false) {
		 $sql = "select cp.name, cp.id from campaign cp  join {$this->db_prefix}aggregate_ads aa on cp.id=aa.campaign_id and aa.date between '$start_date' and '$end_date'  and (aa.measure='f_vi' or aa.measure='f_cl')  group by cp.id order by cp.name";
		$rs = $this->reportConn->execute ( $sql );
		//	echo $sql;
		$str = "";
		if (! $rs) {
			return;
		}
		while ( ! $rs->EOF ) {
			if ($flag == true) {
				$campaignlist = $rs->getRows ();
				return $campaignlist;
			}
			$str .= "<option value=\"" . $rs->fields [1] . "\">" . $rs->fields [0] . "</option>";
			$rs->MoveNext ();
		}
		return $str;
	}
	
	public function run() {
		if (isset ( $_POST ['set_date'] )) {
			if (isset ( $_REQUEST ['start_date'] )) {
				$this->startDate = $_REQUEST ['start_date'];
			}
			if (isset ( $_REQUEST ['end_date'] )) {
				$this->endDate = $_REQUEST ['end_date'];
			}
			$this->channellist= $this->getChannels ( $this->startDate, $this->endDate, true );
			 $this->campaignlist = $this->getCampaigns ( $this->startDate, $this->endDate, true );}
		if (isset ( $_REQUEST ['timeframe'] )) {
			$this->timeFrame = $_REQUEST ['timeframe'];
		}
		if (isset ( $_REQUEST ['campaign'] )) {
			$this->campaign_id = $_REQUEST ['campaign'];
		}
		if (isset ( $_REQUEST ['channel'] )) {
			$this->channel_id = $_REQUEST ['channel'];
		}
		$result = $this->getData ( $this->channel_id, $this->campaign_id );
		$this->smarty->assign ( 'data', $result );
		$this->smarty->assign ( 'campaignlist', $this->campaignlist);
		$this->smarty->assign ( 'channellist', $this->channellist );
		$this->smarty->assign ( 'timeframe', $this->timeFrameArray );
		$_REQUEST ['start_date'] = $this->startDate;
		$_REQUEST ['end_date'] = $this->endDate;
	
	}
}

?>
