<?php
include_once("../include/datefuncs.php");

if(is_admin_loggedin()){
  switch ($_REQUEST['sub']) {
    case 'newinventory';
      if($_REQUEST['action']=="ajaxcall"){
        if($_REQUEST['pub_id']!='all'){
          $sql = "select id, name from channels where channel_type<>'iphone' and publisher_id=".$_REQUEST['pub_id'];
          $rs = $reportConn->execute($sql);
          if($rs && $rs->recordcount()>0){
            $ch_ids = $rs->getrows();
            foreach($ch_ids as $key => $val){
              $ch_id[$val['id']] = $val['name'];
            }
            echo json_encode($ch_id);
            exit;
          }
        }
      }
      
      # export to excel
      if($_REQUEST['excelreport']=="yes"){
        $blank[]=array("");
        if($_SESSION['period']=="summary"){
          $colHead[]=array("Region", "Publisher", "Channel", "Ad Type", "Player Loads", "Ad Calls", "Call Drops", "Unfilled Inventory", "Ad Plays", "Utilization", "Efficiency", "eCPM ".$_SESSION['CURRENCYSYM']);
        }else{
          $colHead[]=array("Date", "Publisher", "Channel", "Ad Type", "Player Loads", "Ad Calls", "Call Drops", "Unfilled Inventory", "Ad Plays", "Utilization", "Efficiency", "eCPM ".$_SESSION['CURRENCYSYM']);
        }
        $_SESSION['isEI']="yes";
        exportToExcel($_SESSION['report_header'], $colHead, $_SESSION['inventoryreport'], $blank, $_SESSION['period']);
      }else{
        $_SESSION['isEI']="no";
      }
      
      # publisher list
      $sql = "select id, company_name from publisher where type='online' order by company_name";
      $rs = $reportConn->execute($sql);
      if($rs && $rs->recordcount()>0){
        $pub_ids = $rs->getrows();
        $smarty->assign("pub_ids",$pub_ids); 
      }
      
      # Country list
	  $smarty->assign("countrylist", getCountryList());
	//  $smarty->assign("baseurl", $config['seucre_baseurl']);
	  
	  # set from date & to date
      if(!isset($_REQUEST['fromdate']) && $_REQUEST['fromdate']==""){
    	$fromdate = date("Y-m-d", mktime(0, 0, 0, date("m")-1 , 1, date("Y")));
        $todate = date("Y-m-d", mktime(0, 0, 0, date("m")+1 , date("d")-date("d"), date("Y")));
	    $_REQUEST['fromdate']=$fromdate;
		$_REQUEST['todate']=$todate;
	  }
	  
      # generate report
      if($_REQUEST['act_newinventory']=="newinventory"){
        # find channel list
        if($_REQUEST['pub_id']!='all'){
          $sql = "select id, name from channels where channel_type<>'iphone' and publisher_id=".$_REQUEST['pub_id'];
          $rs = $reportConn->execute($sql);
          if($rs && $rs->recordcount()>0){
            $ch_ids = $rs->getrows();
            $smarty->assign('ch_ids',$ch_ids);
          }
        }
        
        # get parameters
        $publisher=getRequest('pub_id');
		$channel=getRequest('ch_id');
		$adtype=getRequest('adtype');
		$country=getRequest('country');
		if($country) $country=implode(',', $country); else $country='Overall';
		$period=getRequest('period');
		$startdate=getRequest('fromdate');
		$enddate=getRequest('todate');
		
		# get channel name
		if($channel=='all'){
		  $channelName="All";
		}else{
		  $ch['channel']=$channel;
		  $channelName=insert_channel_names($ch, 'excel');
		}
		
		# get publisher name
        if($publisher=='all'){
		  $publisherName="All";
		}else{
		  $publisherName=insert_publisher_names($publisher);
		}
		
		$report_header=array();
		$report_header[]=array("Inventory Report (" . ucfirst($period) . ")");
		$report_header[]=array('Report Generated on', date('d-m-Y H:i T'));
		$report_header[]=array("Start Date",$startdate);
		$report_header[]=array("End Date",$enddate);
		$report_header[]=array("Publisher",ucfirst($publisherName));
		$report_header[]=array("Channel",ucfirst($channelName));
		$report_header[]=array("Country",ucfirst($country));
		$report_header[]=array("Ad Format",ucfirst($adtype));
		
		# country lists
		$countryLists = getCountryList();
		
		# include Inventory Class
		include_once(dirname(__FILE__)."/../common/portal/report/InventoryReport.php");
		$inventory = new InventoryReport();
		$report = $inventory->report($period, $adtype, $startdate, $enddate, $publisher, $channel, $country, 'PC');
		
		$_SESSION["report_header"]=$report_header;
		$_SESSION["period"]=$period;
		$_SESSION["reporttype"]="newinventory";
		if($period!="summary") $inventoryreport[$country]=$report; else $inventoryreport=$report;
		$_SESSION["inventoryreport"]=$inventoryreport;
		
		$smarty->assign("report", $report);
      }
    break;
  }
} else {
	doForward("$config[BASEURL]/vadmins/?page=login&desturl=$_SERVER[REQUEST_URI]");
}
?>
