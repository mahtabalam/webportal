<?php
session_start();
include_once(dirname(__FILE__)."/../include/config.php");
require_once(dirname(__FILE__)."/../common/include/validation.php");
require_once(dirname(__FILE__)."/../include/function.php");
require_once(dirname(__FILE__)."/adminfuncs.php");
require_once(dirname(__FILE__)."/../include/smartyBlockFunction.php");
require_once(dirname(__FILE__)."/../common/portal/login/LoginFuncs.php");

$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);

if(isset($_REQUEST['action_admsignin'])) {
    $login=getRequest('login');
    $password=getRequest('password');
    
    $LoginFuncs = new LoginFuncs();
    if($LoginFuncs->VAdminLogin($login, $password, 'vdopia')===false){
    	$smarty->assign("error_message", "Invalid Username/Password.");
    }
}

$smarty->assign("baseurl",$config["BASE_URL"]);
setup_tabs_admins($smarty);

// the request for logging on using google.
if(isset($_REQUEST['googleLogin']) && !isset($_REQUEST['openid_mode'])){
  $LoginFuncs = new LoginFuncs();
  $LoginFuncs->googleRequest();
}

//The authenticatoin was cancelled mid way.
if(isset($_REQUEST['openid_mode']) && $_REQUEST['openid_mode']=='cancel'){
  doForward("?page=login&msg=Authentication got cancelled");
}


//If the request went through well and the user did not cancel the authentication process.
if(isset($_REQUEST['openid_mode']) && $_REQUEST['openid_mode']!='cancel'){
   $LoginFuncs = new LoginFuncs();
   $LoginFuncs->googleAuthentication();
}

if (is_admin_loggedin()) {
	switch ($_REQUEST['page']) {

		case 'tools':
		if(!isset($_REQUEST['sub'])) $_REQUEST['sub']='toolall';
		include_once("tools.php");

		break;
		
		case 'advertisers':
		if(!isset($_REQUEST['sub'])) $_REQUEST['sub']='advall';
		include_once("advertisers.php");

		break;

		case 'prescreen':
			/**
			 * Added code for fetching report time
			 * @var unknown_type
			 */
      
	if(isset($_SESSION['vadmin']['reporttime'])==false){
            try{
            	
                $sql=$conn->Prepare("select time from reporttime");
                $rs=$conn->execute($sql, array());
                if($rs && $rs->recordcount()>0){
                    $time=$rs->fields['time'];
                    $time=explode("-",$time);
                    
                    if(count($time)==5){
                        list($year,$month,$day,$hour,$min)=$time;
                        $tt="$year/$month/$day $hour:$min";
                        $_SESSION['vadmin']['reporttime']="(Last Updation of Report:$tt GMT)";
                  
                    }
                    
                }
                else{
                	 trigger_error($conn->ErrorMsg());
                }
            }catch(Exception $e){
                trigger_error($e->getMessage());
            }
            }
			 if(isset($_SESSION['vadmin']['reporttime'])==false)$_SESSION['vadmin']['reporttime']='';
            $smarty->assign('reporttime', $_SESSION['vadmin']['reporttime']);
            $sql=$conn->Prepare("select campaign.id, campaign.name, campaign.status, campaign.review_status, campaign.budget_amt, campaign.daily_limit, concat(advertiser.fname,' ',advertiser.lname) as advertiser_name, advertiser.email from campaign join advertiser on campaign.advertiser_id=advertiser.id where campaign.status='active' and campaign.review_status='Pending Review' and campaign.device!='iphone' order by advertiser_name, campaign.id");
			$rs=$conn->execute($sql, array());
			if($rs && $rs->recordcount()>0)
				$smarty->assign('campaignsummary', $rs->getRows());
			break;
		case 'viewcamp':
		$campaign_id=getRequest('id', true);
    $_REQUEST['campaign_id']=$campaign_id;
		$reason_codes = array('Select Reason'=>'Select Reason','Adult Content'=>'Adult Content', 'Competing Product/Service'=>'Competing Product/Service',
'Violent Content'=> 'Violent Content', 'Other'=>'Other');

		if($_REQUEST['action_dis_approve']=='do') {
			if($_REQUEST['test']=='Test') {
				$sql= $conn->Prepare("update campaign set review_status='Test' where review_status='Pending Review'	and id=?");
				$rs = $conn->execute($sql, array($campaign_id));
				$rows =  $conn->Affected_Rows();
				if($rows>0){
					$msg= "Campaign has been set as Test successfully";

				}else{
					$msg = "No changes made";
				}
				$_REQUEST['next']='true';
			} else if(preg_match('/^Approve/', $_REQUEST['approve']) == 1) {
				$sql= $conn->Prepare("update campaign set review_status='Approved' where review_status='Pending Review' and id = ?");
				$rs = $conn->execute($sql, array($campaign_id));
				$rows =  $conn->Affected_Rows();
				if($rows>0){
					$msg= "Campaign has been approved successfully";

				}else{
					$msg = "No changes made";
				}
				$_REQUEST['next']='true';
			} else if($_REQUEST['disapprove']=='Disapprove' && $_REQUEST['reason_code']!="Select Reason"
					&& ($_REQUEST['reason_code']!='Other' || $_REQUEST['reasontext']!="")
				) {
				$sql= $conn->Prepare("update campaign set review_status='Disapproved' where review_status='Pending Review' and id=?");
				$rs = $conn->execute($sql, array($campaign_id));
				$rows =  $conn->Affected_Rows();
				$reason_code=getRequest('reason_code');
				$reason_text=getRequest('reasontext');
				$sql= $conn->Prepare("insert into disapproval_reasons set reason_code=?, reason_text=? ,`by`=?, campaign_id=?");
				$rs = $conn->execute($sql, array($reason_code, $reason_text, $_SESSION[ADMIN_ID], $campaign_id));
				if($rows>0){
					$msg= "Campaign has been Disapproved successfully";

				}else{
					$msg = "No changes made";
				}
				$_REQUEST['next']='true';
			} else {
				$_REQUEST['msg']="Code or Text missing"; 
			}
			doForward("$config[baseurl]/vadmins/index.php?page=prescreen&msg=$msg");

		}
		$smarty->assign('reason_codes', $reason_codes);
		if($_REQUEST['next']=="true") {
			$sql=$conn->Prepare("select id from campaign where status='active' and id>? and review_status='Pending Review' order by id limit 1 ");
			$rs=$conn->execute($sql, array($campaign_id));
			if($rs && $rs->recordcount() > 0) {
				$campaign_id=$rs->fields['id'];
				$_REQUEST['id']=$campaign_id;
			} else {
				$_REQUEST['msg']="No more campaigns to approve go back <a href='?page=prescreen'>here</a>";
				$campaign_id="none";
			}
		}

		$sqlblocked = $conn->Prepare("SELECT blocked_campaigns.*, concat(channels.name, ' (', channels.url, ')') as blocked_channels FROM `blocked_campaigns`, channels where blocked_campaigns.campaign_id =? and blocked_campaigns.channel_id = channels.id");
		$rsblocked = $conn->execute($sqlblocked, array($campaign_id));
		if($rsblocked && $rsblocked->recordcount()>0){
			$i=0;
					while(!$rsblocked->EOF) {
						$blockedchannels[$i] = $rsblocked->fields['blocked_channels'];
						$rsblocked->movenext();
						$i++;
					}
					$allblockedchannels = implode(", ", $blockedchannels );
					$alert = "Alert: This campaign is blocked at the sites: ".$allblockedchannels.". Please edit your advertisements if you wish to get these reviewed again";
					$smarty->assign("alert",$alert );
		}

		$sql = $conn->Prepare("select campaign.name, validfrom, validto, price_per_unit, price_per_model, budget_currency, budget_amt, daily_limit,model, stg, device, wtg, campaign.video_choice, campaign.track_choice, campaign.channel_choice from campaign where id = ? ");
		$rs = $conn->execute($sql, array($campaign_id));
		if($rs && $rs->recordcount()>0){
				$campaign['name']=$rs->fields['name'];
				$campaign['duration']=$rs->fields['validfrom']." TO ".$rs->fields['validto'];
				$campaign['video_choice']=$rs->fields['video_choice'];
				$campaign['trktype']=$rs->fields['track_choice'];
				$campaign['channel_choice']=$rs->fields['channel_choice'];
				$campaign['price_per_unit']=$rs->fields['price_per_unit'];
				$campaign['price_per_model']=$rs->fields['price_per_model'];
				$campaign['model']=$rs->fields['model'];
				$campaign['device']=$rs->fields['device'];
				$campaign['wtg']=$rs->fields['wtg'];
				$campaign['stg']=$rs->fields['stg'];
				$campaign['budget_amt']=$rs->fields['budget_amt'];
				$campaign['budget_currency']=$rs->fields['budget_currency'];
				$campaign['daily_limit']=$rs->fields['daily_limit'];
		}

		$sql = $conn->Prepare("select * from  campaign_members left join ads  on campaign_members.ad_id = ads.id where campaign_members.cid = ? ");

		$rs = $conn->execute($sql, array($campaign_id));


    $targetObj['nnd']="$campaign[name]: $campaign[duration] GMT";
    if($campaign['model']='CPM')
        $price=$campaign['price_per_unit']*1000;
    else
        $price=$campaign['price_per_model'];
    $targetObj['prc']="$campaign[model]: $price $campaign[budget_currency], Budget: $campaign[budget_amt] $campaign[budget_currency] Daily: $campaign[daily_limit]$campaign[budget_currency]";

    $sql = "select name, id, url from channels where id in ($campaign[channel_choice])";
    $rs1=$conn->execute($sql);
     $targetObj['channels']="";
    if($rs1)
      $targetObj['channels']=print_r($rs1->getRows(),true);

    $sql = $conn->Prepare("select country_codes, state_codes, postalcodes from geography where campaign_id=?");
    $rs1 = $conn->execute($sql, array($campaign_id));
    $targetObj['geography']="";
    if($rs1)
      $targetObj['geography']=print_r($rs1->getRows(),true);

    $targetObj['other']="Strong targeting: $campaign[stg], Weak targeting: $campaign[wtg], Device: $campaign[device]";
    

		$i=0;

		$textads=array();
		$bannerads=array();
		$videoads=array();
		$brandedads=array();
		$trackerimages=array();
		$trackerswfs=array();
		$dblcliktags=array();
		$videobannerads=array();
		while(!$rs->EOF) {
			$campaign['type']=$rs->fields['ad_format']."advertisement";
			if($rs->fields['ad_format']=='video')
			{
				$videoad['id']=$rs->fields['id'];
				$videoad['uri']=substr($rs->fields['playlist_ad_ref'],0,strlen($rs->fields['playlist_ad_ref'])-4);
				$videoad['name']=$rs->fields['branded_img_right_ref_imgname'];
				$videoad['desturl']=$rs->fields['destination_url'];
				$videoads[]=$videoad;
			} elseif ($rs->fields['ad_format']=='branded') {
				$brandedad['id']=$rs->fields['id'];
				$brandedad['name']=$rs->fields['branded_img_right_ref_imgname'];
				$brandedad['desturl']=$rs->fields['destination_url'];
				$brandedad['color']=$rs->fields['branded_color'];
				$brandedad['imgurl']=$rs->fields['branded_img_top_ref_txtitle'];
				$brandedad['logo']=$rs->fields['branded_logo_ref'];
				$brandedad['skinreff']=$rs->fields['playlist_ad_ref'];
				$campaign['type']="brandedplayer";
				$brandedads[]=$brandedad;
			} elseif ($rs->fields['branded_img_top_ref_txtitle']!="" ) {
				$textad['id']=$rs->fields['id'];
				$textad['title']=$rs->fields['branded_img_top_ref_txtitle'];
				$textad['body']=$rs->fields['branded_img_bot_ref_txbody'];
				$textad['desturl']=$rs->fields['destination_url'];
				$textads[]=$textad;
				$campaign['subtype']="textadvertisement";
      } elseif ($rs->fields['ad_format']=='tracker' || $rs->fields['ad_format']=='banner' || $rs->fields['ad_format']=='vdobanner') {
      	if($campaign['trktype']=='dblclik'){
			$dblcliktag['id']=$rs->fields['id'];
			$dblcliktag['imgname']=$rs->fields['branded_img_right_ref_imgname'];
			$dblcliktag['imgsize']=$rs->fields['dimension'];
			$dblcliktag['tracker_url']=$rs->fields['tracker_url'];
			$trackerurls = explode('|', $dblcliktag['tracker_url']);
			$trackerurl = explode('~',$trackerurls[3]);
			$dblcliktag['imgurl']=$trackerurl[1];
			$trackerurl = explode('~',$trackerurls[2]);
			$dblcliktag['ancurl']=$trackerurl[1];
			$dblcliktag['desturl']=$rs->fields['destination_url'];
			$dblcliktag['trktype']=$campaign['trktype'];
			$sri_details=$rs->fields['sri_details'];
			$dblcliktags[]=$dblcliktag;
		}
		elseif($campaign['trktype']=='trackerimage' || $campaign['trktype']=='extimage'){
			$trackerimage['id']=$rs->fields['id'];
			$trackerimage['imgname']=$rs->fields['branded_img_right_ref_imgname'];
			$trackerimage['imgsize']=$rs->fields['dimension'];
                if($campaign['trktype']=='extimage')
			  $trackerimage['imgurl']=$rs->fields['tracker_url'];
                else {
				$trackerimage['imgurl']=$rs->fields['playlist_ad_ref'];
                }
			$trackerimage['desturl']=$rs->fields['destination_url'];
			$trackerimage['trackurl']=$rs->fields['tracker_url'];
			$trackerimage['trktype']=$campaign['trktype'];
			$sri_details=$rs->fields['sri_details'];
			$trackerimages[]=$trackerimage;
		}
		elseif($campaign['trktype']=='videobanner'){
			$videobannerad['id']=$rs->fields['id'];
			$videobannerad['uri']=substr($rs->fields['playlist_ad_ref'],0,strlen($rs->fields['playlist_ad_ref'])-4);
			$videobannerad['videobannerurl']=$rs->fields['branded_logo_ref'];
			$videobannerad['name']=$rs->fields['branded_img_right_ref_imgname'];
			$videobannerad['dimension']=$rs->fields['dimension'];
			$videobannerad['desturl']=$rs->fields['destination_url'];
			$videobannerad['trackurl']=$rs->fields['tracker_url'];
			$sri_details=$rs->fields['sri_details'];
			$videobannerads[]=$videobannerad;
		} else {
			$trackerswf['id']=$rs->fields['id'];
			$trackerswf['imgname']=$rs->fields['branded_img_right_ref_imgname'];
			$trackerswf['imgsize']=$rs->fields['dimension'];
                if($campaign['trktype']=='extswf')
			    $trackerswf['imgurl']=$rs->fields['tracker_url'];
                else {
			    $trackerswf['imgurl']=$rs->fields['playlist_ad_ref'];
                }
			
                $trackerswf['desturl']=$rs->fields['destination_url'];
                $trackerswf['trackurl']=$rs->fields['tracker_url'];
			$trackerswf['trktype']=$campaign['trktype'];
			$sri_details=$rs->fields['sri_details'];
			$trackerswfs[]=$trackerswf;
		}
        $embeded=array();
        $sqlch="select id, url from channels where id in($campaign[channel_choice])";
        $channel = $conn->execute($sqlch);
        if($channel && $channel->recordcount()>0){
          $channels = $channel->getrows();
          foreach ( $channels as $key => $ch){
            $embeded[$key]['channel']=$ch['url'];
            $ad_id=$rs->fields['id'];
            $cid=$campaign_id;
            $channel_id=$ch['id'];
            $output='rd';

            /*
               switch($campaign['model']){
               case 'CPM':
               $method='vi';
               $ad_id=$rs->fields['id'];
               $cid=$campaign_id;
               $channel_id=$ch['id'];
               $output='rd';
               break;
               case 'CPC':
               $method='cl';
               $ad_id=$rs->fields['id'];
               $cid=$campaign_id;
               $channel_id=$ch['id'];
               $output='rd';
               break;
               case 'CPL':
               $method='ld';
               $ad_id=$rs->fields['id'];
               $cid=$campaign_id;
               $channel_id=$ch['id'];
               $output='px';
               break;
               }*/
            $dimension = explode('x',$rs->fields['dimension']);
            if($campaign['trktype']=='trackerimage' || $campaign['trktype']=='extimage'){
              $embeded[$key]['code']='<a target="_blank" href="http://serve.vdopia.com/adserver/tracker.php?m=cl&ci='.$cid.'&ai='.$ad_id.'&chid='.$channel_id.'&ou='.$output.'&rand=%RAND%" ><img src="http://serve.vdopia.com/adserver/tracker.php?m=vi&ci='.$cid.'&ai='.$ad_id.'&chid='.$channel_id.'&ou='.$output.'&rand=%RAND%" width="'.$dimension[0].'" height="'.$dimension[1].'" /></a>';
            }
            else{
              $encorig='http://serve.vdopia.com/adserver/tracker.php?m=cl&ci='.$cid.'&ai='.$ad_id.'&chid='.$channel_id.'&ou='.$output.'&rand=';
              $enclink=urlencode($encorig);
              $encorig.='%RAND%';
              $enclink.='%RAND%';
              $embeded[$key]['code']='<object width="'.$dimension[0].'" height="'.$dimension[1].'"><param name="movie" value="http://serve.vdopia.com/adserver/tracker.php?m=vi&ci='.$cid.'&ai='.$ad_id.'&chid='.$channel_id.'&ou='.$output.'&rand=%RAND%"></param><param name="clickTAG" value="'.$encorig.'"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://serve.vdopia.com/adserver/tracker.php?m=vi&ci='.$cid.'&ai='.$ad_id.'&chid='.$channel_id.'&ou='.$output.'&rand=%RAND%" flashvars="clickTAG='.$enclink.'" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="'.$dimension[0].'" height="'.$dimension[1].'"></embed></object>';
            }
          }
        }
      }else {
				$bannerad['id']=$rs->fields['id'];;
				$bannerad['imgname']=$rs->fields['branded_img_right_ref_imgname'];;
				$bannerad['imgurl']=$rs->fields['playlist_ad_ref'];;
				$bannerad['desturl']=$rs->fields['destination_url'];
				$campaign['subtype']="banneradvertisement";
				$bannerads[]=$bannerad;
			}
			$rs->movenext();
		}
    $campaign['targeting']=$targetObj;
		$campaign['textads']=$textads;
		$campaign['bannerads']=$bannerads;
		$campaign['videoads']=$videoads;
		$campaign['brandedads']=$brandedads;
    $campaign['trackerswfs']=$trackerswfs;
    $campaign['trackerimages']=$trackerimages;
    if($campaign['trktype']=='extimage' )
        $campaign['extinfo']=$trackerimages;
    if( $campaign['trktype']=='extswf')
      $campaign['extinfo']=$trackerswfs;

		$smarty->assign('readOnly',"1");
		$smarty->assign('viewonly',"1");

		$smarty->assign('campaign',$campaign);

		break;
		  case 'changepass':
        		if(isset($_REQUEST['action_changepassword'])) {
			/*		$rules = array(); // stores the validation rules
					$rules[] = "required,type,Error.";
					$rules[] = "if:type=overlayadvertisement,required,subtype, Error";
					$rules[] = "if:type=videoadvertisement,required,vidtype, Error";


					$error = validateFields($_POST, $rules);
					*/
					if (empty($error)) {

						if(check_pwd("admin", $_REQUEST['oldpassword'], $_SESSION['ADMIN_ID'])== null){

							$pass= md5($_REQUEST['password']);
							$sql = $conn->Prepare("update admin set passwd = ? where id = ?");
							$rs = $conn->execute($sql, array($pass, $_SESSION[ADMIN_ID]));
							$rows = $conn->Affected_Rows();
							if($rows>0){
								$msg= "Your password has been changed successfully";

							}else{
								$msg = "No changes made";
							}
							doForward("$config[baseurl]/vadmins/index.php?page=prescreen&msg=$msg");
						}else{
							$error = "Incorrect password";
						}

					}
				}

				$smarty->assign("error","$error");
				$smarty->assign("msg","$msg");

				break;
        case 'publishers':
        	if(!isset($_REQUEST['sub'])) $_REQUEST['sub']='puball';
			include_once("publishers.php");

			break;

	    case 'viewpub':
        	if( $_REQUEST['email']!="") {
				# clear the session accept admin credentials
      			clearSession(array("ADMIN_ID","ADMIN_PRIVILEGES","CAPABILITIES"));

				/*TODO add verification code/rules*/
				try {
					$email=getRequest('email', true);
					$sql=$reportConn->Prepare("select publisher.id, fname, lname, email, currency,advertiser_id, overlay, branded, video  from publisher left join publisherpricing on publisher_id=publisher.id and isnull(validto) where email=? and type='online'");
					$rs=$reportConn->execute($sql, array($email));
					if($rs->recordcount()>0) {
						$_SESSION['ACCOUNT_TYPE']='publisher';
						$_SESSION['PUB_ID']=$rs->fields['id'];
						$_SESSION['FNAME']=$rs->fields['fname'];
						$_SESSION['LNAME']=$rs->fields['lname'];
						$_SESSION['EMAIL']=$rs->fields['email'];
						$_SESSION['CURRENCY3DGT']=$rs->fields['currency'];
						$_SESSION['CURRENCYVAL']=insert_3dgt_to_val(array("3dgt" =>$rs->fields['currency']));
						$_SESSION['CURRENCYSYM']=insert_val_to_currency(array("value" => $_SESSION['CURRENCYVAL']));
						
						if(isset($rs->fields['advertiser_id'])) {
							$_SESSION['PUB_ADV_ID']=$_SESSION['ADV_ID']=$rs->fields['advertiser_id'];
							$_SESSION['ADV_PUB_ID']=$_SESSION['PUB_ID'];
							$_SESSION['OVERLAY_PRICING']=split(':',$rs->fields['overlay']);
							$_SESSION['BRANDED_PRICING']=split(':',$rs->fields['video']);
							$_SESSION['VIDEO_PRICING']=split(':',$rs->fields['branded']);
						}
	
	
						if(isset($_SESSION['CAMPAIGN']['currency'])) {
							$_SESSION['CAMPAIGN']['currency']=$_SESSION['CURRENCYVAL'];
						}
						//$sql="update publisher set lastlogin=now() where id=$_SESSION[PUB_ID]";
						//$conn->execute($sql);
						//echo "<pre>"; print_r($_SESSION); exit;
						doForward("$config[baseurl]/pub.php");
					}
				} catch(Exception $e){
				}
				$smarty->assign("error","Invalid Email/Password");
			} elseif (is_publisher_loggedin() ) {
				doForward("$config[baseurl]/pub.php");
			}

        break;
	    case 'inventory':
	    	if(!isset($_REQUEST['sub'])) $_REQUEST['sub']='newinventory';
			include_once("inventory.php");		
		  break;
		  
		
		default:
			doForward('?page=prescreen');

	}
} 
else {

	if( /*!isset($_REQUEST['page']) &&*/ $_REQUEST['page']!='login' && $_REQUEST['page']!='newAccount')
  {
    isset($_REQUEST['msg'])?doForward("?page=login&msg=$_REQUEST[msg]"):doForward("?page=login");
//		doForward("?page=login");
  }
}
$smarty->display("vadmins/header.tpl");
$smarty->display("vadmins/$_REQUEST[page].tpl");
$smarty->display("vadmins/footer.tpl");

?>
