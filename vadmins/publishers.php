<?php

include_once("../include/datefuncs.php");


if(is_admin_loggedin()){
  switch ($_REQUEST['sub']) {

    case 'puball':
      if(isset($_REQUEST['action_createadvertiser'])){
        $pubid = mysql_escape_string($_REQUEST['pubid']);
        $sql = "insert into advertiser (fname, lname, apikey, passwd, addtime, email, phone, mobile, fax, address1,address2, city, state, country, zip, type,acc_type, publisher_id)   SELECT  fname, lname, md5(concat(email,'online',now())),passwd, now(),email, phone, mobile, fax, address1,address2, city, state, country, zip, type,acc_type,id from publisher where id='$pubid'";
        $rs = $conn->execute($sql);
        $advid = $conn->Insert_ID();
        $updateSql = "update advertiser set activated = 'yes' where id = $advid";
        $conn->execute($updateSql);
        
        if($advid != ""){
          $sql="update publisher set advertiser_id=$advid where id=$pubid";
          $rs = $conn->execute($sql);
          $sql = "select currency from publisher where id=$pubid";
          $rs = $conn->execute($sql);
          if($rs && $rs->recordcount()>0){
            $currency = $rs->fields['currency'];
            $sql = "insert into advertisersheet set advertiser_id=$advid, currency='$currency'";
            $rs1 = $conn->execute($sql);
          }
        }
      }

      if(isset($_REQUEST['action_search'])){
        $searchtext=mysql_escape_string($_REQUEST['searchtext']);
        $sql = "select p.*, if(a.email!='','true','false') as isExist, (select round(rev_share,2) from revenue_share where p.id=revenue_share.publisher_id order by effective_date desc limit 1) as rev_share, (select effective_date from revenue_share where p.id=revenue_share.publisher_id order by effective_date desc limit 1) as effective_date from publisher p left join  advertiser a on p.email=a.email and p.type=a.type where (p.fname like '%$searchtext%' or p.lname like '%$searchtext%' or p.email like '%$searchtext%' or p.company_name like '%$searchtext%') and p.type='online'";
        $rs = $reportConn->execute($sql);
        if($rs && $rs->recordcount()>0){
          $pubs= $rs->getrows();
          $smarty->assign('pubs',$pubs);
        }     
      } 
      break;
     case 'defaultAccountType':
      	if(isset($_REQUEST['action'])){
      		$sql=$conn->Prepare("update publisher set acc_type=? where id=?");
      		$rs=$conn->Execute($sql,array($_REQUEST['acc_type'],$_REQUEST['id']));
      		if($conn->Affected_rows()>0){
      			$msg="Updated Successfully";
      		}else{
      			$msg="Already Selected";
      		}
      		$smarty->assign("msg",$msg);
      	}
      	$query=$conn->Prepare("Select acc_type from publisher where id=? and acc_type is not null");
      	$res=$conn->Execute($query,array($_REQUEST['id']));
      	if($res && $res->recordcount()>0){
      		$_REQUEST['acc_type']=$res->fields['acc_type'];
      	}
      	$smarty->display("vadmins/innercontent/defaultAccountType.tpl");
      	exit(1);
      	break;
    case 'newchannels':

      if(isset($_REQUEST['allow_sitetargeting'])){
        $channelid = mysql_escape_string($_REQUEST['channelid']);
        $sql="update channels set allow_sitetargeting=1, verified='Verified' where id=$channelid";
        $rs=$conn->execute($sql);
      }
      
      if(isset($_REQUEST['action_search'])){
        $searchtext=mysql_escape_string($_REQUEST['searchtext']);
        $sql = "select channels.name, channels.url, channels.apikey, channels.id as channelid, publisher.email, publisher.id as pubid, video_unit_price, currency, overlay_unit_price, margin, branded_unit_price from channels, publisher,publishersheet where (channels.url like '%$searchtext%' or channels.name like '%$searchtext%' or channels.apikey like '%$searchtext%' or publisher.email like '%$searchtext%') and publisher.id=channels.publisher_id and channels.allow_sitetargeting='0' and publishersheet.channel_id=channels.id and isnull(validto) and channel_type='PC'";
        $rs = $conn->execute($sql);

        if($rs && $rs->recordcount()>0){
          $pubs= $rs->getrows();
          $smarty->assign('pubs',$pubs);
        }
      }else{
      	$sql=$conn->Prepare("select channels.name, channels.url, channels.apikey, channels.id as channelid, publisher.email, publisher.id as pubid, video_unit_price, currency, overlay_unit_price, margin, branded_unit_price from channels, publisher,publishersheet where publisher.id=channels.publisher_id and channels.allow_sitetargeting='0' and publishersheet.channel_id=channels.id and isnull(validto) and datediff(date(now()), date(channels.addedon))<=30 and channels.channel_type='PC'");
      	$rsc=$conn->Execute($sql, array());
      	if($rsc&&$rsc->recordcount()>0){
      		$pubs= $rsc->getrows();
          	$smarty->assign('pubs',$pubs);
      	}
      }
      
      break;
    case 'pub_earnings':
    case 'pub_dump':

      //Get $year and $month

      $year=getRequest('Date_Year', 0);
      $month=getRequest('Date_Month', 0);
      $vdopiaEarnings=getRequest('VdopiaEarnings', 0);

      if($year=='') $year=date('Y');
      if($month=='') $month=date('m');
      if($vdopiaEarnings=="") $vdopiaEarnings="true";

      $_REQUEST['Date_Year']=$year;
      $_REQUEST['Date_Month']=$month;

      //Generate startdate and enddate
      $startdate="$year-$month-01";
      $enddate = "$year-$month-31";

      function smarty_modifier_number_format($string, $decimals = 0, $dec_sep=",", $thous_sep = ".")
      {
        return number_format($string,$decimals,$dec_sep,$thous_sep);
      } 


      $period="$year-".(int)$month;
      if(isset($_SESSION['billing_keyinfo'][$period])) {
        $keyinfo=$_SESSION['billing_keyinfo'][$period];
      } else {
        $sql="select details from billingupdate where month='$period' and site='vdopia'";
        $rs=$conn->execute($sql);
        if($rs && $rs->recordcount()>0){   
          $keyinfo=json_decode($rs->fields['details'], true);
        }
      }

      $sql = "select 
        channel_id as channel_id,
                   adtype as AdFormats,
                   sum(impressions) as Total_Impressions,
                   sum(clicks) as Total_Clicks, 
                   sum(revenue_advcurrency*ex_rate_advpub)as Advertiser_Revenue,
                   sum(revenue_advcurrency)as Advertiser_Revenue_advcurrency,
                   sum(pubrevenue_advcurrency*ex_rate_advpub)as Publisher_Revenue,
                   sum(pubrevenue_advcurrency)as Publisher_Revenue_advcurrency,
                   ex_rate_advpub as exrate,
                   (sum(pubrevenue_advcurrency*ex_rate_advpub)*1000)/sum(impressions) as Effective_CPM, 
                   '$year-$month' as TimeFrame, 
                   campaign_id as campaign_id 
                     from daily_stats ds where date>='$startdate' and date<='$enddate' 
                     group by ds.channel_id , ds.campaign_id";

      $rs = $reportConn->execute($sql);

      //We now have to work with the array and assign publisher_id to each row

      $finalData=array();


      while($rs && !$rs->EOF) {

        $rowdata=$rs -> FetchRow();


        //Logic to skip rows
        if(getChannelType($rowdata['channel_id'])!='PC'){
          continue;
        } 
        $publisher_id= getPublisherId($rowdata['channel_id']);
        $pub_adv_id = getPubAdvertiser($publisher_id);
        $advertiser_id = getAdvertiserId($rowdata['campaign_id']);

        if(
            ($vdopiaEarnings=="true" && $pub_adv_id==$advertiser_id) || 
            ($vdopiaEarnings!="true" && $pub_adv_id!=$advertiser_id)  
          ){
          continue;
        }

        if($_REQUEST['sub']=='pub_earnings') {

          if(!isset($finalData[$publisher_id]['info'])) {
            $finalData[$publisher_id]['info']= getPublisherInfo($publisher_id);
            $finalData[$publisher_id]['data']=array();
            $finalData[$publisher_id]['totalRev']=0;
            $finalData[$publisher_id]['totalImp']=0;
            $finalData[$publisher_id]['totalRev_advcurrency']=0;
            $finalData[$publisher_id]['totalVdopiaRev']=0;
            $finalData[$publisher_id]['totalVdopiaRev_advcurrency']=0;
            $finalData[$publisher_id]['totalClicks']=0;
          }

          $rowdata['Channel_Name']  = getChannelName   ($rowdata['channel_id']);
          $rowdata['Campaign_Name'] = getCampaignName  ($rowdata['campaign_id']);

          $finalData[$publisher_id]['data'][]= $rowdata;
          $finalData[$publisher_id]['totalRev']+=    $rowdata['Publisher_Revenue'];
          $finalData[$publisher_id]['totalRev_advcurrency']+=    $rowdata['Publisher_Revenue_advcurrency'];

          $finalData[$publisher_id]['totalVdopiaRev']+=    $rowdata['Advertiser_Revenue'];
          $finalData[$publisher_id]['totalVdopiaRev_advcurrency']+=    $rowdata['Advertiser_Revenue_advcurrency'];


          $finalData[$publisher_id]['totalImp']+=    $rowdata['Total_Impressions'];
          $finalData[$publisher_id]['OverAllTotalImp']+=    $rowdata['Overall_Total_Impressions'];
          $finalData[$publisher_id]['totalClicks']+= $rowdata['Total_Clicks'];

          $finalData[$publisher_id]['formatInfo'][$rowdata['AdFormats']]['totalRev']+=    $rowdata['Publisher_Revenue'];
          $finalData[$publisher_id]['formatInfo'][$rowdata['AdFormats']]['totalImp']+=    $rowdata['Total_Impressions'];
          $finalData[$publisher_id]['formatInfo'][$rowdata['AdFormats']]['OverAllTotalImp']+=    $rowdata['Overall_Total_Impressions'];
          $finalData[$publisher_id]['formatInfo'][$rowdata['AdFormats']]['totalClicks']+= $rowdata['Total_Clicks'];
        } else {

          $campaign_id=$rowdata['campaign_id'];
          $format=$rowdata['AdFormats'];

          $key=getPublisherCurrency($publisher_id)."~".getPublisherInfo($publisher_id)."~".getCampaignName($campaign_id)."~".$format;
          //$key=str_replace(array(', ',' (') , array(',','('),$key);
          //Defaults
          if($keyinfo[$key]['approval']=='') {
            $keyinfo[$key]['approval']='approved';
            if($format=='tracker'||$format=='branded')
              $keyinfo[$key]['approval']='tobeconfirmed';
          }

         
          $finalData[getPublisherCurrency($publisher_id)][getPublisherInfo($publisher_id)][getCampaignName($campaign_id)][$format]['model']=getCampaignModel($campaign_id);
          $finalData[getPublisherCurrency($publisher_id)][getPublisherInfo($publisher_id)][getCampaignName($campaign_id)][$format]['approval']=$keyinfo[$key]['approval'];
          $finalData[getPublisherCurrency($publisher_id)][getPublisherInfo($publisher_id)][getCampaignName($campaign_id)][$format]['ronum']=$keyinfo[$key]['ronum'];
          $finalData[getPublisherCurrency($publisher_id)][getPublisherInfo($publisher_id)][getCampaignName($campaign_id)][$format]['exrate']=$rowdata['exrate'];
          $finalData[getPublisherCurrency($publisher_id)][getPublisherInfo($publisher_id)][getCampaignName($campaign_id)][$format]['advcurr']=getAdvertiserCurrency($advertiser_id);
          $finalData[getPublisherCurrency($publisher_id)][getPublisherInfo($publisher_id)][getCampaignName($campaign_id)][$format]['vdorev_advcurr']+=$rowdata['Advertiser_Revenue_advcurrency'];
          $finalData[getPublisherCurrency($publisher_id)][getPublisherInfo($publisher_id)][getCampaignName($campaign_id)][$format]['rev_advcurr']+=$rowdata['Publisher_Revenue_advcurrency'];
          $finalData[getPublisherCurrency($publisher_id)][getPublisherInfo($publisher_id)][getCampaignName($campaign_id)][$format]['vdorev']+=$rowdata['Advertiser_Revenue'];
          $finalData[getPublisherCurrency($publisher_id)][getPublisherInfo($publisher_id)][getCampaignName($campaign_id)][$format]['rev']+=$rowdata['Publisher_Revenue'];
          $finalData[getPublisherCurrency($publisher_id)][getPublisherInfo($publisher_id)][getCampaignName($campaign_id)][$format]['imp']+=$rowdata['Total_Impressions'];
          $finalData[getPublisherCurrency($publisher_id)][getPublisherInfo($publisher_id)][getCampaignName($campaign_id)][$format]['timp']+=$rowdata['Overall_Total_Impressions'];
          $finalData[getPublisherCurrency($publisher_id)][getPublisherInfo($publisher_id)][getCampaignName($campaign_id)][$format]['clk']+=$rowdata['Total_Clicks'];

         
          
        }
      }  

  
      if($_REQUEST['sub']=='pub_dump') {

        foreach($finalData as $curr=>&$l1) {
          foreach($l1 as  $pub=>&$l2) {
            foreach($l2 as  $camp=>&$l3) {
              foreach($l3 as  $format=>&$rowinfo) {
                $key=$curr."~".$pub."~".$camp."~".$format;

                $clks=$rowinfo['clk'];
                $imps=$rowinfo['imp'];
                $revs=$rowinfo['rev'];

                if($keyinfo[$key]['model']!='') {
                  $model=$keyinfo[$key]['model'];
                  if($model=='CPM') {
                    $revs=$imps*$keyinfo[$key]['value']/1000;
                  } else {
                    $revs=$clks*$keyinfo[$key]['value'];
                  }
                  //Multiply by average exchange rate
                  if($rowinfo['rev']!=0) $rowinfo['exrate']=$rowinfo['rev_advcurr']/$rowinfo['rev'];
                  $rowinfo['rev_advcurr']=$revs*$rowinfo['exrate'];
                  $rowinfo['rev']=$revs;
                }

                if($keyinfo[$key]['model']=='') {
                  $keyinfo[$key]['model']=$rowinfo['model'];
                }

                if($keyinfo[$key]['value']=='') {
                  if($keyinfo[$key]['model']=='CPM')
                    $keyinfo[$key]['value']=$revs*1000/$imps;
                  else
                    $keyinfo[$key]['value']=$revs/$clks;
                }      

              }
            }
          }
        }

        session_register('billing_keyinfo');
        $_SESSION['billing_keyinfo']='';
        $_SESSION['billing_keyinfo'][$period]=$keyinfo;
      }
      $smarty->assign('data',$finalData);
      break;
    case 'currentchannels':
      $channelid = mysql_escape_string($_REQUEST['channelid']);

      if(isset($_REQUEST['action_search'])){
        $searchtext=mysql_escape_string($_REQUEST['searchtext']);
        $sql = "select channels.extAds, channels.name,channels.channel_category,channels.description, channels.url, channels.apikey, channels.id as channelid, channels.playerurl, publisher.email, publisher.id as pubid, video_unit_price, currency, overlay_unit_price, margin, branded_unit_price from channels, publisher,publishersheet where (channels.url like '%$searchtext%' or channels.name like '%$searchtext%' or channels.apikey like '%$searchtext%' or publisher.email like '%$searchtext%') and publisher.id=channels.publisher_id and channels.allow_sitetargeting='1' and publishersheet.channel_id=channels.id and isnull(validto) and channel_type='PC'";
        $rs = $conn->execute($sql);
        if($rs && $rs->recordcount()>0){
          $pubs= $rs->getrows();
          $smarty->assign('pubs',$pubs);
        }
      }
      $selCat = array();
      foreach ($pubs as $key => $chrow) {
        $selCat[$key] = explode(",", $chrow['channel_category']);
      }
      $smarty->assign('selCat',$selCat);
      $path = opendir($config['BASE_DIR']."/skins/");
      while($dir=readdir($path)){
        if(is_file($config['BASE_DIR']."/skins/".$dir)){
          $skinarray[]=$dir;
        }
      }
      $smarty->assign('skins',$skinarray);
      if(isset($_REQUEST['go'])){
        $skins=getRequest('skins');
        $extAds=getRequest('extAds');
        $sql="	update 
          channels 
          set 
          playerurl='http://cdn.vdopia.com/swf/mediaplayer_skin.swf?skinPath=http://cdn.vdopia.com/skins/$skins',
          extAds='$extAds' 
            where 
            id=$channelid";
        $rs = $conn->execute($sql);
        doForward("$config[baseurl]/vadmins/index.php?page=publishers&sub=currentchannels");
      }

      $sqlcat = "select * from category order by category_name";
      $rscat=$conn->execute($sqlcat);
      if($rscat && $rscat->recordcount()>0){
        $catArr= $rscat->getrows();
        $smarty->assign('catArr',$catArr);
      } 
      if(isset($_REQUEST['setCat'])){       	
        $cats = implode(",",$_REQUEST['category']);
        $description = mysql_escape_string(strip_tags($_REQUEST['description']));;
        $sql="	update 
          channels 
          set 
          channel_category='$cats',
          description='$description'
            where 
            id=$channelid";
        $rs = $conn->execute($sql);
        doForward("$config[baseurl]/vadmins/index.php?page=publishers&sub=currentchannels");
      }
      break;
      case 'pub_new_earnings':
      	ini_set('memory_limit', '512M');
      	if(isset($config['DB_PREFIX']) && $config['DB_PREFIX']!=''){
      		$db_prefix=$config['DB_PREFIX'];
      	}else $db_prefix='';
      	if($_REQUEST['pub_new_earnings']!="" && $_REQUEST['pub_new_earnings']=="Go"){
      		$year=getRequest('Date_Year', 0);
      		$month=getRequest('Date_Month', 0);
      		if($year=='') $year=date('Y');
      		if($month=='') $month=date('m');
      		$_REQUEST['Date_Year']=$year;
      		$_REQUEST['Date_Month']=$month;
      		if($_REQUEST['portal_type']=="ivdopia"){
      			$channel_type ='iphone';
      			$device ='iphone';
      		}
      		else{
      			$channel_type ='PC';
      			$device ='pc';
      		}
      		$finalData=array();
      		$table=$db_prefix."pub_earnings"."_".$year."_".$month;
      		//$table='pub_earning';
      		//$table="pub_earning_nidhi_2";
      		$te=checkReportTableExists($table);
      		if(!$te){
      			$message= "Data doesn't exist for selected timerange. Year:$year , Month:$month \n";
      		}
      		else {
      			$vc_exist=isColumnExists("'completes','cr'",$table);
      			if($vc_exist){
      				$sql_vc = "sum(completes) as completes, round(((sum(completes)/sum(total_impressions))*100),2) as cr, ";
      			}else $sql_vc="";
      			 
      			$ld_exist=isColumnExists("'leads'",$table);
      			if($ld_exist){
      				$sql_ld = "sum(leads) as leads,";
      			}else $sql_ld="";
      			 
      			$pc_exists=isColumnExists("'pub_company'",$table);
      			if($pc_exists)
      				$sql_pc=" pub_company as Publisher_Company, ";
      			else $sql_pc="";
      			
                $pl_exist=isColumnExists("'placement_group'",$table);
			       	if($pl_exist)$sql_pl="placement_group,";
			       	else $sql_pl="";
			       	
			    $ad_exist=isColumnExists("'adtype'",$table);
			       	if($ad_exist)$sql_ad="adtype,";
			       	else $sql_ad="";   
			       
			      	
      			$sql="
      			select
      			pub_id as Publisher_id,
      			pub_email as Publisher_email,
      			$sql_pc
      			pub_currency as Publisher_Currency,
      			adv_email as Advertiser_email,
      			adv_currency as Advertiser_Currency,
      			ex_rate_advpub as Exchange_Rate_AdvToPub,
      			campaign_id as Campaign_Id,
      			campaign_name as Campaign_Name,
      			$sql_pl
				$sql_ad
      			model as Model,
      			device as Device,
      			sum(adv_revenue_pub_currency) as adv_revenue_pub_currency ,
      			sum(adv_revenue_adv_currency) as adv_revenue_adv_currency,
      			sum( total_impressions ) as Total_Impressions,
      			sum( total_clicks ) as Total_Clicks,
      			sum( effective_clicks ) as Effective_Clicks,
      			$sql_vc
      			$sql_ld
      			sum(pub_revenue_without_channel_price_override) as pub_revenue_without_channel_price_override,
      			sum(pub_revenue) as Publisher_Revenue
      			from $table where  device='$device'  and channel_type ='$channel_type'
      			group by pub_email,campaign_id,$sql_ad model
      			order by pub_email ,campaign_id
      			;";
      			 
      			error_log($sql);
      			$rs = $reportConn->execute($sql); 
      			 
      			$reportConn->SetFetchMode(ADODB_FETCH_ASSOC);
      			if($rs && $rs->recordcount()>0){
      			$data = $rs->getrows();
      		}
     
      		 
      		$row=0;
      		$handle = openstream();
      		$heading=array('Publisher','Publisher Company','Campaign','Placement','Adtype','Advertiser','Impression','CLicks','CTR','Video Completes','CR(%)','Leads','CPM','CPC','Model','Adv.Currency','Publisher_Revenue ( without channel_price override )','Ex Rate','Pub.Currency','Pub Rev','Adv Rev ( Pub Currency )','Adv Rev ( Adv Currency )','Pub Margin');
      		if(!$vc_exist){
                  	unset($heading[9]);
                  	unset($heading[10]);
                  }
                  if(!$ld_exist) unset($heading[11]);
                  if(!$pc_exists) unset($heading[1]);
                  if (!$pl_exist)unset ($heading[3]);
                  if (!$ad_exist) unset ($heading[4]);
      								$report_headers[0]=array(ucfirst("Publisher Revenue sheet"));
      								$report_headers[1]=array('Report Generated on', date('d-m-Y H:i T'));
      								$report_headers[2]=array("Period","$year-$month");
      								$report_headers[3]=array("Portal",ucfirst($_REQUEST['portal_type']));
      								$report_headers[4]=array("",);
      								foreach($report_headers as $headers){
      								writestream($handle,$headers);
      	}
      										writestream($handle,$heading);
      								foreach($data as $rs){
      										$finalData['Publisher_email'] = $rs['Publisher_email'];
      										if($pc_exists)
      									$finalData['Publisher_Company'] = $rs['Publisher_Company'];
      									$finalData['Campaign_Name'] = $rs['Campaign_Name'];
                                        if($pl_exist) 
                                        $finalData['Placement_Name'] = $rs['placement_group'];
                                        if($ad_exist)
                                        $finalData['Adtype'] = $rs['adtype'];
      										$finalData['Advertiser_email'] = $rs['Advertiser_email'];
      										//$finalData[$row]['AdFormats'] = $rs->fields['AdFormats'];
      										$finalData['Total_Impressions'] = $rs['Total_Impressions'];
      										$finalData['Total_Clicks'] = $rs['Total_Clicks'];
      										$finalData['CTR'] = $rs['Total_Clicks']*100/$rs['Total_Impressions'];
      										if($vc_exist){
      										$finalData['completes'] = $rs['completes'];
      												$finalData['cr'] = $rs['cr'];
      										}
      										$finalData['CPM'] = ($rs['Publisher_Revenue']*1000)/$rs['Total_Impressions'];
      										if($rs['Total_Clicks']==0)
      											$finalData['CPC'] ="NA";
      										else
      											$finalData['CPC'] = $rs['Publisher_Revenue']/$rs['Total_Clicks'];
      
      										$finalData['model'] = $rs['Model'];
      										$finalData['adv_currency'] = $rs['Advertiser_Currency'];
      										$finalData['pub_revenue_without_channel_price_override'] = $rs['pub_revenue_without_channel_price_override'];
      											$finalData['exrate'] = $rs['Exchange_Rate_AdvToPub'];
      											$finalData['Publisher_Currency'] = $rs['Publisher_Currency'];
      											$finalData['Publisher_Revenue'] = $rs['Publisher_Revenue'];
      													$finalData['Advertiser_Revenue_Pub_currency'] = $rs['adv_revenue_pub_currency'];
      													$finalData['Advertiser_Revenue_Adv_currency']= $rs['adv_revenue_adv_currency'];
      											$finalData['margin'] = 100*(1-(($rs['adv_revenue_pub_currency']-$rs['Publisher_Revenue'])/$rs['adv_revenue_pub_currency']));
      											writestream($handle,$finalData);
      										}
      
      												 
      												 		$result = getStreamData($handle);
      												 		$primanrySheet = "$year-$month";
      												 		$portal = $_REQUEST['portal_type'];
           		$filename="publisher_revenue_sheet_"."$portal"."_"."$year"."-"."$month";
                 		// include_once ABSPATH.'/common/portal/tools/PhpExcelUtil.php';
                 				if($_REQUEST['viewoption']=="export"){
                 						header('Content-Type: text/csv');
                 				header("Content-Disposition: attachment;filename=$filename.csv");
                 						header('Cache-Control: max-age=0');
                    echo $result;
                          //doForward($config["BASE_URL"]."/vadmins/index.php?page=publishers&sub=pub_new_earnings");
                          exit();
      										}elseif($_REQUEST['viewoption']=="email"){
      												$userEmails = explode(",",$_REQUEST['userEmail']);
      
      														foreach($userEmails as $k=>$v){
      										if(trim($v)!="")
      											$emailStr.=",".trim($v)."@vdopia.com";
      	}
      	$emailStr = trim($emailStr,",");
      	$type_name = "csv|"."publisher_revenue_sheet_$portal_$year-$month";
      	$cc="abhay@vdopia.com";
      	if(sendAttachment("$emailStr","Vdopia Support","support@vdopia.com","publisher Revenue Sheet $portal $year-$month","publisher revenue sheet $portal $year-$month",$result,$type_name,$cc)){
      	$msg = "Report has been sent at $emailStr";
      	}
      			else{
      					$msg = "Some error please try again";
      					}
      					}
      					}
      					$smarty->assign('msg',$msg);
      					$smarty->assign('message',$message);
      					}
      					break;
      ## inorder to open an overlay for editing the default pricing- by tanu aggarwal
      case 'saveRevShare':
  		$effDates = array(date("Y-m"));
  		if (date('d') < 16) {
      		
      	$effDates = array(date("Y-m"));
      	$migDate = new DateTime($config['cp_freeze_date']);
      	$dateCur = new DateTime();
      	$curMonth = new DateTime($dateCur->format('Y').'-'.$dateCur->format('m').'-01');
      	if($curMonth != $migDate)
      		$effDates = array(date("Y-m",strtotime("-1 month")),date("Y-m"));
      		//$effDates = array(date("Y-m",strtotime("-1 month")),date("Y-m"));
      	}
      	$selectedDate = date('Y-m');
      	$smarty->assign('effDates',$effDates);
      	$smarty->assign('selEffDate',$selectedDate);
      	$smarty->display("vadmins/innercontent/saveRevShare.tpl");
      	exit(1);
      	break;
      ## end
      ## inorder to open an overlay for viewing reve share logs- by tanu aggarwal
      case 'viewLogs':
      	require_once ABSPATH."/common/library/class.revShare.php";
      	$revShareObj = new RevenueShare();

      	$logs = $revShareObj->GetrsLogs($_REQUEST['pubID']);
      	if (empty($logs)) {
      		$smarty->assign("isAvailable", 0);
      	} else {
    		$smarty->assign("isAvailable", 1);
    	}
      	$smarty->assign("logs", $logs);
      	$smarty->display('vadmins/innercontent/viewLogs.tpl');
      		 
      	exit(1);
      	break;
      ## end
  }

} else {
	doForward("$config[BASEURL]/vadmins/?page=login&desturl=$_SERVER[REQUEST_URI]");
}
function isColumnExists($columns,$table){
	global $reportConn;
	$sql_chn = "SELECT column_name FROM information_schema.columns WHERE table_name = '$table' and column_name in ($columns)";
	$rs_chn = $reportConn->Execute($sql_chn);
	return ($rs_chn->recordcount()>0)?True:False;
}
?>
