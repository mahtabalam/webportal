<?php
include_once '../include/config.php';

if(is_admin_loggedin()) {
  if (!empty($_FILES)) {
    $tempFile = $_FILES['Filedata']['tmp_name'];

    $targetPath = $config[AD_IMG_DIR]."/";
    $filename = md5(time()).'.'.extension($_FILES['Filedata']['name']);
    $targetFile =  str_replace('//','/',$targetPath) . $filename;


# Uncomment the following line if you want to make the directory if it doesn't exist
# mkdir(str_replace('//','/',$targetPath), 0755, true);

# check, if file has been uploaded
    if(move_uploaded_file($tempFile,$targetFile)){
      $ad_id = $_GET['ad_id'];
      $channel_id = $_GET['channel_id'];

# delete rows from campaign screenshot and correponding image
      $sql="select screenshot from campaign_screenshots where ad_id='$ad_id' and channel_id='$channel_id'";
      $rs = $conn->execute($sql);
      if($rs && $rs->recordcount()>0){
        $screenshot = $rs->fields('screenshot');
        @unlink($targetPath.$screenshot);

# delete query
        $sql = "delete from campaign_screenshots where ad_id='$ad_id' and channel_id='$channel_id'";
        $rs = $conn->execute($sql);
      }

# insert rows into campaign screenshot
      $sql="insert into campaign_screenshots set ad_id='$ad_id', channel_id='$channel_id', screenshot='$filename'";
      $rs = $conn->execute($sql);


    }
  }
} else {
	doForward("$config[BASEURL]/vadmins/?page=login&desturl=$_SERVER[REQUEST_URI]");
}
	
echo 'none';


function extension($file){
	$ext = explode('.', $file);
	return $ext[1];
}
?>
