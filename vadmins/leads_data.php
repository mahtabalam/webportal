<?php 

include_once '../include/config.php';
require_once("$config[BASE_DIR]/include/function.php");
require_once('adminfuncs.php');

if (is_admin_loggedin()) {
	$date=trim($_REQUEST['date']);
	$campaign_id=trim($_REQUEST['id']);
	$sql=$reportConn->Prepare("select * from leads where date=?");
	$rs=$reportConn->Execute($sql,array($date));
	$output=array();
	$data=$rs->getrows();
	foreach($data as $row){
		  $process_row=false;
		
		$val=json_decode($row['data'],true);
		if(array_key_exists("campaign_id",$val)&&$val["campaign_id"]==$campaign_id){
		  $process_row=true;	
		}
		else 
		{
			continue;
		}
		$out_temp="";
		foreach($val as $key=>$value){
			$out_temp.=str_replace('#'," ",$value)."#";
		}
		$output[]=$out_temp;
	}
foreach($output as $val){
	echo $val."<br>";
}
}
else {
    if($_REQUEST['page']!='login')
        doForward("index.php");
}

?>