<?php

require_once ABSPATH . "/common/portal/VdopiaPortalObject.php";
include_once(dirname(__FILE__).'/../../adserver/vdoflo/xmlparser.php');
include_once(dirname(__FILE__).'/../../cliTools/serverConf.php');

class VdoFloStatusReport extends VdopiaPortalObject {
	
	private $startDate;
	private $endDate;
	private $timeFrame = "summary";
	private $reportType = "serving";
	private $timeFrameArray = array ("Summary", "Daily" );
	private $data;
	private $measures;
	private $channels;
	private $channelStr;
	private $db_prefix;
	
	public function __construct() {
		parent::__construct ();global $config;
		$this->startDate = $_SESSION ['vdofloStatusReport'] ['startDate'];
		$this->endDate = $_SESSION ['vdofloStatusReport'] ['endDate'];
		$this->timeFrame = $_SESSION ['vdofloStatusReport'] ['timeFrame'];
		if (isset ( $this->timeFrame ) == false) {
			$this->timeFrame = "summary";
		}
	
		if (isset ( $this->reportType ) == false) {
			$this->reportType = "serving";
		}
		
		if (isset ( $this->startDate ) == false) {
			$this->endDate = date ( "Y-m-d" );
			$this->startDate = $this->getMonth ( $this->endDate );
		}
		$this->measures = array(
									"n_ai" => "Network Available Inventory",
   									"n_ui" => "Network Unfilled Inventory",
  									"n_uui" => "Network Unused Inventory",
  									"vf_fe6" => "Couldn't resolve host. The given remote host was not resolved.",
  									"vf_fe7" => "Failed to connect() to host or proxy.",
  									"vf_fe28" => "Operation timeout. The specified time-out period was reached according to the conditions.",
  									"vf_qp" => "Local queue has been pushed to sqs",
  									"vf_qe" => "Local queue is empty",
  									"vf_qpe" => "Error in sending message to queue",
  									"vf_par" => "vdoflo_queue_push script is already running",
  									"vf_aif" => "Ad insertion is failed",
  									"vf_aex" => "Ad already exist",
  									"vf_nc" => "Campaign does not exist",
  									"vf_cmif" => "Campaign members insertion failed",
  									"vf_anaif" => "ad_network_ads insertion failed",
  									"vf_vf" => "Operation vdofy failed",
  									"vf_vdlf" => "Operation vdo file download failed",
  									"vf_fmf" => "Operation file moving from temp to videos is failed",
  									"vf_cf" => "Downloaded file is corrupt",
  									"vf_gfnf" => "Unique file generation is failed",
  									"vf_var" => "vdoflo_queue_management script is already running",
  									"vf_vlr" => "Conversion Limit reached",
  									"vf_ac" => "Ad created successfully",
  									"vf_sqse" => "SQS is empty",
  									"vf_xc" => "Corrupted xml",
									"vf_iimp" => "Impression tracker not available",
									"vf_iclk" => "ClickThrough tracker not available",
									"vf_itrke" => "Tracking events not available",
									"vf_bmf" => "Bad media files received"
  								);
  		$this->channels = $this->getChannels();	
  		if(isset($config['DB_PREFIX']) && $config['DB_PREFIX']!='') {
  			$this->db_prefix=$config['DB_PREFIX'];
  		}else $this->db_prefix='';
	}
	
	private function getChannels(){
		$channels=array();
		$sql = "select ch.id, ch.name, ch.publisher_id from channels ch, channel_settings chs where ch.id=chs.channel_id and chs.additional_settings regexp 'network_list' order by ch.publisher_id, ch.id";
		$rs = $this->conn->execute($sql);
		if($rs&&$rs->recordcount()>0){
			$rows = $rs->getrows();
			foreach($rows as $key => $row){
				$channels[$row['id']]=$row['name']." (pubid: ".$row['publisher_id'].", chid: ".$row['id'].")";
			}
		}
		return $channels;
	}
	
	private function getMonth($date) {
		$time = strtotime ( $date );
		$date = date ( "Y-m-d", mktime ( 0, 0, 0, date ( "m", $time ), date ( "d", $time ) - date ( "j", $time ) + 1, date ( "Y", $time ) ) );
		return $date;
	}
	
	private function getData() {
		global $servers;
		switch($this->reportType){
			case 'serving':
				$sql = $this->getSql("{$this->db_prefix}aggregate_channels");
				if(trim($sql)!=""){
					$rs = $this->reportConn->execute($sql);
					if($rs&&$rs->recordcount()>0) $this->data = $rs->getrows();
				}
			break;
			case 'conversion':
				$sql = $this->getSql("ad_log0");
				if(trim($sql)!=""){
					$rs = $this->conn->execute($sql);
					if($rs&&$rs->recordcount()>0) $this->data = $rs->getrows(); else $this->data = null;
				}
				
			break;
			case 'sqs':
				$prefix="adserver/vdoflo/vdoflo_sqs_status.php";
  				$secret=$_SESSION['ADMIN_ID'];
  				if($_SERVER['HTTP_HOST']=="i2.vdopia.com") $request = "http://i2.vdopia.com/dev/pankaj_adserver/$prefix?secret=".$secret; else $request = "http://www.vdopia.com/$prefix?secret=".$secret;
				$xmlContent = $this->getContent($request);
				if(trim($xmlContent)!="") $this->fillData($xmlContent);
			break;
			case 'redis':
				$prefix="adserver/vdoflo/vdoflo_queue_status.php";
  				$secret=$_SESSION['ADMIN_ID'];
				foreach($servers as $server){
					$request = "http://".$server."/".$prefix."?secret=".$secret;
					$xmlContent = $this->getContent($request);
					if(trim($xmlContent)!="") $this->fillData($xmlContent);
				}
			break;			
		}
	}
	
	private function fillData($xml){
		$xmlObj = new VastXMLParser($xml);
		$dat = $xmlObj->getNodeData('Ad');
		if(sizeof($dat)>0){
			foreach($dat as $key => $row){
				$id = $row->NetworkAdId;
				$this->data["$id"]=(array)$row;
			}
		}
	}
	private function getContent($request){
		$curlSession = curl_init($request);
		curl_setopt($curlSession, CURLOPT_HEADER, false);
		curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
		$xmlResponse = curl_exec($curlSession);
		curl_close($curlSession);
		return $xmlResponse;
	}
	
	private function getSql($table) {
		$sql = "";
		if($this->channelStr!="") $sqlSnip = " and channel_id in ($this->channelStr) ";
		if(trim($table)=="{$this->db_prefix}aggregate_channels"){
			$sql = "select ".$this->getSnip('column', 'date')." 
						sum(if(measure='vf_fe6',val,0)) 'vf_fe6',
						sum(if(measure='vf_fe7',val,0)) 'vf_fe7',
						sum(if(measure='vf_fe28',val,0)) 'vf_fe28',
						sum(if(measure='n_ai',val,0)) 'n_ai',
						sum(if(measure='n_ui',val,0)) 'n_ui',
						sum(if(measure='vf_xc',val,0)) 'vf_xc',
						sum(if(measure='n_uui',val,0)) 'n_uui',
						sum(if(measure='vf_par',val,0)) 'vf_par', 
						sum(if(measure='vf_qp',val,0)) 'vf_qp',
						sum(if(measure='vf_qe',val,0)) 'vf_qe',
						sum(if(measure='vf_qpe',val,0)) 'vf_qpe',
						sum(if(measure='vf_bmf',val,0)) 'vf_bmf'
					from 
						$table
					where 
						date>='".$this->startDate."' and date<='".$this->endDate."'  $sqlSnip ".$this->getSnip('groupby', 'date');
		}elseif(trim($table)=="ad_log0"){
			$sql = "select ".$this->getSnip('column', 'date(t.tstamp)')." 
						sum(if(t.measure='vf_var',t.value,0)) 'vf_var',
						sum(if(t.measure='vf_vlr',t.value,0)) 'vf_vlr',
						sum(if(t.measure='vf_ac',t.value,0)) 'vf_ac',
						sum(if(t.measure='vf_sqse',t.value,0)) 'vf_sqse',
						sum(if(t.measure='vf_vdlf',t.value,0)) 'vf_vdlf',
						sum(if(t.measure='vf_cf',t.value,0)) 'vf_cf',
						sum(if(t.measure='vf_fmf',t.value,0)) 'vf_fmf',
						sum(if(t.measure='vf_gfnf',t.value,0)) 'vf_gfnf', 
						sum(if(t.measure='vf_vf',t.value,0)) 'vf_vf',
						sum(if(t.measure='vf_aif',t.value,0)) 'vf_aif',
						sum(if(t.measure='vf_aex',t.value,0)) 'vf_aex',
						sum(if(t.measure='vf_nc',t.value,0)) 'vf_nc',
						sum(if(t.measure='vf_cmif',t.value,0)) 'vf_cmif',
						sum(if(t.measure='vf_anaif',t.value,0)) 'vf_anaif'
					from 
						(select * from ad_log0 Union all select * from ad_log1) t 
					where 
						date(t.tstamp)>='".$this->startDate."' and date(t.tstamp)<='".$this->endDate."' $sqlSnip ".$this->getSnip('groupby', 'date(t.tstamp)');
		}
		return $sql;
	}
	
	private function getSnip($action, $column) {
		$snip = "";
		switch ($this->timeFrame) {
			case "daily" :
				if($action=="column"){
					if($this->channelStr!="") $snip = " $column as date, channel_id, "; else $snip = " $column as date, ";
				}elseif($action=="groupby"){
					if($this->channelStr!="") $snip = " group by $column, channel_id "; else $snip = " group by $column ";
				}
				break;
			case "summary":
				if($action=="column"){
					if($this->channelStr!="")  $snip = " channel_id, ";
				}elseif($action=="groupby"){
					if($this->channelStr!="")  $snip = " group by channel_id ";
				}
				break;	
		}
		return $snip;
	}
	
	private function getSummaryData(){
		$data = array("ads_created_today" => 0,"message_processed_today" => 0, "bad_media_file" => 0, "message_pushed_today" => 0,"available_inventory_today" => 0,"unfilled_inventory_today" => 0,"unfilled_ratio" => 0);
		
		# get serving stats
		 $servingSql = "select {$this->db_prefix}aggregate_channels.channel_id, name, sum(if(measure='vf_bmf',val,0)) 'vf_bmf', sum(if(measure='n_ai',val,0)) 'n_ai', sum(if(measure='vi',val,0)) 'vi', sum(if(measure in ('n_ui', 'vf_xc'),val,0)) 'n_ui',  sum(if(measure='vf_qp',val,0)) 'vf_qp', round((sum(if(measure in('n_ui','vf_xc'), val,0))/sum(if(measure='n_ai', val, 0)))*100,2) as 'unfilled_ratio', round((sum(if(measure='n_ai',val,0)) - (sum(if(measure in ('n_ui', 'vf_xc'),val,0))))/sum(if(measure='n_ai',val,0))*100,2) as 'utilization_ratio', round((sum(if(measure='vi',val,0))/(sum(if(measure='n_ai',val,0)) - (sum(if(measure='n_ui',val,0)) + sum(if(measure='vf_xc',val,0)))))*100,2) as 'efficiency_ratio', if(channel_settings.additional_settings regexp 'network_list', 1, 0) as vdoflo from {$this->db_prefix}aggregate_channels join channels on channels.id={$this->db_prefix}aggregate_channels.channel_id join channel_settings on channel_settings.channel_id={$this->db_prefix}aggregate_channels.channel_id where date>=date(date_sub(now(), interval 1 day)) group by {$this->db_prefix}aggregate_channels.channel_id having sum(if(measure='n_ai',val,0))>0";
		$rss = $this->reportConn->execute($servingSql);
		if($rss&&$rss->recordcount()>0) $serving = $rss->getrows();
		
		# get conversion stats
		$conversionSql = "select sum(if(t.measure='vf_ac',t.value,0)) 'vf_ac', sum(if(t.measure in ('vf_ac', 'vf_vdlf', 'vf_vf', 'vf_aif', 'vf_aex', 'vf_nc', 'vf_cmif', 'vf_anaif'),t.value,0)) 'processed' from (select * from ad_log0 Union all select * from ad_log1) t where date(t.tstamp)>=date(date_sub(now(), interval 1 day))";
		$rsc = $this->conn->execute($conversionSql);
		if($rsc&&$rsc->recordcount()>0) $conversion = $rsc->getrows();
		
		if(isset($conversion[0]['vf_ac'])) $data['ads_created_today'] = $conversion[0]['vf_ac'];
		if(isset($conversion[0]['processed'])) $data['message_processed_today'] = $conversion[0]['processed'];
		
		foreach ($serving as $key => $row){
			$data['bad_media_file']+=$row['vf_bmf'];
			$data['message_pushed_today']+=$row['vf_qp'];
			$data['available_inventory_today']+=$row['n_ai'];
			$data['unfilled_inventory_today']+=$row['n_ui'];
		}
		$data['unfilled_ratio'] = round($data['unfilled_inventory_today']/$data['available_inventory_today']*100, 2);
		$this->data=$data;
		$this->smarty->assign ( 'serving', $serving );
	}
	
	public function run() {
	  switch($_REQUEST['vdoflo']){
	  		case 'vdoflostatus':
	  			# call get summary data
	  			$this->getSummaryData();
	  		break;
	  		case 'vdofloadvancedstatus':
		  		if($_REQUEST['submit']){	
					if (isset ( $_REQUEST ['start_date'] )) {
						$this->startDate = $_REQUEST ['start_date'];
					}
			
					if (isset ( $_REQUEST ['end_date'] )) {
						$this->endDate = $_REQUEST ['end_date'];
					}
			
					if (isset ( $_REQUEST ['timeframe'] )) {
						$this->timeFrame = $_REQUEST ['timeframe'];
					}
			
					if (isset ( $_REQUEST ['reporttype'] )) {
						$this->reportType = $_REQUEST ['reporttype'];
					}
			
					if (isset ( $_REQUEST ['channel'] )) {
						$this->channelStr = implode(',', $_REQUEST ['channel']);
					}
	
					# call get data
					$this->getData ();
		  		}
	  		break;	
	  }	
	  
	  $this->smarty->assign ( 'data', $this->data );
	  $this->smarty->assign ( 'noofrecords', sizeof($this->data) );
	  $this->smarty->assign ( 'measures', $this->measures );
	  $this->smarty->assign ( 'timeframe', $this->timeFrameArray );
	  $this->smarty->assign ( 'channels', $this->channels );
	  $this->smarty->assign ( 'channelStr', $this->channelStr );
	  $_REQUEST ['start_date']=$this->startDate;
	  $_REQUEST ['end_date']=$this->endDate;
	}
}

?>
