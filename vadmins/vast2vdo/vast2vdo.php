<?php

require_once "../include/config.php";

class Vast2VDO {
	
	public function __construct(){
		global $smarty,$conn;
		$this->smarty=&$smarty;
		$this->queryConn=$conn;		
	}
	
	public function run() {
		
		if (isset($_REQUEST['sec']) && $_REQUEST['sec'] != "") {
			
			if ($_REQUEST['sec'] == "cp") {
				
				// content provider
				switch($_REQUEST['action']){
					case 'add': 	$this->addNewContentProvider();
									break;
					case 'update':
									$this->updateContentProvider();
									break;
					case 'get':
									$this->getContentProviders();
									break;
					}
			} else {
							// video content
			}
		}
	}
	
	private function addNewContentProvider() {
		$name = $_POST['name'];
		$website = $_POST['website'];
		$email = $_POST['email'];
		$contact = $_POST['contact'];
		$desc = $_POST['description'];
		$date = now();
		$status = 'true';
		
		$sql = "INSERT INTO content_provider (name, website, email, contact, description, addedOn, status) 
			    VALUES($name, $website, $email, $contact, $desc, NOW(), true)";
		$res = $this->queryConn->execute($sql);
	}
	
	private function updateContentProvider() {
		$cpid = mysql_escape_string($_REQUEST['cpid']);
		$sql = "UPDATE content_provider
				SET status=false
				WHERE id=$cpid";
		$res = $this->queryConn->execute($sql);
		if($res){
      			echo '<script>alert("Updated Successfully")</script>';
      		}else{
      			echo '<script>alert("Some Error Occured")</script>';
      		}
	}
	
	private function getContentProviders() {
		
		$CURRENT_PAGE=mysql_escape_string($_REQUEST['cur_page']);
		$RECORDS_PER_PAGE=mysql_escape_string($_REQUEST['records_per_page']);
		$SORT_BY=mysql_escape_string($_REQUEST['sortBy']);
		$DIR=mysql_escape_string($_REQUEST['dir']);
		
		$sql="select count(id) as count from content_provider";
		$res=$this->queryConn->execute($sql);
		$totalCount=$res->fields['count'];
		$to=($CURRENT_PAGE)*$RECORDS_PER_PAGE;
		$from=$to-$RECORDS_PER_PAGE;
				
		$sql = "SELECT * 
				FROM content_provider 
				ORDER BY ".$SORT_BY." ".$DIR." 
				LIMIT ".$from.", ".$RECORDS_PER_PAGE;
		$res = $this->queryConn->execute($sql);
		$stack = array();
		if ($res && $res->recordcount() > 0) {
		$arr = $res->getrows();
			for($i = 0; $i < $totalCount; $i++) {
				$arr = array($arr[$i]['id'],$arr[$i]['name'],$arr[$i]['website'],$arr[$i]['contact'],$arr[$i]['email'],$arr[$i]['addedOn'],$arr[$i]['status']);
				array_push($stack, $arr);
			}
		}
		echo '{"totalRecords":'.$totalCount.',"curPage":'.$CURRENT_PAGE.',"data":'.json_encode($stack).'}';
		exit;
	}

}