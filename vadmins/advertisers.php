<?php
//ini_set('display_errors',1);

include_once("../include/datefuncs.php");
include_once("../common/portal/CreditProcessingPortal.php");

if(is_admin_loggedin()) {
	if(isset($config['DB_PREFIX']) && $config['DB_PREFIX']!=''){
		$db_prefix =$config['DB_PREFIX'];
	
	}else {
		$db_prefix='';
	}
  switch ($_REQUEST['sub']) {

    case 'collections_expiredate':
      $smarty->assign("monthoptions",$monthoptions);
      $smarty->assign("yearoptions",$yearoptions);
      if(!isset($_REQUEST['month']) || !isset($_REQUEST['year'] )){
        $_REQUEST['month']=date("m");
        $_REQUEST['year']=date("Y");


      }

      $month= mysql_escape_string($_REQUEST['month']);		
      $year = mysql_escape_string($_REQUEST['year']);		
      $sql = "select sum(impressions) as impressions , sum(total) as total,budget_currency, name, campaign_id, date(campaign.validto) as finishdate, email from {$db_prefix}aggregate_statistics left join campaign on campaign.id={$db_prefix}aggregate_statistics.campaign_id left join advertiser on advertiser.id=campaign.advertiser_id left join advertisersheet on advertiser.id=advertisersheet.advertiser_id where month(campaign.validto)=$month and year(campaign.validto)=$year and campaign.device='pc' group by campaign_id"  ;
      $rs = $reportConn->execute($sql);
      if($rs && $rs->recordcount()>0){


        $collections = $rs->getrows();
        $smarty->assign('collections',$collections);

      }
      break;


    case 'collections_monthwise':
      $smarty->assign("monthoptions",$monthoptions);
      $smarty->assign("yearoptions",$yearoptions);
      if(!isset($_REQUEST['month']) || !isset($_REQUEST['year'] )){
        $_REQUEST['month']=date("m");
        $_REQUEST['year']=date("Y");


      }

      $month= mysql_escape_string($_REQUEST['month']);		
      $year = mysql_escape_string($_REQUEST['year']);		
      $sql = " select sum(impressions) as impressions, sum(total) as total, name, campaign_id, email from {$db_prefix}aggregate_statistics left join campaign on campaign.id={$db_prefix}aggregate_statistics.campaign_id left join advertiser on advertiser.id=campaign.advertiser_id where month(date)=$month and campaign.device='pc' group by campaign_id "  ;
      $rs = $reportConn->execute($sql);
      if($rs && $rs->recordcount()>0){


        $collections = $rs->getrows();
        $smarty->assign('collections',$collections);

      }
      break;
    case 'active':
      if(!$_REQUEST['cstatus']) $_REQUEST['cstatus']='all';
      if(strtolower($_REQUEST['cstatus']) == 'all'){
        $statusstring = '';
      }else{
        $statusstring = " status = '".$_REQUEST['cstatus']."' and ";
      }

      if(strtolower($_REQUEST['activate'])=="true" && $_REQUEST['activate']!=''){
        $sql="update campaign set status='active' where id='" . $_REQUEST['cid'] . "'";
        $rs = $conn->execute($sql);
      }

      $sql = "SELECT 
        advertiser.email, 
        campaign.*, 
        geography.country_codes, 
        geography.state_codes, 
        geography.postalcodes, 
        campaign_UI.timezone, 
        campaign_UI.starttime, 
        campaign_UI.endtime, 
        group_concat(cast(blocked_campaigns.channel_id as char) separator ',') as channel 
          FROM 
          campaign 
          LEFT JOIN geography ON campaign.id = geography.campaign_id 
          LEFT JOIN blocked_campaigns ON campaign.id = blocked_campaigns.campaign_id 
          LEFT JOIN campaign_UI ON campaign.id = campaign_UI.campaign_id  
          LEFT JOIN advertiser ON campaign.advertiser_id = advertiser.id 
          WHERE 
          $statusstring 
          device='pc' 
          GROUP BY id"  ;
      $rs = $reportConn->execute($sql);

      if($rs && $rs->recordcount()>0){
        $campaignsummary = $rs->getrows();
        $smarty->assign('campaignsummary',$campaignsummary);
      }
      break;
    case 'recent_creditupdates':
      $sql = "SELECT id,fname,lname,email from advertiser where type='online' order by fname";
      $rs = $reportConn->execute($sql);
      if($rs && $rs->recordcount()>0){
        $advertisers = $rs->getrows();
        $smarty->assign('advertisers',$advertisers);
      }
      if(isset($_REQUEST['fromDate']) && isset($_REQUEST['toDate']) && isset($_REQUEST['advertiser']) && $_REQUEST['fromDate']!='' && $_REQUEST['toDate']!='')
      {
       
        if($_REQUEST['advertiser']=='all')
        {
        $sql = "select id,fname,lname,email,last_updated,credit_amt from adv_credit,advertiser where 
          id=adv_id and date(last_updated)>='$_REQUEST[fromDate]' and date(last_updated)<='$_REQUEST[toDate]' and type='online' 
          order by fname";
        }
        else
        {
        $sql = "select id,fname,lname,email,last_updated,credit_amt from adv_credit,advertiser where 
         id=adv_id and date(last_updated)>='$_REQUEST[fromDate]' and date(last_updated)<='$_REQUEST[toDate]' and adv_id='$_REQUEST[advertiser]' and type='online'";
        }
        $rs = $reportConn->execute($sql);
        if($rs && $rs->recordcount()>0){
          $creditsummary = $rs->getrows();
          $smarty->assign('creditsummary',$creditsummary);
        
        }
        $_REQUEST['start_date'] = $_REQUEST['fromDate'];
        $_REQUEST['end_date'] = $_REQUEST['toDate'];
        
      $_REQUEST['advertiser']=$_REQUEST['advertiser'];
      }
      else
      {
        global $dateformat;
      $dates=lastNdays(1);
      $_REQUEST['start_date']=date($dateformat, strtotime($dates[0]));
      $_REQUEST['end_date']=date($dateformat, strtotime($dates[1]));
      $_REQUEST['advertiser']="";
      }
      break;
    case 'advall':
      $creditprocessing=new CreditProcessingPortal();
      $creditprocessing->commonCode('online');
      break;
    case 'viewadv':
      if( $_REQUEST['email']!="") {
        # clear the session accept admin credentials
      	clearSession(array("ADMIN_ID","ADMIN_PRIVILEGES","CAPABILITIES"));
        
        /*TODO add verification code/rules*/
        try {
          $email=getRequest('email', true);
          $sql="select 
            		advertiser.id, advertiser.timezone, fname, lname, email, currency,advertiser.acc_type, publisher_id
              from 
              		advertiser, advertisersheet
              where 
              		advertiser.id = advertisersheet.advertiser_id and
              		advertiser.type='online' and  
              		email='$email'";
          $rs=$reportConn->execute($sql);
          if($rs->recordcount()>0) {
            if(isset($rs->fields['publisher_id'])) {
              $_SESSION['PUB_ID']=$rs->fields['publisher_id'];
              $_SESSION['PUB_ADV_ID']=$rs->fields['id']; 
              $_SESSION['ADV_PUB_ID']=$rs->fields['publisher_id'];
            }
			$_SESSION['ACCOUNT_TYPE']='advertiser';
            $_SESSION['ADV_ID']=$rs->fields['id'];
            $_SESSION['FNAME']=$rs->fields['fname'];
            $_SESSION['LNAME']=$rs->fields['lname'];
            $_SESSION['EMAIL']=$rs->fields['email'];
            $_SESSION['ADV_TIMEZONE']=$rs->fields['timezone'];
            $_SESSION['CURRENCY3DGT']=$rs->fields['currency'];
            $_SESSION['ACC_TYPE']=$rs->fields['acc_type'];
            $_SESSION['CURRENCYVAL']=insert_3dgt_to_val(array("3dgt" =>$rs->fields['currency']));
            $_SESSION['CURRENCYSYM']=insert_val_to_currency(array("value" => $_SESSION['CURRENCYVAL']));
            

            if(isset($_SESSION['CAMPAIGN']['currency'])) {
              $_SESSION['CAMPAIGN']['currency']=$_SESSION['CURRENCYVAL'];
            }
            // $sql="update advertiser set lastlogin=now() where id=$_SESSION[ADV_ID]";
            // $conn->execute($sql);
            //abhaydoForward
            doForward("$config[baseurl]/adv.php");
          }
        } catch(Exception $e){
        }
        $smarty->assign("error","true");
      } elseif (is_advertiser_loggedin() ) {
      	//abhaydoForward
        doForward("$config[baseurl]/adv.php");
      }

      break;
     case 'defaultAccountType':
      	if(isset($_REQUEST['action'])){
      		$sql=$conn->Prepare("update advertiser set acc_type=? where id=?");
      		$rs=$conn->Execute($sql,array($_REQUEST['acc_type'],$_REQUEST['id']));
      		if($conn->Affected_rows()>0){
      			$msg="Updated Successfully";
      		}else{
      			$msg="Already Selected";
      		}
      		$smarty->assign("msg",$msg);
      	}
      	$query=$conn->Prepare("Select acc_type from advertiser where id=? and acc_type is not null");
      	$res=$conn->Execute($query,array($_REQUEST['id']));
      	if($res && $res->recordcount()>0){
      		$_REQUEST['acc_type']=$res->fields['acc_type'];
      	}
      	$smarty->display("vadmins/innercontent/defaultAccountType.tpl");
      	exit(1);
      	break;
    case 'campaignsummary':

      if($_REQUEST['status']=='paused'){

        $campaignids=$_REQUEST['campaignids'];
        if(!empty($campaignids)){
          $ids = mysql_escape_string("(".implode( ',', $campaignids ).")");
          $sql = "update campaign set status = 'paused' where id in $ids and status = 'active'";
          $rs = $conn->execute($sql);
          $rows = $conn->Affected_Rows();
          if($rows>0){
            $error = "Campaign(s) paused successfully";

          }else{
            $error = "Only active campaigns can be paused";
          }
        }else{
          $error = "No campaign selected to be paused";
        }


      }elseif($_REQUEST['status']=='resumed'){
        $campaignids=$_REQUEST['campaignids'];
        if(!empty($campaignids)){
          $ids = mysql_escape_string("(".implode( ',', $campaignids ).")");
          $sql = "update campaign set status = 'active' where id in $ids and status = 'paused'";
          $rs = $conn->execute($sql);
          $rows = $conn->Affected_Rows();
          if(count($campaignids)==$rows){
            $error = "Campaign(s) resumed successfully";

          }else{
            $error = "Only paused campaigns can be resumed";
          }
        }else{
          $error = "No campaign selected to be resumed";
        }

      } elseif($_REQUEST['status']=='deleted'){
        if(!empty($campaignids)){
          $campaignids=$_REQUEST['campaignids'];
          $ids = "(".implode( ',', $campaignids ).")";
          $sql = "update campaign set status = 'deleted' where id in $ids";
          $rs = $conn->execute($sql);
          $rows = $conn->Affected_Rows();
          if($rows>0){
            $error = "Campaign deleted successfully";

          }
        }else{
          $error = "No campaign selected to be deleted";
        }

      }
      elseif($_REQUEST['status']=='expired'){
        $campaignids=$_REQUEST['campaignids'];
        if(!empty($campaignids)){
          $ids = mysql_escape_string("(".implode( ',', $campaignids ).")");
          $sql = "update campaign set status = 'active' where id in $ids and status = 'expired' and validto >=curdate()";
          $rs = $conn->execute($sql);
          $rows = $conn->Affected_Rows();
          if(count($campaignids)==$rows){
            $error = "Campaign(s) resumed successfully";

          }else{
            $error = "Date is not greater then current date so it can't be resumed";
          }
        }else{
          $error = "No campaign selected to be resumed";
        }

      }
      $dates=processDates($_REQUEST,'timeperiod',$_SESSION['ADV_ID']);
      $_REQUEST['start_date']=date($viewformat, strtotime($dates[0]));
      $_REQUEST['end_date']=date($viewformat, strtotime($dates[1]));


      $smarty->assign("err", $error);
      if($_REQUEST['time']=='dropdown' && $_REQUEST['timeperiod']=='alltime') {
        $datequery="";
      } else {
        $datequery=" and date>='$dates[0]' and date<='$dates[1]'";
      }
      $sql = "SELECT name, advertiser.email, advertiser_id, campaign.id AS cid, cast( budget_amt AS decimal ) AS budget_amt, budget_currency, daily_limit, sum( impressions ) AS timp, sum( clicks ) AS tclicks, cast( sum( total ) *1000 AS decimal ) AS total1000, budget_currency, daily_limit, status , review_status, group_concat( cast( blocked_campaigns.channel_id AS char ) SEPARATOR ',' ) AS channel FROM campaign left join advertiser on advertiser_id=advertiser.id LEFT JOIN {$db_prefix}aggregate_statistics ON campaign.id = campaign_id LEFT JOIN blocked_campaigns ON campaign.id = blocked_campaigns.campaign_id WHERE (status='active' or status='paused') and campaign.device='pc' $datequery  GROUP BY cid";
      /*$sql = "select name, id as cid,  cast(budget_amt as decimal) as budget_amt, budget_currency, daily_limit, sum(impressions) as timp, sum(clicks) as tclicks, cast(sum(total)*1000 as decimal) as total1000, budget_currency, daily_limit, status, review_status from campaign left join aggregate_statistics on id=campaign_id where advertiser_id=$_SESSION[ADV_ID] $datequery group by cid";*/
      $rs=$reportConn->execute($sql);
      if($rs && $rs->recordcount()>0){
        $campaignsummary=$rs->getrows();
      }
      $smarty->assign('campaignsummary',$campaignsummary);
      break;

    case 'campaignmgmt':
      switch(strtolower($_REQUEST['table'])){
        case "viewcamp":
          $sql = "SELECT 
          advertiser.email, 
          campaign.*, 
          group_concat(cast(blocked_campaigns.channel_id as char) separator ',') as channel 
            FROM 
            campaign 
            LEFT JOIN blocked_campaigns ON campaign.id = blocked_campaigns.campaign_id 
            LEFT JOIN advertiser ON campaign.advertiser_id = advertiser.id 
            WHERE 
            status='active' and
            device='pc'
            GROUP BY 
            id";
        $rs = $reportConn->execute($sql);
        if($rs && $rs->recordcount()>0){
          $campaignsummary = $rs->getrows();
          $smarty->assign('campaignsummary',$campaignsummary);
        }
        break;
        case "viewads":
          if($_REQUEST['sendemail']){
            $sql = "SELECT 
              cs.screenshot,
              ads.branded_img_right_ref_imgname as ad_name, 
              cm.name as campaign_name,
              adv.email as advertiser_email,
              concat(adv.fname,' ', adv.lname) as advertiser_name,
              ch.url as channel_name
                FROM
                campaign_screenshots cs
                left join campaign_members cmm on cmm.ad_id=cs.ad_id
                left join campaign cm on cmm.cid=cm.id
                left join ads on ads.id=cmm.ad_id
                left join advertiser adv on adv.id=ads.advertiser_id
                left join channels ch on ch.id=cs.channel_id
                WHERE 
                cs.ad_id='$_REQUEST[ad_id]' and
                cs.channel_id='$_REQUEST[channel_id]'";
            $rs = $reportConn->execute($sql);
            if($rs && $rs->recordcount()>0){
              $ad_name = $rs->fields('ad_name');
              $campaign_name = $rs->fields('campaign_name');
              $channel_name = $rs->fields('channel_name');
              $screenshot = $rs->fields('screenshot');

              $to = $rs->fields('advertiser_email');
              $toname = $rs->fields('advertiser_name');
              $cc="pankaj@vdopia.com,amit@vdopia.com";
              $subject = "Screenshot of ". $campaign_name . "( " . $ad_name . " ) for channel - ".$channel_name . ".";
              $fromname= "Vdopia Admin";

              $filename=str_replace(' ','_',$campaign_name);
              if($filename!='') $filename.='_';
              $filename.="(".str_replace(' ','_',$ad_name).")";	

              $smarty->assign("aname", $toname);
              $smarty->assign("campaign_name", $campaign_name);
              $smarty->assign("ad_name", $ad_name);
              $smarty->assign("channel_name", $channel_name);
              $smarty->assign("screenshot", $screenshot);

              $body = $smarty->fetch("emails/screenemail.tpl");
              $content = $smarty->fetch("emails/screencontent.tpl");

# send email
              if(sendAttachment($to, $fromname, "advertisers@vdopia.com", $subject, $body, $content, "html|".$filename, $cc)){
                $smarty->assign("emailmsg","Email has been sent.");
              }
              else{
                $smarty->assign("emailmsg","Please try after some time.");
              }
            }
          }

        $sql = "SELECT 
          ads.id as ad_id, 
          ads.branded_img_right_ref_imgname as ad_name, 
          cm.channel_choice
            FROM 
            campaign cm 
            left join campaign_members cmm on cmm.cid=cm.id 
            left join ads on ads.id=cmm.ad_id
            WHERE 
            cm.status='active' and 
            cm.device='pc' and 
            cm.id='$_REQUEST[campaign_id]'";
        $rs = $reportConn->execute($sql);
        if($rs && $rs->recordcount()>0){
          $ctr=0;
          while (!$rs->EOF){
            $adsummary[$ctr]['ad_id']=$rs->fields[ad_id];	
            $adsummary[$ctr++]['ad_name']=$rs->fields[ad_name];
            $channels=$rs->fields[channel_choice];

            $sql="select id as channel_id, url as channel, screenshot from channels left join campaign_screenshots on id=channel_id and ad_id='".$rs->fields('ad_id')."' where id in ($channels)";
            $rs_screen=$reportConn->execute($sql);
            if($rs_screen && $rs->recordcount()>0){
              $ads[]=$rs_screen->getrows();
            }
            $rs->movenext();
          }

          $jscript= '';
          foreach ($adsummary as $key => $value){
            foreach ($ads[$key] as $key1 => $value1){
              $jscript.='<script type="text/javascript">
                $(document).ready(function() { 
                    $("#fileUpload_'.$key.$key1.'").fileUpload({
                      "uploader": baseurl+"/uploadify/uploader.swf",
                      "cancelImg": baseurl+"/uploadify/cancel.png",
                      "script": "upload.php",
                      "folder": "",
                      "fileDesc": "Image Files",
                      "fileExt": "*.jpg;*.jpeg;*.gif;*.png",
                      "multi": false,
                      scriptData:{"ad_id":"'.$value['ad_id'].'", "channel_id":"'.$value1['channel_id'].'"},
                      "displayData": "speed",
                      "auto": true
                      });
                    });
              </script>';
            }
          }

          $smarty->assign('jscript',$jscript);
          $smarty->assign('adsummary',$adsummary);
          $smarty->assign('ads',$ads);
          $smarty->assign('currenturl',"http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
        }
        break;
      }

      break;

  }
} else {
	doForward("$config[BASEURL]/vadmins/?page=login&desturl=$_SERVER[REQUEST_URI]");
}
?>
