<?php 

class Vdopia{
	
	private $conn;
	private $smarty;
	private $startDate;
	private $endDate;
	
	
	public function __construct(){
		global $reportConn,$smarty;
		$this->conn=$reportConn;
		$this->smarty=$smarty;
		}
	

	public function setRange($startDate,$endDate){
		 
		$this->startDate=$startDate;
		$this->endDate=$endDate;
	}
	
public function run() {
	//for assigning date range checking 'action_submit' post value
		if ($_POST ['action_submit'] != "") {
			$this->setRange($_POST["start_date"],$_POST["end_date"]);
			}
		//on each generate this loop will be called
			if ($_POST ['submit'] != "") {	
							$this->setRange($_POST["start_date"],$_POST["end_date"]);

			$period = getRequest ( 'period' );
			switch ($period) {
				case 'day' :
					$period = 'd';
					break;
				case 'week' :
					//$date=UniquesFetcher::getWeek($date);
					$period = 'w';
					break;
				case 'month' :
					//$date=UniquesFetcher::getMonth($date);
					$period = 'm';
					break;
			}
			
			$measure = "$period.all.all.vi";
			$result = UniquesFetcher::getResult ( $this->startDate, $this->endDate, 0, 0, $measure );
			$data ["uniques"] = $result;
			$this->smarty->assign ( "data", $data );
		
		}
	}
}

?>