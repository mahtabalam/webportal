<?php
class UniquesFetcher{
	
	
	public static function getWeek($date){
		$time=strtotime($date);
		$date=date ( "Y-m-d", mktime ( 0, 0, 0, date ( "m" ,$time), date ( "d" ,$time) - date ( "w" ,$time), date ( "Y" ,$time) )) ;
		return $date;
	}
	
	public static function getMonth($date){
		$time=strtotime($date);
		$date=date ( "Y-m-d", mktime ( 0, 0, 0, date ( "m" ,$time), date ( "d" ,$time) - date ( "j" ,$time) + 1, date ( "Y" ,$time) ) );
		return $date;
	}
	
	public static function getResult($date_Start,$date_End,$user1,$user2,$measure){
		if(UniquesFetcher::checkRange($date_Start,$date_End)===false)return null;
		global $reportConn;
		$sql="select date,value from uniques where date between '$date_Start' and '$date_End' and user_1=$user1 and user_2=$user2 and measure='$measure'";
		$rs = $reportConn->execute ( $sql );
		//echo $sql;
		if (!$rs){
			throw new Exception($reportConn->ErrorMsg());
		}
		if($rs->recordcount()<=0)return null;
		$result=array();
		$count=1;
		while(!$rs->EOF) {
			$result[$count]=array('date'=>$rs->fields["date"],'value'=>$rs->fields["value"]);
			$count++;
			$rs->MoveNext();
		}	
	
		return $result;
	
	}
	
	public static function checkRange($dateS,$dateE){
		$timeS=strtotime($dateS);
		$timeE=strtotime($dateE);
		if ($timeS === false || $timeE=== false) {
			return false;
		}
		if($timeS>$timeE)return false;
		return true;
	}
	
	
}


?>