<?php

include_once 'AdvertiserReporting.php';
include_once 'PublisherReporting.php';
include_once 'AdvertiserUserReporting.php';
include_once 'Vdopia.php';

class UniqueReporting{
	
	

	private function getTypeList(){
		global $reportConn,$smarty,$config;
		$searchtext=mysql_real_escape_string($_POST['searchtext']);
		$type=mysql_real_escape_string($_POST['type']);
		if($type=="advertiser"){
			$sql="SELECT id,fname,lname,email,company_name,country FROM advertiser WHERE (advertiser.email like '%$searchtext%' or advertiser.fname like '%$searchtext%' or advertiser.lname like '%$searchtext%' or advertiser.company_name like '%$searchtext%') and advertiser.type='online'";
		}
		else if($type=="publisher"){
			$sql="SELECT id,fname,lname,email,company_name,country FROM publisher WHERE (publisher.email like '%$searchtext%' or publisher.fname like '%$searchtext%' or publisher.lname like '%$searchtext%' or publisher.company_name like '%$searchtext%') and publisher.type='online'";
		}
		else if($type=="advusr"){
						$sql="SELECT id,fname,lname,email,company FROM adv_user WHERE (adv_user.email like '%$searchtext%' or adv_user.fname like '%$searchtext%' or adv_user.lname like '%$searchtext%' or adv_user.company like '%$searchtext%') ";
		}
		else if($type=="vdopia"){
		   $url=$config['BASE_URL']."/vadmins/index.php?page=tools&sub=uniqueReporting&type=vdopia";
			
		  // echo $url;
		   header("Location: $url");
			exit(0);
		}
		//echo $sql;
		$rs = $reportConn->execute($sql);
        if($rs && $rs->recordcount()>0){
          $result= $rs->getrows();
          //echo print_r($result);
          $smarty->assign('data',$result);
        }
	}
	
	private function getSubListForType() {
		if ($_GET ['type'] == "advertiser") {
			$advReport = new AdvertiserReporting ();
			$advReport->run ();
		} else if ($_GET ['type'] == "publisher") {
			$pubReport = new PublisherReporting ();
			$pubReport->run ();
		} else if ($_GET ['type'] == "advusr") {
			$advusr = new AdvertiserUserReporting ();
			$advusr->run ();
		} else if ($_GET ['type'] == "vdopia") {
			$vdopia = new Vdopia ();
			$vdopia->run ();
		}
	}
	
	public function run(){
		if($_POST['action_search']){
			$this->getTypeList();
		}
		else if($_GET['type'] !=""){
			$this->getSubListForType();
		}
		/*
		else if($_POST['type']=="vdopia"){
			global $config;
			$url=$config['BASE_URL']."/vadmins/index.php?page=tools&sub=uniqueReporting&type=vdopia";
			
		  // echo $url;
		   header("Location: $url");
			exit(0);
		}*/
	}
	
}

?>