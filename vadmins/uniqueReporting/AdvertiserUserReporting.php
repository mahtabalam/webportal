<?php 

include_once('UniquesFetcher.php');
include_once("Utils.php");

class AdvertiserUserReporting{

	private $conn;
	private $smarty;
	private $startDate;
	private $endDate;
	private $advertiserId;
	private $id;
	private $cache;
	
	public function __construct(){
		global $reportConn,$smarty;
		$this->conn=$reportConn;
		$this->smarty=$smarty;
		$this->advertiserId=$_REQUEST['apid'];
		$this->id="advusr_$this->advertiserId";
		//$this->cache=new Utils("adv",$this->id);
		//$this->cacheKey=$this->cache->getKey();
		$this->startDate=$_SESSION['UR'][$this->id]['startDate'];
		$this->endDate=$_SESSION['UR'][$this->id]['endDate'];
	}
	
	private function getCampaigns() {
		//first check the cache
		if(isset($_SESSION['UR'][$this->id]['campaign_list']))
		return $_SESSION['UR'][$this->id]['campaign_list'];
		$campaignlist = array ();
		$sql = "select cg.name as name, cg.id as id from campaign cg join adv_user_campaign advusr on advusr.campaign_id=cg.id join uniques uniq on cg.id=uniq.user_1 and uniq.date between '$this->startDate' and '$this->endDate' and uniq.measure='d.ca.ch.vi' where advusr.adv_user_id=$this->advertiserId group by cg.id order by cg.name";
		//echo $sql;
	//	echo "Hiiiii";
		$rs = $this->conn->execute ( $sql );
		if ($rs && $rs->recordcount () > 0) {
			$campaignlist = $rs->getrows ();
		}
		$_SESSION['UR'][$this->id]['campaign_list']=$campaignlist;
		return $campaignlist;
	}
	
	//for ajax calls
	public function getChannles($campaignId,$flag=false){
			$channellist = array ();
		//for across advertiser
		if($campaignId==0){
				$sql = "select channels.name, channels.id from channels left join uniques on channels.id=uniques.user_2  and uniques.date between '$this->startDate' and '$this->endDate' and uniques.measure='d.advusr.ch.vi' where uniques.user_1=$this->advertiserId group by channels.id order by channels.name";		
		}
		else{
		$sql = "select channels.name, channels.id from channels left join uniques on channels.id=uniques.user_2  and uniques.date between '$this->startDate' and '$this->endDate' and uniques.measure='d.ca.ch.vi' where uniques.user_1=$campaignId group by channels.id order by channels.name";
		}
		//echo $sql;
		$rs = $this->conn->execute ( $sql );
	//	echo $sql;
		$str="";
		if(!$rs){
			return;
		}
		while (!$rs->EOF) {
			if($flag==true){
					$channellist=$rs->getRows();
				return $channellist;
			}$str.="<option value=\"".$rs->fields[1]."\">".$rs->fields[0]."</option>";
					$rs->MoveNext();
				}
				return $str;
		//return $channellist;
	
	}
	
	
	public function setRange($startDate,$endDate){
		$_SESSION['UR'][$this->id]['startDate']=$startDate;
		$_SESSION['UR'][$this->id]['endDate']=$endDate;	 
		unset($_SESSION['UR'][$this->id]['campaign_list']);  
		$this->startDate=$startDate;
		$this->endDate=$endDate;
	}
	
	/***
	 * function to be run at each request.$_REQUESR['apid'] is to be passed each time
	 */
	public function run() {
	//for assigning date range checking 'action_submit' post value
		if ($_POST ['action_submit'] != "") {	
			$this->setRange($_POST["start_date"],$_POST["end_date"]);
			$campaignList=$this->getCampaigns();
			$this->smarty->assign("campaignlist",$campaignList);
		}
		//on each generate this loop will be called
			if ($_POST ['submit'] != "") {	
			$campaignList=$this->getCampaigns();
		
			//echo $campaignList;
			$this->smarty->assign("campaignlist",$campaignList);
			$campaign_id = getRequest ( 'campaign' );
			$channel_id = getRequest ( 'channel' );

			$channelList=$this->getChannles($campaign_id,true);
			$this->smarty->assign("channellist",$channelList);
			
			$period = getRequest ( 'period' );
			switch ($period) {
				case 'day' :
					$period = 'd';
					break;
				case 'week' :
					//$date=UniquesFetcher::getWeek($date);
					$period = 'w';
					break;
				case 'month' :
					//$date=UniquesFetcher::getMonth($date);
					$period = 'm';
					break;
			}
			
			$measure_ca = "ca";
			$measure_ch = "ch";
			if ($campaign_id == "0") {
				$campaign_id = $this->advertiserId;
				$measure_ca = "advusr";
				$measure1 = "All Campaigns";
			}
			if ($channel_id == "0") {
				$measure_ch = "all";
				$measure2 = "All Channels";
			}
			$measure = "$period.$measure_ca.$measure_ch.vi";
			$result = UniquesFetcher::getResult ( $this->startDate, $this->endDate, $campaign_id, $channel_id, $measure );
			$data ["uniques"] = $result;
			$this->smarty->assign ( "data", $data );
			
		//echo "<pre>"; print_r($data);
		//print_r($_POST);
		}
	}
	
}
?>