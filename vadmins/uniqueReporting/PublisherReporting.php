<?php 


include_once('UniquesFetcher.php');

class PublisherReporting{

	private $conn;
	private $smarty;
	private $startDate;
	private $endDate;
	private $publisherId;
	private $id;
	private $cache;
	
	public function __construct(){
		global $reportConn,$smarty;
		$this->conn=$reportConn;
		$this->smarty=$smarty;
		$this->publisherId=$_REQUEST['apid'];
		$this->id="pub_$this->publisherId";
		//$this->cache=new Utils("adv",$this->id);
		//$this->cacheKey=$this->cache->getKey();
		$this->startDate=$_SESSION['UR'][$this->id]['startDate'];
		$this->endDate=$_SESSION['UR'][$this->id]['endDate'];
	}
	
	private function getChannels() {
	//first check the cache
		if(isset($_SESSION['UR'][$this->id]['channel_list']))
		return $_SESSION['UR'][$this->id]['channel_list'];
		$channellist = array ();
		$sql = "select ch.name as name, ch.id as id from channels ch join uniques uniq on ch.id=uniq.user_2 and uniq.date between '$this->startDate' and '$this->endDate' and uniq.measure='d.ca.ch.vi' where ch.publisher_id=$this->publisherId  group by ch.id order by ch.name";
		//echo $sql;
		$rs = $this->conn->execute ( $sql );
		if ($rs && $rs->recordcount () > 0) {
			$channellist = $rs->getrows ();
		}
		//echo $channellist;
		$_SESSION['UR'][$this->id]['channel_list']=$channellist;
		return $channellist;
	}
		
	//for ajax calls
	public function getCampaigns($channelId,$flag=false) {
		$channellist = array ();
		//for across advertiser
		if ($channelId == 0) {
			$sql = "select campaign.name, campaign.id from campaign left join uniques on campaign.id=uniques.user_1  and uniques.date between '$this->startDate' and '$this->endDate' and uniques.measure='d.ca.pub.vi' where uniques.user_2=$this->publisherId group by campaign.id order by campaign.name";
		} else {
			$sql = "select campaign.name, campaign.id from campaign left join uniques on campaign.id=uniques.user_1  and uniques.date between '$this->startDate' and '$this->endDate' and uniques.measure='d.ca.ch.vi' where uniques.user_2=$channelId group by campaign.id order by campaign.name";
		}
		//echo $sql;
		$rs = $this->conn->execute ( $sql );
		//	echo $sql;
		$str = "";
		if (! $rs) {
			return;
		}
		while ( ! $rs->EOF ) {
		if($flag==true){
					$channellist=$rs->getRows();
				return $channellist;
			}
			$str .= "<option value=\"" . $rs->fields [1] . "\">" . $rs->fields [0] . "</option>";
			$rs->MoveNext ();
		}
		return $str;
	}
	
	public function setRange($startDate, $endDate) {
		$_SESSION ['UR'] [$this->id] ['startDate'] = $startDate;
		$_SESSION ['UR'] [$this->id] ['endDate'] = $endDate;
		unset ( $_SESSION ['UR'] [$this->id] ['channel_list'] );
		$this->startDate = $startDate;
		$this->endDate = $endDate;
	}
	
	/***
	 * function to be run at each request.$_REQUESR['apid'] is to be passed each time
	 */
	public function run() {
		//for assigning date range checking 'action_submit' post value
		if ($_POST ['action_submit'] != "") {	
			$this->setRange($_POST["start_date"],$_POST["end_date"]);
			$channelList=$this->getChannels();
			$this->smarty->assign("campaignlist",$channelList);
		}

		//on each generate this loop will be called
		if ($_POST ['submit'] != "") {
			$channelList=$this->getChannels();
			$this->smarty->assign("campaignlist",$channelList);
			$campaign_id = getRequest ( 'channel' );
			$channel_id = getRequest ( 'campaign' );
			
			$campaignList=$this->getCampaigns($channel_id,true);
			//echo "Hi";
			//echo print_r($campaignList);
			$this->smarty->assign("channellist",$campaignList);
			
			$period = getRequest ( 'period' );
			switch ($period) {
				case 'day' :
					$period = 'd';
					break;
				case 'week' :
					//$date=UniquesFetcher::getWeek($date);
					$period = 'w';
					break;
				case 'month' :
					//$date=UniquesFetcher::getMonth($date);
					$period = 'm';
					break;
			}
			$measure_ca = "ca";
			$measure_ch = "ch";
			if ($campaign_id == "0") {
				$measure_ca = "all";
				$measure1 = "All Campaigns";
			}
			if ($channel_id == "0") {
				$channel_id = $this->publisherId;
				$measure_ch = "pub";
				$measure2 = "All Channels";
			}
			$measure = "$period.$measure_ca.$measure_ch.vi";
			//UniquesFetcher::getResult($date,adv_id/campiagn_id,0/channel_id, d/w/m . adv/ca . all/ch .vi );
			$result = UniquesFetcher::getResult (  $this->startDate, $this->endDate, $campaign_id, $channel_id, $measure );
			
			$data ["uniques"] = $result;
			//$data ["measure1"] = $measure1;
			//$data ["measure2"] = $measure2;
			$this->smarty->assign ( "data", $data );
			
		//echo "<pre>"; print_r($data);
		//print_r($_POST);
		}
	}
	
}
?>