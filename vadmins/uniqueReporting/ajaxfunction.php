<?php 

include_once '../../include/config.php';
include_once 'AdvertiserReporting.php';
include_once 'PublisherReporting.php';
include_once 'AdvertiserUserReporting.php';

class AjaxFunction{
	
	private $conn;
	private $smarty;
	private $startDate;
	private $endDate;
	private $advertiserId;
	
	public function __construct(){
		global $reportConn,$smarty;
		$this->conn=$reportConn;
		$this->smarty=$smarty;
	}
	
	/*
	private function getChannels($campaignId) {
		$channellist = array ();
		//for across advertiser
		if($campaignId==0){
				$sql = "select channels.name, channels.id from channels left join uniques on channels.id=uniques.user_2  and uniques.date between '$this->startDate' and '$this->endDate' and uniques.measure='d.adv.ch.vi' where uniques.user_1=$this->advertiserId group by channels.id order by channels.name";		
		}
		else{
		$sql = "select channels.name, channels.id from channels left join uniques on channels.id=uniques.user_2  and uniques.date between '$this->startDate' and '$this->endDate' and uniques.measure='d.ca.ch.vi' where uniques.user_1=$campaignId group by channels.id order by channels.name";
		}
		//echo $sql;
		$rs = $this->conn->execute ( $sql );
	//	echo $sql;
		$str="";
		if(!$rs){
			return;
		}
		while (!$rs->EOF) {
					$channellist[$rs->fields[1]]=$rs->fields[0];
					$str.="<option value=\"".$rs->fields[1]."\">".$rs->fields[0]."</option>";
					$rs->MoveNext();
				}
				return $str;
		//return $channellist;
	}
	
	*/
	public function run() {
		if ($_GET ['fetch'] == 'channellist') {
			$var = new AdvertiserReporting ();
			$channel_list = $var->getChannles ( $_GET ['campaign_id'] );
			echo $channel_list;
		}
		if ($_GET ['fetch'] == 'campaignlist') {
			$var = new PublisherReporting ();
			$campaign_list = $var->getCampaigns ( $_GET ['channel_id'] );
			echo $campaign_list;
		}
		if ($_GET ['fetch'] == 'channellist_advusr') {
			$var = new AdvertiserUserReporting ();
			$channel_list = $var->getChannles ( $_GET ['campaign_id'] );
			echo $channel_list;
		}
	}
	
}

$var=new AjaxFunction();
$var->run();


?>