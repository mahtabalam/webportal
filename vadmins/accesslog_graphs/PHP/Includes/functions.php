 <?php
require_once(dirname(__FILE__).'/../../../../include/config.php');
include_once ABSPATH."/include/config.php";
  include_once ABSPATH."/vadmins/accesslog_graphs/PHP/Includes/FusionCharts.php";
  include_once ABSPATH."/include/function.php";
if(is_admin_loggedin()) {
} else {
	doForward("$config[BASE_URL]/vadmins/?page=login&desturl=$_SERVER[REQUEST_URI]");
}
 function showYear()
{
$y = array(
'2011','2012'
);
print '<select name="Year[]">';
print '<option value="">year</option>';
foreach ($y as $p)
{
print "<option value='" . $p . "'>" . $p . "</option>";
}
print '</select>';
}

 function showMonth()
{
print '<select name="Month[]">';
print '<option value="">Month</option>';
for ($p=1;$p<13;$p++)
{
         if($p<10)
        print "<option value='" ."0". $p . "'>0" . $p . "</option>";
        else
        print "<option value='" . $p . "'>" . $p . "</option>";
}
print '</select>';
}


 function showDay()
 {
print '<select name="Day[]">';
print '<option value="">Day</option>';
for ($p=1;$p<32;$p++)
{
if($p<10)
        print "<option value='" . "0".$p . "'>0" . $p . "</option>";
        else
        print "<option value='" . $p . "'>" . $p . "</option>";
}
print '</select>';
}

 function showHour()
 {
print '<select name="Hour[]">';
print '<option value="">Hour</option>';
for ($p=0;$p<24;$p++)
{
if($p<10)
        print "<option value='" . "0".$p . "'>0" . $p . "</option>";
        else
        print "<option value='" . $p . "'>" . $p . "</option>";
}
print '</select>';
}
  function showMeasure()
{
$y = array(
"count","average","perctile95","perctile90","perctile80","perctile50","perctile20"
);
print '<select multiple name="measure[]" size=10>';
print '<option value="">Select Measure</option>';
foreach ($y as $p)
{
        print "<option value='" . $p . "'>" . $p . "</option>";
}
print '</select>';
}
  function showMachine()
{
 $m = array(
'10.120.71.217'
,'10.122.130.126'
,'10.122.209.214'
,'10.122.34.137'
,'10.122.45.102'
,'10.122.49.16'
,'10.122.49.51'
,'10.122.79.115'
,'10.220.121.155'
,'10.220.158.159'
,'10.220.198.63'
,'10.223.70.244'
,'10.223.71.34'
,'10.250.131.239'
,'10.250.46.47'
,'10.251.10.6'
,'10.251.11.191'
,'10.251.14.143'
,'10.251.15.111'
,'10.251.167.191'
,'10.251.174.47'
,'10.251.186.255'
,'10.251.241.203'
,'10.251.242.239'
,'10.251.34.111'
,'10.251.47.47'
,'10.251.93.171'
,'10.74.57.180'
,'10.74.57.212'
,'10.74.58.135'
,'10.74.59.157'
);
print '<select multiple name="machine[]" size=10>';
print '<option value="">Select Machine</option>';
foreach ($m as $p)
{
print "<option value='" . $p . "'>" . $p . "</option>";
}
print '</select>';
}  



  function showFilename()
 {
          $f = array(
'GET /adserver/adplaylist'
,'GET /adserver/adserver.php'
,'GET /adserver/html5/adFetch'
,'GET /adserver/html5/inwapads'
,'GET /adserver/html5/iphonecanvas'
,'GET /adserver/iphone.php'
,'GET /adserver/iphonetrk'
,'GET /adserver/mobclix/adrequest'
,'GET /adserver/resolver.php'
,'GET /adserver/statistics'
,'GET /adserver/tracker.php'
,'GET /adserver/trk'
,'GET /adserver/vastxml'
,'GET /adserver/vdobanner'
,'GET /cliTools/rtFeedServer.php'
,'HEAD /adserver/adplaylist'
,'HEAD /adserver/adserver.php'
,'HEAD /adserver/html5/inwapads'
,'HEAD /adserver/html5/iphonecanvas'
,'HEAD /adserver/iphone.php'
,'HEAD /adserver/iphonetrk'
,'HEAD /adserver/resolver.php'
,'HEAD /adserver/statistics'
,'HEAD /adserver/tracker.php'
,'HEAD /adserver/trk'
,'HEAD /adserver/vastxml'
,'POST /adserver/adserver.php'
,'POST /adserver/iphone.php'
,'POST /adserver/iphonetrk'
,'POST /adserver/statistics'
,'POST /adserver/tracker.php'
,'POST /adserver/trk'
);
print '<select multiple name="filename[]" size=10>';
print '<option value="">Select File</option>';
foreach ($f as $p)
{
print "<option value='" . $p . "'>" . $p . "</option>";
}
print '</select>';
} 
 
  function showGraph()
{
          $G = array(
"Pie3D","Area2D","Column3D","Column2D","Line" 
);
                      print '<select  name="Graph[]">';
print '<option value="">Select Graph</option>';
foreach ($G as $p)
{
        print "<option value='" . $p . "'>" . $p . "</option>";
}
print '</select>';
}

 
  function showMultiGraph()
{
          $G = array(
"MSLine","MSColumn3D","MSCombi3D"
);
                      print '<select  name="Graph[]">';
print '<option value="">Select Graph</option>';
foreach ($G as $p)
{
        print "<option value='" . $p . "'>" . $p . "</option>";
}
print '</select>';
}


function showGraphformachines($measure2,$filename,$machineName,$GraphType,$Date,$Date2)
                        {
     $count=count($measure2);
  
  $count=count($filename);
 
 $count=count($machineName);
  
   $totalGraphs=count($measure2)*count($filename)*count($machineName);
  /*
  if(isset($Date2)&&$Date2!="")
                echo "Date2 not null<br>"; 
                else
                 echo "Date2 null<br>";
*/
global $reportConn;
                      if (!$reportConn)
                      {
                      die('Could not connect: ' . mysql_error());
                      }               
 $index=0;
 for( $k=0;$k<count($machineName);$k++)
 {
	 for( $j=0;$j<count($filename);$j++)
	 {
		 for( $i=0;$i<count($measure2);$i++)
		 {
		  if(isset($Date2)&&$Date2!="")
		   $XMLString[$index++] ="<graph caption='Graph for ".$measure2[$i]." Hourly for File ".$filename[$j]."' subCaption='For Machine  ".$machineName[$k]." DATE ".$Date. "and ".$Date2."' xAxisName='DateandHour' yAxisName='".$measure2[$i]." Execution Time in micro second' decimalPrecision='2' formatNumberScale='0'>";
		  else
		 $XMLString[$index++] ="<graph caption='Graph for ".$measure2[$i]." Hourly for File ".$filename[$j]."' subCaption='For Machine  ".$machineName[$k]." DATE ".$Date. "' xAxisName='DateandHour' yAxisName='".$measure2[$i]." Execution Time in micro second' decimalPrecision='2' formatNumberScale='0'>";
		 }
	 }
 }
  $index=0;
 for( $k=0;$k<count($machineName);$k++)
 {
	 for( $j=0;$j<count($filename);$j++)
	 {
		 for( $i=0;$i<count($measure2);$i++)
		 {
		 if(isset($Date2)&&$Date2!="")
		 $Query[$index++]="select date,hour,".$measure2[$i]." from access_logs1 where machine_ip=\"".$machineName[$k]."\" and INSTR(filename, '".$filename[$j]."')!=0 and (date=\"".$Date."\" or date=\"".$Date2."\") order by date,hour;";
		 else
		  $Query[$index++]="select date,hour,".$measure2[$i]." from access_logs1 where machine_ip=\"".$machineName[$k]."\" and INSTR(filename, '".$filename[$j]."')!=0 and date=\"".$Date."\" order by date,hour;";
		 }
	 }
 }                    
  $index=0;
 for( $k=0;$k<count($machineName);$k++)
 {
	 for( $j=0;$j<count($filename);$j++)
	 {
		 for( $i=0;$i<count($measure2);$i++)
		 {           
        //echo  $Query[$index++]."<br>";                 
        }
	 }
 }         
 $measure="count";
  //$XMLString[0]="<graph caption='Graph for ".$measure2." Hourly for File ".$filename."' subCaption='For Machine  ".$machineName." DATE ".$Date. "' xAxisName='DateandHour' yAxisName='".$measure2." Execution Time in micro second' decimalPrecision='2' formatNumberScale='0'>";
    $index=0;
 for( $k=0;$k<count($machineName);$k++)
 {
	 for( $j=0;$j<count($filename);$j++)
	 {
		 for( $i=0;$i<count($measure2);$i++)
		 {    
                      $rs = $reportConn->execute($Query[$index]);
                     if ($rs && $rs->recordcount() > 0) 
                     {
                     $result = $rs->getrows ();


foreach ( $result as $values )
                           {
                               $XMLString[$index] .= "<set label='".$values['date']."hour".$values['hour']. "' value='". $values[$measure2[$i]] . "' />";   
                          
                           }

                           
                    }
                    $index++;
            }
        }
    }              
                  //Finally, close <graph> element				
                    for( $k=0;$k<$totalGraphs;$k++)
                        {
                        
                        $XMLString[$k].= "</graph>";
                        }    
                         for( $k=0;$k<$totalGraphs;$k++)
                        {
                         echo renderChartHTML("../../FusionCharts/".$GraphType.".swf", "", $XMLString[$k], "execution time", 1200, 500, false, false);
                         echo "<br>";
                        }
                        }  
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   


function compareallMachineHourly($measure2,$filename,$machineName,$GraphType,$Date,$hour)
                        {
     $count=count($measure2);
  
  $count=count($filename);
 
 $count=count($machineName);
  
   $totalGraphs=count($measure2)*count($filename);//*count($machineName);
global $reportConn;
                      if (!$reportConn)
                      {
                      die('Could not connect: ' . mysql_error());
                      }               
 
 
 
 $index=0;
 
 //for( $k=0;$k<count($machineName);$k++)
 //{
	 for( $j=0;$j<count($filename);$j++)
	 {
		 for( $i=0;$i<count($measure2);$i++)
		 {
		   $XMLString[$index++] ="<graph caption='Graph for ".$measure2[$i]." Hourly for File ".$filename[$j]."' subCaption='For All Machine DATE ".$Date. "and hour ".$hour."' xAxisName='DateandHour' yAxisName='".$measure2[$i]." Execution Time in micro second' decimalPrecision='2' formatNumberScale='0'>";
		  }
	 }
 //}
  $index=0;
 //for( $k=0;$k<count($machineName);$k++)
 //{
	 for( $j=0;$j<count($filename);$j++)
	 {
		 for( $i=0;$i<count($measure2);$i++)
		 {
		 
		  $Query[$index++]="select date, machine_ip,".$measure2[$i]." from access_logs1 where  INSTR(filename, '".$filename[$j]."')!=0 and date=\"".$Date."\"and hour=\"".$hour."\" order by date,hour;";
		 }
	 }
// }                    
  $index=0;
 //for( $k=0;$k<count($machineName);$k++)
// {
	 for( $j=0;$j<count($filename);$j++)
	 {
		 for( $i=0;$i<count($measure2);$i++)
		 {           
     //   echo  $Query[$index++]."<br>";                 
        }
	 }
// }         
 $measure="count";
  //$XMLString[0]="<graph caption='Graph for ".$measure2." Hourly for File ".$filename."' subCaption='For Machine  ".$machineName." DATE ".$Date. "' xAxisName='DateandHour' yAxisName='".$measure2." Execution Time in micro second' decimalPrecision='2' formatNumberScale='0'>";
    $index=0;
// for( $k=0;$k<count($machineName);$k++)
 //{
	 for( $j=0;$j<count($filename);$j++)
	 {
		 for( $i=0;$i<count($measure2);$i++)
		 {    
                      $rs = $reportConn->execute($Query[$index]);
                     if ($rs && $rs->recordcount() > 0) 
                     {
                     $result = $rs->getrows ();


foreach ( $result as $values )
                           {
                               $XMLString[$index] .= "<set label='".$values['machine_ip']. "' value='". $values[$measure2[$i]] . "' />";   
                          
                           }

                           
                    }
                    $index++;
            }
        }
   // }              
                  //Finally, close <graph> element				
                    for( $k=0;$k<$totalGraphs;$k++)
                        {
                        
                        $XMLString[$k].= "</graph>";
                        }    
                         for( $k=0;$k<$totalGraphs;$k++)
                        {
                         echo renderChartHTML("../../FusionCharts/".$GraphType.".swf", "", $XMLString[$k], "execution time", 1200, 500, false, false);
                         echo "<br>";
                        }
                      //  $XMLString[$index++] ="<graph caption='Graph for ".$measure2[0]." Hourly for File ".$filename[0]."' subCaption='For Machine  ".$machineName[$k]." DATE ".$Date."' xAxisName='DateandHour' yAxisName='".$measure2[$i]." Execution Time in micro second' decimalPrecision='2' formatNumberScale='0'>";
		 
                 /*       
                   $graphstr=  " <graph caption='Graph for ".$measure2[0]." Hourly for File ".$filename[0]."' subCaption='For all Machine  ".$machineName[$k]." DATE ".$Date."' xAxisName='DateandHour' yAxisName='".$measure2[$i]." Execution Time in micro second' decimalPrecision='2' formatNumberScale='0' decimalPrecision='0' showvalues='0' animation='1' numdivlines='3' numVdivlines='0' lineThickness='3' rotateNames='1'>
    <categories>
      <category name='1' />
      <category name='2' />
      <category name='3' />
      <category name='4' />
      <category name='5' />
      <category name='6' />
      <category name='7' />
      <category name='8' />
      <category name='9' />
      <category name='10' />
      <category name='11' />
      <category name='12' />
      <category name='13' />
      <category name='14' />
      <category name='15' />
      <category name='16' />
      <category name='17' />
      <category name='18' />
      <category name='19' />
      <category name='20' />
      <category name='21' />
      <category name='22' />
      <category name='23' />
      <category name='24' />
   </categories>
   <dataset seriesname='Current Day 2/3/05' >
      <set value='1188' />
      <set value='1189' />
      <set value='1177' />
      <set value='1175' />
      <set value='1210' />
      <set value='1280' />
      <set value='1390' />
      <set value='1524' />
      <set value='1459' />
      <set value='1423' />
      <set value='1382' />
      <set value='1356' />
      <set value='1310' />
      <set value='1282' />
      <set value='1247' />
      <set value='1182' />
      <set value='1219' />
      <set value='1272' />
      <set value='1391' />
      <set value='1381' />
      <set value='1370' />
      <set value='1359' />
      <set value='1299' />
      <set value='1216' />
   </dataset>
   <dataset seriesname='February Peak Load' >
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
   </dataset>
     <dataset seriesname='Current Day 2/3/05' >
      <set value='1188' />
      <set value='1189' />
      <set value='1177' />
      <set value='1175' />
      <set value='1210' />
      <set value='1280' />
      <set value='1390' />
      <set value='1524' />
      <set value='1459' />
      <set value='1423' />
      <set value='1382' />
      <set value='1356' />
      <set value='1310' />
      <set value='1282' />
      <set value='1247' />
      <set value='1182' />
      <set value='1219' />
      <set value='1272' />
      <set value='1391' />
      <set value='1381' />
      <set value='1370' />
      <set value='1359' />
      <set value='1299' />
      <set value='1216' />
   </dataset>
   <dataset seriesname='February Peak Load' >
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
      <set value='1550' />
   </dataset>
   <dataset seriesname='Current Day 2/3/05' >
      <set value='1188' />
      <set value='1189' />
      <set value='1177' />
      <set value='1175' />
      <set value='1210' />
      <set value='1280' />
      <set value='1390' />
      <set value='1524' />
      <set value='1459' />
      <set value='1423' />
      <set value='1382' />
      <set value='1356' />
      <set value='1310' />
      <set value='1282' />
      <set value='1247' />
      <set value='1182' />
      <set value='1219' />
      <set value='1272' />
      <set value='1391' />
      <set value='1381' />
      <set value='1370' />
      <set value='1359' />
      <set value='1299' />
      <set value='1216' />
   </dataset>
</graph> ";
$GraphType="MSLine";
$GraphType="MSColumn3D";
//$GraphType="MSCombi3D";
//$GraphType="MSColumn3D";
//$GraphType="MSColumn3DLineDY";
echo renderChartHTML("../../FusionCharts/".$GraphType.".swf", "", $graphstr, "execution time", 1200, 500, false, false);
                         echo "<br>";
                       
                       */
                       }  
            
            
            
            
            
            
            
            
function showGraphformachinesdaily($measure2,$filename,$machineName,$GraphType,$Date,$Date2)
                        {
     $count=count($measure2);
  
  $count=count($filename);
 
 $count=count($machineName);
  
   $totalGraphs=count($measure2)*count($filename);//*count($machineName);
  /*
  if(isset($Date2)&&$Date2!="")
                echo "Date2 not null<br>"; 
                else
                 echo "Date2 null<br>";
*/
global $reportConn;
                      if (!$reportConn)
                      {
                      die('Could not connect: ' . mysql_error());
                      }               
 /*
  $index=0;
 for( $k=0;$k<count($machineName);$k++)
 {
	 for( $j=0;$j<count($filename);$j++)
	 {
		 for( $i=0;$i<count($measure2);$i++)
		 {
		  if(isset($Date2)&&$Date2!="")
		   $XMLString[$index++] ="<graph caption='Graph for ".$measure2[$i]." Hourly for File ".$filename[$j]."' subCaption='For Machine  ".$machineName[$k]." DATE ".$Date. "and ".$Date2."' xAxisName='DateandHour' yAxisName='".$measure2[$i]." Execution Time in micro second' decimalPrecision='2' formatNumberScale='0'>";
		  else
		 $XMLString[$index++] ="<graph caption='Graph for ".$measure2[$i]." Hourly for File ".$filename[$j]."' subCaption='For Machine  ".$machineName[$k]." DATE ".$Date. "' xAxisName='DateandHour' yAxisName='".$measure2[$i]." Execution Time in micro second' decimalPrecision='2' formatNumberScale='0'>";
		 }
	 }
 }
 */
// /*
 $index=0;
 
 
 
 //for( $k=0;$k<count($machineName);$k++)
 //{
	 for( $j=0;$j<count($filename);$j++)
	 {
		 for( $i=0;$i<count($measure2);$i++)
		 {
		  if(isset($Date2)&&$Date2!="")
		   
		   {
		   $XMLString[$index] =" <graph caption='Graph for ".$measure2[$i]." Hourly for File ".$filename[$j]."' subCaption='For DATE ".$Date. "and ".$Date2."' xAxisName='DateandHour' yAxisName='".$measure2[$i]." Execution Time in micro second' decimalPrecision='2' formatNumberScale='0' decimalPrecision='2' showvalues='0' animation='1' numdivlines='3' numVdivlines='0' lineThickness='3' rotateNames='1'><categories>";
		
		for( $m=0;$m<24;$m++)
		 {
 $XMLString[$index] .="<category name='".$Date."_Hour_".$m."' />";

}
for( $m=0;$m<24;$m++)
		 {
 $XMLString[$index] .="<category name='".$Date2."_Hour_".$m."' />";

}

$XMLString[$index++] .=" </categories>";
}
		  else
		 {
		 $XMLString[$index] =" <graph caption='Graph for ".$measure2[$i]." Hourly for File ".$filename[$j]."' subCaption='For DATE ".$Date. "' xAxisName='DateandHour' yAxisName='".$measure2[$i]." Execution Time in micro second' decimalPrecision='2' formatNumberScale='0' decimalPrecision='2' showvalues='0' animation='1' numdivlines='3' numVdivlines='0' lineThickness='3' rotateNames='1'><categories>";
		 for( $m=0;$m<24;$m++)
		 {
 $XMLString[$index] .="<category name='".$Date."_Hour_".$m."' />";

}
$XMLString[$index++] .=" </categories>";
		 }
		 }
	 }
// }
//*/
  $index=0;
 
	 for( $j=0;$j<count($filename);$j++)
	 {
		 for( $i=0;$i<count($measure2);$i++)
		 {
		 for( $k=0;$k<count($machineName);$k++)
 {
		 if(isset($Date2)&&$Date2!="")
		 $Query[$index++]="select date,hour,".$measure2[$i]." from access_logs1 where machine_ip=\"".$machineName[$k]."\" and INSTR(filename, '".$filename[$j]."')!=0 and (date=\"".$Date."\" or date=\"".$Date2."\") order by date,hour;";
		 else
		  $Query[$index++]="select date,hour,".$measure2[$i]." from access_logs1 where machine_ip=\"".$machineName[$k]."\" and INSTR(filename, '".$filename[$j]."')!=0 and date=\"".$Date."\" order by date,hour;";
		 }
	 }
 }                    
  $index=0;
 for( $k=0;$k<count($machineName);$k++)
 {
	 for( $j=0;$j<count($filename);$j++)
	 {
		 for( $i=0;$i<count($measure2);$i++)
		 {           
     //   echo  $Query[$index++]."<br>";                 
        }
	 }
 }         
 //$measure="count";
  //$XMLString[0]="<graph caption='Graph for ".$measure2." Hourly for File ".$filename."' subCaption='For Machine  ".$machineName." DATE ".$Date. "' xAxisName='DateandHour' yAxisName='".$measure2." Execution Time in micro second' decimalPrecision='2' formatNumberScale='0'>";
    
  // /*
    $index=0;
 $index1=0;
	 for( $j=0;$j<count($filename);$j++)
	 {
		 for( $i=0;$i<count($measure2);$i++)
		 {    
		 for( $k=0;$k<count($machineName);$k++)
 {
// echo  $Query[$index]."<br>"; 
 
                      $rs = $reportConn->execute($Query[$index]);
                     if ($rs && $rs->recordcount() > 0) 
                     {
                      $XMLString[$index1] .= "<dataset seriesname='".$machineName[$k]."' >";
                     $result = $rs->getrows ();


                           foreach ( $result as $values )
                           {
                         //   echo  $values[$measure2[$i]]."<br>";
                               $XMLString[$index1] .= "<set  value='". $values[$measure2[$i]] . "' />";   
                          
                           }

                       $XMLString[$index1] .= "</dataset>";    
                    }
                    $index++;
                    
            }
            
            $index1++;
        }
    }     
    
  
//$GraphType="MSCombi3D";
//$GraphType="MSColumn3D";
//$GraphType="MSColumn3DLineDY";
// */
/* 
 $index=0;
 for( $k=0;$k<count($machineName);$k++)
 {
	 for( $j=0;$j<count($filename);$j++)
	 {
		 for( $i=0;$i<count($measure2);$i++)
		 {    
                      $rs = $reportConn->execute($Query[$index]);
                     if ($rs && $rs->recordcount() > 0) 
                     {
                     $result = $rs->getrows ();


foreach ( $result as $values )
                           {
                               $XMLString[$index] .= "<set label='".$values['date']."hour".$values['hour']. "' value='". $values[$measure2[$i]] . "' />";   
                          
                           }

                           
                    }
                    $index++;
            }
        }
    } 
*/
                  //Finally, close <graph> element				
                    for( $k=0;$k<$totalGraphs;$k++)
                        {
                        
                        $XMLString[$k].= "</graph>";
                        }    
                         for( $k=0;$k<$totalGraphs;$k++)
                        {
                         echo renderChartHTML("../../FusionCharts/".$GraphType.".swf", "", $XMLString[$k], "execution time", 1200, 500, false, false);
                         echo "<br>";
                        }
                        }  
                   
           
                   
                   
                 
                   
                   
                   
                   function showTable($Date,$machineName)
                        { 
                        
                        /*
                        $databasetable ="access_logs1";
                        $fieldseparator = ",";
                        $lineseparator = "\n";
                     $csvfile = ABSPATH."/vadmins/accesslog_graphs/PHP/Includes/2010-10-11.csv";
                     $addauto = 0;
                     $save =0;
$outputfile = "output.sql";
if(!file_exists($csvfile)) {
	echo "File not found. Make sure you specified the correct path.\n";
	exit;
}

$file = fopen($csvfile,"r");

if(!$file) {
	echo "Error opening data file.\n";
	exit;
}

$size = filesize($csvfile);

if(!$size) {
	echo "File is empty.\n";
	exit;
}

$csvcontent = fread($file,$size);
$lines = 0;
$queries = "";
$linearray = array();
fclose($file);
*/
  

/*                      
foreach(split($lineseparator,$csvcontent) as $line) {

	$lines++;

	//$line = trim($line," \t");
	//echo $line."<br>";
	
	//$line = str_replace("\r","",$line);
	
	/************************************
	This line escapes the special character. remove it if entries are already escaped in the csv file
	
	//$line = str_replace('"',"\'",$line);
	
	//echo $line."<br>";
	$linearray = explode($fieldseparator,$line);
	
	$linemysql = implode(",",$linearray);
		if($addauto)
		$query = "insert into $databasetable values('','$linemysql');";
	else
		$query = "insert into $databasetable values($linemysql);";
	//echo $query."<br>";
	 $rs = $reportConn->execute($query);
	$queries .= $query . "\n";

	
}
 */                   
                   //$linemysql=insert into values('"2011-10-11"','"00"','"67"','"10.120.71.217"','"GET /adserver/adplaylist"','"165271"','"876008"','"188580"','"56777.8"','"8741"','"3927.4"');
                   //$query = "insert into $databasetable values('$linemysql');";
                 //  echo $query."<br>";
  //    $rs = $reportConn->execute($queries);             
                      //echo $Query."<br>";
                     // echo $queries."<br>";
                  //  $Query= "SELECT *  FROM access_logs1 WHERE date=".$Date;
                     
                    //  select * from access_logs1 where machine_ip="10.122.45.102" and INSTR(filename, 'GET /adserver/html5/iphonecanvas')!=0 and date="2011-09-11" order by date,hour;
                       global $reportConn;
                      if (!$reportConn)
                      {
                      die('Could not connect: ' . mysql_error());
                      } 
                      $rst=$reportConn->execute("SHOW COLUMNS FROM access_logs1;");
                      $table="access_logs1";
                      echo "<h1>Table: {$table}</h1>";
                      echo "<table border='2'><tr>";          
                      if ($rst && $rst->recordcount() > 0) 
                     {
                     $result = $rst->getrows ();
                     foreach ( $result as $row )
                           {
                           $name.="$row[0] ";
                           }
                           $name=explode(" ",$name);
                           $count_n=count($name);
                           $a=0;
                           while ($a<$count_n-1)
                           {
                           echo "<td>".$name[$a]."</td>";$a++;
                           }
                            echo "</tr>\n";
                     }
           $machine="( ";     
                for( $k=0;$k<count($machineName)-1;$k++)
 { $machine.="machine_ip=\"".$machineName[$k]."\"  or " ;
 }
 $machine.="machine_ip=\"".$machineName[$k]."\" )";
                   $Query="select * from access_logs1 where ".$machine." and date=\"".$Date."\" order by date,hour,machine_ip,filename;";
                     echo $Query."<br>";
                     $rs = $reportConn->execute($Query);
                     if ($rs && $rs->recordcount() > 0) 
                     {
                     $result = $rs->getrows ();
                     foreach ( $result as $values )
                           {           
                            echo "<tr>";
                          //foreach($values as $cell)
                        for($i=0;$i<$rst->recordcount();$i++)
                             echo "<td>".$values[$i]."</td>";
                            echo "</tr>\n";
                           }

                     }
                     
                      echo "</table>";	
                    }
                    
                     function importTOtable()
                        {               
                        global $reportConn;
                        $fieldseparator = ",";
                        $lineseparator = "\n";
                        $csvfile = "filename.csv";
                      if (!$reportConn)
                      {
                      die('Could not connect: ' . mysql_error());
                      }   
                     $Query="select * from access_logs1 where machine_ip=\"".$machineName."\" and date=\"".$Date."\"and hour=\"00\" order by date,hour;";
                      //echo $Query."<br>";
                  //  $Query= "SELECT *  FROM access_logs1 WHERE date=".$Date;
                     echo $Query."<br>";
                    //  select * from access_logs1 where machine_ip="10.122.45.102" and INSTR(filename, 'GET /adserver/html5/iphonecanvas')!=0 and date="2011-09-11" order by date,hour;
                      $rs = $reportConn->execute($Query);
                      $rst=$reportConn->execute("SHOW COLUMNS FROM access_logs1;");
                      $table="access_logs1";
                      echo "<h1>Table: {$table}</h1>";
                      echo "<table border='2'><tr>";          
                      if ($rst && $rst->recordcount() > 0) 
                     {
                     $result = $rst->getrows ();
                     foreach ( $result as $row )
                           {
                           $name.="$row[0] ";
                           }
                           $name=explode(" ",$name);
                           $count_n=count($name);
                           $a=0;
                           while ($a<$count_n-1)
                           {
                           echo "<td>".$name[$a]."</td>";$a++;
                           }
                            echo "</tr>\n";
                     }
                     if ($rs && $rs->recordcount() > 0) 
                     {
                     $result = $rs->getrows ();
                     foreach ( $result as $values )
                           {           
                            echo "<tr>";
                          //foreach($values as $cell)
                        for($i=0;$i<$rst->recordcount();$i++)
                             echo "<td>".$values[$i]."</td>";
                            echo "</tr>\n";
                           }

                     }
                      echo "</table>";	
                    }
                        
                         ?>