<?php
require_once(dirname(__FILE__).'/../../../../include/config.php');
include_once ABSPATH."/vadmins/accesslog_graphs/PHP/Includes/functions.php";
include_once ABSPATH."/include/function.php";
if(is_admin_loggedin()) {
} else {
	doForward("$config[BASE_URL]/vadmins/?page=login&desturl=$_SERVER[REQUEST_URI]");
	
}
?>
<html>
<head>
        <title>Acesslogs</title>
        <link rel="stylesheet" type="text/css" media="all" href="
        <?php 
 echo $config[BASE_URL]."/vadmins/accesslog_graphs/PHP/DBExample/jsDatePick_ltr.min.css";
?>
 "/>


<script type="text/javascript" src="
<?php 
echo  $config[BASE_URL]."/vadmins/accesslog_graphs/PHP/DBExample/jsDatePick.min.1.3.js";
?>
"></script>

<script type="text/javascript">
	window.onload = function(){
		new JsDatePick({
			useMode:2,
			target:"inputField",
			dateFormat:"%Y-%M-%d"
			
		});
			new JsDatePick({
			useMode:2,
			target:"inputField2",
			dateFormat:"%Y-%M-%d"
			
		});
		new JsDatePick({
			useMode:2,
			target:"inputField3",
			dateFormat:"%Y-%M-%d"
			
		});
			new JsDatePick({
			useMode:2,
			target:"inputField4",
			dateFormat:"%Y-%M-%d"
			
		});
			new JsDatePick({
			useMode:2,
			target:"inputField5",
			dateFormat:"%Y-%M-%d"
			
		});
	};
</script>

</head>        
<body>
<h3>Graph for Acesss logs</h3>

Compare machines for a day
<br />
<form name="test" action="accesslog_graphs/PHP/DBExample/Acesslogs.php" method="post">

<input type="hidden" name="formid" value="3">
<table>
<tr>
<td>
 <?php
	showMachine();
	echo "
	</td>
	
	<td>"; 
   showMeasure();
   echo"    
   </td>
   
   <td>";
   showFilename();
   echo"    
   </td>
   
   <td>";

   showMultiGraph();
   echo"    
Date";
?>
   </td>
	
<td>
<input name="dateField" type="text" size="12" id="inputField4"  />
</td>
<td>
 <?php
   echo"    
Date2 optional";
?>
</td>
	<td>
<input name="dateField2" type="text" size="12" id="inputField5"  />
</td>
<td>
<input type="submit" value="submit">
</td>
</tr>
</table>
</form>

Compare all machines for a hour
<br />
<form name="test" action="accesslog_graphs/PHP/DBExample/Acesslogs.php" method="post">
<input type="hidden" name="formid" value="2">
<table>
<tr>
<td>
 <?php
   showMeasure();
   echo"    
   </td>
   
   <td>";
   showFilename();
   echo"    
   </td>
   
   <td>";

   showGraph();
   echo"    
Date";
?>
   </td>
	
<td>
<input name="dateField" type="text" size="12" id="inputField3"  />
</td>
<td>
 <?php
   echo"    
Hour ";
showHour();
?>
</td>
<td>
<input type="submit" value="submit">
</td>
</tr>
</table>
</form>


Select multiple options
<br />
<form name="test" action="accesslog_graphs/PHP/DBExample/Acesslogs.php" method="post">

<input type="hidden" name="formid" value="1">
<table>
<tr>
<td>
 <?php
	showMachine();
	echo "
	</td>
	
	<td>"; 
   showMeasure();
   echo"    
   </td>
   
   <td>";
   showFilename();
   echo"    
   </td>
   
   <td>";

   showGraph();
   echo"    
Date";
?>
   </td>
	
<td>
<input name="dateField" type="text" size="12" id="inputField"  />
</td>
<td>
 <?php
   echo"    
Date2 optional";
?>
</td>
	<td>
<input name="dateField2" type="text" size="12" id="inputField2"  />
</td>
<td>
<input type="submit" value="submit">
</td>
</tr>
</table>
</form>





    </body>
</html>
