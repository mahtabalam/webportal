<?php

include_once '../include/config.php';
include_once '../include/function.php';

if(is_admin_loggedin()){
  switch ($_REQUEST['method']) {
     case 'updatebilling':
      if(isset($_SESSION['billing_keyinfo'])) {
        $year=getRequest('year',1);
        $month=getRequest('month',1);
        $period=$year."-".$month;
        $keyinfo=&$_SESSION['billing_keyinfo'][$period];
        $key=getRequest('key',1);
        $value=getRequest('value',0);
        $model=getRequest('model',0);
        $ronum=getRequest('ronum',0);
        $approval=getRequest('approval',0);
        if ($model!='' && ($keyinfo[$key]['value']!=$value || $keyinfo[$key]['model']!=$model||$keyinfo[$key]['ronum']!=$ronum)) {
          $keyinfo[$key]['value']=$value;
          $keyinfo[$key]['model']=$model;
          $keyinfo[$key]['ronum']=$ronum;
          echo "Updated";
        } else if($approval!='' && $keyinfo[$key]['approval']!=$approval){
          $keyinfo[$key]['approval']=$approval;
          echo "Updated";
        } else {
          echo "No Change";
        }
        $keyinfo[$key]['who']=$_SESSION['ADMIN_ID'];
      }
    break;
    case 'savebilling':
      if(isset($_SESSION['billing_keyinfo'])) {
        $year=getRequest('year');
        $month=getRequest('month');
        $period=$year."-".$month;
        $keyinfo=&$_SESSION['billing_keyinfo'][$period];
       
        $jsoninfo=mysql_escape_string(json_encode($keyinfo));
        $sql="insert into billingupdate set details='$jsoninfo',month='$year-$month',site='vdopia' on duplicate key update details='$jsoninfo'";
        $rs=$conn->execute($sql);
        if($conn->Affected_Rows()>0) echo "Updated"; else echo "No Change Registered";

        session_unregister('billing_keyinfo');

      }
      break;
  }
} else {
	doForward("$config[BASEURL]/vadmins/?page=login&desturl=$_SERVER[REQUEST_URI]");
}

?>
