<?php
require_once(dirname(__FILE__).'/../../include/config.php');
include_once ABSPATH."/include/function.php";
include_once ABSPATH."/include/config.php";
if(is_admin_loggedin()) {
} else {
    doForward("$config[BASE_URL]/vadmins/?page=login&desturl=$_SERVER[REQUEST_URI]");
    
}
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
	<title>BT Dashboard</title> 
	<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.7/themes/redmond/jquery-ui.css" type="text/css" />
		<link rel="stylesheet" href="css/ui.daterangepicker.css" type="text/css" />
	<link rel="stylesheet" href="css/common.css" type="text/css" />
	<link rel="stylesheet" href="css/ui.daterangepicker.css" type="text/css" />
	<link type="text/css" href="css/ui.multiselect.css" rel="stylesheet" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/plugins/localisation/jquery.localisation-min.js"></script>
	<script type="text/javascript" src="js/plugins/scrollTo/jquery.scrollTo-min.js"></script>
	<script type="text/javascript" src="js/ui.multiselect.js"></script>
		<script type="text/javascript" src="js/date.js"></script>
<script type="text/javascript" src="js/daterangepicker.jQuery.js"></script>

		
<script type="text/javascript" src="<?php echo $config[BASE_URL]; ?>/common/utils/DataTables-1.8.1/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo $config[BASE_URL]; ?>/common/utils/DataTables-1.8.1/media/js/jquery.dataTables.js"></script>
		
	<link rel="stylesheet" href="bt_dashboard.css" type="text/css" />


		
	<script type="text/javascript">
	
		
		$(function(){

			function toggleType(){
			 	$val= $("#reportType").val();
				if($val=="camp_perf")
				{
					$(".params").hide();
					$(".camps").show();
				}
				else
				{
					$(".camps").hide();
					$(".params").show();
				}
			};
			toggleType();
				$("#daterange").daterangepicker({
				presetRanges: [
                       {text: 'Today', dateStart: 'Today', dateEnd: 'Today' },
                       {text: 'Yesterday', dateStart: 'yesterday', dateEnd: 'yesterday' },
                       {text: 'Last 7 days', dateStart: 'yesterday - 7d', dateEnd: 'yesterday' },
                   ],
                   defaultDate: Date.parse("2012-01-01"),
                   constrainDates: true, 
                   earliestDate: Date.parse("2012-01-01"),
                   latestDate: "Today",
                   dateFormat: 'dd-M-yy',
                   rangeSplitter: 'to',
		}); 
			
			$(".multiselect").multiselect(); 
			$('#uniq_stats').dataTable();
             $('#camp_perf').dataTable();
             $("#reportType").change(toggleType); 		
				

		});
	
	</script>
	

		
</head>


<body>



<?php
require dirname(__FILE__)."/bt_dashboard_dbo.php";
require dirname(__FILE__)."/../../include/config.php";
require dirname(__FILE__)."/../../include/smartyBlockFunction.php";


$bt=new BT_Dashboard_dbo();



$smarty->assign("xbfnames",$bt->getBFnameList());
$smarty->assign("xcamplist",$bt->getCampaignsList());
$smarty->assign("xkeyslist",$bt->getDisplayKeysList());
$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);


$smarty->display("bt_dashboard.tpl");
?>

<?php
if ( isset( $_REQUEST['submit'] ) )
{	//var_dump($_REQUEST);
 
 $bfeatures=$_REQUEST['bfeatures'];
 $keys=$_REQUEST['keyslist'];
 $camplist=$_REQUEST['camplist'];
 $daterange=$_REQUEST['daterange'];
 if($_REQUEST['reportType']=='uniq_stats')
 $res=$bt->getResults($bfeatures,$keys,null,$daterange);
 else
 $res=$bt->getResults($bfeatures,null,$camplist,$daterange);
// var_dump($res);
if(isset($res["uniqStats"])){
echo "<h3>BT Feature Inventory Report</h3>";
echo'

<table class="reportTable" id="uniq_stats" border="1" width="100%">
    <thead>
        <tr>
        ';
$us=$res["uniqStats"][0];
foreach($us as $key=>$val)
echo '<th>'.ucwords($key).'</th>';

	echo'
        </tr>
    </thead>
    <tbody>';
foreach($res['uniqStats'] as $val){
	echo '<tr>';
	foreach($val as $f)
	echo '<td>'.$f.'</td>';
	
	echo "</tr>";
}
echo '
    </tbody>
</table>
';
}
else if(isset($res['campStats'])){
echo "<h3>BT Feature Campaign Performance Report</h3>";
echo'

<table class="reportTable" id="camp_perf" border="1" width="100%">
    <thead>
        <tr>
        ';
$us=$res["campStats"][0];
foreach($us as $key=>$val)
echo '<th>'.ucwords($key).'</th>';

	echo'
        </tr>
    </thead>
    <tbody>';
foreach($res['campStats'] as $val){
	echo '<tr>';
	foreach($val as $f)
	echo '<td>'.$f.'</td>';
	
	echo "</tr>";
}
echo '
    </tbody>
</table>
';
}
else echo "<div><br><br>No Records Available</div>";
echo'


</body>
</html>

';
echo '
<script type="text/javascript">
			$(\'#uniq_stats\').dataTable();
                $(\'#camp_perf\').dataTable();
		</script>'
	;
	
exit;

;
}


?>







</body>
</html>
