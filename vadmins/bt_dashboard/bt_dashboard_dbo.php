<?php
ini_set('max_execution_time', 0);
require_once dirname(__FILE__)."/../../include/config.php";

// Report all PHP errors
//error_reporting(0);
date_default_timezone_set('GMT');

Class BT_Dashboard_dbo{


private $conn; 
private $db_selected;
private $map;
private $displayMap;
public function __construct()
{
global $reportConn;
 $this->conn= $reportConn;
 
 $this->map=array("active_today" =>"active.today","active_lastWeek"=>"active.last_week","active_lastMonth"=>"active.last_month","total"=>"active.total","added_today"=>"new.new_today","added_thisWeek"=>"new.new_thisWeek","removed_today"=>"new.removed_today",
 "removed_thisWeek"=>"new.removed_thisWeek");
 
  $this->displayMap=array("active_today" =>"active.today","active_lastWeek"=>"active.last_week","added_today"=>"new.new_today","added_thisWeek"=>"new.new_thisWeek","total"=>"active.total");

}

public function getBFnameList()
{
	
 $query="select name from bt_basic_features";
 $rs=$this->conn->Execute($query);
  if(!$rs) {echo "Error in query\n";exit;}
  $ret=array();
 $result=$rs->getrows();
 
  
  foreach($result as $row)
  $res[]=$row['name'];
  
  return $res;
  
}

public function getKeysList()
{
	return array_keys($this->map);
}

public function getDisplayKeysList()
{
	
	 return array_keys($this->displayMap); 
}
public function getCampaignsList()
{
	global $conn;
	//$query="select distinct campaign.name from campaign join bt_daily_stats on (campaign.id=bt_daily_stats.campaign_id)";
		$query="select distinct campaign_id as name from bt_daily_stats";

	 $rs=$this->conn->Execute($query);
  if(!$rs) {echo "Error in query\n";exit;}
  $ret=array();
  $result=$rs->getrows();
  foreach($result as $row)
  $res[]=$row['name'];
  
  
  return $res;
}

public function getResults($bfeatures,$keys,$camplist,$daterange)
{
//var_dump($daterange);
global $conn;
$date=explode(" to ",$daterange);
//var_dump($date);

$ed=$sd=date("Y-m-d",strtotime($date[0]));
if(count($date)==2)
$ed=date("Y-m-d",strtotime($date[1]));

$tb=array();
$ntb=array();
foreach($keys as $key)
{
$row=$this->map[$key];
$ntb[]=" $row as $key";
}



$bf=array();
foreach($bfeatures as $b)
$bf[]='"'.$b.'"';

	$ret=array();
	
	
if($keys != null && !empty($keys) ){
	
	$query="select active.date,active.bfname as Feature,";
	$query.=implode(",",$ntb);
	$query.=" from bt_active_uniqs_stats active join bt_new_uniqs_stats new on (active.date=new.date and active.bfname=new.bfname) ";
	$query.=' where active.bfname in ( '.implode(',',$bf).' ) ';
	$query.=" and active.date >= \"".$sd."\" and active.date<=\"".$ed."\"";
	$query.=" order by active.date";
	if(isset($_REQUEST["pq"]))
	print_r($query);
	$res=array();
	$rs=$this->conn->Execute($query);
	$result=$rs->getrows();
	foreach($result as $row)
	{
		foreach($row as $k=>$v)
		if(is_numeric($k))
		unset($row[$k]);
		
 	 	$res[]=$row;
	}
	$ret["uniqStats"]=$res;
}
if($camplist != null && !empty($camplist) )
{
	foreach($camplist as $x)
	$newcamplist[]='"'.$x.'"';
	
	// $query="select bfname as Feature, campaign.name as Campaign, sum(impressions) as Impressions,sum(clicks) as Clicks,sum(clicks)/sum(impressions) as ctr";
// 	$query.=" from bt_daily_stats join campaign on (bt_daily_stats.campaign_id=campaign.id) ";
// 	$query.=' where bfname in ( '.implode(',',$bf).' ) ';
// 	$query.=' and name in ( '.implode(',',$newcamplist).' ) ';
// 	$query.=" and date >= \"".$sd."\" and date<=\"".$ed."\"";
// 	$query.=" group by bfname,campaign.name order by bfname,ctr desc ";

$query="select bfname as Feature, campaign_id as Campaign, sum(impressions) as Impressions,sum(clicks) as Clicks,sum(clicks)/sum(impressions) as ctr";
	$query.=" from bt_daily_stats ";
	$query.=' where bfname in ( '.implode(',',$bf).' ) ';
	$query.=' and campaign_id in ( '.implode(',',$newcamplist).' ) ';
	$query.=" and date >= \"".$sd."\" and date<=\"".$ed."\"";
	$query.=" group by bfname,campaign_id order by bfname,ctr desc ";
	if(isset($_REQUEST["pq"]))
	print_r($query);
	$rs=$this->conn->Execute($query);
	$result=$rs->getrows();
	foreach($result as $row)
 	{
 		foreach($row as $k=>$v)
		if(is_numeric($k))
		unset($row[$k]);
		
		$res[]=$row;
 	}
	
	$ret["campStats"]=$res;
}
return $ret;
}


}




?>



