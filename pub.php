<?php
session_start();

include_once(dirname(__FILE__).'/include/config.php');
include_once(dirname(__FILE__).'/include/function.php');
include_once(dirname(__FILE__)."/common/include/validation.php");
require_once(dirname(__FILE__)."/include/smartyBlockFunction.php");
$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);

chk_publisher_login();

if($_REQUEST['page']==""){
	$_REQUEST['page']="dashboard";
	setup_tabs($smarty);
}
elseif($_REQUEST['page']=="formloader"){
	$_REQUEST['page']=$_REQUEST['parent'];
	setup_tabs($smarty);
	$_REQUEST['page']="formloader";
}
else 
	setup_tabs($smarty);


$smarty->assign('tabselected', $_REQUEST[ 'page' ]);

switch ($_REQUEST['page']) {
	case 'dashboard':
		if(!isset($_REQUEST['sub'])) $_REQUEST['sub']='viewdashboard';
		include_once("library/dashboard.php");
		break;

	case 'managesite':
		if(!isset($_REQUEST['sub'])) $_REQUEST['sub']='site';
		include_once("library/managesite.php");

		break;
	case 'channelbuilder':
		if(!isset($_REQUEST['sub'])) $_REQUEST['sub']='channel';
		include_once("library/channelbuilder.php");

		break;
	case 'earnings':
			if(!isset($_REQUEST['sub'])) $_REQUEST['sub']='vdopiaearnings';
		include_once("library/earnings.php");
		break;

	case 'reviewads':
		if(!isset($_REQUEST['sub'])) $_REQUEST['sub']='review';
		include_once("library/reviewads.php");
		break;
	case 'account':
		if(!isset($_REQUEST['sub'])) $_REQUEST['sub']='pubaccountpref';
		include_once("library/pubaccount.php");
		break;
	case 'report':
		include_once("library/report.php");
		break;

	case 'formloader':
		$smarty->assign("form", $_REQUEST['form'].".tpl");
		switch ($_REQUEST['form']) {
			case "changepassword":
				$error = array();
				if(isset($_REQUEST['action_changepassword'])) {
					$rules = array(); // stores the validation rules
					$rules[] = "required,oldpassword, Please enter your current password.";
					$rules[] = "required,password, Please provide a password";
					$rules[] = "length>=5,password, Your password must be at least 5 characters long";
					$rules[] = "required,confirm_password, Please provide a password";
					$rules[] = "length>=5,confirm_password, Your password must be at least 5 characters long";
					$rules[] = "same_as,confirm_password,password, Please enter the same password as above";
						
					$error = validateFields($_POST, $rules);
					
					if (empty($error)) {
						if(check_pwd("publisher", $_REQUEST['oldpassword'], $_SESSION['PUB_ID'])== null){
							$pass= md5($_REQUEST['password']);
							$sql = $conn->Prepare("update publisher set passwd = ? where id = ?");
							$rs = $conn->execute($sql, array($pass, $_SESSION[PUB_ID]));
							if($conn->Affected_Rows()>0){
								$msg= "Your password has been changed successfully";
							}else{
								$msg = "No changes made";
							}
							doForward("$config[baseurl]/pub.php?page=account&msg=$msg");
						}else{
							$error_msg = "Incorrect password";
							$smarty->assign('error_msg',$error_msg);
						}
					}
				}
				$smarty->assign("error",$error);
				break;
			case "emailexcelreport":
				$_REQUEST['page']="dashboard";
				setup_tabs($smarty);
				$_REQUEST['page']="formloader";
				if($_REQUEST['action_email']){
					$blank[]=array("");
					if($_SESSION['period']=="summary"){
					    if($_SESSION['reporttype']=="inventory")
					      $colHead[]=array("Country","Country Code","Video Views","Ads Served","Utilization","eCPM ".$_SESSION['CURRENCYSYM']);
				        elseif($_SESSION['reporttype']=="newinventory")
					      $colHead[]=array("Region", "Channel", "Ad Type", "Player Loads", "Ad Calls", "Call Drops", "Unfilled Inventory", "Ad Plays", "Utilization", "Efficiency", "eCPM ".$_SESSION['CURRENCYSYM']);	
					 $colHead[]=array(ucfirst($_SESSION['reporttype']),"Impressions","Ads Played","Clicks","CTR","EC","ECR","Revenues ".$_SESSION['CURRENCYSYM'],"eCPM ".$_SESSION['CURRENCYSYM'],'Video Completes','CR(%)');
                
                if(($_SESSION['reporttype']=="campaign" && $_SESSION['adtype']=='tracker')){
                	unset($colHead[0][getArrayKey($colHead[0],'EC')]);
                	unset($colHead[0][getArrayKey($colHead[0],'ECR')]);
                }
                if(!($_SESSION['reporttype']=="campaign")){
                	unset($colHead[0][getArrayKey($colHead[0],'EC')]);
                	unset($colHead[0][getArrayKey($colHead[0],'ECR')]);
                }
                if(!in_array('r.cpcvcolumn', $_SESSION['CAPABILITIES'])){
                	unset($colHead[0][getArrayKey($colHead[0],'Video Completes')]);
                	unset($colHead[0][getArrayKey($colHead[0],'CR(%)')]);
                }
                
                if(!($_SESSION['reporttype']=="campaign" && $_SESSION['adtype']=='tracker')){
                	unset($colHead[0][getArrayKey($colHead[0],'Ads Played')]);
                }
                foreach($colHead[0] as $key=>$val){
                	$colhead[0][]=$val;
		} 

						if($_REQUEST['reportFormat']=="excel"){
						$mailStatus=exportToExcel($_SESSION['header'],$colhead,$_SESSION['data'],$blank,$_SESSION['period'],'S');
						}
						elseif($_REQUEST['reportFormat']=="pdf"){
							$old=true;
							$content=exportToPdf($_SESSION['header'],$colhead,$_SESSION['data'],$_SESSION['period'],'S');
						}
					}
					else{
						if($_SESSION['reporttype']=="inventory")
      					  $colHead[]=array("Date","Video Views","Ads Served","Utilization","eCPM ".$_SESSION['CURRENCYSYM']);
      				    elseif($_SESSION['reporttype']=="newinventory")
      					  $colHead[]=array("Date", "Channel", "Ad Type", "Player Loads", "Ad Calls", "Call Drops", "Unfilled Inventory", "Ad Plays", "Utilization", "Efficiency", "eCPM ".$_SESSION['CURRENCYSYM']);
					 $colHead[]=array("Date","Impressions","Ads Played","Clicks","CTR","EC","ECR","Revenues ".$_SESSION['CURRENCYSYM'],"eCPM ".$_SESSION['CURRENCYSYM'],'Video Completes','CR(%)');
                
                if(($_SESSION['reporttype']=="campaign" && $_SESSION['adtype']=='tracker')){
                	unset($colHead[0][getArrayKey($colHead[0],'EC')]);
                	unset($colHead[0][getArrayKey($colHead[0],'ECR')]);
                }
                if(!($_SESSION['reporttype']=="campaign")){
                	unset($colHead[0][getArrayKey($colHead[0],'EC')]);
                	unset($colHead[0][getArrayKey($colHead[0],'ECR')]);
                }
                if(!in_array('r.cpcvcolumn', $_SESSION['CAPABILITIES'])){
                	unset($colHead[0][getArrayKey($colHead[0],'Video Completes')]);
                	unset($colHead[0][getArrayKey($colHead[0],'CR(%)')]);
                }
                
                if(!($_SESSION['reporttype']=="campaign" && $_SESSION['adtype']=='tracker')){
                	unset($colHead[0][getArrayKey($colHead[0],'Ads Played')]);
                }
                foreach($colHead[0] as $key=>$val){
                	$colhead[0][]=$val;
                }
						
		if($_REQUEST['reportFormat']=="excel"){
							$mailStatus=exportToExcel($_SESSION['header'],$colhead,$_SESSION['data'],$blank,$_SESSION['period'],'S');
						}
						elseif($_REQUEST['reportFormat']=="pdf"){
							$old=true;
							$content=exportToPdf($_SESSION['header'],$colhead,$_SESSION['data'],$_SESSION['period'],"S");
						}
					}
					
					# send email
					if(!$old&&$mailStatus)
						$smarty->assign("msg","Email has been sent.");
					else if(!$old){
							$smarty->assign("msg","May be you have given invalid email ids. For valid email ids email has been sent. Try with valid email ids....");
					}

						
					if($old&&($response=sendAttachment($_REQUEST['email'],$_REQUEST['name'],$_REQUEST['email_from'],$_REQUEST['subject'],$_REQUEST['message'],$content,$_REQUEST['reportFormat']))){
						$smarty->assign("msg","Email has been sent.");
					}
					else if($old&&!$response){
						$smarty->assign("msg","May be you have given invalid email ids. For valid email ids email has been sent. Try with valid email ids....");
					}
				}
				break;
				
			case "changepersonalinfo":
				$error = array();
				if(isset($_REQUEST['action_changepersonalinfo'])) {
			  		$rules = array(); // stores the validation rules
			  		if (isset($_SESSION['ADMIN_ID']) && $_SESSION['ADMIN_ID'] != '') {
                                            $rules[] = "required,payment_cycle,Payment Cycle is required.";
                                            $rules[] = "digits_only,payment_cycle,Please provide payment cycle in days.";
                                            $rules[] = "range>29,payment_cycle,Minimum value for payment cycle is 30 days.";
                                        }
                                        $rules[] = "required,firstname,Name is required.";
				  	$rules[] = "required,legal_name,Name is required.";
				  	$rules[] = "required,address1,Address is required.";
				  	$rules[] = "required,city,City is required.";
				  	$rules[] = "required,zip,Zip is required.";
				  	$rules[] = "required,email,Email is required.";
				  	$rules[] = "valid_email,email,Please enter valid email.";
				  	$rules[] = "required,company_name,Company Name is required.";
				  	$rules[] = "length<16,phone,Your phone number cannot be more than 15 characters long";
				  	$rules[] = "length<16,mobile,Your mobile number cannot be more than 15 characters long";
				  	$rules[] = "length<16,fax,Your fax number cannot be more than 15 characters long";
				  	
			  		$error = validateFields($_POST, $rules);
			
			  		// if there were errors, re-populate the form fields
			  		if (empty($error)){
                                                $paymentCycle=requestParams('payment_cycle');
						$firstname=requestParams('firstname');
						$mobile=requestParams('mobile');
						$email=requestParams('email');
						$legal_name=requestParams('legal_name');
						$company_name=requestParams('company_name');
						$phone=requestParams('phone');
						$address1=requestParams('address1');
						$address2=requestParams('address2');
						$city=requestParams('city');
						$zip=requestParams('zip');
						$state=requestParams('state');
						$country=requestParams('country');
				
						$chkSql = $conn->Prepare("select email from publisher where email=? and id<>? and type=?");
						$chkrs = $conn->Execute($chkSql, array($email, $_SESSION['PUB_ID'], 'online'));
						
                                                if($chkrs && $chkrs->fields['email']!=""){
							$error['email'] = "E-mail ($email) already exist. Please try with another E-mail address";
						}else{
                                                    
                                                    if (isset($_SESSION['ADMIN_ID']) && $_SESSION['ADMIN_ID'] != '') {
                                                        $sql=$conn->Prepare("update publisher set fname=?, legal_name=?, company_name=?, phone=?, email=?, mobile=?, address1=?, address2=?, city=?, zip=?, state=?, country=?, payment_cycle=? 
							where id=?");
							$rs=$conn->execute($sql, array($firstname, $legal_name, $company_name, $phone, $email, $mobile, $address1, $address2, $city, $zip, $state, $country, $paymentCycle, $_SESSION[PUB_ID]));
                                                    } else {
                                                        $sql=$conn->Prepare("update publisher set fname=?, legal_name=?, company_name=?, phone=?, email=?, mobile=?, address1=?, address2=?, city=?, zip=?, state=?, country=? 
							where id=?");
							$rs=$conn->execute($sql, array($firstname, $legal_name, $company_name, $phone, $email, $mobile, $address1, $address2, $city, $zip, $state, $country, $_SESSION[PUB_ID]));
                                                    }
                                                    
                                                    if($conn->Affected_Rows()>0) {
                                                            $msg= "Your information has been updated";
                                                    } else {
                                                            $msg= "No changes made";
                                                    }
                                                    doForward("$config[baseurl]/pub.php?page=account&msg=$msg");
						}
					}
					$smarty->assign("error", $error);
				}
				$smarty->assign("countrycodes", getCountryList($_REQUEST));
				$sql = $conn->Prepare("select * from publisher where id =?");
				$rs = $conn->execute($sql, array($_SESSION['PUB_ID']));
				if($rs && $rs->recordcount()>0){
					$personalinfo = $rs->getrows();
					$smarty->assign("personalinfo",$personalinfo);
					$_REQUEST['countrycodes'] = true;
					$_REQUEST['country'] = $personalinfo[0]['country'];
				}
				break;
		}
		break;
}
if($_REQUEST['inframe']!="true"){
	$smarty->assign('submenu',$submenu);
	$smarty->display("pubheader.tpl");
}

$smarty->display("$_REQUEST[page].tpl");
$smarty->display("pubfooter.tpl");
?>
