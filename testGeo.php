<?php
session_start();

print "Country<br>\n";
if (geoip_db_avail(GEOIP_COUNTRY_EDITION))
    print geoip_database_info(GEOIP_COUNTRY_EDITION);

print "<br>City Rev0<br>\n";
if (geoip_db_avail(GEOIP_CITY_EDITION_REV0))
    print geoip_database_info(GEOIP_CITY_EDITION_REV0);

print "<br>City Rev1<br>\n";
if (geoip_db_avail(GEOIP_CITY_EDITION_REV1))
    print geoip_database_info(GEOIP_CITY_EDITION_REV1);

print "<br>ISP<br>\n";
if (geoip_db_avail(GEOIP_ISP_EDITION))
    print geoip_database_info(GEOIP_ISP_EDITION);

print "<br>NetSpeed<br>\n";
if (geoip_db_avail(GEOIP_NETSPEED_EDITION))
    print geoip_database_info(GEOIP_NETSPEED_EDITION);

print "<br><br>Test ips <br>\n";

echo "<br>";
$ips = array(
        "75.140.96.184",
        "166.205.137.155",
        "138.217.68.8",
        "203.196.172.18",
        "59.92.223.121",
        "122.167.75.145",
        "59.92.168.160",
        "61.95.192.126",
        "61.95.206.63",
        "59.96.20.203",
        "221.134.201.4",
);

$ip = trim($_SERVER['REMOTE_ADDR']);
if($ip ){
  echo "Requesting address: $ip <br>"; 
  array_unshift($ips, $ip);
}
$ip = trim($_SERVER['HTTP_X_FORWARDED_FOR']);
if($ip ){
  echo "Forwarded-for address: $ip <br>"; 
  array_unshift($ips, $ip);
}
$ip = trim($_REQUEST['ip']);
if($ip ){
  echo "Queried address: $ip <br>"; 
  array_unshift($ips, $ip);
}

for($i=0;$i < count($ips); $i++) {
    echo " <br> IP: " . $ips[$i] ."\n";
    echo " <br> CC: ".geoip_country_code_by_name($ips[$i])."\n";
    echo " <br> Record: ".json_encode(geoip_record_by_name($ips[$i]))."\n";
    echo "<br>ISP: " . geoip_isp_by_name($ips[$i])."\n";
    $netspeed = geoip_id_by_name($ips[$i]);

    echo "<br> Connection speed: ";
    switch ($netspeed) {
        case GEOIP_DIALUP_SPEED:
            echo 'dial-up';
            break;
        case GEOIP_CABLEDSL_SPEED:
            echo 'cable or DSL';
            break;
        case GEOIP_CORPORATE_SPEED:
            echo 'corporate';
            break;
        case GEOIP_UNKNOWN_SPEED:
        default:
            echo 'unknown';
    }
    echo "<br>\n";
}


?>
