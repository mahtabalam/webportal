
<?php
include('../include/config.php');
global $conn;
// generate some random data:

$sql="select impressions,hour(hour) as hour from aggregate_geography group by hour";
$rs = $conn->execute($sql);
if($rs && $rs->recordcount()>0){
	$result = $rs->getrows();
}

for($i=0;$i<count($result);$i++)
{
	$data[$i] = $result[$i]['impressions'];
	$xlabel[$i] = $result[$i]['hour'];
}

// use the chart class to build the chart:
include_once( 'ofc-library/open-flash-chart.php' );
$g = new graph();

// Spoon sales, March 2007
$g->title( 'Impressions by hour ', '{font-size: 26px;}' );

$g->set_data( $data );
$g->line_hollow( 2, 4, '0x80a033', 'Impressions by the hour', 10 );

// label each point with its value
$g->set_x_labels( $xlabel );

// set the Y max
$g->set_y_max( 60 );
// label every 20 (0,20,40,60)
$g->y_label_steps( 6 );

// display the data
echo $g->render();
?>
