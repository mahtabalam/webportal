//elid - Element ID where you need to place the chart,
//tableid - Table to pick data from,
//col1,col2 - columns to pick data from
//graphtype
var opencharturl	='http://i2.vdopia.com/dev/pankaj_adserver/openflashchart';

var headerRow=undefined;

function getHeaderRow(tableid) {
	if(headerRow==undefined)
		headerRow=$("#" +tableid+"> thead > tr").children();
}

function getColId(tableid,colName) {
	var colId=-1, iter=0;
	getHeaderRow(tableid);
	for(var i=0;i<headerRow.length;i++) {
		if(jQuery.trim(headerRow[i].innerHTML)==jQuery.trim(colName))
			return i;
	}
	return -1;
}

function getReportCols(tableid, rlen) {
	getHeaderRow(tableid);
	var reportVals=new Array();
	for(var i=2;i<=headerRow.length-rlen;i++) {
		reportVals[i-2]=headerRow[i].innerHTML;
	}
	return reportVals;
}


function createWtAvg(tableid, aggCol, impCol, yCol, filters) {
	var retval=new Object();

	$("#" +tableid+"> tbody > tr").each(function(){
		//alert(this.innerHTML);
		var agg;
		var imp;
		var y;
		var iter=0;
		var filtersPass=true;

		$(this).children().each(function() {


			if(filters[iter]!=undefined && filters[iter]!=jQuery.trim(this.innerHTML)) {
				filtersPass=false;
			}
			if(filtersPass==false)
				return;

			if(iter==aggCol) {
				agg=jQuery.trim(this.innerHTML);
			}
			if(iter==yCol) {
				y=parseFloat(jQuery.trim(this.innerHTML));
				if(isNaN(y)) {
					y=0;
				}
			}
			if(iter==impCol) {
				imp=parseInt(jQuery.trim(this.innerHTML));
			}
			iter=iter+1;
		});
		if(filtersPass) {
			if(retval[agg]==undefined) {
				retval[agg] = new Object();
				retval[agg]['imp']=0;
				retval[agg]['y']=0;
			}
			//alert('y:'+y );
			if((imp+retval[agg]["imp"])==0) {
				retval[agg]["y"] = 0;
			} else {
				retval[agg]["y"] = ((y*imp)+(retval[agg]["imp"]*retval[agg]["y"])) / (imp+retval[agg]["imp"]);
			}
			retval[agg]["imp"] += imp;
		}


	});

	return retval;

}

function createSum(tableid, aggCol, yCol, filters) {
	var retval=new Object();

	$("#" +tableid+"> tbody > tr").each(function(){
		//alert(this.innerHTML);
		var agg;
		var y;
		var iter=0;
		var filtersPass=true;

		$(this).children().each(function() {

			//alert(filters[''+iter]);
			if(filters[iter]!=undefined && filters[iter]!=jQuery.trim(this.innerHTML)) {
				filtersPass=false;
			}
			if(filtersPass==false)
				return;

			if(iter==aggCol) {
				agg=jQuery.trim(this.innerHTML);
			}
			if(iter==yCol) {
				y=parseFloat(jQuery.trim(this.innerHTML));
			}
			iter=iter+1;
		});

		if(filtersPass) {
			if(retval[agg]==undefined) {
				retval[agg] = new Object();
				retval[agg]['y']=0;
			}

			retval[agg]["y"] += y;
		}

	});
	return retval;
}

function getUniqueInCol(tableid, colId) {
	var retval=new Array();
	$("#" +tableid+"> tbody > tr").each(function(){
		//alert(this.innerHTML);
		var name;
		var iter=0;

		$(this).children().each(function() {
			if(iter==colId) {
				agg=jQuery.trim(this.innerHTML);
			}
			iter=iter+1;
		});
		if(retval[agg]==undefined) {
			retval[agg] = 1;
		}
	});

	var arr=new Array();
	var i=0;
	for (var agg in retval) {
		arr[i++]=agg;
	}

	return arr.sort();
};

function isSum(colName) {
	if(colName.match("^Total Impressions")!=null) {
		return true;
	} else if(colName.match("^Total Amount")!=null) {
		return true;
	} else if(colName.match("^Clicks")!=null) {
		return true;
	}
}

function createChart(elid,tableid,xcol,ycol,filters,graphtype,width,height)
	{

		if(xcol!=ycol)
		{

			var xColId=getColId(tableid,xcol);
			var yColId=getColId(tableid,ycol);

			//alert("xColId:"+xColId + " yColid:"+ yColId);

			if(xColId!=0 && yColId!=0){
				var xAggregate=new Array();
				if(isSum(ycol)) {
					xAggregate=createSum(tableid, xColId, yColId, filters);

				} else {
					var impColId=getColId(tableid,'Total Impressions');
					xAggregate=createWtAvg(tableid, xColId, impColId, yColId,  filters );
				}

				var xvalues="";var yvalues="";var xlabel;var ylabel;var ymax = 0;
				//Move this to selectFilter thingy
				//$("option[@value="+(col1-1)+"]").hide();

				var num=0;
				for(var agg in xAggregate) {

					if(num==0) {
						xvalues=agg;
						yvalues=xAggregate[agg]['y'];
					} else {
						xvalues=xvalues+","+agg;
						yvalues=yvalues+","+xAggregate[agg]['y'];
					}
					num++;
					if(ymax<xAggregate[agg]['y'])
						ymax =xAggregate[agg]['y']*1.1;
				}

				xstep=Math.ceil(num/25);

				ymax=(Math.floor(ymax/10)+1)*10;
				if(yvalues!="" & xvalues!="")
				{

					ylabel = encodeURIComponent(ycol+ ',12,0x0000FF');
					xlabel = encodeURIComponent(xcol + ',12,0x0000FF');

					var so = new SWFObject(opencharturl+"/open-flash-chart.swf", "ofc", width ,height, "9", "#FFFFFF");
					so.addVariable("variables","true");
					//so.addVariable("title","Test,{font-size: 20;}");

					so.addVariable("y_legend",ylabel);
					so.addVariable("x_legend",xlabel);

					so.addVariable("bg_colour","#FFFFFF");
					//so.addVariable("y_label_size","15");

					//so.addVariable("y_ticks","5,10,4");
					//so.addVariable(graphtype);
					//so.addVariable("bar_glass", "55,#42A0DB,#3290CB,2006,10")
					so.addVariable("bar_3d","75,#42A0DB, ,10");
					so.addVariable("x_axis_3d", "12");
					so.addVariable("x_axis_colour","#909090");
					//so.addVariable("x_grid_colour","#ADB5C7");
					so.addVariable("values",yvalues);
					so.addVariable("x_labels",xvalues);
					so.addVariable("x_label_style","12,#330000,2,"+xstep);

					so.addVariable("y_max",ymax);
					so.addVariable("y_label_steps","5");
					so.addVariable("tool_tip",encodeURIComponent(xcol)+': #x_label#<br>'+encodeURIComponent(ycol)+': #val#  ');


					so.write(elid);
				}
			}
		}
	};

