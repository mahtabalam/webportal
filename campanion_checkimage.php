<?php

include_once('include/config.php');

/*Define tolerances for each image type*/
$validsizes=array(
	"brandeduploader" => array(
		"width" => array ("min"=> 958, "max"=>958),
		"height"=> array ("min"=> 80, "max"=>80)
	),
	"logouploader" => array(
		"width" => array ("min"=> 60, "max"=>60),
		"height"=> array ("min"=> 25, "max"=>25)
	),
	"banneruploader" => array(
		"width" => array ("min"=> 468, "max"=>468),
		"height"=> array ("min"=> 60, "max"=>60)
	),
	"trackeruploader" => array(
		"width" => array ("min"=> 0, "max"=>9999),
		"height"=> array ("min"=> 0, "max"=>9999)
	),
	"campanionbanneruploader" => array(
		"width" => array ("min"=> 20, "max"=>728),
		"height"=> array ("min"=> 20, "max"=>728)
	)
);




function checkFileExtension($fname) {
	global $config;

	$allowedFormats=array(
		"jpg" => 'imagecreatefromjpeg',
		"jpeg" => 'imagecreatefromjpeg',
		"gif" => 'imagecreatefromgif',
		"png" => 'imagecreatefrompng',	
		"swf" => 'imagecreatefromswf'
	);
	
	$swfGenerate = array(
		"jpg" => 'jpeg2swf',
		"jpeg" => 'jpeg2swf',
		"gif" => 'gif2swf',
		"png" => 'png2swf',
		"swf" => 'swf2swf',
		/*"wbmp" => imagecreatefromwbmp,
		"bmp" => imagecreatefromwbmp,
		"xbm" => imagecreatefromxbm,
		"xpm" => imagecreatefromxpm*/
	);

	$p=$fname;
	$pos=strrpos($p,".");
	$ph=strtolower(substr($p,$pos+1,strlen($p)-$pos));


	if(!isset($allowedFormats[$ph]))
		throw new Exception("Invalid file extension");

	return "success";
}

$filepath=escapeshellcmd($config['tempfilepath']."/".$_REQUEST['id']);
$filename=$_REQUEST['file'];

try {
if(file_exists($filepath)){
	$result = checkFileExtension($filename);
	echo $result;
}
else
	throw new Exception( "failure:$_REQUEST[id]");
}
catch (Exception $e){
	echo $e->getMessage();
}

?>