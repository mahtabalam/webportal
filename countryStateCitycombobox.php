<?php
include_once('include/config.php');
include_once('include/function.php');

$array = array();

if ($_GET['_name'] == 'country'){
	$arr = getStateList($_GET['_value']);
	if (sizeof($arr)>0){
		$array[] = array('all'=>'All');
		foreach ( $arr as $key => $value ){
			$array[] = array($key => $value);
		}
	} 
	else{
		$array[] = array('all' => 'No state');
	}
} 
else{
	$array = array();	
}

echo json_encode($array);

?>
