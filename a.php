<?php
ini_set('display_errors',on);
require_once './excelClasses/PHPExcel.php';
require_once './excelClasses/PHPExcel/Writer/Excel2007.php';

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Robin Goyal");
$objPHPExcel->getProperties()->setLastModifiedBy("Robin goyal");
$objPHPExcel->getProperties()->setTitle("testing-excel");
$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setDescription("this is test excel generated from php classes");

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1,'Robin');

$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1,'Goyal');
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2,'Pankaj');
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2,'Goel');


header('Content-Type: application/pdf');
header('Content-Disposition: attachment;filename="sample.pdf"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
$objWriter->save('php://output');

//print_r($objPHPExcel);die;




