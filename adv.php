<?php
session_start();

ini_set('memory_limit','128M');

include_once(dirname(__FILE__)."/include/config.php");
include_once(dirname(__FILE__)."/include/function.php");
include_once(dirname(__FILE__)."/common/include/validation.php");
include_once(dirname(__FILE__)."/include/sri.php");
require_once(dirname(__FILE__)."/include/smartyBlockFunction.php");

$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);

chk_advertiser_login();


if($_REQUEST['page']=="")
$_REQUEST['page']="campaign";
setup_tabs($smarty);

$smarty->assign('tabselected', $_REQUEST[ 'page' ]);


switch ($_REQUEST['page']) {
	case 'readonly':
		break;
	case 'campaign':
		if(!isset($_REQUEST['sub'])) $_REQUEST['sub']='campaignsummary';
		include_once("library/campaign.php");
		break;
	case 'credits':
		if(!isset($_REQUEST['sub'])) $_REQUEST['sub']='buycredits';
		include_once("library/credits.php");
		break;
	case 'account':
		if(!isset($_REQUEST['sub'])) $_REQUEST['sub']='accountpreferences';
		include_once("library/account.php");
		break;
	case 'clients':
		if(!isset($_REQUEST['sub'])) $_REQUEST['sub']='viewclient';
		include_once("library/clients.php");
		break;
	case 'report':
		if(!isset($_REQUEST['sub'])) $_REQUEST['sub']='advreport';
			include_once("library/report.php");
		break;
	case 'newreport':
	        if(!isset($_REQUEST['sub'])) $_REQUEST['sub']='newadvreport';
	        include_once("library/report.php");
	        break;	
	case 'formloader':
		$smarty->assign("form", $_REQUEST['form'].".tpl");
		switch ($_REQUEST['form']) {
			case "changepassword":
				$error = array();
				if(isset($_REQUEST['action_changepassword'])) {
					$rules = array(); // stores the validation rules
					$rules[] = "required,oldpassword, Please enter your current password.";
					$rules[] = "required,password, Please provide a password";
					$rules[] = "length>=5,password, Your password must be at least 5 characters long";
					$rules[] = "required,confirm_password, Please provide a password";
					$rules[] = "length>=5,confirm_password, Your password must be at least 5 characters long";
					$rules[] = "same_as,confirm_password,password, Please enter the same password as above";
					
					$error = validateFields($_POST, $rules);
					
					if (empty($error)) {
						if(check_pwd("advertiser", $_REQUEST['oldpassword'], $_SESSION['ADV_ID'])==null){
							$pass= md5($_REQUEST['password']);
							$sql = $conn->Prepare("update advertiser set passwd = ? where id = ?");
							$rs = $conn->execute($sql, array($pass, $_SESSION[ADV_ID]));
							if($rs){
								doForward("$config[baseurl]/adv.php?page=account&msg=Password+changed+successfully");
							}else{
								doForward("$config[baseurl]/adv.php?page=account&msg=Password was not changed, please try again");
							}
						}else{
							$error_msg = "Incorrect current password";
							$smarty->assign('error_msg',$error_msg);
						}
					}
				}
				$smarty->assign("error",$error);
				break;
			case "advemailexcelreport":
				setup_tabs($smarty);
				$_REQUEST['page']="formloader";
				foreach($_SESSION['data'] as $key=>$value) {
				foreach($value as $key2=>$value2)
					if($value2==''||$value2==null)
						$_SESSION['data'][$key][$key2]=0;

				}
				if($_REQUEST['action_email']){
					$blank[]=array("");
					if($_SESSION['period']=="summary"){
						$colHead =array();
						//$colHead[]=array(ucfirst($_SESSION['reporttype']),"Total Impressions","Clicks","CTR");
						if($_SESSION['reporttype']=='ad'){
							$colHead[]='Campaign';
						}else $colHead[]=ucfirst($_SESSION['reporttype']);
						//$colHead[]=ucfirst($_SESSION['reporttype']);
						foreach ($_SESSION['SelectedField'] as $key => $val){
							$colHead[] = $val;
						}
						if($_REQUEST['reportFormat']=="excel"){

								$mailStatus=adv_exportToExcel($_SESSION['header'],$colHead,$_SESSION['data'],$blank,$_SESSION['period'],$_SESSION['SelectedField'], $_SESSION['totalArr'],'S');
						}
						elseif($_REQUEST['reportFormat']=="pdf"){
						$old=true;		
						$content=adv_exportToPdf($_SESSION['header'],$colHead,$_SESSION['data'],$_SESSION['period'],$_SESSION['SelectedField'], $_SESSION['totalArr'],'S');
						}
					}
					else{
						
						$colHead =array();
						//$colHead[]=array(ucfirst($_SESSION['reporttype']),"Total Impressions","Clicks","CTR");
						$colHead[]="Date";
						foreach ($_SESSION['SelectedField'] as $key => $val){
							$colHead[] = $val;
						}
						if($_REQUEST['reportFormat']=="excel"){
							$mailStatus=adv_exportToExcel($_SESSION['header'],$colHead,$_SESSION['data'],$blank,$_SESSION['period'],$_SESSION['SelectedField'], $_SESSION['totalArr'],'S');

						}
						elseif($_REQUEST['reportFormat']=="pdf"){
							$old=true;	
							$content=adv_exportToPdf($_SESSION['header'],$colHead,$_SESSION['data'],$_SESSION['period'],$_SESSION['SelectedField'], $_SESSION['totalArr'],"S");
						}
					}
					if(!$old&&$mailStatus)
						$smarty->assign("msg","Email has been sent.");
					else if(!$old) $smarty->assign("msg","Please try after some time.");
					# send email
					if($old&&sendAttachment($_REQUEST['email'],$_REQUEST['name'],$_REQUEST['email_from'],$_REQUEST['subject'],$_REQUEST['message'],$content,$_REQUEST['reportFormat'])){
						$smarty->assign("msg","Email has been sent.");
					}
					else if($old){
						$smarty->assign("msg","Please try after some time.");
					}
}
				break;
				//=======================================================================
			case "detailtalk2mereport":	
					if($_REQUEST['excelreport']=="yes"){
						$blank[]=array("");
							$colHead[]=array("Date","Channel Name", "Name","Email","Phone","City");
							talk2me_exportToExcel($_SESSION['header'],$colHead,$_SESSION['talk2meData'],$blank);
					}
					setup_tabs($smarty);
					$_REQUEST['page']="formloader";
					
					//echo "<pre>";
					//print_r($_SESSION['header']);
					$startdate = $_SESSION['header'][2][1]; 					
					$enddate = $_SESSION['header'][3][1]; 					
					$channel_id = $_SESSION['header'][4][1]; 					
					if($channel_id!="All")
						$channel_str = " and lead_table.chid='$channel_id'"; 
					else
						$channel_str = "";
					$sql = $conn->Prepare("select DATE_FORMAT(tstamp,'%Y-%m-%d') as date, chid, adid, lead_table.type, 
					if(channels.name='','Other',channels.name) as channelName, data
					from lead_table
					LEFT JOIN channels ON channels.id = lead_table.chid
					where lead_table.cid=? and tstamp>=? and tstamp<=? $channel_str and lead_table.type='Registration '
					order by date");
					$rs=$conn->execute($sql, array($_REQUEST[cid], $startdate, $enddate));
					if($rs && $rs->recordcount()>0){
						$data=$rs->getrows();
						for($i=count($data)-1;$i>=0;$i--) {
							$userArr = explode(",",$data[$i]['data']);
							foreach ($userArr as $value){
								$infoarr = explode("=>",$value);
								if($infoarr[0]!='Registration' && trim($infoarr[0])!="")
									$data[$i][trim($infoarr[0])] = trim($infoarr[1]);
									$infoarr = array();
							}
						}
					}
					$smarty->assign('data',$data);
					$_SESSION['talk2meData'] = $data;			
//					echo "<pre>";
//					print_r($data);
				break;
				
				
				//=======================================================================
			case "changepersonalinfo":
				$error = array();
				if(isset($_REQUEST['action_changepersonalinfo'])) {
					$rules = array(); // stores the validation rules
					  
					// standard form fields
  					$rules[] = "required,firstname,Name is required.";
  					$rules[] = "required,legal_name,Name is required.";
  					$rules[] = "required,address1,Address is required.";
  					$rules[] = "required,timezone,Time Zone is required.";
  					$rules[] = "required,city,City is required.";
  					$rules[] = "required,zip,Zip is required.";
  					$rules[] = "required,email,Email is required.";
  					$rules[] = "valid_email,email,Please enter your valid email";
  					$rules[] = "required,company_name,Company Name is required.";
  					$rules[] = "length<16,phone,Your phone number cannot be more than 15 characters long";
  					$rules[] = "length<16,mobile,Your mobile number cannot be more than 15 characters long";
  					$rules[] = "length<16,fax,Your fax number cannot be more than 15 characters long";

	
					$error = validateFields($_POST, $rules);
					  
					// if there were errors, re-populate the form fields
					if (empty($error)){
						$firstname=requestParams('firstname');
						$email=requestParams('email');
						$mobile=requestParams('mobile');
						$timezone=requestParams('timezone');
						$legal_name=requestParams('legal_name');
						$company_name=requestParams('company_name');
						$phone=requestParams('phone');
						$address1=requestParams('address1');
						$address2=requestParams('address2');
						$city=requestParams('city');
						$zip=requestParams('zip');
						$state=requestParams('state');
						$country=requestParams('country');
						
						$chkSql = $conn->Prepare("select email from advertiser where email=? and id<>? and type=?");
						$chkrs = $conn->Execute($chkSql, array($email, $_SESSION['ADV_ID'], 'online'));
						if($chkrs && $chkrs->fields['email']!=""){
							$error['email'] = "E-mail ($email) already exist. Please try with another E-mail address";
						}else{
							$sql=$conn->Prepare("update advertiser set fname=?,legal_name=?,company_name=?,phone=?,address1=?,address2=?,city=?,zip=?,state=?,country=?, timezone=?, email=?, mobile=? where id=?");
							$rs=$conn->execute($sql, array($firstname, $legal_name, $company_name, $phone, $address1, $address2, $city, $zip, $state, $country, $timezone, $email, $mobile, $_SESSION[ADV_ID]));
							if($conn->Affected_Rows()>0){
								doForward("$config[baseurl]/adv.php?page=account&msg=Changes+successfully+saved");
							}else{
								doForward("$config[baseurl]/adv.php?page=account&msg=No change in personal information");
							}
						}
					}
					$smarty->assign("error",$error);
				}
				
				$sql = $conn->Prepare("select * from advertiser where id =?");
				$rs = $conn->execute($sql, array($_SESSION['ADV_ID']));
				if($rs && $rs->recordcount()>0){
					$personalinfo = $rs->getrows();
					$smarty->assign("personalinfo",$personalinfo);
					$_REQUEST['countrycodes'] = true;
					$_REQUEST['country'] = $personalinfo[0]['country'];
					$_REQUEST['timezone'] = $personalinfo[0]['timezone'];
					$smarty->assign("countrycodes",  getCountryList());
					$smarty->assign("timezone",  getTimeZoneList($_REQUEST, true));
				}
			break;
		}
		break;
}

//echo $_REQUEST[sub];

if(!isset($_REQUEST['inframe'])||$_REQUEST['inframe']!="true")
{
	$submenu = $_REQUEST['sub'];	
	$smarty->assign('submenu',$submenu);
	$smarty->display("advheader.tpl");
}
$smarty->display("$_REQUEST[page].tpl");
$smarty->display("advfooter.tpl");
?>
