<?php
include_once "../include/config.php";
if(!isset($_REQUEST["id"])){
	header("Status: ID Not provided");
	die('Error 404');
}
$key=$_REQUEST["id"];

//echo var_dump($_SERVER); exit(0);
//echo $_SERVER['SERVER_PROTOCOL'].' : '.$_SERVER['HTTP_HOST']; exit(0);

$file_map=array(
		"swc_component" => "VDOPIA_v4_setup_guide.zip",
		"frame_buster" => "frameBuster"
);



$local_file_name = $file_map[$key];
$file_path = $config['CDN_URL']."/resources/".$local_file_name;
//echo $file_path; exit(0);
if(isset($local_file_name)){ // if file name exits in array
	header("Content-Type: application/epub+zip");
	header("Pragma: public");
	header("Cache-Control: public");
	header("Content-Description: File Transfer");
	header("Content-Transfer-Encoding: binary");
	header("Content-disposition: attachment; filename=".$local_file_name);
	header("Location: $file_path");
} else {
	header("Status: wrong ID provided");
	die("File not found");
}

?>