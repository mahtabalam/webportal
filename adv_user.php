<?php
session_start();

include_once(dirname(__FILE__).'/include/config.php');
include_once(dirname(__FILE__).'/include/function.php');
include_once(dirname(__FILE__)."/common/include/validation.php");
require_once(dirname(__FILE__).'/include/smartyBlockFunction.php');

$smarty->register_block("fillInFormValues", "smartyFillInFormValues", false);
chk_adv_user_login();
if($_REQUEST['page']=="") $_REQUEST['page']="advuser_newreport";

setup_tabs($smarty);

$smarty->assign('tabselected', $_REQUEST[ 'page' ]);


switch ($_REQUEST['page']) {
	case 'account':
		include_once("library/advuseraccount.php");
		break;
	/*case 'report':
		if(!isset($_REQUEST['sub'])) $_REQUEST['sub']='advusercreatereport';
		include_once("library/advuserreport.php");
		break;*/
	case 'advuser_newreport':
		if($_REQUEST['sub']=="") $_REQUEST['sub']='advusernewreport';
		include_once("library/advuser_newreport.php");
		break;
	
	case 'formloader':
		$smarty->assign("form", $_REQUEST['form'].".tpl");
		switch ($_REQUEST['form']) {
			case "changepassword":
				if(isset($_REQUEST['action_changepassword'])) {
			/*		$rules = array(); // stores the validation rules
					$rules[] = "required,type,Error.";
					$rules[] = "if:type=overlayadvertisement,required,subtype, Error";
					$rules[] = "if:type=videoadvertisement,required,vidtype, Error";


					$error = validateFields($_POST, $rules);
					*/
					if (empty($error)) {

						if(check_pwd("adv_user", $_REQUEST['oldpassword'], $_SESSION['ADV_USER_ID'])==null){
							$pass= md5($_REQUEST['password']);
							$sql = "update adv_user set passwd = '$pass' where id = '$_SESSION[ADV_USER_ID]'";
							$rs = $conn->execute($sql);
							$rows = $conn->Affected_Rows();;
							if($rows>0){
								doForward("$config[baseurl]/adv_user.php?page=account&msg=Password+changed+successfully");
							}else{
								doForward("$config[baseurl]/adv_user.php?page=account&msg=Password was not changed, please try again");
							}
						}else{
							$error = "Incorrect current password";
						}

					}
				}

				break;
				case "advuser_emailexcelreport":
				setup_tabs($smarty);
				$_REQUEST['page']="formloader";
				if($_REQUEST['action_email']){
					$blank[]=array("");
					if($_SESSION['period']=="summary"){
						$colHead =array();
						//$colHead[]=array(ucfirst($_SESSION['reporttype']),"Total Impressions","Clicks","CTR");
						$colHead[]=ucfirst($_SESSION['reporttype']);
						foreach ($_SESSION['SelectedField'] as $key => $val){
							$colHead[] = $val;
						}
						if($_REQUEST['reportFormat']=="excel"){
							$content=adv_exportToExcel($_SESSION['header'],$colHead,$_SESSION['data'],$blank,$_SESSION['period'],$_SESSION['SelectedField'], $_SESSION['totalArr'],'S');
						}
						elseif($_REQUEST['reportFormat']=="pdf"){
							$content=adv_exportToPdf($_SESSION['header'],$colHead,$_SESSION['data'],$_SESSION['period'],$_SESSION['SelectedField'], $_SESSION['totalArr'],'S');
						}
					}
					else{
						$colHead =array();
							//$colHead[]=array(ucfirst($_SESSION['reporttype']),"Total Impressions","Clicks","CTR");
							$colHead[]="Date";
							foreach ($_SESSION['SelectedField'] as $key => $val){
								$colHead[] = $val;
							}
						if($_REQUEST['reportFormat']=="excel"){
							$content=adv_exportToExcel($_SESSION['header'],$colHead,$_SESSION['data'],$blank,$_SESSION['period'],$_SESSION['SelectedField'], $_SESSION['totalArr'],'S');
						}
						elseif($_REQUEST['reportFormat']=="pdf"){
							$content=adv_exportToPdf($_SESSION['header'],$colHead,$_SESSION['data'],$_SESSION['period'],$_SESSION['SelectedField'], $_SESSION['totalArr'],"S");
						}
					}
					
					# send email
					if(sendAttachment($_REQUEST['email'],$_REQUEST['name'],$_REQUEST['email_from'],$_REQUEST['subject'],$_REQUEST['message'],$content,$_REQUEST['reportFormat'])){
						$smarty->assign("msg","Email has been sent.");
					}
					else{
						$smarty->assign("msg","Please try after some time.");
					}
				}
				break;
				case "changepersonalinfo":
					$sql = "select * from advertiser where id = '$_SESSION[ADV_ID]'";
							$rs = $conn->execute($sql);
							if($rs && $rs->recordcount()>0){
								$personalinfo = $rs->getrows();
								$smarty->assign("personalinfo",$personalinfo);
								$_REQUEST['countrycodes'] = true;
								$_REQUEST['country'] = $personalinfo[0]['country'];
								$smarty->assign("countrycodes",  getCountryCodes($_REQUEST, true));
							}

				if(isset($_REQUEST['action_changepersonalinfo'])) {

				  $rules = array(); // stores the validation rules

				  // standard form fields
				  $rules[] = "required,firstname,First Name is required.";
				  $rules[] = "required,lastname,Last Name is required.";
				  $rules[] = "required,address1,Address line 1 is required.";
				  $rules[] = "required,city,City is required.";
				  $rules[] = "required,state,State is required.";

				  $rules[] = "digits_only,phone,Please enter only digits.";
				  $rules[] = "length<16,phone,Your phone number cannot be more than 15 characters long";
				  $rules[] = "digits_only,mobile,Please enter only digits.";
				  $rules[] = "length<16,mobile,Your mobile number cannot be more than 15 characters long";
				  $rules[] = "digits_only,fax,Please enter only digits.";
				  $rules[] = "length<16,fax,Your fax number cannot be more than 15 characters long";
				  $rules[] = "digits_only,zip,Please enter only digits.";

				  $error = validateFields($_POST, $rules);
				  $smarty->assign("error",$error);
				  $_REQUEST['formErrors']=$error;
				  // if there were errors, re-populate the form fields
				  if (empty($error)){
					$firstname=requestParams('firstname');
					$lastname=requestParams('lastname');
					$address1=requestParams('address1');
					$address2=requestParams('address2');
					$zip=requestParams('zip');
					$city=requestParams('city');
					$state=requestParams('state');
					$company_name=requestParams('company_name');
					$mobile=requestParams('mobile');
					$phone=requestParams('phone');
					$fax=requestParams('fax');
					$country=requestParams('country');

					$sql="update advertiser set fname='$firstname',	lname='$lastname',company_name='$company_name',address1='$address1',address2='$address2',city='$city',state='$state',country='$country',zip='$zip',	mobile='$mobile',phone='$phone',fax='$fax' where id='$_SESSION[ADV_ID]'";
					$rs=$conn->execute($sql);
					$rows=$conn->Affected_Rows();;
					if($rows>0){
						doForward("$config[baseurl]/adv.php?page=account&msg=Changes+successfully+saved");

					}else{
						doForward("$config[baseurl]/adv.php?page=account&msg=No change in personal information");

					}

					}
				}
				break;
		}
		$smarty->assign("error", $error);
		break;
}

if($_REQUEST['inframe']!="true")
{
	$smarty->assign('submenu',$submenu);
	$smarty->display("advuserheader.tpl");
}
$smarty->display("$_REQUEST[page].tpl");
if($_REQUEST['inframe']!="true"){
if(isset($_SESSION['ADV_ID']) || isset($_SESSION['PUB_ID']) || isset($_SESSION['ADV_USER_ID']))
	$smarty->display("footer.tpl");
else
	$smarty->display("footer-external.tpl");
}
?>
