<?php
include('include/config.php');
include('include/function.php');

session_start();

$campaign_ids=requestParams('campaign_ids', true);


if(!is_admin_loggedin()) {
	$sql="select group_concat(cast(id as char)) as campaign_ids from campaign where id in ($campaign_ids) and advertiser_id=$_SESSION[ADV_ID]";

	$rs1=$conn->execute($sql);

	if($rs1 && $rs1->recordcount() > 0) {
		$campaign_ids=$rs1->fields['campaign_ids'];
	} else
		$campagn_ids="";
}

if($campaign_ids!="") {
	$sql="select  distinct(ip) as ip from processed_stats where campaign_id in ($campaign_ids)";

	$rs=$conn->execute($sql);
	header("Content-Type: text/csv");
	header('Content-Disposition: attachment;filename="list.csv"');
	while($rs && !$rs->EOF) {
		echo long2ip($rs->fields['ip'])."\n";
		$rs->movenext();
	}
}

?>
