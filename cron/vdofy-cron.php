<?php
  if(!isset($argv)) exit(1);
  include_once(dirname(__FILE__).'/../include/config.php');
  require_once dirname ( __FILE__ ) . "/../include/singleton.php";
  require_once dirname ( __FILE__ ) . "/../common/adserving/redis/RedisEncoding.php";
  include_once dirname ( __FILE__ ) . "/../common/portal/tools/encoding.php";
  include_once (dirname(__FILE__).'/../include/serviceconfig.php');

  if(VdoSingle::isSingletonRunning(VdoSingle::VDO_SINGLE_VDOFYCRON)){
    echo( "A script is already running. Exiting ... \n");
    exit(0);
  }
  
  $encoding_com_id=SConfig::get("Accounts", "encoding_com_id");
  $encoding_com_key=SConfig::get("Accounts", "encoding_com_key");
  $pathPref = $config["BASE_DIR"]."/";
  
  /*VDO FLO conversion which went wrong*/
  $sql="select id, playlist_ad_ref, playlist_url, status from ad_network_ads join ads on id=ad_id where ads.status>=0 and ads.status<=3";
	$rencode = new RedisEncoding();
  $rs = $conn->execute($sql);
  $files = $rs->getrows();
  foreach ( $files as $index => $element) {
    $playlist_url=$element['playlist_url'];
    $adref=md5_file($playlist_url);
    if(!$adref){
      continue;
    }
    if(!$rencode->get($element['id']))
    {
	    Encoding::encode($playlist_url, $encoding_com_id, $encoding_com_key, $element['id'], $adref, $element['status'], getAllFormats());
	    $sql="update ads set status=".($element['status']+10)." where id=".$element['id'];
	    $conn->execute($sql);
	    sleep(2);
	}
  }

  /*END of VDOFLO Conversions*/
  
  
  function getAllFormats($format = NULL)
  {
  	unset($formats);
  	if(!isset($format))
  		$formats = array('mp4', '3gp', 'ogg', 'webm');
  	else
  		$formats = $format;
  	return $formats;
  }
  

  $sql= "select distinct ads.playlist_ad_ref, ads.id, ads.status from campaign,campaign_members,ads where campaign.id=cid and ads.id=ad_id and campaign.status='active' and campaign_members.status='enabled' and device='iphone' and (ad_format='video' or ad_format='vdobanner' or ad_format='leadervdo' or ad_format='vastfeed' or ad_format='preroll' or ad_format='mvtpreroll') and (ads.status=-1 or ads.status=0)";
  $rs = $conn->execute($sql);
  $files = $rs->getrows();

  foreach ( $files as $index => $element) {
    $fname = $pathPref."files/videos/".$element['playlist_ad_ref'].'_a.mp4';
    $dname = $pathPref."files/videos/".$element['playlist_ad_ref'].'.VDO';
    if( file_exists($fname) && filesize($fname) < 50000){
      continue;
    }
  	if( file_exists($fname) && (!file_exists($dname))){
      $cmd= "php ".$pathPref."common/portal/tools/vdofy.php $fname $dname 9";
      echo $cmd."\n"; 
      system($cmd);
    }
    
    # take care of campaigns that has been created through REST API using screenshots and audios
    $mp4fname = $pathPref."files/videos/".$element['playlist_ad_ref'].'.mp4';
    if( file_exists($mp4fname) && !file_exists($fname) && !$rencode->get($element['id']) ){
      Encoding::encode("$config[advdourl]/$element[playlist_ad_ref].mp4", $encoding_com_id, $encoding_com_key, $element['id'], $element['playlist_ad_ref'], $element['status'], getAllFormats());
      sleep(2);
      continue;
    }
    
    $avifname = $pathPref."files/videos/".$element['playlist_ad_ref'].'_w.avi';
    if( file_exists($fname) && (!file_exists($avifname))){
      $cmd= "mencoder -vf scale=240:180 $fname -o $avifname -of avi  -ovc lavc -oac mp3lame -lavcopts vcodec=mpeg4:vbitrate=230:acodec=mp3:abitrate=64";
      echo $cmd."\n"; 
      system($cmd);
    }
    $gp3fname = $pathPref."files/videos/".$element['playlist_ad_ref'].'_3.3gp';
    if( file_exists($fname) && (!file_exists($gp3fname))){
      $cmd= "/usr/bin/ffmpeg -y -i $fname -vcodec h263 -s qcif -r 15 -b 256k -acodec libfaac -ac 1 -ar 8000 -ab 12.2k -f 3gp $gp3fname";
      echo $cmd."\n"; 
      system($cmd);
    }
    $lqfname = $pathPref."files/videos/".$element['playlist_ad_ref'].'_lq.mp4';
    if( file_exists($fname) && (!file_exists($lqfname))){
      $cmd = "mencoder $fname  -o $lqfname -oac faac -faacopts mpeg=4:object=2:raw:br=32  -channels 1 -srate 11025 -vf scale=240:-10,harddup -of lavf -lavfopts format=mp4 -ovc x264 -x264encopts nocabac:level_idc=30:bframes=0:global_header:threads=auto:subq=5:frameref=6:partitions=all:trellis=1:chroma_me:me=umh:bitrate=128";
      echo $cmd."\n";
      system($cmd);
    }
    // flv version
    $flvfname = $pathPref."files/videos/".$element['playlist_ad_ref'].".flv";
    if(file_exists($fname) && (!file_exists($flvfname))){
    	$cmd = "mencoder $fname -o $flvfname -ss 0 -endpos 15 -of lavf -oac mp3lame -lameopts abr:br=40:mode=3 -ovc lavc -lavcopts vcodec=flv:vbitrate=300:mbd=2:mv0:trell:v4mv:cbp:last_pred=3 -ofps 12 -srate 22050 -vf scale=320:240";
    	echo $cmd."\n";
    	system($cmd);
    }
    
    unset($formats);
    //3GP converted from encoding.com
    $enc3gp= $pathPref."files/videos/".$element['playlist_ad_ref'].'_3gp';
    $enc3gpname= $pathPref."files/videos/".$element['playlist_ad_ref'].'_lq.3gp';
    if( file_exists($enc3gp) && (time() - filemtime($enc3gp)) > 3600){
      $cmd = "rm -f $enc3gp";
      echo $cmd."\n";
      system($cmd);
    }
    else if(file_exists($fname) && (!file_exists($enc3gpname)) && !$rencode->get($element['id']) ){
    $formats[] = getAllFormats('3gp');
    }
    
  	# ogg video format from encoding.com
  	$oggfname = $pathPref."files/videos/".$element['playlist_ad_ref'].'.ogg';
    if( file_exists($fname) && (!file_exists($oggfname))  && !$rencode->get($element['id']) ){
    	$formats[] = getAllFormats('ogg');
    }
    
    
    
    # webm video format from encoding.com
  	$webmfname = $pathPref."files/videos/".$element['playlist_ad_ref'].'.webm';
    if( file_exists($fname) && (!file_exists($webmfname))  && !$rencode->get($element['id']) ){
    	$formats[] = getAllFormats('webm');
  	}
  	
  	if(isset($formats) && $formats && count($formats)>0 )
  	{
  		sleep(2);
      	Encoding::encode("$config[advdourl]/$element[playlist_ad_ref]_a.mp4", $encoding_com_id, $encoding_com_key, $element['id'], $element['playlist_ad_ref'], $element['status'], $formats );
  	}
  	
  }
?>
