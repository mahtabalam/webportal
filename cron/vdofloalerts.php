<?php 
require_once dirname ( __FILE__ ) . '/../include/config.php';
require_once dirname ( __FILE__ ) . '/../adserver/vdoflo/SQS_Queue.php';

class VdofloAlerts{
	private $sqsObj;
	private $emails;
	private $config;
	private $conn;
	private $reportConn;
	private $vdoflo_alert;
	private $vdoflo_alert_dir;
	private $db_prefix;
	public function __construct(){
		global $config, $conn, $reportConn;
		$this->sqsObj = new SQS_Queue();
		$this->sqsObj->connect ();
		$this->tech_ops_emails='abhay@vdopia.com,shashwat@vdopia.com,prashant@vdopia.com,pankaj@vdopia.com';
		$this->ops_emails='abhay@vdopia.com,pankaj@vdopia.com,melissa@vdopia.com,prashant@vdopia.com'; //,pankaj@vdopia.com,melissa@vdopia.com
		$this->config = $config;
		$this->conn = $conn;
		$this->reportConn = $reportConn;
		$this->vdoflo_alert['vdoflostuck']='vdoflo_alerts_'.date('Y_m_d_h').".txt";
		$this->vdoflo_alert['vdfloinventorystatus']='vdoflo_inventory_alerts_'.date('Y_m_d').".txt";
		$this->vdoflo_alert['vdofloqueuestatus']='vdoflo_sqs_alerts_'.date('Y_m_d_h').".txt";
		$this->vdoflo_alert['vdoflo_xc_ui']='vdoflo_xc_ui_'.date('Y_m_d_h').".txt";
		$this->vdoflo_alert_dir = '/mnt/vdoflo_alerts';
		if(isset($config['DB_PREFIX']) && $config['DB_PREFIX']!='') {
			$this->db_prefix=$config['DB_PREFIX'];
		}else $this->db_prefix='';
		
	}
	
	private function sendMail($subj, $body, $emails){
		mail($this->$emails, $subj, $body, $this->config['support_headers']);
	}
	
	private function vdofloFeedAlert(){
		$hour=date('h');
  		$suffix=$hour%2;
  		$table="ad_log$suffix";
  		// aggregate_campaign: select ac.campaign_id, c.name as name, an.additional_settings as feed, sum(if(ac.measure='n_ai',ac.val,0)) 'n_ai', sum(if(ac.measure in ('n_ui', 'vf_xc'),ac.val,0)) 'unfilled', round((sum(if(ac.measure in('n_ui','vf_xc'), ac.val,0))/sum(if(ac.measure='n_ai', ac.val, 0)))*100,2) as 'fill_rate' from aggregate_campaign ac left join campaign c on c.id=ac.campaign_id left join ad_network an on an.campaign_id=ac.campaign_id where ac.date=date_sub(date(now()), interval 1 day) group by ac.campaign_id having sum(if(ac.measure='n_ai',ac.val,0)) > 0
  		// ad_log: select ac.campaign_id, c.name as name, an.additional_settings as feed, sum(if(ac.measure='n_ai',ac.value,0)) 'n_ai', sum(if(ac.measure in ('n_ui', 'vf_xc'),ac.value,0)) 'unfilled', round((sum(if(ac.measure in('n_ui','vf_xc'), ac.value,0))/sum(if(ac.measure='n_ai', ac.value, 0)))*100,2) as 'fill_rate' from $table ac left join campaign c on c.id=ac.campaign_id left join ad_network an on an.campaign_id=ac.campaign_id where ac.tstamp>(now()-3600) group by ac.campaign_id having sum(if(ac.measure='n_ai',ac.value,0)) > 0";
  		$sql = $this->conn->Prepare("select ac.campaign_id, c.name as name, an.additional_settings as feed, sum(if(ac.measure='n_ai',ac.value,0)) 'n_ai', sum(if(ac.measure in ('n_ui', 'vf_xc'),ac.value,0)) 'unfilled', round((sum(if(ac.measure in('n_ui','vf_xc'), ac.value,0))/sum(if(ac.measure='n_ai', ac.value, 0)))*100,2) as 'fill_rate' from $table ac left join campaign c on c.id=ac.campaign_id left join ad_network an on an.campaign_id=ac.campaign_id where ac.tstamp>(now()-3600) group by ac.campaign_id having sum(if(ac.measure='n_ai',ac.value,0)) > 0");
		$rs = $this->reportConn->Execute($sql, array());
		if($rs&&$rs->recordcount()>0){
			$rows = $rs->getrows();
			foreach($rows as $key => $row){
				$fill_rate = (int)$row["fill_rate"];
				if($fill_rate>20){
					$feed = (array)json_decode($row['feed']);
					$data["$key"]["cid"]=$row["campaign_id"];
					$data["$key"]["cname"]=$row["name"];
					$data["$key"]["n_ai"]=$row["n_ai"];
					$data["$key"]["unfilled"]=$row["unfilled"];
					$data["$key"]["fill_rate"]=$row["fill_rate"];
					$data["$key"]["vastfeedurl"]=$feed["adfeed"];
				}
			}
			if(sizeof($data)>0){
				$body = "Hi All,<br /><br />";
				$body .= $this->fillRateAlertBody($data);
				$body .= "<br /><br /> Thank you <br />Vdopia Support Team";
				if(!file_exists($this->vdoflo_alert_dir."/".$this->vdoflo_alert['vdoflo_xc_ui'])){
					$this->sendMail('Alert::These vdoflo campaigns/tags are not performing well.....', $body, "ops_emails");
					touch($this->vdoflo_alert_dir."/".$this->vdoflo_alert['vdoflo_xc_ui']);
				}
			}
		}
	}
	
	private function vdofloStuck(){
		$hour=date('h');
  		$suffix=$hour%2;
  		$table="ad_log$suffix";
		$sql = $this->conn->Prepare("select sum(if(measure='vf_var', value, 0)) as 'vf_var', sum(if(measure='vf_sqse', value, 0)) as 'vf_sqse' from $table where tstamp>(now()-3600) group by hour(tstamp) order by tstamp desc");
		$rs = $this->conn->Execute($sql, array());
		if($rs&&$rs->recordcount()>0){
			$conversion_already_running = $rs->fields['vf_var'];
			$sqs_empty = $rs->fields['vf_sqse'];
			if($conversion_already_running>=7){
				$body = "Hi All, <br /><br />vdoflo_queue_management has been stuck. Thats why ad conversion has been stopped. Please check ASAP.";
				$body .= "<br /><br /> Thank you <br />Vdopia Support Team";
				if(!file_exists($this->vdoflo_alert_dir."/".$this->vdoflo_alert['vdoflostuck'])){
					$this->sendMail('Urgent: Vdoflo queue management stopped', $body, "tech_ops_emails");
					touch($this->vdoflo_alert_dir."/".$this->vdoflo_alert['vdoflostuck']);
				}
			}
		}
	}
	
	private function vdofloInventoryStatus(){
		$servingSql = $this->reportConn->Prepare("select {$this->db_prefix}aggregate_channels_details.channel_id, name, sum(if(measure='n_ai',val,0)) 'n_ai', sum(if(measure='vi',val,0)) 'vi', sum(if(measure in ('n_ui', 'vf_xc'),val,0)) 'n_ui',  sum(if(measure='vf_qp',val,0)) 'vf_qp', round((sum(if(measure in('n_ui','vf_xc'), val,0))/sum(if(measure='n_ai', val, 0)))*100,2) as 'unfilled_ratio', round((sum(if(measure='n_ai',val,0)) - (sum(if(measure in ('n_ui', 'vf_xc'),val,0))))/sum(if(measure='n_ai',val,0))*100,2) as 'utilization_ratio', round((sum(if(measure='vi',val,0))/(sum(if(measure='n_ai',val,0)) - (sum(if(measure='n_ui',val,0)) + sum(if(measure='vf_xc',val,0)))))*100,2) as 'efficiency_ratio', if(channel_settings.additional_settings regexp 'network_list', 1, 0) as vdoflo from {$this->db_prefix}aggregate_channels_details join channels on channels.id={$this->db_prefix}aggregate_channels_details.channel_id join channel_settings on channel_settings.channel_id={$this->db_prefix}aggregate_channels_details.channel_id where date>=date(date_sub(now(), interval 1 day)) group by {$this->db_prefix}aggregate_channels_details.channel_id having sum(if(measure='n_ai',val,0))>0");
		$rss = $this->reportConn->Execute($servingSql, array());
		if($rss&&$rss->recordcount()>0) $serving = $rss->getrows();
		if(sizeof($serving)>0){
			$body = "Hi All,<br /><br />";
			$body .= $this->InventoryReportBody($serving);
			$body .= "<br /><br /> Thank you <br />Vdopia Support Team";
			if(!file_exists($this->vdoflo_alert_dir."/".$this->vdoflo_alert['vdfloinventorystatus'])){
				$this->sendMail('Vdoflo daily inventory status report', $body, "tech_ops_emails");
				touch($this->vdoflo_alert_dir."/".$this->vdoflo_alert['vdfloinventorystatus']);
			}
		}
	}
	
	private function InventoryReportBody($serving){
		$body='<div>
				<div style="color: red;font-style: italic;width: 100%;padding: 5px;background: #FFFFD9;text-align:center;border: 1px solid gray;margin-bottom: 10px;"><strong>Note:</strong> Channel wise inventory and unfilled inventory ratio. Red labeled rows showing unfilled inventory are greater than 30%.</div>';
		if(sizeof($serving)>0){
			$body.='<h4>Vdoflo Specific Channels</h4>
						<table id="vdofloStatusReportingtable" border="0" cellpadding="0" cellspacing="1" >
							<thead>
								<tr>
									<th>Channel ID</th>
									<th>Channel Name</th>
									<th>Available Inventory</th>
									<th>Unfilled Inventory</th>
									<th>Impressions</th>
									<th>Unfilled Inventory %</th>
									<th>Utilization %</th>
									<th>Efficiency %</th>
								</tr>
							</thead>
							<tbody>';
			foreach($serving as $row){
				if($row['vdoflo']==1){
					$unfilled_ratio = round((int)$row['unfilled_ratio'], 0);
					if( $unfilled_ratio > 30) $style='style="background-color:red"'; else $style='';
					$body.='<tr>
						<td '.$style.'>'.$row['channel_id'].'</td>	
						<td '.$style.'>'.$row['name'].'</td>
						<td '.$style.'>'.$row['n_ai'].'</td>
						<td '.$style.'>'.$row['n_ui'].'</td>
						<td '.$style.'>'.$row['vi'].'</td>
						<td '.$style.'>'.$row['unfilled_ratio'].'</td>
						<td '.$style.'>'.$row['utilization_ratio'].'</td>
						<td '.$style.'>'.$row['efficiency_ratio'].'</td>
					</tr>';
				}
			}
			$body.='</tbody>
				</table>
				<h4>Other Channels</h4>
				<table id="vdofloStatusReportingtable" border="0" cellpadding="0" cellspacing="1" >
					<thead>
						<tr>
							<th>Channel ID</th>
							<th>Channel Name</th>
							<th>Available Inventory</th>
							<th>Unfilled Inventory</th>
							<th>Impressions</th>
							<th>Unfilled Inventory %</th>
							<th>Utilization %</th>
							<th>Efficiency %</th>
						</tr>
					</thead>
					<tbody>';
			foreach($serving as $row){
				if($row['vdoflo']==0){
					if( $row['unfilled_ratio'] > 30) $style='style="background-color:red"';
					$body.='<tr>
						<td '.$style.'>'.$row['channel_id'].'</td>	
						<td '.$style.'>'.$row['name'].'</td>
						<td '.$style.'>'.$row['n_ai'].'</td>
						<td '.$style.'>'.$row['n_ui'].'</td>
						<td '.$style.'>'.$row['vi'].'</td>
						<td '.$style.'>'.$row['unfilled_ratio'].'</td>
						<td '.$style.'>'.$row['utilization_ratio'].'</td>
						<td '.$style.'>'.$row['efficiency_ratio'].'</td>
					</tr>';
				}
			}
			$body.='</tbody>
				</table>';
		}
		$body.='</div>';
		return $body;
	}
	
	private function fillRateAlertBody($serving){
		$body='<div>
				<div style="color: red;font-style: italic;width: 100%;padding: 5px;background: #FFFFD9;text-align:center;border: 1px solid gray;margin-bottom: 10px;"><strong>Note:</strong> These are the vdoflo campaigns, they are not performing well. Please follow up. </div>';
		if(sizeof($serving)>0){
			$body.='<h4>Vdoflo Campaigns</h4>
						<table id="vdofloStatusReportingtable" border="1" cellpadding="2" cellspacing="4" >
							<thead>
								<tr>
									<th>Campaign ID</th>
									<th>Campaign Name</th>
									<th>Available Inventory</th>
									<th>Unfilled Inventory(vf_xc+n_ui)</th>
									<th>Unfilled Ratio</th>
									<th>Vast Feed Url</th>
								</tr>
							</thead>
							<tbody>';
			foreach($serving as $row){
				$body.='<tr>
					<td>'.$row['cid'].'</td>	
					<td>'.$row['cname'].'</td>
					<td>'.$row['n_ai'].'</td>
					<td>'.$row['unfilled'].'</td>
					<td>'.$row['fill_rate'].'</td>
					<td>'.$row['vastfeedurl'].'</td>
				</tr>';
				
			}
			$body.='</tbody></table>';
		}	
		$body.='</div>';
		return $body;
	}
	
	private function vdofloQueueStatus(){
		$no_of_messages = $this->sqsObj->getSizeofQueue();
		if($no_of_messages >=1000){
			$body = "Hi All, <br /><br />Queue has more than 1000 messages. Number of messages in queue: $no_of_messages, Please check ASAP.";
			$body .= "<br /><br /> Thank you <br />Vdopia Support Team";
			if(!file_exists($this->vdoflo_alert_dir."/".$this->vdoflo_alert['vdofloqueuestatus'])){
				$this->sendMail('Urgent: Queue has more than 1000 messages', $body, "tech_ops_emails");
				touch($this->vdoflo_alert_dir."/".$this->vdoflo_alert['vdofloqueuestatus']);
			}
		}
		
		/*
		$message = $this->sqsObj->receiveMessage();
		$body_main= $message->body->ReceiveMessageResult->Message->Body ;
		$body = (json_decode ($body_main));
		*/
	}
	
	public function run($alert){
		switch($alert){
			case 'vdflostuck':
				$this->vdofloStuck();
				break;
			case 'vdfloinventorystatus':
				$this->vdofloInventoryStatus();
				break;
			case 'vdofloqueuestatus':
				$this->vdofloQueueStatus();
				break;
			case 'vdofloalerts':
				$this->vdofloFeedAlert();
				break;			
		}
	}
}

if (!empty($argc) && strstr($argv[0], basename(__FILE__))) {
	$obj = new VdofloAlerts();
	$obj->run('vdflostuck');
	$obj->run('vdfloinventorystatus');
	$obj->run('vdofloqueuestatus');
	$obj->run('vdofloalerts');
}

?>
