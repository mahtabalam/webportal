<?php 
require_once dirname ( __FILE__ ) . '/../include/config.php';

class RevenueShareUpdate
{
	private $tech_ops_emails;
	private $config;
	private $conn;
	private $db_prefix;
	public function __construct()
	{
		global $reportConn, $config;
		$this->tech_ops_emails='abhay@vdopia.com,pradeep@vdopia.com,nidhi@vdopia.com,qa@vdopia.com,mohitg@vdopia.com,anubhav@vdopia.com';
		$this->conn = $reportConn;
		$this->config = $config;
		if(isset($config['DB_PREFIX']) && $config['DB_PREFIX']!='') {
			$this->db_prefix=$config['DB_PREFIX'];
		}else $this->db_prefix='';
	}
	
	private function sendMail($subj, $body){
		$check = mail($this->tech_ops_emails, $subj, $body, $this->config['support_headers']);
	}
	
	
	
	public function updateDailyStats($pubId)
	{
		$migDate = new DateTime($config['cp_freeze_date']);
		
		//date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' -1 day'))
		//date('Y-m-d H:i:s')
		$getQuery = " select * from revenue_share where isupdated = false and updated_at < '".date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' -1 day'))."' and effective_date >= '".$migDate->format('Y-m-d')."'" ;
		if($pubId != -1)
			$getQuery .= " and publisher_id = ".$pubId;
		$getQuery .= " order by publisher_id, effective_date ";
		
		$rs = $this->conn->Execute($getQuery);
		$channelList;
		if($rs && $rs->recordcount()>0)
		{
			$channelList = $rs->getrows();
		}
		$msg='';
	$prevId = -1;
		foreach( $channelList as $ch)
		{
			$dailyQuery = " update {$this->db_prefix}daily_stats as ds inner join channels as c on c.id = ds.channel_id set ds.pubrevenue_advcurrency = revenue_advcurrency * ".(100 - $ch['rev_share'])."/100   
			where c.publisher_id = ".$ch['publisher_id']." and ds.date >= '".$ch['effective_date']."'";
			$rs = $this->conn->Execute($dailyQuery);
			$ms = $this->conn->ErrorMsg();
			
			$dailyQuery = " update revenue_share  set isupdated = true where id = ".$ch['id'] ;  
			$rs = $this->conn->Execute($dailyQuery);
			
			
			$ms .= $this->conn->ErrorMsg();
			
			if($ms  != '')
			{
				$dailyQuery = " update revenue_share  set isupdated = false where id = ".$ch['id'] ;  
				$rs = $this->conn->Execute($dailyQuery);
				
				$msg .= $ms;
				
				if($prevId != $ch['publisher_id'])
					$pubIds[] = $ch['publisher_id'];
				
				$prevId =$ch['publisher_id'];
			
			}
			
		}
		
		
		if($msg  != '')
		{
			
			$rs = $this->conn->Execute($dailyQuery);
			
			$pubId = implode(',' , $pubIds);
			
			$body = "Hi All, <br /><br />An error has occurred while updating publisher revenue in daily stats table. Please check ASAP.
			 		And manually run cron for following publishers ".$pubId.".";
			$body .= "<br /><br /> Thank you <br />Vdopia Support Team";
			$this->sendMail('Urgent: An error occurred while updating publisher revenue ', $body);
		
		}
		
	}
}


if (!empty($argc) && strstr($argv[0], basename(__FILE__))){
	if(isset($argv[1]) && $argv[1]>0){
		$obj = new RevenueShareUpdate();
		$obj->updateDailyStats($argv[1]);

	}else{
		$obj = new RevenueShareUpdate();
		$obj->updateDailyStats(-1);
	}
}

?>
