<?php
if (! empty ( $argc ) && strstr ( $argv [0], basename ( __FILE__ ) )) {
  # xdebug_disable();
  ini_set("log_errors", "false");
  require_once dirname(__FILE__) . '/../include/config.php';
  ini_set("log_errors", "true");
  # xdebug_enable();

  require_once dirname(__FILE__) . '/../common/include/channelchoice.php';
  require_once dirname ( __FILE__ ) . "/../include/singleton.php";

  class MemCampCron {

    private function fullArrayDiff($left, $right) {
      return array_diff(array_merge($left, $right), array_intersect($left, $right));
    }

    private function verifyChannelSet($campaignId, $channel_choice, $channel_set){
      $goodResult = array(true, "Channel set and choice matched for $campaignId. choice = $channel_choice.");
      $badResult = array(false, "EXCEPTION: Channel set and choice don't match for $campaignId. channel_choice = $channel_choice.");

      if(($channel_choice == NULL || $channel_choice == "NULL")){
        if($channel_set == NULL){
          return $goodResult;
        } else {
          return $badResult;
        }
      } else if($channel_set == NULL){
          return $badResult;
      }
      $conv_channel_arr = VdoChannelChoice::bitSetToIntArr($channel_set);
      $camp_channel_arr = explode(',', $channel_choice);
      $diff = $this->fullArrayDiff($conv_channel_arr, $camp_channel_arr);
      if(count($diff) == 0){
        return $goodResult;
      } else {
          var_dump($diff);
          return $badResult;
      }
    }

    private function convertActiveCampaigns(){
      global $conn;
      $sqlStr = "select id,channel_choice from campaign where status='active' and ";
      $sqlStr .= "budget_amt - tot_money_spent > 5*price_per_unit and daily_limit - money_spent_today > 0 and ";
      $sqlStr .= "review_status<>'Pending Review' ";
      $sql = $conn->Prepare($sqlStr);
      $rs = $conn->Execute($sql,array());
      if(!$rs){ throw new Exception($conn->ErrorMsg());}
      if($rs->recordcount() > 2000){ throw new Exception($conn->ErrorMsg());}
      $idChanSetArray = array();
      if($rs && $rs->recordcount()>0){
        $rows = $rs->getRows();
        $sqlStr = "delete from campchanset";
        $sql = $conn->Prepare($sqlStr);
        $rs = $conn->Execute($sql,array());
        if(!$rs){ throw new Exception($conn->ErrorMsg());}
   
        $sqlStr = "insert into campchanset values ";
        $firstRow = true;
        foreach($rows as $row){
          array_push($idChanSetArray, $row['id'], $row['channel_choice'], VdoChannelChoice::intListToBitSet($row['channel_choice']));
          if($firstRow){
            $firstRow = false;
            $sqlStr .= "(?,?,?)";
          } else {
            $sqlStr .= ",(?,?,?)";
          }
        }
        $beginT = microtime();
        $sql = $conn->Prepare($sqlStr);
        $rs = $conn->Execute($sql, $idChanSetArray);
        $endT = microtime();
        if(!$rs){ throw new Exception($conn->ErrorMsg());}
        echo "Updating " . count($idChanSetArray)/3 . " campaigns completed in " . ($endT - $beginT)/1000 . " ms. \n";
      } else {
        echo "No active channels."."\n";
      }
    }
    
    private function validateActiveCampaigns($num){
      global $conn;
      $limit = "";
      if($num > 0){
        $limit = "order by rand() limit $num";
      }
      $sql = $conn->Prepare("select id as cid, channel_choice, channel_set from campaign " . $limit);
      $rs = $conn->Execute($sql,array());
      if(!$rs){ throw new Exception($conn->ErrorMsg());}
      if($rs && $rs->recordcount()>0){
        $rows = $rs->getRows();
        $bad = 0;
        foreach($rows as $row){
          $result = $this->verifyChannelSet($row['cid'], $row['channel_choice'], $row['channel_set']);
          if($result[0] != true){
            echo $result[1]. "\n";
            $bad++;
          }
        }
        echo "Verification result: " . (count($rows) - $bad) . " out of " . count($rows) . " passed.\n";
      } else {
        echo "No active channels."."\n";
      }
    }
 
    private function recreateMemCamp(){
      global $conn;
      $dropMemNewSql = "drop table if exists memcampnew";
      $createMemNewSql = "create table memcampnew like memcamp";
      $insertSql = "insert into  memcampnew select campaign.advertiser_id, country_codes, state_codes, postalcodes, dma_codes, cities, device, track_choice, overlay_choice, ";
      $insertSql .= "video_choice,cat_choice, timetargetfrom, timetargetto, vdofx_details,review_status,campaign.id as cid, price_norm as maxprice, ";
      $insertSql .= "UNIX_TIMESTAMP(validfrom) - UNIX_TIMESTAMP() as vf, UNIX_TIMESTAMP(validto) - UNIX_TIMESTAMP() as vt, stg as stg, wtg as wtg, cg , depends, "; 
      $insertSql .= "(price_per_unit/price_norm) as ex_rate, custom_details, ads.id,ad_format, branded_img_right_ref_imgname as title, dimension, duration, ";
      $insertSql .= "TRIM(destination_url), action_type, replace(TRIM(tracker_url),\"|\",\"%7C\") as tracker_url, custom_canvas_url, if(unit is not null, unit, '') as frequnit , ";
      $insertSql .= "frequency.impressions as freq_cap_imp, branded_logo_ref, ";
      $insertSql .= "branded_img_bot_ref_txbody, playlist_ad_ref, branded_img_left_ref, branded_img_right_ref_imgname,branded_color, sri_details, campaign.channel_set, ad_details, priority_level ";
      $insertSql .= "from campaign_members inner join campaign  on campaign.id=campaign_members.cid ";
      $insertSql .= "left join geography  on campaign.id=geography.campaign_id left join frequency on campaign.id=frequency.campaign_id ";
      $insertSql .= "inner join ads on ads.id= campaign_members.ad_id inner join adv_credit  on adv_id=campaign.advertiser_id ";
      $insertSql .= "where now()>=validfrom AND (now()<=validto OR isnull(validto)) and credit_amt>=price_per_unit and budget_amt - tot_money_spent > 5*price_per_unit ";
      $insertSql .= "and daily_limit - money_spent_today > 0 and review_status<>'Pending Review' and campaign_members.status='enabled' and campaign.status='active' ";
      $insertSql .= "and ads.ad_format !='vastfeed'; ";
      $mvMemNewSql = "rename table memcamp to memcampold, memcampnew to memcamp";
      $dropMemOldSql = "drop table memcampold";

      # $lockSql = "LOCK TABLES memcamp WRITE, campchanset READ, adv_credit READ,campaign_members READ,campaign READ, frequency READ, geography READ, ads READ";
      # $unlockSql = "unlock tables; ";

      $beginT = microtime();
      $rs = $conn->execute($dropMemNewSql);
      if(!$rs){echo $dropMemNewSql; throw new Exception($conn->ErrorMsg());}
      $rs = $conn->execute($createMemNewSql);
      if(!$rs){echo $createMemNewSql; throw new Exception($conn->ErrorMsg());}
      $rs = $conn->execute($insertSql);
      if(!$rs){echo $insertSql; throw new Exception($conn->ErrorMsg());}
      $rs = $conn->execute($mvMemNewSql);
      if(!$rs){echo $mvMemNewSql; throw new Exception($conn->ErrorMsg());}
      $rs = $conn->execute($dropMemOldSql);
      if(!$rs){echo $dropMemOldSql; throw new Exception($conn->ErrorMsg());}
      $endT = microtime();
      echo "Updating memcamp completed in " . ($endT - $beginT)/1000 . " ms. \n";
    }

    public function run(){
      if(VdoSingle::isSingletonRunning(VdoSingle::VDO_SINGLE_MEMCAMPCRON)){
        echo( "A script is already running. Exiting ... \n");
        exit(0);
      }

     // $this->convertActiveCampaigns();
     $this->validateActiveCampaigns(50);
     $this->recreateMemCamp();
    }
  }

  $mmc = new MemCampCron();
  $mmc->run();
}
