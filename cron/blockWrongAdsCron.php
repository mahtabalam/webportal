<?php 
include_once(dirname(__FILE__).'/../include/config.php');

$sql = "select distinct ads.playlist_ad_ref, ads.id, campaign.id as cid from campaign,campaign_members, ads where campaign.id=cid and ads.id=ad_id and campaign.status != 'expired' and campaign.status != 'deleted' and campaign.review_status != 'disapproved' and campaign_members.status='enabled' and device='iphone' and (ad_format='video' or ad_format='vdobanner' or ad_format='leadervdo' or ad_format='vastfeed' or ad_format='preroll' or ad_format='mvtpreroll') and ads.status = -1 and ads.playlist_ad_ref is not null and ads.playlist_ad_ref != 'ad_conversion_failed'";
$rs = $conn->execute($sql);
if ($rs && $rs->recordcount() > 0) {
	$files = $rs->getrows();
	
	foreach ( $files as $index => $element) {
		
		$mp4Name = $config["BASE_DIR"]."/".$config['files_folder_name']."/videos/".$element['playlist_ad_ref'].'_a.mp4';
		
		$reason = "";
		if(!file_exists($mp4Name)){
			$reason = " proper media file (_a.mp4) was not generated. ";
				
			$updateSql = "update campaign_members set status = 'disabled' where ad_id=? and cid=?";
			$updateSql = $conn->prepare($updateSql);
			$conn->execute($updateSql, array($element['id'], $element['cid']));
		
			$insertSql = "insert into blocked_ads set ad_id=?, reason=?";
			$insertSql = $conn->prepare($insertSql);
			$conn->execute($insertSql, array($element['id'], $reason));
		}
	}
}
?>
